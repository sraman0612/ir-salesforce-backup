<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Bay Area</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Great Lakes</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Great Plains</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Midwest</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>North Texas</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Northwest</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Rocky Mountain</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>South Alberta</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>South Texas</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Southwest</values>
        </dashboardFilterOptions>
        <name>District Filter</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>Dryer Focus Details</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_District__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>OPPORTUNITY_NAME</column>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>FULL_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity_Line_Item__c.Quantity__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity_Line_Item__c.Total__c</column>
                <showTotal>true</showTotal>
                <sortBy>RowValueDescending</sortBy>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Top Jobs</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>30</maxValuesDisplayed>
            <report>NA_Marketing_Reports/Dryer_Pipeline</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Top 30 Deals</title>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity_Line_Item__c.Total__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_District__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity.Opportunity_District__c</groupingColumn>
            <groupingSortProperties/>
            <header>Top Performers by District</header>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Marketing_Reports/Dryer_Wins_YTD</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Top to Bottom YTD Closed Won Dryer Deals</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Opportunity_Line_Item__c.Total__c</column>
            </chartSummary>
            <componentType>Bar</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_District__c</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Opportunity.Opportunity_District__c</groupingColumn>
            <groupingSortProperties/>
            <legendPosition>Bottom</legendPosition>
            <report>NA_Marketing_Reports/Dryer_Pipeline_by_Dist</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>false</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowValueAscending</sortBy>
            <title>90 Day Dryer Pipeline</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <owner>chris_leight@irco.com</owner>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>Opportunity.Opportunity_District__c</column>
            </dashboardFilterColumns>
            <dashboardTableColumn>
                <column>CLOSE_MONTH</column>
                <sortBy>RowLabelDescending</sortBy>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <calculatePercent>false</calculatePercent>
                <column>OPPORTUNITY_NAME</column>
                <showTotal>false</showTotal>
            </dashboardTableColumn>
            <dashboardTableColumn>
                <aggregateType>Sum</aggregateType>
                <calculatePercent>false</calculatePercent>
                <column>Opportunity_Line_Item__c.Total__c</column>
                <showTotal>true</showTotal>
            </dashboardTableColumn>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <groupingSortProperties/>
            <header>Most Recent Wins</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <maxValuesDisplayed>30</maxValuesDisplayed>
            <report>NA_Marketing_Reports/Dryer_Wins_YTD</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <title>Recent 30 Wins</title>
        </components>
    </rightSection>
    <runningUser>james.cumiskey@irco.com</runningUser>
    <textColor>#000000</textColor>
    <title>Dryer Dashboard West Region</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
