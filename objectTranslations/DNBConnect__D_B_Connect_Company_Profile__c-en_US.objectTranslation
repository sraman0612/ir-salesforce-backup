<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- The Dun &amp; Bradstreet D-U-N-S® Number is an identification number assigned by Dun &amp; Bradstreet that uniquely identifies the entity in accordance with the Data Universal Numbering System (D-U-N-S). --></help>
        <label><!-- D-U-N-S Number --></label>
        <name>DNBConnect__DUNSNumber__c</name>
    </fields>
    <fields>
        <help><!-- The two-letter country code, defined by the International Organization for Standardization (ISO) ISO 3166-1 scheme, identifying the country in which the entity is located. --></help>
        <label><!-- ISO Alpha 2 Char Country Code --></label>
        <name>DNBConnect__ISO_Alpha_2_Char_Country_Code__c</name>
    </fields>
    <fields>
        <help><!-- The single name by which the entity is primarily known or identified. --></help>
        <label><!-- Primary Business Name --></label>
        <name>DNBConnect__PrimName__c</name>
    </fields>
    <fields>
        <help><!-- The three-letter currency code, defined in the ISO 4217 scheme published by International Organization for Standardization (ISO) identifying the type of money in which this amount is expressed (e.g. USD, CAD, GBP, EUR). --></help>
        <label><!-- Fins Yearly Revenue Currency --></label>
        <name>Fins_yearlyRevenue_curr__c</name>
    </fields>
    <fields>
        <help><!-- The monetary value of income received from customers from the sale of the entity&apos;s goods and/or services.  This is the gross sales minus any returns, rebates/discounts, allowances for damages or shortages, shipping expenses passed on to the customer. --></help>
        <label><!-- Fins Yearly Revenue Value --></label>
        <name>Fins_yearlyRevenue_value__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 1 --></label>
        <name>IndustCodes_Descr_NAICS_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 2 --></label>
        <name>IndustCodes_Descr_NAICS_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 3 --></label>
        <name>IndustCodes_Descr_NAICS_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 4 --></label>
        <name>IndustCodes_Descr_NAICS_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 5 --></label>
        <name>IndustCodes_Descr_NAICS_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) description that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Description Priority 6 --></label>
        <name>IndustCodes_Descr_NAICS_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 1 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 2 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 3 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 4 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 5 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) description that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Description Priority 6 --></label>
        <name>IndustCodes_Descr_US_1987_SIC_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 1 --></label>
        <name>IndustCodes_Descr_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 2 --></label>
        <name>IndustCodes_Descr_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 3 --></label>
        <name>IndustCodes_Descr_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 4 --></label>
        <name>IndustCodes_Descr_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 5 --></label>
        <name>IndustCodes_Descr_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification description that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Description Priority 6 --></label>
        <name>IndustCodes_Descr_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 1 --></label>
        <name>IndustCodes_NAICS_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 2 --></label>
        <name>IndustCodes_NAICS_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 3 --></label>
        <name>IndustCodes_NAICS_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 4 --></label>
        <name>IndustCodes_NAICS_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 5 --></label>
        <name>IndustCodes_NAICS_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The North American Industry Classification System (NAICS) code that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- NAICS Code Priority 6 --></label>
        <name>IndustCodes_NAICS_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 1 --></label>
        <name>IndustCodes_US_1987_SIC_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 2 --></label>
        <name>IndustCodes_US_1987_SIC_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 3 --></label>
        <name>IndustCodes_US_1987_SIC_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 4 --></label>
        <name>IndustCodes_US_1987_SIC_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 5 --></label>
        <name>IndustCodes_US_1987_SIC_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The US Standard Industrial Classification (SIC) code that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- US 1987 SIC Code Priority 6 --></label>
        <name>IndustCodes_US_1987_SIC_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 1 --></label>
        <name>IndustCodes_priority1__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the 2nd greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 2 --></label>
        <name>IndustCodes_priority2__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the 3rd greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 3 --></label>
        <name>IndustCodes_priority3__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the 4th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 4 --></label>
        <name>IndustCodes_priority4__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the 5th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 5 --></label>
        <name>IndustCodes_priority5__c</name>
    </fields>
    <fields>
        <help><!-- The D&amp;B Standard Industrial Classification code that identifies the business activity in which the entity is engaged. The entity performs the 6th greatest percentage of its business within this industry. --></help>
        <label><!-- D&amp;B Industry Code Priority 6 --></label>
        <name>IndustCodes_priority6__c</name>
    </fields>
    <fields>
        <help><!-- The date when the accounting period ended. If the figures are projected (future), then this is the date when the accounting period will end.Format: YYYY-MM-DD or YYYY-MM --></help>
        <label><!-- Fiscal Year End --></label>
        <name>fiscal_year_end__c</name>
    </fields>
    <fields>
        <help><!-- The total number of people engaged by the entity to perform its operations. The scope of this employee figure is identified in informationScopeDescription. --></help>
        <label><!-- NumEmpl-HQ/Indiv-Value --></label>
        <name>hq_ind_numberofemployees_val__c</name>
    </fields>
    <fields>
        <help><!-- The description of the entity&apos;s legal form as registered with government authorities (e.g., Public Limited Liability Company, Foreign Branch of Limited Company, Public Corporation). --></help>
        <label><!-- LegalForm Description --></label>
        <name>legalForm_Descr__c</name>
    </fields>
    <fields>
        <help><!-- A unique name used to identify the website of the entity that serves as an address to access that website. --></help>
        <label><!-- Organization Website Address Domain 1 --></label>
        <name>organization_website_address_domain_1__c</name>
    </fields>
    <fields>
        <help><!-- The Internet URL (Uniform Resource Locator) that uniquely identifies the proprietary website of the entity or the website preferred by the entity that gives more information on the entity. --></help>
        <label><!-- Organization Website Address URL 1 --></label>
        <name>organization_website_address_url_1__c</name>
    </fields>
</CustomObjectTranslation>
