trigger ContentVersion_Trigger on ContentVersion (before update, after update) {
    
    if(trigger.isbefore){
        ContentVersion_TriggerHandler.beforeUpdate(trigger.newMap, trigger.oldMap);
    }
    
    if(Trigger.isAfter){
        ContentVersion_TriggerHandler.afterUpdate(trigger.newMap, trigger.oldMap);
    }
}