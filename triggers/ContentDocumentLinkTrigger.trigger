trigger ContentDocumentLinkTrigger on ContentDocumentLink (before insert, after insert) {
    
    if(trigger.isbefore){
        //This trigger will share all Chatter files with Community Users
        for(ContentDocumentLink l:Trigger.new) {
            l.Visibility='AllUsers';
        }
    }else{
        ContentDocumentLink_TriggerHandler.afterInsert(trigger.newMap);
    }

}