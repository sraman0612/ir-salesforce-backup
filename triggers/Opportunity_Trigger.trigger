//
// (c) 2015, Appirio Inc.
//
// Opportunity Trigger
//
// April 17,2015    Surabhi Sharma    T-377640(delete the record)
// Dec 9th 2015 -  Dhilip Updated
// Nov 30th 2016 Cal Steele / Ben Lorenz refactored to handle Club_Car types
// Feb 8 2018 CloudGofer
//              - Added PowerTools Opportunity Trigger Code at BEFORE UPDATE and AFTER UPDATE
// 8/11/2021 - Removed all Club Car and Residential references
trigger Opportunity_Trigger on Opportunity(before insert, before update, before delete, after update, after insert) {    
    
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Opportunity');  
     //Merged Triggers as per SGOM 144.
     if(skipTrigger!=True){         
         if(!EMEIA_OpportunityHandler.hasRanOnce){
             system.debug('hasRanOnce Opportunity--> '+EMEIA_OpportunityHandler.hasRanOnce);
             EMEIA_OpportunityHandler.user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
            EMEIA_OpportunityHandler.rtList = [SELECT Id, DeveloperName FROM RecordType where SobjectType='Opportunity'];
             EMEIA_OpportunityHandler.hasRanOnce = true;
         }
         system.debug('user Opportunity--> '+EMEIA_OpportunityHandler.user);
    switch on Trigger.operationType {
                        
                        When BEFORE_UPDATE{
                            
                                EMEIA_OpportunityHandler.validateBusiness(trigger.new,false,Trigger.oldmap,EMEIA_OpportunityHandler.user);
                                EMEIA_OpportunityHandler.beforeUpdate(trigger.new,trigger.old,Trigger.oldmap,EMEIA_OpportunityHandler.user,EMEIA_OpportunityHandler.rtList);                                                                 
                        }
                        When BEFORE_DELETE{ 
                                EMEIA_OpportunityHandler.beforeDelete(trigger.new,trigger.old,EMEIA_OpportunityHandler.rtList);                                
                        } 
                        When AFTER_UPDATE{
                                EMEIA_OpportunityHandler.afterUpdate(trigger.new,trigger.old,EMEIA_OpportunityHandler.rtList);
                                Opportunity_TriggerHandler.shareOppsWithChannelPartner(trigger.newmap,trigger.oldmap);
                        }
                        When BEFORE_INSERT{
                                EMEIA_OpportunityHandler.validateBusiness(trigger.new,true,null,EMEIA_OpportunityHandler.user);
                                EMEIA_OpportunityHandler.validateBusinessRelationship(trigger.new,EMEIA_OpportunityHandler.user);
                                EMEIA_OpportunityHandler.beforeInsert(Trigger.new,EMEIA_OpportunityHandler.user);
                        }
        				WHEN AFTER_INSERT{
                            HttpCalloutInterlynx.oppAfterInsert(trigger.new);
                        }

                }
     }
}