// Handler Class for OpportunityWinPlan_Trigger
//
// Created on Dec 11th,2018 by Harish KJ

trigger OpportunityWinPlan_Trigger on OpportunityWinPlan__c (before insert, before update, after Insert, after Update, after delete) {

    List<OpportunityWinPlan__c> OpportunityWinPlanNew = new List<OpportunityWinPlan__c>();
    List<OpportunityWinPlan__c> OpportunityWinPlanOld = new List<OpportunityWinPlan__c>();
    
    //Separate new updates objects by record types    
    if(null!=trigger.new)
    {
        for (OpportunityWinPlan__c a : trigger.new)
        {
            OpportunityWinPlanNew.add(a);
        }
    }
    
   //Separate old values by record types
   if(null!=trigger.old)
   {
       for (OpportunityWinPlan__c a : trigger.old)
       {
            OpportunityWinPlanOld.add(a);
       }
   }
   
   Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('OpportunityWinPlan__c');
    
    if (trigger.isBefore)
    {
        system.debug('trigger.isBefore');
        
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            if(!OpportunityWinPlanNew.isEmpty())
            {
                OptyWinPlan_Trigger_TriggerHandler.beforeUpdate(OpportunityWinPlanNew);
            }    
        } 
        
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            if(!OpportunityWinPlanNew.isEmpty())
            {
                OptyWinPlan_Trigger_TriggerHandler.beforeInsert(OpportunityWinPlanNew);
            }    
        }
        
    }
   
    if(trigger.isAfter)
    {    
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            if(!OpportunityWinPlanNew.isEmpty())
            {
                OptyWinPlan_Trigger_TriggerHandler.afterUpdate(OpportunityWinPlanNew);
            }    
        } 
        
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            if(!OpportunityWinPlanNew.isEmpty())
            {
                OptyWinPlan_Trigger_TriggerHandler.afterInsert(OpportunityWinPlanNew);
            }    
        }
        
        if(trigger.isDelete && !softDisable.deleteDisabled())
        {
            if(!OpportunityWinPlanOld.isEmpty())
            {
                OptyWinPlan_Trigger_TriggerHandler.afterDelete(OpportunityWinPlanOld);
            }
        }
    }
    
}