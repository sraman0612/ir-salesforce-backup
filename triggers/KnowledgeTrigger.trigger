trigger KnowledgeTrigger on Knowledge__kav(before insert, before update, before delete, after insert, after update, after delete, after undelete) {   
  Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Knowledge');
  if (Trigger.isBefore){
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  //BEFORE INSERT
    } else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  //BEFORE UPDATE
    }
  } else {
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  // AFTER INSERT
    } else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  // AFTER UPDATE  
         CTS_TD_ChatterMentionFollowers.postToFollowers(Trigger.new, Trigger.oldMap);//@mention chatter followers in CTS TD community      
    }
  }
  
}