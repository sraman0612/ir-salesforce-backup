//Created By Capgemini Aman kumar 12/15/2022 
trigger Business_Relationship_Trigger on Business_Relationship__c (after insert, after update, before update, before insert) {
Boolean skipTrigger=CGCommonUtility.sKipTrigger('Business_Relationship__c');  

    if(skipTrigger!=True){                    
    switch on Trigger.operationType {
                        When BEFORE_INSERT{  
                                VPTECH_BusinessRelatioshipHandler.beforeInsert(trigger.new);
                        }
                        When BEFORE_UPDATE{                                             
                                VPTECH_BusinessRelatioshipHandler.beforeUpdate(trigger.new,Trigger.old,Trigger.oldmap);                                                                 
                        }
                        When AFTER_INSERT{                                              
                                VPTECH_BusinessRelatioshipHandler.afterInsert(trigger.new);                                                                                                                         
                        }
                        When AFTER_UPDATE{                                              
                                VPTECH_BusinessRelatioshipHandler.afterUpdate(trigger.new,Trigger.oldmap);
                        }
                        When AFTER_DELETE{                                              
                                VPTECH_BusinessRelatioshipHandler.afterDelete(trigger.old);                                
                        }
                }
     }
}