/**
* Used to share attached files/content document with Community Users
* 
* */

trigger CTS_TechDirect_ShareFilesWithCommunityUsersTrigger on ContentDocumentLink (before insert) {
    
        ShareFilesWithCommunityUser_TrgrHandlr.ShareFilesWithCommunityUsers(trigger.new);

        CTS_TechDirectArticleFileVisibility.ShareFilesWithCommunityUsers(trigger.new);
     
}