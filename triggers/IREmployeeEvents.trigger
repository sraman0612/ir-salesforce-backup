trigger IREmployeeEvents on IR_Employee__c (before insert, before update, before delete,
                                            after insert, after update, after delete, after undelete) {

  Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('IR_Employee__c');

  if (Trigger.isBefore ){
    
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  //BEFORE INSERT
    
    } 
    else if (Trigger.isUpdate && !softDisable.updateDisabled()) {  //BEFORE UPDATE 
    
    } 
    else if (Trigger.isDelete) {  // BEFORE DELETE
    
    }
  } 
  else {
    
    if (Trigger.isInsert && !softDisable.insertDisabled()) {  // AFTER INSERT
      IREmployeeEventHandler.copyDataToEmployeeFromUser(trigger.new);
      IREmployeeEventHandler.deactivateUsers(trigger.new, null);
    } 
    else if (Trigger.isUpdate  && !softDisable.updateDisabled()) {  // AFTER UPDATE
      IREmployeeEventHandler.copyDataToEmployeeFromUser(trigger.new);
      IREmployeeEventHandler.deactivateUsers(trigger.new, trigger.oldMap);
    } 
    else if (Trigger.isDelete && !softDisable.deleteDisabled()) {  // AFTER DELETE
    
    } 
    else if (Trigger.isUndelete && !softDisable.undeleteDisabled()) {  // AFTER UNDELETE
    
    }
  }
}