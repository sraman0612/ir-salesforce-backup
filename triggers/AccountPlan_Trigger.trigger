// AccountPlan_Trigger
//
// Created on Nov 29th,2018 by Dhilip C

trigger AccountPlan_Trigger on AccountPlan__c (before Insert, before Update, after Insert, after Update, after delete) {

    List<AccountPlan__c> AccountPlansNew = new List<AccountPlan__c>();
    List<AccountPlan__c> AccountPlansOld = new List<AccountPlan__c>();
    
    //Separate new updates objects by record types    
    if(null!=trigger.new)
    {
        for (AccountPlan__c a : trigger.new)
        {
            AccountPlansNew.add(a);
        }
    }
    
   //Separate old values by record types
   if(null!=trigger.old)
   {
       for (AccountPlan__c a : trigger.old)
       {
            AccountPlansOld.add(a);
       }
   }
   
   Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('AccountPlan__c');
    
    if(trigger.isBefore)
    {    
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            if(!AccountPlansNew.isEmpty())
            {
                AccountPlan_TriggerHandler.beforeUpdate(AccountPlansNew);
            }    
        } 
        
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            if(!AccountPlansNew.isEmpty())
            {
                AccountPlan_TriggerHandler.beforeInsert(AccountPlansNew);
            }    
        }
        
    }
   
    if(trigger.isAfter)
    {    
        if(trigger.isUpdate && !softDisable.updateDisabled())
        {
            if(!AccountPlansNew.isEmpty())
            {
                AccountPlan_TriggerHandler.afterUpdate(AccountPlansNew);
            }    
        } 
        
        if(trigger.isInsert && !softDisable.updateDisabled())
        {
            if(!AccountPlansNew.isEmpty())
            {
                AccountPlan_TriggerHandler.afterInsert(AccountPlansNew);
            }    
        }
        
        if(trigger.isDelete && !softDisable.deleteDisabled())
        {
            if(!AccountPlansOld.isEmpty())
            {
               AccountPlan_TriggerHandler.afterDelete(AccountPlansOld);
            }
        }
    }
    
}