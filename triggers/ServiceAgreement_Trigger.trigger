//
// (c) 2015, Appirio Inc.
//
// Service Agreement Trigger
//
// April 16,2015    Surabhi Sharma    T-377640
// August 17, 2015    Doug Weinheiemr    Added Soft Disable
//
trigger ServiceAgreement_Trigger on Service_Agreement__c (after update) {
Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Service_Agreement__c');
    if(trigger.isAfter){
        if(trigger.isUpdate && !softDisable.updateDisabled())
            ServiceAgreementTriggerHandler.afterUpdate(trigger.oldMap, trigger.new); //calls afterUpdate method of ServiceAgreementTriggerHandler
    }
}