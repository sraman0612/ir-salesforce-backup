// (c) 2015, Appirio Inc.
// Contact Trigger
// April 17,2015    Surabhi Sharma    T-377640
// Dec 9th 2015 - Dhilip Updated
// May 16th 2016 - Sudeep - Added extra logic for Contact owner update 
// July 13, 2016 - Ben Lorenz - refactored to put records in SBU MAP
// 8/11/2021 - Removed all Club Car references
trigger Contact_Trigger on Contact (before insert,before update,after update, after delete) {

     /* Code updated by Vineet Capgemini on 11-Oct-21 to update Logic CG_Org_Channel__c on before events*/
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Contact');
    if(skipTrigger!=True){
    List<Contact> newContactList = new List<Contact>();
    List<Contact> lirContactList = new List<Contact>();
    List<Contact> lgdContactList = new List<Contact>();
    List<Contact> afterlirContactList = new List<Contact>();
    List<Contact> afterlgdContactList = new List<Contact>();
    List<Contact> sTrchannel = new List<Contact>();    
        switch on Trigger.operationType {
        When BEFORE_INSERT{
          //CGCommonUtility.ContactRecordTypeUpdate(Trigger.new,null,Trigger.NewMap,true);  
                     sTrchannel=CGCommonUtility.branchingAndChannelPopulation(trigger.new);
        }
        
    }
     
     if(!trigger.isDelete){
     //before list   
     for(Contact con:sTrchannel){
        if(con.CG_Org_Channel__c==System.Label.CG_EMEIA_Trigger){
            lirContactList.add(con);
        }
        else if(con.CG_Org_Channel__c==System.Label.CG_BRNO_Trigger){
            lgdContactList.add(con);
        }
     }
     //after list   
     for(Contact con : trigger.new){
        if(trigger.isAfter){
            if(con.CG_Org_Channel__c!=null){
                if(con.CG_Org_Channel__c==System.Label.CG_EMEIA_Trigger){
                  afterlirContactList.add(con);
              }
              else if(con.CG_Org_Channel__c==System.Label.CG_BRNO_Trigger){
                  afterlgdContactList.add(con);
              }
            }
        }
      }      
     }
     Map<Id,Contact> contactMap;        
           switch on Trigger.operationType {
                        When BEFORE_INSERT{                        
                            EMEIA_contactTriggerHandler.beforeInsert(lirContactList);  
                            GD_contactTriggerHandler.beforeInsert(trigger.new);
                            
                        }
                        When BEFORE_UPDATE{
                            for(Contact cn:lirContactList){
                                contactMap = new Map<Id,Contact>();
                                contactMap.put(cn.Id,cn);
                                EMEIA_contactTriggerHandler.beforeUpdate(contactMap, trigger.oldMap);
                            }   
                            
                            GD_contactTriggerHandler.beforeUpdate(trigger.new,trigger.old,trigger.oldmap);

                        }
                     
                         When AFTER_UPDATE{
                                EMEIA_contactTriggerHandler.afterUpdate(afterlirContactList);
                            // GD_contactTriggerHandler.afterUpdate(afterlgdContactList);
                               
                        }
                     When AFTER_DELETE{
                                EMEIA_contactTriggerHandler.afterDelete(afterlirContactList);                            
                        }
            }             
    }
 
}