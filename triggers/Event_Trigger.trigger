// 
// (c) 2015 Appirio, Inc.
//
// Trigger On Event object
//
// Apr 30, 2015   Barkha Jain       Original 
// Mar 4,  2019   Ben Lorenz        Refactored to include all trigger events, added PT handler

trigger Event_Trigger on Event (before insert, before update, before delete,after insert, after update, after delete, after undelete) {
  Boolean skipTrigger=CGCommonUtility.sKipTrigger('Event');
  List<Event> EventList ;
     if(skipTrigger!=true){ 
   Set<ID> accIDs = new Set<ID>();
   List<Event> eventVPTECHIDs = new List<Event>();
  Id eventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('Standard_Event').getRecordTypeId();
  Map<id, Event> oldEventMap, newEventMap;
   if(null!=trigger.new){
      for (Event a : trigger.new){
         if (a.RecordTypeId == eventRecordTypeId ){
              eventVPTECHIDs.add(a);
          }
          }}
  if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
        Event_TriggerHandler.beforeInsert(Trigger.new);
        PT_EventTriggerHandler.blockEventsOnKeyAccounts(Trigger.new);
        PT_EventTriggerHandler.setWhatIdToContactAccount(Trigger.new);
        List<Event> airNAEventNew = new List<Event>();
        // Added by CG to check inactive accounts on Event
          if(!eventVPTECHIDs.isEmpty()){
        for(Event accEvent : eventVPTECHIDs){
           if(accEvent.whatID != null && ((string)accEvent.whatID).startsWith('001')){
            accIDs.add(accEvent.whatID);
                }
        }
            Event_TriggerHandler.inactiveEvent(accIDs, eventVPTECHIDs);
        }
       // Task_BillTo_ShipTOInactiveCheck.BillTo_ShipTOInactiveCheckEvent(Trigger.new);
    } else if (Trigger.isUpdate) {  //BEFORE UPDATE
         PT_EventTriggerHandler.setWhatIdToContactAccount(Trigger.new);
    } else if (Trigger.isDelete) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert) {  // AFTER INSERT
        newEventMap = trigger.newMap;
        PT_EventTriggerHandler.updateEventPlannedDate(trigger.new);
    } else if (Trigger.isUpdate) {  // AFTER UPDATE
        newEventMap = trigger.newMap;
        oldEventMap = trigger.oldMap;
        PT_EventTriggerHandler.updateEventPlannedDate(trigger.new);
        PT_EventTriggerHandler.linkOutlookEvent(Trigger.New);
    } else if (Trigger.isDelete) {  // AFTER DELETE
         oldEventMap = trigger.oldMap;
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
    EventList = trigger.isDelete ? trigger.old : trigger.new ;
    TaskMethods.UpdateFaceToFaceEvent(EventList, trigger.oldMap, trigger.newMap);  
    TaskMethods.SetMaxEventDateField(EventList);  
  }
     }
}