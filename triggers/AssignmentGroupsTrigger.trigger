trigger AssignmentGroupsTrigger on Assignment_Groups__c (before insert, before update) {
    switch on Trigger.operationType{
        when BEFORE_INSERT, BEFORE_UPDATE{
            AssignmentGroupsTriggerHandler.queueNameHandler(trigger.new);
        }
    }
}