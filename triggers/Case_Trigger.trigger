// 8/11/2021 - removed all Club Car and Residential references
trigger Case_Trigger on Case(before insert, before update, after insert) {   
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Case');  
    if(skipTrigger!=True){
        if (Trigger.isBefore){
            
            if (Trigger.isInsert ) {  //BEFORE INSERT
                CTS_OM_Case_TriggerHandler.onBeforeInsert(trigger.new);
                // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -START
                CTS_OM_Case_TriggerHandler.validateBusiness(trigger.new);
                // Changes by Capgemini Devender Singh Date 28/6/2023. SGGC 132 -END
                
                CaseResponseTriggerHandler.copyOnHoldReasonToSubStatus(Trigger.new);
            } 
            else if (Trigger.isUpdate ) {  //BEFORE UPDATE
                CTS_OM_UpdateCaseAge.updateCaseAge(trigger.new, Trigger.oldMap);
                //Milton Roy Total On hold Time Calculation
                MRCalculateTotalOnHoldDays.calculateOnHoldDays(Trigger.new,Trigger.oldMap,Trigger.newMap);
                CaseResponseTriggerHandler.copyOnHoldReasonToSubStatus(Trigger.new);
                Case_TriggerHandlerHelper.updateActionItemRollupSummary(Trigger.oldmap,Trigger.newmap);
            }
             

             
        }
        else if (Trigger.isAfter){
            
            if (Trigger.isInsert){
                //CaseResponseTriggerHandler.reply(Trigger.new);
            }
           
           
        }
       
    }
}