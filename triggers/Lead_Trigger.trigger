//
// (c) 2015,Appirio Inc.
//
// Lead Trigger
//
// April 17,2015    Surabhi Sharma    T-377640(delete the record)
trigger Lead_Trigger on Lead (after insert, before insert, before update, after update){  
    
    Boolean skipTrigger=CGCommonUtility.skipTrigger('Lead');  
    //Merged Triggers as per SGOM 144.
    system.debug('skipTrigger --> '+skipTrigger);
    if(skipTrigger!=True){   
        //Changes by Capgemini Devender Singh Date 13/6/2023. START US SGGC-70
        if(!EMEIA_LeadTriggerHandler.hasRanOnce){ 
            system.debug('hasRanOnce Lead--> '+EMEIA_LeadTriggerHandler.hasRanOnce);
            EMEIA_LeadTriggerHandler.user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
            EMEIA_LeadTriggerHandler.hasRanOnce = true;
        }
        system.debug('user Lead--> '+EMEIA_LeadTriggerHandler.user);
        switch on Trigger.operationType {
            When BEFORE_INSERT{
                EMEIA_LeadTriggerHandler.validateBusiness(trigger.new,true,null,EMEIA_LeadTriggerHandler.user);
                EMEIA_LeadTriggerHandler.beforeInsert(trigger.new,trigger.old,EMEIA_LeadTriggerHandler.user);                      
            }
            When BEFORE_UPDATE{
                EMEIA_LeadTriggerHandler.validateBusiness(trigger.new,false,Trigger.oldmap,EMEIA_LeadTriggerHandler.user);
                EMEIA_LeadTriggerHandler.beforeUpdate(trigger.new,trigger.old,trigger.oldMap,EMEIA_LeadTriggerHandler.user);  
            }
            //Changes by Capgemini Devender Singh Date 13/6/2023. END US SGGC-70
            When AFTER_INSERT{
                EMEIA_LeadTriggerHandler.afterInsert(trigger.new,trigger.old);
                HttpCalloutInterlynx.afterInsert(trigger.new);
            }
            When AFTER_UPDATE{
                EMEIA_LeadTriggerHandler.afterUpdate(trigger.new,trigger.oldMap,trigger.old);
                HttpCalloutInterlynx.afterUpdate(trigger.new, trigger.oldMap);
                Lead_TriggerHandler.shareLeadsWithChannelPartner(trigger.newMap, trigger.oldMap);
            }
        }   
    }
}