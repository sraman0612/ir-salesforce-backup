trigger CTS_CSITrigger on Customer_Satisfaction_Results__c (before insert,after insert,before update,after update,before delete,after delete,after undelete) {
    TriggerDispatcher.run(new CTS_CSITriggerHandler(), 'CTS_CSITrigger');
}