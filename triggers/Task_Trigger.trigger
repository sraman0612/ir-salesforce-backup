// (c) 2015 Appirio, Inc.
//
// Trigger on Task object
//
// Apr 05, 2015      Surabhi Sharma        Original: T-375998
// Apr 14, 2016      Dhilip C, Sujan S     Updated
// Feb 27, 2019      Ben Lorenz            Refactored to include all trigger events, folded in PT handler
// Apr 21 2020       Harish KJ             Calling class Task_BillTo_ShipTOInactiveCheck

trigger Task_Trigger on Task (before insert, before update, before delete,after insert, after update, after delete, after undelete) {
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Task');
    
    if(skipTrigger!=true){ 
        if(Task_TriggerRunOnce.isFirstTime){
            map<Id, Profile> idByProfile = new Map<Id, Profile> ([SELECT Id, Name FROM Profile where name IN ('PT Inside Sales', 'PT Marketing User', 'PT Service User', 'PT Solution Center User', 'PT Standard User', 'PT System Administrator')]);
            Task_TriggerRunOnce.isFirstTime = false;
            Task_TriggerRunOnce.hasAccess = idByProfile.containsKey(UserInfo.getProfileId());
        }
        
		system.debug('Task_TriggerRunOnce.hasAccess ' + Task_TriggerRunOnce.hasAccess);
        PT_DSMP_TaskTriggerHandler handler = new PT_DSMP_TaskTriggerHandler();
        
        List<Task> TaskList;
        Map<id, Task> oldTaskMap, newTaskMap;
        
        if (Trigger.isBefore){
            if (Trigger.isInsert) {  //BEFORE INSERT
                Id AirNATaskRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'NA_Air_Task' and SobjectType='Task' limit 1].Id;
                List<Task> airNATaskNew = new List<Task>();
                if(null!=trigger.new){for (task a : trigger.new){if (a.RecordTypeId==AirNATaskRT_ID){airNATaskNew.add(a);}}}
                if (!airNATaskNew.isEmpty()){Task_TriggerHandler.beforeInsert(airNATaskNew);}
                // Task_BillTo_ShipTOInactiveCheck.BillTo_ShipTOInactiveCheck(Trigger.New);
            } else if (Trigger.isUpdate) {  //BEFORE UPDATE
            } else if (Trigger.isDelete) {  // BEFORE DELETE
            }
        } else {
            if (Trigger.isInsert) {  // AFTER INSERT
                if(Task_TriggerRunOnce.hasAccess){
                    handler.handleAfterInsert(Trigger.new);
                    newTaskMap = trigger.newMap;
                }
                
                //Task_BillTo_ShipTOInactiveCheck.BillTo_ShipTOInactiveCheck(trigger.new);
            } else if (Trigger.isUpdate) {  // AFTER UPDATE
                if(Task_TriggerRunOnce.hasAccess){
                    handler.handleAfterUpdate(Trigger.new, Trigger.old);
                    newTaskMap = trigger.newMap;
                    oldTaskMap = trigger.oldMap;
                }
            } else if (Trigger.isDelete) {  // AFTER DELETE
                if(Task_TriggerRunOnce.hasAccess){
                    handler.handleAfterDelete(Trigger.old);
                    oldTaskMap = trigger.oldMap;
                }
            } else if (Trigger.isUndelete) {  // AFTER UNDELETE
            }
            
            TaskList = trigger.isDelete ? trigger.old : trigger.new ;
            if(Task_TriggerRunOnce.hasAccess){
            	TaskMethods.UpdateFaceToFaceTask(TaskList, oldTaskMap, newTaskMap);
            }
        }
    }
}