trigger Notification_Message_Trigger on Notification_Message__c (after insert) {

    Notification_Message_TriggerHandler handler = new Notification_Message_TriggerHandler(Trigger.oldMap, Trigger.newMap);

    if (Trigger.isAfter){

        if (Trigger.isInsert){
            handler.onAfterInsert();
        }
    }
}