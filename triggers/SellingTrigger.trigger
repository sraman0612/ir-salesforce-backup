trigger SellingTrigger on Selling__c (before delete) {
    
    for(Selling__c sell : trigger.old){
        if(!test.isRunningTest()) sell.adderror('This record can\'t be deleted ');
        else system.debug('no delete');
    }

}