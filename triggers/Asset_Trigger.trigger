//
// (c) 2015, Appirio Inc.
//
// Asset_Trigger
//
// August27,2015    Surabhi Sharma    T-429707
// Dec 11th 2015 - Dhilip Updated
// Aug 18th 2017 - Dhilip - extended the events to After Update, After Insert, After Delete as part of case 00007751
// Jun 5th 2018 - Ashwini Updated - EMEIA RecordType appended
trigger Asset_Trigger on Asset (before Insert, after Insert, after Update, after delete) {
    
    Boolean skipTrigger=CGCommonUtility.sKipTrigger('Asset');  
     //Merged Triggers as per SGOM 144.
     if(skipTrigger!=True){                    
    switch on Trigger.operationType {
                        When BEFORE_INSERT{  
                                EMEIA_AssetTriggerHandler.beforeInsert(trigger.new,trigger.old);                                                                                                                         
                        }
                        When AFTER_INSERT{                                              
                                EMEIA_AssetTriggerHandler.afterInsert(trigger.new,trigger.old);                                                                                                                         
                        }
                        When AFTER_UPDATE{                                              
                                EMEIA_AssetTriggerHandler.afterUpdate(trigger.new,trigger.old);
                        }
                        When AFTER_DELETE{                                              
                                EMEIA_AssetTriggerHandler.afterDelete(trigger.new,trigger.old);                                
                        }
                }
     }
}