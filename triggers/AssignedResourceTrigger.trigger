/**
 * Created by CloudShapers on 11/19/2020.
 */

trigger AssignedResourceTrigger on AssignedResource (after insert, after update) {
    if (Trigger.isInsert)
        AssignedResourceTriggerHandler.createProductItems(Trigger.new, null);

    if (Trigger.isUpdate)
        AssignedResourceTriggerHandler.createProductItems(Trigger.new, Trigger.oldMap);
}