// 
// (c) 2015 Appirio, Inc.
//
// April 17,2015    Surabhi Sharma    T-377640(delete the record)
// T-376328 : Trigger on Quote object to Update Fields when new Quote is created On Opportunity 
//


trigger Quote_Trigger on Quote__c (before insert, after insert, before update, after update, after delete) {
    Utility_Trigger_SoftDisable softDisable = new Utility_Trigger_SoftDisable('Quote__c');
    if (trigger.isBefore)
    {
        if (trigger.isInsert && !softDisable.insertDisabled())
            Quote_TriggerHandler.beforeInsert(trigger.new);
        else if (trigger.isUpdate && !softDisable.updateDisabled())
            Quote_TriggerHandler.beforeUpdate(trigger.new, trigger.oldMap);
    }
    if (trigger.isAfter)
    {
        if (trigger.isInsert && !softDisable.insertDisabled())
            Quote_TriggerHandler.afterInsert(trigger.new);
        else if (trigger.isUpdate && !softDisable.updateDisabled())
            Quote_TriggerHandler.afterUpdate(trigger.new, trigger.oldMap);    //record deleted
        else if (trigger.isDelete && !softDisable.deleteDisabled())
            Quote_TriggerHandler.afterDelete(trigger.old);
    }
}