/*
Added Recordtype for PT_powertools

Mar 12, 2018: CloudGofer, Added code to update Total Open Amount on Account based on Opportunity Input (Parent Opportunity)
*/
trigger PT_ParentOpportunity_Trigger on PT_Parent_Opportunity__c (before insert, before update, after update) {
    
    switch on Trigger.operationType {
        
        when BEFORE_INSERT {
            PT_ParentOpportunityTriggerHandler.setLinkedInCampaign(Trigger.New);
        }
        when AFTER_INSERT {}
        when BEFORE_UPDATE {}
        when AFTER_UPDATE {
            Set<id> parentOpptyIds = new Set<id>();
            Set<Id> accountIds = new Set<Id>();
            Map<Id,Decimal> accountIdOpenAmountMap = new Map<Id,Decimal>(); // added for summing up Parent opportunity amount to Account
            
            for (PT_Parent_Opportunity__c parentOppty : Trigger.New) {
                //if(Trigger.oldMap.get(parentOppty.id).Total_Open_Amount__c != parentOppty.Total_Open_Amount__c) {            
                parentOpptyIds.add(parentOppty.id);
                accountIds.add(parentOppty.Account__c);
                //}
            }    
            
            for (PT_Parent_Opportunity__c paOpp : [select id, Account__c, Total_Open_Amount__c, Total_Amount__c from PT_Parent_Opportunity__c where Account__c in :accountIds]){
                
                if (paOpp.Total_Open_Amount__c == null) paOpp.Total_Open_Amount__c = 0.0;
                Decimal parentOpptyOpenAmount = accountIdOpenAmountMap.get(paOpp.Account__c);
                if (parentOpptyOpenAmount == null) {
                    accountIdOpenAmountMap.put(paOpp.Account__c,paOpp.Total_Open_Amount__c);
                } else {
                    accountIdOpenAmountMap.put(paOpp.Account__c,paOpp.Total_Open_Amount__c + parentOpptyOpenAmount);
                }
                
            }
            List<Account> accountsForUpdate = new List<Account>();
            for (Id accountid : accountIdOpenAmountMap.keyset()) {
                accountsForUpdate.add(new Account(id=accountid, PT_Total_Open_Amount__c=accountIdOpenAmountMap.get(accountid)));
            } 
            
            if (!accountsForUpdate.isEmpty()) {
                update accountsForUpdate;
            }     
            
        }
        when AFTER_DELETE {}
        when else {}
    }
    
    
}