trigger PT_AccountPlanInputEvents on PT_DSMP__c (before insert, before update, before delete,after insert, after update, after delete, after undelete) {
  if (Trigger.isBefore){
    if (Trigger.isInsert) {  //BEFORE INSERT
    } else if (Trigger.isUpdate) {  //BEFORE UPDATE 
    } else if (Trigger.isDelete) {  // BEFORE DELETE
    }
  } else {
    if (Trigger.isInsert) {  // AFTER INSERT
    } else if (Trigger.isUpdate) {  // AFTER UPDATE
      PT_AccountPlanInputEventHandler.updateAccount(trigger.new,trigger.oldMap);
    } else if (Trigger.isDelete) {  // AFTER DELETE
    } else if (Trigger.isUndelete) {  // AFTER UNDELETE
    }
  }
}