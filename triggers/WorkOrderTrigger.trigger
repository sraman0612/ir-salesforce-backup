/**
 * Created by CloudShapers on 12/11/2020.
 */

trigger WorkOrderTrigger on WorkOrder (after insert) {
    if (Trigger.isInsert)
        CTS_WorkOrderTriggerHandler.associateEntitlements(Trigger.new);

}