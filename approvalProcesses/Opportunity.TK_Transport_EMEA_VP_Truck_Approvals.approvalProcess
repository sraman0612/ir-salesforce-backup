<?xml version="1.0" encoding="UTF-8"?>
<ApprovalProcess xmlns="http://soap.sforce.com/2006/04/metadata">
    <active>false</active>
    <allowRecall>true</allowRecall>
    <allowedSubmitters>
        <type>owner</type>
    </allowedSubmitters>
    <approvalPageFields>
        <field>Account</field>
        <field>Name</field>
        <field>Owner</field>
        <field>TK_ProductFamily__c</field>
        <field>TKM_Model__c</field>
        <field>TK_TotalSellingPrice__c</field>
        <field>TK_DeviationAmount__c</field>
        <field>TK_DeviationPercent__c</field>
    </approvalPageFields>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Truck Product Family, the opportunity is routed to the user&apos;s manager if the Deal value is greater than 1M or 300 units</description>
        <entryCriteria>
            <booleanFilter>1 OR 2</booleanFilter>
            <criteriaItems>
                <field>Opportunity.TK_Highest_Country_Avg_Deviation__c</field>
                <operation>greaterThan</operation>
                <value>-2</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.TK_Largest_PDR_Amount__c</field>
                <operation>greaterThan</operation>
                <value>&quot;USD 1,000,000&quot;</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>ApproveRecord</ifCriteriaNotMet>
        <label>TK Transport -  1M/300</label>
        <name>TK_Transport_1M_300</name>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Truck Product Family, the opportunity is routed to the user&apos;s manager if the Total Value is &gt;2M or 300 units</description>
        <entryCriteria>
            <booleanFilter>1 OR 2</booleanFilter>
            <criteriaItems>
                <field>Opportunity.TK_Highest_Country_Avg_Deviation__c</field>
                <operation>greaterThan</operation>
                <value>1</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.TK_Largest_PDR_Amount__c</field>
                <operation>greaterThan</operation>
                <value>&quot;USD 2,000,000&quot;</value>
            </criteriaItems>
        </entryCriteria>
        <ifCriteriaNotMet>ApproveRecord</ifCriteriaNotMet>
        <label>TK Transport - 2M/300</label>
        <name>TK_Transport_2M_300</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <approvalStep>
        <allowDelegate>true</allowDelegate>
        <assignedApprover>
            <approver>
                <type>userHierarchyField</type>
            </approver>
        </assignedApprover>
        <description>For the Truck Product Family, the opportunity is routed to the user&apos;s manager if the Total Value is &gt; 4M</description>
        <entryCriteria>
            <booleanFilter>1 OR 2</booleanFilter>
            <criteriaItems>
                <field>Opportunity.TK_Largest_PDR_Amount__c</field>
                <operation>greaterThan</operation>
                <value>&quot;USD 4,000,000&quot;</value>
            </criteriaItems>
            <criteriaItems>
                <field>Opportunity.TK_Highest_Country_Avg_Deviation__c</field>
                <operation>greaterThan</operation>
                <value>15</value>
            </criteriaItems>
        </entryCriteria>
        <label>TK Transport - 4M</label>
        <name>TK_Transport_4M</name>
        <rejectBehavior>
            <type>RejectRequest</type>
        </rejectBehavior>
    </approvalStep>
    <description>Base approval process for the TK Transport business group, Large Truck and Heater Product Families, using opportunity record type and Deviation % as criteria</description>
    <emailTemplate>unfiled$public/TK_Transport_Opportunity_Approval</emailTemplate>
    <enableMobileDeviceAccess>false</enableMobileDeviceAccess>
    <entryCriteria>
        <criteriaItems>
            <field>Opportunity.RecordType</field>
            <operation>equals</operation>
            <value>TK Transport EMEA</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Propose,Propose/Quote</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.TK_TotalSellingPrice__c</field>
            <operation>notEqual</operation>
            <value>USD 0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.TK_ProductFamily__c</field>
            <operation>equals</operation>
            <value>VP Truck</value>
        </criteriaItems>
    </entryCriteria>
    <finalApprovalRecordLock>true</finalApprovalRecordLock>
    <finalRejectionRecordLock>false</finalRejectionRecordLock>
    <label>TK Transport EMEA VP_Truck Approvals</label>
    <nextAutomatedApprover>
        <useApproverFieldOfRecordOwner>false</useApproverFieldOfRecordOwner>
        <userHierarchyField>Manager</userHierarchyField>
    </nextAutomatedApprover>
    <processOrder>1</processOrder>
    <recordEditability>AdminOnly</recordEditability>
    <showApprovalHistory>true</showApprovalHistory>
</ApprovalProcess>
