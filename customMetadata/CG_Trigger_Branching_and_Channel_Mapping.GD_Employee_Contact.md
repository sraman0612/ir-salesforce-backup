<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GD Employee_Contact</label>
    <protected>false</protected>
    <values>
        <field>BRNO__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:type="xsd:string">BRNO</value>
    </values>
    <values>
        <field>EMEIA__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
    <values>
        <field>Record_Type_Name__c</field>
        <value xsi:type="xsd:string">GD_Employee</value>
    </values>
</CustomMetadata>
