<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Italy</label>
    <protected>false</protected>
    <values>
        <field>Country_de__c</field>
        <value xsi:type="xsd:string">Italien</value>
    </values>
    <values>
        <field>Country_en__c</field>
        <value xsi:type="xsd:string">Italy</value>
    </values>
    <values>
        <field>Country_fr__c</field>
        <value xsi:type="xsd:string">Italie</value>
    </values>
    <values>
        <field>Country_it__c</field>
        <value xsi:type="xsd:string">Italia</value>
    </values>
    <values>
        <field>VAT_Pattern_Example__c</field>
        <value xsi:type="xsd:string">IT12345678901</value>
    </values>
    <values>
        <field>VAT_Regex_Pattern__c</field>
        <value xsi:type="xsd:string">^IT\d{11}</value>
    </values>
</CustomMetadata>
