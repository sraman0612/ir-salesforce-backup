<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bhawna Chakraborty</label>
    <protected>false</protected>
    <values>
        <field>Shipping_Country__c</field>
        <value xsi:type="xsd:string">INDIA</value>
    </values>
    <values>
        <field>User_Alias__c</field>
        <value xsi:type="xsd:string">BCha</value>
    </values>
    <values>
        <field>User_Email__c</field>
        <value xsi:type="xsd:string">bhawna.chakraborty@irco.com</value>
    </values>
</CustomMetadata>
