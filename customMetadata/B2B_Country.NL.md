<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Netherlands</label>
    <protected>false</protected>
    <values>
        <field>Country_de__c</field>
        <value xsi:type="xsd:string">Niederlande</value>
    </values>
    <values>
        <field>Country_en__c</field>
        <value xsi:type="xsd:string">The Netherlands</value>
    </values>
    <values>
        <field>Country_fr__c</field>
        <value xsi:type="xsd:string">Pays-Bas</value>
    </values>
    <values>
        <field>Country_it__c</field>
        <value xsi:type="xsd:string">Paesi Bassi</value>
    </values>
    <values>
        <field>VAT_Pattern_Example__c</field>
        <value xsi:type="xsd:string">NL111234567B01</value>
    </values>
    <values>
        <field>VAT_Regex_Pattern__c</field>
        <value xsi:type="xsd:string">^NL\d{9}B\d{2}</value>
    </values>
</CustomMetadata>
