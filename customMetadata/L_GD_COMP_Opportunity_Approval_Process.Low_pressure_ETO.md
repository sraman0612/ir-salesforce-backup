<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Low pressure ETO</label>
    <protected>false</protected>
    <values>
        <field>Business__c</field>
        <value xsi:type="xsd:string">Low Pressure</value>
    </values>
    <values>
        <field>Channel__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Level_1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Level_2__c</field>
        <value xsi:type="xsd:string">enrico.vigna@irco.com</value>
    </values>
    <values>
        <field>Level_3_1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Level_3__c</field>
        <value xsi:type="xsd:string">stefano.santelli@irco.com</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">ETO</value>
    </values>
    <values>
        <field>Product__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Region__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Sales_Enablement__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
