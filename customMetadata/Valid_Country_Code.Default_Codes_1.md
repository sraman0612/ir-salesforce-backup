<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Codes 1</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AG;AR;AS;AU;AW;AX;BB;BI;BN;BO;BS;BT;BZ;CA;CD;CF;CL;CN;CO;CR;CV;DM;DO;EC;FJ;FK;FM;GD;GF;GM;GN;GQ;GT;GU;GW;GY;HK;HN;HT;ID;JM;JP;KG;KH;KI;KM;KN;KP;KR;KY;LA;LC;LS;MH;MN;MO;MQ;MR;MV;MX;MY;NC;NE;NI;NP;NZ;PA;PE;PF;PG;PH;PR;PS;PW;PY;RE;SB;SG;SR;SV;SX;TC;TH;TJ;TL</value>
    </values>
</CustomMetadata>
