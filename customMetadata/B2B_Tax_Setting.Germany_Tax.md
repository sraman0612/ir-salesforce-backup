<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Germany Tax</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">DE</value>
    </values>
    <values>
        <field>Tax_Percentage__c</field>
        <value xsi:type="xsd:double">19.0</value>
    </values>
</CustomMetadata>
