<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>iPad_Mobile_Software_Tools</label>
    <protected>false</protected>
    <values>
        <field>FilterCategory__c</field>
        <value xsi:type="xsd:string">iPad_Mobile_Software_Tools</value>
    </values>
</CustomMetadata>
