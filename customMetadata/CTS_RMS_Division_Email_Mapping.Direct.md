<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Direct</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Direct</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">suresh.raman@irco.com</value>
    </values>
</CustomMetadata>
