<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Northeast Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Boston Customer Center,Edison Customer Center,London Customer Center,Northeast Area,Philadelphia Customer Center,Portland Customer Center,Syracuse Customer Center,Toronto Customer Center,Upstate New York District</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">nicole.willis@irco.com,renee.pineaultnowak@irco.com,scott_keevan@irco.com,karyn_bitterman@irco.com,ron_buttros@irco.com,darren.zelinski@irco.com,brad.somers@irco.com,karyn_schreefer@irco.com,Destinee.Inverso@irco.com,thom.pickett@irco.com,dan.larou@irco.com,bryan.josefchuk@irco.com,tracy_mackey@irco.com,marco.dilauro@irco.com,nino.desantis@irco.com,paul_b_smith@irco.com,wjones1@irco.com,christian.banes@irco.com,john_shields@irco.com,chad.snopkowski@irco.com</value>
    </values>
</CustomMetadata>
