<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Roots</label>
    <protected>false</protected>
    <values>
        <field>Manufacturer__c</field>
        <value xsi:type="xsd:string">RO</value>
    </values>
    <values>
        <field>Mfr_Full_Name__c</field>
        <value xsi:type="xsd:string">Roots</value>
    </values>
</CustomMetadata>
