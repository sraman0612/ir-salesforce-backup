<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Gulnara Kuvatova +49 152 28566994</label>
    <protected>false</protected>
    <values>
        <field>Contact_Email__c</field>
        <value xsi:type="xsd:string">gulnara_kuvatova@trane.com</value>
    </values>
</CustomMetadata>
