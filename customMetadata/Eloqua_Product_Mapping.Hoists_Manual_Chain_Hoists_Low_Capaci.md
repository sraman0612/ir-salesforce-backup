<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Hoists (Manual Chain Hoists &amp; Low Capaci</label>
    <protected>false</protected>
    <values>
        <field>Eloqua_Product__c</field>
        <value xsi:type="xsd:string">Hoists (Manual Chain Hoists &amp; Low Capacity)</value>
    </values>
    <values>
        <field>Salesforce_Product__c</field>
        <value xsi:type="xsd:string">Lifting / Material Handling - Hoists (Manual Chain Hoists &amp; Low Capacity Air Chain Hoist)</value>
    </values>
</CustomMetadata>
