<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>North Central Area</label>
    <protected>false</protected>
    <values>
        <field>Division__c</field>
        <value xsi:type="xsd:string">Chicago Customer Center,Cincinnati Customer Center,Detroit Customer Center,Goshen Customer Center,Indianapolis Customer Center,Michigan District,Milwaukee Customer Center,North Central Area</value>
    </values>
    <values>
        <field>Emails__c</field>
        <value xsi:type="xsd:string">Robert.Petak@irco.com,Jeffrey.Oliver@irco.com,kris_hubbard@irco.com,lisa.boulton@irco.com,william.santie@irco.com,Bill.Hartung@irco.com,sblair@irco.com,Eric.Satterlee@irco.com,billy_fulgham@irco.com,walter.elliott@irco.com,matt.long@irco.com,marlo.kintzele@irco.com,matt.miller@irco.com,melanie.covert@irco.com,shaun.cash@irco.com,kathy.christenson@irco.com,tiffany.ryle@irco.com,ron_kerr@irco.com,daniel_hastings@irco.com,samuel.shuberg@irco.com,timothy.huntington@irco.com,douglas_nader@irco.com,eric.grimmeissen@irco.com</value>
    </values>
</CustomMetadata>
