<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Hypercare_EMEIA_English</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">ES;PT;KZ;UZ;AE;AF;BD;BH;EG;IQ;IR;JO;KW;LB;LY;OM;PK;QA;SA;SY;TR;YE;AM;AZ;BY;GE;KG;MD;RU;TM;AL;BA;BG;DK;EE;FI;GB;HR;IE;IS;LT;LV;ME;MK;NO;RO;RS;SE;UA;XK;TJ;IL;IN;LK;MV;BE;CY;GR;LU;MT;NL</value>
    </values>
</CustomMetadata>
