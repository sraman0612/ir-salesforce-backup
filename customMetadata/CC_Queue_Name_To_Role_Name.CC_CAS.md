<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CC CAS</label>
    <protected>false</protected>
    <values>
        <field>Queue_ID__c</field>
        <value xsi:type="xsd:string">00G0a000000c3YKEAY</value>
    </values>
    <values>
        <field>Queue_Name__c</field>
        <value xsi:type="xsd:string">CC CAS</value>
    </values>
    <values>
        <field>Role_Name__c</field>
        <value xsi:type="xsd:string">Customer Account Specialist</value>
    </values>
</CustomMetadata>
