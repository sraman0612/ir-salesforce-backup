<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>contentversion bullnumber</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Bulletin_Number__c</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">ContentVersion</value>
    </values>
</CustomMetadata>
