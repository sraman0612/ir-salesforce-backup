<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EMCO NON - EMEIA</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">CN,TW,HK,MO,MN,KZ,JP,VN,TH,ID,PH,SG,KH,MM,FM,PF,MY,AU,NZ,CA,US,KP,KR,US,CA</value>
    </values>
</CustomMetadata>
