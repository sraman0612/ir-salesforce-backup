<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>US_55720</label>
    <protected>false</protected>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">Carlton</value>
    </values>
</CustomMetadata>
