<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>France</label>
    <protected>false</protected>
    <values>
        <field>Country_de__c</field>
        <value xsi:type="xsd:string">Frankreich</value>
    </values>
    <values>
        <field>Country_en__c</field>
        <value xsi:type="xsd:string">France</value>
    </values>
    <values>
        <field>Country_fr__c</field>
        <value xsi:type="xsd:string">France</value>
    </values>
    <values>
        <field>Country_it__c</field>
        <value xsi:type="xsd:string">Francia</value>
    </values>
    <values>
        <field>VAT_Pattern_Example__c</field>
        <value xsi:type="xsd:string">FRAB123456789</value>
    </values>
    <values>
        <field>VAT_Regex_Pattern__c</field>
        <value xsi:type="xsd:string">^FR[A-HJ-NP-Z0-9]{2}\d{9}</value>
    </values>
</CustomMetadata>
