<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Default_Lead_Record_Type_Name__c</field>
        <value xsi:type="xsd:string">PT_Not_My_Lead</value>
    </values>
    <values>
        <field>Default_LinkedIn_Campaign_Name__c</field>
        <value xsi:type="xsd:string">LinkedIn Sales Navigator</value>
    </values>
    <values>
        <field>PT_Profile_Names__c</field>
        <value xsi:type="xsd:string">PT Marketing User,PT Service User,PT Solution Center User,PT Standard User,PT System Administrator</value>
    </values>
</CustomMetadata>
