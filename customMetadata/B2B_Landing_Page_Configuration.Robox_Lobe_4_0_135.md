<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Robox Lobe 4.0 #135</label>
    <protected>false</protected>
    <values>
        <field>Label_en__c</field>
        <value xsi:type="xsd:string">Robox Lobe 4.0 #135</value>
    </values>
    <values>
        <field>Label_fr__c</field>
        <value xsi:type="xsd:string">Robox Lobe 4.0 #135</value>
    </values>
    <values>
        <field>Label_it__c</field>
        <value xsi:type="xsd:string">Robox Lobo 4.0 #135</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Value__c</field>
        <value xsi:type="xsd:string">Robox Lobe 4.0 #135</value>
    </values>
</CustomMetadata>
