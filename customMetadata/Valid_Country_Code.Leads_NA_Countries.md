<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Leads NA Countries</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AG,AR,BS,BB,BZ,BO,BR,CA,CL,CO,CR,CU,DM,DO,EC,SV,GD,GT,GY,HT,HN,JM,MX,NI,PA,PY,PE,KN,LC,VC,SR,TT,US,UY,VE,PR,AI,AW,BL,BM,BQ,FK,GL,KY,MS,SX,TC,VG,VI,AS</value>
    </values>
</CustomMetadata>
