<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>US_65747</label>
    <protected>false</protected>
    <values>
        <field>County__c</field>
        <value xsi:type="xsd:string">Barry</value>
    </values>
</CustomMetadata>
