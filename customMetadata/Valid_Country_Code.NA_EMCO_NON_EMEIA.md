<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NA EMCO NON - EMEIA</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AG,AR,BB,BO,BR,BS,CL,CO,CR,EC,GT,HN,MX,NI,PA,PE,PY,SR,SV,TT,UY,PR,AI,AW,BL,BM,BQ,BZ,CU,DM,DO,FK,GD,GL,GY,HT,JM,KN,KY,LC,MS,SX,TC,VC,VE,VG,VI,AS,GF,NL,SH,BV,US,CA</value>
    </values>
</CustomMetadata>
