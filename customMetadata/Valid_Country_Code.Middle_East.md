<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Middle East</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">AF;BH;BU;EG;IQ;JO;KW;LB;OM;PK;QA;SA;AE;YE;LY;BR;CY;IA;IS;KU;LE;SD;SY;UA;YM</value>
    </values>
</CustomMetadata>
