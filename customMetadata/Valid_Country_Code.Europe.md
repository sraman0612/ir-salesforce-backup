<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Europe</label>
    <protected>false</protected>
    <values>
        <field>Country_Code__c</field>
        <value xsi:type="xsd:string">GP;AL;DZ;AM;AT;AZ;BE;BJ;BA;BG;BF;CM;CG;HR;CY;CZ;DK;DJ;ER;EE;FI;GA;GE;DE;GH;GR;HU;IS;IE;IL;IT;CI;KZ;XK;LV;LR;LT;LU;MK;ML;MT;MD;XM;MA;NL;NG;NO;PL;PT;RO;RU;SN;XS;SL;SK;SI;ES;SE;CH;TG;TN;TR;UA;GB;UZ;FR;SM;RS;AD;BY;FO;GI;IM;JE;LI;MC;ME;SJ;VA
 </value>
    </values>
</CustomMetadata>
