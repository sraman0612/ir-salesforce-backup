<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>NG_Pump_Natural_Gas</label>
    <protected>false</protected>
    <values>
        <field>FilterCategory__c</field>
        <value xsi:type="xsd:string">NG_Pump_Natural_Gas</value>
    </values>
</CustomMetadata>
