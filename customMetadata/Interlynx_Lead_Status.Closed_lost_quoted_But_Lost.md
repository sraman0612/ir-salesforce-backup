<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Closed-lost (quoted But Lost)</label>
    <protected>false</protected>
    <values>
        <field>Disposition_Reason__c</field>
        <value xsi:type="xsd:string">Competitor Won</value>
    </values>
    <values>
        <field>Interlynx_Lead_Status__c</field>
        <value xsi:type="xsd:string">Closed-lost (quoted But Lost)</value>
    </values>
    <values>
        <field>Salesforce_Lead_Status__c</field>
        <value xsi:type="xsd:string">Closed - Not Converted</value>
    </values>
</CustomMetadata>
