<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Email Message Setting For Case</label>
    <protected>false</protected>
    <values>
        <field>CTS_Default_Case_Owner_ID__c</field>
        <value xsi:type="xsd:string">00G4Q000004nQnhUAE</value>
    </values>
    <values>
        <field>CTS_OM_Americas_Default_Case_Owner_Id__c</field>
        <value xsi:type="xsd:string">00553000000rdL3AAI</value>
    </values>
    <values>
        <field>CTS_OM_Americas_ZEKS_Validation_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CTS_OM_Case_Contact_Trigger_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CTS_OM_Contact_Record_Type_IDs_to_Match__c</field>
        <value xsi:type="xsd:string">CTS_Global_Distributor_Contact,GES_Contact,Customer_Contact,End_Customer,CTS_EU_Contact,Hibon_Contact,GES_Contact,CTS_EU_Indirect_Contact,CTS_MEIA_Contact,CTS_MEIA_Contact,CTS_MEIA_Indirect_Contact,LFS_Contact,Milton_Roy_Contact</value>
    </values>
    <values>
        <field>CTS_OM_Email_Case_Record_Type_IDs_1__c</field>
        <value xsi:type="xsd:string">CTS_Non_Warranty_Claims,Internal_Case,Customer_Care,Action_Item</value>
    </values>
    <values>
        <field>CTS_OM_Email_Case_Record_Type_IDs_2__c</field>
        <value xsi:type="xsd:string">CTS_TechDirect_Issue_Escalation,CTS_TechDirect_Ask_a_Question,CTS_TechDirect_Start_Up,Champion_TechKnowledgy_Case</value>
    </values>
    <values>
        <field>CTS_OM_Email_Case_Record_Type_IDs_3__c</field>
        <value xsi:type="xsd:string">CTS_Tech_Direct_Site_Feedback</value>
    </values>
    <values>
        <field>CTS_OM_Email_Case_Record_Type_IDs_4__c</field>
        <value xsi:type="xsd:string">SFA_Enhancement_tracking,CTS_Tech_Direct_Case_Feedback,TechDirect_Feedback,CTS_Tech_Direct_Article_Feedback</value>
    </values>
    <values>
        <field>CTS_OM_Email_Case_Record_Type_IDs__c</field>
        <value xsi:type="xsd:string">CTS_Non_Warranty_Claims,Internal_Case,Action_Item,CTS_RMS,CTS_TechDirect_Issue_Escalation,CTS_TechDirect_Ask_a_Question,CTS_TechDirect_Start_Up,GD_TechKnowledgy_Case,Champion_TechKnowledgy_Case,CTS_Tech_Direct_Site_Feedback,SFA_Enhancement_tracking,CTS_Tech_Direct_Case_Feedback,TechDirect_Feedback,CTS_Tech_Direct_Article_Feedback,CTS_TechDirect_AIRPS_Case,CTS_OM_Account_Management,Customer_Service,CompAir_TechKnowledgy_Case,ETO_Support_Case,Milton_Roy_Case_Management_Application,Milton_Roy_Case_Management_Customer,Milton_Roy_Case_Management_YZ</value>
    </values>
    <values>
        <field>CTS_OM_Email_Trigger_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CTS_OM_NWC_Default_Case_Owner_Id__c</field>
        <value xsi:type="xsd:string">00553000000rdKuAAI</value>
    </values>
    <values>
        <field>CTS_OM_ZEKS_Default_Case_Owner_Id__c</field>
        <value xsi:type="xsd:string">00553000000rdKvAAI</value>
    </values>
    <values>
        <field>Email_Message_Trigger_Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
