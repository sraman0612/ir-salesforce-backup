<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Germany</label>
    <protected>false</protected>
    <values>
        <field>Country_de__c</field>
        <value xsi:type="xsd:string">Deutschland</value>
    </values>
    <values>
        <field>Country_en__c</field>
        <value xsi:type="xsd:string">Germany</value>
    </values>
    <values>
        <field>Country_fr__c</field>
        <value xsi:type="xsd:string">Allemagne</value>
    </values>
    <values>
        <field>Country_it__c</field>
        <value xsi:type="xsd:string">Germania</value>
    </values>
    <values>
        <field>VAT_Pattern_Example__c</field>
        <value xsi:type="xsd:string">DE123456789</value>
    </values>
    <values>
        <field>VAT_Regex_Pattern__c</field>
        <value xsi:type="xsd:string">^DE\d{9}$</value>
    </values>
</CustomMetadata>
