<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Verbano-Cusio-Ossola</label>
    <protected>false</protected>
    <values>
        <field>Country__c</field>
        <value xsi:type="xsd:string">IT</value>
    </values>
    <values>
        <field>State_en__c</field>
        <value xsi:type="xsd:string">Verbano-Cusio-Ossola</value>
    </values>
    <values>
        <field>State_fr__c</field>
        <value xsi:type="xsd:string">Verbano-Cusio-Ossola</value>
    </values>
    <values>
        <field>State_it__c</field>
        <value xsi:type="xsd:string">Verbano-Cusio-Ossola</value>
    </values>
</CustomMetadata>
