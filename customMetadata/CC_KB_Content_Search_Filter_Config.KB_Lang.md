<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>KB Lang</label>
    <protected>false</protected>
    <values>
        <field>Data_Type__c</field>
        <value xsi:type="xsd:string">String</value>
    </values>
    <values>
        <field>Filter_Customer_Profile_Field__c</field>
        <value xsi:type="xsd:string">LanguageLocaleKey</value>
    </values>
    <values>
        <field>Filter_Type__c</field>
        <value xsi:type="xsd:string">Auto</value>
    </values>
    <values>
        <field>Filter_Values__c</field>
        <value xsi:type="xsd:string">en_US</value>
    </values>
    <values>
        <field>Matching_Field__c</field>
        <value xsi:type="xsd:string">Language</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">Knowledge__kav</value>
    </values>
    <values>
        <field>Operator__c</field>
        <value xsi:type="xsd:string">Equals</value>
    </values>
</CustomMetadata>
