<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Standard Monthly Billing</label>
    <protected>false</protected>
    <values>
        <field>Column_Name__c</field>
        <value xsi:type="xsd:string">Standard_Monthly_Billing__c</value>
    </values>
    <values>
        <field>Column_Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">CC_Short_Term_Lease__c</value>
    </values>
    <values>
        <field>Sortable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Type_Attributes__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">currency</value>
    </values>
</CustomMetadata>
