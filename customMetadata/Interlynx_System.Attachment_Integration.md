<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Attachment Integration</label>
    <protected>false</protected>
    <values>
        <field>Bearer_Token__c</field>
        <value xsi:type="xsd:string">ba5ff5-99f7ff05d2-c5cfbeffdfdf25-86h778-be0cffe74c-4fdff2</value>
    </values>
    <values>
        <field>Interlynx_Endpoint__c</field>
        <value xsi:type="xsd:string">https://leads.irleads.com/srv/salesforce/leads/attachment</value>
    </values>
</CustomMetadata>
