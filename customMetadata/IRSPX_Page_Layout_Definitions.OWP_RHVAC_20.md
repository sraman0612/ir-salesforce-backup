<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OWP RHVAC 20</label>
    <protected>false</protected>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">Canwesellfollowonproductsservices__c</value>
    </values>
    <values>
        <field>Object_Name__c</field>
        <value xsi:type="xsd:string">OpportunityWinPlan__c</value>
    </values>
    <values>
        <field>Page_Layout_Name__c</field>
        <value xsi:type="xsd:string">Opportunity Win Plan Layout</value>
    </values>
    <values>
        <field>Record_Type_Name__c</field>
        <value xsi:type="xsd:string">RS_Opportunity_Win_Plan</value>
    </values>
    <values>
        <field>Rollup_Summary_Field__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
