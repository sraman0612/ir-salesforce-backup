<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>IR Comp OM Account Management</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Brand__c</field>
        <value xsi:type="xsd:string">IR</value>
    </values>
    <values>
        <field>Business_Hours__c</field>
        <value xsi:type="xsd:string">IR Comp Business Hours - EU + Africa</value>
    </values>
    <values>
        <field>Business__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Department__c</field>
        <value xsi:type="xsd:string">CTS OM EU (Italy)</value>
    </values>
    <values>
        <field>Email_DC__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Form_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Group_Queue__c</field>
        <value xsi:type="xsd:string">Emanuela Lanzani</value>
    </values>
    <values>
        <field>Legacy_Org__c</field>
        <value xsi:type="xsd:string">LIR</value>
    </values>
    <values>
        <field>NWC__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Org_Wide_Email_Address__c</field>
        <value xsi:type="xsd:string">airom.el@irco.com</value>
    </values>
    <values>
        <field>Product_Category__c</field>
        <value xsi:type="xsd:string">CENTRIFUGAL AFTERMARKET</value>
    </values>
    <values>
        <field>Request_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Address_Text__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Address__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
