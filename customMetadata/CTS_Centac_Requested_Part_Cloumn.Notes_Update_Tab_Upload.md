<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Notes</label>
    <protected>false</protected>
    <values>
        <field>API_Name__c</field>
        <value xsi:type="xsd:string">Notes__c</value>
    </values>
    <values>
        <field>CTS_Sequence_Number__c</field>
        <value xsi:type="xsd:double">11.0</value>
    </values>
    <values>
        <field>Field_Label__c</field>
        <value xsi:type="xsd:string">Notes</value>
    </values>
    <values>
        <field>Tab_Option__c</field>
        <value xsi:type="xsd:string">Upload Parts Tab</value>
    </values>
    <values>
        <field>fieldType__c</field>
        <value xsi:type="xsd:string">TEXTAREA</value>
    </values>
    <values>
        <field>isMandatory__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
