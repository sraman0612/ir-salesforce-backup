<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Shipping Vals</label>
    <protected>false</protected>
    <values>
        <field>LTL_Flat_Rate__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>LTL_Service_Name__c</field>
        <value xsi:type="xsd:string">FedEx® LTL</value>
    </values>
    <values>
        <field>Standard_Rate_Perc_Increase__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
</CustomMetadata>
