<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MAN-GHH</label>
    <protected>false</protected>
    <values>
        <field>Manufacturer__c</field>
        <value xsi:type="xsd:string">MAN</value>
    </values>
    <values>
        <field>Mfr_Full_Name__c</field>
        <value xsi:type="xsd:string">MAN-GHH</value>
    </values>
</CustomMetadata>
