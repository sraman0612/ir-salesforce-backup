<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Belgium</label>
    <protected>false</protected>
    <values>
        <field>Country_de__c</field>
        <value xsi:type="xsd:string">Belgien</value>
    </values>
    <values>
        <field>Country_en__c</field>
        <value xsi:type="xsd:string">Belgium</value>
    </values>
    <values>
        <field>Country_fr__c</field>
        <value xsi:type="xsd:string">Belgique</value>
    </values>
    <values>
        <field>Country_it__c</field>
        <value xsi:type="xsd:string">Belgio</value>
    </values>
    <values>
        <field>VAT_Pattern_Example__c</field>
        <value xsi:type="xsd:string">BE0123456789</value>
    </values>
    <values>
        <field>VAT_Regex_Pattern__c</field>
        <value xsi:type="xsd:string">^BE[01]\d{9}</value>
    </values>
</CustomMetadata>
