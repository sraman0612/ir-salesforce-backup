<?xml version="1.0" encoding="UTF-8"?>
<Flow xmlns="http://soap.sforce.com/2006/04/metadata">
    <apiVersion>57.0</apiVersion>
    <areMetricsLoggedToDataCloud>false</areMetricsLoggedToDataCloud>
    <assignments>
        <description>Store the individual values from Meta data into a Record collection variable</description>
        <name>Assign_Campaing_Member_Status_Details_from_Metadata</name>
        <label>Assign Campaing Member Status Details from Metadata</label>
        <locationX>138</locationX>
        <locationY>935</locationY>
        <assignmentItems>
            <assignToReference>CampaignMemberStatusAllVariables.CampaignId</assignToReference>
            <operator>Assign</operator>
            <value>
                <elementReference>CampaignSFDCId</elementReference>
            </value>
        </assignmentItems>
        <assignmentItems>
            <assignToReference>CampaignMemberStatusAllVariables.Label</assignToReference>
            <operator>Assign</operator>
            <value>
                <elementReference>Loop_through_Metadata.Label</elementReference>
            </value>
        </assignmentItems>
        <assignmentItems>
            <assignToReference>CampaignMemberStatusAllVariables.HasResponded</assignToReference>
            <operator>Assign</operator>
            <value>
                <elementReference>Loop_through_Metadata.Responded__c</elementReference>
            </value>
        </assignmentItems>
        <connector>
            <targetReference>Campaign_Member_Records_from_mdt_assignment</targetReference>
        </connector>
    </assignments>
    <assignments>
        <description>To assign the Campaign SFDC Id which trigerred the flow to a variable,</description>
        <name>Assign_Campaing_SFDC_Id</name>
        <label>Assign Campaign SFDC Id Variable Assignment</label>
        <locationX>182</locationX>
        <locationY>335</locationY>
        <assignmentItems>
            <assignToReference>CampaignSFDCId</assignToReference>
            <operator>Assign</operator>
            <value>
                <elementReference>$Record.Id</elementReference>
            </value>
        </assignmentItems>
        <connector>
            <targetReference>Record_Type_Check</targetReference>
        </connector>
    </assignments>
    <assignments>
        <description>Assign the single Record collection variable into multiple record collection varaible which will be used further during record creation</description>
        <name>Campaign_Member_Records_from_mdt_assignment</name>
        <label>Campaign Member Records from mdt assignment</label>
        <locationX>138</locationX>
        <locationY>1055</locationY>
        <assignmentItems>
            <assignToReference>CreateCampaignMemberStatusRecords</assignToReference>
            <operator>Add</operator>
            <value>
                <elementReference>CampaignMemberStatusAllVariables</elementReference>
            </value>
        </assignmentItems>
        <connector>
            <targetReference>Loop_through_Metadata</targetReference>
        </connector>
    </assignments>
    <decisions>
        <name>Compare_Record_Types</name>
        <label>Compare Record Types</label>
        <locationX>270</locationX>
        <locationY>815</locationY>
        <defaultConnector>
            <targetReference>Loop_through_Metadata</targetReference>
        </defaultConnector>
        <defaultConnectorLabel>Default Outcome</defaultConnectorLabel>
        <rules>
            <name>Record_Types_Match</name>
            <conditionLogic>and</conditionLogic>
            <conditions>
                <leftValueReference>Loop_through_Metadata.Campaign_Type__c</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <elementReference>$Record.RecordType.Name</elementReference>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Assign_Campaing_Member_Status_Details_from_Metadata</targetReference>
            </connector>
            <label>Record Types Match</label>
        </rules>
    </decisions>
    <decisions>
        <description>To check the Record Type of Campaign Record which trigerred the flow with the Meta Data Types</description>
        <name>Record_Type_Check</name>
        <label>Record Type Check</label>
        <locationX>182</locationX>
        <locationY>455</locationY>
        <defaultConnectorLabel>Default Outcome</defaultConnectorLabel>
        <rules>
            <name>Matches_Required_Recordtype</name>
            <conditionLogic>or</conditionLogic>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Physical Event</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Virtual Event</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Digital Advertising</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Email Marketing</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Telemarketing</stringValue>
                </rightValue>
            </conditions>
            <conditions>
                <leftValueReference>$Record.RecordType.Name</leftValueReference>
                <operator>EqualTo</operator>
                <rightValue>
                    <stringValue>Print Advertising</stringValue>
                </rightValue>
            </conditions>
            <connector>
                <targetReference>Query_Metadata_to_Fetch_Campaign_Member_Status</targetReference>
            </connector>
            <label>Matches Recordtype</label>
        </rules>
    </decisions>
    <description>This flow is used to create Campaign Member Status for a campaign based on Campaign Record Type.
The status for creation is stored in Metadata</description>
    <environments>Default</environments>
    <interviewLabel>Campaign Member Status Creation Flow {!$Flow.CurrentDateTime}</interviewLabel>
    <label>Campaign Member Status Creation Flow</label>
    <loops>
        <name>Loop_through_Metadata</name>
        <label>Loop through Metadata</label>
        <locationX>50</locationX>
        <locationY>695</locationY>
        <collectionReference>GetCampaignMemberStatus</collectionReference>
        <iterationOrder>Asc</iterationOrder>
        <nextValueConnector>
            <targetReference>Compare_Record_Types</targetReference>
        </nextValueConnector>
        <noMoreValuesConnector>
            <targetReference>Create_Campaign_Member_Status_Records</targetReference>
        </noMoreValuesConnector>
    </loops>
    <processMetadataValues>
        <name>BuilderType</name>
        <value>
            <stringValue>LightningFlowBuilder</stringValue>
        </value>
    </processMetadataValues>
    <processMetadataValues>
        <name>CanvasMode</name>
        <value>
            <stringValue>AUTO_LAYOUT_CANVAS</stringValue>
        </value>
    </processMetadataValues>
    <processMetadataValues>
        <name>OriginBuilderType</name>
        <value>
            <stringValue>LightningFlowBuilder</stringValue>
        </value>
    </processMetadataValues>
    <processType>AutoLaunchedFlow</processType>
    <recordCreates>
        <description>Creation of mutliple Campaign Member Status records using multi record collection variable</description>
        <name>Create_Campaign_Member_Status_Records</name>
        <label>Create Campaign Member Status Records</label>
        <locationX>50</locationX>
        <locationY>1367</locationY>
        <inputReference>CreateCampaignMemberStatusRecords</inputReference>
    </recordCreates>
    <recordLookups>
        <description>Query and fetch the metadata records whose Campaign Type matches the Record Type of the record which trigerred this flow.</description>
        <name>Query_Metadata_to_Fetch_Campaign_Member_Status</name>
        <label>Query Metadata to Fetch Campaign Member Status</label>
        <locationX>50</locationX>
        <locationY>575</locationY>
        <assignNullValuesIfNoRecordsFound>false</assignNullValuesIfNoRecordsFound>
        <connector>
            <targetReference>Loop_through_Metadata</targetReference>
        </connector>
        <filterLogic>and</filterLogic>
        <filters>
            <field>Campaign_Type__c</field>
            <operator>EqualTo</operator>
            <value>
                <elementReference>$Record.RecordType.Name</elementReference>
            </value>
        </filters>
        <object>Campaign_Member_Status_Settings__mdt</object>
        <outputReference>GetCampaignMemberStatus</outputReference>
        <queriedFields>Id</queriedFields>
        <queriedFields>Label</queriedFields>
        <queriedFields>IsDefault__c</queriedFields>
        <queriedFields>Responded__c</queriedFields>
        <queriedFields>Campaign_Type__c</queriedFields>
    </recordLookups>
    <start>
        <locationX>56</locationX>
        <locationY>0</locationY>
        <connector>
            <targetReference>Assign_Campaing_SFDC_Id</targetReference>
        </connector>
        <object>Campaign</object>
        <recordTriggerType>Create</recordTriggerType>
        <triggerType>RecordAfterSave</triggerType>
    </start>
    <status>Active</status>
    <variables>
        <name>CampaignMemberStatusAllVariables</name>
        <dataType>SObject</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>CampaignMemberStatus</objectType>
    </variables>
    <variables>
        <name>CampaignSFDCId</name>
        <dataType>String</dataType>
        <isCollection>false</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
    </variables>
    <variables>
        <name>CreateCampaignMemberStatusRecords</name>
        <dataType>SObject</dataType>
        <isCollection>true</isCollection>
        <isInput>false</isInput>
        <isOutput>false</isOutput>
        <objectType>CampaignMemberStatus</objectType>
    </variables>
    <variables>
        <description>RecordVariable to store Campaign Member Statsu</description>
        <name>GetCampaignMemberStatus</name>
        <dataType>SObject</dataType>
        <isCollection>true</isCollection>
        <isInput>true</isInput>
        <isOutput>true</isOutput>
        <objectType>Campaign_Member_Status_Settings__mdt</objectType>
    </variables>
</Flow>
