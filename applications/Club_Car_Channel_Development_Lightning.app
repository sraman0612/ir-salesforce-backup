<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override updated by Lightning App Builder during activation.</comment>
        <content>Club_Car_Account1</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Account</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Club_Car_Contacts</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Contact</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Club_Car_Opportunities</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Opportunity</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>Rail_Forms</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>CC_RAIL__c</pageOrSobjectType>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>IRSPX_LEX_ACCOUNT_PLAN_CLUBCAR</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>AccountPlan__c</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#76F111</headerColor>
        <logo>CC_Logo</logo>
        <logoVersion>1</logoVersion>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>A Channel Development Application</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <label>Channel Development</label>
    <navType>Standard</navType>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Thermo_King_Marine_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>TK Marine Operations (Read-Only)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Thermo_King_Marine_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>TK Marine Operations</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS AP District Leadership</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS AP Sales Reps</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS EU Advanced Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CC_GPSi_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_GPSi</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>GES Standard User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS LATAM Advanced Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS LATAM Business Operations (Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CHVAC_Support_Services_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CHVAC Services</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PT_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>PT Marketing User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Hibon Standard User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>IR Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS LATAM District Leadership</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PT_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>PT Service User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS AP Business Operations (Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS LATAM Sales Reps</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_CX</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_General Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Edit</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Pavilion Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Co_Op_ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_CX</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_General Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Edit</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_Pavilion Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.Credit_Re_Bill</recordType>
        <type>Flexipage</type>
        <profile>CC_System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Advanced Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PT_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>PT Solution Center User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Business Operations (Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PT_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>PT Standard User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>PT_Home</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>PT System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_CX</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_General Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Edit</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_Pavilion Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Dealer_Invoicing</recordType>
        <type>Flexipage</type>
        <profile>CC_System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_CX</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_General Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Global User Edit</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Pavilion Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <recordType>CC_Order__c.CC_Parts_Ordering</recordType>
        <type>Flexipage</type>
        <profile>CC_System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_CX</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_General Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_Global User Read Only</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_Global User Edit</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_Pavilion Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_Sales Manager</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>View</actionName>
        <content>Club_Car_Orders</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>CC_Order__c</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CC_System Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Business Operations (Non-Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Marketing Team</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>MarketingProfile</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Sales Reps</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS AP Advanced Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Admin</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS EU Business Operations (Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS EU Sales Reps</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS MEIA Sales Reps</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Delegated Administrator</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS EU District Leadership</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>District Leadership</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>Global Leader</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS MEIA Advanced Sales Rep</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>CTS_Lightning_Home_Page</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>OEM Standard User</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS MEIA Business Operations (Edit)</profile>
    </profileActionOverrides>
    <profileActionOverrides>
        <actionName>Tab</actionName>
        <content>Home_Page_Default</content>
        <formFactor>Large</formFactor>
        <pageOrSobjectType>standard-home</pageOrSobjectType>
        <type>Flexipage</type>
        <profile>CTS MEIA District Leadership</profile>
    </profileActionOverrides>
    <tabs>standard-home</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Task</tabs>
    <tabs>standard-Event</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Dashboard</tabs>
    <tabs>standard-report</tabs>
    <tabs>CC_PostalCode__c</tabs>
    <tabs>standard-Contract</tabs>
    <tabs>CC_Programs_Eligibility__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Club_Car_Channel_Development_Lightning_UtilityBar</utilityBar>
</CustomApplication>
