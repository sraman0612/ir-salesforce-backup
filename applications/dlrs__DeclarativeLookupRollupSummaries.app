<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>dlrs__Welcome</defaultLandingTab>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <isNavTabPersistenceDisabled>false</isNavTabPersistenceDisabled>
    <isOmniPinnedViewEnabled>false</isOmniPinnedViewEnabled>
    <label>Declarative Lookup Rollup Summaries</label>
    <tabs>dlrs__Welcome</tabs>
    <tabs>dlrs__ManageLookupRollupSummaries</tabs>
    <tabs>dlrs__LookupRollupSummaryLog__c</tabs>
    <tabs>Assignment_Groups__c</tabs>
</CustomApplication>
