<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>OBMNotifier_Null_ErrorMessage</fullName>
        <field>Integration_Error_Message__c</field>
        <name>OBMNotifier_Null_ErrorMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update Integration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime_Delete</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>NOW()</formula>
        <name>Update_Integration_DateTime_Delete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update Integration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status_Delete</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update_Integration_Status_Delete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>IR_OBM_Notifier_Delete_PROD</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>External_Id__c</fields>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>Master_External_Id__c</fields>
        <fields>Object_Type__c</fields>
        <fields>Operation__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_OBM_Notifier_Delete_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>IR_OBM_Notifier_PROD</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>External_Id__c</fields>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>Master_External_Id__c</fields>
        <fields>Object_Type__c</fields>
        <fields>SFDC_Id__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_OBM_Notifier_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>IR_Delete_OBM_PROD</fullName>
        <actions>
            <name>Update_Integration_DateTime_Delete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status_Delete</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_OBM_Notifier_Delete_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; AND(  NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ),  $User.LastName &lt;&gt; &quot;Clean&quot;,  Master_External_Id__c == &quot;&quot;  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IR_OBM_Notifier_PROD</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_OBM_Notifier_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == false &amp;&amp; IF(AND(ISNEW(),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ),  $User.LastName&lt;&gt;&quot;Clean&quot;, Master_External_Id__c&lt;&gt;&quot;&quot;),true, IF(AND(NOT(ISNEW()),AND( NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ),  $User.LastName&lt;&gt;&quot;Clean&quot;, Master_External_Id__c&lt;&gt;&quot;&quot;,ISCHANGED(Master_External_Id__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OBMNotifier_Null_ErrorMessage</fullName>
        <actions>
            <name>OBMNotifier_Null_ErrorMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; TEXT( Integration_Status__c) == &apos;Success&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
