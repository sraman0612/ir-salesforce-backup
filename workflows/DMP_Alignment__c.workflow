<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculate_DMP_Alignment_Score</fullName>
        <field>Alignment_Score__c</field>
        <formula>Conducts_demandside_audit_Score__c + Conducts_supplier_audits_Score__c  + Has_3_years_good_credit_Score__c +
IR_Placed_Prominently_on_Website_Score__c +
 IR_Premium_Brand_Image_Score__c +
 IR_Safety_Requirements_Score__c +
 Management_stability_Score__c +
 Offers_Multi_Year_Svc_Contracts_Score__c +
 Participates_in_IR_Training_Score__c +
 Professional_Service_Vehicles_Score__c +
 Promotes_only_IR_compressors_Score__c +
 Promotes_only_IR_dryers_Score__c +
 Provides_24_Hour_Service_Score__c +
 Stocks_Adequate_Parts_Score__c +
 Stocks_Complete_Equipment_Score__c +
 Uses_only_rec_equipment_Score__c +
 Uses_only_rec_filters_Score__c +
 Uses_only_rec_lubricant_Score__c</formula>
        <name>Calculate DMP Alignment Score</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Calculate DMP Alignment Score</fullName>
        <actions>
            <name>Calculate_DMP_Alignment_Score</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DMP_Alignment__c.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
