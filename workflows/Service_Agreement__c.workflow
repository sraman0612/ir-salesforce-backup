<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_LATAM_Service_Agreement_Active</fullName>
        <description>CTS LATAM Service Agreement Active</description>
        <protected>false</protected>
        <recipients>
            <recipient>paola.paredes@irco.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>talis_bennemann@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_LATAM_Service_Agreement_Active</template>
    </alerts>
    <alerts>
        <fullName>CTS_LATAM_Service_Agreement_Booked</fullName>
        <description>CTS LATAM Service Agreement Booked</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_LATAM_Service_Agreement_Booked</template>
    </alerts>
    <alerts>
        <fullName>CTS_LATAM_Service_Agreement_Rejected</fullName>
        <description>CTS LATAM Service Agreement Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_LATAM_Service_Agreement_Rejected</template>
    </alerts>
    <alerts>
        <fullName>SA_End_date_Notified_to_Owner</fullName>
        <description>Notify Account and Service Agreement Owner that Service agreement End date reached 90 days prior it was defined</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/X90_Days_before_Agreement_end_date</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Submitted</fullName>
        <field>CTS_Approval_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Approval Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>CTS_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Serv_Agree_RecType_Dir</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Service_Agreement</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Serv Agree RecType Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Serv_Agree_RecType_Ind</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Ind_Service_Agreement</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Serv Agree RecType Ind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CreateNewOpp_Record</fullName>
        <field>X90DaysPriorEndDate__c</field>
        <literalValue>1</literalValue>
        <name>CreateNewOppRecord</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Approval</fullName>
        <description>Approved by Marketing</description>
        <field>Agreement_Status__c</field>
        <literalValue>Approved by Marketing</literalValue>
        <name>Marketing Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Sets the SBU ID Workflow field value for IBM Watson Marketing Automation</description>
        <field>SBU_ID_Workflow__c</field>
        <formula>IF( 
Account__r.RecordType.DeveloperName = &apos;Thermo_King_Marine&apos;, &apos;TK&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CHVAC_China&apos;, &apos;CHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CHVAC_Singapore&apos;, &apos;CHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CHVAC_Thailand&apos;, &apos;CHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;India_CS_Account&apos;, &apos;CHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_Global_Distributor&apos;, &apos;CTS&apos;,   
IF(Account__r.RecordType.DeveloperName = &apos;Site_Account_NA_Air&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_AP&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_AP_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_New_AP_Account&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_EU&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_New_EU_Account&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_LATAM&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_LATAM_INDIRECT&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_New_LATAM_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_MEIA&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_New_MEIA_Account&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;CTS_TechDirect_Account&apos;, &apos;CTS&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;GES_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;Hibon_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;OEM_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;RS_CRS_Home_Owner&apos;, &apos;RHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;RS_CRS_IWD_Dealer&apos;, &apos;RHVAC&apos;,
IF(Account__r.RecordType.DeveloperName = &apos;Thermo_King_Strategic&apos;, &apos;TK&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;PT_powertools&apos;, &apos;Power Tools&apos;,
IF( LEFT(RecordType.DeveloperName,4) = &apos;Club&apos;, &apos;Club Car&apos;,
&apos;&apos;))))))))))))))))))))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Recalled</fullName>
        <field>CTS_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected</fullName>
        <field>CTS_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Agreement_Booked</fullName>
        <description>Service Agreement Booked</description>
        <field>Agreement_Status__c</field>
        <literalValue>Booked</literalValue>
        <name>Service Agreement Booked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Agreement_Recalled</fullName>
        <description>Service Agreement Recalled</description>
        <field>Agreement_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Service Agreement Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Agreement_Rejected</fullName>
        <description>Service Agreement Rejected</description>
        <field>Agreement_Status__c</field>
        <literalValue>Draft</literalValue>
        <name>Service Agreement Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Ops_Approval</fullName>
        <description>Service Ops Approval</description>
        <field>Agreement_Status__c</field>
        <literalValue>Approved by Service Ops</literalValue>
        <name>Service Ops Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Global Update Serv Agree RecType Dir</fullName>
        <actions>
            <name>CTS_Global_Update_Serv_Agree_RecType_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CTS Global Update Service Agree RecType Dir</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DIRECT&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update Serv Agree RecType Ind</fullName>
        <actions>
            <name>CTS_Global_Update_Serv_Agree_RecType_Ind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>CTS Global Update Service Agree RecType Ind</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,  ISPICKVAL(Account__r.CTS_Channel__c, &apos;DISTRIBUTOR&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS LATAM Service Agreement Active</fullName>
        <actions>
            <name>CTS_LATAM_Service_Agreement_Active</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Notification to Marketing Team and Service Ops when Agreement is Active</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(ISCHANGED(Agreement_Status__c), TEXT(Agreement_Status__c) = &quot;Active&quot;, OR(RecordType.DeveloperName == &quot;CTS_LATAM_Service_Agreement&quot;, RecordType.DeveloperName == &quot;CTS_LATAM_Ind_Service_Agreement&quot;),  CreatedBy.LastName &lt;&gt; &quot;Migration&quot;, CreatedBy.LastName &lt;&gt; &quot;Integration&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Agreement</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets the SBU ID Workflow field value for IBM Watson Marketing Automation</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISBLANK(SBU_ID_Workflow__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
