<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TwilioSF__Opt_In_Keyword_Set_Unique_Key</fullName>
        <field>TwilioSF__Unique_Key_Word__c</field>
        <formula>Name</formula>
        <name>Opt-In Keyword Set Unique Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TwilioSF__Opt-In Keyword Unique</fullName>
        <actions>
            <name>TwilioSF__Opt_In_Keyword_Set_Unique_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TwilioSF__Opt_In_Keyword__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
