<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_isBDM_MDM_changed</fullName>
        <field>Is_BDM_MDM_Changed__c</field>
        <literalValue>1</literalValue>
        <name>Set isBDM/MDM changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Costcenter checkbox on owner change</fullName>
        <actions>
            <name>Set_isBDM_MDM_changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; OR(ISCHANGED(BDM__c),ISCHANGED(MDM__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
