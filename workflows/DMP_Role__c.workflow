<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Rollup_Formula_for_IR_CTS_Focus</fullName>
        <field>Focus_Percent_Formula_for_Rollup__c</field>
        <formula>CASE( IR_CTS_Focus__c ,
&quot;10%&quot;, 0.1 ,
&quot;20%&quot;, 0.2 ,
&quot;30%&quot;, 0.3 ,
&quot;40%&quot;, 0.4 ,
&quot;50%&quot;, 0.5 ,
&quot;60%&quot;, 0.6 ,
&quot;70%&quot;, 0.7 ,
&quot;80%&quot;, 0.8 ,
&quot;90%&quot;, 0.9 ,
&quot;100%&quot;, 1.0 ,
0)</formula>
        <name>Update Rollup Formula for IR CTS Focus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Populate Rollup Formula for FTE</fullName>
        <actions>
            <name>Update_Rollup_Formula_for_IR_CTS_Focus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>DMP_Role__c.IR_CTS_Focus__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
