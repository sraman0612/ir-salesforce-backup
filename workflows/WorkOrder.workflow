<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set</fullName>
        <description>Set the classification code to &apos;Service Request&apos;</description>
        <field>Classification_Code__c</field>
        <literalValue>SERVICE REQUEST</literalValue>
        <name>Set Classification Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
