<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Delete_Opp_Line_Item_Request_Reject</fullName>
        <description>Delete Request Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_of_Request</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Remove_Green_Sheet_Flag</fullName>
        <field>Green_Sheet__c</field>
        <literalValue>0</literalValue>
        <name>CTS Remove Green Sheet Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Set_Green_Sheet_Checkbox</fullName>
        <field>Green_Sheet__c</field>
        <literalValue>1</literalValue>
        <name>CTS Set Green Sheet Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Stage_updated_to_Verify</fullName>
        <description>Opportunity Stage Updated to Verify when Opportunity Offerings added.</description>
        <field>StageName</field>
        <literalValue>Stage 2. Discover/Verify</literalValue>
        <name>Opportunity Stage updated to Verify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Opportunity__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Remove Green Sheet Flag</fullName>
        <actions>
            <name>CTS_Remove_Green_Sheet_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;814_Centac,850_Plant Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;851_Engineered Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;852_Process Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Centac&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Centrifugal&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Completes/814_Centac&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Completes/850_Plant Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Completes/851_Engineered Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;Completes/852_Process Air&apos; &amp;&amp; TEXT( Master_Sub_Category__c) != &apos;PAC&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Set Green Sheet Flag</fullName>
        <actions>
            <name>CTS_Set_Green_Sheet_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ( TEXT(Master_Sub_Category__c) == &apos;814_Centac&apos; || TEXT(Master_Sub_Category__c) == &apos;850_Plant Air&apos; || TEXT(Master_Sub_Category__c) == &apos;851_Engineered Air&apos; || TEXT(Master_Sub_Category__c) == &apos;852_Process Air&apos; || TEXT(Master_Sub_Category__c) == &apos;Centac&apos; || TEXT(Master_Sub_Category__c) == &apos;Centrifugal&apos; || TEXT(Master_Sub_Category__c) == &apos;Completes/814_Centac&apos; || TEXT(Master_Sub_Category__c) == &apos;Completes/850_Plant Air&apos; || TEXT(Master_Sub_Category__c) == &apos;Completes/851_Engineered Air&apos; || TEXT(Master_Sub_Category__c) == &apos;Completes/852_Process Air&apos; || TEXT(Master_Sub_Category__c) == &apos;PAC&apos; )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
