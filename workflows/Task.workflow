<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_NWC_Validation_Task_Assigned_Email</fullName>
        <description>CTS NWC Validation Task Assigned Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>cts-nonwarrantyclaim@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_Order_Management/CTS_NWC_Validation_Task_Assigned_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activity_Date</fullName>
        <field>Activity_Date__c</field>
        <formula>IF( 

NOT( ISBLANK(ActivityDate ) ) , 

DATE (
YEAR ( ActivityDate ),
MONTH( ActivityDate ),
DAY( ActivityDate ) ), 

NULL )</formula>
        <name>Activity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Task_to_be_Closed</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Task to be Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Stage_to_Target</fullName>
        <field>Opportunity_Stage__c</field>
        <formula>&quot;Target&quot;</formula>
        <name>Update Opportunity Stage to Target</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS NWC Validation Task Assigned Email</fullName>
        <actions>
            <name>CTS_NWC_Validation_Task_Assigned_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>CTS Non-Warranty Claims - Email sent to IR when Case/Claim activity is sent out for validation</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( RecordType.DeveloperName = &apos;CTS_Non_Warranty_Claims&apos;,   OwnerId  &lt;&gt; &apos;&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task Not Needed</fullName>
        <actions>
            <name>Task_to_be_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If task Not Needed is set to True, Status is updated to &apos;Completed&apos;</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(Task_Not_Needed__c, RecordType.DeveloperName =&apos;NA_Air_Task&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Activity Date</fullName>
        <actions>
            <name>Activity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; OR( ISBLANK( Activity_Date__c ) ,  ISCHANGED( ActivityDate ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Opportunity Stage for Targets</fullName>
        <actions>
            <name>Update_Opportunity_Stage_to_Target</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Opportunity Stage on Tasks to &quot;Target&quot; when created on Target.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Opportunity_Stage__c == &apos;&apos; &amp;&amp; CallObject == &apos;Target&apos; &amp;&amp; RecordType.Name == &apos;NA Air Task&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
