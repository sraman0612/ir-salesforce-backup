<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>A_Buffalo_RFQ_Form_has_been_submitted</fullName>
        <description>A Buffalo RFQ Form has been submitted.</description>
        <protected>false</protected>
        <recipients>
            <recipient>CTS_Buffalo_RFQ_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Buffalo_RFQ_Quote</template>
    </alerts>
    <alerts>
        <fullName>Buffalo_RFQ_Quote_Accepted</fullName>
        <description>Buffalo RFQ Quote Accepted</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Buffalo_RFQ_Quote_Approved</template>
    </alerts>
    <alerts>
        <fullName>Buffalo_RFQ_Quote_Completed</fullName>
        <description>Buffalo RFQ Quote Completed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Buffalo_RFQ_Quote_Completed</template>
    </alerts>
    <alerts>
        <fullName>Buffalo_RFQ_Quote_Rejected</fullName>
        <description>Buffalo RFQ Quote Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Buffalo_RFQ_Quote_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_Aero_RT_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Aero_Upgrade_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Buffalo RFQ Aero RT Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_Controls_RT_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Controls_Upgrade_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Buffalo RFQ Controls RT Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_General_RT_Update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>General_Upgrade_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Buffalo RFQ General RT Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_Repair_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Repair_Quote_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Buffalo RFQ Repair RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_Set_Owner_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>Buffalo_RFQ_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Buffalo RFQ Set Owner Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Buffalo_RFQ_Update_Cycle_Time</fullName>
        <field>Cycle_Time__c</field>
        <formula>( Completed_Date__c - Send_Form_Date__c)</formula>
        <name>Buffalo RFQ Update Cycle Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Rejection_Flag_on_Send</fullName>
        <field>Quote_Rejected__c</field>
        <literalValue>0</literalValue>
        <name>Clear Rejection Flag on Send</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Rejection_Notes</fullName>
        <field>Quote_Rejection_Notes__c</field>
        <name>Clear Rejection Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_Rejection_Reason</fullName>
        <field>Quote_Rejection_Reasons__c</field>
        <name>Clear Rejection Reason</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Send_Form_Date</fullName>
        <field>Send_Form_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Send Form Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Sent</fullName>
        <field>Quote_Status__c</field>
        <literalValue>Quote Received</literalValue>
        <name>Status Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Custom_Name_Field</fullName>
        <field>Buffalo_RFQ_Name__c</field>
        <formula>TEXT(Quote_Type__c) +&apos; &apos;+ TEXT(CreatedDate)</formula>
        <name>Update Custom Name Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Buffalo RFQ Aero RT</fullName>
        <actions>
            <name>Buffalo_RFQ_Aero_RT_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Custom_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Quote_Type__c, &apos;Aero Upgrade Quote Request&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Buffalo RFQ Controls RT</fullName>
        <actions>
            <name>Buffalo_RFQ_Controls_RT_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Custom_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Quote_Type__c, &apos;Controls Upgrade Quote Request&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Buffalo RFQ General RT</fullName>
        <actions>
            <name>Buffalo_RFQ_General_RT_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Custom_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Quote_Type__c, &apos;General Upgrade Quote Request&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Buffalo RFQ Repair RT</fullName>
        <actions>
            <name>Buffalo_RFQ_Repair_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Custom_Name_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Quote_Type__c, &apos;Repair Quote Request&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Buffalo RFQ Send Form</fullName>
        <actions>
            <name>A_Buffalo_RFQ_Form_has_been_submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Buffalo_RFQ_Set_Owner_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Rejection_Flag_on_Send</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Rejection_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Clear_Rejection_Reason</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Send_Form_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Status_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; Send_Form__c  = True</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
