<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AI_Original_Email_Body_Update</fullName>
        <field>AI_Original_Email_Body__c</field>
        <formula>TextBody</formula>
        <name>AI &apos;Original Email Body&apos; Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Brand_Field_Update_Part_1</fullName>
        <field>Brand_Text__c</field>
        <formula>CASE( ToAddress , 
&quot;gdcomp.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS - Compressor&quot;, 
&quot;champion.cs.qcy@gardnerdenver.com&quot;,&quot;Champion - CS&quot;,
&quot;highpressuremachines.cs.qcy@gardnerdenver.com&quot;,&quot;Belliss - CS&quot;,
&quot;bellissparts.cs.qcy@gardnerdenver.com&quot;,&quot;Belliss - CS&quot;,
&quot;reveallparts.cs.qcy@gardnerdenver.com&quot;,&quot;Reveall - CS&quot;,
&quot;locomotive.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS - Compressor&quot;,
&quot;mako.cs.qcy@gardnerdenver.com&quot;,&quot;MAKO - CS&quot;,
&quot;quantima.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS - Compressor&quot;,
&quot;blower.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS - Blower&quot;,
&quot;vacuum.cs.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - CS&quot;,
&quot;vacuumsystems.cs.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - CS&quot;,
&quot;mobile.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS - Blower&quot;,
&quot;gdrotary.tech.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - TS - Compressor&quot;,
&quot;gdrecip.tech.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - TS - Compressor&quot;,
&quot;championrecip.tech.qcy@gardnerdenver.com&quot;,&quot;Champion - TS&quot;,
&quot;championrotary.tech.qcy@gardnerdenver.com&quot;,&quot;Champion - TS&quot;,
&quot;leroi.tech.qcy@gardnerdenver.com&quot;,&quot;LeROI - TS&quot;,
&quot;rotaryvane.tech.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - TS - Compressor&quot;,
&quot;quantima.tech.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - TS - Compressor&quot;,
&quot;mako.tech.qcy@gardnerdenver.com&quot;,&quot;MAKO - TS&quot;,
&quot;reveall.tech.qcy@gardnerdenver.com&quot;,&quot;Reveall - TS&quot;,
&quot;gddryer.tech.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - TS - Compressor&quot;,
&quot;championdryer.tech.qcy@gardnerdenver.com&quot;,&quot;Champion - TS&quot;,
&quot;gdrotary.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Compressor&quot;,
&quot;gdrecip.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Compressor&quot;,
&quot;championrecip.field.qcy@gardnerdenver.com&quot;,&quot;Champion - FSS&quot;,
&quot;championrotary.field.qcy@gardnerdenver.com&quot;,&quot;Champion - FSS&quot;,
&quot;leroi.field.qcy@gardnerdenver.com&quot;,&quot;LeROI - FSS&quot;,
&quot;rotaryvane.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Compressor&quot;,
&quot;quantima.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Compressor&quot;,
&quot;mako.field.qcy@gardnerdenver.com&quot;,&quot;MAKO - FSS&quot;,
&quot;reveall.field.qcy@gardnerdenver.com&quot;,&quot;Reveall - FSS&quot;,
&quot;gddryer.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Compressor&quot;,
&quot;championdryer.field.qcy@gardnerdenver.com&quot;,&quot;Champion - FSS&quot;,
&quot;blower.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Blower&quot;,
&quot;vacuum.field.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - FSS&quot;,
&quot;vacuumsystems.field.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - FSS&quot;,
&quot;mobile.field.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - FSS - Blower&quot;, 
&quot;gdcomp.warranty.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - WA - Compressor&quot;, 
&quot;mako.warranty.qcy@gardnerdenver.com&quot;,&quot;MAKO - WA&quot;,
&quot;champion.warranty.qcy@gardnerdenver.com&quot;,&quot;Champion - WA&quot;,
&quot;blower.warranty.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - WA - Blower&quot;,
&quot;vacuum.warranty.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - WA&quot;, 
&quot;vacuumsystems.warranty.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - WA&quot;,
&quot;mobile.warranty.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - WA - Blower&quot;,
&quot;belliss.service.qcy@gardnerdenver.com&quot;,&quot;Belliss - BSS&quot;,
&quot;managedservice.qcy@gardnerdenver.com&quot;,&quot;Champion - MC&quot;,
&quot;national.accounts@gardnerdenver.com&quot;,&quot;Champion - MC&quot;,
&quot;dispatch.ms.qcy@gardnerdenver.com&quot;,&quot;Champion - MC&quot;,
&quot;mdm.qcy@gardnerdenver.com&quot;,&quot;Master Data Management&quot;,
&quot;leroigas.cs.qcy@gardnerdenver.com&quot;,&quot;LeRoi - CS&quot;,
&quot;leroigas.tech.sidney@gardnerdenver.com&quot;, &quot;LeRoi - CS&quot;,
&quot;leroigas.warranty.qcy@gardnerdenver.com&quot;, &quot;LeRoi - WA&quot;,
&quot;&quot;
)</formula>
        <name>Brand Field Update Part 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Brand_Field_Update_Pt_2</fullName>
        <field>Brand_Text__c</field>
        <formula>CASE( ToAddress ,
&quot;customerservice@dvsystems.com&quot;,&quot;DV Systems - CS&quot;,
&quot;dryer.field@dvsystems.com&quot;,&quot;DV Systems - FSS&quot;,
&quot;recip.field@dvsystems.com&quot;,&quot;DV Systems - FSS&quot;,
&quot;rotary.field@dvsystems.com&quot;,&quot;DV Systems - FSS&quot;,
&quot;dryer.tech@dvsystems.com&quot;,&quot;DV Systems - TS&quot;,
&quot;recip.tech@dvsystems.com&quot;,&quot;DV Systems - TS&quot;,
&quot;rotary.tech@dvsystems.com&quot;,&quot;DV Systems - TS&quot;,
&quot;warranty@dvsystems.com&quot;,&quot;DV Systems - WA&quot;,
&quot;belliss.warranty.qcy@gardnerdenver.com&quot;,&quot;Belliss - WA&quot;,
&quot;reavell.warranty.qcy@gardnerdenver.com&quot;,&quot;Reavell - WA&quot;,
&quot;blower.cs.spr@md-kinney.com&quot;,&quot;MD Blower - CS&quot;,
&quot;mobile.cs.spr@md-kinney.com&quot;,&quot;MD Transport - CS&quot;,
&quot;vacuum.cs.spr@md-kinney.com&quot;,&quot;Kinney Vacuum - CS&quot;,
&quot;blower.field.spr@md-kinney.com&quot;,&quot;MD Blower - FSS&quot;,
&quot;mobile.field.spr@md-kinney.com&quot;,&quot;MD Transport - FSS&quot;,
&quot;vacuum.field.spr@md-kinney.com&quot;,&quot;Kinney Vacuum - FSS&quot;,
&quot;blower.warranty.spr@md-kinney.com&quot;,&quot;MD Blower - WA&quot;,
&quot;mobile.field.spr@md-kinney.com&quot;,&quot;MD Transport - WA&quot;,
&quot;vacuum.field.spr@md-kinney.com&quot;,&quot;Kinney Vacuum - WA&quot;,
&quot;&quot;
)</formula>
        <name>Brand Field Update Pt 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Set_Read_Email_Flag</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>0</literalValue>
        <name>CTS OM Set Read Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_OM_Set_Unread_Email_Flag</fullName>
        <field>Unread_Email_box__c</field>
        <literalValue>1</literalValue>
        <name>CTS OM Set Unread Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_RMS_Messages_Received</fullName>
        <field>Number_of_Case_Messages_Received__c</field>
        <formula>IF(
ISBLANK(Parent.Number_of_Case_Messages_Received__c),
1,
Parent.Number_of_Case_Messages_Received__c + 1
)</formula>
        <name>CTS RMS Messages Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_RMS_Set_Unread_Email_Flag</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>1</literalValue>
        <name>CTS RMS Set Unread Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Set_Unread_Email_Flag</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>1</literalValue>
        <name>CTS Set Unread Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Email_Targeted_Address</fullName>
        <field>Targeted_Email__c</field>
        <formula>ToAddress</formula>
        <name>Case Email - Targeted Address</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Number_of_Messages_Received</fullName>
        <description>Updates/increments the Case Record with number of case emails received</description>
        <field>Number_of_Case_Messages_Received__c</field>
        <formula>IF(
				ISBLANK(Parent.Number_of_Case_Messages_Received__c), 
				1, 
				Parent.Number_of_Case_Messages_Received__c + 1
)</formula>
        <name>Case: Number of Messages Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Email_Waiting_Update</fullName>
        <field>Status</field>
        <literalValue>Email Waiting</literalValue>
        <name>Case Status Email Waiting Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Open_Update</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Case Status Open Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Waiting_Case_Status_Field_Update</fullName>
        <field>Status</field>
        <literalValue>Email Waiting</literalValue>
        <name>&apos;Email Waiting&apos; Case Status Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Waiting_Checkbox_Checked</fullName>
        <field>Email_Waiting_Icon__c</field>
        <literalValue>1</literalValue>
        <name>Email Waiting Checkbox - Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Category_Field_Update_Part_1</fullName>
        <field>Product_Category_Text__c</field>
        <formula>CASE( ToAddress , 
&quot;gdcomp.cs.qcy@gardnerdenver.com&quot;,&quot;Compressor - CS&quot;, 
&quot;champion.cs.qcy@gardnerdenver.com&quot;,&quot;Compressor - CS&quot;, 
&quot;locomotive.cs.qcy@gardnerdenver.com&quot;,&quot;Compressor - CS&quot;, 
&quot;quantima.cs.qcy@gardnerdenver.com&quot;,&quot;Compressor - CS&quot;, 
&quot;blower.cs.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - CS&quot;, 
&quot;vacuum.cs.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - CS&quot;, 
&quot;vacuumsystems.cs.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - CS&quot;, 
&quot;mobile.cs.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - CS&quot;, 
&quot;gdrotary.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;gdrecip.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;championrecip.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;championrotary.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;leroi.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;rotaryvane.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;quantima.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;gddryer.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;championdryer.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;, 
&quot;blower.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS&quot;, 
&quot;vacuum.cs.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - CS&quot;, 
&quot;vacuumsystems.cs.qcy@gardnerdenver.com&quot;,&quot;Elmo Rietschle - CS&quot;, 
&quot;mobile.cs.qcy@gardnerdenver.com&quot;,&quot;Gardner Denver - CS&quot;, 
&quot;gdrotary.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;gdrecip.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;championrecip.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;championrotary.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;leroi.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;rotaryvane.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;quantima.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;gddryer.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;championdryer.field.qcy@gardnerdenver.com&quot;,&quot;Compressor - FSS&quot;, 
&quot;blower.field.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - FSS&quot;, 
&quot;vacuum.field.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - FSS&quot;, 
&quot;vacuumsystems.field.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - FSS&quot;, 
&quot;mobile.field.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - FSS&quot;, 
&quot;gdcomp.warranty.qcy@gardnerdenver.com&quot;,&quot;Compressor - WA&quot;, 
&quot;champion.warranty.qcy@gardnerdenver.com&quot;,&quot;Compressor - WA&quot;, 
&quot;blower.warranty.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - WA&quot;, 
&quot;vacuum.warranty.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - WA&quot;, 
&quot;vacuumsystems.warranty.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - WA&quot;, 
&quot;mobile.warranty.qcy@gardnerdenver.com&quot;,&quot;Blower &amp; Vacuum - WA&quot;, 
&quot;managedservice.qcy@gardnerdenver.com&quot;,&quot;Compressor - MC&quot;,
&quot;national.accounts@gardnerdenver.com&quot;,&quot;Compressor - MC&quot;,
&quot;dispatch.ms.qcy@gardnerdenver.com&quot;,&quot;Compressor - MC&quot;,
&quot;ancillary.tech.qcy@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;,
&quot;fluidpump.cs.qcy@gardnerdenver.com&quot;,&quot;Fluid Pump - CS&quot;,
&quot;oberdorfermail@gardnerdenver.com&quot;,&quot;Fluid Pump - CS&quot;,
&quot;fluidpump.tech.qcy@gardnerdenver.com&quot;,&quot;Fluid Pump - TS&quot;,
&quot;fluidpump.app.qcy@gardnerdenver.com&quot;,&quot;Fluid Pump - AE&quot;,
&quot;fluidpump.warranty.qcy@gardnerdenver.com&quot;,&quot;Fluid Pump - WA&quot;,
&quot;mdm.qcy@gardnerdenver.com&quot;,&quot;Master Data Management&quot;,
&quot;leroigas.tech.sidney@gardnerdenver.com&quot;,&quot;Compressor - TS&quot;,
&quot;leroigas.cs.qcy@gardnerdenver.com&quot;,&quot;Compressor - CS&quot;,
&quot;leroigas.warranty.qcy@gardnerdenver.com&quot;,&quot;Compressor - WA&quot;,
&quot;&quot; 
)</formula>
        <name>Product Category Field Update Part 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Re_open_Case_Checkbox_Field_Update</fullName>
        <field>Re_opened__c</field>
        <literalValue>1</literalValue>
        <name>Re-open Case Checkbox Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Priority_to_High</fullName>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Set Case Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Status_to_In_Progress</fullName>
        <field>Status</field>
        <literalValue>Open</literalValue>
        <name>Set Case Status to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Number_of_Messages_Sent</fullName>
        <description>Updates/increments the Case Record with number of cases sent</description>
        <field>Number_of_Case_Messages_Sent__c</field>
        <formula>IF(
				ISBLANK(Parent.Number_of_Case_Messages_Sent__c), 
				1, 
				Parent.Number_of_Case_Messages_Sent__c + 1
)</formula>
        <name>Update Case: Number of Messages Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_First_Email_Date</fullName>
        <field>First_Email_Sent__c</field>
        <formula>MessageDate</formula>
        <name>Update First Email Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Full_Description_on_Case</fullName>
        <field>Full_Case_Description__c</field>
        <formula>HtmlBody</formula>
        <name>Update Full Description on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Last_Received_Email_Date</fullName>
        <field>Last_Email_Received_Date__c</field>
        <formula>IF( 
ISPICKVAL(Status, &apos;New&apos;), 
MessageDate, 
NULL)</formula>
        <name>Update Last Received Email Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>CTS Email to Case - High Priority</fullName>
        <actions>
            <name>Set_Case_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Sets the Case Priority to High if the Email is marked as urgent. Active for TechDirect and OM cases</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND(Parent.RecordType.DeveloperName = &quot;CTS_TechDirect_Ask_a_Question&quot;,  OR ( CONTAINS( Headers , &apos;Importance: High&apos;) ,  CONTAINS( Headers , &apos;X-Priority: 1&apos;) ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>CTS New Case Email</fullName>
        <actions>
            <name>CTS_Set_Unread_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND( 	Incoming, 	OR( 		Parent.RecordType.DeveloperName  = &quot;NA_Completes_Product_Support&quot;, Parent.RecordType.DeveloperName  = &quot;CTS_NA_North_Central_Parts&quot;,  Parent.RecordType.DeveloperName  = &quot;CTS_NA_South_Central_Parts&quot;,  Parent.RecordType.DeveloperName  = &quot;CTS_NA_South_Parts&quot;,  Parent.RecordType.DeveloperName  = &quot;North_Central_Area_Area_Specialists&quot;, Parent.RecordType.DeveloperName  = &quot;South_Area_Area_Specialists&quot;	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Email Message Received</fullName>
        <actions>
            <name>CTS_OM_Set_Unread_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Number_of_Messages_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS OM - Inbound Email has been received for the Case</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND( Incoming, OR( Parent.RecordType.DeveloperName = &quot;CTS_Non_Warranty_Claims&quot;, Parent.RecordType.DeveloperName = &quot;CTS_OM_Transfers&quot;, Parent.RecordType.DeveloperName = &quot;CTS_OM_ZEKS&quot;, Parent.RecordType.DeveloperName = &quot;CTS_OM_Americas&quot;, Parent.RecordType.DeveloperName = &quot;CTS_OM_Americas_Transfers&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Email Message Sent</fullName>
        <actions>
            <name>CTS_OM_Set_Read_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Number_of_Messages_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS OM - Outbound Email has been sent from the Case</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND(  ISPICKVAL( Status , &apos;Sent&apos;) , OR( Parent.RecordType.DeveloperName = &quot;CTS_Non_Warranty_Claims&quot;, Parent.RecordType.DeveloperName = &quot;Customer_Service&quot;, Parent.RecordType.DeveloperName = &quot;CTS_OM_Account_Management&quot;, Parent.RecordType.DeveloperName = &quot;ETO_Support_Case&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Read Email Message</fullName>
        <actions>
            <name>CTS_OM_Set_Read_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; IF(    OR(       AND(          OR(             CONTAINS(Parent.RecordType.DeveloperName, &apos;CTS_OM&apos;),             CONTAINS(Parent.RecordType.DeveloperName, &apos;CTS_Non&apos;)          ),                    ISCHANGED(Status), 								              PRIORVALUE(Status) = &apos;New&apos;                 ), 								       AND( 								              OR(             CONTAINS(Parent.RecordType.DeveloperName, &apos;CTS_OM&apos;),             CONTAINS(Parent.RecordType.DeveloperName, &apos;CTS_Non&apos;)          ),          ISCHANGED(LastModifiedDate), 												           Parent.LastModifiedBy.Id = Parent.OwnerId  								       ) 				    ),         TRUE,          FALSE   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS OM Unread Email Message</fullName>
        <actions>
            <name>CTS_OM_Set_Unread_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>IR Comp OM Account Management,IR Comp OM Account Management (Closed),IR Comp OM Account Management (Incident),IR Comp OM Account Management (Problem),IR Comp OM Transfers,IR Comp Non-Warranty Claims</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Unread_Email_box__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS RMS Email Received</fullName>
        <actions>
            <name>CTS_RMS_Messages_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_RMS_Set_Unread_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; Incoming == True &amp;&amp; Parent.RecordType.DeveloperName = &apos;CTS_RMS&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Email Message Received</fullName>
        <actions>
            <name>CTS_OM_Set_Unread_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Case_Number_of_Messages_Received</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS TechDirect - Inbound Email has been received for the Case - Uses existing OM fields</description>
        <formula>AND( $Permission.DataMigrationUser == False, Incoming == True, OR( Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Ask_a_Question&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Issue_Escalation&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Start_Up&apos;, Parent.RecordType.DeveloperName=&apos;CTS_Tech_Direct_Article_Feedback&apos;, Parent.RecordType.DeveloperName=&apos;CTS_Tech_Direct_Site_Feedback&apos;, Parent.RecordType.DeveloperName=&apos;Champion_TechKnowledgy_Case&apos;, Parent.RecordType.DeveloperName=&apos;CTS_RMS&apos;, Parent.RecordType.DeveloperName=&apos;GD_TechKnowledgy_Case&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_AIRPS_Case&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_ZEKS_Case&apos; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS TechDirect Email Message Sent</fullName>
        <actions>
            <name>CTS_OM_Set_Read_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Case_Number_of_Messages_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS TechDirect - Outbound Email has been sent from the Case. Uses existing OM fields</description>
        <formula>AND( $Permission.DataMigrationUser == False, ISPICKVAL( Status , &apos;Sent&apos;), OR( Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Ask_a_Question&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Issue_Escalation&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_Start_Up&apos;, Parent.RecordType.DeveloperName=&apos;CTS_Tech_Direct_Article_Feedback&apos;, Parent.RecordType.DeveloperName=&apos;CTS_Tech_Direct_Site_Feedback&apos;, Parent.RecordType.DeveloperName=&apos;Champion_TechKnowledgy_Case&apos;, Parent.RecordType.DeveloperName= &apos;CTS_RMS&apos;, Parent.RecordType.DeveloperName=&apos;GD_TechKnowledgy_Case&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_AIRPS_Case&apos;, Parent.RecordType.DeveloperName=&apos;CTS_TechDirect_ZEKS_Case&apos; ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Copy Most Recent Email Body</fullName>
        <actions>
            <name>Update_Full_Description_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an email message record is created related to a Case (Action Item), copy the body of that email message and populate the &apos;Full Case Description&apos; field on the Case (Action Item) record.</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND ( OR ( Parent.RecordType.DeveloperName = &quot;Action_Item&quot;,  Parent.RecordType.DeveloperName = &quot;Internal_Case&quot; ),  OR ( NOT(ISPICKVAL( Status , &apos;Forwarded&apos;) ),  NOT( ISPICKVAL( Status , &apos;Draft&apos;) ) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Action Item - Copy The Recent Email Body Into Original Email Body</fullName>
        <actions>
            <name>AI_Original_Email_Body_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an email message comes in and is attached to an action item, this workflow will copy the body of that email message and populate the Follow-Up field &apos;Email Body&apos; in case the user would like to include this in their correspondence.</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp;  Incoming &amp;&amp;  Parent.RecordType.DeveloperName = &quot;Action_Item&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email - Targeted Address</fullName>
        <actions>
            <name>Case_Email_Targeted_Address</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Cascades the &apos;Target Address&apos; from a Case Email up to the Case details page for automation logic.</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp;  NOT( ISNULL( ToAddress ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Case - Brand Field Update</fullName>
        <actions>
            <name>Brand_Field_Update_Part_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates the &apos;Brand&apos; field on the Case record when a case is created via &apos;Email to Case&apos;.</description>
        <formula>ISBLANK( Parent.Brand_Text__c ) ||  ISNULL(Parent.Brand_Text__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Case - Brand Field Update%09 2</fullName>
        <actions>
            <name>Brand_Field_Update_Pt_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISBLANK( Parent.Brand_Text__c ) || ISNULL(Parent.Brand_Text__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Case - Email Icon Displayed</fullName>
        <actions>
            <name>Email_Waiting_Checkbox_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an Email Message record is added to a case, this workflow checks the &apos;Email Waiting Icon&apos; checkbox that is used to add an email icon on list views.</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp;  Incoming == True</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Case - Product Category Field Update</fullName>
        <actions>
            <name>Product_Category_Field_Update_Part_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Updates the &apos;Product Category&apos; field on the Case record when a case is created via &apos;Email to Case&apos;.</description>
        <formula>ISBLANK( Parent.Brand_Text__c ) ||  ISNULL(Parent.Brand_Text__c)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Re-Open Case - Action Item</fullName>
        <actions>
            <name>Case_Status_Email_Waiting_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_open_Case_Checkbox_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the &apos;Re-open&apos; checkbox on the case and opens the case back up. (Action Items)</description>
        <formula>/* TEXT(Parent.Status) == &apos;Closed&apos;  &amp;&amp;  Incoming == TRUE  &amp;&amp;  Parent.RecordType.Name == &apos;Action Item - Locked&apos; */ $Permission.DataMigrationUser == False &amp;&amp; TEXT(Parent.Status) == &apos;Closed&apos; &amp;&amp; Incoming == TRUE &amp;&amp; OR( Parent.RecordType.Name == &apos;External Action Item - Locked&apos;, Parent.RecordType.Name == &apos;Internal Action Item - Locked&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Re-Open Case - Customer Cases</fullName>
        <actions>
            <name>Case_Status_Open_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Re_open_Case_Checkbox_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the &apos;Re-open&apos; checkbox on the case and opens the case back up. (Customer Cases)</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; TEXT(Parent.Status) == &apos;Resolved&apos; &amp;&amp; Incoming == TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Email Message - Set Case Status to %27Email Waiting%27</fullName>
        <actions>
            <name>Email_Waiting_Case_Status_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Action Item Status to &apos;Email Waiting&apos; when email is added to an Action Item.</description>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; Incoming &amp;&amp; (Parent.RecordTypeName__c == &apos;Action_Item&apos; &amp;&amp; TEXT(Parent.Status) &lt;&gt; &apos;Closed&apos; &amp;&amp; Not(BEGINS(Subject, &apos;Automatic reply&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update First Sent Email Date</fullName>
        <actions>
            <name>Update_First_Email_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND( ISPICKVAL(Status  , &quot;Sent&quot;), Was_Not_Auto_Acknowledgement__c == True, ISBLANK(Parent.First_Email_Sent__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Last Received Email Date</fullName>
        <actions>
            <name>Update_Last_Received_Email_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp;  ISPICKVAL(Status  , &quot;New&quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
