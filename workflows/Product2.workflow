<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Delete_Product_Request_Reject</fullName>
        <description>Delete Product Request Reject</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Request_Reject</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Active_to_TRUE</fullName>
        <description>Set the Product.Active field to TRUE</description>
        <field>IsActive</field>
        <literalValue>1</literalValue>
        <name>Set Active to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
