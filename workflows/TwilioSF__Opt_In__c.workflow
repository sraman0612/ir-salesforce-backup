<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>TwilioSF__Opt_In_Set_Deactivated_Date_Time</fullName>
        <field>TwilioSF__Opt_Out_Date_Time__c</field>
        <formula>Now()</formula>
        <name>Opt-In Set Deactivated Date Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Opt_In_Set_Deactivated_Date_Time_to_Null</fullName>
        <field>TwilioSF__Opt_Out_Date_Time__c</field>
        <name>Opt-In Set Deactivated Date Time to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>TwilioSF__Opt-In Made Inactive</fullName>
        <actions>
            <name>TwilioSF__Opt_In_Set_Deactivated_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>TwilioSF__Opt_In__c.TwilioSF__Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>TwilioSF__Opt-In Reactivated</fullName>
        <actions>
            <name>TwilioSF__Opt_In_Set_Deactivated_Date_Time_to_Null</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNEW()) &amp;&amp; PRIORVALUE(TwilioSF__Active__c) = false &amp;&amp; TwilioSF__Active__c = true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
