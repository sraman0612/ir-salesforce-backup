<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_Competitive_Quote</fullName>
        <ccEmails>CTS_competitive_quotes@irco.com</ccEmails>
        <description>CTS Competitive Quote</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Competitive_Quote_Email</template>
    </alerts>
</Workflow>
