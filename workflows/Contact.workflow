<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <alerts>
        <fullName>CTS_IOT_Notification_IR_Pending_Contact_Request</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Notification - IR (Pending Contact Request)</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_IOT_Community_Status_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Notification_IR_Pending_Contact_Request</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Notification_Super_User_Pending_Request</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Notification - Super User (Pending Request)</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_IOT_Community_Status_Approver__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Notification_Super_User_Pending_Request</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_Already_Available</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access Already Available</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_Already_Available</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_Approved_Existing_Asset_Contact</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access Approved (Existing Asset Contact)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_Approved_Existing_Asset_Contact</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_Approved_Existing_Contact_Different_Account</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access Approved (Existing Contact Different Account)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_Approved_Existing_Contact_Different_Account</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_Approved_New_Contact_on_Asset_Account</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access Approved (New Contact on Asset Account)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_Approved_New_Contact_on_Asset_Account</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_Approved_New_Lead_Reconciled</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access Approved (New Lead Reconciled)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_Approved_New_Lead_Reconciled</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_IR_Rejected_Access_Denied</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access IR Rejected (Access Denied)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_IR_Rejected_Access_Denied</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_IR_Rejected_Unknown_Identity</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access IR Rejected (Unknown Identity)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_IR_Rejected_Unknown_Identity</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_SU_Rejected_Access_Denied</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access SU Rejected (Access Denied)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_SU_Rejected_Access_Denied</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Access_SU_Rejected_Unknown_Identity</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Access SU Rejected (Unknown Identity)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Access_SU_Rejected_Unknown_Identity</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Additional_Site_Access_Approved_Existing_User</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration Additional Site Access Approved Existing User</description>
        <protected>false</protected>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Additional_Site_Access_Approved_Existing_User</template>
    </alerts>
    <alerts>
        <fullName>CTS_IOT_Registration_Received_Contact</fullName>
        <ccEmails>marc.powell@irco.com</ccEmails>
        <ccEmails>jan.pingel@irco.com</ccEmails>
        <description>CTS IOT Registration - Received (Contact)</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>irportalsupport@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_IOT_Community/CTS_IOT_Registration_Received_Contact</template>
    </alerts>
    <alerts>
        <fullName>Contact_Delete_Approved</fullName>
        <description>Contact: Delete Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Contact_Approval_Approved</template>
    </alerts>
    <alerts>
        <fullName>Delete_Contact_Request_Reject</fullName>
        <description>Delete Contact Request Reject</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Delete_Contact_Approval_Denied</template>
    </alerts>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Contact_RecType_Dir</fullName>
        <description>CTS Global Update Contact RecType Dir</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Contact RecType Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Contact_RecType_Ind</fullName>
        <description>CTS_Global_Update_Contact_RecType_Ind</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Indirect_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Contact RecType Ind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_IOT_Set_Community_Status_Date</fullName>
        <description>Sets the Community Status Date field to Now() whenever status date changes.  (Not able to add this field to Field History Tracking so tracking it inline via W/F and field update.)</description>
        <field>CTS_IOT_Community_Status_Date__c</field>
        <formula>Now()</formula>
        <name>CTS IOT - Set Community Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_IOT_Set_Community_Status_Update_By</fullName>
        <description>CTS Community - Sets the value/name of the user who updated the Status.</description>
        <field>CTS_IOT_Community_Status_Update_By__c</field>
        <formula>LastModifiedBy.FirstName + &apos; &apos; + LastModifiedBy.LastName</formula>
        <name>CTS IOT - Set Community Status Update By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Null_ErrorMessage</fullName>
        <field>Integration_Error_Message__c</field>
        <name>Contact_Null_ErrorMessage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_requestApproved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Delete_request_Approved</fullName>
        <field>Is_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Delete request Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IR_Contact_OBM</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>IR_Contact_OBM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IR_Contact_OBMDatetime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>IR_Contact_OBMDatetime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Sets the SBU ID Workflow field value for IBM Watson Marketing Automation</description>
        <field>SBU_ID_Workflow__c</field>
        <formula>IF (
RecordType.DeveloperName = &apos;CHVAC_APAC&apos;, &apos;CHVAC&apos;, 
IF(RecordType.DeveloperName = &apos;CC_GPSi_Contact&apos;, &apos;Club Car&apos;,
IF(RecordType.DeveloperName = &apos;CHVAC_Contact&apos;, &apos;CHVAC&apos;,
IF(RecordType.DeveloperName = &apos;Club_Car&apos;, &apos;Club Car&apos;, 
IF(RecordType.DeveloperName = &apos;Compressed_Air&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;Customer_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_AP_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_AP_Indirect_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_EU_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_EU_Indirect_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_LATAM_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_LATAM_Indirect_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_MEIA_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_MEIA_Indirect_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;CTS_Global_Distributor_Contact&apos;, &apos;CTS&apos;,
IF(RecordType.DeveloperName = &apos;PT_powertools&apos;, &apos;Power Tools&apos;,
IF(RecordType.DeveloperName = &apos;GES_Contact&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;Hibon_Contact&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;India_CS_Contact&apos;, &apos;CHVAC&apos;, 
IF(RecordType.DeveloperName = &apos;OEM_Contact&apos;, &apos;CTS&apos;, 
IF(RecordType.DeveloperName = &apos;RS_CRS_Contact&apos;, &apos;RHVAC&apos;, 
IF(RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;, 
IF(RecordType.DeveloperName = &apos;Thermo_King_Marine&apos;, &apos;TK&apos;, 
&apos;None&apos;))))))))))))))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Data_com_Clean_Status_Skipped_Upd</fullName>
        <field xsi:nil="true"/>
        <literalValue>Skipped</literalValue>
        <name>PT_Data_com_Clean_Status_Skipped_Upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PT_Set_UID</fullName>
        <field>PT_UID__c</field>
        <formula>Email</formula>
        <name>PT Set UID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Skipped_Clean_Status</fullName>
        <field xsi:nil="true"/>
        <literalValue>Skipped</literalValue>
        <name>Set Skipped Clean Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status</fullName>
        <field>TwilioSF__Last_Message_Status__c</field>
        <name>Clear Last Message Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TwilioSF__Clear_Last_Message_Status_Date</fullName>
        <field>TwilioSF__Last_Message_Status_Date__c</field>
        <name>Clear Last Message Status Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateAfterRejection</fullName>
        <field>Request_Delete__c</field>
        <literalValue>0</literalValue>
        <name>UpdateAfterRejection</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_DateTime</fullName>
        <field>Integration_OBM_DateTime__c</field>
        <formula>Now()</formula>
        <name>Update Integration DateTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Integration_Status</fullName>
        <field>Integration_Status__c</field>
        <literalValue>In Transit</literalValue>
        <name>Update Integration Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>IR_Contact_OBM_PROD</fullName>
        <apiVersion>33.0</apiVersion>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_Contact_OBM_PROD</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>CTS Global Update Contact RecType Dir</fullName>
        <actions>
            <name>CTS_Global_Update_Contact_RecType_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Contact RecType Dir</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,ISPICKVAL(CG_Org_channel__c,&quot;EMEIA&quot;),  ISPICKVAL(Account.CTS_Channel__c, &apos;DIRECT&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update Contact RecType Ind</fullName>
        <actions>
            <name>CTS_Global_Update_Contact_RecType_Ind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Contact RecType Indirect</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(  Account.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;,ISPICKVAL(CG_Org_channel__c,&quot;EMEIA&quot;),  ISPICKVAL(Account.CTS_Channel__c, &apos;DISTRIBUTOR&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT - Set Community Status Date</fullName>
        <actions>
            <name>CTS_IOT_Set_Community_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>CTS_IOT_Set_Community_Status_Update_By</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Update the Community Status Date field upon Status changes</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISCHANGED(CTS_IOT_Community_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Notification - IR %28Pending Contact Request%29</fullName>
        <actions>
            <name>CTS_IOT_Notification_IR_Pending_Contact_Request</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Notification - IR (Pending Contact Request)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Pending Review&apos;), CTS_IOT_Community_Status_Approver_Type__c = &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Notification - Super User %28Pending Request%29</fullName>
        <actions>
            <name>CTS_IOT_Notification_Super_User_Pending_Request</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Notification - Super User (Pending Request)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Pending Review&apos;), CTS_IOT_Community_Status_Approver_Type__c &lt;&gt; &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access Already Available</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_Already_Available</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access Already Available</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;), ISPICKVAL(CTS_IOT_Match_Type__c, &apos;1 - Access Already Available&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access Approved %28Existing Asset Contact%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_Approved_Existing_Asset_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access Approved (Existing Asset Contact)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;), ISPICKVAL(CTS_IOT_Match_Type__c, &apos;2 - Access Approved (Existing Asset Contact)&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access Approved %28Existing Contact Different Account%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_Approved_Existing_Contact_Different_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access Approved (Existing Contact Different Account)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;), ISPICKVAL(CTS_IOT_Match_Type__c, &apos;4 - Access Approved (Existing Contact Different Account)&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access Approved %28New Contact on Asset Account%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_Approved_New_Contact_on_Asset_Account</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access Approved (New Contact on Asset Account)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;), ISPICKVAL(CTS_IOT_Match_Type__c, &apos;5 - Access Approved (New Contact on Asset Account)&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access Approved %28New Lead Reconciled%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_Approved_New_Lead_Reconciled</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access Approved (New Lead Reconciled)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;), ISPICKVAL(CTS_IOT_Match_Type__c, &apos;6 - Access Approved (New Lead Reconciled)&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access IR Rejected %28Access Denied%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_IR_Rejected_Access_Denied</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access IR Rejected (Access Denied)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Rejected&apos;), ISPICKVAL(CTS_IOT_Community_Reject_Reason__c, &apos;Access Denied&apos;), CTS_IOT_Community_Status_Approver_Type__c = &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access IR Rejected %28Unknown Identity%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_IR_Rejected_Unknown_Identity</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access IR Rejected (Unknown Identity)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Rejected&apos;), ISPICKVAL(CTS_IOT_Community_Reject_Reason__c,&apos;Unknown Identity&apos;), CTS_IOT_Community_Status_Approver_Type__c = &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access SU Rejected %28Access Denied%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_SU_Rejected_Access_Denied</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access SU Rejected (Access Denied)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Rejected&apos;), ISPICKVAL(CTS_IOT_Community_Reject_Reason__c, &apos;Access Denied&apos;), CTS_IOT_Community_Status_Approver_Type__c &lt;&gt; &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Access SU Rejected %28Unknown Identity%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Access_SU_Rejected_Unknown_Identity</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Access SU Rejected (Unknown Identity)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Rejected&apos;), ISPICKVAL(CTS_IOT_Community_Reject_Reason__c,&apos;Unknown Identity&apos;), CTS_IOT_Community_Status_Approver_Type__c &lt;&gt; &apos;Internal IR&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Additional Site Access Approved %28Existing User%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Additional_Site_Access_Approved_Existing_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Additional Site Access Approved (Existing User)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Approved&apos;),ISPICKVAL(CTS_IOT_Match_Type__c, &apos;3 - Additional Site Access Approved (Existing User)&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS IOT Registration - Received %28Contact%29</fullName>
        <actions>
            <name>CTS_IOT_Registration_Received_Contact</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>CTS IOT Registration - Received (Contact)</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(CTS_IOT_Community_Status__c, &apos;Pending Review&apos;), NOT(ISPICKVAL(CTS_IOT_Match_Type__c,&apos;6 - Access Approved (New Lead Reconciled)&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact_Null_ErrorMessage</fullName>
        <actions>
            <name>Contact_Null_ErrorMessage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND($Permission.DataMigrationUser == FALSE, ISPICKVAL(Integration_Status__c,&apos;Success&apos;), OR(RecordType.Name = &apos;Customer Contact&apos;, RecordType.Name = &apos;Compressed Air&apos;), ISPICKVAL(CG_Org_channel__c, &apos;EMEIA&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>IR_Contact_OBM_CTS_GLOBAL</fullName>
        <actions>
            <name>Update_Integration_DateTime</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Integration_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>IR_Contact_OBM_PROD</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to send Contact Outbound notifications to Siebel via Fusion</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; IF(AND(ISNEW(), OR( RecordType.DeveloperName == &quot;CTS_EU_Contact&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect_Contact&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM_Contact&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect_Contact&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA_Contact&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect_Contact&quot; , RecordType.DeveloperName == &quot;End_Customer&quot;),NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot; ),true, IF(AND(NOT(ISNEW()),OR( RecordType.DeveloperName == &quot;CTS_EU_Contact&quot;,RecordType.DeveloperName == &quot;CTS_EU_Indirect_Contact&quot;,AND(RecordType.DeveloperName == &quot;CTS_LATAM_Contact&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;),AND(RecordType.DeveloperName == &quot;CTS_LATAM_Indirect_Contact&quot;,UPPER(Account.ShippingCountry) = &quot;CHILE&quot;), RecordType.DeveloperName == &quot;CTS_MEIA_Contact&quot;, RecordType.DeveloperName == &quot;CTS_MEIA_Indirect_Contact&quot;, RecordType.DeveloperName == &quot;End_Customer&quot;), NOT( $Setup.IR_API_Bypass__c.IR_Validation_Bypass__c ), NOT( Is_Approved__c ), $User.LastName&lt;&gt;&quot;Clean&quot;, Owner.LastName&lt;&gt;&quot;Integration&quot;, Owner.LastName&lt;&gt;&quot;Migration&quot;, OR(ISCHANGED(AssistantName),ISCHANGED(Asst_Email__c),ISCHANGED(AssistantPhone),ISCHANGED(Birthdate), ISCHANGED(MobilePhone),ISCHANGED(Email),ISCHANGED(ASR_Email__c),ISCHANGED(FirstName),ISCHANGED(Title), ISCHANGED(LastName),ISCHANGED(Salutation),ISCHANGED(Phone),ISCHANGED(Business_Ext__c),ISCHANGED(AccountId), ISCHANGED(Integration_Status__c),ISCHANGED(Status__c),ISCHANGED(Integration_Error_Message__c) )),true,false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Contact</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sets the SBU ID Workflow field value for IBM Watson Marketing Automation</description>
        <formula>AND($Permission.DataMigrationUser == FALSE, Contains(SBU_ID_Workflow__c, &apos;&apos;), ISPICKVAL(CG_Org_channel__c,&apos;EMEIA&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PT Email Create or Update</fullName>
        <actions>
            <name>PT_Set_UID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( 	RecordType.DeveloperName =&quot;PT_powertools&quot;,ISPICKVAL(CG_Org_channel__c,&quot;EMEIA&quot;), 	OR( 		AND( 			ISNEW(), 			NOT(ISBLANK(Email)) 		), 		AND( 			NOT(ISNEW()), 			ISCHANGED(Email) 		) 	) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TwilioSF__Clear Last Message Status%2FDate</fullName>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>TwilioSF__Clear_Last_Message_Status_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>1!=1</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
