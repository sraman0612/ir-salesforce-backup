<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_TechDirect_Email_for_Rejected_Articles</fullName>
        <ccEmails>TimothyA.Hall@irco.com</ccEmails>
        <description>CTS TechDirect Email for Rejected Articles</description>
        <protected>false</protected>
        <recipients>
            <field>CTS_TechDirect_Author_Reviewer__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc_no_reply@irco.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_Article_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_to_Chatter_group</fullName>
        <ccEmails>0F90a000000g3NpCAI@post.j-1nuezeai.na54.chatter.salesforce.com</ccEmails>
        <ccEmails>timothya.hall@irco.com</ccEmails>
        <description>Email alert to Chatter group</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>CTS_TechDirect/CTS_TechDirect_New_Article</template>
    </alerts>
    <fieldUpdates>
        <fullName>CTS_TD_Update_Custom_Version_Number</fullName>
        <field>CTS_Custom_Version_Number__c</field>
        <formula>IF(ISBLANK(CTS_Custom_Version_Number__c), VersionNumber, CTS_Custom_Version_Number__c + 1)</formula>
        <name>CTS TD Update Custom Version Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_TechDirect_Remove_SME_Approval</fullName>
        <description>SME Approval Required = false</description>
        <field>CTS_TechDirect_SME_Approval_Required__c</field>
        <literalValue>0</literalValue>
        <name>CTS TechDirect Remove SME Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_TechDirect_Update_Proposed_Status</fullName>
        <field>ValidationStatus</field>
        <literalValue>Proposed</literalValue>
        <name>Update Proposed Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_TechDirect_Update_Status_To_Proposed</fullName>
        <description>Update validation status to Proposed.</description>
        <field>ValidationStatus</field>
        <literalValue>Proposed</literalValue>
        <name>Update Status To Proposed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_TechDirect_Update_Status_To_Publish</fullName>
        <field>ValidationStatus</field>
        <literalValue>Publish</literalValue>
        <name>Update Status To Publish</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_TechDirect_Update_Status_to_Rework</fullName>
        <field>ValidationStatus</field>
        <literalValue>Rework</literalValue>
        <name>Update Status to Rework</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposed_False</fullName>
        <field>CTS_TechDirect_Proposed__c</field>
        <literalValue>0</literalValue>
        <name>Proposed = False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proposed_True</fullName>
        <field>CTS_TechDirect_Proposed__c</field>
        <literalValue>1</literalValue>
        <name>Proposed = True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <knowledgePublishes>
        <fullName>CTS_TechDirect_Knowledge_Publish</fullName>
        <action>Publish</action>
        <description>This action is created only for the Articles migrating from Oracle Service cloud.</description>
        <label>CTS TechDirect Knowledge Publish</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>CTS_TechDirect_Publish_Current_Version_Article</fullName>
        <action>Publish</action>
        <description>To publish article as current version</description>
        <label>CTS Publish Current Version Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>CTS_TechDirect_Publish_New_Version_Article</fullName>
        <action>PublishAsNew</action>
        <description>To publish article as new version</description>
        <label>CTS_TechDirect Publish New Version Article</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <knowledgePublishes>
        <fullName>Club_Car_Publish_New_Version</fullName>
        <action>PublishAsNew</action>
        <description>Publish the new article</description>
        <label>Club Car Publish New Version</label>
        <language>en_US</language>
        <protected>false</protected>
    </knowledgePublishes>
    <rules>
        <fullName>CTS_TechDirect_Migrated_Article_Publish</fullName>
        <actions>
            <name>CTS_TechDirect_Knowledge_Publish</name>
            <type>KnowledgePublish</type>
        </actions>
        <active>true</active>
        <description>This workflow is created for the migrated articles from oracle service cloud.This will set &apos;Publish Status&apos; as Publish for the migrated Articles.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(BEGINS( $User.Username , &apos;TechDirect&apos;), ISPICKVAL(ValidationStatus ,&apos;Publish&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
