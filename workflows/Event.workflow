<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_Event_Notification</fullName>
        <description>CTS Event Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Event_Notification_revised</template>
    </alerts>
    <fieldUpdates>
        <fullName>Activity_Date</fullName>
        <field>Activity_Date__c</field>
        <formula>IF( 

NOT( ISBLANK(ActivityDate ) ) , 

DATE (
YEAR ( ActivityDate ),
MONTH( ActivityDate ),
DAY( ActivityDate ) ), 

NULL )</formula>
        <name>Activity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CC_Set_Completion_Date</fullName>
        <field>CC_Visage_Demo_Date__c</field>
        <formula>NOW()</formula>
        <name>Set Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update Activity Date %28Event%29</fullName>
        <actions>
            <name>Activity_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; OR( ISBLANK( Activity_Date__c ) , ISCHANGED( ActivityDate ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
