<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Order_Summary_Email_Notification_Alert</fullName>
        <ccEmails>Welch.NA@irco.com</ccEmails>
        <description>Order Summary Email Notification Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>welch.na@gardnerdenver.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Ecommerce/Order_Summary_Email_Template</template>
    </alerts>
</Workflow>
