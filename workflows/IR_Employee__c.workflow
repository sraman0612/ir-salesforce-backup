<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Format_Business_Cell_Phone</fullName>
        <description>Format business cell phone with North America formatting (xxx) xxx-xxxx.</description>
        <field>Business_Cell_Phone__c</field>
        <formula>IF(LEN(Business_Cell_Phone__c)&gt;10,
&quot;(&quot; &amp; MID(Business_Cell_Phone__c,2,3) &amp; &quot;) &quot; &amp; MID(Business_Cell_Phone__c,5,3) &amp; &quot;-&quot; &amp; MID(Business_Cell_Phone__c,8,4),
&quot;(&quot; &amp; MID(Business_Cell_Phone__c,1,3) &amp; &quot;) &quot; &amp; MID(Business_Cell_Phone__c,4,3) &amp; &quot;-&quot; &amp; MID(Business_Cell_Phone__c,7,4)
)</formula>
        <name>Format Business Cell Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Format_Office_Phone</fullName>
        <description>Format office phone with North America formatting (xxx) xxx-xxxx.</description>
        <field>Office_Phone_Number__c</field>
        <formula>IF(LEN(Office_Phone_Number__c)&gt;10,
   &quot;(&quot; &amp; MID(Office_Phone_Number__c,2,3) &amp; &quot;) &quot; &amp; MID(Office_Phone_Number__c,5,3) &amp; &quot;-&quot; &amp; MID(Office_Phone_Number__c,8,4),
   &quot;(&quot; &amp; MID(Office_Phone_Number__c,1,3) &amp; &quot;) &quot; &amp; MID(Office_Phone_Number__c,4,3) &amp; &quot;-&quot; &amp; MID(Office_Phone_Number__c,7,4)
)</formula>
        <name>Format Office Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Unformatted Business Cell Phone</fullName>
        <actions>
            <name>Format_Business_Cell_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule fires when a record is created or updated and the business cell phone is not formatted.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(   ISNUMBER(Business_Cell_Phone__c),   OR(LEN(Business_Cell_Phone__c)=10,LEN(Business_Cell_Phone__c)=11 &amp;&amp; LEFT(Business_Cell_Phone__c,1)=&quot;1&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Unformatted Office Phone</fullName>
        <actions>
            <name>Format_Office_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule fires when a record is created or updated and the office phone is not formatted.</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND(   ISNUMBER(Office_Phone_Number__c),   OR(LEN(Office_Phone_Number__c)=10,LEN(Office_Phone_Number__c)=11 &amp;&amp; LEFT(Office_Phone_Number__c,1)=&quot;1&quot; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
