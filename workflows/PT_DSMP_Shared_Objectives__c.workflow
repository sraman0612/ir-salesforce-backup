<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PT_DSMP_FU_UpdateName</fullName>
        <field>Name</field>
        <formula>Text__c</formula>
        <name>PT_DSMP_FU_UpdateName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>PT_DSMP_WF_SharedObj_UpdateName</fullName>
        <actions>
            <name>PT_DSMP_FU_UpdateName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == False &amp;&amp; AND(  OR( ISNEW(), ISCHANGED( Text__c ) ),  !ISNULL(Text__c)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
