<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>CC_ERP_INVENTORY_TRX</fullName>
        <apiVersion>45.0</apiVersion>
        <description>Used to send inventory transactions (currently transfer and consume on work order) to Club Car ERP (currently MAPICS).</description>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/MapicsInventoryFSLReqABCSImpl/ProxyServices/MapicsInventoryFSLReqABCSImpl_PS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>CC_ERP_Item_Number__c</fields>
        <fields>CC_Inventory_Trx_Type__c</fields>
        <fields>CreatedDate</fields>
        <fields>Destination_Location__c</fields>
        <fields>Destination_Warehouse__c</fields>
        <fields>Id</fields>
        <fields>Quantity</fields>
        <fields>Source_Location__c</fields>
        <fields>Source_Warehouse__c</fields>
        <fields>Work_Order_Number__c</fields>
        <fields>Work_Order__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>benjamin.lorenz@irco.com</integrationUser>
        <name>CC ERP INVENTORY TRX</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
