<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CTS_Global_Update_Activity_RecType_Dir</fullName>
        <description>CTS Global Update Activity RecType Direct</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Service_Activity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Activity RecType Dir</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CTS_Global_Update_Activity_RecType_Ind</fullName>
        <description>CTS Global Update Activity RecType Indirect</description>
        <field>RecordTypeId</field>
        <lookupValue>CTS_MEIA_Ind_Service_Activity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CTS Global Update Activity RecType Ind</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Automation_Set_SBU_ID_WF</fullName>
        <description>Sets the SBU ID Workflow field value for IBM Watson Marketing Automation</description>
        <field>SBU_ID_Workflow__c</field>
        <formula>IF( 
Account__r.RecordType.DeveloperName = &apos;Thermo_King_Marine&apos;, &apos;TK&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;Site_Account_NA_Air&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_AP&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_AP_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_EU&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_EU_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_LATAM&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_LATAM_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_MEIA&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;CTS_MEIA_INDIRECT&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;GES_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;Hibon_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;OEM_Account&apos;, &apos;CTS&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;RS_HVAC&apos;, &apos;RHVAC&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;Thermo_King_Strategic&apos;, &apos;TK&apos;, 
IF(Account__r.RecordType.DeveloperName = &apos;PT_powertools&apos;, &apos;Power Tools&apos;,
IF( BEGINS(Account__r.RecordType.DeveloperName, &apos;Club Car&apos;), &apos;Club Car&apos;,
&apos;&apos;))))))))))))))))
)</formula>
        <name>Marketing Automation - Set SBU ID WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Global Update Activity RecType Dir</fullName>
        <actions>
            <name>CTS_Global_Update_Activity_RecType_Dir</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Activity RecType Direct</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;, ISPICKVAL(Account__r.CTS_Channel__c, &apos;DIRECT&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CTS Global Update Activity RecType Ind</fullName>
        <actions>
            <name>CTS_Global_Update_Activity_RecType_Ind</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>CTS Global Update Activity RecType Indirect</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; AND( Account__r.Division__c = &apos;DUBAI SALES AND SERVICE CENTRE ORG&apos;, ISPICKVAL(Account__r.CTS_Channel__c, &apos;DISTRIBUTOR&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Marketing Automation - Set SBU ID on Svc Activity</fullName>
        <actions>
            <name>Marketing_Automation_Set_SBU_ID_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used for IBM Watson marketing automation processes to indicate the SBU associated with the record</description>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; SBU_ID_Workflow__c == &apos;&apos;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
