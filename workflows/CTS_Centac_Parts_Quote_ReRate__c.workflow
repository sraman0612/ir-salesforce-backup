<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CTS_Centac_Part_Quote_Approval_Mail_24_Hour_Overdue</fullName>
        <description>CTS Centac Part Quote Approval Mail 24 Hour Overdue</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Centac_Part_Quote_Approval_Mail_24_Hour_Overdue</template>
    </alerts>
    <alerts>
        <fullName>CTS_Centac_Part_Quote_Request_Received</fullName>
        <ccEmails>centac&amp;cacustcare@irco.com</ccEmails>
        <description>CTS Centac Part Quote Request Received Machine Down</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Centac_Parts_Quote_Created_Machine_Down</template>
    </alerts>
    <alerts>
        <fullName>CTS_Centac_Part_Quote_Submitted_for_Approval</fullName>
        <ccEmails>centac&amp;cacustcare@irco.com</ccEmails>
        <description>CTS Centac Part Quote Submitted for Approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>datamigration@irco.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Centac_Part_Quote_Approval_Mail</template>
    </alerts>
    <alerts>
        <fullName>CTS_Centac_Parts_Quote_Request_Updated</fullName>
        <description>CTS Centac Parts Quote Request Updated</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Centac_Parts_Quote_Attached</template>
    </alerts>
    <alerts>
        <fullName>CTS_Rejected_Centac_Part_Quote_Notification_15_days_Overdue</fullName>
        <description>CTS Rejected Centac Part Quote Notification 15 days Overdue</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Rejected_Centac_Part_Quote_Notification_15_days_Overdue</template>
    </alerts>
    <alerts>
        <fullName>CTS_Rejected_Centac_Part_Quote_Request_Notification</fullName>
        <description>CTS Rejected Centac Part Quote Request Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Rejected_Centac_Part_Quote_Notification</template>
    </alerts>
    <alerts>
        <fullName>Centac_Part_Quote_Request_Approved</fullName>
        <description>Centac Part Quote Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CTS_Approved_Centac_Part_Quote_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Approval_Status</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Approval Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Status_to_Submit_for_App</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Update Approval Status to Submit for App</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Quote_Requested</fullName>
        <field>Quote_Status__c</field>
        <literalValue>Quote Request Received</literalValue>
        <name>Update Quote Status to Quote Requested</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Rejected</fullName>
        <field>Quote_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Quote Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Quote_Status_to_Work_In_Progress</fullName>
        <field>Quote_Status__c</field>
        <literalValue>Work In Progress</literalValue>
        <name>Update Quote Status to Work In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CTS Centac Part Quote Created Machine Down</fullName>
        <actions>
            <name>CTS_Centac_Part_Quote_Request_Received</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>CTS_Centac_Parts_Quote_ReRate__c.Quote_Status__c</field>
            <operation>equals</operation>
            <value>Quote Request Received</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Centac Part Quote Submitted For Approval</fullName>
        <actions>
            <name>CTS_Centac_Part_Quote_Submitted_for_Approval</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Approval_Status__c,&apos;Submitted for Approval&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CTS Centac Parts Quote Request Updated</fullName>
        <actions>
            <name>CTS_Centac_Parts_Quote_Request_Updated</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>$Permission.DataMigrationUser == FALSE &amp;&amp; ISPICKVAL(Quote_Status__c , &apos;Quote Completed&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
