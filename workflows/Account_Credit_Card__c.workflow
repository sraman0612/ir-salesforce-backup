<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>IR_Account_CreditCard_OBM_CTS</fullName>
        <apiVersion>46.0</apiVersion>
        <endpointUrl>https://icosbomwprd.ingersollrand.com/CRM_SF/SalesforceNotifyOutbound/ProxyService/SalesForceCommonProvABCSImplPS?username=IRPrtnrPr0dSalesforce&amp;password=IRPrtnrPr0dWtxKmWGWGK</endpointUrl>
        <fields>Fusion_OBM_Routing_Code__c</fields>
        <fields>Id</fields>
        <fields>RecordTypeName__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>siebelintegration@irco.com</integrationUser>
        <name>IR_Account_CreditCard_OBM_CTS</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>IR_Account_CreditCard_OBM_CTS</fullName>
        <actions>
            <name>IR_Account_CreditCard_OBM_CTS</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISNULL( Account__r.Siebel_ID__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
