import { LightningElement, api } from 'lwc';

export default class ToastMessage extends LightningElement {

    @api message;
    @api messageType; // Valid values are error, warning, success

    get themeClasses(){
        return "slds-notify slds-notify_toast slds-theme_" + this.messageType;
    }

    get messageIcon(){
        return "utility:" + this.messageType;
    }
}