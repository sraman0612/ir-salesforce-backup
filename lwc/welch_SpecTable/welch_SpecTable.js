import { LightningElement, api } from 'lwc';
import getSpecData from '@salesforce/apex/welch_SpecTableController.getSpecData';
import getFieldNameValues from '@salesforce/apex/welch_SpecTableController.getFieldNameValues';
export default class Welch_SpecTable extends LightningElement {
    @api productId;
    
    isLoading = false;
    columns = [];
    data;
    rows = [];
    fieldNames = [];
    parentSKUs = [];
    products = [];
    tempData = [];
    error;

    // @wire(getFieldNameValues, {objectApiName: 'Account', fieldApiName: 'Industry' })
    // wiredIndustryOptions({ error, data }) {

    //     if (data) {

    //         this.industryPicklistOptions = [{label:"All", value: ""}, ...JSON.parse(data)];

    //     } else if (error) {

    //         console.error(error);

    //         this.error = error;

    //     }

    // }

    connectedCallback(){
        console.log('Product Id:: ' + this.productId);
        this.columns.push({label:'Model Number',fieldName:'specName',hideDefaultActions:true, wrapText:true});
        this.isLoading = true;
        getSpecData({productId:this.productId})
        .then(result =>{
            console.log('THIS IS THE DATA RETURNED \n', result);
            //loop through and take out data
            this.data = result;
            this.error = undefined;

            // this.data.forEach(element => {
            //     if(!parentSKUs.includes(element.StockKeepingUnit)){
            //         this.products.push(element);
            //         parentSKUs.push(element.StockKeepingUnit);
            //         this.columns.push({label:element.StockKeepingUnit, fieldName:element.StockKeepingUnit, hideDefaultActions:true});
            //     }
            // });

            // const resultBased = Object.assign(...Array.from(
            //     new Set(this.data.reduce((keys, o) => keys.concat(Object.keys(o)), [] )),
            //     key => ({ [key]: this.data.map( o => o[key] ) })
            // ));
            // resultBased.fieldLabels = this.fieldNames;
            // console.log(resultBased);
            // this.tempData = [...Object.values(resultBased)];
            // this.tempData.forEach((e,index)=>{
            //     e.unshift(this.fieldNames[index]);
            // })
         
            // this.columns = this.parentSKUs.filter(n => n);
            // this.columns.unshift('spec');
            this.filterProducts();

            //call method to make the data into the table i want
            
        })
        .catch(error =>{
            this.error = error;
            this.isLoading = false;
            console.error('**** error **** \n ',this.error);
        });
    }

    filterProducts(){
        this.data.forEach(element => {

            if(!this.parentSKUs.includes(element.StockKeepingUnit)){
                this.products.push(element);
                this.parentSKUs.push(element.StockKeepingUnit);
                this.columns.push({label:element.StockKeepingUnit, fieldName:element.StockKeepingUnit, hideDefaultActions:true, wrapText:true});
            }
        });
        console.log(this.parentSKUs);
        console.log('PORODUCTS ::');
        console.log(this.products);
        this.getProductFieldNames();
    }

    getProductFieldNames(){
        getFieldNameValues({prods:this.products})
        .then(result =>{
            this.fieldNames = result;
            console.log(this.fieldNames);
            this.makeTable();
        }).catch(error =>{
            this.error = error;
            this.isLoading = false;
            console.error('**** error **** \n ',this.error);
        });
        
    }

    //for how many columns
    //add that many arrays to table obj
    //tableColumn = data.size() + 1;
    makeTable(){
        
        for(let spec of this.fieldNames){
            let productIndex = 0;
            let currrow = [];
            let rowTest = {};
            let dontInclude = false;
            //wrapper class of label and apiName
            for(let product of this.products){
                if(!currrow.includes(spec.label)){
                    // if(product[spec.apiName] != null){
                    currrow.push(spec.label);
                    rowTest.specName = spec.label;
                    // }
                    if(productIndex < 1){
                        if(product[spec.apiName] == undefined){
                            currrow.push('-');
                            rowTest[product.StockKeepingUnit] = '-';
                        }
                        else if(product[spec.apiName] == true){
                            currrow.push('Yes');
                            rowTest[product.StockKeepingUnit] = 'Yes';
                        }
                        else if(product[spec.apiName] == false){
                            currrow.push('No');
                            rowTest[product.StockKeepingUnit] = 'No';
                        }
                        else if(product[spec.apiName] != null){
                            currrow.push(product[spec.apiName]);
                            rowTest[product.StockKeepingUnit] = product[spec.apiName];
                        }
                        
                        productIndex++;
                    }
                }
                else{
                    if(spec.apiName == 'StockKeepingUnit' || spec.apiName == 'Name'){
                        dontInclude = true;
                    }
                    if(product[spec.apiName] == undefined){
                        currrow.push('-');
                        rowTest[product.StockKeepingUnit] = '-';
                    }
                    else if(product[spec.apiName] == true){
                        currrow.push('Yes');
                        rowTest[product.StockKeepingUnit] = 'Yes';
                    }
                    else if(product[spec.apiName] == false){
                        currrow.push('No');
                        rowTest[product.StockKeepingUnit] = 'No';
                    }
                    else if(product[spec.apiName] != null){
                        currrow.push(product[spec.apiName]);
                        rowTest[product.StockKeepingUnit] = product[spec.apiName];
                    }
                    
                    else{
                        dontInclude = true;
                        break;
                    }
                }
                
            }
            if(!dontInclude){
                this.rows.push(rowTest);
            }    
        }
        this.isLoading = false;
        //array or array
        //have wrapper class with only 1 field?
        //then create a collection where i have a list of wrapper class
        //add values to that list?
    }

    //method to get populated field names

    //wire in field names
    //wire list of products
    // columns.push({label:'',fieldName:'specName',hideDefaultActions:true})
    // for(let prod of results){
    //     this.columns.push({label:prod.parentSKUs,fieldName:prod.parentSku,hideDefaultActions:true})
    // }
}