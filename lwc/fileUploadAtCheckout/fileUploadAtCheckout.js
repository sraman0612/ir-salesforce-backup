import {api, wire, LightningElement} from 'lwc';

import { CartSummaryAdapter } from "commerce/cartApi";
import { useCheckoutComponent } from 'commerce/checkoutApi';

import Supported_formats from '@salesforce/label/c.Supported_formats';
import Drag_and_drop_a_file from '@salesforce/label/c.Drag_and_drop_a_file';
import Maximum_file_size from '@salesforce/label/c.Maximum_file_size';
import Uploaded_successfully from '@salesforce/label/c.Uploaded_successfully';
import File_Size_Error from '@salesforce/label/c.File_Size_Error';
import Upload_PO_document from '@salesforce/label/c.Upload_PO_document';
import Browse from '@salesforce/label/c.Browse';
import Please_upload_a_PO_document from '@salesforce/label/c.Please_upload_a_PO_document';

import getCartDetails from '@salesforce/apex/FileUploadAtCheckoutController.getCartDetails';
import uploadFile from '@salesforce/apex/FileUploadAtCheckoutController.uploadFile'
const PO_CREDIT = 'PO (CREDIT)';

const CheckoutStage = {
    CHECK_VALIDITY_UPDATE: 'CHECK_VALIDITY_UPDATE',
    REPORT_VALIDITY_SAVE: 'REPORT_VALIDITY_SAVE',
    BEFORE_PAYMENT: 'BEFORE_PAYMENT',
    PAYMENT: 'PAYMENT',
    BEFORE_PLACE_ORDER: 'BEFORE_PLACE_ORDER',
    PLACE_ORDER: 'PLACE_ORDER'
};
export default class fileUploadAtCheckout extends useCheckoutComponent(LightningElement) {
    @api recordId;
    @api error;
    @api uploadAmountRequirement;
    cartId;
    showUploadComponent = false;
    showError = false;
    fileUploaded = false;
    fileUploadedSuccessMessage;
    fileData;
    showSpinner = false;

    labels = {
        Supported_formats,
        Maximum_file_size,
        Drag_and_drop_a_file,
        Uploaded_successfully,
        File_Size_Error,
        Upload_PO_document,
        Browse,
        Please_upload_a_PO_document
    }
    @wire(CartSummaryAdapter)
    setCartSummary({ data, error }) {
        if (data) {
            //console.log("Cart Id", data.cartId);
            this.cartId = data.cartId;
            //this.getAmountFromCart();
            this.fetchCartDetails();
        } else if (error) {
            console.error('Could not get the cart data:'+ JSON.stringify(error));
        }
    }

    fetchCartDetails() {
        getCartDetails({ cartId: this.cartId })
            .then(result => {
                if (result.totalAmount > this.uploadAmountRequirement && result.paymentOption.toUpperCase().includes(PO_CREDIT)) {
                    this.showUploadComponent = true;
                }
            })
            .catch(error => {
                console.error('Error fetching cart details:', error);
            });
    }

    openfileUpload(event) {
        const file = event.target.files[0];
        this.uploadFileToSalesforce(file);
    }

    allowDrop(ev) {
        ev.preventDefault();
    }

    dropped(event) {
        //console.log('on drop ');
        event.preventDefault();
        const file = event.dataTransfer.files[0];
        this.uploadFileToSalesforce(file);
    }

    uploadFileToSalesforce(file) {
        this.showSpinner = true;
        let reader = new FileReader();
        reader.onload = () => {
            let _base64 = reader.result.split(',')[1];
            if ((_base64.length)/1024 > 7000) {
                this.showError = true;
                this.showSpinner = false;
                this.error = this.labels.File_Size_Error;
            }
            else {
                this.fileData = {
                    'filename': file.name,
                    'base64': _base64,
                    'recordId': this.cartId
                }
                const {base64, filename, recordId} = this.fileData;
                uploadFile({base64, filename, recordId}).then(result => {
                    this.fileData = null;
                    this.fileUploadedSuccessMessage = `${filename} ` + this.labels.Uploaded_successfully;
                    // console.log(this.fileUploadedSuccessMessage);
                    this.fileUploaded = true;
                    this.showError = false;
                    this.showSpinner = false;
                }).catch((error) => {
                    this.showSpinner = false;
                    console.log("File could not be uploaded -" + JSON.stringify(error));
                })
            }
        }
        reader.readAsDataURL(file);
    }

    //used to determine whether the component is visible or not
    //the visibility of the component is controlled by a script on the head markup of the site
    //when the payment method is credit card, the component is visible, otherwise, it's not and thus the validations of this component are not required
    get isComponentVisible() {
        return this.refs?.main?.checkVisibility({
            visibilityProperty: true,
        });
    }

    /**
     * update form when our container asks us to
     */
    stageAction(checkoutStage /*CheckoutStage*/) {
        if (this.showUploadComponent && this.isComponentVisible) {
            switch (checkoutStage) {
                case CheckoutStage.CHECK_VALIDITY_UPDATE:
                    return Promise.resolve(this.checkValidity());
                case CheckoutStage.REPORT_VALIDITY_SAVE:
                    return Promise.resolve(this.reportValidity());
                default:
                    return Promise.resolve(true);
            }
        }
    }

    checkValidity() {
        if (this.showUploadComponent) {
            return this.fileUploaded;
        }
        else {
            return true;
        }
    }

    reportValidity() {
        this.showError = !(this.showUploadComponent && this.fileUploaded);

        if (this.showError) {
            this.dispatchUpdateErrorAsync({
                groupId: 'TermsAndConditions',
                type: '/commerce/errors/checkout-failure',
                exception: this.labels.Upload_PO_document,
            });
        }

        return this.showUploadComponent && this.fileUploaded;
    }
}