import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CurrentPageReference, NavigationMixin } from 'lightning/navigation';
import Id from '@salesforce/user/Id'; //gets Id of running user
import isGuest from '@salesforce/user/isGuest'; //true if unauthenticated, otherwise false
import { getRecord } from 'lightning/uiRecordApi';
import { getRelatedListRecords } from 'lightning/uiRecordApi';

const CONST_TYPE_RELOAD = 'reload';
const CONST_TYPE_BACK = 'back_forward';
const FIELDS = ['OrderSummary.Description',
            'OrderSummary.AccountId', 
            'OrderSummary.CurrencyIsoCode', 
            'OrderSummary.TotalAdjDeliveryAmtWithTax', 
            'OrderSummary.TotalTaxAmount', 
            'OrderSummary.TotalProductAmount', 
            'OrderSummary.TotalAdjustedDeliveryAmount', 
            'OrderSummary.TotalAdjustedProductTaxAmount',
            'OrderSummary.TotalDeliveryAmount',
            'OrderSummary.GrandTotalAmount'];

const ITEM_FIELDS = ['OrderItemSummary.Id',
            'OrderItemSummary.Name',
            'OrderItemSummary.Product2Id', 
            'OrderItemSummary.StockKeepingUnit', 
            'OrderItemSummary.TotalPrice'];

export default class welch_GoogleAnalyticsConversionTracking extends NavigationMixin(LightningElement) {

    @api recordId;
    @api objectApiName;
    orderSummary;
    orderLineItems;
    cookieVal;

    //Get the OrderSummary record on page load and send the event with the Purchase Information
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    wiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error loading OrderSummary',
                    message,
                    variant: 'error',
                }),
            );
        } else if (data) {
            this.orderSummary = data;
            let payload = { ecommerce: 
                { 
                    'transaction_id': this.orderSummary.id, 
                    'currency': this.orderSummary.fields.CurrencyIsoCode.value,
                    'value': this.orderSummary.fields.TotalProductAmount.value, 
                    'tax': this.orderSummary.fields.TotalTaxAmount.value,
                    'shipping': this.orderSummary.fields.TotalDeliveryAmount.value
                }
            };
            //publish custom event for the listener in the head markup to handle
            //Add a cookie for the order summary record ID so that we don't send duplicate events
            this.cookieVal = this.getCookie('recID');
            console.log('CookieVal: ' +this.cookieVal );
            if(this.cookieVal != this.recordId)
            {
                document.dispatchEvent(new CustomEvent('purchase', {detail: { ecommerce: 
                    { 
                        'transaction_id': this.orderSummary.id, 
                        'currency': this.orderSummary.fields.CurrencyIsoCode.value,
                        'value': this.orderSummary.fields.TotalProductAmount.value, 
                        'tax': this.orderSummary.fields.TotalTaxAmount.value,
                        'shipping': this.orderSummary.fields.TotalDeliveryAmount.value
                    }
                }}));
                this.createCookie('recID',this.recordId, null);
            }
            
        }
    }
    //creates a cookie
      createCookie(name, value, days) {
        var expires;
 
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
 
        document.cookie = name + "=" + encodeURIComponent(value) + expires + "; path=/";
        console.log(document.cookie);
    }

    getCookie(cookieName) {
        var tr = this.retrieveCookie(cookieName);
        if(tr != ''){
            this.cookieVal = tr;
        } else{
            this.cookieVal = [];
        }
         
       return tr;
    }

    retrieveCookie(cookieName){
        var cookieString = "; " + document.cookie;
        var parts = cookieString.split("; " + cookieName + "=");
        return decodeURIComponent(parts.pop().split(";").shift());
    }

}