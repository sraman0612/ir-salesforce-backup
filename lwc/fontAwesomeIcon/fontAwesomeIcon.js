import { LightningElement, api, wire } from 'lwc';
import getInit from '@salesforce/apex/FontAwesomeIconController.getInit';

export default class FontAwesomeIcon extends LightningElement {

    // required, corresponds to developer name in custom metadata: Font_Awesome_Icon__mdt
    @api iconName;
    
    // Possible values: sm, lg, 1x, 2x, 3x, Nx etc.
    // Documentation: https://fontawesome.com/v5.15/how-to-use/on-the-web/styling/sizing-icons
    @api iconSizeOverride; // default 1x

    // Any data value to pass back when the icon is clicked/selected
    @api val;

    @api disabled;

    iconClassSetting;
    defaultColor;
    hoverColor;

    @wire(getInit, {iconName: '$iconName'})
    initialize({ error, data }) {
        if (data) {
            
            console.log("icon settings: " + JSON.stringify(data));
            this.iconClassSetting = data.Icon_Class__c;
            this.defaultColor = data.Default_Color__c;
            this.hoverColor = data.Hover_Color__c;
            
            this.updateIconStyles(this.defaultColor);
        } 
        else if (error) {
            console.log("error initlializing icon(" + this.iconName + "): " + JSON.stringify(error));
        }
    }  

    onMouseOver(){
        this.updateIconStyles(this.hoverColor);
    }

    onMouseOut(){
        this.updateIconStyles(this.defaultColor);
    }    

    get iconSizeMagnitude(){
        return this.iconSizeOverride ? this.iconSizeOverride : '1x';
    }

    get iconClass(){
        return this.iconClassSetting + ' fa-' + this.iconSizeMagnitude;
    }

    get iconContainer(){
        return this.template.querySelector(".icon-container");
    }

    updateIconStyles(color){

        if (this.iconContainer){

            if (color){
                this.iconContainer.style = "color: " + color;
            }
            else{
                this.iconContainer.style = "";
            }

            if (this.disabled){

                if (this.iconContainer.style != ""){
                    this.iconContainer.style += ";cursor: no-drop;";
                }                
                else{
                    this.iconContainer.style = "cursor: no-drop;";
                }
            }
        }
        else{
            console.log("iconContainer not found");
        }
    }

    onClick(event){
        console.log("icon click: " + this.val);
        let response = {val : this.val}
        this.dispatchEvent(new CustomEvent('iconclick', { detail: response }));
    }
}