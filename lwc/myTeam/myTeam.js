import { LightningElement, wire, api,track } from 'lwc';
import getAccountsRecords from '@salesforce/apex/MyTeam.getMyTeamAccount';
import { NavigationMixin } from 'lightning/navigation';
import { CurrentPageReference } from 'lightning/navigation';
import tabNameLabel from '@salesforce/label/c.BR_My_Teams_Tab_Label';

export default class MyTeam extends NavigationMixin(LightningElement) {
    @track accounts = [];
    @track loaderSpinner = false;
    @track recordEnd = 0;
    @track recordStart = 0;
    @track pageNumber = 1;
    @track totalRecords = 0;
    @track totalPages = 0;
    @track error = null;
    @track pageSize = 5;
    @track isPrev = true;
    @track isNext = true;
    @track isUrl;
    @track isPagenation=false;
    @api tabName;// = "My_Team";

    connectedCallback() {
   this.tabName=tabNameLabel;
        console.log('Base url --> '+window.location.origin);
        console.log('Base url --> '+this.tabName);
        console.log('document.location --> '+ document.location.href);
        this.isUrl= document.location.href;
        var urlArray=this.isUrl.split('/');
        for(var i=0;i<urlArray.length;i++){
             if(urlArray[i]=='home'){
                console.log('urlArray --> '+urlArray[i]); 
                this.pageSize=10;
             }else if(urlArray[i]=='My_Team'){
                 this.isPagenation=true;
                  this.pageSize=100;
             }
        }
        this.getAccounts();
    }
    handlePageNextAction() {
        this.pageNumber = this.pageNumber + 1;
        this.getAccounts();
    }
 @wire(CurrentPageReference) pageRef;
    handleViewRecordsAction() {
           console.log("tabName = ", this.tabName)
            this[NavigationMixin.Navigate]
                ({
                    type: 'standard__navItemPage',
                    attributes: {
                        apiName: this.tabName
                    }
                });
    }


    handlePagePrevAction() {
        this.pageNumber = this.pageNumber - 1;
        this.getAccounts();
    }


       getAccounts() {
        this.loaderSpinner = true;
        getAccountsRecords({ pageSize: this.pageSize, pageNumber: this.pageNumber })
            .then(result => {
                this.loaderSpinner = false;
                if (result) {
                    //this.accounts = result;
                    
                    var resultData = JSON.parse(result);
                    console.log('businessRelationship --> '+JSON.stringify(resultData.businessRelationship));

                    this.recordEnd = resultData.recordEnd;
                    this.totalRecords = resultData.totalRecords;
                    this.recordStart = resultData.recordStart;
                    this.accounts = resultData.businessRelationship;
                    this.pageNumber = resultData.pageNumber;
                    this.totalPages = Math.ceil(resultData.totalRecords / this.pageSize);
                    this.isNext = (this.pageNumber == this.totalPages || this.totalPages == 0);
                    this.isPrev = (this.pageNumber == 1 || this.totalRecords < this.pageSize);
                }
            })
            .catch(error => {
                this.loaderSpinner = false;
                this.error = error;
            });
    }
     get isDisplayNoRecords() {
        var isDisplay = true;
        if (this.accounts) {
            if (this.accounts.length == 0) {
                isDisplay = true;
            } else {
                isDisplay = false;
            }
        }
        return isDisplay;
    }
}