import {
    LightningElement,
    track,
    wire,
    api
} from 'lwc';
import getContacts from '@salesforce/apex/searchContactForCaseController.searchContactNameMethod';
import updateCurrentContact from '@salesforce/apex/searchContactForCaseController.updateCurrentContact';
import updateAllContact from '@salesforce/apex/searchContactForCaseController.updateAllContact';
import isMultipleContacts from '@salesforce/apex/searchContactForCaseController.isMultipleContacts';
import { CloseActionScreenEvent } from 'lightning/actions';

import {
    ShowToastEvent
} from "lightning/platformShowToastEvent";


const DELAY = 100;

export default class SearchContactForCase extends LightningElement {
    contactName = '';
    contactEmail = '';
    @track contactList = [];
    @api recordId;
    @api objectApiName;
    @track contactId;
    @track isDatavisible = true;
    @track isConvisible = false;
    @track visible; //used to hide/show dialog
    @track title; //modal title
    @track name; //reference name of the component
    @track message; //modal message
    @track confirmLabel; //confirm button label
    @track cancelLabel; //cancel button label
    @track originalMessage; //any event/message/detail to be published back to the parent component
    @track isSingleRecordUpdate = false;
    @track isAllRecordUpdate = false;
    @track showSpinner = false;
    @track isMultipleContact = false;
    @track isSingle = false;
    @wire(getContacts, {
        conStrName: '$contactName',
        conStrPhone: '$contactEmail'
    })
    retrieveContact({ 
        error,
        data
    }) {
        if (data) {
            this.contactList = data;
        } else if (error) {
            console.log('**Error**', error);
        }

    }
   


    searchContactAction(event) {
        //this.accountName = event.target.value;
        const searchString = event.target.value;
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            this.contactName = searchString;
        }, DELAY);
    }


    radioChnage(event) {
        let conId = event.target.value;
        this.contactId = conId;



    }
    handleUpdateButtonClick(event) {
        if (this.recordId) {
            this.isConvisible = true;
            this.visible = true;
            this.isDatavisible = false;
           let recId = this.recordId;
            //this.isSingleRecordUpdate = true;
            isMultipleContacts({
                    currenCaseRecordId : recId
                })
                .then(result => {

                    let isMulti = result;
                    if (isMulti) {
                        this.isMultipleContact = true;
                    } else {
                        this.isSingle = true;
                    }
                })
                .catch((error) => {
                    this.showSpinner = true;
                    this.showToast("Error !!!", error.body.message, "error", 5000);
                });
        }
    }
    handleUpdateAllButtonClick(event) {
        if (this.contactId) {
            this.isConvisible = true;
            this.visible = true;
            this.isDatavisible = false;
            this.isAllRecordUpdate = true;
        }
    }

    updateCurrectCase(event) {
        this.showSpinner = true;
        let conId = this.contactId;
        let recId = this.recordId;
        updateCurrentContact({
                recordId : recId,
                contactRecordId : conId
            })
            .then(result => {

                if (result) {
                    this.isDatavisible = false;
                    this.isConvisible = false;
                    this.showSpinner = false;
                    this.visible = false;
                    this.isAllRecordUpdate = false;
                    this.isSingle = false;
                    this.isMultipleContact = false;
                    this.showSpinner = false;
                   
                    this.showToast(
                        "Success!!!",
                        "Records Save Succesfully",
                        "success",
                        5000
                    );
                      this.closeAction();
                }
            })
            .catch((error) => {
               this.isDatavisible = false;
                    this.isConvisible = false;
                    this.showSpinner = false;
                    this.visible = false;
                    this.isSingleRecordUpdate = false;
                     this.isMultipleContact = false;
                       this.isSingle = false;
                    this.isMultipleContact = false;
                   
                this.showToast("Error !!!", error.body.message, "error", 5000);
                  this.closeAction();
            });
    }

    updateAllContact(event) {



        this.showSpinner = true;
        let conId = this.contactId;
        let recId = this.recordId;
        updateAllContact({
                recordId: recId,
                contactRecordId: conId
            })
            .then(result => {
                if (result) {
                    this.isDatavisible = false;
                    this.isConvisible = false;
                    this.showSpinner = false;
                    this.visible = false;
                    this.isSingleRecordUpdate = false;
                     this.isMultipleContact = false;
                       this.isSingle = false;
                    this.isMultipleContact = false;
                  
                    this.showToast(
                        "Success!!!",
                        "Records Save Succesfully",
                        "success",
                        5000
                    );
                       this.closeAction();
                    
                }

            })
            .catch((error) => {
                // this.isDatavisible = false;
                //     this.isConvisible = false;
                //     this.showSpinner = false;
                //     this.visible = false;
                //     this.isSingleRecordUpdate = false;
                //      this.isMultipleContact = false;
                //        this.isSingle = false;
                //     this.isMultipleContact = false;
                this.showToast("Error !!!", error.body.message, "error", 5000);
                // this.closeAction();
            });



    }
    closeClick(event) {
        this.isDatavisible = true;
        this.isConvisible = false;

    }

    showToast(title, msg, variant, duration) {
        const event = new ShowToastEvent({
            title: title,
            message: msg,
            variant: variant//,
            // duration: duration
        });
        this.dispatchEvent(event);
    }

    closeAction(){
     
          this.dispatchEvent(new CloseActionScreenEvent());
           setTimeout(() => {
           window.location.reload();
        }, 1000);
       
    }
}