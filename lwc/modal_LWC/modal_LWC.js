import { LightningElement, api } from 'lwc';

export default class Modal_LWC extends LightningElement {

    @api modalTitle = '';
    @api hideCancel = false;
    @api hideSave = false;   
    @api hideHeader = false; 
    @api hideFooter = false; 
    @api cancelButtonLabel = 'Cancel';
    @api saveButtonLabel = 'Save';

    cancel(event){
        this.dispatchEvent(new CustomEvent('modalcancel'));
    }

    save(event){
        this.dispatchEvent(new CustomEvent('modalsave'));
    }    
}