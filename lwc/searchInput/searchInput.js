import { LightningElement, api } from 'lwc';

export default class SearchInput extends LightningElement {

    @api helpIconSizeOverride;
    @api helpIconStyleOverride;

    searchTerm;
    timer;
    initialized = false;

    renderedCallback(){

        if (!this.initialized){

            let iconContainer = this.template.querySelector(".help-icon-container");

            if (iconContainer){
                iconContainer.style = this.searchIconStyle;
            }
            else{
                console.log("search help icon container not found..");
            }

            this.initialized = true;
        }
    }

    // Fire the vent on a delay to ensure the user is done typing in the search box
    handleKeyUp(event){
        
        if (event.target.value.length > 1){

            clearTimeout(this.timer);
            this.searchTerm = event.target.value;  
            this.timer = setTimeout(function(){this.dispatchSearchTermChangeEvent()}.bind(this), 500);       
        }
    }

    // Function that will handle the search input clear feature
    onSearchChange(event){

        if (event.target.value.length === 0){
            this.searchTerm = event.target.value;
            this.dispatchSearchTermChangeEvent();
        }
    }

    dispatchSearchTermChangeEvent(){
         
        this.dispatchEvent(new CustomEvent(
            "searchtermchange",
            {
                detail : {
                    "searchTerm" : this.searchTerm
                }
            }
        ));

        clearTimeout(this.timer);
        this.timer = null;              
    }

    get helpIconSize(){
        return this.helpIconSizeOverride ? this.helpIconSizeOverride : "1x";
    }

    get searchIconStyle(){
        return this.helpIconStyleOverride ? this.helpIconStyleOverride : "vertical-align: super;";
    }
}