import {LightningElement, api, wire} from "lwc";
import { CartSummaryAdapter } from "commerce/cartApi";
import { useCheckoutComponent } from "commerce/checkoutApi";
import updateCart from '@salesforce/apex/B2BTermsAndConditionsController.updateCart';

const CheckoutStage = {
    CHECK_VALIDITY_UPDATE: "CHECK_VALIDITY_UPDATE",
    REPORT_VALIDITY_SAVE: "REPORT_VALIDITY_SAVE",
    BEFORE_PAYMENT: "BEFORE_PAYMENT",
    PAYMENT: "PAYMENT",
    BEFORE_PLACE_ORDER: "BEFORE_PLACE_ORDER",
    PLACE_ORDER: "PLACE_ORDER",
};

export default class B2bTermsAndConditionsAtCheckout extends useCheckoutComponent(LightningElement) {
    @api disclaimerText
    @api links
    @api error
    checked = false;
    showError = false;
    _disclaimerText;

    @wire(CartSummaryAdapter)
    setCartSummary({ data, error }) {
        if (data) {
            this.cartId = data.cartId;
        } else if (error) {
            console.error('Could not get the cart data:'+ JSON.stringify(error));
        }
    }

    /**
     * Embed a link directing in the disclaimer string.
     */
    get disclaimerTextWithLinks() {
        let regex = /\{(.*?)\}/g;
        this._disclaimerText = this.disclaimerText;
        let links = this.links.split(';');
        let match = regex.exec(this.disclaimerText);
        if (links) {
            for (let i = 0; i < links.length && match; i++) {
                let linkedText = match[0].replace( "{",
                    `<a href="${links[i]}"
                                    target="termsandconditions">`,).replace("}", "</a>");
                this._disclaimerText = this._disclaimerText.replace(match[0], linkedText);
                match = regex.exec(this.disclaimerText);
            }
            return this._disclaimerText;
        }
    }

    /**
     * update form when our container asks us to
     */
    /* jscpd:ignore-start */
    stageAction(checkoutStage /*CheckoutStage*/) {
        switch (checkoutStage) {
            case CheckoutStage.CHECK_VALIDITY_UPDATE:
                return Promise.resolve(this.checkValidity());
            case CheckoutStage.REPORT_VALIDITY_SAVE:
                return Promise.resolve(this.reportValidity());
            default:
                return Promise.resolve(true);
        }
    }
    /* jscpd:ignore-end */

    /**
     * Return true when terms checkoutbox is checked
     */
    checkValidity() {
        return !this.checked;
    }

    /**
     * Return true when terms checkbox is checked
     */
    reportValidity() {
        this.showError = !this.checked;

        if (this.showError) {
            this.dispatchUpdateErrorAsync({
                groupId: "TermsAndConditions",
                type: "/commerce/errors/checkout-failure",
                exception: this.error,
            });
        }
        else {
            this.clearError();
        }

        return this.checked;
    }

    clearError() {
        this.dispatchUpdateErrorAsync({
            groupId: 'TermsAndConditions',
            type: '/commerce/errors/checkout-failure',
            exception: '',
        });
    }

    /**
     * Check and uncheck the checkbox. Show error unless checked.
     * @param {*} event
     */
    handleChange(event) {
        this.checked = event.target.checked;
        this.showError = !this.checked;
        if (this.checked) {
            updateCart({ recordId: this.cartId })
                .catch(error => {
                    console.error('Error updating cart details:', error);
                });
            this.reportValidity();
        }
    }
}