import { LightningElement, api } from "lwc";
import getCSRecommendationProductsByProductId from "@salesforce/apex/B2BRecommendedProducts.getCSRecommendationProductsByProductId";

import ToastContainer from "lightning/toastContainer";
import { refreshCartSummary } from "commerce/cartApi";
import networkId from "@salesforce/community/Id";
import RECOMMENDED_PARTS from "@salesforce/label/c.Recommended_Parts";

export default class RecommendedProductsByProductId extends LightningElement {
	@api recordId;
	label = {
		RECOMMENDED_PARTS
	};
	crossSellProductsList;
	//Number of items retrieved
	noOfRecommendedItems;
	productId;

	connectedCallback() {
		console.log("this.recordId>>>" + this.recordId);
		console.log("networkId>>>" + networkId);
		getCSRecommendationProductsByProductId({ productId: this.recordId, communityId: networkId })
			.then((result) => {
				console.log("result>>>" + JSON.stringify(result));
				this.crossSellProductsList = result;
				this.noOfRecommendedItems = this.crossSellProductsList?.length;
			})
			.catch((error) => {
				console.log("error", error);
				this.noOfRecommendedItems = 0;
			});
	}
	handleAddToCart(event) {
		console.log("Event>>>" + JSON.stringify(event));
		refreshCartSummary();
		const toastContainer = ToastContainer.instance();
		toastContainer.maxShown = 1;
		toastContainer.toastPosition = "top-center";
	}
}