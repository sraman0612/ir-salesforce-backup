import { LightningElement } from 'lwc';
import requestUrl from '@salesforce/label/c.welch_RequestServiceLink';
import contactUrl from '@salesforce/label/c.welch_ContactUsLink';
export default class Welch_CaseRedirectButtons extends LightningElement {
    url = window.location.href;
    // encodedUrl = encodeURIComponent(this.url);
    

    goToContactUs(){
        const [first, ...rest] = this.url.split('?');
        console.log(first);
        console.log(rest);
        // console.log(this.encodedUrl);
        let encodedUrl = encodeURIComponent(first);
        let contactUsUrl = '';
        if(rest != null){
            contactUsUrl = contactUrl + '?returnUrl=' + encodedUrl + '&' + rest[0];
        }
        else{
            contactUsUrl = contactUrl + '?returnUrl=' + encodedUrl
        }
        window.location.href = contactUsUrl;
    }
    goToRequestService(){
        const [first, ...rest] = this.url.split('?');
        console.log(first);
        console.log(rest);
        // console.log(this.encodedUrl);
        let encodedUrl = encodeURIComponent(first);
        let requestServiceUrl = '';
        if(rest != null){
            requestServiceUrl = requestUrl + '?returnUrl=' + encodedUrl + '&' + rest[0];
        }
        else{
            requestServiceUrl = requestUrl + '?returnUrl=' + encodedUrl
        }
        // let requestServiceUrl = requestUrl + '?returnUrl=' + this.encodedUrl;
        window.location.href = requestServiceUrl;
    }
}