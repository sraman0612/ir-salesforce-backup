import {api, wire, LightningElement} from 'lwc';
import { getSessionContext, getAppContext } from 'commerce/contextApi';
import { navigate, NavigationContext, CurrentPageReference, NavigationMixin  } from 'lightning/navigation';
import getResults from '@salesforce/apex/B2BCustomSearchController.getRelevantAssets';
import getAllProductsCategoryId from '@salesforce/apex/B2BCustomSearchController.getCategoryId';

import { publish, subscribe, MessageContext } from "lightning/messageService";
import searchResultCardChannel from '@salesforce/messageChannel/SearchResultMessageChannel__c';
import searchCardChannel from '@salesforce/messageChannel/SearchMessageChannel__c';

import Search_Filter_HomePage_description from '@salesforce/label/c.Search_Filter_HomePage_description';
import Search_Filter_CategoryPage_description from '@salesforce/label/c.Search_Filter_CategoryPage_description';
import Search_Filter_HomePage_text from '@salesforce/label/c.Search_Filter_HomePage_text';
import Search_Filter_CategoryPage_text from '@salesforce/label/c.Search_Filter_CategoryPage_text';
import Results_Assets_GroupName from '@salesforce/label/c.Results_Assets_GroupName';
import Results_Models_GroupName from '@salesforce/label/c.Results_Models_GroupName';
import Serial_Number from '@salesforce/label/c.Serial_Number';
import Model from '@salesforce/label/c.Model';
import Filter_results from '@salesforce/label/c.Filter_results';
import Search_for_Parts from '@salesforce/label/c.Search_for_Parts';
import Missing_Search_text from '@salesforce/label/c.Missing_Search_text';
import Search_asset_placeholder from '@salesforce/label/c.Search_asset_placeholder';

export default class CustomSearch extends NavigationMixin(LightningElement) {
    labels = {
        Search_Filter_HomePage_description,
        Search_Filter_CategoryPage_description,
        Search_Filter_HomePage_text,
        Search_Filter_CategoryPage_text,
        Results_Assets_GroupName,
        Results_Models_GroupName,
        Serial_Number,
        Model,
        Filter_results,
        Missing_Search_text,
        Search_asset_placeholder,
        Search_for_Parts
    }

    /**
     * List of search results from apex call
     */
    searchResults = [];

    /**
     * List of search results
     */
    resultsToShow = [];

    /**
     * Account Id of the session
     */
    effectiveAccountId;

    /**
     * Selected search result from the dropdown
     */
    selectedSearchResult;

    /**
     * Handles mouse event
     */
    isListening = false;

    /**
     * Boolean to either show/hide the search card
     */
    showSearchCard = true;

    /**
     * Boolean to either show/hide the selected Asset card
     */
    showSelectedAssetCard = false;

    /**
     * Boolean to either show/hide the selected model card
     */
    showSelectedModelCard = false;

    /**
     * Boolean to either show/hide error message
     */
    showError = false;

    /**
     * Boolean to enable/disable search button
     */
    disableButton = true;

    /**
     * Category Id of the page on which the filter is placed
     */
    categoryId;

    /**
     * key for Model header in the dropdown
     * @type {string}
     */
    modelsHeader = 'models';

    /**
     * If the search is from homepage
     * @type {boolean}
     */
    fromHomePage = false;

    /**
     * If there are search results then show dropdown list
     * @type {boolean}
     */
    showDropDown = false;

    /**
     * Metadata Property to check if the component is placed on the homepage
     */
    @api
    isHomePage;

    /**
     * Navigation context
     */
    @wire(NavigationContext)
    navContext;

    /**
     * Message context
     */
    @wire(MessageContext)
    messageContext;

    /**
     * All Products category name
     */
    @api allProductsCategoryName;

    /**
     * Callbacks
     */
    async connectedCallback() {
        this.subscribeToMessageChannel();
        try {
            this.effectiveAccountId = await this.getEffectiveAccountId();
            this.webstoreId = await this.getWebstoreId();
            if (this.isHomePage) {
                let name = this.allProductsCategoryName;
                this.categoryId = await getAllProductsCategoryId({webstoreId: this.webstoreId, categoryName: name});
            }
        } catch (error) {
            console.error(error);
        }
    }

    renderedCallback() {
        if (this.resultsToShow?.length !== this.searchResults?.length) {
            this.resultsToShow = [].concat(this.searchResults);
        }
        if (this.isListening) return;
        window.addEventListener("click", (event) => {
            this.hideDropdown(event);
        });
        this.isListening = true;
    }

    /**
     * Gets the currentPageReference to get category id and selected result
     */
    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
        if (currentPageReference && !this.isHomePage) {
            const categoryPath = currentPageReference.state?.categoryPath;
            if (categoryPath) {
                this.getProductCategoryId(currentPageReference);
            }
            const selectedResult = currentPageReference.state?.selectedResult;
            if (selectedResult) {
                this.getSelectedResultItemFromHomePage(selectedResult);
                this.fromHomePage = currentPageReference.state?.fromHomePage;
            }
        }
    }

    /**
     * Gets the category Id
     */
    getProductCategoryId(currentPageReference) {
        if (currentPageReference) {
            // console.log('page ref connected' + JSON.stringify(currentPageReference));
            if (currentPageReference.type === 'standard__recordPage') {
                this.categoryId = currentPageReference.attributes?.recordId ?? '';
            }
            if (currentPageReference.state?.category) {
                this.categoryId = currentPageReference.state.category;
            }
        }
    }

    /**
     * Gets the selectedResult in the search component
     */
    getSelectedResultItemFromHomePage(selectedResult) {
        this.selectedSearchResult = JSON.parse(selectedResult);
        this.showSelectedItemCard();
    }

    /**
     * Determines which cards to show
     */
    showSelectedItemCard() {
        this.showSearchCard = false;
        if (this.selectedSearchResult.isSerialNumberSearch) {
            this.showSelectedAssetCard = true;
            this.showSelectedModelCard = false;
        }
        else {
            this.showSelectedModelCard = true;
            this.showSelectedAssetCard = false;
        }
        if (!this.isHomePage) {
            this.publishChannelMessage();
        }
    }

    /**
     * get the account Id
     */
    async getEffectiveAccountId() {
        const { effectiveAccountId } = await getSessionContext();
        return String(effectiveAccountId);
    }


    /**
     * get the webstore Id
     */
    async getWebstoreId() {
        const { webstoreId } = await getAppContext();
        return String(webstoreId);
    }

    /**
     * get the selected value
     */
    get selectedValue() {
        return this.selectedSearchResult?.name ?? null;
    }

    /**
     * Gets the cardStyle in the search component
     */
    get cardStyle() {
        let style = 'slds-card slds-align_absolute-center ';
        return this.isHomePage ? style + ' homePage-card-style' : style + ' card-style'
    }

    /**
     * Gets the buttonStyle in the search component
     */
    get buttonStyle() {
        let style = 'slds-button slds-button_brand button-style';
        return this.isHomePage ? style + ' homepage-filter-button-color-style ' : style;
    }

    /**
     * Gets the button text in the search component
     */
    get buttonText() {
        return this.isHomePage ? this.labels.Search_for_Parts : this.labels.Filter_results;
    }

    /**
     * Gets the top grid style in the search component
     */
    get gridTopStyle() {
        let style = 'slds-col input-style slds-size_5-of-6';
        return style;
    }

    /**
     * Gets the bottom grid style in the search component
     */
    get gridBottomStyle() {
        let style = 'slds-col slds-align-middle slds-size_1-of-6';
        return style;
    }

    /**
     * Handle the search input changes
     */
    handleInputChange(event) {
        this.showError = false;
        const inputVal = event.target.value; // gets search input value
        if (inputVal?.length > 2) {
            this.disableButton = false;
        }
        if (this.effectiveAccountId) {
            getResults({searchText: inputVal}).then((result) => {
                //console.log(result);
                if (result && result?.length > 0) {
                    this.searchResults = [].concat(result);
                    this.resultsToShow = [];
                    if (result.length == 1 && result[0].isSerialNumberSearch) {
                        this.selectedSearchResult = result[0];
                        this.showDropDown = false;
                    }
                    else {
                        this.showDropDown = true;
                    }
                }
                else {
                    this.showDropDown = false;
                }
            }).catch((error) => {
                console.error("Could not find the assets -" + JSON.stringify(error));
                this.showError = true;
            })
        }
    }

    /**
     * Handle the selected search input value
     */
    handleOptionClick(event) {
        const selectedValue = event.currentTarget?.dataset?.name;
        this.selectedSearchResult = this.resultsToShow.find(
            (pickListOption) => (pickListOption.name === selectedValue)
        );

        this.clearSearchResults();
    }

    /**
     * hide the dropdown list
     */
    hideDropdown(event) {
        const cmpName = this.template.host.tagName;
        const clickedElementSrcName = event.target.tagName;
        const isClickedOutside = cmpName !== clickedElementSrcName;
        if (this.resultsToShow && isClickedOutside) {
            this.clearSearchResults();
        }
    }

    /**
     * clear the search results
     */
    clearSearchResults() {
        this.searchResults = null;
        this.resultsToShow = null;
        this.showDropDown = false;
        this.showError = false;
        this.disableButton = this.selectedSearchResult ? false : true ;
    }

    /**
     * handles the event when the search text is cleared
     */
    handleClear(event) {
        if (!event.target.value.length) {
            this.clearSearchResults();
        }
    }

    /**
     * handles when the 'Filter results' button is clicked
     */
    filterResults() {
        if (this.selectedSearchResult) {
            this.showSelectedItemCard();
            let models = this.selectedSearchResult.modelsToFilter.split(';');
            let setModels = new Set(models);
            this.addModelFilters(setModels, this.selectedSearchResult.isLandingPageModel);
        }
        else {
            this.showError = true;
        }
    }

    /**
     * handles filtering the products list based on the models found
     */
    addModelFilters(models, isLandingPageModel) {
        const refinements = [
            {
                nameOrId: isLandingPageModel ? 'Landing_Page_Model__c' : 'Models__c',
                type: 'DistinctValue',
                attributeType: 'Custom',
                values: Array.from(models)
            }
        ]
        if (this.isHomePage)
        {
            const categoryPage = {
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.categoryId,
                    objectApiName: 'ProductCategory',
                    actionName: 'view'
                },state: {
                    refinements: JSON.stringify(refinements),
                    selectedResult: JSON.stringify(this.selectedSearchResult),
                    showSelectedAssetCard: this.showSelectedAssetCard,
                    showSelectedModelCard: this.showSelectedModelCard,
                    isHomePage: this.isHomePage,
                    categoryId: this.categoryId,
                    fromHomePage: true
                }
            };

            navigate(this.navContext, categoryPage);
        }
        else {
            const categoryPage = {
                type: 'standard__recordPage',
                attributes: {
                    recordId: this.categoryId,
                    objectApiName: 'ProductCategory',
                    actionName: 'view'
                },state: {
                    refinements: JSON.stringify(refinements)
                }
            };

            navigate(this.navContext, categoryPage);
        }
    }

    /**
     * publishes a message via LMS to searchResultCard
     */
    publishChannelMessage() {
        const payload = {selectedSearchResult: this.selectedSearchResult,
            showSelectedAssetCard: this.showSelectedAssetCard,
            showSelectedModelCard: this.showSelectedModelCard,
            isHomePage: this.isHomePage,
            categoryId: this.categoryId
        };

        // console.log('publish result card')
        publish(this.messageContext, searchResultCardChannel, payload);
    }

    /**
     * subscribes to the message via LMS from searchResultCard
     */
    subscribeToMessageChannel() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                searchCardChannel,
                (message) => this.handleMessage(message)
            );
        }
    }

    /**
     * handles the message from searchResultCard
     */
    handleMessage(message) {
        // console.log('in handle')
        this.showSearchCard = message.showCard;
        this.selectedSearchResult = null;
    }
}