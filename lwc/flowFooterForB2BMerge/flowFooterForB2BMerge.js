import { LightningElement, api } from 'lwc';
import { FlowNavigationFinishEvent, FlowNavigationNextEvent, FlowAttributeChangeEvent } from 'lightning/flowSupport';
import { NavigationMixin } from 'lightning/navigation';
import mergeAccounts from '@salesforce/apex/B2BMergeAccountsController.mergeAccountsFromLwc';
import Merge_accounts from '@salesforce/label/c.Merge_accounts';
import Cancel_merge from '@salesforce/label/c.Cancel_merge';

export default class flowFooterForB2BMerge extends NavigationMixin(LightningElement) {
    @api availableActions = [];
    @api currentAccount;
    @api duplicateAccount;
    @api isError;
    showSpinner = false;

    labels = {
        Cancel_merge,
        Merge_accounts
    }

    handleCancel() {
        this.handleNext();
    }

    handleMerge() {
        this.showSpinner = true;
        mergeAccounts({prospectAccount: this.currentAccount, masterAccount: this.duplicateAccount}).then((result) => {
            //console.log(result);
            if (result) {
                this.NavigateToRecord();
                this.showSpinner = false;
            }
        }).catch((error) => {
            console.error("Merge failed - " + JSON.stringify(error));
            this.showSpinner = false;
            this.isError = true;
            const attributeChangeEvent = new FlowAttributeChangeEvent('isError', this.isError);
            this.dispatchEvent(attributeChangeEvent);
            this.handleNext();
        })
    }

    NavigateToRecord() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.duplicateAccount.Id,
                objectApiName: 'Account',
                actionName: 'view'
            }
        });
    }

    handleNext() {
        const navigateNextEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(navigateNextEvent);
    }
}