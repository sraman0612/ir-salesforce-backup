import {api, LightningElement} from 'lwc';
import { getAppContext } from 'commerce/contextApi';
import { NavigationMixin  } from 'lightning/navigation';
import getProductCategoryIdByName from '@salesforce/apex/B2BLandingPageController.getCategoryId';
import getLandingPageConfiguration from '@salesforce/apex/B2BLandingPageController.getLandingPageConfiguration';
import userLanguage from '@salesforce/i18n/lang'

export default class b2bLandingPage extends NavigationMixin(LightningElement) {
    @api pageTitle;
    @api pageSubTitle;
    @api partsForRoboxLobeMachineLabel;
    @api partsForRoboxScrew_f_MachineLabel;
    @api partsForRoboxScrew_s_MachineLabel;
    @api partsForRoboxLobeMachineLegacyLabel;
    @api categoryName;
    @api allRoboxLobefilterLabel;
    @api allRoboxScrewfilterLabel;

    roboxListOne = [];
    roboxListTwo = [];
    roboxListThree = [];
    roboxListFour = [];
    roboxListFive = [];

    allRoboxLobe =[];
    allRoboxScrew=[];

    // Define a mapping of categories to their respective arrays
    categories = {
        'Robox Lobe': this.allRoboxLobe,
        'Robox Screw ': this.allRoboxScrew
    };

    /**
     * Category Id of the page on which the filter is placed
     */
    categoryId;

    webstoreId;

    get userLanguageCode() {
        return userLanguage.substring(0, 2);
    }

    /**
     * connectedCallback
     */
    async connectedCallback() {
        try {
            this.webstoreId = await this.getWebstoreId();
            await getProductCategoryIdByName({webstoreId: this.webstoreId, categoryName: this.categoryName})
                .then(result => {
                    this.categoryId = result[0].Id;
                })
                .catch(error => {
                    console.error(error);
                })
            await this.fetchLandingPageConfiguration();
        } catch (error) {
            console.error(error);
        }
    }

    async getWebstoreId() {
        const { webstoreId } = await getAppContext();
        return String(webstoreId);
    }

    async fetchLandingPageConfiguration() {
        try {
            let landingPageConfigurationData;
            await getLandingPageConfiguration()
                .then(result => {
                    landingPageConfigurationData = result;
                 })
                .catch(error => {
                    console.error(error);
                });

            this.roboxListOne = this.filterAndPushRoboxLobe(landingPageConfigurationData, 1);
            this.roboxListTwo = this.filterAndPushRoboxLobe(landingPageConfigurationData, 2);
            this.roboxListThree = this.filterAndPushRoboxLobe(landingPageConfigurationData, 3);
            this.roboxListFour = this.filterAndPushRoboxLobe(landingPageConfigurationData, 4);
            this.roboxListFive = this.filterAndPushRoboxLobe(landingPageConfigurationData, 5);

        } catch (error) {
            console.error('Error fetching landing page configuration:', error);
        }
    }

    filterAndPushRoboxLobe(data, order) {
        return data
            .filter(record => record.order === order)  // Filter by order
            .map(record => {
                // Loop through categories and check if the value matches
                for (const [key, array] of Object.entries(this.categories)) {
                    if (record.value.includes(key)) {
                        array.push(record.value);
                    }
                }
                const label = 'label_en'; //for all the languages it should bring the English value
                // Return the formatted record
                return {
                    id: record.value,
                    name: record[label]
                };
            });
    }

    handleAllRoboxLobeClick() {
        this.addModelFilters(this.allRoboxLobe);
    }

    handleAllRoboxScrewClick() {
        this.addModelFilters(this.allRoboxScrew);
    }

    handleClick(event) {
        const itemId = event.target.dataset.id;
        this.addModelFilters([itemId]);
    }

    addModelFilters(models) {
        // Construct refinements
        const refinements = [
            {
                nameOrId: 'Landing_Page_Model__c',
                type: 'DistinctValue',
                attributeType: 'Custom',
                values: models
            }
        ];
        // Construct navigation payload
        const categoryPage = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.categoryId,
                objectApiName: 'ProductCategory',
                actionName: 'view'
            },
            state: {
                refinements: JSON.stringify(refinements)
            }
        };

        // Perform navigation
        this[NavigationMixin.Navigate](categoryPage);
    }
}