import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getEvents from '@salesforce/apex/CTS_IOT_Site_Helix_EventsController.getEvents';
import hasConnectedAssets from '@salesforce/apex/CTS_IOT_Site_Helix_MessageController.hasConnectedAssets';

export default class CTS_IOT_Site_Helix_Alarm_Events extends LightningElement {

    @api recordId;
    @track data = [];
    pageSize = 10;
    offSet = 0;
    totalRecordCount = 0;
    hasConnectedAssets = false;

    columns = [
        {label : "Serial Number", fieldName : "assetRecordURL", type : "url", 
            typeAttributes: {
                label : {fieldName : "assetNickname"}
            }
        },
        {label : "Event Id", fieldName : "eventURL", type : "url",
            typeAttributes: {
                label : {fieldName : "eventName"}
            }
        },
        {label : "Event Timestamp", fieldName : "eventTimestamp", type: "date", 
            typeAttributes:{
                year: "numeric",
                month: "numeric",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                timeZoneName: "short"
            },
            sortable: true
        },
        {label : "Event Code", fieldName : "eventCode"},
        {label : "Event Message", fieldName : "eventMessage"}
    ];

    isLoading = true;

    @wire(hasConnectedAssets, {siteId: '$recordId'})
    hasConnectedAssets({error, data}){
        if (data) {
            this.hasConnectedAssets = data;
        } 
        else if (error) {
            console.log("error checking for connected assets: " + JSON.stringify(error));
        }
    }    

    connectedCallback(){
        console.log("connectedCallback");
        this.getEvents();
    }

    getEvents(){

        console.log("getEvents");

        this.isLoading = true;

        let params = {
            searchTerm : this.searchTerm,
            siteId : this.recordId,
            pageSize : this.pageSize,
            offSet : this.offSet
        };

        console.log("getEvents request: " + JSON.stringify(params));

        getEvents(params)
        .then(result => {
            
            console.log("getEvents result: " + JSON.stringify(result));

            if (result.success){
                this.data = result.events;
                this.totalRecordCount = result.totalRecordCount;
            }
            else{
                this.showToast(null, "Error retrieving events: " + result.errorMessage, "error");
            }
        })
        .catch(error => {
            console.log("getEvents error: " + JSON.stringify(error));
            this.showToast(null, JSON.stringify(error), "error");
        })
        .finally(()=>{
            this.isLoading = false;
        });
    }  

    get hasRecords(){
        return this.data.length > 0;
    }    

    get showPaginator(){
        return this.totalRecordCount > this.pageSize;
    }

    handlePageChange(event){
        this.offSet = event.detail.offSet;
        this.getEvents();
    }

    handlePageSizeChange(event){
        this.pageSize = event.detail.pageSize;
        this.getEvents();
    }

    handleSearchTermChange(event){
        this.searchTerm = event.detail.searchTerm;
        this.getEvents();
    }

    showToast(title, message, variant){
        
        let event = new ShowToastEvent({
            "variant": variant,
            "title": title,
            "message": message,
            duration: 10000
        });

        this.dispatchEvent(event);        
    } 
}