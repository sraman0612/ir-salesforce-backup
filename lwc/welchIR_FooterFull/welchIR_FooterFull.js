import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import Welch_Logo from '@salesforce/resourceUrl/Welch_IR_Logo';
import Welch_Logo_White from '@salesforce/resourceUrl/Welch_IR_Logo_White';
import welch_linkendin_logo from '@salesforce/resourceUrl/welch_linkedin';
import welch_youtube_logo from '@salesforce/resourceUrl/welch_youtube';
import welch_footer_waves from '@salesforce/resourceUrl/welch_footer_waves';
import welch_ingresoll_footer_logo from '@salesforce/resourceUrl/welch_ingresoll_footer_logo';
import networkId from '@salesforce/community/Id';
import basePath from '@salesforce/community/basePath';
export default class WelchIR_FooterFull extends LightningElement {

    // Expose the static resource URL for use in the template
    welchLogo = Welch_Logo;
    welchLogoWhite = Welch_Logo_White;
    welchLinkendinLogo = welch_linkendin_logo;
    welchYoutubeLogo = welch_youtube_logo;
    //welchFooterWaves = welch_footer_waves;
    welchIngresollFooterLogo = welch_ingresoll_footer_logo;
    AboutUs = basePath + "/about-us";
    ReturnPolicy = basePath + "/return-policy";

    get footerBackgroudStyle() {
        return `background-color: #f5f5f5;  border-top: 3px solid #d42a1d;  background-image: url(${welch_footer_waves});  background-repeat: no-repeat;  background-position: 50% 95%;  background-size: contain;`;
    }

}