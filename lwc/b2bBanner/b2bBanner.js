import {LightningElement, api} from 'lwc';
import basePath from '@salesforce/community/basePath';

const CMS_PATH = '/sfsites/c/cms/delivery/media/'

export default class B2BBanner extends LightningElement {
    @api banner1;
    @api banner2;
    @api banner3;
    @api altTextBanner1;
    @api altTextBanner2;
    @api altTextBanner3;
    @api urlBanner1;
    @api urlBanner2;
    @api urlBanner3;

    get banner1Url() {
        if(this.banner1) {
            return `${basePath}${CMS_PATH}${this.banner1}`;
        }
    }

    get banner2Url() {
        if(this.banner2) {
            return `${basePath}${CMS_PATH}${this.banner2}`;
        }
    }

    get banner3Url() {
        if(this.banner3) {
            return `${basePath}${CMS_PATH}${this.banner3}`;
        }
    }
}