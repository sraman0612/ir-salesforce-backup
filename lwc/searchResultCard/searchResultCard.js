import {LightningElement, api, wire} from 'lwc';
import {navigate, NavigationContext, CurrentPageReference} from "lightning/navigation";
import { publish, subscribe, unsubscribe, MessageContext,} from "lightning/messageService";
import searchResultCardChannel from '@salesforce/messageChannel/SearchResultMessageChannel__c';
import searchCardChannel from '@salesforce/messageChannel/SearchMessageChannel__c';
import Serial_Number from '@salesforce/label/c.Serial_Number';
import Model from '@salesforce/label/c.Model';
import Clear_Filter from '@salesforce/label/c.Clear_Filter';
export default class SearchResultCard extends LightningElement {
    labels = {
        Serial_Number,
        Model,
        Clear_Filter
    }

    /**
     * Selected search result from the dropdown
     */
    selectedSearchResult;

    /**
     * Boolean to either show/hide the selected Asset card
     */
    showSelectedAssetCard;

    /**
     * Boolean to either show/hide the selected model card
     */
    showSelectedModelCard;

    /**
     * If the search is from homepage
     * @type {boolean}
     */
    isHomePage;

    /**
     * Category Id of the page on which the filter is placed
     */
    categoryId;

    /**
     * Boolean to either show/hide the result card
     */
    showCard;

    /**
     * Navigation context
     */
    @wire(NavigationContext)
    navContext;

    /**
     * Message context
     */
    @wire(MessageContext)
    messageContext;

    /**
     * Gets the currentPageReference to get check if the event/data is coming from Home page or category change from the tabs menu
     * @param currentPageReference
     */
    @wire(CurrentPageReference)
    getStateParameters(currentPageReference) {
        if (currentPageReference) {
            const isHomepageParam = currentPageReference.state?.isHomePage;
            const categoryPath = currentPageReference.state?.categoryPath;
            const refinements = currentPageReference.state?.refinements;
            if ( Boolean(isHomepageParam)) {
                this.setParams(JSON.parse(currentPageReference.state?.selectedResult),
                    Boolean(currentPageReference.state?.showSelectedAssetCard),
                    Boolean(currentPageReference.state?.showSelectedModelCard),
                    Boolean(isHomepageParam),
                    currentPageReference.state?.categoryId);
            }
            else if (categoryPath && !refinements) {
                if (currentPageReference.type === 'standard__recordPage') {
                    this.categoryId = currentPageReference.attributes?.recordId ?? '';
                    this.publishChannelMessage();
                    this.showCard = false;
                    this.selectedSearchResult = null;
                }
            }
        }
    }

    /**
     * connected callback
     */
    connectedCallback() {
        this.subscribeToMessageChannel();
    }

    /**
     * publishes a message via LMS to customSearch
     */
    publishChannelMessage() {
        // console.log('in publish')
        const payload = {showCard: true};
        publish(this.messageContext, searchCardChannel, payload);
    }

    /**
     * subscribes to the message via LMS from customSearch
     */
    subscribeToMessageChannel() {
        if (!this.subscription) {
            // console.log('subscribeToMessageChannel result card')
            this.subscription = subscribe(
                this.messageContext,
                searchResultCardChannel,
                (message) => this.handleMessageFromSearchCard(message)
            );
        }
    }

    /**
     * handles the message from customSearch
     */
    handleMessageFromSearchCard(message) {
        // console.log('handle result card')
        this.setParams(message.selectedSearchResult, message.showSelectedAssetCard, message.showSelectedModelCard, message.isHomePage, message.categoryId)
    }

    /**
     * sets values to parameters used in html
     * @param selectedSearchResult
     * @param showSelectedAssetCard
     * @param showSelectedModelCard
     * @param isHomePage
     * @param categoryId
     */
    setParams(selectedSearchResult, showSelectedAssetCard, showSelectedModelCard, isHomePage, categoryId) {
        // console.log('set param')
        this.selectedSearchResult = selectedSearchResult;
        this.showSelectedAssetCard = showSelectedAssetCard;
        this.showSelectedModelCard = showSelectedModelCard;
        this.isHomePage = isHomePage;
        this.categoryId = categoryId;
        this.showCard = true;
    }


    /**
     * handles when the 'Clear Filter' button is clicked
     */
    clearFilter() {
        this.publishChannelMessage();
        this.showCard = false;
        this.selectedSearchResult = null;
        this.navigateToProductCategoryPage();
    }

    /**
     * Navigates to product category page
     */
    navigateToProductCategoryPage() {
        const categoryPage = {
            type: 'standard__recordPage',
            attributes: {
                recordId: this.categoryId,
                objectApiName: 'ProductCategory',
                actionName: 'view'
            },state: {}
        };
        navigate(this.navContext, categoryPage);
    }

}