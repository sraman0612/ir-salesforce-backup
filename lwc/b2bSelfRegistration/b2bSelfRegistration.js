import {api, LightningElement, wire, track} from 'lwc';
import registerUser from '@salesforce/apex/B2BSelfRegistrationController.registerUser';
import getCountryAndSateList from '@salesforce/apex/B2BSelfRegistrationController.getCountryAndSateList';

import userLanguage from '@salesforce/i18n/lang'

const FR_COUNTRY_CODE = 'FR';
const IT_COUNTRY_CODE = 'IT';

export default class B2BSelfRegistration extends LightningElement {
    @api billingStreetAddressLabel;
    @api billingCityLabel;
    @api billingStateLabel;
    @api billingPostalCodeLabel;
    @api billingCountryLabel;
    @api shippingStreetAddressLabel;
    @api shippingCityLabel;
    @api shippingStateLabel;
    @api shippingPostalCodeLabel;
    @api shippingCountryLabel;
    @api submitButtonLabel;
    @api accountOwnerId;
    @api genericErrorMessage;
    @api loadingLabel;
    @api acceptLabel;
    @api generalTermsConditionsLabel;
    @api generalTermsConditionsError;

    isTermsAccepted = false;
    error;
    loading;

    accountName;
    accountVATNumber;
    accountFiscalCode;
    accountSDINumber;
    accountInvoiceEmail;
    accountPECEmail;
    billingStreetAddress;
    billingCity;
    billingState;
    billingPostalCode;
    billingCountry;
    shippingStreetAddress;
    shippingCity;
    shippingState;
    shippingPostalCode;
    shippingCountry;
    contactPointAddressShippingState;
    contactFirstName;
    contactLastName;
    contactEmail;
    contactPhoneNumber;
    password;
    passwordConfirmation;
    @track countryAndStateData;
    shippingAddressEqualBillingAddress = true;

    _accountNameInfo = { labelText: '', helperText: '' };
    _accountVATNumberInfo = { labelText: '', helperText: '' };
    _accountFiscalCodeInfo = { labelText: '', helperText: '' };
    _accountSDINumberInfo = { labelText: '', helperText: '' };
    _accountInvoiceEmailInfo = { labelText: '', helperText: '' };
    _accountPECEmailInfo = { labelText: '', helperText: '' };
    _billingAddressInfo = { labelText: '', helperText: '' };
    _shippingAddressEqualBillingAddressInfo = { labelText: '', helperText: '' };
    _shippingAddressInfo = { labelText: '', helperText: '' };
    _contactFirstNameInfo = { labelText: '', helperText: '' };
    _contactLastNameInfo = { labelText: '', helperText: '' };
    _contactEmailInfo = { labelText: '', helperText: '' };
    _contactPhoneNumberInfo = { labelText: '', helperText: '' };
    _passwordInfo = { labelText: '', helperText: '' };
    _passwordConfirmationInfo = { labelText: '', helperText: '' };

    @api
    get accountNameInfo() {
        return JSON.stringify(this._accountNameInfo);
    }

    @api
    get accountVATNumberInfo() {
        return JSON.stringify(this._accountVATNumberInfo);
    }

    @api
    get accountFiscalCodeInfo() {
        return JSON.stringify(this._accountFiscalCodeInfo);
    }

    @api get accountSDINumberInfo () {
        return JSON.stringify(this._accountSDINumberInfo);
    };

    @api get accountInvoiceEmailInfo () {
        return JSON.stringify(this._accountInvoiceEmailInfo);
    };

    @api get accountPECEmailInfo() {
        return JSON.stringify(this._accountPECEmailInfo);
    };

    @api get billingAddressInfo() {
        return JSON.stringify(this._billingAddressInfo);
    };

    @api get shippingAddressEqualBillingAddressInfo() {
        return JSON.stringify(this._shippingAddressEqualBillingAddressInfo);
    };

    @api get shippingAddressInfo() {
        return JSON.stringify(this._shippingAddressInfo);
    };

    @api get contactFirstNameInfo() {
        return JSON.stringify(this._contactFirstNameInfo);
    };

    @api get contactLastNameInfo() {
        return JSON.stringify(this._contactLastNameInfo);
    };

    @api get contactEmailInfo() {
        return JSON.stringify(this._contactEmailInfo);
    };

    @api get contactPhoneNumberInfo() {
        return JSON.stringify(this._contactPhoneNumberInfo);
    };

    @api get passwordInfo() {
        return JSON.stringify(this._passwordInfo);
    };

    @api get passwordConfirmationInfo() {
        return JSON.stringify(this._passwordConfirmationInfo);
    };

    //parsing function for cleaner setters
    _parseInfo(value, defaultInfo) {
        if (!value) {
            console.warn('Provided value is null or undefined. Using defaults.');
            return defaultInfo;
        }
        try {
            const parsedData = JSON.parse(value);
            return parsedData.map(item => ({
                labelText: item.labelText || '',
                helperText: item.helperText || ''
            }))[0] || defaultInfo;
        } catch (error) {
            console.error('Error parsing account info:', error);
            return defaultInfo;
        }
    }

    set accountNameInfo(value) {
        this._accountNameInfo = this._parseInfo(value, this._accountNameInfo);
    }

    set accountVATNumberInfo(value) {
        this._accountVATNumberInfo = this._parseInfo(value, this._accountVATNumberInfo);
    }

    set accountFiscalCodeInfo(value) {
        this._accountFiscalCodeInfo = this._parseInfo(value, this._accountFiscalCodeInfo);
    }

    set accountSDINumberInfo(value) {
        this._accountSDINumberInfo = this._parseInfo(value, this._accountSDINumberInfo);
    }

    set accountInvoiceEmailInfo(value) {
        this._accountInvoiceEmailInfo = this._parseInfo(value, this._accountInvoiceEmailInfo);
    }

    set accountPECEmailInfo(value) {
        this._accountPECEmailInfo = this._parseInfo(value, this._accountPECEmailInfo);
    }

    set billingAddressInfo(value) {
        this._billingAddressInfo = this._parseInfo(value, this._billingAddressInfo);
    }

    set shippingAddressEqualBillingAddressInfo(value) {
        this._shippingAddressEqualBillingAddressInfo = this._parseInfo(value, this._shippingAddressEqualBillingAddressInfo);
    }

    set shippingAddressInfo(value) {
        this._shippingAddressInfo = this._parseInfo(value, this._shippingAddressInfo);
    }

    set contactFirstNameInfo(value) {
        this._contactFirstNameInfo = this._parseInfo(value, this._contactFirstNameInfo);
    }

    set contactLastNameInfo(value) {
        this._contactLastNameInfo = this._parseInfo(value, this._contactLastNameInfo);
    }

    set contactEmailInfo(value) {
        this._contactEmailInfo = this._parseInfo(value, this._contactEmailInfo);
    }

    set contactPhoneNumberInfo(value) {
        this._contactPhoneNumberInfo = this._parseInfo(value, this._contactPhoneNumberInfo);
    }

    set passwordInfo(value) {
        this._passwordInfo = this._parseInfo(value, this._passwordInfo);
    }

    set passwordConfirmationInfo(value) {
        this._passwordConfirmationInfo = this._parseInfo(value, this._passwordConfirmationInfo);
    }

    get languageCode() {
        return userLanguage.substring(0, 2);
    }

    get countryFieldName() {
        return `Country_${this.languageCode}__c`;
    }

    get stateFieldName() {
        return `State_${this.languageCode}__c`;
    }

    get fiscalCodeRequired() {
        return [FR_COUNTRY_CODE, IT_COUNTRY_CODE].includes(this.billingCountry);
    }

    get isItalian() {
        return IT_COUNTRY_CODE === this.billingCountry;
    }

    get sdiNumberRequired() {
        return this.isItalian && !this.accountPECEmail;
    }

    get pecAddressRequired() {
        return this.isItalian && !this.accountSDINumber;
    }

    get submitLabel() {
        return this.loading ? this.loadingLabel : this.submitButtonLabel;
    }

    get isButtonDisabled() {
        return this.loading || !this.isTermsAccepted;
    }

    @wire(getCountryAndSateList)
    wiredCountryAndState(result) {
        if (result.data) {
            this.countryAndStateData = result.data;
        } else if (result.error) {
            console.error(result.error);
        }
    }

    get countryOptions() {
        return this.countryAndStateData ? this.countryAndStateData.map(country => ({
            label: country[this.countryFieldName],
            value: country.DeveloperName
        })) : [];
    }

    get vatRegexPattern() {
        return this.countryAndStateData?.find(country => country.DeveloperName === this.billingCountry)?.VAT_Regex_Pattern__c ?? '';
    }

    get vatRegexPlaceholder() {
        return this.countryAndStateData?.find(country => country.DeveloperName === this.billingCountry)?.VAT_Pattern_Example__c ?? '';
    }

    get billingStateOptions() {
        const country = this.countryAndStateData?.find(country => country.DeveloperName === this.billingCountry);

        return country?.B2B_States__r?.map(state => ({
            label: state[this.stateFieldName],
            value: state.DeveloperName
        })) || undefined;
    }

    get shippingStateOptions() {
        const country = this.countryAndStateData?.find(country => country.DeveloperName === this.shippingCountry);

        return country?.B2B_States__r?.map(state => ({
            label: state[this.stateFieldName],
            value: state.DeveloperName
        })) || undefined;
    }

    get showShippingAddress() {
        return !this.shippingAddressEqualBillingAddress;
    }

    handlePhoneKeyDown(event) {
        // Allow numbers (0-9), navigation keys, control keys, "+" and cmd/ctrl shortcuts
        if (
            event.key >= '0' && event.key <= '9' || // Allow numbers
            event.key === '+' ||                   // Allow "+"
            event.key === 'Backspace' ||
            event.key === 'Delete' ||
            event.key === 'ArrowLeft' ||
            event.key === 'ArrowRight' ||
            event.key === 'Tab' ||
            event.key === 'Alt' ||
            (event.ctrlKey || event.metaKey) && ['z', 'v', 'x'].includes(event.key.toLowerCase()) // Allow Cmd+Z, Cmd+V, Cmd+X, and Ctrl+Z, Ctrl+V, Ctrl+X
        ) {
            return; // Allow the event
        }
        event.preventDefault(); // Prevent other keys
    }

    handleFieldChange(event) {
        this[event.target.name] = event.detail.value || event.detail.checked;

        if(event.target.name === 'shippingAddressEqualBillingAddress' && event.detail.checked) {
            this.shippingStreetAddress = this.billingStreetAddress;
            this.shippingPostalCode = this.billingPostalCode;
            this.shippingCity = this.billingCity;
            this.shippingState = this.billingState;
            this.shippingCountry = this.billingCountry;
        }

        if(event.target.name === 'contactPhoneNumber') {
            this.contactPhoneNumber = '+' + event.target.value.replace(/\D/g, ''); // Remove all non-digit characters
        }
    }

    handleInvoiceEmailBlur(event) {
        if(this.isItalian && !this.accountPECEmail) {
            this.accountPECEmail = event.target.value;
        }
    }

    handleBillingAddressChange(event) {
        this.billingStreetAddress = event.detail.street;
        this.billingPostalCode = event.detail.postalCode;
        this.billingCity = event.detail.city;
        this.billingState = event.detail.province;

        if(event.detail.country !== this.billingCountry) {
            this.billingCountry = event.detail.country;
            this.billingState = undefined;
        }

        if(!this.isItalian) {
            this.accountSDINumber = this.accountPECEmail = undefined;
        } else {
            if(!this.accountPECEmail) {
                this.accountPECEmail = this.accountInvoiceEmail;
            }
        }

        if(this.shippingAddressEqualBillingAddress) {
            this.handleShippingAddressChange(event);
        }
    }

    handleShippingAddressChange(event) {
        this.shippingStreetAddress = event.detail.street;
        this.shippingPostalCode = event.detail.postalCode;
        this.shippingCity = event.detail.city;
        this.shippingState = event.detail.province;
        this.shippingCountry = event.detail.country;

        if(event.detail.country !== this.shippingCountry) {
            this.shippingCountry = event.detail.country;
            this.shippingState = undefined;
        }

        if(this.shippingStateOptions && this.shippingStateOptions.length > 0) {
            this.contactPointAddressShippingState = this.shippingState;
        } else {
            this.contactPointAddressShippingState = undefined;
        }
    }

    async handleUserCreation() {
        try {
            this.loading = true;
            this.error = undefined;
            const cardId = localStorage.getItem('guestCartId');
            if (!this.isTermsAccepted) {
                this.error = this.generalTermsConditionsError;
                return;
            }

            const isFormDataValid = this.validateFormFields();
            if(!isFormDataValid) {
                return;
            }

            let formData = {
                accountName: this.accountName,
                accountVATNumber: this.accountVATNumber,
                accountFiscalCode: this.accountFiscalCode,
                accountSDINumber: this.accountSDINumber,
                accountInvoiceEmail: this.accountInvoiceEmail,
                accountPECEmail: this.accountPECEmail,
                billingStreetAddress: this.billingStreetAddress,
                billingCity: this.billingCity,
                billingState: this.billingState,
                billingPostalCode: this.billingPostalCode,
                billingCountry: this.billingCountry,
                shippingStreetAddress: this.shippingStreetAddress,
                shippingCity: this.shippingCity,
                shippingState: this.shippingState,
                shippingPostalCode: this.shippingPostalCode,
                shippingCountry: this.shippingCountry,
                contactPointAddressShippingState: this.contactPointAddressShippingState,
                contactFirstName: this.contactFirstName,
                contactLastName: this.contactLastName,
                contactEmail: this.contactEmail,
                contactPhoneNumber: this.contactPhoneNumber,
                username: this.contactEmail,
                password: this.password,
                passwordConfirmation: this.passwordConfirmation,
                accountOwnerId: this.accountOwnerId,
                cartId: cardId
            }
            window.location.href =  await registerUser({ formData: formData });
        } catch (e) {
            this.error = e?.body?.message ?? this.genericErrorMessage;
            if (this.error.startsWith('[') && this.error.endsWith(']')) {
                // Remove the first and last characters (the brackets)
                this.error = this.error.slice(1, -1);
            }
            console.error(e);
        } finally {
            this.loading = false;
        }
    }

    /**
     * Validate all form fields.
     * Check all field fields with the "validate" class and if they're
     * not valid, then report the validity and return false. Otherwise,
     * return true.
     */
    validateFormFields() {
        let isValid = true;
        let inputFields = this.template.querySelectorAll('.validate');
        inputFields.forEach(inputField => {
            if (!inputField.checkValidity()) {
                inputField.reportValidity();
                isValid = false;
            }
        });
        return isValid;
    }

    /**
     * handle checkbox change.
     * set isTermsAccepted to the checkbox value
     */

    handleCheckboxChange(event) {
        this.isTermsAccepted = event.target.checked;
    }

}