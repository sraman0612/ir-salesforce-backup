import { LightningElement, api, wire } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import NOT_COMPATIBLE_MSG from '@salesforce/label/c.CTS_IOT_Asset_Not_Helix_Connected_Not_Compatible';
import COMPATIBLE_MSG from '@salesforce/label/c.CTS_IOT_Asset_Not_Helix_Connected_Compatible';

const FIELDS = ['Asset.Name','Asset.Helix_Connectivity_Status__c'];

export default class CTS_IOT_Asset_Helix_Message extends LightningElement {
    
    @api recordId;
    
    @wire(getRecord, { recordId: '$recordId', fields: FIELDS })
    asset;

    get showMessage(){
        return this.showCompatibleMessage || this.showNotCompatibleMessage;
    }

    get showNotCompatibleMessage(){
        return this.asset && this.asset.data ? this.asset.data.fields.Helix_Connectivity_Status__c.value == 'Not Compatible' : false;
    }

    get notCompatibleMessage(){
        return NOT_COMPATIBLE_MSG;
    }

    get showCompatibleMessage(){
        return this.asset && this.asset.data ? this.asset.data.fields.Helix_Connectivity_Status__c.value == 'Compatible' : false;
    }

    get compatibleMessage(){
        return COMPATIBLE_MSG;
    }    
}