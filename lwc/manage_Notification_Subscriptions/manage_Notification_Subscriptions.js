import { LightningElement, api, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CTS_IOT_C0MMUNITY_RESOURCES from '@salesforce/resourceUrl/CTS_IOT_Community_Resources';
import getInit from '@salesforce/apex/Manage_Notification_Subscriptions_Ctrl.getInit';
import updateSubscriptions from '@salesforce/apex/Manage_Notification_Subscriptions_Ctrl.updateSubscriptions';
import hasConnectedAssets from '@salesforce/apex/CTS_IOT_Site_Helix_MessageController.hasConnectedAssets';

const UNEXPECTED_ERROR_TITLE = "An unexpected error occurred.";
const DELIVERY_METHOD_OPTIONS = [
    {label: "Email", value: "Email"},
    {label: "SMS", value: "SMS"},
    {label: "Community", value: "Community"}
];

export default class Manage_Notification_Subscriptions extends NavigationMixin(LightningElement) {

    @api recordId;
    @api cancelPageOverride;
    @api saveSuccessPageOverride;
    @api titlePaddingStyleOverride;
    @api titleActionsPaddingStyleOverride;
    record;
    objectType;
    objectLabel;
    isPortalUser = false;
    mobilePhone;
    currentUserId;
    @track subscriptionOptions; 
    availableDeliveryMethodOptions = DELIVERY_METHOD_OPTIONS;   
    isEmbeddedOnDetailsPage = false;
    hasConnectedAssets = false;  
    initialized = false;  
    componentLoaded = false;  
    isLoading = true;   

    connectedCallback(){

        console.log("manage_Notification_Subscriptions.connectedCallback");  

        if (!this.initialized){

            const urlParams = new URLSearchParams(window.location.search);
            const path = window.location.pathname;
            console.log('urlParams: ' + JSON.stringify(urlParams));

            this.recordId = "";

            if (urlParams.get('recordId')){
                this.recordId = urlParams.get('recordId');
            }      
            else{

                // Must be either for global site subscriptions or on the record details page
                // TODO: make this object agnostic (i.e. expose as page parameter for community builder)
                if (path.indexOf("/account/") > -1){
                    this.recordId = window.location.pathname.substring(window.location.pathname.indexOf("/account/") + 9, window.location.pathname.indexOf("/account/") + 27);
                    this.isEmbeddedOnDetailsPage = true;
                }
                else if (path.indexOf("/asset/") > -1){
                    this.recordId = window.location.pathname.substring(window.location.pathname.indexOf("/asset/") + 7, window.location.pathname.indexOf("/asset/") + 25);
                    this.isEmbeddedOnDetailsPage = true;
                }
                else{}
            }

            this.initialized = true;
            this.getInit();
        }
    }

    renderedCallback(){

        if (!this.componentLoaded){

            let titleSection = this.template.querySelector(".title-section");            

            if (titleSection){

                console.log("titleSection found");

                if (this.titlePaddingStyleOverride){
                    console.log("setting padding override..");
                    titleSection.style.padding = this.titlePaddingStyleOverride;
                }                
            }
            else{
                console.log("titleSection not found");
            }

            let titleActionsSection = this.template.querySelector(".title-actions-section");

            if (titleActionsSection){

                console.log("titleActionsSection found");

                if (this.titleActionsPaddingStyleOverride){
                    console.log("setting padding override..");
                    titleActionsSection.style.padding = this.titleActionsPaddingStyleOverride;
                }
            }
            else{
                console.log("titleActionsSection not found");
            }       
            
            if (titleSection && titleActionsSection){
                console.log("componentLoaded..");
                this.componentLoaded = true;
            }
        }        
    }

    getInit(){

        console.log("manage_Notification_Subscriptions:getInit");      
        
        getInit({recordId: this.recordId})
        .then(result => {
            this.handleInitResponse(result);
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("getInit error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoading = false;
        });         
    }

    handleInitResponse(data){

        console.log("handleInitResponse");
        console.log(JSON.stringify(data));
        
        this.isPortalUser = data.isPortalUser;
        this.mobilePhone = data.mobilePhone;
        this.currentUserId = data.currentUserId;
        this.record = data.record;
        this.objectType = data.objectType;
        this.objectLabel = this.objectType;

        let subscriptionOptions = [];

        data.subscriptionOptions.forEach(function(subscriptionOption){

            let subscriptionOptionClone = {...subscriptionOption};

            let subscriptionToUse = !subscriptionOptionClone.subscription && subscriptionOptionClone.overarchingSubscription ? subscriptionOptionClone.overarchingSubscription : subscriptionOptionClone.subscription; 
            
            subscriptionOptionClone.initialSelectedDeliveryMethods = subscriptionToUse && subscriptionToUse.Id && subscriptionToUse.Delivery_Methods__c ? subscriptionToUse.Delivery_Methods__c.split(";") : [];
            subscriptionOptionClone.selectedDeliveryMethods = subscriptionToUse ? subscriptionToUse.Delivery_Methods__c : "";
            subscriptionOptionClone.selectedDeliveryFrequencies = subscriptionToUse && subscriptionToUse.Id && subscriptionToUse.Delivery_Frequencies__c ? subscriptionToUse.Delivery_Frequencies__c.split(";") : "";
            subscriptionOptionClone.dailyEmailDigest = subscriptionOptionClone.selectedDeliveryFrequencies.indexOf("Daily") > -1;
            subscriptionOptionClone.weeklyEmailDigest = subscriptionOptionClone.selectedDeliveryFrequencies.indexOf("Weekly") > -1;
            this.calculateSubscriptionLevel(subscriptionOptionClone);
            subscriptionOptions.push(subscriptionOptionClone);
        }.bind(this));
        
        this.subscriptionOptions = [...subscriptionOptions];

        if (this.objectType == "Account"){

            hasConnectedAssets({siteId: this.recordId})
            .then(result => {
                this.hasConnectedAssets = result;
            })
            .catch(error => {
                console.log("error checking for connected assets: " + JSON.stringify(error));
            })     
            .finally(()=>{
                this.isLoading = false; 
            })      
        }
        else{
            this.isLoading = false; 
        }
    }   

    get isAccount(){
        return this.objectType == "Account";
    }

    get isAsset(){
        return this.objectType == "Asset";
    }    
    
    get communityStaticResourceURL(){
        return CTS_IOT_C0MMUNITY_RESOURCES;
    }
    
    get titleIconURL(){
        return this.communityStaticResourceURL + '/Icons/PNG/Preventative-Predictive_Red&Gray.png';
    }

    get pageTitle(){

        let title = "Manage Notification Subscriptions";

        if (this.record){

            let name = this.record.Name;

            if (this.objectType == "Account"){
                name = this.record["Account_Name_Displayed__c"];
            }
            else if (this.objectType == "Asset"){
                name = this.record["Asset_Name_Displayed__c"];
            }        

            title += " for " + name;         
        }
        else{
            title += " for All My Sites";
        }        

        return title;
    }

    get showHelixMessage(){

        if (this.objectType == "Asset"){
            return this.record.Helix_Connectivity_Status__c != 'Active';
        }    
        else if (this.objectType == "Account"){
            return !this.hasConnectedAssets;
        }          
        
        return false;
    }
    
    handleDailyDigestChange(event){

        let index = event.target.dataset.index;
        console.log("handleDailyDigestChange: (" + index + ")");
        this.subscriptionOptions[index].dailyEmailDigest = !this.subscriptionOptions[index].dailyEmailDigest;  
        this.evaluateDeliveryOptionsChange(this.subscriptionOptions[index]);
    }    

    handleWeeklyDigestChange(event){

        let index = event.target.dataset.index;
        console.log("handleWeeklyDigestChange: (" + index + ")");
        this.subscriptionOptions[index].weeklyEmailDigest = !this.subscriptionOptions[index].weeklyEmailDigest;
        this.evaluateDeliveryOptionsChange(this.subscriptionOptions[index]);
    }     

    handleDeliveryMethodChange(event){

        let index = event.detail.index;
        let value = event.detail.value;        
        console.log("handleDeliveryMethodChange: (" + index + ") " + value);           
        this.subscriptionOptions[index].selectedDeliveryMethods = value;
        this.evaluateDeliveryOptionsChange(this.subscriptionOptions[index]);
    }

    // Check to see if all delivery option changes should impact subscription status
    evaluateDeliveryOptionsChange(subscriptionOption){

        if (!subscriptionOption.weeklyEmailDigest && !subscriptionOption.dailyEmailDigest && subscriptionOption.selectedDeliveryMethods == ""){

            subscriptionOption.subscribed = false;
            subscriptionOption.unsubscribed = true;

            if (subscriptionOption.overarchingSubscription){
                subscriptionOption.optedOut = true;
            }
        } 
        else{
            subscriptionOption.subscribed = true;
            subscriptionOption.unsubscribed = false;
            subscriptionOption.optedOut = false;
        }      

        this.calculateSubscriptionLevel(subscriptionOption);
    }    

    calculateSubscriptionLevel(subscriptionOption){

        if (this.record && this.record.Id){

            if (this.record.Id.startsWith('001')){
                subscriptionOption.subscriptionLevel = 'Site';
                subscriptionOption.siteLevel = true;
            }
            else if (this.record.Id.startsWith('02i')){
                subscriptionOption.subscriptionLevel = 'Asset';
                subscriptionOption.assetLevel = true;
            }                
        }              
        else{
            subscriptionOption.subscriptionLevel = 'Global';
            subscriptionOption.globalLevel = true;
        }

        // If there is an override, determine if they have changed any selections
        let useOverrideSubscription = false;        

        if (subscriptionOption.subscribed){
     
            // Compare the delivery options to determine if a new subscription is needed or if the overarching subscription suffices
            if (!subscriptionOption.optedOut && subscriptionOption.overarchingSubscription && subscriptionOption.overarchingSubscription.Id){

                let overarchingWeeklyEmailDigest = subscriptionOption.overarchingSubscription.Delivery_Frequencies__c.indexOf("Weekly") > -1;
                let overarchingDailyEmailDigest = subscriptionOption.overarchingSubscription.Delivery_Frequencies__c.indexOf("Daily") > -1;
                let overarchingDeliveryMethods = subscriptionOption.overarchingSubscription.Delivery_Methods__c;
                
                console.log("selectedDeliveryMethods: " + subscriptionOption.selectedDeliveryMethods);
                console.log("overarchingDeliveryMethods: " + overarchingDeliveryMethods);

                if (subscriptionOption.weeklyEmailDigest != overarchingWeeklyEmailDigest || subscriptionOption.dailyEmailDigest != overarchingDailyEmailDigest){
                    useOverrideSubscription = true;
                }
                else if (subscriptionOption.selectedDeliveryMethods != overarchingDeliveryMethods){
                    useOverrideSubscription = true;
                }

                console.log("useOverrideSubscription: " + useOverrideSubscription); 

                if (!useOverrideSubscription){
    
                    if (subscriptionOption.overarchingSubscription.Record_ID__c && subscriptionOption.overarchingSubscription.Record_ID__c.startsWith('001')){
                        subscriptionOption.subscriptionLevel = 'Site';
                    }
                    else if (subscriptionOption.overarchingSubscription.Record_ID__c && subscriptionOption.overarchingSubscription.Record_ID__c.startsWith('02i')){
                        subscriptionOption.subscriptionLevel = 'Asset';
                    }    
                    else{
                        subscriptionOption.subscriptionLevel = 'Global';
                    }                                                   
                }                   
            }              
        }      

        switch(subscriptionOption.subscriptionLevel) {
            case 'Global':
                subscriptionOption.subscriptionLevelIcon = 'Global';
                break;
            case 'Site':
                subscriptionOption.subscriptionLevelIcon = 'Site_Displayed';
                break;
            case 'Asset':
                subscriptionOption.subscriptionLevelIcon = 'Asset_Displayed';
                break;              
        }
    }

    onUnsubscribe(event){

        let index = event.target.dataset.index;
        console.log("onUnsubscribe: (" + index + ")");

        let subscriptionOption = this.subscriptionOptions[index];
        subscriptionOption.subscribed = false; 
        subscriptionOption.unsubscribed = true;

        if (subscriptionOption.overarchingSubscription){
            subscriptionOption.optedOut = true;
        }        
        else{
            subscriptionOption.optedOut = false;   
        }
        
        subscriptionOption.weeklyEmailDigest = false;
        subscriptionOption.dailyEmailDigest = false;
        subscriptionOption.selectedDeliveryMethods = "";

        this.template.querySelectorAll("c-multi-select-combo-box").forEach(function(combo){
            if (combo.index == index){
                combo.updateSelections("");
            }
        });

        this.calculateSubscriptionLevel(subscriptionOption);
    }      

    onSubscribe(event){

        let index = event.target.dataset.index;
        console.log("onSubscribe: (" + index + ")");

        let subscriptionOption = this.subscriptionOptions[index];
        subscriptionOption.subscribed = true; 
        subscriptionOption.unsubscribed = false;
        subscriptionOption.optedOut = false;  
        this.calculateSubscriptionLevel(subscriptionOption);        
    }      

    onUpdateSubscriptions(event){

        console.log("onUpdateSubscriptions");
        this.isLoading = true;

        // Validations
        let errors;

        this.subscriptionOptions.forEach(function(subscriptionOption){

            if (subscriptionOption.subscribed){

                console.log("Evaluating: " + JSON.stringify(subscriptionOption));
                console.log("mobilePhone: " + this.mobilePhone);

                if (subscriptionOption.selectedDeliveryMethods && subscriptionOption.selectedDeliveryMethods.indexOf('SMS') > -1 && !this.mobilePhone){

                    let message = "You have subscribed for SMS notifications on Event Type '" + subscriptionOption.notificationType.Name + "' " +                            
                                  " but do not have a mobile phone number on record.  Please update your mobile phone under the 'My Profile' page and try again.";

                    errors = !errors ? message : errors + "\n" + message;
                }                      

                if (!subscriptionOption.dailyEmailDigest && !subscriptionOption.weeklyEmailDigest && !subscriptionOption.selectedDeliveryMethods){

                    let message = "You must select a Delivery Option for Event Type '" + subscriptionOption.notificationType.Name + "'.";

                    errors = !errors ? message : errors + "\n" + message;
                }                                 

                console.log("errors: " + errors);
            }

        }.bind(this));

        if (errors){
            this.showToast("Error(s) found while updating subscrptions.", errors, "error");
            this.isLoading = false;
        }
        else{

            console.log("no errors, proceeding to update subscriptions..");    

            let request = {
                recordId: this.recordId,
                subscriptionOptions : this.subscriptionOptions
            }

            let requestStr = JSON.stringify(request);

            console.log("requestStr: " + requestStr);

            updateSubscriptions({updateSubscriptionsRequestStr : requestStr})
            .then(result => {

                if (result.success){
                    this.onUpdateSubscriptionsSuccess();
                }
                else{
                    console.log("updateSubscriptions error: " + result.errorMessage);
                    this.showToast(UNEXPECTED_ERROR_TITLE, result.errorMessage, "error");
                    this.isLoading = false;                    
                }
            })
            .catch(error => {
                let msg = (error && error.body && error.body.message) ? error.body.message : error;
                console.log("updateSubscriptions error: " + msg);
                this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
                this.isLoading = false;
            });                        
        }
    }

    onCancel(event){
        
        this[NavigationMixin.Navigate]({
            type: 'comm__namedPage',
            attributes: {
                name: this.cancelPageOverride ? this.cancelPageOverride : 'Notification_Subscriptions_Tree_View__c'
            }
        });
    }

    onUpdateSubscriptionsSuccess(){

        console.log("onUpdateSubscriptionsSuccess");

        this.showToast(null, "Notification subscriptions updated successfully.", "success");

        // Reinitialize the page
        getInit({recordId: this.recordId})
        .then(result => {

            if (!this.isEmbeddedOnDetailsPage){

                this[NavigationMixin.Navigate]({
                    type: 'comm__namedPage',
                    attributes: {
                        name: this.saveSuccessPageOverride ? this.saveSuccessPageOverride : 'Notification_Subscriptions_Tree_View__c'
                    }
                });
            }
            else{
                this.handleInitResponse(result);
            }
        })
        .catch(error => {
            let msg = (error && error.body && error.body.message) ? error.body.message : error;
            console.log("getInit error: " + msg);
            this.showToast(UNEXPECTED_ERROR_TITLE, msg, "error");
            this.isLoading = false;
        });                   
    }   

    showToast(title, message, variant){
        
        let event = new ShowToastEvent({
            "variant": variant,
            "title": title,
            "message": message,
            duration: 10000
        });

        this.dispatchEvent(event);        
    }

    get deliveryMethodClasses(){
        return this.isPortalUser ? 'slds-p-bottom_xx-small' : 'slds-p-top_medium slds-p-bottom_xx-small';
    }

    get dropdownAlignment(){
        return this.isPortalUser ? 'bottom-left' : 'auto';
    }
}