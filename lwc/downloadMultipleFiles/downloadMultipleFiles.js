import { LightningElement, api,wire} from 'lwc';
import getFiles from '@salesforce/apex/ExtractFilesMultipleDownload.getFiles';
import { ShowToastEvent } from "lightning/platformShowToastEvent";

const COLUMNS = [
    {
        label: 'Title',
        fieldName: 'urlToRedirect',
        type: 'url',
        typeAttributes: { 
            label: { fieldName: 'Title' },
            target : '_blank'
        }
    },
    {
        label: 'Type',
        fieldName: 'FileExtension',
        type: 'text'
    },
    {
        label:'Created Date',
        fieldName: 'CreatedDate',
        type:'date',
        typeAttributes:{
            year: "numeric",
            month: "2-digit",
            day: "2-digit"
        }
    }
    
    
];

const BASE_DOWNLOAD_PATH = '/sfc/servlet.shepherd/version/download';

export default class downloadMultipleFiles extends LightningElement {
    
    @api recordId;
    displayTable = false;
    files = [];
    columns = COLUMNS;

   @wire(getFiles,{
        recordId:"$recordId"
           }) 
    Wiredfiles({error,data}){
        if(data){
            if(data.length > 0){
               
                this.displayTable = true;}
            
            let tempFiles = [];
            data.forEach((item)=>{
                let tempFile = Object.assign({},item);
                tempFile.urlToRedirect = '/'+tempFile.Id;
               
                //console.log(tempFile);
               // console.log(tempFile.LatestPublishedVersion.File_Size__c);
                tempFiles.push(tempFile);
            });
            this.files = tempFiles;
           // console.log(`recordId : ${this.recordId}`);
            
        }
        else if(error){
            console.log(error);
        }
    }
   
    downloadFiles() {

        let selectedFiles = this.getSelectedRows();

        if(selectedFiles.length === 0) {
            const evt = new ShowToastEvent({
                title: 'No Files Selected',
                message: 'Please Select atleast 1 File to Proceed',
                variant: 'error',
              });
              this.dispatchEvent(evt);
            //alert('No files selected');
            return;
        }

        this.initDownloading(
            this.getDownloadString(selectedFiles)
        );
    }

    getDownloadString(files) {
        let downloadString = '';
        files.forEach(item => {
            downloadString += '/' + item.LatestPublishedVersionId
        });
        return downloadString;
    }

    initDownloading(downloadString) {
       // alert(BASE_DOWNLOAD_PATH + downloadString);
    //   console.log(BASE_DOWNLOAD_PATH + downloadString);
        window.open(BASE_DOWNLOAD_PATH + downloadString, '_blank');
       
    
    }

    getSelectedRows() {
        return this.template.querySelector('lightning-datatable').getSelectedRows(); 
    }
}