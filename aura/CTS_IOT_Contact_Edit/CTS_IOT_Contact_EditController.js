({
    init: function (cmp, event, helper) {
        console.log("CTS_IOT_Contact_Edit:init");                
    },    

    onContactIdChange : function(cmp, event, helper){
        console.log("onContactIdChange");
        
        var contactID = cmp.get("v.contactID");

        if (contactID != null && contactID != ""){
            helper.initialize(cmp, event, helper); 
        }
    },
    
    onSaveUser : function(cmp, event, helper){
        console.log("onSaveUser");

        helper.updateUser(cmp, event, helper);        
    },    

    onCancelSaveUser : function(cmp, event, helper){

        console.log("onCancelSaveUser");
        var modalEvent = cmp.getEvent('onModalCancel');
        modalEvent.setParams({"modalID" : cmp.get("v.modalID")});
        modalEvent.fire();           
    },

    onSaveMappings : function(cmp, event, helper){
        helper.updateUserSiteAccess(cmp, event, helper);
    },

    onApprove : function(cmp, event, helper){
        
        cmp.set("v.approvalRequest", true);
        helper.processApprovalAction(cmp, event, helper, true, null);
    },

    onReject : function(cmp, event, helper){

        cmp.set("v.approvalRequest", true);
        var rejectReason = event.getParam("value");        
        helper.processApprovalAction(cmp, event, helper, false, rejectReason);
    },  
    
    onStatusChange : function(cmp, event, helper){
        console.log("onStatusChange");
                
        var contact = cmp.get("v.contact");

        if (contact.Status == 'Active'){
            helper.deactivateUser(cmp, event, helper);
        }
        else{
            helper.activateUser(cmp, event, helper);
        }        
    }
})