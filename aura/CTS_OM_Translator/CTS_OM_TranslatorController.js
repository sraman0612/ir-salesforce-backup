({	
    translateRelated: function(component, event, helper) {        
        helper.translateVals(component, event, null, 'emailMessages');
        helper.translateVals(component, event, null, 'caseComments');
        component.set("v.relatedTranslated",true);
    },
    onInit: function(component, event, helper) {
        let action = component.get("c.initialize");
        action.setParams({recordId : component.get("v.recordId") });
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                let result = response.getReturnValue();
                component.set("v.fields", result)
                helper.translateVals(component, event, result, null);//translate fields on load of the cmp
                helper.translateVals(component, event, null, 'caseComments');//translate fields on load of the cmp
                helper.translateVals(component, event, null, 'emailMessages');//translate fields on load of the cmp
            } else {
                console.log('error state**', state);
            }
        });
        $A.enqueueAction(action);
    }
})