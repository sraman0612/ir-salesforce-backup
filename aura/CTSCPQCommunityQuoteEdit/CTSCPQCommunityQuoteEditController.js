({
    editQuote: function (component, event, helper) {
        var navService = component.find("navService");
        event.preventDefault();
        var urlString = window.location.href;
        var baseURL = urlString.substring(0, urlString.indexOf("/s"));
        var quoteURLStr = baseURL + '/s/quoteeditor?id=' + component.get("v.recordId");
        let pr2 = {"type": "standard__webPage","attributes": {"url": quoteURLStr}}
        navService.navigate(pr2);
    }
})