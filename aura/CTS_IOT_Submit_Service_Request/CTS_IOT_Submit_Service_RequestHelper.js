({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
        var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            "assetID" : cmp.get("v.assetID"),
            "siteID" : cmp.get("v.siteID")
        });

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleGetInitSuccess : function(cmp, event, helper, response) {

        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

                cmp.set("v.user", responseVal.user);
                cmp.set("v.site", responseVal.site);
                cmp.set("v.assetDetail", responseVal.assetDetail);
                cmp.set("v.assetOptions", responseVal.assetOptions);
            }
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);        
        }    
        
        cmp.set("v.isLoading", false);
    },    

    submitServiceRequest : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
        var utils = cmp.find("utils");
        var action = cmp.get("c.submitServiceRequest");
        var serviceRequest = cmp.get("v.serviceRequest");

        action.setParams({
            "serviceRequestJSON" : JSON.stringify(serviceRequest)
        });
    
        utils.callAction(action, helper.handleSubmitServiceRequestSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleSubmitServiceRequestSuccess: function(cmp, event, helper, response){

        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

                var toastEvent = $A.get("e.force:showToast");
				
			    toastEvent.setParams({
			        mode : "dismissible",
			        duration : 3000,
			        message : "Your service request was successfully submitted.",
			        type : "success"			        
			    });
			    
                toastEvent.fire();

                // Reset breadcrumbs
                // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
                
                // navEvent.setParams({
                //     "resetBreadcrumbs" : "true"
                // });

                // navEvent.fire();                  
                
                var urlEvent = $A.get("e.force:navigateToURL");

                urlEvent.setParams({
                  "url": "/"
                });

                urlEvent.fire();
            }
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);        
        }    
        
        cmp.set("v.isLoading", false);
    },    
    
    handleActionCallFailure: function(cmp, event, helper, response){
        console.log("handleActionCallFailure");
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }    
})