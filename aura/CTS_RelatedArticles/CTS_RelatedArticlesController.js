({
	onInit: function(component, event, helper) {
        let action = component.get("c.getRelatedArticles");
        var recordId = component.get("v.recordId");
        if(recordId == null){
            var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            console.log('sPageURL', sPageURL);
            console.log('sURLVariables', sURLVariables);
            
            var sParameterName;
            var i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                
                if (sParameterName[0] === 'aid') { //find aid
                    recordId = sParameterName[1];
                }
            }
            
        }
        action.setParams({knowledgeId : recordId});
        action.setCallback(this, function(response) {
            let state = response.getState();
            if(state === "SUCCESS") {
                let result = response.getReturnValue();
                console.log('result Megha '+ result);
                component.set("v.relatedArticles", result);
                var networkURL = window.location.pathname.split('/')[1];
                component.set("v.networkURL", networkURL);
                console.log('result Megha 1>> '+ component.get("v.relatedArticles"));
            } else {
                console.log('error state**', response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})