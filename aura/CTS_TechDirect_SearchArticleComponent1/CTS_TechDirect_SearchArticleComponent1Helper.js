({
	searchArticle : function(component, event, helper) {
		//var validationStatus = component.find("validationStatusId").get("v.value");  
        var keywordValue = component.find("keywordId").get("v.value");
        var languageValue = component.find("languageId").get("v.value");
        var selectedProduct = component.get("v.selectedProduct");
        var selectedCategory = component.get("v.selectedCategory");
        // Get the component event by using the name value from aura:registerEvent
        var searchEvent = $A.get("e.c:CTS_TechDirect_SearchArticleEvent1");
        searchEvent.setParams({
            //"validationStatus" : validationStatus,
            "searchKeyword" : keywordValue,
            "language" : languageValue,
            "product" : selectedProduct,
            "category" : selectedCategory
        });
        searchEvent.fire();
	}
})