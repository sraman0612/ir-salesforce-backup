({
    submitFeedback : function(component, event,helper){
        var action = component.get("c.submitArticleFeedback");
        var feedbackDesc = component.get("v.feedbackDescripiton");
        var recId = component.get("v.recordId");
        var caseType = component.get("v.caseType");
        var caseSubj = component.get("v.caseSubject");
        
        //check if feedback is empty
        if(feedbackDesc.trim().length === 0){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Failure",
                    message: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Error"),
                    type: "failure"
                });
                toastEvent.fire();
            return;
        }
        
        console.log('::recId'+recId);
        console.log('::feedback::'+document.getElementById('feedback'));
        action.setParams({message: feedbackDesc,articleId: recId, caseType: caseType, subject :caseSubj });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                console.log('Success**' + results);
                if(results[0] == 'SUCCESS'){
                    if(component.get("v.isModal") == true){
                        $A.get("e.force:closeQuickAction").fire();
                    }else{
                        var elements = document.getElementById("feedback");
                        elements.style.display = 'none';
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Success!",
                        message: "Thank you for your feedback.",
                        messageTemplate: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Received") + ' {0}',
                        messageTemplateData: [{
                            url: '/case/' + results[1].Id,
                            label: results[1].CaseNumber
                        }],
                        type: "success"
                    });
                    toastEvent.fire();   
                }else{
                    console.log('failure');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: "Failure",
                        message: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Error"),
                        type: "failure"
                    }); 
                    toastEvent.fire();  
                }
            } else {
                console.log('error' + state);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Failure",
                    message: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Error"),
                    type: "failure"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    CancelFeedback : function(component,event,helper){
        if(component.get("v.isModal") == true){
            $A.get("e.force:closeQuickAction").fire();
        }else{
            var elements = document.getElementById("feedback");
            elements.style.display = 'none';
        }
    },
    
    // show spinner: called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.spinner", true); 
    },
    
    // hide spinner call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.spinner", false);
    }
})