({   
    initialize : function(cmp, event, helper) {
        
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	
                cmp.set("v.communitySettings", responseVal.communitySettings);                 
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
        	utils.handleFailure(e.message);
        }    
        
        cmp.set("v.isLoading", false);
	},

    handleSelfRegister: function (cmp, event, helper) {
        console.log("handleSelfRegister");

        // Validations
        var firstName = cmp.get("v.firstName");
        var lastName = cmp.get("v.lastName");
        var email = cmp.get("v.email");
        var phone = cmp.get("v.phone");
        var title = cmp.get("v.title");
        var serialNumber = cmp.get("v.serialNumber");
        var company = cmp.get("v.company");
        var address1 = cmp.get("v.address1");
        var address2 = cmp.get("v.address2");
        var city = cmp.get("v.city");
        var state = cmp.get("v.state");
        var zip = cmp.get("v.zip");
        var country = cmp.get("v.country");
        var assetSite = cmp.get("v.assetSite");
        var asset = cmp.get("v.asset");
        var isValid = true;

        var input = cmp.find("firstname");

        if (!firstName || firstName.length == 0){
              
            input.setCustomValidity("First Name is required.");
            input.reportValidity();
            isValid = false;
        }
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }

        input = cmp.find("lastname");

        if (!lastName || lastName.length == 0){
            
            input.setCustomValidity("Last Name is required.");
            input.reportValidity();
            isValid = false;
        }    
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }        

        input = cmp.find("email");

        if (!email || email.length == 0){
            
            input.setCustomValidity("Email is required.");
            input.reportValidity();
            isValid = false;
        }   
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }

        input = cmp.find("serialnumber");

        if (!serialNumber || serialNumber.length == 0){
            
            input.setCustomValidity("Serial Number is required.");
            input.reportValidity();
            isValid = false;
        } 
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }        

        input = cmp.find("company");

        if (!company || company.length == 0){
            
            input.setCustomValidity("Company is required.");
            input.reportValidity();
            isValid = false;
        }     
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }

        input = cmp.find("address1");

        if (!address1 || address1.length == 0){
            
            input.setCustomValidity("Address is required.");
            input.reportValidity();
            isValid = false;
        }                 
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }

        input = cmp.find("zip");

        if (!zip || zip.length == 0){
            
            input.setCustomValidity("Postal Code is required.");
            input.reportValidity();
            isValid = false;
        }  
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }

        input = cmp.find("country");

        if (!country || country.length == 0){

            input.setCustomValidity("Country is required.");
            input.reportValidity();
            isValid = false;
        }     
        else{
            input.setCustomValidity("");
            input.reportValidity();
        }             

        console.log("isValid: " + isValid);

        if (isValid){
        
            cmp.set("v.isLoading", true);

            var utils = cmp.find("utils");
            var action = cmp.get("c.selfRegister");

            var request = {
                "selfReg": true,
                "firstName": firstName,
                "lastName": lastName,
                "email": email,
                "phone": phone,
                "title": title,
                "serialNumber": serialNumber,
                "company": company,
                "address1": address1,
                "address2": address2,
                "city": city,
                "state": state,
                "zip": zip,
                "country": country,
                "assetSite": assetSite,
                "asset": asset        
            };

            action.setParams({
                "requestStr" : JSON.stringify(request)
            });

            utils.callAction(action, helper.handleSelfRegisterSuccess, helper.handleActionCallFailure, cmp, helper);	 
        }
    },

    handleSelfRegisterSuccess: function(cmp, event, helper, response){
    
		try{
            
            var utils = cmp.find("utils");
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){	
                
                cmp.set("v.errorMessage", "");           
			}
			else{
			
                console.log("failed getInfo response");             
                cmp.set("v.errorMessage", responseVal.errorMessage);
                cmp.set("v.isLoading", false);
			}	
        }
        catch(e){
            console.log("exception: " + e);
            cmp.set("v.errorMessage", e.message);
            cmp.set("v.isLoading", false);
        }                    
    },        

    handleActionCallFailure: function(cmp, event, helper, response){
        console.log("action call failure");
        cmp.set("v.errorMessage", response.getError());
 		cmp.set("v.isLoading", false);   
    }      
})