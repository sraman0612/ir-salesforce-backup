({
    initialize: function(cmp, event, helper) {
        console.log("CTS_IOT_Self_Register:initialize");

        var utils = cmp.find("utils");
        var countryOptions = utils.getCountryOptions(); 

        cmp.set("v.countryOptions", countryOptions);        
        
        // Add a recaptcha message post event handler
        var hostDomain = ".ingersollrand.com";

        window.addEventListener("message", function(event) {
            
            console.log("post message received from domain: " + event.origin);
            
            utils.logObjectToConsole(event.data);

            if (event.origin.indexOf(hostDomain) < 0){
                console.log("Message from unauthorized domain.");
                return;
            }          
            
            let captchEl = document.getElementById("recaptchaFrame");
            
            // Resize the iframe when the challenge is presented
            if (captchEl) {
                      
                if(event.data.captchaVisible === "visible"){
                    captchEl.classList.add("reCaptchaBig");
                    captchEl.classList.remove("reCaptchaSmall");
                } 
                else {
                    captchEl.classList.remove("reCaptchaBig");
                    captchEl.classList.add("reCaptchaSmall");
                }
            }            
            
            // Show the register button upon success
            if (event.data.action == "unlock"){
                console.log("Recaptcha unlock");
                cmp.set("v.recaptchaValidated", true);         
            }
        }, false);        
        
        helper.initialize(cmp, event, helper);
    },

    handleRegister: function(cmp, event, helper){
        console.log("handleRegister");
        helper.handleSelfRegister(cmp, event, helper);
    }
})