({
	initFeedback : function(component, event, helper) {
        var modalBody;
        var modalFooter;
        $A.createComponents([
                ["c:CTS_SubmitCase", {
                    buttonLabel:$A.get("$Label.c.CTS_TechDirect_SendFeedbackButton"),
                    caseType: "IR Comp Tech Direct Site Feedback",
                    caseSubject: "Feedback for Site",
                    isModal : true, 
                    feedbackTextLabel : $A.get("$Label.c.CTS_TechDirect_SiteFeedback_Label"),
                }]
            ],
            function(modalCmps, status, errorMessage) {
                if (status === "SUCCESS") {
                    modalBody = modalCmps[0];
                    modalFooter = modalCmps[1];
                    component.find('overlayLib').showCustomModal({
                        header: $A.get("$Label.c.CTS_TechDirect_SiteFeedback_Heading"),
                        body: modalBody,
                  		showCloseButton: true,
                        cssClass: "slds-modal_large",
                        closeCallback: function() {
                            console.log('modal closed!');
                        }
                    });
                } else if (status === "ERROR") {
                    console.log('ERROR: ', errorMessage);
                }
            }
        )
    }
})