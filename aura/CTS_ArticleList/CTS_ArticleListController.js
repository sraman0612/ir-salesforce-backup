({   
    follow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        action.setParams({recId: recId, action: 'follow'});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", true);
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
    },
    unfollow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        action.setParams({recId: recId, action: 'unfollow'});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", false);
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
    },
    getSearchConfig : function(component, event, helper) {
        helper.getSearchConfig(component, event, helper);
    },
    onRender : function(component, event, helper) {
        var pageSize = component.get("v.pageSize");
        var opt = document.getElementById(pageSize);
        if(opt) opt.selected = true;        
    },
    clearText: function(component, event, helper) {
        if(component.get("v.searchText").length == 0) {
            component.set("v.searchResults", null);
        }
    },
    resetSearch: function(component, event, helper) {        
        helper.resetSearch(component);
    },
    setSubProd: function(component, event, helper) {
        helper.setSubProd(component, event, helper);
    },
    searchTypeAhead : function(component, event, helper) {
        component.set('v.isInitialSearchLoad', false);
        var eventSource = typeof event.getSource === 'undefined' ? '' : event.getSource().get("v.name");
        if(eventSource == 'srchbutton' || event.keyCode == 13) {
            component.set("v.searchOrTypeahead", "search");
            helper.searchKBHelper(component, event, helper);
        } else if(event.keyCode !== 13 && component.get("v.enableTypeAhead")) {
            component.set("v.searchOrTypeahead", "typeahead");
            helper.searchKBHelper(component, event, helper);
        }             
    },
    searchKB : function(component, event, helper) {
        component.set("v.searchOrTypeahead", "search");        
        helper.searchKBHelper(component, event, helper);
    },
    onNext : function(component, event, helper){
        helper.onNext(component, event, helper);
    },
    onPrevious: function(component, event, helper){
        helper.onPrevious(component, event, helper);
    },
    pageSizeChanged: function(component, event, helper){
        helper.pageSizeChanged(component);
    },
    openDetail: function(component, event, helper) {
        helper.openDetail(component, event, helper);
    },    
    handleSearchEvent : function(cmp, event, helper) {
        cmp.set('v.counter', cmp.get('v.counter') + 1);
        var tabName = event.getParam("tabName");
        cmp.set("v.tabName", tabName);
        if(tabName == 'search') {
            cmp.set('v.isInitialSearchLoad', true);
            helper.searchKBHelper(cmp, event, helper);
        } else if(tabName == 'recent') {
            var method = "c.getArticlesOnLoad";
            var parameter = "LastModifiedDate";
            helper.callServer(method, parameter, cmp);
        } else if(tabName == 'viewed') {
            var method = "c.getArticlesOnLoad";
            var parameter = "ArticleTotalViewCount";
            helper.callServer(method, parameter, cmp);
        } else if(tabName == 'helpful') {
            var method = "c.getArticlesOnLoad";
            var parameter = "mostHelpfulArticles";
            helper.callServer(method, parameter, cmp);
        } else if(tabName == 'following') {
            var method = "c.getArticlesOnLoad";
            var parameter = "EntitySubscription";
            helper.callServer(method, parameter, cmp);
        }
    }
})