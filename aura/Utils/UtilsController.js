({
	callAction : function(cmp, event, helper){
		console.log("callAction");
		
		var params = event.getParam('arguments');		
		var action = params.action;
		var successCallback = params.successCallback;
		var errorCallback = params.errorCallback;
		var sourceCmp = params.sourceCmp;
		var sourceHelper = params.sourceHelper;
		
		if (sourceCmp){
			cmp = sourceCmp;
		}
		
		if (sourceHelper){
			helper = sourceHelper;
		}
		
		action.setCallback(this, function(response){

            var state = response.getState();
            
	        if (state === "SUCCESS" && successCallback != null){	            	
	        	successCallback(cmp, event, helper, response);
	        } 
	        else if (errorCallback != null){
	           errorCallback(cmp, event, helper, response);
	        }
        });
        
        console.log("enqueing action");
        $A.enqueueAction(action); 			
	},
	
    getEntityType : function(cmp, event, helper){
        
        var params = event.getParam('arguments');
        var entityId = params['entityId'];
        var resultHandler = params['resultHandler'];
        var entityType;
        
        if (entityId.substring(0, 3) == '00Q'){
            entityType = 'Lead';
        }
        else if (entityId.substring(0, 3) == '003'){
            entityType = 'Contact';
        }
        else if (entityId.substring(0, 3) == '001'){
            entityType = 'Account';
        }
        else if (entityId.substring(0, 3) == '006'){
            entityType = 'Opportunity';
        }        
        
        resultHandler(entityType);        
    },
    
    initInfiniteScroll: function (cmp, event, helper){
    	
    	try{
    	
	    	var params = event.getParam('arguments'); 
	    	var element = params['element'];	
	    
	        // scroll optimizer as defined by MDN
	        // https://developer.mozilla.org/en-US/docs/Web/Events/scroll
	        (function() {
	            var throttle = function(type, name, obj) {
	                var running = false;
	                var func = function() {
	                    if (running) { return; }
	                    running = true;
	                    requestAnimationFrame(function() {
	                        // The event needed to be initialized this way to support IE.
	                        var evt = document.createEvent('CustomEvent');
	                        evt.initEvent(name, true, true);
	                        obj.dispatchEvent(evt);
	                        running = false;
	                    });
	                };
	                obj.addEventListener(type, func);
	            };
	        	
	            throttle('scroll', 'optimizedScroll', element);
	        })();    
	    }
		catch(e){console.log("error during initInfiniteScroll: " + e);}	        
	},
    findElementWithClass: function(cmp, event, helper){
        
    	var params = event.getParam('arguments'); 
    	var triggeredEvent = params['triggeredEvent'];
        var className = params['className'];
        var elementToCheck = triggeredEvent.target.viewportElement != null ? triggeredEvent.target.viewportElement : triggeredEvent.target;
        var elementWithClass;

		while (elementWithClass === undefined){

            if (elementToCheck.classList != undefined){

                for (var i = 0; i < elementToCheck.classList.length; i++){
                    
                    var c = elementToCheck.classList[i];

                    if (c === className){
                        elementWithClass = elementToCheck;
                        break;
                    }
                }              
            }
            
            if (elementWithClass === undefined){
                elementToCheck = elementToCheck.parentElement;
            }   
        }  

        params['resultHandler'](elementWithClass);
    },

	handleFormatDate : function(cmp, event, helper) {
		console.log("handleFormatDate");
		
		var params = event.getParam("arguments");
	    var dt = params.dt;
        var month = "" + (dt.getMonth() + 1);
        var day = "" + dt.getDate();
        var year = dt.getFullYear();
	
	    if (month.length < 2) month = "0" + month;
	    if (day.length < 2) day = "0" + day;
	
	    return [year, month, day].join("-");		
	},
	handleGetQuarter : function(cmp, event, helper){
		
		var params = event.getParam("arguments");
	
		return helper.getQuarter(cmp, event, helper, params.dt); 
	},
	handleGetStartOfQuarter : function(cmp, event, helper){
		console.log("handleGetStartOfQuarter");
		
		var params = event.getParam("arguments");		
		var quarter = helper.getQuarter(cmp, event, helper, params.dt);
		
		console.log("quarter: " + quarter);
		
		var quarterMonthMap = {
		    1 : 0,
		    2 : 3,
		    3 : 6,
		    4 : 9
		};
		
		var month = quarterMonthMap[quarter];
		
		console.log("month: " + month);
		
		params.dt.setMonth(month);
		params.dt.setDate(1);
		
		console.log("final date: " + params.dt);
		
		return params.dt.getFullYear() + "-" + (params.dt.getMonth() + 1) + "-" + params.dt.getDate();		
	},
	handleGetEndOfQuarter : function(cmp, event, helper){
		console.log("handleGetEndOfQuarter");
				
		var params = event.getParam("arguments");				
		var quarter = helper.getQuarter(cmp, event, helper, params.dt);
		
		console.log("quarter: " + quarter);
		
		var quarterMonthMap = {
		    1 : 2,
		    2 : 5,
		    3 : 8,
		    4 : 11
		};
		
		var month = quarterMonthMap[quarter];
		
		console.log("month: " + month);
			
		var month = quarterMonthMap[quarter];
		var day = helper.getDaysInMonth(cmp, event, helper, params.dt);
		
		console.log("day: " + day);
				
		params.dt.setMonth(month);		
		params.dt.setDate(day);
		
		console.log("final date: " + params.dt);
		
		return params.dt.getFullYear() + "-" + (params.dt.getMonth() + 1) + "-" + params.dt.getDate();	
	},
	handleGetDaysInMonth : function(cmp, event, helper){
	
		var params = event.getParam("arguments");	
		return helper.getDaysInMonth(cmp, event, helper, params.dt)
	},
	logObjectToConsole : function(cmp, event, helper){
		
		var params = event.getParam("arguments");
		console.log(JSON.parse(JSON.stringify(params.obj)));
    },
    navigateToCommunityPage : function(cmp, event, helper){

        var params = event.getParam("arguments");
        var navService = params.navService;
        var commPageName = params.commPageName;
        var urlParams = params.urlParams;

        var pageReference = {    
            "type": "comm__namedPage",
            "attributes": {
                "pageName": commPageName   
            },    
            "state": urlParams
        }        
        
        // Work-around since state is not supported in direct component navigation (https://www.wissel.net/blog/2019/03/navigation-in-lightning-communities.html)
        sessionStorage.setItem('localTransfer', JSON.stringify(pageReference.state));
        navService.navigate(pageReference);        
    },
    handleFailure : function(cmp, event, helper){
        
        var params = event.getParam("arguments");
        var errorMessage = params.errorMessage;
        var displayToast = params.displayToast;
        var toastDuration = params.toastDuration;
        var toastType = params.toastType;
        var toastMode = params.toastMode;

        console.log("errorMessage: " + errorMessage);

        if (displayToast){

            var toastEvent = $A.get("e.force:showToast");
                
            toastEvent.setParams({
                mode : toastMode,
                duration : toastDuration,
                message : errorMessage,
                type : toastType			        
            });
            
            toastEvent.fire();      
        }   
    },
    
    getCountryOptions : function(cmp, event, helper){
        
        return [
            {"label": "UNITED STATES", "value": "USA"},
            {"label": "AFGHANISTAN", "value": "AFGHANISTAN"},
            {"label": "ALAND ISLANDS", "value": "ALAND ISLANDS"},
            {"label": "ALBANIA", "value": "ALBANIA"},
            {"label": "ALGERIA", "value": "ALGERIA"},
            {"label": "AMERICAN SAMOA", "value": "AMERICAN SAMOA"},
            {"label": "ANDORRA", "value": "ANDORRA"},
            {"label": "ANGOLA", "value": "ANGOLA"},
            {"label": "ANGUILLA", "value": "ANGUILLA"},
            {"label": "ANTARCTICA", "value": "ANTARCTICA"},
            {"label": "ANTIGUA AND BARBUDA", "value": "ANTIGUA AND BARBUDA"},
            {"label": "ARGENTINA", "value": "ARGENTINA"},
            {"label": "ARMENIA", "value": "ARMENIA"},
            {"label": "ARUBA", "value": "ARUBA"},
            {"label": "AUSTRALIA", "value": "AUSTRALIA"},
            {"label": "AUSTRIA", "value": "AUSTRIA"},
            {"label": "AZERBAIJAN", "value": "AZERBAIJAN"},
            {"label": "BAHAMAS", "value": "BAHAMAS"},
            {"label": "BAHRAIN", "value": "BAHRAIN"},
            {"label": "BANGLADESH", "value": "BANGLADESH"},
            {"label": "BARBADOS", "value": "BARBADOS"},
            {"label": "BELARUS", "value": "BELARUS"},
            {"label": "BELGIUM", "value": "BELGIUM"},
            {"label": "BELIZE", "value": "BELIZE"},
            {"label": "BENIN", "value": "BENIN"},
            {"label": "BERMUDA", "value": "BERMUDA"},
            {"label": "BHUTAN", "value": "BHUTAN"},
            {"label": "BOLIVIA, PLURINATIONAL STATE OF", "value": "BOLIVIA, PLURINATIONAL STATE OF"},
            {"label": "BONAIRE, SINT EUSTATIUS AND SABA", "value": "BONAIRE, SINT EUSTATIUS AND SABA"},
            {"label": "BOSNIA AND HERZEGOVINA", "value": "BOSNIA AND HERZEGOVINA"},
            {"label": "BOTSWANA", "value": "BOTSWANA"},
            {"label": "BOUVET ISLAND", "value": "BOUVET ISLAND"},
            {"label": "BRAZIL", "value": "BRAZIL"},
            {"label": "BRITISH INDIAN OCEAN TERRITORY", "value": "BRITISH INDIAN OCEAN TERRITORY"},
            {"label": "BRUNEI DARUSSALAM", "value": "BRUNEI DARUSSALAM"},
            {"label": "BULGARIA", "value": "BULGARIA"},
            {"label": "BURKINA FASO", "value": "BURKINA FASO"},
            {"label": "BURUNDI", "value": "BURUNDI"},
            {"label": "CAMBODIA", "value": "CAMBODIA"},
            {"label": "CAMEROON", "value": "CAMEROON"},
            {"label": "CANADA", "value": "CANADA"},
            {"label": "CAPE VERDE", "value": "CAPE VERDE"},
            {"label": "CAYMAN ISLANDS", "value": "CAYMAN ISLANDS"},
            {"label": "CENTRAL AFRICAN REPUBLIC", "value": "CENTRAL AFRICAN REPUBLIC"},
            {"label": "CHAD", "value": "CHAD"},
            {"label": "CHILE", "value": "CHILE"},
            {"label": "CHINA", "value": "CHINA"},
            {"label": "CHRISTMAS ISLAND", "value": "CHRISTMAS ISLAND"},
            {"label": "COCOS (KEELING) ISLANDS", "value": "COCOS (KEELING) ISLANDS"},
            {"label": "COLOMBIA", "value": "COLOMBIA"},
            {"label": "COMOROS", "value": "COMOROS"},
            {"label": "CONGO", "value": "CONGO"},
            {"label": "CONGO, THE DEMOCRATIC REPUBLIC OF THE", "value": "CONGO, THE DEMOCRATIC REPUBLIC OF THE"},
            {"label": "COOK ISLANDS", "value": "COOK ISLANDS"},
            {"label": "COSTA RICA", "value": "COSTA RICA"},
            {"label": "COTE D'IVOIRE", "value": "COTE D'IVOIRE"},
            {"label": "CROATIA", "value": "CROATIA"},
            {"label": "CUBA", "value": "CUBA"},
            {"label": "CURACAO", "value": "CURACAO"},
            {"label": "CYPRUS", "value": "CYPRUS"},
            {"label": "CZECH REPUBLIC", "value": "CZECH REPUBLIC"},
            {"label": "DENMARK", "value": "DENMARK"},
            {"label": "DJIBOUTI", "value": "DJIBOUTI"},
            {"label": "DOMINICA", "value": "DOMINICA"},
            {"label": "DOMINICAN REPUBLIC", "value": "DOMINICAN REPUBLIC"},
            {"label": "ECUADOR", "value": "ECUADOR"},
            {"label": "EGYPT", "value": "EGYPT"},
            {"label": "EL SALVADOR", "value": "EL SALVADOR"},
            {"label": "EQUATORIAL GUINEA", "value": "EQUATORIAL GUINEA"},
            {"label": "ERITREA", "value": "ERITREA"},
            {"label": "ESTONIA", "value": "ESTONIA"},
            {"label": "ETHIOPIA", "value": "ETHIOPIA"},
            {"label": "FALKLAND ISLANDS (MALVINAS)", "value": "FALKLAND ISLANDS (MALVINAS)"},
            {"label": "FAROE ISLANDS", "value": "FAROE ISLANDS"},
            {"label": "FIJI", "value": "FIJI"},
            {"label": "FINLAND", "value": "FINLAND"},
            {"label": "FRANCE", "value": "FRANCE"},
            {"label": "FRENCH GUIANA", "value": "FRENCH GUIANA"},
            {"label": "FRENCH POLYNESIA", "value": "FRENCH POLYNESIA"},
            {"label": "FRENCH SOUTHERN TERRITORIES", "value": "FRENCH SOUTHERN TERRITORIES"},
            {"label": "GABON", "value": "GABON"},
            {"label": "GAMBIA", "value": "GAMBIA"},
            {"label": "GEORGIA", "value": "GEORGIA"},
            {"label": "GERMANY", "value": "GERMANY"},
            {"label": "GHANA", "value": "GHANA"},
            {"label": "GIBRALTAR", "value": "GIBRALTAR"},
            {"label": "GREECE", "value": "GREECE"},
            {"label": "GREENLAND", "value": "GREENLAND"},
            {"label": "GRENADA", "value": "GRENADA"},
            {"label": "GUADELOUPE", "value": "GUADELOUPE"},
            {"label": "GUAM", "value": "GUAM"},
            {"label": "GUATEMALA", "value": "GUATEMALA"},
            {"label": "GUERNSEY", "value": "GUERNSEY"},
            {"label": "GUINEA", "value": "GUINEA"},
            {"label": "GUINEA-BISSAU", "value": "GUINEA-BISSAU"},
            {"label": "GUYANA", "value": "GUYANA"},
            {"label": "HAITI", "value": "HAITI"},
            {"label": "HEARD ISLAND AND MCDONALD ISLANDS", "value": "HEARD ISLAND AND MCDONALD ISLANDS"},
            {"label": "HOLY SEE (VATICAN CITY STATE)", "value": "HOLY SEE (VATICAN CITY STATE)"},
            {"label": "HONDURAS", "value": "HONDURAS"},
            {"label": "HONG KONG", "value": "HONG KONG"},
            {"label": "HUNGARY", "value": "HUNGARY"},
            {"label": "ICELAND", "value": "ICELAND"},
            {"label": "INDIA", "value": "INDIA"},
            {"label": "INDONESIA", "value": "INDONESIA"},
            {"label": "IRAN, ISLAMIC REPUBLIC OF", "value": "IRAN, ISLAMIC REPUBLIC OF"},
            {"label": "IRAQ", "value": "IRAQ"},
            {"label": "IRELAND", "value": "IRELAND"},
            {"label": "ISLE OF MAN", "value": "ISLE OF MAN"},
            {"label": "ISRAEL", "value": "ISRAEL"},
            {"label": "ITALY", "value": "ITALY"},
            {"label": "JAMAICA", "value": "JAMAICA"},
            {"label": "JAPAN", "value": "JAPAN"},
            {"label": "JERSEY", "value": "JERSEY"},
            {"label": "JORDAN", "value": "JORDAN"},
            {"label": "KAZAKHSTAN", "value": "KAZAKHSTAN"},
            {"label": "KENYA", "value": "KENYA"},
            {"label": "KIRIBATI", "value": "KIRIBATI"},
            {"label": "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF", "value": "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF"},
            {"label": "KOREA, REPUBLIC OF", "value": "KOREA, REPUBLIC OF"},
            {"label": "KUWAIT", "value": "KUWAIT"},
            {"label": "KYRGYZSTAN", "value": "KYRGYZSTAN"},
            {"label": "LAO PEOPLE'S DEMOCRATIC REPUBLIC", "value": "LAO PEOPLE'S DEMOCRATIC REPUBLIC"},
            {"label": "LATVIA", "value": "LATVIA"},
            {"label": "LEBANON", "value": "LEBANON"},
            {"label": "LESOTHO", "value": "LESOTHO"},
            {"label": "LIBERIA", "value": "LIBERIA"},
            {"label": "LIBYA", "value": "LIBYA"},
            {"label": "LIECHTENSTEIN", "value": "LIECHTENSTEIN"},
            {"label": "LITHUANIA", "value": "LITHUANIA"},
            {"label": "LUXEMBOURG", "value": "LUXEMBOURG"},
            {"label": "MACAO", "value": "MACAO"},
            {"label": "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF", "value": "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF"},
            {"label": "MADAGASCAR", "value": "MADAGASCAR"},
            {"label": "MALAWI", "value": "MALAWI"},
            {"label": "MALAYSIA", "value": "MALAYSIA"},
            {"label": "MALDIVES", "value": "MALDIVES"},
            {"label": "MALI", "value": "MALI"},
            {"label": "MALTA", "value": "MALTA"},
            {"label": "MARSHALL ISLANDS", "value": "MARSHALL ISLANDS"},
            {"label": "MARTINIQUE", "value": "MARTINIQUE"},
            {"label": "MAURITANIA", "value": "MAURITANIA"},
            {"label": "MAURITIUS", "value": "MAURITIUS"},
            {"label": "MAYOTTE", "value": "MAYOTTE"},
            {"label": "MEXICO", "value": "MEXICO"},
            {"label": "MICRONESIA, FEDERATED STATES OF", "value": "MICRONESIA, FEDERATED STATES OF"},
            {"label": "MOLDOVA, REPUBLIC OF", "value": "MOLDOVA, REPUBLIC OF"},
            {"label": "MONACO", "value": "MONACO"},
            {"label": "MONGOLIA", "value": "MONGOLIA"},
            {"label": "MONTENEGRO", "value": "MONTENEGRO"},
            {"label": "MONTSERRAT", "value": "MONTSERRAT"},
            {"label": "MOROCCO", "value": "MOROCCO"},
            {"label": "MOZAMBIQUE", "value": "MOZAMBIQUE"},
            {"label": "MYANMAR", "value": "MYANMAR"},
            {"label": "NAMIBIA", "value": "NAMIBIA"},
            {"label": "NAURU", "value": "NAURU"},
            {"label": "NEPAL", "value": "NEPAL"},
            {"label": "NETHERLANDS", "value": "NETHERLANDS"},
            {"label": "NEW CALEDONIA", "value": "NEW CALEDONIA"},
            {"label": "NEW ZEALAND", "value": "NEW ZEALAND"},
            {"label": "NICARAGUA", "value": "NICARAGUA"},
            {"label": "NIGER", "value": "NIGER"},
            {"label": "NIGERIA", "value": "NIGERIA"},
            {"label": "NIUE", "value": "NIUE"},
            {"label": "NORFOLK ISLAND", "value": "NORFOLK ISLAND"},
            {"label": "NORTHERN MARIANA ISLANDS", "value": "NORTHERN MARIANA ISLANDS"},
            {"label": "NORWAY", "value": "NORWAY"},
            {"label": "OMAN", "value": "OMAN"},
            {"label": "PAKISTAN", "value": "PAKISTAN"},
            {"label": "PALAU", "value": "PALAU"},
            {"label": "PALESTINIAN TERRITORY, OCCUPIED", "value": "PALESTINIAN TERRITORY, OCCUPIED"},
            {"label": "PANAMA", "value": "PANAMA"},
            {"label": "PAPUA NEW GUINEA", "value": "PAPUA NEW GUINEA"},
            {"label": "PARAGUAY", "value": "PARAGUAY"},
            {"label": "PERU", "value": "PERU"},
            {"label": "PHILIPPINES", "value": "PHILIPPINES"},
            {"label": "PITCAIRN", "value": "PITCAIRN"},
            {"label": "POLAND", "value": "POLAND"},
            {"label": "PORTUGAL", "value": "PORTUGAL"},
            {"label": "PUERTO RICO", "value": "PUERTO RICO"},
            {"label": "QATAR", "value": "QATAR"},
            {"label": "REUNION", "value": "REUNION"},
            {"label": "ROMANIA", "value": "ROMANIA"},
            {"label": "RUSSIAN FEDERATION", "value": "RUSSIAN FEDERATION"},
            {"label": "RWANDA", "value": "RWANDA"},
            {"label": "SAINT BARTHELEMY", "value": "SAINT BARTHELEMY"},
            {"label": "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA", "value": "SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA"},
            {"label": "SAINT KITTS AND NEVIS", "value": "SAINT KITTS AND NEVIS"},
            {"label": "SAINT LUCIA", "value": "SAINT LUCIA"},
            {"label": "SAINT MARTIN (FRENCH PART)", "value": "SAINT MARTIN (FRENCH PART)"},
            {"label": "SAINT PIERRE AND MIQUELON", "value": "SAINT PIERRE AND MIQUELON"},
            {"label": "SAINT VINCENT AND THE GRENADINES", "value": "SAINT VINCENT AND THE GRENADINES"},
            {"label": "SAMOA", "value": "SAMOA"},
            {"label": "SAN MARINO", "value": "SAN MARINO"},
            {"label": "SAO TOME AND PRINCIPE", "value": "SAO TOME AND PRINCIPE"},
            {"label": "SAUDI ARABIA", "value": "SAUDI ARABIA"},
            {"label": "SENEGAL", "value": "SENEGAL"},
            {"label": "SERBIA", "value": "SERBIA"},
            {"label": "SEYCHELLES", "value": "SEYCHELLES"},
            {"label": "SIERRA LEONE", "value": "SIERRA LEONE"},
            {"label": "SINGAPORE", "value": "SINGAPORE"},
            {"label": "SINT MAARTEN (DUTCH PART)", "value": "SINT MAARTEN (DUTCH PART)"},
            {"label": "SLOVAKIA", "value": "SLOVAKIA"},
            {"label": "SLOVENIA", "value": "SLOVENIA"},
            {"label": "SOLOMON ISLANDS", "value": "SOLOMON ISLANDS"},
            {"label": "SOMALIA", "value": "SOMALIA"},
            {"label": "SOUTH AFRICA", "value": "SOUTH AFRICA"},
            {"label": "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS", "value": "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS"},
            {"label": "SOUTH SUDAN", "value": "SOUTH SUDAN"},
            {"label": "SPAIN", "value": "SPAIN"},
            {"label": "SRI LANKA", "value": "SRI LANKA"},
            {"label": "SUDAN", "value": "SUDAN"},
            {"label": "SURINAME", "value": "SURINAME"},
            {"label": "SVALBARD AND JAN MAYEN", "value": "SVALBARD AND JAN MAYEN"},
            {"label": "SWAZILAND", "value": "SWAZILAND"},
            {"label": "SWEDEN", "value": "SWEDEN"},
            {"label": "SWITZERLAND", "value": "SWITZERLAND"},
            {"label": "SYRIAN ARAB REPUBLIC", "value": "SYRIAN ARAB REPUBLIC"},
            {"label": "TAIWAN, PROVINCE OF CHINA", "value": "TAIWAN, PROVINCE OF CHINA"},
            {"label": "TAJIKISTAN", "value": "TAJIKISTAN"},
            {"label": "TANZANIA, UNITED REPUBLIC OF", "value": "TANZANIA, UNITED REPUBLIC OF"},
            {"label": "THAILAND", "value": "THAILAND"},
            {"label": "TIMOR-LESTE", "value": "TIMOR-LESTE"},
            {"label": "TOGO", "value": "TOGO"},
            {"label": "TOKELAU", "value": "TOKELAU"},
            {"label": "TONGA", "value": "TONGA"},
            {"label": "TRINIDAD AND TOBAGO", "value": "TRINIDAD AND TOBAGO"},
            {"label": "TUNISIA", "value": "TUNISIA"},
            {"label": "TURKEY", "value": "TURKEY"},
            {"label": "TURKMENISTAN", "value": "TURKMENISTAN"},
            {"label": "TURKS AND CAICOS ISLANDS", "value": "TURKS AND CAICOS ISLANDS"},
            {"label": "TUVALU", "value": "TUVALU"},
            {"label": "UGANDA", "value": "UGANDA"},
            {"label": "UKRAINE", "value": "UKRAINE"},
            {"label": "UNITED ARAB EMIRATES", "value": "UNITED ARAB EMIRATES"},
            {"label": "UNITED KINGDOM", "value": "UNITED KINGDOM"},
            {"label": "UNITED STATES MINOR OUTLYING ISLANDS", "value": "UNITED STATES MINOR OUTLYING ISLANDS"},
            {"label": "URUGUAY", "value": "URUGUAY"},
            {"label": "UZBEKISTAN", "value": "UZBEKISTAN"},
            {"label": "VANUATU", "value": "VANUATU"},
            {"label": "VENEZUELA, BOLIVARIAN REPUBLIC OF", "value": "VENEZUELA, BOLIVARIAN REPUBLIC OF"},
            {"label": "VIET NAM", "value": "VIET NAM"},
            {"label": "VIRGIN ISLANDS, BRITISH", "value": "VIRGIN ISLANDS, BRITISH"},
            {"label": "VIRGIN ISLANDS, U.S.", "value": "VIRGIN ISLANDS, U.S."},
            {"label": "WALLIS AND FUTUNA", "value": "WALLIS AND FUTUNA"},
            {"label": "WESTERN SAHARA", "value": "WESTERN SAHARA"},
            {"label": "YEMEN", "value": "YEMEN"},
            {"label": "ZAMBIA", "value": "ZAMBIA"},
            {"label": "ZIMBABWE", "value": "ZIMBABWE"}
        ];
    }
})