({
    getLanguage: function(returnList) {
        for(var i = 0; i < returnList.length; i++) {
            returnList[i].TitleURL = '/one/one.app?#/sObject/'+ returnList[i].Id + '/view';
            if(returnList[i].Language == 'en_US') {                        
                returnList[i].Language = 'English';
            } else if(returnList[i].Language == 'zh_CN') {
                returnList[i].Language = 'Chinese (Simplified)';
            } else if(returnList[i].Language == 'zh_TW') {
                returnList[i].Language = 'Chinese (Traditional)';
            } else if(returnList[i].Language == 'nl_NL') {
                returnList[i].Language = 'Dutch';
            } else if(returnList[i].Language == 'da') {
                returnList[i].Language = 'Danish';
            } else if(returnList[i].Language == 'fi') {
                returnList[i].Language = 'Finnish';
            } else if(returnList[i].Language == 'fr') {
                returnList[i].Language = 'French';
            } else if(returnList[i].Language == 'de') {
                returnList[i].Language = 'German';
            } else if(returnList[i].Language == 'it') {
                returnList[i].Language = 'Italian';
            } else if(returnList[i].Language == 'ja') {
                returnList[i].Language = 'Japanese';
            } else if(returnList[i].Language == 'ko') {
                returnList[i].Language = 'Korean';
            } else if(returnList[i].Language == 'no') {
                returnList[i].Language = 'Norwegian';
            } else if(returnList[i].Language == 'pt_BR') {
                returnList[i].Language = 'Portuguese (Brazil)';
            } else if(returnList[i].Language == 'ru') {
                returnList[i].Language = 'Russian';
            } else if(returnList[i].Language == 'es') {
                returnList[i].Language = 'Spanish';
            } else if(returnList[i].Language == 'es_MX') {
                returnList[i].Language = 'Spanish (Mexico)';
            } else if(returnList[i].Language == 'sv') {
                returnList[i].Language = 'Swedish';
            } else if(returnList[i].Language == 'th') {
                returnList[i].Language = 'Thai';
            } 
        }
    },
    getData: function(component) {
        var action = component.get("c.getAllArticles");        
        action.setParams({
            "publishStatus" : "Online"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.articles", response.getReturnValue());
            }
        }); 
        $A.enqueueAction(action);
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.articles");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        component.set("v.articles", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    }
})