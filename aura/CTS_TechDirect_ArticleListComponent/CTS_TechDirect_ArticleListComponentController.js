({
	doInit : function(component, event, helper) {        
        component.set('v.mycolumn', [
            {label: 'Article Number', fieldName: 'ArticleNumber', type: 'text', sortable: 'true'},
            {label: 'Title', fieldName: 'TitleURL', type: 'url', sortable: 'true', typeAttributes: {
                label: { fieldName: 'Title' }
            }},
            {label: 'Article Type', fieldName: 'CTS_TechDirect_Article_Type__c', type: 'text', sortable: 'true'},
            {label: 'Validation Status', fieldName: 'ValidationStatus', type: 'text', sortable: 'true'},
            {label: 'Language', fieldName: 'Language', type: 'text', sortable: 'true'}
        ]);
        
        var action = component.get("c.getAllArticles");        
        action.setParams({
            "validationStatus" : "Publish",
            "searchKeyword" : "",
            "language" : "en_US",
            "product" : "",
            "category" : ""
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnList = response.getReturnValue();
                helper.getLanguage(returnList);
                component.set("v.articles", returnList);                
            }
        });         
        $A.enqueueAction(action);
	},    
    searchArticles : function(component, event, helper) {
        var validationStatus = event.getParam("validationStatus");
        var keywordValue = event.getParam("searchKeyword");
        var languageValue = event.getParam("language");
        var productValue = event.getParam("product");
        var categoryValue = event.getParam("category");
        // set the handler attributes based on event data        
        var action = component.get("c.getAllArticles");
        action.setParams({
            "validationStatus" : validationStatus,
            "searchKeyword" : keywordValue,
            "language" : languageValue,
            "product" : productValue,
            "category" : categoryValue
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnList = response.getReturnValue();
                helper.getLanguage(returnList);
                component.set("v.articles", returnList);
            }
        }); 
        $A.enqueueAction(action);
    },
    updateColumnSorting: function (component, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        component.set("v.sortedBy", fieldName);
        component.set("v.sortedDirection", sortDirection);
        helper.sortData(component, fieldName, sortDirection);
    },
    getSelectedName: function (component, event) {        
        var selectedRows = event.getParam('selectedRows');
        for (var i = 0; i < selectedRows.length; i++){
           console.log(selectedRows[i].Id);
        }       
    }
})