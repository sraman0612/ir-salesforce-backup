({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            searchTerm : cmp.get("v.searchTerm"),
            pageSize : cmp.get("v.pageSize"),
            offSet : cmp.get("v.offSet"),
            totalRecordCount: cmp.get("v.totalRecordCount")
        });

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	
    },

    handleGetInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		
                cmp.set("v.siteDetails", responseVal.siteDetails);
                cmp.set("v.isSuperUser", responseVal.isSuperUser);
                cmp.set("v.communitySettings", responseVal.communitySettings);
                cmp.set("v.totalRecordCount", responseVal.totalRecordCount);   
                
                if (responseVal.userSettings.User_Selected_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.userSettings.User_Selected_List_Page_Size__c);
                }
                else if (responseVal.communitySettings.Default_List_Page_Size__c){
                    cmp.set("v.pageSize", responseVal.communitySettings.Default_List_Page_Size__c);
                }
                else{
                    cmp.set("v.pageSize", 20);
                }                  
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
        	utils.handleFailure(e.message);
        }    
        
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})