({
    initialize : function(cmp, event, helper) {

        cmp.set("v.isLoading", true);
		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        utils.callAction(action, helper.handleGetInitSuccess, helper.handleActionCallFailure, cmp, helper);	     
    },

    handleGetInitSuccess: function(cmp, event, helper, response){        

        var utils = cmp.find("utils");

		try{
            
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);

            if (responseVal.success){   
                cmp.set("v.communitySettings", responseVal.communitySettings);              
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);
        }    
       
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})