({
	doInit : function(cmp, event, helper) {
		var action = cmp.get("c.checkOwner");
		cmp.set("v.hideSpinner", true);
        action.setParams({ accountId : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                //if(response.getReturnValue()){
		            cmp.set("v.ownerCheck", response.getReturnValue());
                    cmp.set("v.hideSpinner", false);
                //}
            }
            else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	},

    markAccountForDeletionCtr : function(cmp, event, helper) {
        event.preventDefault();
		var action = cmp.get("c.markAccountForDeletion");
        action.setParams({ accountId : cmp.get("v.recordId") });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
                    
                    cmp.set("v.hasSameBuisness", response.getReturnValue());
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    window.location = "/lightning/o/Account/list?filterName=Recent";
                    console.log("Successfully Deleted!!");
                }
                
            }
            else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });

        $A.enqueueAction(action);
	}
})