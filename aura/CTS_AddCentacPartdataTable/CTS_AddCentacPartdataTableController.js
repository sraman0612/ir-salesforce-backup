({
    /*Create formated data list from data received from Parent component */
    doInit : function(component, event, helper) {
        var navService = component.find("navService");
        var centacQuoteId = component.get("v.centacQuoteId");
        // Sets the route to /lightning/o/CTS_Centac_Parts_Quote_ReRate__c/home
        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": centacQuoteId,
                "objectApiName": "CTS_Centac_Parts_Quote_ReRate__c",
                "actionName": "view"
            }
        };
        component.set("v.pageReference", pageReference);
        helper.setupTable(component);
    },
    
    /*Method get called when user sort table*/
    sortTable : function(component, event, helper) {
        component.set("v.isLoading", true);
        setTimeout(function(){
            var childObj = event.target;
            var parObj = childObj.parentNode;
            while(parObj.tagName != 'TH') {
                parObj = parObj.parentNode;
            }
            var sortBy = parObj.name, //event.getSource().get("v.name"),
                sortDirection = component.get("v.sortDirection"),
                sortDirection = sortDirection === "asc" ? "desc" : "asc"; //change the direction for next time
            
            component.set("v.sortBy", sortBy);
            component.set("v.sortDirection", sortDirection);
            helper.sortData(component, sortBy, sortDirection);
            component.set("v.isLoading", false);
        }, 0);
    },
    /*Method to calculate increase in width of a column on on mouse down activity */
    calculateWidth : function(component, event, helper) {
        var childObj = event.target;
        var parObj = childObj.parentNode;
        var startOffset = parObj.offsetWidth - event.pageX;
        component.set("v.startOffset", startOffset);
    },
    /*Method is called when table column is draged */
    setNewWidth : function(component, event, helper) {
        var childObj = event.target;
        var parObj = childObj.parentNode;
        while(parObj.tagName != 'TH') {
            parObj = parObj.parentNode;
        }
        var startOffset = component.get("v.startOffset");
        var newWidth = startOffset + event.pageX;
        parObj.style.width = newWidth+'px';
    },
    
    /*Method to enable edit mode called on edit button*/
    editField : function(component, event, helper) {
        var field = event.getSource(),
            indexes = field.get("v.name"),
            rowIndex = indexes.split('-')[0],
            colIndex = indexes.split('-')[1];
        var data = component.get("v.tableData");
        /*field attributes are set to enable input field and disable output field*/
        data[rowIndex].fields[colIndex].mode = 'edit';
        data[rowIndex].fields[colIndex].tdClassName = 'slds-cell-edit slds-is-edited';
        component.set("v.tableData", data);        
        component.set("v.isEditModeOn", true);
    },
    
    /*Method called on input change*/
    onInputChange : function(component, event, helper){
        var field = event.getSource(),
            value = field.get("v.value"),
            indexes = field.get("v.name"),
            rowIndex = indexes.split('-')[0],
            colIndex = indexes.split('-')[1];
        helper.updateTable(component, rowIndex, colIndex, value);
    },
    
    /*Method to disable edit mode called on cancel button*/
    closeEditMode : function(component, event, helper){
        component.set("v.isLoading", true);
        var dataCache = component.get("v.dataCache");
        var originalData = component.get("v.tableDataOriginal");
        /*Reinstantiate all table attribute with original data*/
        component.set("v.data", JSON.parse(JSON.stringify(dataCache)));
        helper.setupData(component, dataCache); 
        //component.set("v.tableData", JSON.parse(JSON.stringify(originalData)));
        component.set("v.isEditModeOn", false);
        component.set("v.isLoading", false);
        component.set("v.error", "");
    },
    
    /*Method called on Save button*/
    saveRecords : function(component, event, helper){
        component.set("v.isLoading", true);
        var isUpdateTab = component.get("v.isUpdateTab");
        var records,isValidData,tableData;
        records = component.get("v.modifiedRecords");
        isValidData = helper.validateRecords(component,records);
        if(isUpdateTab){
            tableData = component.get("v.tableData");
            var listUpdateComplete = helper.validateTableData(component,tableData);
            component.set('v.listUpdateComplete',listUpdateComplete);
        }
        if(isValidData){
            /*Loop to delete all non salesforce record Id for new records*/
            records.forEach(function(value, index) {
                var recordId = (value.id).toString();
                console.log(recordId.length<15);
                if(recordId.length<15){ 
                    delete value.id ;
                    
                } 
                
                
            });
            /*Intantiate Save Event variable*/
            var saveEvent = component.getEvent("CTS_Centac_Part_SaveEvent");
            /*Set Event Parameter */
            saveEvent.setParams({
                listUpdateComplete:component.get("v.listUpdateComplete"),//Work in case of update tab only tell whether update tab's list update complete or not
                tableAuraId: component.get("v.auraId"),//Child component Id
                recordsString: JSON.stringify(records),//modified records
                deletedRecordsId :component.get("v.deletedRecordsId")//deleted records id if any
            });
            /*Call Parent's saveTableRecords method*/
            saveEvent.fire();
            
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title : 'Error',
                message:'Please fill mandatory fields',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'dismissible'
            });
            toastEvent.fire();
            component.set("v.isLoading", false); 
        }
        
        
    },
    /*Method to add new empty row to datatable */
    addRow:function(component,event,helper){
        var centacPartsList = component.get("v.tableData");
        var centacQuoteId = component.get("v.centacQuoteId");
        var cols = component.get("v.columns");
        var newRow = [];
        //Add New CentacPart Record
        newRow.push({
            Id: centacPartsList.length+1
            
        });
        centacPartsList.push(helper.initRow(newRow[0], cols));
        component.set("v.tableData",centacPartsList);
        
        
        
    },
    /*Method called onclick of delete button*/
    remove:function(component,event,helper){
        var rowIndex = event.target.id; //rowIndex-colIndex-actionName
        /*Helper method to delete row*/
        helper.removeRow(component, rowIndex);
        component.set("v.isEditModeOn", true);
    },
    /*Method called from parent component for postprocessing after save */
    finishSaving : function(component, event, helper){
        var params = event.getParam('arguments');
        if(params){
            var result = params.result, //Valid values are "SUCCESS" or "ERROR"
                data = params.data, //refreshed data from server
                message = params.message;
            console.log()
            if(result === "SUCCESS"){//success
                if(data){
                    helper.setupData(component, data);
                }else{
                    
                    /*If data is not sent from calling component then old data is restored*/
                    var dataCache = component.get("v.dataCache");
                    helper.setupData(component, dataCache);                  
                }
                
            }else{
                if(message){
                    component.set("v.error", message);
                } 
                /*If error from server then old data is restored*/
                var dataCache = component.get("v.dataCache");
                helper.setupData(component, dataCache);    
            }
        }
        component.set("v.isEditModeOn", false);
        component.set("v.isLoading", false);
        var navService = component.find("navService");
        // Uses the pageReference definition in the init handler
        var pageReference = component.get("v.pageReference");
        event.preventDefault();
        navService.navigate(pageReference);
        
        
        //$A.get('e.force:refreshView').fire();
        
    }    
})