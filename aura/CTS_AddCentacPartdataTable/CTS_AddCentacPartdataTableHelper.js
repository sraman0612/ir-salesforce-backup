({
    /*Set up formated data and column List*/
    setupTable : function(component){
        var cols = component.get("v.columns"),
            data = component.get("v.data");
        this.setupColumns(component, cols);
        this.setupData(component, data);
        component.set("v.isLoading", false);
    },
    
    /* Depending on attributes of columns 
     * received from parent component 
     * slds classes are set for 
     * columns attribute which is used to 
     * display columns in data table
     */
    setupColumns : function(component, cols){
        var tempCols = [];
        if(cols){
            cols.forEach(function(col) {
                //set col values
                col.isMandatory = col.mandatory;
                col.thClassName = "slds-truncate";
                col.thClassName += col.sortable === true ? " slds-is-sortable" : "";
                col.thClassName += col.resizable === true ? " slds-is-resizable" : "";
                col.style = col.width ? "width:"+col.width+"px;" : "";
                col.style += col.minWidth ? "min-width:"+col.minWidth+"px;" : "";
                col.style += col.maxWidth ? "max-width:"+col.maxWidth+"px;" : "";
                if(col.sortable === true){
                    col.sortBy = col.fieldName;
                    if(col.type === "link" && col.attributes && typeof col.attributes.label === "object")
                        col.sortBy = col.attributes.label.fieldName;
                }
                //if(!tableData) col.thClassName = "";
                tempCols.push(col);
            });
            component.set("v.columns", JSON.parse(JSON.stringify(tempCols)));
        }
    },
    
    /* Depending on type of field, 
     * format data in list of rows which in turn contain list of fields
     * received from parent component 
     */
    setupData : function(component, data){        
        var tableData = [], cols = component.get("v.columns");
        component.set("v.dataCache", JSON.parse(JSON.stringify(data)));
        if(data){
            /*Iterate over List of record of Data*/
            for(var i=0; i<data.length;i++){
                var row = this.initRow(data[i],cols);
                tableData.push(row);
            }
            component.set("v.tableData", tableData);
            component.set("v.tableDataOriginal", tableData);            
            var recList = [];
            component.set("v.modifiedRecords",recList);
            component.set("v.deletedRecordsId",recList);
        }
    },
    
    /*Method to initialize a row*/
    initRow : function(value,cols){
        var row = {}, fields = [];
        /*Iterate over each field of record*/
        cols.forEach(function(col) {
            /*set format of data values*/
            var field = {};
            field.name = col.fieldName;
            field.isMandatory = col.mandatory;
            field.value = value[col.fieldName];
            field.type = col.type ? col.type : "text";
            if(field.type === "date"){
                field.isViewSpecialType = true;
            }
            if(field.type === "number"){
                field.isViewSpecialType = true;
                if(col.attributes){
                    field.formatter = col.attributes.formatter;
                    field.style = col.attributes.formatter;
                    field.minimumFractionDigits = col.attributes.minimumFractionDigits ? col.attributes.minimumFractionDigits : 0;
                    field.maximumFractionDigits = col.attributes.maximumFractionDigits ? col.attributes.maximumFractionDigits : 2;
                    field.currencyCode = col.attributes.currencyCode ? col.attributes.currencyCode : "USD";
                }
            }
            if(field.type === "picklist"){
                field.isEditSpecialType = true;
                field.selectOptions = col.selectOptionsList;
            }                        
            if(field.type === "link"){
                field.isViewSpecialType = true;
                if(col.attributes){
                    if(typeof col.attributes.label === "object")
                        field.label = value[col.attributes.label.fieldName];
                    else field.label = col.attributes.label;
                    
                    if(typeof col.attributes.title === "object")
                        field.title = value[col.attributes.title.fieldName];
                    else field.title = col.attributes.title;
                    
                    if(col.attributes.actionName){
                        field.type = "link-action";
                        field.actionName = col.attributes.actionName;
                    }                                
                    field.target = col.attributes.target;
                }
            }
            field.editable = col.editable ? col.editable : false;
            field.tdClassName = field.editable === true ? 'slds-cell-edit' : '';
            field.mode = "view";
            field.hasError = false;
            fields.push(field);
        });
        /*Set Row id with data Id */
        row.id = value.Id;
        row.fields = fields;
        row.hasError = false;
        return row;
    },    
    
    
    /*Method get called when user sort table*/
    sortData : function(component, sortBy, sortDirection){
        var reverse = sortDirection !== "asc",
            data = component.get("v.dataCache");
        if(!data) return;
        
        var data = Object.assign([], data.sort(this.sortDataBy(sortBy, reverse ? -1 : 1)));
        this.setupData(component, data);
    },
    /*Method to sort data by mentioned field*/
    sortDataBy : function (field, reverse, primer) {
        var key = primer
        ? function(x) { return primer(x[field]) }
        : function(x) { return x[field] };
        
        return function (a, b) {
            var A = key(a);
            var B = key(b);
            return reverse * ((A > B) - (B > A));
        };
    },
    
    /*Method to update modifiedRecords attribute with modified values of a row and row Id*/
    updateTable : function(component, rowIndex, colIndex, value){
        var rowIndexSet = new Set(component.get("v.modifiedRowIndex"));
        rowIndexSet.add(rowIndex);
        component.set("v.modifiedRowIndex",rowIndexSet);
        //Update Displayed Data
        var data = component.get("v.tableData");
        data[rowIndex].fields[colIndex].value = value;
        component.set("v.tableData", data);
        //Update modified records which will be used to update corresponding salesforce records
        var records = component.get("v.modifiedRecords");
        var recIndex = records.findIndex(rec => rec.id === data[rowIndex].id);
        if(recIndex !== -1){
            records[recIndex][""+data[rowIndex].fields[colIndex].name] = value;
        }else{
            var obj = {};
            obj["rowIndex"]=rowIndex;
            obj["id"] = data[rowIndex].id;
            obj[""+data[rowIndex].fields[colIndex].name] = value;
            records.push(obj);
        }
        component.set("v.modifiedRecords", records);
    },
    /*Method to remove row from datatable */
    removeRow: function (component, rowIndex) {
        var data = component.get('v.tableData');
        var recordId = (data[rowIndex].id).toString();
        
        var modifiedRecords = component.get("v.modifiedRecords");
        var modifiedRecIndex = modifiedRecords.findIndex(rec => rec.id === data[rowIndex].id);
        //delete row from datatable
        data.splice(rowIndex, 1);
        component.set("v.tableData", data);
        
        //delete row from  modifiedRecord List is available
        if(modifiedRecIndex!=-1){
            modifiedRecords.splice(modifiedRecIndex,1);
            
        }
        //realign modified record index with table data after deletion
        data.forEach(function(value,index){
            var changedRecIndex = modifiedRecords.findIndex(rec => rec.id === value.id);
            if(changedRecIndex!=-1){
                modifiedRecords[changedRecIndex].rowIndex = index;    
            }
            
        });
        component.set("v.modifiedRecords", modifiedRecords);
        //add deleted row id to deleted records list if it is existing salesforce record
        if(recordId.length>15){
            var deletedRecIds = component.get("v.deletedRecordsId");
            deletedRecIds.push(recordId);
            component.set("v.deletedRecordsId",deletedRecIds);
        }
        
        
    },
    //Validate mandatory fields of Add Parts tab,only mandatory fields are scanned for not null mandatory fields
    validateRecords:function(component,recordsToValidate){
        var data = component.get('v.tableData');
        var invalidData = false;
        recordsToValidate.forEach(function(value, index) {
            var rowIndex = (value.rowIndex);
            var rowFields = data[rowIndex].fields;
            var rowHasError = false;
            rowFields.forEach(function(field,index){                
                console.log('field------>',field);
                if(field.isMandatory && (field.value==null|| field.value==''|| field.value == "[]" || field.value == "{}")){
                    field.mode = 'edit';
                    rowHasError = true;
                    invalidData = true;                    
                }else{
                    field.mode = 'view';
                }
            });
            data[rowIndex].fields = rowFields;
            data[rowIndex].hasError = rowHasError;
        });
        component.set('v.tableData',data);
        if(invalidData){
            component.set("v.isEditModeOn", true);
            return false;
            
        }else{
            return true;
        }
        
    },
    //Validate mandatory fields of Update Parts tab, complete data is scan for not null mandatory fields
    validateTableData:function(component,data){
        var invalidData = false;
        data.forEach(function(value, index) {
            var rowFields = value.fields;
            rowFields.forEach(function(field,index){                
                console.log('field------>',field);
                if(field.isMandatory && (field.value==null|| field.value==''|| field.value == "[]" || field.value == "{}")){
                    invalidData = true;                    
                }
            });
        });
        if(invalidData){
            return false;
        }else{
            return true;
        }
        
    }
})