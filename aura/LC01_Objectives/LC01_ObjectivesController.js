({

	init : function(component, event, helper) {
		// console.log("in init");

		// var recordId = component.get("v.recordId");
		var action = component.get("c.retrieveObjectives");

		action.setParams({
			"accountId": component.get("v.accountId"),
			"pt_dsmp_id": component.get("v.pt_dsmp_id")
		});  
		action.setCallback(this, function(response) {
			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
				console.log('mgr result ', result);
				
				if(result.error){
                    // console.log('mgr 1');

				} else if(!result.error){
                    // console.log('mgr 2--end');

					component.set("v.lstSuggestedObj", result.lstSuggestedObj);
					component.set("v.lstFreeObj", result.lstFreeObj);

                } 
                
			}

		});
 
		$A.enqueueAction(action);

	},


	saveObjectives : function(component, event, helper){
		// console.log("in saveObjectives");

		var lstObjective = component.get("v.lstFreeObj");
		var lstSuggestedObjective = component.get("v.lstSuggestedObj");

		// console.log('lstObjective ', lstObjective);

		var action = component.get("c.updateObjectives");
		action.setParams({"lstObjective": lstObjective,
						  "lstSuggested": lstSuggestedObjective});  

		action.setCallback(this, function(response) {
			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
				// console.log('mgr result ', result);
				
				if(result.error){
                    // console.log('mgr 1');

                    var toastEvent = $A.get("e.force:showToast");
			        toastEvent.setParams({
			         "type" : "error",
			            "title": "Error!",
			            "message": result.message,
			            "mode" : "sticky"
			        });
			        toastEvent.fire();   

				} else if(!result.error){
                    // console.log('mgr 2--end');

                 	var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type" : "success",
                        "title": "Success!",
                        "message": result.message
                    });
                    toastEvent.fire();

                } 
                
			}

		});
 
		$A.enqueueAction(action);


	}

})