({
    init: function (cmp, event, helper) {
        console.log("CTS_IOT_Navigation:init");    
                
        var tabUrlMap = {
            "HOME" : "/",
            "COMPRESSED AIR EQUIPMENT" : "/my-equipment",
            "ACCESS MANAGEMENT" : "/administration",
            "REAL-TIME DATA" : "/iot-dashboard"
        };        

        cmp.set("v.tabUrlMap", tabUrlMap);

        helper.initialize(cmp, event, helper);
    },
    onMenuItemsChange : function(cmp, event, helper){
        console.log("onMenuItemsChange");
        
        var utils = cmp.find("utils");
        var breadcrumbCollection = [];
        var menuItems = cmp.get("v.menuItems");
        var oldMenuItems = event.getParam("oldValue");
        var activeMenuItem = {};

        if (menuItems && menuItems.length > 0){
            
            cmp.set("v.isLoading", true);

            var activeTabChange = false;

            for (var i = 0; i < menuItems.length; i++){

                var menuItem = menuItems[i];

                var oldMenuItem = oldMenuItems && oldMenuItems.length > 0 ? oldMenuItems[i] : null;

                if (oldMenuItem != null && menuItem.active != oldMenuItem.active){
                    activeTabChange = true;
                }

                if (menuItem.active){
                    activeMenuItem = menuItem;
                    cmp.set("v.activeTabLabel", menuItem.label);
                    break;
                }
            }            

            console.log("activeTabChange: " + activeTabChange);

            if (activeMenuItem && activeMenuItem.label && activeTabChange){
                          
                var breadcrumb = {"label" : activeMenuItem.label.toUpperCase()};
                var tabUrlMap = cmp.get("v.tabUrlMap");

                if (tabUrlMap[breadcrumb.label]){
                    breadcrumb.href = tabUrlMap[breadcrumb.label];
                }

                breadcrumbCollection.push(breadcrumb);  
                cmp.set("v.breadcrumbCollection", breadcrumbCollection);    
            }
            
            // No active menu item found
            if (!activeMenuItem || (activeMenuItem && !activeMenuItem.label)){

                console.log("no active menu item found");

                breadcrumbCollection = cmp.get("v.breadcrumbCollection");

                console.log("first breadcrumb: " + breadcrumbCollection[0].label.toUpperCase());
                utils.logObjectToConsole(menuItems);

                for (var i = 0; i < menuItems.length; i++){
    
                    var menuItem = menuItems[i];
    
                    if (menuItem.label.toUpperCase() == breadcrumbCollection[0].label.toUpperCase()){
                        menuItem.active = true;
                        break;
                    }
                }  
                
                cmp.set("v.menuItems", menuItems); 

                // Check for standard page url's that we have no control over breadcrumbs
                var urlBase = decodeURIComponent(window.location);

                if (urlBase.includes("/settings/")){

                    var breadcrumbCollection = [
                        {"label" : "HOME"},
                        {"label" : "MY SETTINGS", "href" : window.location.pathname}
                    ];

                    cmp.set("v.breadcrumbCollection", breadcrumbCollection);
                }
            }
        }    
        
        cmp.set("v.isLoading", false);
    },
    onBreadCrumbClick : function(cmp, event, helper){
        console.log("onBreadCrumbClick");

        var utils = cmp.find("utils");
        var index = event.target.dataset.breadcrumbIndex;
        var url = event.target.dataset.breadcrumbUrl;
        var breadcrumbCollection = cmp.get("v.breadcrumbCollection");
        
        console.log("index: " + index);
        console.log("url: " + url);
        
        // Remove any breadcrumbs past the selection
        if (index < (breadcrumbCollection.length - 1)){

            console.log("need to remove trailing breadcrumbs (" + breadcrumbCollection.length + ")");
            var i = parseInt(index) + 1;
            console.log("i: " + i);

            for (i; i <= breadcrumbCollection.length; i++){                
                console.log("removing last breadcrumb");
                breadcrumbCollection.pop();
            }

            cmp.set("v.breadcrumbCollection", breadcrumbCollection);
        }

        var urlEvent = $A.get("e.force:navigateToURL");

        urlEvent.setParams({
            "url": url
        });
        
        urlEvent.fire();        
    },
    navigateToHome : function(cmp, event, helper){
        console.log("navigateToHome");

        cmp.set("v.breadcrumbCollection", [{"label" : "HOME"}]);

        var urlEvent = $A.get("e.force:navigateToURL");
        var communitySettings = cmp.get("v.communitySettings");

        urlEvent.setParams({
            "url": communitySettings.Community_Root_URL__c
        });
        
        urlEvent.fire();
    },
    onMenuSelect : function(cmp, event, helper){
        console.log("onMenuSelect");
        
        var utils = cmp.find("utils");
        var id = event.target.dataset.menuItemId;
        var parentId = event.target.parentElement.dataset.menuItemId;

        if (!id){
            id = parentId;
        }
        
        if (id) {      
            
            var menuItems = cmp.get("v.menuItems");
            var breadcrumbCollection = [{"label" : "HOME"}];
            var activeMenuItem = {};            

            // Update breadcrumbs
            for (var i = 0; i < menuItems.length; i++){

                var menuItem = menuItems[i];

                if (menuItem.id == id){
                    activeMenuItem = menuItem;
                    break;
                }
            }

            if (activeMenuItem && activeMenuItem.label){
                
                console.log("activeMenuItem");
                utils.logObjectToConsole(activeMenuItem);

                if (activeMenuItem.label.toUpperCase() == "HOME"){
                    breadcrumbCollection = [];
                }

                // Do not add if they re-clicked the menu item
                if (breadcrumbCollection.length > 0 && breadcrumbCollection[breadcrumbCollection.length - 1].label.toUpperCase() == activeMenuItem.label.toUpperCase()){
                    console.log("tab reselected, not adding duplicate to breadcrumb collection");
                }
                else{
                    
                    var breadcrumb = {"label" : activeMenuItem.label.toUpperCase()};                  
                    var tabUrlMap = cmp.get("v.tabUrlMap");

                    if (tabUrlMap[breadcrumb.label]){
                        breadcrumb.href = tabUrlMap[breadcrumb.label];
                    }                

                    breadcrumbCollection.push(breadcrumb);
                }

                cmp.set("v.breadcrumbCollection", breadcrumbCollection);    
            }     
            
            cmp.getSuper().navigate(id);  
        }
    },
    handleNavigationEvent : function(cmp, event, helper){
        console.log("handleNavigationEvent");

        try{

            var utils = cmp.find("utils");
            var pageLabel = event.getParam("label");
            var pageURL = event.getParam("url");
            var resetBreadcrumbs = event.getParam("resetBreadcrumbs");
            var breadcrumbCollection = cmp.get("v.breadcrumbCollection");

            console.log("current breadcrumbCollection");
            utils.logObjectToConsole(breadcrumbCollection);
            console.log("pageLabel: " + pageLabel);
            console.log("pageURL: " + pageURL);
            console.log("resetBreadcrumbs: " + resetBreadcrumbs);

            if (resetBreadcrumbs){
                cmp.set("v.breadcrumbCollection", [{"label" : "HOME"}]);
            }
            else{

                // Only add it if it doesn't already exist (i.e. they clicked on a breadcrumb)
                var duplicate = false;

                for (var i = 0; i < breadcrumbCollection.length; i++){

                    var breadcrumb = breadcrumbCollection[i];

                    if (breadcrumb.label.toUpperCase() == pageLabel.toUpperCase()){
                        duplicate = true;
                        break;
                    }
                }

                if (!duplicate){
                    breadcrumbCollection.push({"label" : pageLabel.toUpperCase(), "href" : pageURL});              
                    cmp.set("v.breadcrumbCollection", breadcrumbCollection);
                }

                console.log("breadcrumbCollection");            
                utils.logObjectToConsole(breadcrumbCollection);

                // Make sure the selected tab still shows as active after a drill-down
                var menuItems = cmp.get("v.menuItems");

                if (menuItems && menuItems.length > 0){

                    for (var i = 0; i < menuItems.length; i++){

                        var menuItem = menuItems[i];
                        console.log("checking menu item..");
                        utils.logObjectToConsole(menuItem);

                        if (menuItem.label.toUpperCase() == breadcrumbCollection[0].label.toUpperCase()){
                            menuItem.active = true;
                            break;
                        }
                    }            

                    cmp.set("v.menuItems", menuItems);
                }
            }
        }
        catch(e){console.log("exception: " + e)};
    }
});