({
    apex: function(cmp, method, params) {
        helper.apex(cmp, method, params);
    },
    soql: function (cmp, query) {
        helper.soql(cmp, query);
    },
    fields: function (cmp, objectName, fsName) {
    	helper.fields(cmp, objectName, fsName);
	}
})