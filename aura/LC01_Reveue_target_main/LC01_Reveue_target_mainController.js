({

	init : function(component, event, helper) {
		// console.log("in init main ", component.get("v.recordId"));
		component.set("v.isLoading", true);

		var recordId = component.get("v.recordId");   
		var action = component.get("c.retrieveTarget");

		action.setParams({"pt_dsmp_id": recordId});  
		action.setCallback(this, function(response) {

			var state = response.getState();

			if(component.isValid() && state === 'SUCCESS') {

				var result =  response.getReturnValue();
				// console.log('mgr result ', result);

				component.set("v.Labels", result.labels);

				if(result.error){
                    // console.log('mgr 1');

				} else if(!result.error){
                    // console.log('mgr 2--end');
					component.set("v.accountId", result.accountId);
					component.set("v.sprint1", result.sprint1);

					component.set("v.sprint1", result.sprint1);
					component.set("v.sprint1_Target_NPD", result.sprint1_Target_NPD);

					component.set("v.sprint2", result.sprint2);
					component.set("v.sprint3", result.sprint3);

					component.set("v.year", result.year);
					component.set("v.year_1", result.year_1);
					component.set("v.year_2", result.year_2);
					
					component.set("v.dvp_segment", result.dvp_segment);
					component.set("v.min_target", result.min_target);
					component.set("v.final_target", result.final_target);
					component.set("v.total_lastYear", result.total_lastYear);
					component.set("v.total_Current", result.total_Current);
					component.set("v.total_base", result.total_base);

					component.set("v.growth_mintarget", result.growth_mintarget);
					component.set("v.growth_base", result.growth_base);
					component.set("v.strategic_prod", result.strategic_prod);
					component.set("v.incategory", result.incategory);

					component.set("v.readOnly", result.readOnly);

					component.set("v.displayWarning", result.displayWarning);
					/*component.set("v.warning1", result.warning1);
					component.set("v.warning2", result.warning2);
					component.set("v.warning3", result.warning3);
					component.set("v.warning4", result.warning4);*/

					var sprint1 = result.sprint1;
					var sprint2 = result.sprint2;
					var sprint3 = result.sprint3;

					var newresult = [];

					for (var i = 0; i < sprint1.length; i++) {
						newresult.push(sprint1[i]);
					}

					for (var i = 0; i < sprint2.length; i++) {
						newresult.push(sprint2[i]);
					}

					for (var i = 0; i < sprint3.length; i++) {
						newresult.push(sprint3[i]);
					}

					helper.calculateTotals(component, newresult);

                }
                
			}

			component.set("v.isLoading", false);

		});
 
		$A.enqueueAction(action);

	},

	updateTotals : function(component, event, helper){
		// console.log('updateTotals');

		var sprint1 = component.get("v.sprint1");
		var sprint2 = component.get("v.sprint2");
		var sprint3 = component.get("v.sprint3");

		var result = [];

		for (var i = 0; i < sprint1.length; i++) {
			result.push(sprint1[i]);
		}

		for (var i = 0; i < sprint2.length; i++) {
			result.push(sprint2[i]);
		}

		for (var i = 0; i < sprint3.length; i++) {
			result.push(sprint3[i]);
		}

		helper.calculateTotals(component, result);

	},

	save : function(component, event, helper) {
		// console.log("save");
		component.set("v.isLoading", true);

		if(component.get("v.showRevenue")){
			// console.log('showRevenue');

			var sprint1 = component.get("v.sprint1");
			var sprint2 = component.get("v.sprint2");
			var sprint3 = component.get("v.sprint3");

			// console.log('sprint1 ', sprint1);
			// console.log('sprint2 ', sprint2);
			// console.log('sprint3 ', sprint3);

			var result = [];

			for (var i = 0; i < sprint1.length; i++) {
				if(sprint1[i].id != undefined)
				result.push(sprint1[i]);
			}

			for (var i = 0; i < sprint2.length; i++) {
				if(sprint2[i].id != undefined)
					result.push(sprint2[i]);
			}

			for (var i = 0; i < sprint3.length; i++) {
				if(sprint3[i].id != undefined)
					result.push(sprint3[i]);
			}

			// console.log('showRevenue ', result);

			helper.saveTargets(component, result);

		} else if(component.get("v.showProduct")){
			// console.log('showProduct');
			
			var childComponent = component.find("cmpProdTargets");
			// console.log('childComponent', childComponent);
			
        	childComponent.save();

		} else if(component.get("v.showObjective")){
			// console.log('showObjective');

			var childComponent = component.find("cmpObjectives");
			// console.log('childComponent', childComponent);

        	childComponent.save();
		
		}

		component.set("v.isLoading", false);

	},

	moveToRevenue : function(component, event, helper){
		component.set("v.showRevenue",true);
		component.set("v.showProduct",false);
		component.set("v.showObjective",false);
		component.set("v.title","Revenue Target");
		helper.changePathBg(component,true,false,false);

	},

	moveToProduct : function(component, event, helper){
		component.set("v.title","Product Target");
		component.set("v.showRevenue",false);
		component.set("v.showProduct",true);
		component.set("v.showObjective",false);
		helper.changePathBg(component,false,true,false);


	},

	moveToObjective : function(component, event, helper){
		component.set("v.title","Objectives");
		component.set("v.showRevenue",false);
		component.set("v.showProduct",false);
		component.set("v.showObjective",true);
		helper.changePathBg(component,false,false,true);


	},

	
	toggleWarning : function function_name(component, event, helper) {
		$A.util.toggleClass( component.find("warning"), 'hideWarning');
	},


	checkWarning3Products : function function_name(component, event, helper) {
		var count = component.get("v.isChangedProductQuantity");

		// console.log('mgr count ', count);

		if(count == 0)
			component.set("v.strategic_prod", false);
		else 
			component.set("v.strategic_prod", true);

	}


})