<aura:component implements="forceCommunity:availableForAllPageTypes" controller="CTS_IOT_Asset_ListController">

    <!--Attributes-->
    <aura:attribute name="assets" type="List" default="[]"/>
    <aura:attribute name="siteID" type="String"/>
    <aura:attribute name="pageSize" type="Integer" default="20"/>
    <aura:attribute name="offSet" type="Integer" default="0"/>
    <aura:attribute name="totalRecordCount" type="Integer" default="0"/>    
    <aura:attribute name="searchTerm" type="String" default=""/>    
    <aura:attribute name="isLoading" type="Boolean" default="true"/>
    <aura:attribute name="showTitle" type="Boolean" default="true"/>
    <aura:attribute name="showSubTitle" type="Boolean" default="true"/>
    <aura:attribute name="viewMode" type="String" default="small" description="If set to 'large' on the height of the table will increase to take up more space on the page."/>
    <aura:attribute name="communitySettings" type="sObject" default="{}"/>   
    <aura:attribute name="helixIconURLDefault" type="String" default="{!$Resource.CTS_IOT_Community_Resources + '/Icons/PNG/Optimize_Gray.png'}"/>
    <aura:attribute name="helixIconURLHover" type="String" default="{!$Resource.CTS_IOT_Community_Resources + '/Icons/PNG/Optimize_Red&amp;Gray.png'}"/>   
    <aura:attribute name="equipmentTypeFilter" type="String" default=""/>    
    <aura:attribute name="equipmentTypeFilters" type="List" default="[]"/>
    <aura:attribute name="helixStatusFilter" type="String" default=""/>    
    <aura:attribute name="helixStatusFilters" type="List" default="[]"/>    
 
    <!--Event Handlers-->
    <aura:handler name="init" value="{!this}" action="{!c.init}"/>   
    <aura:handler event="c:List_Page_Size_Change" action="{!c.handleGlobalPageSizeChange}"/>
    
    <!--Embedded Components-->
    <c:Utils aura:id="utils"/>
    <lightning:navigation aura:id="navService"/>

    <div>
        <aura:if isTrue="{!v.isLoading}">
            <lightning:spinner alternativeText="Loading"/>
        </aura:if>
    </div>	  
    
    <aura:if isTrue="{!v.showTitle}">      

        <div class="slds-text-heading_medium slds-align_absolute-center slds-p-around_medium">
            <b><aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Title}"/></b>
        </div>

    </aura:if>    

    <aura:if isTrue="{!v.showSubTitle}">

        <div class="slds-text-heading_small slds-align_absolute-center">
            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_SubTitle}"/>
        </div>

    </aura:if>
     
    <lightning:card class="section-container">            

        <lightning:layout horizontalAlign="spread">

            <lightning:layoutItem>

                <!-- <lightning:icon iconName="utility:toggle_panel_left" size="small" variant="error"/>  -->
                <span class="slds-p-right_xx-small" style="vertical-align: middle;">
                    <c:fontAwesomeIcon iconName="Asset_Displayed" iconSizeOverride="2x"/>
                </span>

                <span class="slds-text-heading_medium" style="vertical-align: sub;">
                    <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Title}"/>
                </span>

            </lightning:layoutItem>

            <lightning:layoutItem class="slds-p-bottom_small">

                <lightning:combobox 
                    value="{!v.equipmentTypeFilter}" 
                    dropdownAlignment="auto"
                    options="{!v.equipmentTypeFilters}" 
                    onchange="{!c.equipmentTypeFilterChange}"/>

            </lightning:layoutItem>

            <lightning:layoutItem class="slds-p-bottom_small">

                <lightning:combobox 
                    value="{!v.helixStatusFilter}" 
                    dropdownAlignment="auto"
                    options="{!v.helixStatusFilters}" 
                    onchange="{!c.helixStatusFilterChange}"/>

            </lightning:layoutItem>            

            <lightning:layoutItem>

                <c:searchInput onsearchtermchange="{!c.handleSearchTermChange}" helpIconSizeOverride="2x" helpIconStyleOverride="vertical-align: inherit;"/>

            </lightning:layoutItem>            
                
        </lightning:layout>       

        <aura:if isTrue="{!v.assets.length == 0}">

        <div class="slds-text-title">
            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_No_Assets_Found_Label}"/>
        </div>

        </aura:if>   

        <div class="{!v.assets.length > 0 ? (v.viewMode == 'large' ? 'data-table-large' : 'data-table') : 'slds-hide'}">

            <table class="iot-table slds-table slds-table_cell-buffer slds-table_bordered">
       
                <tbody>
                    
                    <aura:iteration items="{!v.assets}" var="assetDetail">

                    <tr class="slds-hint-parent site-row">
                        
                        <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Serial_Header_Label}">
                            <lightning:formattedUrl value="{!'/asset/' + assetDetail.asset.Id}" tooltip="{!assetDetail.asset.Asset_Name_Displayed__c}" label="{!assetDetail.asset.Asset_Name_Displayed__c}"/>
                        </td>
                        <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Model_Header_Label}">{!assetDetail.asset.Model_Name__c}</td>  
                        <!-- <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Type_Header_Label}">{!assetDetail.assetType}</td>    -->
                        
                        <aura:if isTrue="{!v.viewMode == 'large'}">

                            <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Site_Header_Label}">                             
                                <lightning:formattedUrl value="{!'/account/' + assetDetail.asset.AccountId}" tooltip="{!assetDetail.asset.Account.Account_Name_Displayed__c}" label="{!assetDetail.asset.Account.Account_Name_Displayed__c}"/> 
                            </td> 

                        </aura:if>                        
                        
                        <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Install_Date_Header_Label}">{!assetDetail.asset.Ship_Date__c}</td>  
                        <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Status_Header_Label}">{!assetDetail.asset.Helix_Connectivity_Status__c}</td>    

                        <aura:if isTrue="{!v.communitySettings.Losant_Dashboard_Enabled__c}">
                        
                            <td class="slds-cell-wrap" data-label="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Dashboard_Header_Label}">                             

                                <aura:if isTrue="{!assetDetail.asset.IRIT_RMS_Flag__c}">
                                    
                                    <a data-asset-id="{!assetDetail.asset.Id}" title="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_View_Dashboard_Hover_Help}" onclick="{!c.onViewDashboard}">
                                        <!-- <img class="table-action-icon-medium" onmouseover="{!c.onMouseOverHelixIcon}" onmouseout="{!c.onMouseOutHelixIcon}" src="{!v.helixIconURLDefault}"/> -->
                                        <c:fontAwesomeIcon iconName="Real_Time_Dashboard" iconSizeOverride="2x"/>
                                    </a>  

                                    <aura:set attribute="else">

                                        <aura:if isTrue="{!assetDetail.asset.Controller_Type_RMS_Compatible__c}">

                                            <a data-asset-id="{!assetDetail.asset.Id}" 
                                               title="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Upgrade_Hover_Help}" 
                                               onclick="{!c.onUpgrade}">

                                               <c:fontAwesomeIcon iconName="Helix_Upgradable" iconSizeOverride="2x"/>
                                                
                                                <!-- <lightning:icon iconName="utility:arrowup" 
                                                                size="small" 
                                                                variant="" 
                                                                title="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Upgrade_Hover_Help}" 
                                                                alternativeText="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Upgrade_Hover_Help}"
                                                                onmouseover="{!c.onMouseOverServiceRequestIcon}" 
                                                                onmouseout="{!c.onMouseOutServiceRequestIcon}"/> -->
                                            </a> 

                                        </aura:if>

                                    </aura:set>

                                </aura:if>                                

                            </td>                                                                                                                   

                        </aura:if>
                
                    </tr>

                    </aura:iteration>
                
                </tbody>

                <thead>

                    <tr class="slds-line-height_reset">
                        
                        <th class="slds-cell-wrap" scope="col">
                            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Serial_Header_Label}"/>
                        </th>
                        <th class="slds-cell-wrap" scope="col">
                            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Model_Header_Label}"/>
                        </th>
                        <!-- <th class="slds-cell-wrap" scope="col">
                            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Type_Header_Label}"/>
                        </th> -->

                        <aura:if isTrue="{!v.viewMode == 'large'}">
                            <th class="slds-cell-wrap" scope="col">
                                <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Site_Header_Label}"/>
                            </th>
                        </aura:if>

                        <th class="slds-cell-wrap" scope="col">
                            <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Install_Date_Header_Label}"/>
                        </th>
                        <th class="slds-cell-wrap" scope="col"><aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Status_Header_Label}"/>
                        </th>                           

                        <aura:if isTrue="{!v.communitySettings.Losant_Dashboard_Enabled__c}">
                        
                            <th class="slds-cell-wrap" scope="col">
                                <aura:unescapedHtml value="{!$Label.c.CTS_IOT_My_Equipment_Asset_List_Dashboard_Header_Label}"/>
                            </th>                                                                                

                        </aura:if>

                    </tr>

                </thead>                 
                
            </table>         
            
        </div>

        <aura:if isTrue="{!v.totalRecordCount > 0}">

            <c:paginator 
                pageSize="{!v.pageSize}" 
                totalRecordCount="{!v.totalRecordCount}" 
                showFirstPageButton="true" 
                showLastPageButton="true"
                onpagechange="{!c.handlePageChange}"
                onpagesizechange="{!c.handlePageSizeChange}"/>

        </aura:if>        

    </lightning:card>

</aura:component>