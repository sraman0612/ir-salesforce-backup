({
    initialize : function(cmp, event, helper) {
        
        cmp.set("v.isLoading", true);

		var utils = cmp.find("utils");
        var action = cmp.get("c.getInit");

        action.setParams({
            "siteId" : cmp.get("v.siteID")
        });

        utils.callAction(action, helper.handleInitSuccess, helper.handleActionCallFailure, cmp, helper);	 
    },

    handleInitSuccess: function(cmp, event, helper, response){
    
        var utils = cmp.find("utils");

		try{
                        
			var responseVal = response.getReturnValue();
		
			console.log(responseVal);
				
			if (responseVal.success){		

                cmp.set("v.site", responseVal.site);

                // Update breadcrumbs
                // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
                
                // navEvent.setParams({
                //     "label" : responseVal.site.Name,
                //     "url" : "/site-details?site-id=" + responseVal.site.Id
                // });

                // navEvent.fire();                
			}
			else{
                utils.handleFailure(responseVal.errorMessage);
			}	
        }
        catch(e){
            utils.handleFailure(e.message);           
        }    
        
        cmp.set("v.isLoading", false);
	},

    handleActionCallFailure: function(cmp, event, helper, response){
        var utils = cmp.find("utils");
        utils.handleFailure(response.getError()[0].message); 
        cmp.set("v.isLoading", false);
    }
})