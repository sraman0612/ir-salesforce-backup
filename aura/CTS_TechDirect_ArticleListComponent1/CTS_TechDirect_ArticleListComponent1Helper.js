({
    getLanguage: function(returnList) {
        var communityName = (window.location.pathname).split('/')[1];
        for(var i = 0; i < returnList.length; i++) {    
            // returnList[i].TitleURL = '/one/one.app?#/sObject/'+ returnList[i].Id + '/view';
            returnList[i].TitleURL = '/' + communityName + '/s/article/'+ returnList[i].UrlName;
            if(returnList[i].Language == 'en_US') {                        
                returnList[i].Language = 'English';
            } else if(returnList[i].Language == 'zh_CN') {
                returnList[i].Language = 'Chinese (Simplified)';
            } else if(returnList[i].Language == 'zh_TW') {
                returnList[i].Language = 'Chinese (Traditional)';
            } else if(returnList[i].Language == 'nl_NL') {
                returnList[i].Language = 'Dutch';
            } else if(returnList[i].Language == 'da') {
                returnList[i].Language = 'Danish';
            } else if(returnList[i].Language == 'fi') {
                returnList[i].Language = 'Finnish';
            } else if(returnList[i].Language == 'fr') {
                returnList[i].Language = 'French';
            } else if(returnList[i].Language == 'de') {
                returnList[i].Language = 'German';
            } else if(returnList[i].Language == 'it') {
                returnList[i].Language = 'Italian';
            } else if(returnList[i].Language == 'ja') {
                returnList[i].Language = 'Japanese';
            } else if(returnList[i].Language == 'ko') {
                returnList[i].Language = 'Korean';
            } else if(returnList[i].Language == 'no') {
                returnList[i].Language = 'Norwegian';
            } else if(returnList[i].Language == 'pt_BR') {
                returnList[i].Language = 'Portuguese (Brazil)';
            } else if(returnList[i].Language == 'ru') {
                returnList[i].Language = 'Russian';
            } else if(returnList[i].Language == 'es') {
                returnList[i].Language = 'Spanish';
            } else if(returnList[i].Language == 'es_MX') {
                returnList[i].Language = 'Spanish (Mexico)';
            } else if(returnList[i].Language == 'sv') {
                returnList[i].Language = 'Swedish';
            } else if(returnList[i].Language == 'th') {
                returnList[i].Language = 'Thai';
            } 
        }
    },
    sortData: function (component, fieldName, sortDirection) {
        var data = component.get("v.AllArticles");
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        this.createPagination(component, data);
        // component.set("v.articles", data);
    },
    sortBy: function (field, reverse, primer) {
        var key = primer ?
            function(x) {return primer(x[field])} :
            function(x) {return x[field]};
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    /*
     * Method will be called when use clicks on next button and performs the 
     * calculation to show the next set of records
     */
    next: function(component, event){
        var sObjectList = component.get("v.AllArticles");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var Paginationlist = [];
        var counter = 0;
        for(var i=end+1; i<end+pageSize+1; i++){
            if(sObjectList.length > i){
                Paginationlist.push(sObjectList[i]);
            }
            counter ++ ;
        }
        start = start + counter;
        end = end + counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.articles', Paginationlist);
    },
    /*
     * Method will be called when use clicks on previous button and performs the 
     * calculation to show the previous set of records
     */
    previous: function(component, event){
        var sObjectList = component.get("v.AllArticles");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var Paginationlist = [];
        var counter = 0;
        for(var i= start-pageSize; i < start ; i++){
            if(i > -1){
                Paginationlist.push(sObjectList[i]);
                counter ++;
            } else{
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        component.set("v.startPage",start);
        component.set("v.endPage",end);
        component.set('v.articles', Paginationlist);
    },
    createPagination: function(component, returnList) {        
        this.getLanguage(returnList);
        //for pagination logic
        var pageSize = component.get("v.pageSize");
        // get size of all the records and then hold into an attribute "totalRecords"
        component.set("v.totalRecords", returnList.length);
        // set star as 0
        component.set("v.startPage", 0);
        component.set("v.endPage", pageSize-1);
        var PaginationList = [];
        for(var i=0; i< pageSize; i++){
            if(returnList.length> i){
                // this.getLanguage(returnList[i]);
                PaginationList.push(returnList[i]);
            }
        }                 
        component.set('v.articles', PaginationList);
    }
})