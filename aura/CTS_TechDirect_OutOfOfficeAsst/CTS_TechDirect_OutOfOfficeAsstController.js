({
	initialize : function(cmp, event, helper) {
		
		console.log("CTS_TechDirect_OutOfOfficeAsst.controller.initialize");
		
        var actions = [
            { label: 'Delete', name: 'delete' }
        ];

        // TODO: Update to show record type and backup user/queue names
		var caseRoutingColumns = [
            { label: 'Record Type', fieldName: 'Record_Type_ID__c', type: 'text' },
            { label: 'Backup User/Queue', fieldName: 'Backup_User_Queue_ID__c', type: 'text' },
            { label: 'Actions', type: 'action', typeAttributes: { rowActions: actions } }		     
		];
		
		cmp.set("v.caseRoutingColumns", caseRoutingColumns);
		
		helper.getInit(cmp, event, helper);
	},
	
	onSave : function(cmp, event, helper){
		
		console.log("CTS_TechDirect_OutOfOfficeAsst.controller.onSave");
        console.log("User Id:" + cmp.get("v.backupUserId"));
        console.log("Queue Id:" + cmp.get("v.backupQueueId"));
		
		// Add any validations here
        
        //clear queue/user if user/queue is selected
        if(cmp.get("v.userActive")){
            cmp.set("v.backupQueueId", null);
        }
        else{
            cmp.set("v.backupUserId", null);
        }
        
        //check start/end dates
        if(cmp.get("v.outOfOfficeSettings.Start_Date__c") >= cmp.get("v.outOfOfficeSettings.End_Date__c")){
            let message = "End Date should be AFTER Start Date";
            helper.showErrorToast(cmp, event, helper, message);
        }
        
        //show error indicating that a forwarding queue or user must be selected to save OOO settings
        else if((cmp.get("v.backupUserId") == null || cmp.get("v.backupUserId") == "") && 
           (cmp.get("v.backupQueueId") == null || cmp.get("v.backupQueueId") == "") &&
           cmp.get("v.outOfOfficeSettings.Enabled__c")){
            var message = "Please select a Forwarding Queue or Forwarding User to save your OOO Settings.";
            helper.showErrorToast(cmp, event, helper, message);
        }
        //save settings if validations pass
        else{	
            //update case routings if OOO is enabled
            if(cmp.get("v.outOfOfficeSettings.Enabled__c")){
                helper.createRoutings(cmp, event, helper);
            }
            //save
			helper.save(cmp, event, helper);
        }
	},
    
    //handle switching between user/queue
    toggleUserQueue : function(cmp, event, helper){
        if(cmp.get("v.userActive"))
        	cmp.set("v.userActive", false);
        else
            cmp.set("v.userActive", true);
    }
})