({
     handleRecordChanged: function(component, event, helper) {
  switch(event.getParams().changeType) {
    case "ERROR":
      
      break;
    case "LOADED":
          var OrcNum = component.get("v.opptyRecord")["Ship_To_Customer__c"];
          component.set("v.BillCust", OrcNum);
          console.log("OrcNum : " +OrcNum);  
          
      break;
    case "REMOVED":
      
      break;
    case "CHANGED":
      
      break;
  }
         $A.enqueueAction(component.get('c.fetchAccounts'));
}, 
    
    fetchAccounts : function(component, event, helper) {
			
        var OracleNumber = component.get("v.BillCust"); 
        
        component.set('v.myColumns',[
            {label:'Account Name', fieldName:'Name',type:'text',initialWidth: 130},
            {label: 'Type', fieldName: 'Type', type: 'text',initialWidth: 60},
            {label: 'Oracle Number', fieldName: 'Oracle_Number__c', type: 'text',initialWidth: 120},
            {label: 'Shipping Street', fieldName: 'ShippingStreet', type: 'text',initialWidth: 130},
            {label: 'Shipping City', fieldName: 'ShippingCity', type: 'text',initialWidth: 130},
            {label: 'Shipping State', fieldName: 'ShippingState', type: 'text',initialWidth: 130},            
            {label: 'Postal Code', fieldName: 'ShippingPostalCode', type: 'text',initialWidth: 130}
        ]);
               
        
        var action = component.get("c.fetchAccts");
        
        action.setParams({"Bill_Acc" : OracleNumber  });       
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                	
					component.set("v.accList", response.getReturnValue());
                    
                }
                
            
        });
        $A.enqueueAction(action);
    },
    
   updateSelectedText: function (component, event,helper) {
        var selectedRows = event.getParam('selectedRows');
       component.set('v.selectedRowsCount', selectedRows.length);
       if(selectedRows.length > 1){
           var toastEvent = $A.get("e.force:showToast");
           toastEvent.setParams({
               "title": "Error!",
               "message": "Kindly Select Only One 'Bill To' Account"
		})
          toastEvent.fire(); 
           let button = component.find('disablebuttonid');
           button.set('v.disabled',true);
       }
       else{
           let button = component.find('disablebuttonid');
           button.set('v.disabled',false);
           
       }
       
                                
       //console.log(JSON.parse(JSON.stringify(component.get("v.selectedRows"))));
       let obj =[];
       for(var i=0; i < selectedRows.length; i++){
           obj.push({Id:selectedRows[i].Id});
       }
       //component.set("v.selectedRowsDetails" ,JSON.stringify(obj) );
       
       component.set("v.AccId",JSON.stringify(obj));
       
     },
    handleSelect: function (component, event, helper){
        var opptyId = component.get("v.recordId");
        console.log('Acc ID :'+JSON.stringify(JSON.parse(component.get("v.AccId"))));
        var acId= component.get("v.AccId");
        
		var nos=component.get('v.selectedRowsCount');
        console.log("No. of rows selected :" +nos);
        
        var par=JSON.parse(acId);
        var accc= par[0];
        console.log("accc :"+accc.Id);
        
        
        var updateAction = component.get("c.setBillAccount");
        updateAction.setParams({"OpId" : opptyId , "AccId" : accc.Id});
        
        updateAction.setCallback(this,function(response){
        var state = response.getState();
		console.log("opptyId inside: "+opptyId);            
    if (state === "SUCCESS") 
    {	
        console.log("Account Updated");
    }
            else if (state==="ERROR") 
            {	var errors= response.getError();
             if(errors){
                 if (errors[0] && errors[0].message) {
                     console.log("Error message: " +
						errors[0].message);
                 }
             }else {console.log("Unknown Error");
                   }
                
            }   
            
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(updateAction);


    }
})