({
    toastError: function(message)
    {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "mode": 'sticky',
            "type": 'error',
            "title": 'Error',
            "message": message
        });
        toastEvent.fire();
    },
    /*
    People:{"requiredAttendees":[
        {"name":"benjenkin@hotmail.com","email":"benjenkin@hotmail.com","response":"none"},
        {"name":"Alex Binks","email":"abinks2003@yahoo.co.uk","response":"none"},
        {"name":"Alan Klaus","email":"alan_klaus@Hotmail.com","response":"none"}],
    "optionalAttendees":[]}
    
    Each Attedee can have one of 7 statuses
    001 Unable to query - For example no account
	002 Loading
	003 Not found (Create Contact)			|	Checkbox present, default checked, enabled	|	Create contact
    004 Reparent Contact						|	Checkbox present, default checked, enabled	|	Reparent
    005 Found correct account				|	No checkbox									| 	None
    006 IRCO contact (has email irco.com) 	
    007 This user
    
    					Checked								Not checked
    Reparent			Move contact to {Account Name}		Contact under {Account Name}
    Create (003)   		Create Contact Under {Account Name}		Do not create Contact   
    
    Exists				Contact Under {Account Name}
    
    */
    setStatusLabel:function(cmp, attendee)
    {
        
		var attendeeStatusLabels = {
            '001': 'Please select account',
            '002': 'Loading...',
            '003': '003 - Error',
            '004': '004 - Error',
            '005': '005 - Error',
            '006': '006',
            '007': '007'
        };
        //
        //account.title
        let result ='Label';
        if(attendee.statusCode === '003' || attendee.statusCode === '004' || attendee.statusCode === '005')
        {
            
            var accounts = cmp.get('v.selectedAccount');
            var account = accounts[0];
            var accountName = account.title;

            if(attendee.statusCode === '003')
            {
                if(attendee.checked)
                {
                    result = 'Create contact under '+accountName;
                }
                else
                {
                    result = 'Do not create contact';
                }
                
            }
            else if(attendee.statusCode === '004')
            {
				if(attendee.checked)
                {
                    result = 'Move Contact under '+accountName;
                }
                else
                {
                    //We need to choose the account it is under here. So we need this account!
                    result = 'Contact under '+attendee.accountName;
                }               
            }
            else if(attendee.statusCode === '005')
            {
                result = 'Contact under '+accountName;
            }
        }
        else 
        {
            result = attendeeStatusLabels[attendee.statusCode]
        }
        //cmp.set('v.log','setStatusLabel:' + result + ' - Attendee:' +  JSON.stringify(attendee));
        return result;
    },
    pollPeople: function(cmp,evt,hlp)
    {
        console.log('[OutlookEventPanel].pollPeople');
        window.setInterval($A.getCallback(function() {
            console.log('[OutlookEventPanel].pollPeople polling');
            //cmp.set('v.log','Polled panel. Subject:'+cmp.get('v.subject'));
            
            var account = cmp.get('v.selectedAccount');
        	var isAccountSelected = account.length === 0 ? false : true;
            //cmp.set('v.log',JSON.stringify(account));
            var attendees = cmp.get('v.attendees');
            
            var people = cmp.get('v.people');
            var requiredAttendees = people.requiredAttendees;

            //Add any new attendees and then query salesforce to see if they exist
            var contactEmailAddresses = [];
            requiredAttendees.forEach(function(requiredAttendee){
                //cmp.set('v.log',requiredAttendee.email);
                if(requiredAttendee.email.indexOf('irco.com') === -1 && requiredAttendee.email.indexOf('irco365.com') && requiredAttendee.email.indexOf('benjenkin@hotmail.com')) {
                    var isExistingRequiredAttendee = false;
                    
                    //Find if the attendee already exists in the list
                    attendees.forEach(function(attendee){
                        if(attendee.email === requiredAttendee.email)
                        {
                            //The attendee already exists. Flag this and, if the account has changed or been selected,
                            //set the status and add the email address to the list to query the database.
                            isExistingRequiredAttendee = true;
                            //'001' = 'Select an Account'
                            if(attendee['statusCode'] === '001' && isAccountSelected)
                            {
                                attendee.statusCode = '002';
                                //attendee.statusLabel = hlp.getStatusLabel('002');
                                attendee.statusLabel = hlp.setStatusLabel(cmp, attendee);
                                contactEmailAddresses.push(requiredAttendee.email);
                            }
                            else if(!isAccountSelected)
                            {
                                attendee.statusCode = '001';
                                //attendee.statusLabel = hlp.getStatusLabel('001');
                                attendee.statusLabel = hlp.setStatusLabel(cmp, attendee);
                                
                            }
                        }
                    });  
                    
                    
                    
                    //If the attendee doesn't exist then add them to the list
                    if(!isExistingRequiredAttendee)
                    {
                        var newAttendee = {};
                        newAttendee.name = requiredAttendee.name;
                        newAttendee.email = requiredAttendee.email;
                        newAttendee.checked = false;
                        newAttendee.disabled = true;
                        newAttendee.hasCheckbox = false;
                        
                        if(isAccountSelected)
                        {
                            newAttendee.statusCode = '002';
                            //newAttendee.statusLabel = hlp.getStatusLabel('002');
                            newAttendee.statusLabel = hlp.setStatusLabel(cmp, newAttendee);
                            contactEmailAddresses.push(requiredAttendee.email);   
                        }
                        else
                        {   
                            newAttendee.statusCode = '001';
                            //newAttendee.statusLabel = hlp.getStatusLabel('001');
                            newAttendee.statusLabel = hlp.setStatusLabel(cmp, newAttendee);
                        }
                        attendees.push(newAttendee);
                        
                        //cmp.set('v.log','Adding attendee:'+requiredAttendee.name);
                    }
    			}	
                
            }); 
                
            //Remove any deleted attendees
            for (let i = attendees.length - 1; i >= 0; i--) {
                var attendee = attendees[i];
                var attendeeInRequiredAttendees = false;
                requiredAttendees.forEach(function(requiredAttendee){
                    if(attendee.email === requiredAttendee.email)
                    {
                        attendeeInRequiredAttendees = true;
            			
                    }
                });
                if(!attendeeInRequiredAttendees)
                {
    				//cmp.set('v.log','Deleting attendee:'+attendee.name);
                    attendees.splice(i, 1);
                }
            }
            cmp.set('v.attendees', attendees);
			
			if(contactEmailAddresses.length >0)
            {
                var accounts = cmp.get('v.selectedAccount');
                if(accounts.length === 0) return null;

                var getContactStatusesAction = cmp.get('c.getContactStatuses');

                getContactStatusesAction.setParams({
                    emailAddresses: contactEmailAddresses,
                    accountId: accounts[0].id
                });
                getContactStatusesAction.setCallback(this, function(response){
                    console.log('[OutlookEventPanel].pollPeople getContactStatusesAction callback');
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var result = response.getReturnValue();
                        //cmp.set('v.log',JSON.stringify(result));
                        if(result.isSuccess)
                        {
                            contactEmailAddresses.forEach(function(emailAddress){
                                attendees.forEach(function(attendee){
                                	if(attendee.email === emailAddress)
                                	{   
                                		var contact = result.attendees[emailAddress.toLowerCase()];
                                		//attendee.statusLabel = hlp.getStatusLabel(contact.statusCode);;
                                		attendee.statusCode = contact.statusCode;
                                        
                                        //003=Not found, 004 = Reparent
                                		if(contact.statusCode === '003' || contact.statusCode === '004')
                                		{
                                			attendee.checked = true;
                                        	attendee.disabled = false;
                                        	attendee.hasCheckbox = true;
                            			}
                                
                                		if(contact.statusCode = '004')
                                        {
                                        	//Reparent contact so has Id
                                        	attendee.Id = contact.Id;
                                            attendee.accountName = contact.accountName;
                                    	}
                                        attendee.statusLabel = hlp.setStatusLabel(cmp, attendee);
                            		}
                            	});
                            });
                            cmp.set('v.attendees', attendees);
                        }
                        else
                        {
                            hlp.toastError(result.errorMessage);
                        }
                        
                    }
                    else if (state === "ERROR") {
                        var errors = response.getError();
                        if (errors) {
                            if (errors[0] && errors[0].message) {
                                hlp.toastError(errors[0].message);
                            }
                        } else {
                            hlp.toastError('Error in component helper poll people callback');
                        }
                    }
                });
                $A.enqueueAction(getContactStatusesAction);
            }
            
        }), 200);    
    } 
})