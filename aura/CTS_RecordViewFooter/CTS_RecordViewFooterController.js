({
	doInit : function(component, event, helper) {
		var action = component.get("c.getInit");
        var artId = component.get("v.recordId");
        //LS 6/6/19 - to handle id access on custom article detail page
        //Changes done for case no:-00811492
        if(artId == null || artId == '' ){
            var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            console.log('sPageURL', sPageURL);
            console.log('sURLVariables', sURLVariables);
            
            var sParameterName;
            var i;
            
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
             //Changes done for case no:-00811492   
                if (sParameterName[0] === 'aid') { //find kaid
                    artId = sParameterName[1];
                    component.set('v.recordId', artId);
                    console.log('artId------>'+artId);
                }
            }
        }
        
        action.setParams({recordId: artId});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = JSON.parse(response.getReturnValue());
                console.log('results init**', results);
                if(results != null) {
                    component.set("v.hasVotedWith", results[0]);
                    component.set("v.following", results[1]);
                }
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    upVote : function(component, event, helper) {
        var elements = document.getElementById("feedback");
        elements.style.display = '';
        var action = component.get("c.vote");
        var artId = component.get("v.recordId");
        action.setParams({aId: artId, type: 'Up'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.hasVotedWith", 'Up');
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    downVote : function(component, event, helper) {
        var elements = document.getElementById("feedback");
        elements.style.display = '';
        var action = component.get("c.vote");
        var artId = component.get("v.recordId");
        action.setParams({aId: artId, type: 'Down'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.hasVotedWith", 'Down');
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    follow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        console.log('recId**', recId);
        action.setParams({recId: recId, action: 'follow'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", true);
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    unfollow : function(component, event, helper) {
        var action = component.get("c.followUnfollow");
        var recId = component.get("v.recordId");
        action.setParams({recId: recId, action: 'unfollow'});
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.following", false);
                console.log('results**', results);
            } else {
            	console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
	},
    submitFeedback : function(component, event,helper){
        var action = component.get("c.submitArticleFeedback");
        var feedbackDesc = component.get("v.feedbackDescripiton");
        var recId = component.get("v.recordId");
        action.setParams({message: feedbackDesc,articleId: recId });
        action.setCallback(this, function(response) {
        var state = response.getState();
        	if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                console.log('Success**' + results);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Success!",
                    message: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Received"),
                    type: "success"
                });
                toastEvent.fire();
                var elements = document.getElementById("feedback");
        		elements.style.display = 'none';
                
            } else {
            	console.log('error' + state);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: "Failure",
                    message: $A.get("$Label.c.CTS_TechDirect_ArticleFeedback_Error"),
                    type: "failure"
                });
                toastEvent.fire();
                var elements = document.getElementById("feedback");
        		elements.style.display = 'none';
            }
        });
        $A.enqueueAction(action);
	},
    CancelFeedback : function(component,event,helper){
        var elements = document.getElementById("feedback");
        elements.style.display = 'none';
    },
    openInNewTab: function(component,event,helper){
        var networkURL = window.location.pathname.split('/')[1];
        var kavURL = component.get('v.kavURL');
        var lang = component.get('v.lang');
        //var url = '/' + networkURL + '/s/article/'+ lang + '/' + kavURL;
        //LS 6/6/19 - to handle id access on custom article detail page
        var url = '/' + networkURL + '/s/cts-article-detail?aid=' + component.get('v.kavId') + '&kaid=' + component.get('v.recordId');
        console.log('test URL', url);
        window.open(url, '_blank');
    }
    
})