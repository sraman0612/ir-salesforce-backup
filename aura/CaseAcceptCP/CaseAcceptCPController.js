({
    acceptCaseCP : function(component, event, helper) {
        var caseId = component.get("v.recordId");
        var url = '/apex/C_CustomEmail_ManagedCare?CaseId='+caseId+'&action=Accept';
        console.log(caseId);
        console.log(url);
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.openSubtab({
                parentTabId: focusedTabId,
                url:  url,
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    },
    cancelCaseCP: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})