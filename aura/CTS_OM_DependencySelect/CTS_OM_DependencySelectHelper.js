({
    nextLevel: function(component, event) {
        let currentPickVals = component.get('v.currentPickVals');
        let currentLevel = component.get('v.currentLevel');
        let currentClickedValAuto = component.get('v.currentClickedValAuto');
        let currentClickedVal = event.currentTarget ? event.currentTarget.id.split('==')[0] : currentClickedValAuto;
        component.set('v.currentClickedVal', currentClickedVal);
        
        console.log('currentClickedVal**', currentClickedVal);

        let allPicks = JSON.parse(JSON.stringify(component.get('v.allPickVals')));
        let keys = JSON.parse(JSON.stringify(component.get('v.allKeys')));
        if (keys.length > 0) {
            let nextVals = allPicks[keys[currentLevel]][currentClickedVal];
            let nextValsObj = [];

            if (nextVals) {
                nextVals.forEach(val => {
                    let nextValObj = {};
                    nextValObj['field'] = val;
                    nextValObj['hasNext'] = allPicks[keys[currentLevel + 1]] !== undefined ? (allPicks[keys[currentLevel + 1]][val]).length > 0 : false;
                    nextValsObj.push(nextValObj);
                });
            }

            //LS 4/2
            let selectedMap = component.get('v.selectedVals');
            selectedMap[currentLevel] = currentClickedVal;
            component.set('v.selectedVals', selectedMap);
			
        	console.log('selectedMap**', selectedMap);
        
            component.set('v.currentPickVals', nextValsObj);
            component.set('v.currentLevel', currentLevel + 1);
        }
    },
    setInitVals: function(cmp, event) {
        let recordId = cmp.get("v.recordId");
        let objectName = cmp.get("v.objectName");
        let fields = cmp.get("v.fields");
        let qString = 'SELECT ' + fields.join(', ') + ' FROM ' + objectName + ' WHERE Id = \'' + recordId + '\'';
        this.soql(cmp, qString)
            .then(result => {
                let tempVals = result[0];
                delete tempVals.Id;
                cmp.set("v.fieldsWithVals", tempVals);
                let pFields = Object.keys(tempVals);
                let selLvl = pFields.length;
                let lastSelVal = tempVals[pFields[selLvl - 1]];
                delete tempVals[pFields[selLvl - 1]];

                if (lastSelVal) this.selectPick(cmp, event, lastSelVal, 'false');
            })
            .catch(error => {
                console.log('q error**', error);
            });
    },
    selectPick: function(component, event, selectedVal, hasNext) {
        console.log('selectedVal**', selectedVal);
        selectedVal = selectedVal != null ? selectedVal : event.currentTarget.id.split('==')[0];
        hasNext = hasNext != null ? hasNext : event.currentTarget.id.split('==')[1];

        if (hasNext === 'true') {
            this.nextLevel(component, event);
        } else {
            component.find('select-input').set('v.value', selectedVal);
            component.set('v.selectVal', selectedVal);
            $A.util.removeClass(component.find('opener'), 'slds-is-open');

            //If last node was click, fire the select event to be handled by the cmp/app including this cmp, with the selected values.
            let currentLevel = component.get('v.currentLevel');
            let fields = component.get('v.fields');
            let selectedVals = component.get('v.selectedVals');
            var selectEvent = $A.get("e.c:CTS_TD_DependencySelectEvent");
            let selectedValsMap = {};
            for (let i = 0; i <= currentLevel; i++) {
                if (i < currentLevel) {
                    selectedValsMap[fields[i]] = selectedVals[i];
                } else if (i == currentLevel) {
                    selectedValsMap[fields[i]] = selectedVal;
                }
            }
            if (selectEvent) {
                selectEvent.setParams({
                    "selectVals": JSON.stringify(selectedValsMap)
                });
                selectEvent.fire();
            }
        }
    },
    initHelper: function(component, event, result) {
        let currentLevel = 0;
        let allPicks = result;

        component.set('v.allPickVals', allPicks);
        console.log('allPickVals**', allPicks);
        let keys = Object.keys(allPicks);
        keys.sort();
        component.set('v.allKeys', keys);
        if (keys.length > 0) {
            let nextVals = Object.keys(allPicks[keys[currentLevel]]);
            let nextValsObj = [];

            component.set('v.currentLevel', parseInt(keys[currentLevel].split('==')[0]));

            nextVals.forEach(val => {
                let nextValObj = {};
                nextValObj['field'] = val;
                nextValObj['hasNext'] = (allPicks[keys[currentLevel]][val]).length > 0;
                nextValsObj.push(nextValObj);
            });
            component.set('v.currentPickVals', nextValsObj);
        }
    },
    setLevel2Select: function(component, event, allPickVals) {
    	//if this is for the second set of fields
        let firstSetCtrlVal = component.get("v.firstSetCtrlVal");
        
        if(firstSetCtrlVal && firstSetCtrlVal != null) {
        this.apex(component, 'populateFirstLvlPicklists', {fields: ['CTS_OM_Category__c', 'CTS_OM_Disposition_Category__c'], objectName: 'Case'})
        	.then( resp => {
                if(allPickVals.hasOwnProperty("0==CTS_OM_Disposition_Category__c==CTS_OM_Disposition_Category2__c")) {
                	
                	component.set("v.selectVal", "");
                	let temp = allPickVals["0==CTS_OM_Disposition_Category__c==CTS_OM_Disposition_Category2__c"];
                	
                	let l2Vals = resp[firstSetCtrlVal];
                	let temp2Obj = {};
                	l2Vals.forEach(l2Val => {                		
                    	if(temp.hasOwnProperty(l2Val)) {
                			temp2Obj[l2Val] = temp[l2Val];
                        }
            		});
                
                	allPickVals["0==CTS_OM_Disposition_Category__c==CTS_OM_Disposition_Category2__c"] = temp2Obj;
                	component.set("v.allPickVals", allPickVals);
                
                	this.initHelper(component, event, allPickVals);//More intialization logic
					if(event.getParam('selectedCat') == null || event.getParam('selectedCat') == undefined) this.setInitVals(component, event); //Populate the value from record
            	} 
                
             })
             .catch(error => {
             	console.log('error**',error);
            });
        }        
    }
})