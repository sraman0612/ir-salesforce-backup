({
    //method called on init
    getSearchConfig: function(component, event, helper) {
        //Populate Data Category picklists
        var action = component.get("c.getFilters");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();                
                //set the data category picklists
                helper.setDataCatPicks(JSON.parse(results[0]), component);                
                component.set("v.langList", JSON.parse(results[1]));
                component.set("v.userInfo", JSON.parse(results[2]));
                component.set("v.selectedLang", component.get("v.userInfo").LanguageLocaleKey);
            } else {
                console.log('error' + state);
            }
        });
        $A.enqueueAction(action);
        
        //Read keyword from param if keyword is null
        var searchText = component.get("v.searchText");
        if (searchText == null || searchText == '') {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName, i;
                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');
                    if (sParameterName[0] === sParam) {
                        var searchStr = sParameterName[1].replace(/\+/g, " ");
                        return searchStr === undefined ? true : searchStr;
                    }
                }
            };
            var initVal = getUrlParameter('keyword');
            component.set("v.searchText", initVal);
        }
        if (component.get("v.searchConfig") == null) {
            var action = component.get("c.getSearchConfigDetails");
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === 'SUCCESS') {
                    var searchConfig = response.getReturnValue();
                    component.set("v.searchConfig", searchConfig);
                    
                    var pageSizeSelectJson = [];
                    var pageSizeSelectVals = searchConfig.pageSizeOptions.split(',');
                    for (var i = 0; i < pageSizeSelectVals.length; i++) {
                        var temp = {};
                        temp.label = pageSizeSelectVals[i];
                        temp.value = pageSizeSelectVals[i];
                        pageSizeSelectJson.push(temp);
                    }
                    component.set("v.pageSizeSelect", pageSizeSelectJson);
                    //call search if a param was passed from the URL
                    var searchText = component.get("v.searchText");
                    if (searchText != null && searchText != '') {
                        component.set("v.searchOrTypeahead", "search");
                        helper.searchKBHelper(component, event, helper);
                    }
                } else {
                    console.log('error' + state);
                }
            });
            $A.enqueueAction(action);
        }
    },
    //main search method
    searchKBHelper: function(component, event, helper) {
        component.set('v.paginationList', null);
        
        var whofired = event.target.id != 'enter-search-div' ? event.getSource().get("v.name") : '';
        console.log('whofired**', whofired, event.target.id);
        if(whofired == 'productSelect') {
            component.set("v.selectedSubProd", "");
            var selectedProduct = component.get('v.selectedProduct');                        
            if(selectedProduct != '') {
                var subProdList = JSON.parse(component.get('v.subProdMap'))[selectedProduct];
                subProdList.sort(function(a, b) { //Sort by label, alphabetically ASC
                    return a.label == b.label ? 0 : +(a.label > b.label) || -1;
                });
                component.set('v.subProdList', subProdList);
                component.set('v.disabledSubProd', false);
            }
            else component.set('v.disabledSubProd', true);
        }    
        
        var searchText = component.get("v.searchText");
        if (whofired == 'enter-search-div' && searchText.length < 2) {
            component.set("v.showSpinner", false);
            component.set("v.searchResults", null);
            return;
        }                
        
        //Data categories, language, type
        var selectedProduct = component.get("v.selectedProduct");
        var selectedSubProduct = component.get("v.selectedSubProd");
        var selectedCategory = component.get("v.selectedCategory");
        var lang = component.get("v.selectedLang");
        var type = component.find("selectedArtType").get("v.value");
        
        if(selectedSubProduct) selectedProduct = selectedSubProduct;
        
        //capture the searched keywrods
        var allSearchTexts;
        if (whofired == 'enter-search-div' && (component.get("v.searchOrTypeahead") == 'search' || searchText.length > 2)) {
            var allSearchTexts = component.get("v.allSearchTexts");
            allSearchTexts += searchText + ';';
            component.set("v.allSearchTexts", allSearchTexts);
        }
        
        component.set("v.showSpinner", true);
        var action = component.get("c.search");
        action.setParams({
            keyword: searchText,
            selectedProduct: selectedProduct,
            selectedCategory: selectedCategory,
            lang: lang,
            type: type
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            component.set("v.showSpinner", false);
            if (state === 'SUCCESS') {
                var results = response.getReturnValue();
                component.set("v.searchResults", results);                                
                
                if (results == null) return;                
                
                //For paging
                var pageSize = component.get("v.pageSize");
                component.set("v.totalRecords", component.get("v.searchResults").length);
                component.set("v.startPage", 0);
                component.set("v.endPage", pageSize - 1);
                var pgList = [];
                for (var i = 0; i < pageSize; i++) {
                    if (results.length > i) pgList.push(results[i]);
                }
                //Calculate total pages
                var totalPage = component.get("v.searchResults").length % pageSize > 0 ? parseInt(component.get("v.searchResults").length / pageSize) + 1 : component.get("v.searchResults").length / pageSize;
                component.set('v.paginationList', pgList);
                component.set('v.totalPage', parseInt(component.get("v.searchResults").length / pageSize) + 1);
            }
        });
        $A.enqueueAction(action);
    },
    //on click of next button for pagination
    onNext: function(component, event) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = end + 1; i < end + pageSize + 1; i++) {
            if (sObjectList.length > i) {
                pagList.push(sObjectList[i]);
            }
            counter++;
        }
        start = start + counter;
        end = end + counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        component.set('v.currentPage', currPage + 1);
    },
    //on click of previous button for pagination
    onPrevious: function(component, event, helper) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var end = component.get("v.endPage");
        var start = component.get("v.startPage");
        var pageSize = component.get("v.pageSize");
        var currPage = component.get("v.currentPage");
        var pagList = [];
        var counter = 0;
        for (var i = start - pageSize; i < start; i++) {
            if (i > -1) {
                pagList.push(sObjectList[i]);
                counter++;
            } else {
                start++;
            }
        }
        start = start - counter;
        end = end - counter;
        //setting pagination variables
        component.set("v.startPage", start);
        component.set("v.endPage", end);
        component.set('v.paginationList', pagList);
        if (currPage > 1) {
            component.set('v.currentPage', currPage - 1);
        }
    },
    //on change of page size for pagination
    pageSizeChanged: function(component, event, helper) {
        component.set('v.paginationList', null); //set results to blank
        var sObjectList = component.get("v.searchResults");
        var pageSize = parseInt(component.find("pageSize").getElement().value);
        component.set("v.pageSize", pageSize);
        component.set("v.totalRecords", component.get("v.searchResults").length);
        component.set("v.startPage", 0);
        component.set("v.endPage", pageSize - 1);
        var pgList = [];
        for (var i = 0; i < pageSize; i++) {
            if (sObjectList.length > i) {
                pgList.push(sObjectList[i]);
            }
        }
        component.set('v.paginationList', pgList);
        component.set('v.currentPage', 1);
        var totalPage = component.get("v.searchResults").length % pageSize > 0 ? parseInt(component.get("v.searchResults").length / pageSize) + 1 : component.get("v.searchResults").length / pageSize;
        component.set('v.totalPage', parseInt(component.get("v.searchResults").length / pageSize) + 1);
    },    
    callServer: function(method, parameter, component) {
        component.set("v.paginationList", null);
        component.set("v.showSpinner", true);
        var action = component.get(method);
        action.setParams({
            parameter: parameter
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.paginationList", response.getReturnValue());
                component.set("v.showSpinner", false);
            } else if (state === "INCOMPLETE") {} else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    setDataCatPicks: function(allcats, component) {
        var catlist = [];
        var prodlist = [];
        var subprodmap = {};
        
        for(var i=0;i<allcats.length;i++) {
            var grp = allcats[i];
            if(grp.name == 'IR_Global_Region_Group') continue;
            for(var j=0;j<grp.topCategories.length;j++) {
                var tcat = grp.topCategories[j];
                for(var k=0;k<tcat.childCategories.length;k++) {
                    var ccat = tcat.childCategories[k];                    
                    for(var l=0;l<ccat.childCategories.length;l++) {
                        var l2cat = ccat.childCategories[l];                        
                        if(grp.name == 'IR_Global_Category_Group' && tcat.name == 'IR_Global_Category' && ccat.name == 'CTS_TechDirect_Category') {
                            var tempCat = {};
                            tempCat.label = l2cat.label;
                            tempCat.value = l2cat.name;
                            catlist.push(tempCat);
                        } else if(grp.name == 'IR_Global_Products_Group' && tcat.name == 'IR_Global_Products' && ccat.name == 'CTS_TechDirect_Products') {
                            var tempProd = {};
                            tempProd.label = l2cat.label;
                            tempProd.value = l2cat.name;
                            prodlist.push(tempProd);
                        }
                        var tempSubProd = [];
                        for(var m=0;m<l2cat.childCategories.length;m++) {
                            var l3cat = l2cat.childCategories[m];
                            
                            if(grp.name == 'IR_Global_Category_Group' && tcat.name == 'IR_Global_Category' && ccat.name == 'CTS_TechDirect_Category') {
                                var tempCatL3 = {};
                                tempCatL3.label = ' > ' + l3cat.label;
                                tempCatL3.value = l3cat.name;
                                catlist.push(tempCatL3);
                            } else if(grp.name == 'IR_Global_Products_Group' && tcat.name == 'IR_Global_Products' && ccat.name == 'CTS_TechDirect_Products') {
                                var tempProdL3 = {};
                                tempProdL3.label = l3cat.label;
                                tempProdL3.value = l3cat.name;
                                tempSubProd.push(tempProdL3);
                            }                            
                        }
                        if(grp.name == 'IR_Global_Products_Group' && tcat.name == 'IR_Global_Products' && ccat.name == 'CTS_TechDirect_Products') {
                            subprodmap[l2cat.name] = tempSubProd;   
                        }                        
                    }
                }      
            }
        }
        component.set("v.prodList", prodlist);
        component.set("v.categList", catlist);
        component.set("v.subProdMap", JSON.stringify(subprodmap));
    },
    resetSearch: function(component) {        
        component.set("v.searchText", "");
        component.set("v.searchResults", null);
        component.set("v.selectedLang", component.get("v.userInfo").LanguageLocaleKey);
        component.set("v.selectedProduct", "");
        component.set("v.selectedCategory", "");
        component.set("v.selectedSubProd", "");
        component.set("v.selectedArtType", component.find("selectedArtType").set("v.value", ""));
    }
})