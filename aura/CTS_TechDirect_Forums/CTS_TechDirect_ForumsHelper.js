({
    getInit : function(cmp, event, helper) {
        
        console.log("CTS_TechDirect_Forums.helper.getInit");
        var action = cmp.get("c.getInit");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                console.log("raw response: " + response.getReturnValue());
                var initResponse = JSON.parse(response.getReturnValue());
                
                cmp.set("v.splashHeading", initResponse.splashHeading);
                cmp.set("v.splashTerms", initResponse.splashTerms);
                cmp.set("v.splashInstructions", initResponse.splashInstructions);
                cmp.set("v.splashAcceptButtonLabel", initResponse.splashAcceptButtonLabel);  
                
                var showTerms = false;
                
                if (initResponse.termsForumSettings.Active__c && (!initResponse.userRecord.CTS_TechDirect_T_C_Accepted__c || initResponse.userRecord.CTS_TechDirect_T_C_Accepted_Version__c != initResponse.termsForumSettings.Current_Version__c)){
                	showTerms = true;
                }
                
                cmp.set("v.showTerms", showTerms);
                
                if (!showTerms){
                	helper.goToChatterTab(cmp, event, helper);
                }
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors) {
                    
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown error");
                }
            }
        });     
        
        $A.enqueueAction(action);   	
    },
    
    acceptTerms : function(cmp, event, helper){
    
    	console.log("CTS_TechDirect_Forums.helper.acceptTerms");
    
        var action = cmp.get("c.acceptTerms");
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (state === "SUCCESS") {
                
                console.log("raw response: " + response.getReturnValue());
                var responseObj = JSON.parse(response.getReturnValue());
                
                if (responseObj.success == true){
                
                	cmp.set("v.showTerms", false);
                	helper.goToChatterTab(cmp, event, helper);
                }
                else{
                	
                	helper.showErrorToast(cmp, event, helper, responseObj.errorMessage);
                }
            }
            else if (state === "ERROR") {
                
                var errors = response.getError();
                
                if (errors) {
                    
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                        helper.showErrorToast(cmp, event, helper, errors[0].message);
                    }
                } 
                else {
                    console.log("Unknown error");
                    helper.showErrorToast(cmp, event, helper, "Unknown error");
                }
            }
        });     
        
        $A.enqueueAction(action);     	
    },
    
    goToChatterTab : function(cmp, event, helper){

    	console.log("CS_TechDirect_Forums.helper.goToChatterTab");
    	
	    var goToChatterTab = $A.get("e.force:navigateToObjectHome");
	    
	    goToChatterTab.setParams({
	        "scope": "CollaborationGroup"
	    });
	    
	    goToChatterTab.fire();        
    },
    
    showErrorToast : function(cmp, event, helper, message){
    
        var errorEvent = $A.get("e.force:showToast");
        
        errorEvent.setParams({
            "title": "Error",
            "message": "An error has occured. " + message +
            " Please contact your System Administrator for assistance.",
            "type": "error"
        });
        
        errorEvent.fire();
    }    
})