({
    init: function (cmp, event, helper) {

    },
    navigateToHome : function(cmp, event, helper){
        
        var urlEvent = $A.get("e.force:navigateToURL");

        urlEvent.setParams({
            "url": "/CTSCustomerPortal/s/"
        });
        
        urlEvent.fire();
    },
    navigateTo: function (cmp, event, helper) {

        var name = event.getSource().get("v.name");
    },
    handleNavigationEvent : function(emp, event, helper){
        console.log("handleNavigationEvent");

        var pageName = event.getParam("pageName");
        var pageLabel = event.getParam("pageLabel");
        var pageURL = event.getParam("pageURL");

        var breadcrumbCollection = cmp.get("v.breadcrumbCollection");

        breadcrumbCollection.push({'label' : pageLabel, 'name' : pageName, 'href' : pageURL});  
        
        cmp.set("v.breadcrumbCollection", breadcrumbCollection);
    }
});