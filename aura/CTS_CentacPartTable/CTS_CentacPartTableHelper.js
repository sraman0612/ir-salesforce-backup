({
    /*Method to get centac parts request list from apex controller*/
    setupTable : function(component) {
        /*fetch data and columns of data table from apex controller init method*/
        var action = component.get("c.init");
        /*To get centac quote id when component added to record page */
        var recordId = component.get("v.recordId");
        if(recordId!= null){
        	component.set("v.centacQuoteId",recordId); 
        }
        var centacQuoteId = component.get("v.centacQuoteId");
        var isUpdateTab = component.get("v.isUpdateTab");
        action.setParams({
            'centacQuoteId':centacQuoteId,
            'isUpdateTab' :isUpdateTab,
            'isInit':true /*set to true if method is called on page load*/
        });
        /*callback action on */
        action.setCallback(this,function(response){
            if(response.getState() === "SUCCESS"){
                var data = response.getReturnValue().dataList;
                console.log('data---->',data);
                this.setColumns(component,response,isUpdateTab);
                this.setPartList(component,data);
                component.set("v.isLoading", false);
                
            }else{
                var errors = response.getError();
                var message = "Error: Unknown error";
                if(errors && Array.isArray(errors) && errors.length > 0)
                    message = "Error: "+errors[0].message;
                component.set("v.error", message);
                console.log("Error: "+message);
            }
        });
        $A.enqueueAction(action);
    },
    /*method to set columns of data table
     * Column format should be as follows:
     * [{label: "field label", fieldName: "field API Name", type:"link", sortable: true, resizable:true, 
     * attributes:{label:{fieldName:"Name"}, title:"Click to View(New Window)", target:"_blank"}},
     * {label: "field label", fieldName: "field API Name", type:"date", editable: true},
     * {label: "field label", fieldName: "field API Name", editable: true, type:"picklist", selectOptions:[{label:'optionLabel1',value:'optionValue1'},{label:'optionLabel2',value:'optionValue2'},]}
     */
    setColumns:function(component,response,isUpdateTab){
        
        var cols = response.getReturnValue().labelList;
        component.set("v.columns", cols); 
    },
    /*method to set data of data table
     * Data format should be 
     * [{field Api Name:field data},{field Api Name:field data}]
     */
    setPartList:function(component,data){
        var centacPartsList = [];
        if(data == null || data == "" || data == "[]" || data == "{}"){
            console.log('---In Null Data---');
            //centacPartsList = component.get("v.centacPartsList");
            
        }else{
            centacPartsList = data;
            if(component.get("v.isUpdateTab") && data.length>0){
            	component.set("v.listUpdateComplete",data[0].Centac_Parts_Quote__r.listUpdateComplete__c)
            }
        } 
        /*If called from add parts tab add default 6 number of empty rows */
        if(!component.get("v.isUpdateTab")){
            var partListLength = 6-centacPartsList.length;
            for(var i=0;i<partListLength;i++){
                //Add New CentacPart Record
                centacPartsList.push({
                    Id: i,
                });
            }  
        }
        console.log('centacPartsList------>',centacPartsList);
        component.set("v.centacPartsList", centacPartsList);
    },
    
})