({
    /*Method to get centac parts request list*/
    doInit : function(component, event, helper) {
        /*set spinner flag to display spinner*/
        component.set("v.isLoading", true);
        helper.setupTable(component);
    },
    
    
    saveTableRecords : function(component, event, helper) {
        var recordsData = event.getParam("recordsString");
        var tableAuraId = event.getParam("tableAuraId");
        var listUpdateComplete = event.getParam("listUpdateComplete");
        var deletedRecordsId = event.getParam("deletedRecordsId");
        var action = component.get("c.updateRecords");
        var centacQuoteId = component.get("v.centacQuoteId");
        console.log('recordsData in saveTableRecords------>',recordsData);
        console.log('deletedRecordsId----->'+deletedRecordsId);
        var isUpdateTab = component.get("v.isUpdateTab");
        action.setParams({
            'jsonString': recordsData,
            'centacQuoteId':centacQuoteId,
            'deletedRecordsId':deletedRecordsId,
            'listUpdateComplete':listUpdateComplete,
            'isUpdateTab' :isUpdateTab
        });
        action.setCallback(this,function(response){
            if(response.getState() === "SUCCESS"){
                var data = response.getReturnValue().dataList;
                helper.setPartList(component,data);
                var datatable = component.find(tableAuraId);
                var updatedData = component.get("v.centacPartsList");
                datatable.finishSaving("SUCCESS",updatedData); 
            }else{
                var datatable = component.find(tableAuraId);
                datatable.finishSaving("Error",null,'Mandatory Field are not filled');   
            }
            
        });
        $A.enqueueAction(action);        
    },
    
})