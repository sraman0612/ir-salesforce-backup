({
    invoke : function(component, event, helper) {
        var type = component.get("v.type").toLowerCase();
        var title = component.get("v.title");
        var message = component.get("v.message");
        
        helper.showToast(component, event, helper,type,title, message);
    }
})