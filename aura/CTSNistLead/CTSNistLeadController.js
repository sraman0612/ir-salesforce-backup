({
    init : function (component) {
        // Find the component whose aura:id is "flowData"
        var flow = component.find("flowData");
        // In that component, start your flow. Reference the flow's API Name.
        flow.startFlow("Create_NIST_Mkt_Lead");
        //$A.get("e.force:closeQuickAction").fire();
    },
    handleStatusChange : function (component, event) {
        //CreatedLead
        if(event.getParam("status") === "FINISHED") {
            //$A.get("e.force:closeQuickAction").fire();
            var outputVariables = event.getParam("outputVariables");
            var outputVariables = event.getParam("outputVariables");
            var outputVar;
            for(var i = 0; i < outputVariables.length; i++) {
                outputVar = outputVariables[i];
                if(outputVar.name === "CreatedLead") {
                    console.log('In createLead if condition');
                    var urlEvent = $A.get("e.force:navigateToSObject");
                    urlEvent.setParams({
                        "recordId": outputVar.value,
                        "isredirect": "true"
                    });
                    urlEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
            }
        }
    }
})