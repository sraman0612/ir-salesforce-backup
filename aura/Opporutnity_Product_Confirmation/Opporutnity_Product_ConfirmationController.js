/**
 * @File Name          : Opporutnity_Product_ConfirmationController.js
 * @Description        :
 * @Author             : Murthy Tumuluri
 * @Group              :
 * @Last Modified By   : Murthy Tumuluri
 * @Last Modified On   : 7/26/2019, 9:07:55 AM
 * @Modification Log   :
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    6/20/2019, 12:54:34 PM   Murthy Tumuluri     Initial Version
 **/
({
    parentComponentEvent: function(component, event, helper) {
        var existingList = component.get("v.prodList");
        var products = event.getParam("arguments");
        console.log("argumets", JSON.stringify(products));
        var myObj = JSON.parse(JSON.stringify(products));
        var selectedArr = [];
        selectedArr.push(myObj.selectedProdList.selectedProdList);
        var opptyId = myObj.selectedProdList.recId;
        var selModel = myObj.selectedProdList.selectedModel;
        var emeaVar = myObj.selectedProdList.emea;
        console.log("JSON.stringify(myObj.selectedProdList):::", opptyId);
        console.log("JSON.stringify(myObj.selModel):::", selModel);
        console.log("JSON.stringify(myObj.selModel):::", emeaVar);
        console.log(
            "JSON.stringify(myObj.selectedProdList):::",
            JSON.stringify(myObj.selectedProdList)
        );
        component.set("v.opportunityId", opptyId);
        console.log("selectedArr:::", selectedArr);
        if (selectedArr) {
            if (existingList.length == 0) {
                component.set("v.prodList", selectedArr);
                component.set("v.seBaseModel", myObj.selectedProdList.selectedModel);
                component.set("v.emeaCheck",emeaVar);
            } else {
                existingList.push(myObj.selectedProdList.selectedProdList);
                component.set("v.prodList", existingList);
                component.set("v.seBaseModel", myObj.selectedProdList.selectedModel);
                component.set("v.emeaCheck",emeaVar);
            }
        }

        helper.callServer(
            component,
            "c.getPriceCheck",
            function(response) {
                console.log("getPriceCheck", response);
                component.set("v.priceCheck", response);
                /*Server call to bring base model details*/
            },
            {
                recId: opptyId
            }
        );
    },
    handleDelete: function(component, event, helper) {
        var buttonVal = event.getSource().get("v.value");
        var existingList = component.get("v.prodList");
        for (var i = 0; i < existingList.length; i++) {
            if (existingList[i].Id == buttonVal.Id) {
            existingList[i].CC_Quantity_Per__c = null;
            existingList.splice(i, 1);
            }
        }
        component.set("v.prodList", existingList);
        var cmpEvent = component.getEvent("confirmationEvent");
        //Set event attribute value
        cmpEvent.setParams({
            addededProd: buttonVal
        });
        cmpEvent.fire();
    },
    handleCustomAddition: function(component, event, helper) {
        var addedList = component.get("v.prodList");
        var opptyId = component.get("v.opportunityId");
        var seleModel = component.get("v.seBaseModel");
        console.log("@@ addedList : ", addedList);
        console.log("@@ opptyId : ", opptyId);
        console.log("@@ seleModel : ", seleModel);

        helper.callServer(
            component,
            "c.addOppLineItems",
            function(response) {
                if(response.includes("006")
                    || response.includes("a9x")){
                    helper.navigateToObject(response,"related",true);
                }else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "message": response,
                        "type": "error",
                        "mode": "sticky"
                    });
                    toastEvent.fire();
                }
                    
            },
            {
                prodList: addedList,
                oppId: opptyId,
                baseModelProd: seleModel
            }
        );
    }
});