({
    doInit : function(component, event, helper) {
        if(component.get('v.label') == '' || component.get('v.label') == null) {
            helper.callAction( component, 'c.getFieldLabel', {
                'objectName' : component.get('v.objectName'),
                'fieldName'  : component.get('v.fieldName')                
            }, function( data ) {
                component.set('v.label', data);
            });   
        }
        helper.callAction( component, 'c.getPicklistOptions', {
            'objectName' : component.get('v.objectName'),
            'fieldName'  : component.get('v.fieldName'),
            'recordTypes' : component.get('v.recordTypes'),
            'pickValsToReturn' : component.get('v.pickVals')
        }, function( data ) {
			data = Array.from(new Set(data.map(JSON.stringify))).map(JSON.parse);
            data.sort(function(a, b) { //Sort by label, alphabetically ASC
                return a.label == b.label ? 0 : +(a.label > b.label) || -1;
            });
            var defaultOption = {};
            defaultOption.label = component.get("v.defaultOptionLabel");
            defaultOption.value = "";
            if(component.get("v.addDefaultOption")) data.unshift(defaultOption);
            component.set('v.value', "");
            component.set('v.options', data);                            
        });
    }
})