({
    handlePathSelect : function (component, event, helper) {
      var stepName = event.getParam("detail").value;
      if (stepName=='4. Close'){
        component.set("v.isModalOpen", true);
      } else {
        component.set("v.showCloseReason", false);
        if(stepName=='1. Open'){
          component.set("v.simpleRecord.Status", "1. Open");
          component.set("v.simpleRecord.PT_Status__c", "1. Open");
        } else if(stepName=='2. Working'){
          component.set("v.simpleRecord.Status", "2. Working");
          component.set("v.simpleRecord.PT_Status__c", "2. Working");
        } else if(stepName=='3. Nurturing'){
          component.set("v.simpleRecord.Status", "3. Nurturing");
          component.set("v.simpleRecord.PT_Status__c", "3. Nurturing");
        }
        component.saveMethod();
      }
    },
    handleCloseReasonSelect : function (component, event, helper) {
      var reason = component.find("closeReason").get("v.value");
      console.log("you chose close reason " + reason);    
        if (reason=='4a. Create Opportunity'){
          var urlEvent = $A.get("e.force:navigateToURL");
          var url = "/apex/PT_LeadConvert?id=" + component.get("v.recordId");
          urlEvent.setParams({"url": url, "isredirect": "true"});
          urlEvent.fire();  
        } else {
          if(reason=='4b. Forward to Distributor'){
            component.set("v.simpleRecord.Status", "4b. Forward to Distributor");
            component.set("v.simpleRecord.PT_Status__c", "4. Close");
            component.set("v.showDist", true);
            component.set("v.showCX", false);
          } else if(reason=='4c. Forward to CX'){
            component.set("v.simpleRecord.Status", "4c. Forward to CX");
            component.set("v.simpleRecord.PT_Status__c", "4. Close");
            component.set("v.showCX", true);
            component.set("v.showDist", false);
          } else if(reason=='4d. Bad Lead'){
            component.set("v.simpleRecord.Status", "4d. Bad Lead");
            component.set("v.simpleRecord.PT_Status__c", "4. Close");
            component.set("v.showDist", false);
            component.set("v.showCX", false);
          }
        }
    },
    sendAndCreateOpportunity : function (component, event, helper) {
        var distributorContactId = component.get("v.simpleRecord.Distribution_Account_s_Contact__c");
        console.log('distributorContactId',distributorContactId);
        
        var urlEvent = $A.get("e.force:navigateToURL");
        var messageToDistributor =  component.get("v.simpleRecord.PT_Message__c");
        console.log('messageToDistributor',messageToDistributor);
        var url = "/apex/PT_LeadConvert";
        url += "?id="+ component.get("v.recordId");
        url += "&forwardToDistributor=true";
        url += "&distributorContactId="+distributorContactId;
        url += "&messageToDistributor="+messageToDistributor;

        console.log('url',url);
        urlEvent.setParams({"url": url, "isredirect": "true"});
        urlEvent.fire(); 
        
    },
    closeModal : function (component, event, helper) {
      component.set("v.isModalOpen", false);
    },
    saveRecord : function(component,event,helper){
      component.find("recordHandler").saveRecord($A.getCallback(function(saveResult) {
        console.log('  save is ' + JSON.stringify(saveResult));
        if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
          component.set("v.isModalOpen", false);
        } else {
          if (saveResult.state === "INCOMPLETE") {
          } else {
            var errors="";
            for (var i = 0; saveResult.error.length > i; i++){
              errors = errors + saveResult.error[i].message;
            } 
            component.set("v.recordError", errors);
          }
        }
      }));
    }
})