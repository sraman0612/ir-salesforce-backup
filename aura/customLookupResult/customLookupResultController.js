({
    selectAsset : function(component, event, helper){      
    // get the selected asset product from list  
      var getSelectProduct = component.get("v.oAsset");
    // call the event   
      var compEvent = component.getEvent("oSelectedAssetEvent");
    // set the Selected asset product to the event attribute.  
         compEvent.setParams({"assetByEvent" : getSelectProduct });  
    // fire the event  
         compEvent.fire();
    },
})