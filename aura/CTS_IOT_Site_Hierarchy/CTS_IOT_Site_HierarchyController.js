({
    init: function (cmp, event, helper) {
        console.log("CTS_IOT_Site_Hierarchy:init");

        // Update breadcrumbs
        // var navEvent = $A.get("e.c:CTS_IOT_Navigation_Event");
        
        // navEvent.setParams({
        //     "label" : cmp.get("v.pageTitle"),
        //     "url" : "/site-hierarchy"
        // });

        // navEvent.fire();         

        helper.initialize(cmp, event, helper);
    }, 

    handlePageChange: function(cmp, event, helper){
        console.log("handlePageChange");
        cmp.set("v.offSet", event.getParam("offSet"));
        helper.initialize(cmp, event, helper);
    },

    handleGlobalPageSizeChange: function(cmp, event, helper){
        console.log("handleGlobalPageSizeChange");
        let pageSize = event.getParam("pageSize");
        console.log("pageSize: " + pageSize);
        cmp.set("v.pageSize", pageSize);
        helper.initialize(cmp, event, helper);
    },    

    handlePageSizeChange: function(cmp, event, helper){
        console.log("handlePageSizeChange");
        cmp.set("v.pageSize", event.getParam("pageSize"));
        helper.initialize(cmp, event, helper);

        // Notify any other sibling list view components of the list page size change
        var pageSizeChangeEvent = $A.get("e.c:CTS_IOT_List_Page_Size_Change");
        pageSizeChangeEvent.setParams({pageSize: event.getParam("pageSize")});
        pageSizeChangeEvent.fire();           
    },

    handleSearchTermChange: function(cmp, event, helper){
        console.log("handleSearchTermChange: " + event.getParam("searchTerm"));

        cmp.set("v.searchTerm", event.getParam("searchTerm"));
        helper.initialize(cmp, event, helper);
    } 
})