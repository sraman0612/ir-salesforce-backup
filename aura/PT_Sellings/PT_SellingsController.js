({
	init : function(component, event, helper) {
        var accIdVal = component.get("v.recordId");
		var action = component.get("c.getYears");
        action.setParams({ 
           accId: accIdVal
        });
		action.setCallback(this, function(response) {
		    var state = response.getState();
            if (state === "SUCCESS") {
                var response = response.getReturnValue();
                component.set("v.years",response.allYears);
                component.set("v.sellingLst",response.ptSellinglst);
            }
        });
        $A.enqueueAction(action);
	},
    selling : function(component, event, helper) {
	    var yearVal = component.find("yearId").get("v.value");
        var accIdVal = component.get("v.recordId");
        if(yearVal == ''){
             helper.showResultUpdate(component, event, 'Error', 'Please Select a valid Year', 'error');
        }
        else{
            var action = component.get("c.getSellings");
            action.setParams({ 
                accId: accIdVal,
                year: yearVal
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    component.set("v.sellingLst",response.getReturnValue())
                }
            });
        
           $A.enqueueAction(action);
        }
	},
    
    saveSellings : function(component, event, helper) {
        
        var accIdVal = component.get("v.recordId");
	    var yearVal = component.find("yearId").get("v.value");
        var sellingLstVal = component.get("v.sellingLst");
		var action = component.get("c.savePTSellings");
        action.setParams({ 
		    accId: accIdVal,
		    year: yearVal,
            sellingLst: sellingLstVal
		});
		action.setCallback(this, function(response) {
		    var state = response.getState();
            if (state === "SUCCESS") {
              helper.showResultUpdate(component, event, 'Success', 'PT Sellings have been updated', 'success');
            }
            else{
              helper.showResultUpdate(component, event, 'Error', 'Something went wrong.', 'error');
            }
        });
        $A.enqueueAction(action);
   }
	
})