/**
* Schedulable class used to send emails to community users
* for yesterday's published articles
* */
/*
* Revisions:
08-Oct-2024 : Update code against Case #03123284 - AMS Team
*/
global class CTS_TechDirect_KnowledgeReportSchdlr implements Schedulable {
    Set<Id>articalIdSet = new Set<Id>();
    global EmailTemplate knowledgeTemplate {
        get {
            if(knowledgeTemplate == null) {
                String templateName = 'KnowledgeReport';
                List< EmailTemplate > EmailTemplateList = [SELECT Id, Name, DeveloperName FROM EmailTemplate WHERE Name = :templateName];
                if(EmailTemplateList != null && EmailTemplateList.isEmpty() == false) {
                    knowledgeTemplate = EmailTemplateList.get(0);
                }            
            }
            return knowledgeTemplate;
        } set;
    }
    
    global void execute(SchedulableContext sc) { 
        /** Changes Done for case #00964962
// To get total count of article published YESTERDAY
// To check article count to send email */
        Integer articleCount = Database.countQuery(getArticleCountQuery());
        //System.debug('##articleCount::-'+articleCount);
        if(Test.isRunningTest()) {
            articleCount = 1;
        }
        if(articleCount > 0) {
            // To get list of users */
            List < User > userList = (List < User >) Database.query(getUserQuery());
            if(userList != null && userList.isEmpty() == false) {
                List < User > recepientList = new List < User > ();
                for(Integer i = 0; i < userList.size(); i++) {                    
                    recepientList.add(userList.get(i));
                }
                sendEmail(recepientList);
            }
        }
    }
    
    /* Revisions: 08-Oct-2024 : Update code against Case #03123284 - AMS Team */
    private void sendEmail(List < User > userList) {
        String templateName = 'KnowledgeReport';         
        if(String.isBlank(templateName) == false) { 
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            if(knowledgeTemplate != null) {              
                mail.setTemplateId(knowledgeTemplate.Id);
                mail.setTargetObjectId(UserInfo.getUserId()); 
                mail.setSaveAsActivity(false);
                // Prepare list for CC / BCC email address                        
                List < String > ToEmailList = new List < String > ();
                for (User userObj: userList) {
                    ToEmailList.add(userObj.Email);
                }
                //System.debug('### To Addresses ### ' + ToEmailList);
                //mail.setToAddresses(ToEmailList);
                
                //List<String> toAddressList = (Label.CTS_TechDirect_ToAddresses).split(';');
                // mail.setToAddresses(ToEmailList);
                
                List<String> BccAddressList = (Label.CTS_TechDirect_BCcAddresses).split(';');
                mail.setBCcAddresses(BccAddressList);
                
                List<String> ccAddressList = (Label.CTS_TechDirect_CcAddresses).split(';');
                mail.setCcAddresses(ccAddressList);
                //System.debug('ccAddressList::'+ccAddressList);
                
                // Query OrgWideEmailAddress by DisplayName and set the OrgWideEmailAddressId for the email if found: #03123284 - AMS Team
                List<OrgWideEmailAddress> listOWEA = [SELECT Id, Address, DisplayName
                                                      FROM OrgWideEmailAddress
                                                      WHERE DisplayName='sfdc_no_reply@irco.com'];
                //System.debug('OweaTest'+owea);
                if (!listOWEA.isEmpty()) {
                    mail.setOrgWideEmailAddressId(listOWEA[0].Id);
                }
                
                Messaging.SendEmailResult[] resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                    mail
                        });
            }                    
        }   
    }
    
    public String getUserQuery() {
        String query = 'SELECT Id, Email FROM User WHERE UserRole.Name = \'CTS TechDirect NA - IR Support Agents\'';        
        query = query + ' AND isActive = true';
        return query;
    }
    
	/* Revisions: 08-Oct-2024 : Update code against Case #03123284 - AMS Team */
    public String getArticleCountQuery() {
        
        try{
            List<Knowledge__kav> knowledgeList =[SELECT id,ArticleNumber,(SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections) FROM Knowledge__kav ORDER BY LastPublishedDate DESC limit 500 ];
            Set<String> filterCategories = new Set<String>();
            List<String> articleNos = new List<String>();	// Create a list to store the relevant article numbers: #03123284 - AMS Team
            
            for (IngersollProductCategories__mdt categoryRecord : [SELECT FilterCategory__c FROM IngersollProductCategories__mdt]) {
                filterCategories.add(categoryRecord.FilterCategory__c);
            }
            
            if(knowledgeList !=null && !knowledgeList.isEmpty()){
                for (Knowledge__kav knowledge : knowledgeList) {
                    for (Knowledge__DataCategorySelection dc : knowledge.DataCategorySelections) {
                        if (filterCategories.contains(dc.DataCategoryName)) {
                            articleNos.add(knowledge.ArticleNumber) ;  
                        }  
                    }
                }
            }       
            
            // Storing articles Id with same article numbers irrespective of their translated versions: #03123284 - AMS Team
            if(knowledgeList !=null && !knowledgeList.isEmpty()){
                for(Knowledge__kav knowledgeVar:knowledgeList){
                    if(articleNos.contains(knowledgeVar.ArticleNumber) ){
                        articalIdSet.add(knowledgeVar.Id);
                    }
                }
            }    
            //system.debug(articalIdSet.size());
            //system.debug('article Ids are>>'+articalIdSet);
            
       }catch(Exception e){
            System.debug('Error Message>>'+e.getMessage());   
       }
        /** Code fix for case #02994530
// To segregate the data based on Data Category */
        String query = 'SELECT Count() FROM Knowledge__kav WHERE PublishStatus = \'Online\'';        
        if(!Test.isRunningTest()) {
            query = query + ' AND LastModifiedDate = YESTERDAY' ; 
            query =query+' AND Id IN:articalIdSet';
        }         
        
        query = query + ' AND IsVisibleInCsp = true';
        query = query + ' AND IsLatestVersion = true';
        query = query + ' AND CTS_TechDirect_Article_Type__c  != \'Tech Note\'';
        query = query + ' AND CTS_TechDirect_Include_on_Email__c  = true';
        query = query + ' limit 500';
        return query;
    }
}