public class ContentVersion_TriggerHandler {
    
    public static void beforeUpdate(map<Id, ContentVersion> newMap, map<Id, ContentVersion> oldMap){
        avoidSalesCallPlanNotesOverride(newMap, oldMap);
    }
    
    public static void afterUpdate(Map<Id, ContentVersion> newMap, Map<Id, ContentVersion> oldMap){
        syncAttachmentToInterlynx(newMap, oldMap);
    }
    
    //When we are writing a note if we stop and it got saved and we back to write it will override the note title that we had estabelished
    public static void avoidSalesCallPlanNotesOverride(map<Id, ContentVersion> newMap, map<Id, ContentVersion> oldMap){
        set<Id> targetIds = new set<Id>();
        
        for(ContentVersion currRecord : newMap.values()){
            System.debug('insertedContentVersion Title == > '+currRecord.Title);
            System.debug('insertedContentVersion FileType == > '+currRecord.FileType);
            if(currRecord.FileType == 'SNOTE' && currRecord.Title == 'Untitled Note' && oldMap.get(currRecord.Id).Title != 'Untitled Note'){
                targetIds.add(currRecord.Id);
            }
        }

        if(targetIds.size() > 0 ){
            List<ContentDocumentLink> contentDocLinkList = [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink where ContentDocumentId IN:targetIds];
            
            Schema.sObjectType objectType = Schema.getGlobalDescribe().get('Opportunity_Sales_Call_Plan__c');
            String salesCallPlanPrefix =  objectType.getDescribe().getKeyPrefix();
            
            for(ContentDocumentLink currCDLRecord: contentDocLinkList){
                if(!String.valueOf(currCDLRecord.LinkedEntityId).contains(salesCallPlanPrefix)){
                    targetIds.remove(currCDLRecord.Id);
                }
            }
            
            for(Id currRecords : targetIds){
                newMap.get(currRecords).Title = oldMap.get(currRecords).Title;
            }
            
        }
    }
    
    public static void syncAttachmentToInterlynx(Map<Id, ContentVersion> newMap, Map<Id, ContentVersion> oldMap) {
        Map<String, List<String>> leadToFileMap = new Map<String, List<String>>();
    
        for(ContentVersion conver : newMap.values()) {
            if(String.isNotBlank(conver.Lead_Ids_ready_to_sync_with_Interlynx__c)) {
                // Collect lead and file IDs for syncing
                Set<String> newAddedLeadIds = new Set<String>();
                newAddedLeadIds.addAll(conver.Lead_Ids_ready_to_sync_with_Interlynx__c.split(','));
    
                if(newAddedLeadIds.size() > 0) {
                    for(String leadId : newAddedLeadIds) {
                        if (!leadToFileMap.containsKey(leadId)) {
                            leadToFileMap.put(leadId, new List<String>());
                        }
                        leadToFileMap.get(leadId).add(conver.Id); // Add file ID to the lead
                    }
                }
            }
        }
    
        if (!leadToFileMap.isEmpty()) {
            // Batch leads and their file IDs, enqueuing fewer jobs to avoid the limit exception
            List<Map<String, List<String>>> leadToFileBatches = batchLeadToFileMap(leadToFileMap, 10); // Adjust batch size as needed
            
            for (Map<String, List<String>> batch : leadToFileBatches) {
                System.enqueueJob(new CalloutInterlynxForFiles(batch));
            }
        }
    }
    
    // Helper method to batch leads and files into smaller groups to avoid exceeding queueable limits
    private static List<Map<String, List<String>>> batchLeadToFileMap(Map<String, List<String>> leadToFileMap, Integer batchSize) {
        List<Map<String, List<String>>> batches = new List<Map<String, List<String>>>();
        Map<String, List<String>> currentBatch = new Map<String, List<String>>();
        Integer count = 0;
    
        for (String leadId : leadToFileMap.keySet()) {
            currentBatch.put(leadId, leadToFileMap.get(leadId));
            count++;
    
            if (count >= batchSize) {
                batches.add(currentBatch);
                currentBatch = new Map<String, List<String>>();
                count = 0;
            }
        }
    
        // Add the last batch if not empty
        if (!currentBatch.isEmpty()) {
            batches.add(currentBatch);
        }
    
        return batches;
    }
}