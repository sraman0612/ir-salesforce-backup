/*------------------------------------------------------------

Controller to update incomplete contacts on case.

History
<Date>       <Authors Name>     <Brief Description of Change>
28-Feb-2022  Capgemini			Initial Version (User Story - 1177) 

Revisions:
19-June-2024 : Update code against Case #03077072 - AMS Team

------------------------------------------------------------*/

public  without sharing class searchContactForCaseController {
    
    // Wtapper Class to Capture the Name and othe info of contacts
    public class ContactList {
        @AuraEnabled
        public String Id {get; set;}
        @AuraEnabled
        public String name {get; set;}
        @AuraEnabled
        public String email {get; set;}
        @AuraEnabled
        public String accountName {get; set;}
        @AuraEnabled  public String conRecordTypeName {get; set;}//Added on 28th June 2022
    }
    
    //Method to Show Contact on LWC Component on Load
    @AuraEnabled(cacheable=true)
    public static List<ContactList> searchContactNameMethod (String conStrName, String conStrEmail){
        List<ContactList> conList = new List<ContactList>();
        String keyNameString = '%' +  conStrName + '%'; // Added RecordType.Name IN SOQL on 28th June 2022-->
        for(Contact con : [Select Id, Name, Email, RecordType.Name, Account.Name, AccountId From Contact Where Name like:keyNameString LIMIT 5000]){
            ContactList con1 = new ContactList();
            con1.Id = con.Id;
            con1.name = con.Name != null ? con.Name : '';
            con1.email = con.Email != null ? con.Email : '';
            con1.accountName = con.AccountId != null ? con.Account.Name : '';
            con1.conRecordTypeName = String.isNotBlank(con.RecordType?.Name) ? con.RecordType?.Name : ''; //Added on 28th June 2022
            conList.add(con1);
        }
        return conList;
    }    
    
    //Method to update single record only
    @AuraEnabled
    public static boolean updateCurrentContact (String recordId, String contactRecordId){
        Boolean isSuccess = false;
        system.debug('updateCurrentContact call'+recordId);
        system.debug('contact Id **'+contactRecordId);
        List<case> caseToUpadte = new List<Case>();
        try{
            List<Contact> contactsToUpdate = new List<Contact>();
            Id contactId = [Select Id, AccountId, ContactId FROM Case WHERE Id =: recordId].ContactId;  // 03 May 2022            
            contactsToUpdate.add(new Contact(Id = contactId, Delete_Contact__c = true));            
            for(Contact conObj : [SELECT id, Name,AccountId, Email FROM Contact WHERE  Id =: contactRecordId]){
                Case caseObj = new Case();
                caseObj.Id = recordId; //Selected case Id
                caseObj.AccountId = conObj.AccountId;//Updating the Account
                caseObj.ContactId = conObj.Id;// Updating the Contact with Selected Contact id
                caseToUpadte.add(caseObj);
                contactsToUpdate.add(new Contact(Id=conObj.Id, Delete_Contact__c=true));
		
                          
            }
            if(!contactsToUpdate.isEmpty()) {
                UPDATE contactsToUpdate;
            }
            if(caseToUpadte.size() > 0 ){                
                update caseToUpadte; // DML to Update the Case and with new Contact and There Account
                isSuccess = true;

                //Updated code for Case-03077072 by AMS team (Pratik)
                //checking if existing contact have multiple cases associated 
                contact newContact  = new Contact(id = contactRecordId);
                contact conObj  = new Contact(id = contactId);
                //Merging the Old Contact  With New Contact , old Will Delete
                List<case> listOfCasesForconObj = [SELECT id, ContactId FROM Case WHERE ContactId =: conObj.Id ];
                    if(listOfCasesForconObj.isEmpty()){
                        merge newContact conObj; // As we are not performing update neither setting up the AccountId,Account will not update
            }
                
                return isSuccess;// if update success return success
            }
	   
	   }catch(exception excep){
            system.debug('***single Record Exception **'+excep.getMessage());
            system.debug('***Single Record Exception line number**'+excep.getLineNumber());
            throw new AuraHandledException(excep.getMessage()); // if error throw exception to LWC
        }
        
        return isSuccess; 
    }
    
    //Method to update all the case with the Contact
    @AuraEnabled
    public static boolean updateAllContact (String recordId, String contactRecordId){
        Boolean isSuccess = false;
        system.debug('updateAllContact call'+recordId);
        system.debug('contact Id **'+contactRecordId);
        List<case> caseToUpadte = new List<Case>();
        try{
           //Getting current Case record's Contact Id
            Id contactId = [Select Id, AccountId, ContactId FROM Case WHERE Id =: recordId].ContactId;
             contact conObj  = new Contact(id = contactId);
            //Getting the Object of selected Contact
            List<Contact> newContact = [SELECT Id,Delete_Contact__c,AccountId FROM Contact WHERE Id  =: contactRecordId];
           //Merging the Old Contact  With New Contact , old Will Delete
            merge newContact[0] conObj; // As we are not performing update neither setting up the AccountId,Account will not update
             
               isSuccess = true;
        }catch(exception excep){
            system.debug('***All Record Exception **'+excep.getMessage());
            system.debug('***All Record Exception line number**'+excep.getLineNumber());
            throw new AuraHandledException(excep.getMessage());
        }
        
        
        return isSuccess; 
    }
    //To check if that selected contact has multiple Case associated
    @AuraEnabled
    public static boolean isMultipleContacts( String currenCaseRecordId){
        List<Case> listOfcase = [SELECT id, ContactId FROM Case WHERE Id =: currenCaseRecordId limit 1];
        system.debug('Size oflist is ==>'+listOfcase.size());
        
        system.debug('**listOfcase is **'+listOfcase);
        if(listOfcase[0].ContactId != null){
            List<case> caseOfList = [SELECT id, ContactId FROM Case WHERE ContactId =: listOfcase[0].ContactId ];
            if(caseOfList.size() > 1){
                return true;
            }

        }
        return false;
    }
    
}