/*******************************************************
* 
* Class : ContractRejectController
* Purpose : To Serialize and Deserialize the Contract fields to JSON text area field and vice versa.used in ARO_Update_Contract and Contract_After_Create_Update_Flow FLows. 
* Developer : Sairaman Koraveni
* Organization : Cognizant
* 
********************************************************/



public class ContractRejectController {
    @InvocableMethod(label='Store and Retrieve JSON Data on Contract Record' description='This method is used to Serialize the record data to JSON string and also to Deserialize the JSON string back to object record')
    public static List<FlowOutput> invoke(List<FlowInput> inputs){
       List<FlowOutput> output = new List<FlowOutput>();
       if(inputs[0].operatingMode == 'Serialize')
       {
          Contract contr = [SELECT Id,Main_Discount_Level__c,OEM_Completes_Level_ARO__c,OEM_Parts_Level_ARO__c,NIS_Level_ARO__c,Diaphragm_Piston_Pumps_Level_ARO__c,Fluid_Power_Level_ARO__c,Electric_Pumps_Level_EVO__c FROM Contract WHERE Id =: inputs[0].ContractId];
           
           String jsonStr = JSON.serialize(contr);
           
           //System.debug('Serialized JSON : ');
           //System.debug(jsonStr);
          
           
          FlowOutput floutput = new FlowOutput();
           floutput.jsonOutputString = jsonStr;
           floutput.outputContract = contr;
           output.add(floutput);
           
       }
        else if (inputs[0].operatingMode == 'Deserialize'){
           Contract contr = [SELECT Id,Account_Type__c,Prior_Contract_JSON__c,Main_Discount_Level__c,OEM_Completes_Level_ARO__c,OEM_Parts_Level_ARO__c,NIS_Level_ARO__c,Diaphragm_Piston_Pumps_Level_ARO__c,
                              Fluid_Power_Level_ARO__c,Electric_Pumps_Level_EVO__c 
                              FROM Contract WHERE Id =: inputs[0].ContractId]; 
           
            
            if(contr.Prior_Contract_JSON__c != null){
                Contract desContr =  (Contract)JSON.deserialize(contr.Prior_Contract_JSON__c,Contract.class);
                System.debug('Deserialized JSON : ');
           		System.debug(desContr);
                Contract outputCtr = new Contract();
                FlowOutput floutput = new FlowOutput();
           		floutput.jsonOutputString = '';
                if(contr.Account_Type__c == 'OEM'){
                    
                    outputCtr.OEM_Completes_Level_ARO__c = desContr.OEM_Completes_Level_ARO__c;
                    outputCtr.OEM_Parts_Level_ARO__c = desContr.OEM_Parts_Level_ARO__c;
                } 
                else if(contr.Account_Type__c == 'NIS'){
                    outputCtr.NIS_Level_ARO__c = desContr.NIS_Level_ARO__c;
                }
                else{
                   outputCtr.Diaphragm_Piston_Pumps_Level_ARO__c = desContr.Diaphragm_Piston_Pumps_Level_ARO__c;
                   outputCtr.Fluid_Power_Level_ARO__c = desContr.Fluid_Power_Level_ARO__c;
                   outputCtr.Electric_Pumps_Level_EVO__c = desContr.Electric_Pumps_Level_EVO__c;
                }
                outputCtr.Main_Discount_Level__c = desContr.Main_Discount_Level__c;
           		
                floutput.outputContract = outputCtr;
                output.add(floutput);
                
            }
           
        }
        return output;
    }
    
    public class FlowInput{
        @InvocableVariable(required = true)
        public String operatingMode;
        
        @InvocableVariable(required = true)
        public String ContractId;
    }
    
    public class FlowOutput{
        @InvocableVariable
        public Contract outputContract;
        
        @InvocableVariable
        public String jsonOutputString;
    }

}