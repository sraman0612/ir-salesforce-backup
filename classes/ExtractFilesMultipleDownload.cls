public with sharing class ExtractFilesMultipleDownload {
    
   
    @AuraEnabled(cacheable=true)
    public static ContentDocument[] getFiles(String recordId) {
         Set<String> queryRecordIds = new Set<String>();
        queryRecordIds.add(recordId);
		List<EmailMessage> emailMessageList = [SELECT Id, Name, ParentId FROM EmailMessage WHERE ParentId =:recordId];
        System.debug(emailMessageList);
        if(emailMessageList != null && emailMessageList.size()>0){
            for(EmailMessage em: EmailMessageList){
                queryRecordIds.add(em.Id);
            }
        }

        List<ContentDocumentLink> linkList = [SELECT ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN:queryRecordIds];
        Set<Id> ContentDocumentIds  = new Set<Id>();
        for(ContentDocumentLink link : LinkList){
            ContentDocumentIds.add(link.ContentDocumentId);
        }
        System.debug(ContentDocumentIds);
        return [
            SELECT
            Id, Title,CreatedDate, FileExtension,ContentSize, LatestPublishedVersionId,LatestPublishedVersion.File_Size__c
            FROM ContentDocument WHERE Id IN :ContentDocumentIds AND FileExtension != 'snote'
        ];
    }

   
}