@isTest
public class PT_LeadEmailServiceInboundTest {
  static testMethod void test1l() {
    Id ptLeadRTId = [SELECT Id FROM RecordType WHERE sobjecttype='Lead' and Developername='PT_Lead'].Id;
    lead l   = new Lead(RecordTypeId=ptLeadRTId, FirstName='PTLead', LastName='Test', Company='EYE_R_SEE_OH', Country='Belgium',email='pttestlead@irco.com');
    insert l;
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    Messaging.InboundEmail.TextAttachment attachTxt = new Messaging.InboundEmail.TextAttachment();
    email.subject = 'Test PT Lead Eml Inbound';
    email.plainTextBody = 'theBody  {ref:' + l.Id + '}';
    String [] toAddys = new List<String>{'ptleademailservice@salesforce.irco.com'};
    email.toAddresses = toAddys;
    env.fromAddress = 'powertoolsleadtest@irco.com';
    attachTxt.body = 'thebodyoftheattachment';
    attachTxt.fileName = 'ptattachtext.txt';
    attachTxt.mimeTypeSubType = 'text/plain';
    email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachTxt };
    PT_LeadEmailServiceInbound es = new PT_LeadEmailServiceInbound();
    es.handleInboundEmail(email,env);
  }
}