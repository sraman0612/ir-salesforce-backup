/**=====================================================================
 * Appirio, Inc
 * Name: OpportunityStageIndicator_Test
 * Description: Test Class for OpportunityStageIndicatorHelper for Stage validations
 * Created Date: May 11, 2015
 * Created By: Sachin Agarwal (Appirio)
 * Updated By : Surabhi Sharma(Appirio)
 =====================================================================*/
@isTest
public with sharing class OpportunityStageIndicatorHelper_Test {
    static testMethod void testOpportunityStageValidations() {
        PageReference pageRef = Page.OpportunityStageIndicatorPage;
        Test.setCurrentPage(pageRef);
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_New_EU_Account' 
                                    and SobjectType='Account' limit 1].Id;
        
        // Create a test account
         Account testAcct = new Account (Name = 'My Test Account');
         testAcct.BillingCity = 'xyz';
         testAcct.BillingPostalCode = '14';
         testAcct.BillingStreet = 'tezwy';
         testAcct.BillingState = 'test';
         testAcct.BillingCountry = 'AU';
         testAcct.ShippingCity = 'city1';
         testAcct.ShippingCountry = 'USA';
         testAcct.ShippingState = 'CA';
         testAcct.ShippingStreet = '13, street2';
         testAcct.ShippingPostalCode = '123';
         testAcct.CTS_Global_Shipping_Address_1__c = '13';
         testAcct.CTS_Global_Shipping_Address_2__c = 'street2';         
         testAcct.County__c = 'test County';   
         testAcct.recordtypeid=AirNAAcctRT_ID;
         insert testAcct;
    
        // Creates first opportunity
        Id AirNAOppRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_EU' 
                                    and SobjectType='Opportunity' limit 1].Id;
        Opportunity oppt = new Opportunity(Name ='New mAWS Deal',
                                           AccountID = testAcct.ID,
                                           StageName = 'Stage 1. Qualify',
                                           CloseDate = System.today(),
                                           recordtypeid=AirNAOppRT_ID,
                                           Amount = 3000);  
        insert oppt;
        
        ApexPages.StandardController sc = new ApexPages.standardController(oppt);      
        OpportunityStageIndicatorHelper controller = new OpportunityStageIndicatorHelper(sc);
        ApexPages.currentPage().getParameters().put('id', oppt.id);
        
        System.assert(controller.lstStages[0].bolCompleted == true);
        
        controller.saveChanges();
    }
}