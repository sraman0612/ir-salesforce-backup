/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the Test class for AccountPlan_Page_Extension.
------------------------------------------------------------*/
@istest(seeAllData=false)
public with sharing class AccountPlan_Page_Extension_Test {
    
    @testSetup
    static void dataSetup(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        //Ashwini: 27-SEP-17 :System.runAs used ,to avoid MIXED_DML_OPERATION error
        System.runAs(u){
        account testAcc=TestDataFactory.createAccount('TestAcc1','IR Comp EU');
        testAcc.ownerid=u.id;
        insert testAcc;
        contact testCon=TestDataFactory.createContact('Test','Test',testAcc.id,'Customer Contact','9029316494');
        testCon.ownerid=u.id;
        insert testCon;
        }
    }
    
    public static testMethod void AccountPlan_Page_Extension_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            Account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.contact=[Select id from contact limit 1];
            aPlan.getRoleOnAccount();
            aPlan.getRoles();
            aPlan.getRelationshipStrength();
            aPlan.getCompetitorNames();
            //aPlan.getAccCompetitorNames();    
            aPlan.selectedEmployeeName='test';
            aPlan.selectedContactTitle='test';
            aPlan.selectedContactRole='test';
            aPlan.selectedCustomerName='test';
            aPlan.selectedOwnerName='test';
            aPlan.selectedDMakerName='test';
            aPlan.selectedIREmployeeName='test';
            aPlan.selectedReortsToName='test';
            aPlan.selectedBusinessTitleName='test';
            aPlan.selectedContactId=null;
            aPlan.selectedEmployeeId=null;
            aPlan.allemployees=new list<user>();
            aPlan.allIRAccount=new list<User>();
            aPlan.allCont=new list<Contact>();
            aPlan.allCust=new list<Contact>();
            aPlan.allOwner=new list<User>();
            aPlan.allDMaker=new list<Contact>();
            system.assertequals(aPlan.acc.id,testAcc.id);
        }
        Test.Stoptest();
    }
    
    public static testMethod void SaveAccPlan_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.SaveAccPlan();
            system.assertequals(aPlan.acc.isNew__c,true);
            AccountPlan_Page_Extension aPlan1=new AccountPlan_Page_Extension(controller);
            aPlan1.SaveAccPlan();
            aPlan1.contact=new contact();
            aPlan1.contact.firstname='test';
            aPlan1.contact.lastname='test';
            aPlan1.contact.Phone='9029316494';
            aPlan1.cancel();
        }
        Test.Stoptest();
        
    }
    
    public static testMethod void addNewRow_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            ApexPages.currentPage().getParameters().put('addRowObjectName','Account_Action_Plan__c');
            aPlan.addRow = 'addRow';
            aPlan.addNewRow();
            //aPlan.addRow = 'addRow';
            //aPlan.addMore('Account_Action_Plan__c');
            system.assertequals(aPlan.addRow,'addRow');
        }
        Test.Stoptest();
    }
    
    public static testMethod void removeNewRow_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.rowToRemove=1;
            aPlan.addNewRow();
            //ApexPages.currentPage().getParameters().put('removeRowObjectName','Account_Action_Plan__c');
            //aPlan.removeNewRow();
            //ApexPages.currentPage().getParameters().put('removeRowObjectName','IR_Account_Team__c');
            aPlan.removeNewRow();
            //system.assertequals(aPlan.count,1);
        }
        Test.Stoptest();
    }
    
    public static testMethod void showPopup_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.objName='IREmployee';
            aPlan.showPopup();
            aPlan.objName3='IRAccountTeam';
            aPlan.showPopup();
            aPlan.objName='KeyAccountContact';
            aPlan.showPopup();
            aPlan.objName1='KeyAccountContact';
            aPlan.showPopup();
            aPlan.objName2='PotFutureOpp';
            aPlan.showPopup();
            //system.assertequals(aPlan.displayEmployeePopup,true);
        }
        Test.Stoptest();
    }
    
    public static testMethod void closePopup_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            //aPlan.closePopup();
            //system.assertequals(aPlan.displayEmployeePopup,false);
        }
        Test.Stoptest();
    }
    
    public static testMethod void getId_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.getId();
            //system.assertequals(aPlan.displayEmployeePopup,false);
        }
        Test.Stoptest();
    }
    
    public static testMethod void searchRecord_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            apexpages.currentpage().getparameters().put('id',testAcc.id);
            AccountPlan_Page_Extension aPlan=new AccountPlan_Page_Extension(controller);
            aPlan.searchString='test';
            aPlan.objName='IREmployee';
            aPlan.searchRecord();
            aPlan.objName3='IRAccountTeam';
            aPlan.searchRecord();
            aPlan.objName4='KeyCustomerInitiative';
            aPlan.searchRecord();
            aPlan.objName='KeyAccountContact';
            aPlan.searchRecord();
            aPlan.objName1='KeyAccountContact';
            aPlan.searchRecord();
            aPlan.objName2='PotFutureOpp';
            aPlan.searchRecord();
            system.assertequals(aPlan.objName,'KeyAccountContact');
        }
        Test.Stoptest();
    }

}