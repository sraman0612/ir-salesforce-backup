public with sharing class CTS_TechDirect_OutOfOfficeAsstCtrl {
    
    private virtual class ResponseBase{
    	public Boolean success = true;
    	public String errorMessage = '';  
        public Boolean warning = false;
        public List<String> warningMessage = new List<String>();
    }
    
    private class Init extends ResponseBase{
    
    	public Out_of_Office_Settings__c outOfOfficeSettings;
    	public Out_of_Office_Case_Routings__c[] caseRoutings;
    }
    
    private class SaveRequest{
    	public Out_of_Office_Settings__c outOfOfficeSettings;
    	public Out_of_Office_Case_Routings__c[] caseRoutings;
    }
    
    private class userActiveException extends Exception {}
    
    @AuraEnabled
    public static String getInit(){
    	
    	Init init = new Init();
    	
    	try{
    		
    		init.outOfOfficeSettings = Out_of_Office_Settings__c.getInstance(UserInfo.getUserId());
    		init.caseRoutings = [Select Id, Name, User_ID__c, Record_Type_ID__c, Backup_User_Queue_ID__c From Out_of_Office_Case_Routings__c Where User_ID__c = :UserInfo.getUserId()];
    		
    		// TODO: Lookup record type and backup user/queue names for display purposes
    	}
		catch (Exception e){
			
			system.debug('error: ' + e.getMessage());
			init.success = false;
			init.errorMessage = e.getMessage();
		}
    	
    	return JSON.serialize(init);
    }
    
    private static Boolean isUserActive(String uId){
        User u = [SELECT Id, isActive FROM User WHERE Id =: uId LIMIT 1];
        if(u.IsActive){
            return true;
        }
        
        return false;
    }
    
    @AuraEnabled
    public static String save(String saveRequest){
    	
    	ResponseBase response = new ResponseBase();
        
        //check if any existing/enabled OOO settings point to current user
        Map<String,Out_of_Office_Case_Routings__c> oooRoutings = Out_of_Office_Case_Routings__c.getAll();
        
        //list to save users whose backups are current user        
        List<Id> referencingUsers = new List<Id>();
        Id usrId = UserInfo.getUserId(); //get current User ID
        Out_of_Office_Settings__c currSettings = Out_of_Office_Settings__c.getInstance(UserInfo.getUserId());
        Out_of_Office_Case_Routings__c tempRouting;
        for(String ooo : oooRoutings.keySet()){ //check each OOO routing to see if routing is active, current, and assigned to current user
            tempRouting = oooRoutings.get(ooo);
            if(Id.valueOf(tempRouting.Backup_User_Queue_ID__c) == usrId){
                if(ooo.indexOf('_') != -1)
                	referencingUsers.add(ooo.substring(0, ooo.indexOf('_')));
            }
        }
        
        //check if users referencing current user as backup have OOO enabled currently
        Out_of_Office_Settings__c tempSetting;
        for(Id u : referencingUsers){
            tempSetting = Out_of_Office_Settings__c.getValues(u);
            if(tempSetting.Enabled__c == true && currSettings.Enabled__c &&
               (tempSetting.Start_Date__c <= currSettings.End_Date__c && tempSetting.End_Date__c >= currSettings.Start_Date__c)){
                response.warning = true;
                response.warningMessage.add('Your Out-of-Office Settings currently overlap with another user who is forwarding cases to you for a portion of the same time-frame that you have selected.');
                break;
            }
        }
                
    	
    	System.Savepoint sp = Database.setSavepoint();
    	
    	try{
    		
			SaveRequest request = (SaveRequest)JSON.deserialize(saveRequest, SaveRequest.class);
			system.debug('request: ' + request);
			
			// Insert/update the out of office settings
			upsert request.outOfOfficeSettings;
			
			// Clear and insert any remaining case routings
			delete [Select Id From Out_of_Office_Case_Routings__c Where User_ID__c = :UserInfo.getUserId()];
			
			// Clear the Ids before insert
			for (Out_of_Office_Case_Routings__c caseRouting : request.caseRoutings){
				caseRouting.Id = null;
                
                //check if primary backup is user
                //if user, check if backup has OOO on
                if(caseRouting.Name.substring(caseRouting.Name.length() - 1) == '_'){
                    if(caseRouting.Backup_User_Queue_ID__c.substring(0, 3) == '005'){
                        //check if user is active
                        if(!isUserActive(caseRouting.Backup_User_Queue_ID__c)){
                            throw new userActiveException('Selected Backup User is Inactive');
                        }
                        
                        Out_of_Office_Settings__c backupSettings = Out_of_Office_Settings__c.getInstance(caseRouting.Backup_User_Queue_ID__c);
                        if(backupSettings.Enabled__c == true && currSettings.Enabled__c &&
                           (backupSettings.Start_Date__c <= currSettings.End_Date__c && backupSettings.End_Date__c >= currSettings.Start_Date__c)){
                            response.warning = true;
                            response.warningMessage.add('The User you have selected for Case Forwarding also has active Out-of-Office forwarding for a portion of the same time-frame that you have selected.');
                        }
                    }
                }
                
			}
			
			insert request.caseRoutings;
    	}
		catch (Exception e){
			
			system.debug('error: ' + e.getMessage());
			Database.rollback(sp);
			response.success = false;
			response.errorMessage = e.getMessage();
		}
    	
    	return JSON.serialize(response);
    }    
}