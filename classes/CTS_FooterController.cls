/**************************
* CTS_FooterController
****************************/
public class CTS_FooterController { 
    //update the number of up and down votes for the given article
    @auraEnabled
    public static Object getInit(String recordId){
        List<Vote> votes = [SELECT Id, Type, LastModifiedById FROM Vote 
                            WHERE ParentId =: recordId AND LastModifiedById =: UserInfo.getUserId()];
        String vType = '';
        if(!votes.isEmpty()){
            vType = votes[0].Type;
        }
        
        String communityId = Network.getNetworkId();
        Boolean following = false;
		EntitySubscription[] subs = [SELECT Id FROM EntitySubscription 
                                     WHERE NetworkId =: communityId AND ParentId =: recordId 
                                     AND SubscriberId =: UserInfo.getUserId()
                                     LIMIT 1000
                                    ];
        if(subs.size() > 0) following = true;
        
        return JSON.serialize(new List<Object>{vType, following});
    }
    /**
     * Up/Down Vote an Article
     * @param articleId
     * @param type
     * @return Boolean
     */
	@AuraEnabled
    public static Boolean vote(String aId, String type) {        
        try {
        	List<Vote> votes = [SELECT Id, Type, ParentId FROM Vote 
                                WHERE ParentId =: aId AND LastModifiedById =: UserInfo.getUserId()];
            if(!votes.isEmpty()) delete votes;
            Vote v = new Vote(ParentId = aId, Type = type);
			insert v;
        } catch(Exception e) {            
            System.debug('e**' + e.getMessage());
            return false;
        }      
        return true;
    }
    
    @AuraEnabled
    public static Boolean followUnfollow(String recId, String action) {
        String communityId = Network.getNetworkId();
        try {
            if(action == 'follow') {
            	ConnectApi.ChatterUsers.follow(communityId, UserInfo.getUserId(), recId);
            } else {
            	EntitySubscription[] subs = [SELECT Id FROM EntitySubscription 
                                             WHERE NetworkId =: communityId AND ParentId =: recId 
                                             AND SubscriberId =: UserInfo.getUserId()
                                             LIMIT 1000
                                            ];
                if(subs.size() > 0) ConnectApi.Chatter.deleteSubscription(communityId, subs[0].Id);
            }
        } catch(Exception e) {
            throw new AuraException(e.getMessage());
        }
        return true;
    }
    //Submit Feedback
    @auraEnabled
    public static String submitArticleFeedback(String message, String articleId){
        String title = '';
        String articleNumber = '';
        String articleURL = '';
        for(Knowledge__kav knowledge : [select id, title,ArticleNumber from Knowledge__kav where id = :articleId]){
            title = knowledge.title;
            articleNumber = knowledge.articleNumber;
            articleURL = System.Label.CTS_Knowledge_URL + knowledge.id + '/view';
        }
        if(title.length() > 180){
            title = title.substring(0,180) + '..';
        }
        String contactId;
        for(User u : [select id, contactId from User where id = : userInfo.getUserId()]){
            contactId = u.contactId;
        }
        String caseId = CTS_CaseHelper.createCase(message,CTS_Constants.CASE_SUBJECT + title +' (' + articleNumber + ')',contactId,CTS_Constants.CASE_TYPE_FEEDBACK,articleURL);
        if(caseId != null && caseId.startsWith('500')){
        	return 'SUCCESS';
        }else{
            return 'FAILURE';
        }
    }
}