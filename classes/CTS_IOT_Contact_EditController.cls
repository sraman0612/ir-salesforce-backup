public with sharing class CTS_IOT_Contact_EditController {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Data_Service_WithoutSharing.CommunityContact contact;
        @AuraEnabled public List<CTS_IOT_Data_Service.UserSiteAccess> userSites;
    }

    @AuraEnabled
    public static InitResponse getInit(Id contactID){

        InitResponse response = new InitResponse();

        try{
            response.contact = CTS_IOT_Data_Service_WithoutSharing.getContacts(contactID).communityContacts[0];
            response.userSites = CTS_IOT_Data_Service.getContactSiteAccess(contactID);
        }
        catch(Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }

    @AuraEnabled
    public static LightningResponseBase updateUser(String userStr){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{
            
            CTS_IOT_Data_Service_WithoutSharing.CommunityContact comContact = (CTS_IOT_Data_Service_WithoutSharing.CommunityContact)JSON.deserialize(userStr, CTS_IOT_Data_Service_WithoutSharing.CommunityContact.class);

            if (comContact.UserId != null){
            
                User userToUpdate = new User(Id = comContact.UserId);

                Boolean isSuperUser = CTS_IOT_Data_Service_WithoutSharing.getIsSuperUser(comContact.UserId);

                // Identify a super user change
                if (isSuperUser != comContact.IsSuperUser){
                    userToUpdate.ProfileId = comContact.IsSuperUser ? CTS_IOT_Data_Service_WithoutSharing.defaultSuperUserCommunityProfile.Id : CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id;
                }     

                system.debug('userToUpdate: ' + userToUpdate);

                CTS_IOT_Data_Service_WithoutSharing.updateUser(userToUpdate);       
            }          
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return new LightningResponseBase();
    }    

    @AuraEnabled
    public static LightningResponseBase updateUserSiteAccess(Id contactId, String userSitesStr){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{

            List<CTS_IOT_Data_Service.UserSiteAccess> updatedUserSites = (List<CTS_IOT_Data_Service.UserSiteAccess>)JSON.deserialize(userSitesStr, List<CTS_IOT_Data_Service.UserSiteAccess>.class);       

            // Get the user's current site access and check for net new/removed sites
            List<CTS_IOT_Data_Service.UserSiteAccess> currentUserSites = CTS_IOT_Data_Service.getContactSiteAccess(contactId);
            Set<Id> sitesToAdd = new Set<Id>();
            Set<Id> sitesToRemove = new Set<Id>();

            for (CTS_IOT_Data_Service.UserSiteAccess updatedUserSite : updatedUserSites){

                if (updatedUserSite.isUpdateable){

                    for (CTS_IOT_Data_Service.UserSiteAccess currentUserSite : currentUserSites){

                        if (currentUserSite.account.Id == updatedUserSite.account.Id){
                            
                            if (updatedUserSite.hasAccess && !currentUserSite.hasAccess){
                                sitesToAdd.add(updatedUserSite.account.Id);
                            }
                            else if (!updatedUserSite.hasAccess && currentUserSite.hasAccess){
                                sitesToRemove.add(updatedUserSite.account.Id);
                            }
                        }
                    }
                }
            }

            system.debug('sitesToAdd: ' + sitesToAdd);
            system.debug('sitesToRemove: ' + sitesToRemove);

            if (sitesToAdd.size() > 0){

                AccountContactRelation[] acrs = new AccountContactRelation[]{};

                for (Id siteToAdd : sitesToAdd){

                    acrs.add(
                        new AccountContactRelation(
                            ContactId = contactId,
                            AccountId = siteToAdd
                        )
                    );                    
                }

                insert acrs;
            }

            if (sitesToRemove.size() > 0){
               CTS_IOT_Data_Service_WithoutSharing.removeSitesFromContact(contactId, sitesToRemove);
            }
        }
        catch(Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;        
    }        

    public class ApprovalActionResponse extends LightningResponseBase{
        @AuraEnabled public Boolean approve;
    }

    @AuraEnabled
    public static ApprovalActionResponse approvalAction(Id contactId, Boolean approve, String reason){

        ApprovalActionResponse response = new ApprovalActionResponse();
        System.SavePoint sp = Database.setSavepoint();

        try{
            
            response.approve = approve;

            CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse ccr = CTS_IOT_Data_Service_WithoutSharing.getContacts(contactId);  
            CTS_IOT_Data_Service_WithoutSharing.CommunityContact cc = ccr.communityContacts[0];           

            Contact c = new Contact(
                Id = cc.ContactId,
                CTS_IOT_Community_Status__c = approve ? 'Approved' : 'Rejected',
                CTS_IOT_Community_Reject_Reason__c = reason,
                CTS_IOT_Registration_Asset__c = null,
                CTS_IOT_Contact_Match_Detail__c = null
            );

            CTS_IOT_Data_Service_WithoutSharing.updateContact(c);

            if (approve){
                
                // If user already has account, do we need to check/add another account via account-contact-relation?
                if (cc.RegistrationAssetMatch != null && cc.RegistrationAssetMatch.AccountId != null && cc.Site != null && cc.RegistrationAssetMatch.AccountId != cc.Site.Id){

                    Integer relationshipCheck = CTS_IOT_Data_Service_WithoutSharing.checkContactAccountRelation(contactId, cc.RegistrationAssetMatch.AccountId);

                    if (relationshipCheck == 0){
                        CTS_IOT_Data_Service_WithoutSharing.addSitesToContact(contactId, new List<Id>{cc.RegistrationAssetMatch.AccountId});
                    }
                }
            }
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }    

    @AuraEnabled
    public static LightningResponseBase activateUser(Id contactId){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{

            CTS_IOT_Data_Service_WithoutSharing.CommunityContactResponse ccr = CTS_IOT_Data_Service_WithoutSharing.getContacts(contactId);  
            CTS_IOT_Data_Service_WithoutSharing.CommunityContact cc =  ccr.communityContacts[0];           

            if (cc.UserId != null){
                CTS_IOT_Data_Service_WithoutSharing.activateUser(cc.UserId);
            }
            else{

                CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest request = new CTS_IOT_Data_Service_WithoutSharing.RegisterUserRequest();
                request.firstName = cc.FirstName;
                request.lastName = cc.LastName;
                request.email = cc.Email;
                request.username = cc.Email;
                request.siteId = cc.Site != null ? cc.Site.Id : null;
                request.superUser = false;
                request.sendEmailConfirmation = true;

                CTS_IOT_Data_Service_WithoutSharing.RegisterUserResponse regResponse = CTS_IOT_Data_Service_WithoutSharing.registerUser(request);
                response.success = regResponse.success;
                response.errorMessage = regResponse.errorMessage;
                system.debug('newUserId: ' + regResponse.newUserId);
            }
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }

    @AuraEnabled
    public static LightningResponseBase deactivateUser(Id contactId){

        LightningResponseBase response = new LightningResponseBase();
        System.SavePoint sp = Database.setSavepoint();

        try{            
            CTS_IOT_Data_Service_WithoutSharing.deactivateUser(contactId);
        }
        catch (Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
            Database.rollback(sp);
        }

        return response;
    }         
}