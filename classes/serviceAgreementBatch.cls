global class serviceAgreementBatch implements Schedulable, Database.Stateful,Database.Batchable<sObject>{
    
    global string queryStr;
    
    public Database.QueryLocator start(Database.BatchableContext BC){return Database.getQueryLocator(queryStr);}
    
    public void execute(Database.BatchableContext BC, List<Service_Agreement__c> scope)
    {
        Service_Agreement__c [] saList = new List<Service_Agreement__c>();
        for(Service_Agreement__c sa : scope){
                sa.X365DaysPriorEndDate__c = TRUE;
                saList.add(sa);
        }
        update saList;
    }
    
    public void finish(Database.BatchableContext BC){}
    
    global void execute(SchedulableContext sc) {
        
        //Date d = system.today();
        //d.addDays(365);
        //String dStr = d.year() + '-' + d.month() + '-' + d.day() + 'T00:00:00.000Z';
        //String qStr = 'SELECT Id, X365DaysPriorEndDate__c ';
        //qStr +=       'FROM Service_Agreement__c ';
        //qStr +=       'WHERE Days_Until_Expiration__c <= 365 AND x365DaysPriorEndDate__c == FALSE AND Agreement_status__c = \'Active\'';
        serviceAgreementBatch statements = new serviceAgreementBatch();
        statements.queryStr = 'SELECT Id, X365DaysPriorEndDate__c FROM Service_Agreement__c WHERE Days_Until_Expiration__c <= 180 AND Days_Until_Expiration__c > 160 AND x365DaysPriorEndDate__c = FALSE';
        Database.executeBatch(statements,50);
    }
}