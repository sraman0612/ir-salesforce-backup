@IsTest
public with sharing class PT_DSMP_BAT02_CountTasksRecords_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for PT_DSMP_BAT02_CountTasksRecords_TEST
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_TARGET__c> lstPTDsmpTarget;
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd;
	static List<PT_DSMP_Objectives__c> lstPTDsmpObjective = new List<PT_DSMP_Objectives__c>();
	static List<Task> lstTask = new List<Task>();

    static Id rtAccPowertools;

	static {
		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();

		mainUser = PT_DSMP_TestFactory.createAdminUser('PT_DSMP_BAT02_CountTasksRecords_TEST@test.com', 
														PT_DSMP_Constant.getProfileIdAdmin());
        insert mainUser;

        System.runAs(mainUser){

        	Account testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');
        	testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            lstAccount.add(testAcc);
        	insert lstAccount;

        	lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf(System.Today().year() + 1) ));
        	insert lstPTDsmpYear;

        	lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test ' ,
        												lstAccount[0].Id,
        												lstPTDsmpYear[0].Id
        												));
        	insert lstPTDsmp;

        	for(integer i=1;i<=6;i++){
        		PT_DSMP_Objectives__c c = PT_DSMP_TestFactory.createDSMPObjectives('# Objective-by this year -'+i,
        																			lstAccount[0].Id,
        																			String.valueOf(i),
        																			lstPTDsmp[0].Id);
        		c.Objective__c = '# Objective-by this year -'+i;
        		lstPTDsmpObjective.add(c);
        	}
        	insert lstPTDsmpObjective;

        	for(Integer i=0;i<10;i++){
        		lstTask.add(PT_DSMP_TestFactory.createTask('test ' + i,
        													lstPTDsmpObjective[0].Id,
        													System.today().addDays(-1),
        													'In Progress'));
        	}
        	insert lstTask;

        }

	}


	@isTest static void test_runBatch_countTask(){
        System.runAs(mainUser){
        	Test.startTest();
        	Database.executeBatch(new PT_DSMP_BAT02_CountTasksRecords());
        	Test.stopTest();
        }
   	}


   	@isTest static void test_runBatch_updateTask(){
        System.runAs(mainUser){
        	Test.startTest();
        	lstTask[0].ActivityDate = System.today();
        	update lstTask[0];
        	Test.stopTest();
        }
   	}


   	@isTest static void test_runBatch_deleteTask(){
        System.runAs(mainUser){
        	Test.startTest();
        	delete lstTask[0];
        	Test.stopTest();
        }
   	}


}