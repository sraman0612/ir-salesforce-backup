@IsTest
public with sharing class PT_DSMP_BAT01_createRecAccountDVP_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for PT_DSMP_BAT01_createRecAccountDVP
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------*/

	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_TARGET__c> lstPTDsmpTarget;
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd;

    static Id rtAccPowertools;

	static {

		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
		System.debug('mgr rtAccPowertools ' + rtAccPowertools);
        PT_Record_types__c settings = PT_Record_types__c.getOrgDefaults();
        settings.Account_RT_Id__c = rtAccPowertools;
        upsert settings PT_Record_types__c.Id;

		mainUser = PT_DSMP_TestFactory.createAdminUser('PT_DSMP_BAT01_createRecAccountDVP@test.COM', 
														PT_DSMP_Constant.getProfileIdAdmin());
        insert mainUser;

        System.runAs(mainUser){

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf(System.today().year()+1) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf(System.today().year()) ));
            insert lstPTDsmpYear;

        	Account testAcc = TestDataFactory.createAccountByDevName('TestAcc1','CTS_OEM_EU');//IR Comp OEM EU
        	testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
        	insert testAcc;

        	lstPTDsmpSharedObjective.add(PT_DSMP_TestFactory.createDSMPSharedObjectives('## objectives Number One',
        																				'STAR',
        																				 1,
        																				 lstPTDsmpYear[0].Id));
        	insert lstPTDsmpSharedObjective;


        }

	}


	@isTest static void test_runBatch_newAccount(){
        System.runAs(mainUser){ 
        	Test.startTest();
        	Account testAcc1 = TestDataFactory.createAccountByDevName('TestAcc2','CTS_OEM_EU');//IR Comp OEM EU
        	testAcc1.PT_BO_Customer_Number__c = '123456';
            testAcc1.PT_Is_DVP__c = 'DVP';
            testAcc1.PT_Account_Segment__c = 'STAR';
            testAcc1.recordTypeId = rtAccPowertools;
        	insert testAcc1;

        	PT_DSMP_BAT01_createRecordsAccountDVP obj = new PT_DSMP_BAT01_createRecordsAccountDVP();
            DataBase.executeBatch(obj);
            Test.stopTest();
        }
    }


	@isTest static void test_runBatch(){
        System.runAs(mainUser){ 
            Test.startTest();
        	PT_DSMP_BAT01_createRecordsAccountDVP obj = new PT_DSMP_BAT01_createRecordsAccountDVP();
            DataBase.executeBatch(obj);
            Test.stopTest();
        }
    }


    @isTest static void test_runBatch_NPD(){
        System.runAs(mainUser){ 
            Test.startTest();

            PT_DSMP_CS_Batch__c settings1 = PT_DSMP_CS_Batch__c.getOrgDefaults();
            settings1.Fiscal_Date__c = System.today().addDays(-1);
            settings1.Number_of_Objectives__c = 6;
            settings1.DSMP_Objective_Placeholder__c = '#Objective Placeholder';
            upsert settings1 PT_DSMP_CS_Batch__c.Id;

        	String currentYear = String.valueOf( System.today().year() );
        	List<PT_DSMP_Year__c> lstCurrentYear = new List<PT_DSMP_Year__c>([SELECT Id, Name
        																	  FROM PT_DSMP_Year__c
        																	  WHERE Name =: currentYear]);
            System.debug('mgr lstCurrentYear ' + lstCurrentYear);

        	lstPTDsmpNPD.add(PT_DSMP_TestFactory.createDSMPNPD('test', lstCurrentYear[0].Id));
        	insert lstPTDsmpNPD;

        	PT_DSMP_BAT01_createRecordsAccountDVP obj = new PT_DSMP_BAT01_createRecordsAccountDVP();
            DataBase.executeBatch(obj);
            Test.stopTest();
        }
    }



}