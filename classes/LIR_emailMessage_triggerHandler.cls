public class LIR_emailMessage_triggerHandler {
    /**22-12-2021 - added emailTriggerMethod logic as part of delta components*/
/*    public static void emailTriggerMethod(List<EmailMessage> ems){
      
        //added by @Capgemini
         Email_Message_Setting__mdt settings=Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
         if (settings.CTS_OM_Email_Trigger_Enabled__c){
            isInsertBefore(ems);
         }
        //end
        
    }*/
    public static void isInsertBefore(List<EmailMessage> ems){
    //added by @Capgemini 23 December'21
      Email_Message_Setting__mdt  settings = [SELECT DeveloperName,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c,CTS_OM_Email_Trigger_Enabled__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case'];

       /* 
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_North_Central_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_South_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'CTS_NA_South_Central_Parts', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'South_Area_Area_Specialists', 'In Progress');
        CTS_NCP_EmailMessage_Trigerhandler.reopenCase(ems, 'North_Central_Area_Area_Specialists', 'In Progress');*/
        if (settings.CTS_OM_Email_Trigger_Enabled__c){
            System.debug('CTS email trigger enabled');
            CTS_OM_EmailMessage_TriggerHandler.beforeInsert(ems);
        }
    }
    
        
}