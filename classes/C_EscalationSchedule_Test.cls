/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class C_EscalationSchedule_Test {
      
    private static testMethod void testScheduler() {
        
        Case newCase = new Case();
        newCase.Status = 'New';
        newCase.Origin = 'Email';
        newCase.isEscalated = True;
        newCase.Subject = 'Test Case';
        newCase.Escalation_Count__c = 0;
        insert newCase;
        
        Test.startTest();
        System.schedule('Case Escalations Test ' + System.now().format(), '0 0 * * * ?', new C_EscalationSchedule() );
        Test.stopTest();
        
    }
    
    
    private static testMethod void testBatch() {
        
        Case newCase = new Case();
        newCase.Status = 'New';
        newCase.Origin = 'Email';
        newCase.isEscalated = True;
        newCase.Subject = 'Test Case';
        newCase.Escalation_Count__c = 0;
        insert newCase;
        
        List<Case> cases = [Select Id, isEscalated, Escalation_Count__c From Case Where isEscalated = true LIMIT 200];
        System.debug(cases);
        
        Test.startTest();
        String q = 'Select Id, isEscalated, Escalation_Count__c From Case';
        Id batchInstanceId = Database.executeBatch(new C_EscalationBatch(q), 1);
        Test.stopTest();      
        
    }
}