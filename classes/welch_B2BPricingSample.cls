// This sample is for the situation when the pricing is validated in an external service. For Salesforce internal price validation please see the corresponding documentation.

// This must implement the sfdc_checkout.CartPriceCalculations interface
// in order to be processed by the checkout flow and used for your Price Calculations integration.
global class welch_B2BPricingSample implements sfdc_checkout.CartPriceCalculations {
    global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
        try {
            // To retrieve sale prices for a customer, get the cart owner's ID and pass it to the external service.
            // 
            // In the real-life scenario, the ID will probably be an external ID
            // that identifies the customer in the external system,
            // but for simplicity we are using the Salesforce ID in this sample.
            Id customerId = [SELECT OwnerId FROM WebCart WHERE id = :cartId][0].OwnerId;
            
            // Type (optional but we want to set it to show this is a charge): 'Charge'.
            // Product2Id: The ID of a 'product' that is actually a shipping charge.
            // Quantity (optional): 1.0.
            // TotalPrice (optional): Total amount for this charge type cart item, including adjustments but excluding taxes.
            // ListPrice (required): List price of this cart item.
            // TotalListPrice (optional): Total amount for this cart item based on ListPrice, provided mostly for comparison.
            // TotalLineAmount (optional): Total amount for this cart item, based on sales price and quantity.
            // TotalAdjustmentAmount (optional): Total adjustments for this cart item.
            // CartId (required): The ID of the cart that this shipping charge belongs to.
            // CartDeliveryGroupId (required): Delivery group for this shipping charge.
            // Name (required): 'Shipping Cost' (can be any value).

            Id cartDeliveryGroupId = [SELECT CartDeliveryGroupId FROM CartItem WHERE CartId = :cartId][0].CartDeliveryGroupId;
            Product2 surChargeProd = [SELECT Name, Id, StockKeepingUnit FROM Product2 WHERE Name = 'Surcharge Fee'];
            WebCart currentCart = [SELECT Id, GrandTotalAmount, TotalProductAmount FROM WebCart WHERE Id=:cartId LIMIT 1];
            CartItem surchargeItem = new CartItem();
            List<CartItem> surcharge = [SELECT Id, Product2Id, Product2.Name, CartId, ListPrice, Quantity, TotalListPrice, TotalLineAmount, TotalPrice
                                        FROM CartItem
                                        WHERE CartId =: cartId AND Product2.Name = 'Surcharge Fee'];
            System.debug('SURCHARGE :: ' + surcharge);
            //validate only one surcharge item in cart.
            //list of cart item where surcharge item is product name
            //list of 1 
            //have 2 of test prod price = 40 surcharge = 10
            //remove 1 test prod price = 20 surcharge still = 10
            //if grandtotal < 50 then update surcharge pricing to be 50 minus price.
            //50 minus price witho fee
            //click checkout 
            //if greater than 50 plus have surcharge fee.
            
            if(!surcharge.isEmpty()){
                Decimal surchargeFeePrice = surcharge[0].ListPrice;
                System.debug(currentCart.GrandTotalAmount);
                System.debug(surchargeFeePrice);
                Decimal priceWithoutFee = surchargeFeePrice!=null ? currentCart.GrandTotalAmount - surchargeFeePrice : currentCart.GrandTotalAmount;
                Decimal difference = 50.00 - priceWithoutFee;
                if(surcharge?.size() == 1){
                    // Decimal surchargeFeePrice = surcharge[0].ListPrice;
                    // Decimal priceWithoutFee = currentCart.GrandTotalAmount - surchargeFeePrice;
                    // Decimal difference = 50.00 - priceWithoutFee;
                    // System.debug('GRAND TOTAL :: ' + currentCart.GrandTotalAmount);
                    // System.debug('PRICE WITHOUT FEE :: ' + priceWithoutFee);
                    // System.debug('DIFFERENCEW :: ' + difference);
                    if(priceWithoutFee > 50){
                        delete surcharge;
                    }
                    else if(priceWithoutFee < 50){
                        surcharge[0].ListPrice = difference;
                        surcharge[0].TotalListPrice = difference;
                        surcharge[0].TotalLineAmount = difference;
                        surcharge[0].TotalPrice = difference;
                        surcharge[0].Quantity = 1.0;
                        update surcharge;
                        update currentCart; 
                    }
                    else if(currentCart.GrandTotalAmount < 50){
                        System.debug('HERE FOR PRICE UNDER 50');
                        surcharge[0].ListPrice = difference;
                        surcharge[0].TotalListPrice = difference;
                        surcharge[0].TotalLineAmount = difference;
                        surcharge[0].TotalPrice = difference;
                        surcharge[0].Quantity = 1.0;
                        update surcharge;
                        update currentCart;
                    }
                }
                else if(surcharge?.size() > 1){
                    delete surcharge;
                    //create new cart item?
                    // System.debug('WE ARE HERE');
                    // surchargeItem.CartDeliveryGroupId = cartDeliveryGroupId;
                    // surchargeItem.Name = 'Surcharge Fee';
                    // surchargeItem.Product2Id = surChargeProd.Id;
                    // surchargeItem.CartId = currentCart.Id;
                    // surchargeItem.Type = 'Product';
                    // surchargeItem.Quantity = 1.0;
                    // surchargeItem.ListPrice = difference;
                    // surchargeItem.TotalListPrice = difference;
                    // surchargeItem.TotalLineAmount = difference;
                    // surchargeItem.TotalPrice = difference;
                    // surchargeItem.Sku = 'surchargefee';
                    // insert surchargeItem;
                    // update currentCart;
                }
            }
            else{
                System.debug('CREATE NEW');
                if(currentCart.GrandTotalAmount < 50.00){
                    Decimal surchargeFee = 50.00 - currentCart.GrandTotalAmount;
                    surchargeItem.CartDeliveryGroupId = cartDeliveryGroupId;
                    surchargeItem.Name = surChargeProd.Name;
                    surchargeItem.Product2Id = surChargeProd.Id;
                    surchargeItem.CartId = currentCart.Id;
                    surchargeItem.Type = 'Product';
                    surchargeItem.Quantity = 1.0;
                    surchargeItem.ListPrice = surchargeFee;
                    surchargeItem.TotalListPrice = surchargeFee;
                    surchargeItem.TotalLineAmount = surchargeFee;
                    surchargeItem.TotalPrice = surchargeFee;
                    surchargeItem.Sku = surChargeProd.StockKeepingUnit;
                    insert surchargeItem;
                    update currentCart;
                    System.debug('TRYING TO UPDATE CART AND ADD CHARGE :: ');
                }
            }
            //cart total minus fee less than 50? 
            // else delete charge
            

            // Get all SKUs and their sale prices (customer-specific prices) from the cart items.
            Map<String, Decimal> salesPricesFromSalesforce = new Map<String, Decimal>();
            for (CartItem cartItem : [SELECT Sku, SalesPrice FROM CartItem WHERE CartId = :cartId AND Type = 'Product']) {
                salesPricesFromSalesforce.put(cartItem.Sku, cartItem.SalesPrice);
            }
            
            // Get all sale prices for the products in the cart (cart items) from an external service
            // for the customer who owns the cart.
            // Map<String, Object> salesPricesFromExternalService = getSalesPricesFromExternalService(salesPricesFromSalesforce.keySet(), Id.valueOf(customerId));            
            
            // For each cart item SKU, check that the price from the external service
            // is the same as the sale price in the cart.
            // If that is not true, set the integration status to "Failed".
            for (String sku : salesPricesFromSalesforce.keySet()) {
                Decimal salesPriceFromSalesforce = salesPricesFromSalesforce.get(sku);
                integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
                // Decimal salesPriceFromExternalService = (Decimal)salesPricesFromExternalService.get(sku);
                // if (salesPriceFromExternalService == null){
                //    String errorMessage = 'The product with sku ' + sku + ' could not be found in the external system';
                //    return integrationStatusFailedWithCartValidationOutputError(
                //        integStatus,
                //        errorMessage,
                //        jobInfo,
                //        cartId
                //    );
                // } 
                // else if (salesPriceFromExternalService != salesPriceFromSalesforce){
                   // Add your logic here for when the price from your external service
                   // does not match what we have in Salesforce.
                   // For example, you may want to cause your pricing integration to fail.
                   // EXAMPLE: integStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
                   // 
                   // Our Heroku external service is a test service and returns a sale price of 0.00 for any SKU except 'SKU_FOR_TEST'.
                   // If the SKU of the product is 'SKU_FOR_TEST', the price returned by the external service is 100.
                   // For testing purposes, we set the integration status to SUCCESS if salesPriceFromExternalService is 0.00,
                   // regardless of the value of the Salesforce price 
                //    if (salesPriceFromExternalService == 0.00){
                //        integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
                //    }
                //    else {
                //        String errorMessage = 'The sale price has changed for the product with sku ' + sku + ': was ' 
                //                + salesPriceFromSalesforce + ', but now is '
                //                + salesPriceFromExternalService + '.';
                //        return integrationStatusFailedWithCartValidationOutputError(
                //            integStatus,
                //            errorMessage,
                //            jobInfo,
                //            cartId
                //        );
                //    }
                   // ----- End of the section that is only for testing.                   
                // }
                // else {
                //     // If the prices in the external system are the same as the prices in Salesforce, set integration status as SUCCESS.
                //     integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
                // }
            }
        } catch(Exception e) {
            // For testing purposes, this example treats exceptions as user errors, which means they are displayed to the buyer user.
            // In production you probably want this to be an admin-type error. In that case, throw the exception here
            // and make sure that a notification system is in place to let the admin know that the error occurred.
            // See the readme section about error handling for details about how to create that notification.
            System.debug('ERROR THROWN :: ' + e.getMessage() + ' AT LINE :: ' + e.getLineNumber());
            System.debug('THROWN REASON :: ' + e.getCause() + ' STACK STRING :: ' + e.getStackTraceString());
            return integrationStatusFailedWithCartValidationOutputError(
                integStatus,
                'An exception of type ' + e.getTypeName() + ' has occurred: ' + e.getMessage(),
                jobInfo,
                cartId
            );
        }
        return integStatus;
    }
    
    // private Map<String, Object> getSalesPricesFromExternalService (Set<String> skus, String customerId) {
    //     Http http = new Http();
    //     HttpRequest request = new HttpRequest();
    //     Integer SuccessfulHttpRequest = 200;
    //     // To access the service below you may need to add endpoint = https://b2b-commerce-test.herokuapp.com in Setup | Security | Remote site settings.
    //     request.setEndpoint('https://b2b-commerce-test.herokuapp.com/get-sales-prices?customerId=' 
    //                         + customerId + '&skus=' + JSON.serialize(skus));
    //     request.setMethod('GET');
    //     HttpResponse response = http.send(request);
    //     // If the request is successful, parse the JSON response.
    //     // The response includes the sale price for each SKU and looks something like this:
    //     // {"SKU-25-10028":0.00, "SKU-25-10030":0.00, "SKU_FOR_TEST":100.00}
    //     // Because this is a sample only and we want this integration to return success in order to allow the checkout to pass,
    //     // the external service created for this sample returns the exact list of SKUs it receives,
    //     // and the same sale price 0.00 for each SKU.
    //     if (response.getStatusCode() == SuccessfulHttpRequest) {
    //         Map<String, Object> salesPricesFromExternalService = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
    //         return salesPricesFromExternalService;
    //     }
    //     else {
    //         throw new CalloutException ('There was a problem with the request. Error: ' + response.getStatusCode());
    //     }
    // }
    
    private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
        sfdc_checkout.IntegrationStatus integrationStatus, String errorMessage, sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
            integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
            // In order for the error to be propagated to the user, we need to add a new CartValidationOutput record.
            // The following fields must be populated:
            // BackgroundOperationId: Foreign Key to the BackgroundOperation
            // CartId: Foreign key to the WebCart that this validation line is for
            // Level (required): One of the following - Info, Error or Warning
            // Message (optional): Message to be shown to the user
            // Name (required): The name of this CartValidationOutput record. For example CartId:BackgroundOperationId
            // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
            // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
            CartValidationOutput cartValidationError = new CartValidationOutput(
                BackgroundOperationId = jobInfo.jobId,
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage.left(255),
                Name = (String)cartId + ':' + jobInfo.jobId,
                RelatedEntityId = cartId,
                Type = 'Pricing'
            );
            insert(cartValidationError);
            return integrationStatus;
    }
}