public with sharing class AcceptLead {
    @AuraEnabled
    public static Lead changeLeadOwner(Id leadId) {
        Lead l    = [SELECT Id, OwnerId FROM Lead WHERE Id = :leadId];
        l.OwnerId = UserInfo.getUserId();
        Update l;
        Return l;
    }
}