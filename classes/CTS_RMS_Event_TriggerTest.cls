@isTest
public with sharing class CTS_RMS_Event_TriggerTest {

    public static Boolean processNotificationSubscriptions = false;    

    @TestSetup
    private static void createData(){

        Account acc = CTS_TestUtility.createAccount('Test Account', true);

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc.Id, assetRecTypeId, true);  
    }

    @isTest
    private static void processNotificationSubscriptionsEnabledTest(){

        Asset asset1 = [Select Id, AccountId From Asset];

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        notificationSettings.RMS_Alerts_Trigger_Enabled__c = true;
        insert notificationSettings;

        Test.startTest();

        CTS_RMS_Event__c rmsEvent = NotificationsTestDataService.createRmsEvent(true, asset1.Id, 'Warning', 'This is a warning!');

        system.assertEquals(true, processNotificationSubscriptions);

        Test.stopTest();
    }

    @isTest
    private static void processNotificationSubscriptionsDisabledTest(){

        Asset asset1 = [Select Id, AccountId From Asset];

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        notificationSettings.RMS_Alerts_Trigger_Enabled__c = false;
        insert notificationSettings;

        Test.startTest();

        CTS_RMS_Event__c rmsEvent = NotificationsTestDataService.createRmsEvent(true, asset1.Id, 'Warning', 'This is a warning!');

        system.assertEquals(false, processNotificationSubscriptions);

        Test.stopTest();
    }    
}