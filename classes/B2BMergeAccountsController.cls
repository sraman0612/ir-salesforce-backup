public with sharing class B2BMergeAccountsController {
    public static final String paymentDefaultOption = 'Credit Card';

    @AuraEnabled
    public static Account mergeAccountsFromLwc(Account prospectAccount, Account masterAccount) {
        try {
            List<Account> mergedAccounts = new List<Account>();
            Account getUpdatedMasterAccount;

            //if Account is not a buyer account then enable it
            if (!masterAccount.IsBuyer) {
                BuyerAccount buyerAccount = new BuyerAccount(
                        Name = masterAccount.Name,
                        IsActive = true,
                        BuyerId = masterAccount.Id,
                        CommerceType = 'Buyer'
                );
                insert buyerAccount;
            }
            getUpdatedMasterAccount = [SELECT Id, IsBuyer FROM Account WHERE Id = :masterAccount.Id LIMIT 1];
            Account updatedMasterAccount = new Account(
                    Id = masterAccount.Id,
                    Robuschi_B2B_Store_Payment_Option__c = String.isEmpty(masterAccount.Robuschi_B2B_Store_Payment_Option__c) ? paymentDefaultOption : masterAccount.Robuschi_B2B_Store_Payment_Option__c
            );

            String prospectAccountBuyerGroupId = B2B_Buyer_Group_Setting__mdt.getInstance(prospectAccount.ShippingCountry)?.Buyer_Group_Id__c;
            SharingDeleteHelper sharingDeleteHelper = new SharingDeleteHelper();
            // if master Account is buyer, check if prospect account's buyer group is already assigned to master account,
            // and remove it as Merge will fail if the same buyer group is reassigned
            if (getUpdatedMasterAccount.IsBuyer) {
                List<BuyerGroupMember> buyerGroupMembers = [SELECT BuyerGroupId, BuyerId FROM BuyerGroupMember WHERE BuyerId = :masterAccount.Id AND BuyerGroupId = :prospectAccountBuyerGroupId];
                if (buyerGroupMembers?.size() > 0 ) {
                    sharingDeleteHelper.deleteRecord(buyerGroupMembers[0]);
                }
            }

            reParentChildRecords(prospectAccount, masterAccount);
            // Merge accounts into master
            sharingDeleteHelper.mergeRecord(updatedMasterAccount, prospectAccount);

            sharingDeleteHelper.updateRecord(updatedMasterAccount);
            return updatedMasterAccount;
        }catch(Exception ex) {
            throw ex;
        }
    }

    private static void reParentChildRecords(Account prospectAccount, Account masterAccount) {
        SharingDeleteHelper sharingDeleteHelper = new SharingDeleteHelper();
        List<ContactPointAddress> reparentedCPAs = new List<ContactPointAddress>();
        for (ContactPointAddress contactPointAddress : [SELECT Id, ParentId FROM ContactPointAddress WHERE ParentId = :prospectAccount.Id]) {
            contactPointAddress.ParentId = masterAccount.Id;
            reparentedCPAs.add(contactPointAddress);
        }
        sharingDeleteHelper.updateRecords(reparentedCPAs);

        //if master account is not portal enabled, the copy the contacts to master account first and the delete the contacts form the prospect account
        if(!masterAccount.IsCustomerPortal) {
            List<Contact> reparentedContacts = new List<Contact>();
            List<Id> contactIds = new List<Id>();
            for (Contact contact : [SELECT Id, AccountId FROM Contact WHERE AccountId = :prospectAccount.Id]) {
                contact.AccountId = masterAccount.Id;
                reparentedContacts.add(contact);
                contactIds.add(contact.Id);
            }
            sharingDeleteHelper.updateRecords(reparentedContacts);

            List<AccountContactRelation> accountContacts = new List<AccountContactRelation>();
            for (AccountContactRelation accountContactRelation : [SELECT Id, AccountId, IsDirect FROM AccountContactRelation WHERE ContactId IN :contactIds]) {
                if (accountContactRelation.AccountId == masterAccount.Id) {
                    //accountContactRelation.IsDirect = true;
                }
                if (accountContactRelation.AccountId == prospectAccount.Id) {
                    //accountContactRelation.IsDirect = false;
                    accountContacts.add(accountContactRelation);
                }
            }
            sharingDeleteHelper.deleteRecords(accountContacts);
        }

    }

    //helper class to merge/update/delete Account record because the OWD for account is private and to delete Buyer group member
    public without sharing class SharingDeleteHelper {
        public void deleteRecord(SObject record) {
            delete record;
        }

        public void deleteRecords(List<SObject> records) {
            delete records;
        }

        public void mergeRecord(Account updatedMasterAccount, Account prospectAccount) {
            Database.MergeResult result = Database.merge(updatedMasterAccount, prospectAccount.Id);
            if (!result.isSuccess()) {
                String message;
                for (Database.Error err : result.getErrors()) {
                    // Write each error to the debug output
                    System.debug(err.getMessage());
                    message += err.getMessage();
                }
                throw new AuraHandledException('Merge Failed due to ' + message);
            }
        }

        public void updateRecord(SObject record) {
            update record;
        }

        public void updateRecords(List<SObject> records) {
            update records;
        }
    }
}