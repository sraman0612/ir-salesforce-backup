public class GD_contactTriggerHandler {
    public static Id STANDARD_CONTACT_RT_ID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();

    public static void beforeInsert(List<Contact> newContact){
       businessRelationshipInactiveCheck(newContact); 
    }
    
    public static void beforeUpdate(List<Contact> newContact, List<Contact> oldContact, Map<Id,Contact> oldMap){
       businessRelationshipInactiveCheck(newContact);
    }
    
    public static void afterUpdate(List<Contact> newContact){
        
    }
    
    // Changes by Capgemini Aman Kumar Date 16/2/2023. START Defect VPTECH-668
    public static void businessRelationshipInactiveCheck(List<Contact> newCon){
        System.debug('Inside businessRelationshipInactiveCheck >>');
        Boolean isDataMigrationUser = FeatureManagement.checkPermission('DataMigrationUser');
        User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
        if(user.profile.name != System.label.VPTECH_System_Admin_Check && user.Name != System.label.VPTECH_CPI_Integration_user && !isDataMigrationUser){            
            
            List<Id> accountIds = new List<Id>();
            List<Account> accounts = new List<Account>();
            for(Contact con : newCon){
                if(con.RecordTypeId == STANDARD_CONTACT_RT_ID){
                    accountIds.add(con.AccountId);
                }
            }
            accounts = [Select id,(Select id,status__c,Business__c from Business_Relationships__r) From Account Where id in : accountIds Limit 49999] ;
            for(Contact con : newCon){
                if(con.RecordTypeId == STANDARD_CONTACT_RT_ID ){
                    
                    for(Account acc : accounts){
                        if(acc.id == con.AccountId){
                            for(Business_Relationship__c br : acc.Business_Relationships__r){
                                if(con.Business__c != null && con.Business__c.containsIgnoreCase(br.Business__c) && br.Status__c == 'Inactive'){
                                    con.addError(System.Label.VPTECH_Opportunity_inactive_Account_Error);  
                                }
                            }
                            
                        }
                    }
                    
                }
            }
        }
    }
      // Changes by Capgemini Aman Kumar Date 16/2/2023. END  Defect VPTECH-668

}