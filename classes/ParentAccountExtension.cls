// 
// (c) 2015 Appirio, Inc.
//
//Generate an Extension Class for VF Page
//
// 27 March 2015  Surabhi Sharma     

// Class used as controller in VF page
public with sharing class ParentAccountExtension{
    public Account acc{get;set;}
    String childAccId;
    
    //Constructor
    public ParentAccountExtension(apexPages.StandardController stdCtrl){
        acc= (Account) stdCtrl.getRecord();  //Account record assigned to variable  
        childAccId = Apexpages.currentpage().getParameters().get('childacc'); // get the value of the parameter 'childacc' from the current url.
     }
    
    //Provide Save button functionality in VF page
    public PageReference save(){
    
        Id parentAccRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Parent Account').getRecordTypeId(); //get Parent Account record type ID
        acc.recordTypeId = parentAccRecTypeId; //assign parent account record type Id to acc
        insert  acc;
        
        if(childAccId !=null){        //Checks that childId passed is not null
            Account childAcc = new Account(Id=childAccId, ParentId=acc.Id);
            update childAcc;
        }
        
        return new PageReference('/' + childAccId); //return the record to child account
    }
    
}