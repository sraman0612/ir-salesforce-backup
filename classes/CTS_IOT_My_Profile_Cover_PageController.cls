public with sharing class CTS_IOT_My_Profile_Cover_PageController {

    private class CTS_IOT_My_Profile_Cover_PageControllerException extends Exception{}

    @TestVisible private static Boolean forceError = false;

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public User user;
        @AuraEnabled public Account account;
        @AuraEnabled public User accountManager;
    }

    @AuraEnabled(Cacheable = true)
	public static InitResponse getInit(){
		
		InitResponse response = new InitResponse();
		
		try{		

            if (Test.isRunningTest() && forceError){
                throw new CTS_IOT_My_Profile_Cover_PageControllerException('test error');
            }
			
            response.user = [Select Id, AccountId, ContactId, Name, FirstName, Username, FullPhotoURL From User Where Id = :UserInfo.getUserId()];
            response.account = response.user.AccountId != null ? [Select Id, Name, OwnerId From Account Where Id = :response.user.AccountId] : new Account();
            response.accountManager = response.account.OwnerId != null ? CTS_IOT_Data_Service_WithoutSharing.lookupUser(response.account.OwnerId) : new User();
		}
		catch(Exception e){
			system.debug('exception: ' + e.getMessage());
			response.success = false;
			response.errorMessage = e.getMessage();
		}
		
        system.debug('response: ' + response);

		return response;
	}   
}