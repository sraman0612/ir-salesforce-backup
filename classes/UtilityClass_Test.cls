/**=====================================================================
 * Appirio, Inc
 * Name: UtilityClass_Test
 * Description: Test Class for testing UtilityClass
 * Created Date: May 11, 2015
 * Created By: Sachin Agarwal (Appirio)
 * Updated By : Surabhi Sharma(Appirio)
 =====================================================================*/
@isTest
private class UtilityClass_Test {

    static testMethod void unitTest() {
        // TO DO: implement unit test
        /***
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.co');
      insert u;
      Test.startTest();
      System.runAs(u) {
      ******/
        Lead l = new Lead();
        l.FirstName = 'CRM Testing First';
        l.LastName = 'CRM Testing Last';
        l.Company = 'CRM Testing INCtest';
        l.description = 'Test descr';
        l.city = 'test';
        l.street = 'test';
        l.state = 'CA';
        l.PostalCode = '12345';
        l.country = 'USA';
        l.email = 'test@testnetgear.com';
        l.website = 'www.testcrm.com';
        l.County__c = 'test County';
        insert l;
        /***
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(l.id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Qualified');
        
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        ****/
        //l.Status = 'Qualified';
        //update l;
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_OEM_EU' 
                                    and SobjectType='Account' limit 1].Id;
        Account testAcct = new Account (Name = 'XXXMy Test AccountXXX');
        testAcct.BillingCity = 'xyz3';
        testAcct.BillingPostalCode = '143';
        testAcct.BillingStreet = 'tezwy3';
        testAcct.BillingState = 'test3';
        testAcct.BillingCountry = 'USA';
        testAcct.Is_Approved__c = true;
        testAcct.ShippingCity = 'city1';
        testAcct.ShippingCountry = 'USA';
        testAcct.ShippingState = 'CA';
        testAcct.ShippingStreet = '13, street2';
        testAcct.ShippingPostalCode = '123'; 
        testAcct.CTS_Global_Shipping_Address_1__c = '13';
        testAcct.CTS_Global_Shipping_Address_2__c = 'street2';        
        testAcct.County__c = 'USA';
        testAcct.recordtypeid=AirNAAcctRT_ID;
        insert testAcct;
        
        Account testAcct1 = new Account (Name = 'XXXTestXXX');
        testAcct1.BillingCity = 'xyz2';
        testAcct1.BillingPostalCode = '142';
        testAcct1.BillingStreet = 'tezwy2';
        testAcct1.BillingState = 'test2';
        testAcct1.BillingCountry = 'USA';
        testAcct1.Is_Approved__c = false;
        testAcct1.ShippingCity = 'city1';
        testAcct1.ShippingCountry = 'USA';
        testAcct1.ShippingState = 'CA';
        testAcct1.ShippingStreet = '13, street2';
        testAcct1.ShippingPostalCode = '123';
        testAcct1.CTS_Global_Shipping_Address_1__c = '13';
        testAcct1.CTS_Global_Shipping_Address_2__c = 'street2';             
        testAcct1.County__c = 'USA';
        testAcct1.recordtypeid=AirNAAcctRT_ID;
        insert testAcct1;
        
        Test.startTest();
        System.assertEquals(l.Cancel_WorflowRule__c, false);
        UtilityClass.updateLead(l.id);
        
        Lead singleLead = [SELECT Cancel_WorflowRule__c
                           FROM Lead
                           WHERE ID = :l.id];
        System.assertEquals(singleLead.Cancel_WorflowRule__c, true);        
        
        RecordType rt = [SELECT Id 
                         FROM RecordType  
                         WHERE SobjectType = 'Account' AND name = 'Parent Account'];
        
        string parentAccountRTId = UtilityClass.getObjectRecordTypeId(Account.SObjectType, 'Parent Account');
        if(rt != null){
            System.assertEquals(rt.id, parentAccountRTId);           
        }
        else{
            System.assert(null == parentAccountRTId);
        }
      
        List<Account> listOfAccounts = new List<Account>();
        listOfAccounts.add(testAcct);
        UtilityClass.deleteApprovedRecords(listOfAccounts, 'Account', UtilityClass.GENERIC_APPROVAL_API_NAME);
        
        list<Account> listAcc = new list<Account>(); 
        listAcc = [SELECT id
                   FROM Account
                   WHERE id = :testAcct.id];
        System.debug('Value is ' +listAcc.isEmpty());
        System.assert(listAcc.isEmpty() == true);
         
        UtilityClass.setOwnerFields(testAcct1.id , 'Account' , 'Delete_Approver__c');
        
        listAcc = [SELECT id, Delete_Approver__c
                   FROM Account
                   WHERE id = :testAcct1.id];
        User u1 =  [select id , managerId from User where id =: UserInfo.getUserId()  LIMIT 1];
        for(Account acc : listAcc){
        if(u1.managerId != null){
            System.assert(acc.Delete_Approver__c == u1.managerId);
        }
        else{
            System.assert(acc.Delete_Approver__c == Label.Default_Admin_User_For_Delete_Approval);
        }
        }
        Test.stopTest();         
    }
}