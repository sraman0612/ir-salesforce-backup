global class C_ReparentAIAttachments{

    @InvocableMethod
    global static void reParentAttachments(List<String> Ids){
        Case theCase = [SELECT Id, HIDDEN_Case_Number_for_AI_Record_for_IC__r.Id FROM Case WHERE Id =: Ids[0]];
        List<Attachment> currentAttachments = [SELECT Id, Name, Description, Body FROM Attachment WHERE ParentId =: theCase.HIDDEN_Case_Number_for_AI_Record_for_IC__r.Id];
        List<Attachment> newAttachments = new List<Attachment>();
        
        if(currentAttachments.size() > 0){
        
        for (Attachment att :currentAttachments)
        {
        Attachment newAttachment = att.clone();
        newAttachment.ParentId = theCase.Id;
        newAttachments.add(newAttachment);
        }
        
        insert newAttachments;
        delete currentAttachments;
        }
        
    }
    
}