/*
	Description: Apex class to add apex mixins to flows.
*/
global class FlowApexMixinsService {
	@InvocableMethod(label='Apex Mixins' description='Various Apex Methods to Assist Flow' category='Apex')
	global static List<String[]> execute(Request[] requests) {
		System.debug('Apex Mixins Requests:'+requests);
        List<String[]> results = new List<String[]>();
        if(requests[0].method == 'splitString')
        {
            results = splitString(requests);
        }
      	
        return results;
	}
    
	private static List<String[]> splitString(Request[] requests) {
		System.debug('Apex Mixins splitString Method');
        List<String[]> results = new List<String[]>();
        for(Request thisRequest: requests)
        {
            String thisTarget = thisRequest.target.deleteWhitespace();
            String[] result = thisTarget.split(';');
            results.add(result);
        }
        return results;
	}

    
    global class Request {
        @InvocableVariable(required=true)
        global String method;
    
        @InvocableVariable
        global String target;

  }
}