public without sharing class sObjectServiceBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    private class sObjectServiceBatchException extends Exception{}
    @TestVisible private static String missingParamsError = 'All parameters are required.';
    
    public Enum OperationType {
    	UPDTE, // Update,
    	INSRT // Insert
	}
        
	private OperationType opType;
	private sObject[] records;
	private String jobName;
	private Apex_Log__c[] errorsToLog = new Apex_Log__c[]{};

	// Constructor
	public sObjectServiceBatch(OperationType opType, sObject[] records, String jobName) {
		
		if (opType == null || records == null || String.isBlank(jobName)){
			throw new sObjectServiceBatchException(missingParamsError);	
		}
		
		this.opType = opType;		
		this.records = records;
		this.jobName = jobName;	
	}
	
    public sObject[] start(Database.BatchableContext bc) {
    	return records;
    }	
    
	public void	execute(Database.BatchableContext bc, List<sObject> scope) {
		
		system.debug('executing batch job: ' + jobName);
		
		try{
							
			if (opType == OperationType.UPDTE || opType == OperationType.INSRT){
				
				Database.SaveResult[] saveResults = opType == OperationType.UPDTE ? Database.Update(scope, false) : Database.Insert(scope, false);
				
				for (Integer i = 0; i < saveResults.size(); i++){
					
					Database.SaveResult result = saveResults[i];
					
					if (!result.isSuccess()){

						errorsToLog.add(new Apex_Log__c(
			        		Exception_Type__c	= 'Apex',
			        		Class_Name__c 		= 'sObjectServiceBatch (jobName: ' + jobName + ')',
			        		Method_Name__c		= 'Execute',
			        		Log_Type__c			= 'ERROR',
			        		Message__c			= 'Record [' + scope[i] + ']\nOperation Type: ' + opType.name() + '\nError: ' + result.getErrors()[0].getMessage()					
						));						
					}
				}
			}																	
    	}
		catch (Exception e){
			
    		system.debug('error: ' + e.getMessage());
    		
        	insert new Apex_Log__c(
        		Exception_Type__c	= 'Apex',
        		Class_Name__c 		= 'sObjectServiceBatch (jobName: ' + jobName + ')',
        		Method_Name__c		= 'execute:catch all',
        		Log_Type__c			= 'ERROR',
        		Message__c			= e.getMessage());
    	}		
	}    
	
	public void finish (Database.BatchableContext bc) {
		
		system.debug('errorsToLog: ' + errorsToLog.size());
		
    	if (errorsToLog.size() > 0){
    		insert errorsToLog;
    	}			
	}
}