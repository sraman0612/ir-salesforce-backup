@IsTest
public with sharing class LC01_ProductTarget_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for LC01_ProductTarget
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static Account testAcc = new Account();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_TARGET__c> lstPTDsmpTarget = new  List<PT_DSMP_TARGET__c>();
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd = new list<PT_DSMP_Strategic_Product__c>();
    static Id rtAccPowertools;

	static {

		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();

		mainUser = PT_DSMP_TestFactory.createAdminUser('LC01_ProductTarget_TEST@test.COM', 
														PT_DSMP_Constant.getProfileIdAdmin());
        insert mainUser;

    }


    @isTest static void test_runBatch_retrieveTarget(){
        System.runAs(mainUser){ 
        	Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
  
            insert lstPTDsmpYear;
            System.debug('mgr lstPTDsmpYear ' + lstPTDsmpYear);

            testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;
            System.debug('mgr lstPTDsmp ' + lstPTDsmp);

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
                lstPTDsmpStratagicProd[i].hide__c = false;
            }
            update lstPTDsmpStratagicProd;

    		LC01_ProductTarget.retrieveTarget(testAcc.Id, 
    										  String.valueOf( System.today().year() ),
    										  String.valueOf( System.today().year() - 1 ),
    										  String.valueOf( System.today().year() - 2 ));
        	Test.stopTest();
        }
    }


    @isTest static void test_runBatch_retrieveTarget_1(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
  
            insert lstPTDsmpYear;
            System.debug('mgr lstPTDsmpYear ' + lstPTDsmpYear);

            testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;
            System.debug('mgr lstPTDsmp ' + lstPTDsmp);

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
                lstPTDsmpStratagicProd[i].hide__c = false;
            }
            update lstPTDsmpStratagicProd;

            LC01_ProductTarget.retrieveTarget(testAcc.Id, 
                                              String.valueOf( System.today().year() ),
                                              String.valueOf( System.today().year() - 1 ),
                                              String.valueOf( System.today().year() - 2 ));
            Test.stopTest();
        }
    }


    @isTest static void test_runBatch_retrieveTarget_2(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
  
            insert lstPTDsmpYear;
            System.debug('mgr lstPTDsmpYear ' + lstPTDsmpYear);

            testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;
            System.debug('mgr lstPTDsmp ' + lstPTDsmp);

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
                lstPTDsmpStratagicProd[i].hide__c = false;
            }
            update lstPTDsmpStratagicProd;

            LC01_ProductTarget.retrieveTarget(testAcc.Id, 
                                              String.valueOf( System.today().year() ),
                                              String.valueOf( System.today().year() - 1 ),
                                              String.valueOf( System.today().year() - 2 ));
            Test.stopTest();
        }
    }


    @isTest static void test_runBatch_retrieveTarget_3(){
        System.runAs(mainUser){ 
            Test.startTest();

            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-2 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year()-1 ) ));
            lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() ) ));
  
            insert lstPTDsmpYear;
            System.debug('mgr lstPTDsmpYear ' + lstPTDsmpYear);

            testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;
            System.debug('mgr lstPTDsmp ' + lstPTDsmp);

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
                lstPTDsmpStratagicProd[i].hide__c = false;
            }
            update lstPTDsmpStratagicProd;

            LC01_ProductTarget.retrieveTarget(testAcc.Id, 
                                              String.valueOf( System.today().year() ),
                                              String.valueOf( System.today().year() - 1 ),
                                              String.valueOf( System.today().year() - 2 ));
            Test.stopTest();
        }
    }

    @isTest static void test_runBatch_retrieveTarget_4(){
        System.runAs(mainUser){ 
            Test.startTest();

            testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
            testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            testAcc.recordTypeId = rtAccPowertools;
            insert testAcc;

            for(Integer i=0;i<lstPTDsmpYear.size();i++){
                lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test 1', testAcc.Id, lstPTDsmpYear[i].Id));
            }
            insert lstPTDsmp;
            System.debug('mgr lstPTDsmp ' + lstPTDsmp);

            for(Integer i=0;i<lstPTDsmp.size();i++){

                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product1 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
                lstPTDsmpStratagicProd.add(PT_DSMP_TestFactory.createStrategicProduct('test product2 ' ,
                                                                                      lstPTDsmp[i].Id,
                                                                                      1,
                                                                                      1));
            }
            insert lstPTDsmpStratagicProd;

            for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
                lstPTDsmpStratagicProd[i].hide__c = false;
            }
            update lstPTDsmpStratagicProd;

            PT_DSMP_AP01_CreateYearlyRecords.createRecords1(new List<Account>{testAcc}, true);

            Test.stopTest();
        }
    }


    @isTest static void test_runBatch_saveTarget(){
        System.runAs(mainUser){ 
        	Test.startTest();
			List<LC01_ProductTarget.wrapperProductTarget> lstProductTarget = new List<LC01_ProductTarget.wrapperProductTarget>();

        	for(Integer i=0;i<lstPTDsmpStratagicProd.size();i++){
        		LC01_ProductTarget.wrapperProductTarget current = new LC01_ProductTarget.wrapperProductTarget();
				current.id = lstPTDsmpStratagicProd[i].Id;
				current.targetY = i;

				lstProductTarget.add(current);
        	}

    		LC01_ProductTarget.saveTarget( JSON.serialize(lstProductTarget) );
        	Test.stopTest();
        }
    }


}