public class MRCalculateTotalOnHoldDays {
    
    private static Id MR_CASE_MANAGEMENT_APP_RT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management_Application').getRecordTypeId();
	private static Id MR_CASE_MANAGEMENT_CUST_RT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management_Customer').getRecordTypeId();
    private static Id MR_CASE_MANAGEMENT_YZ_RT = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Milton_Roy_Case_Management_YZ').getRecordTypeId();
    private static Id MR_CASE_MANAGEMENT_PST = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('PST_Non_Warranty_Claims').getRecordTypeId();
   	private static final Decimal MSCALCULATE = 3600000.00;
    private static final Decimal ONEDAYHRS = 24.0;
    
    
    public static void calculateOnHoldDays(List<Case> newCaseList, Map<Id,Case> oldCaseMap,Map<Id,Case> newCaseMap){
        
       
        Set<Id> MRChangedToOnHoldIdSet = new Set<Id>();
        Set<Id> MRChangedFromOnHoldSet = new Set<Id>();
       
        for(Case cs : newCaseList){
            if(cs.RecordTypeId == MR_CASE_MANAGEMENT_APP_RT || cs.RecordTypeId == MR_CASE_MANAGEMENT_CUST_RT || cs.RecordTypeId == MR_CASE_MANAGEMENT_YZ_RT ||cs.RecordTypeId == MR_CASE_MANAGEMENT_PST)
            {
               if(cs.status != oldCaseMap.get(cs.Id).status && cs.status == 'On Hold')
                	MRChangedToOnHoldIdSet.add(cs.Id);
                else if(cs.status != oldCaseMap.get(cs.Id).status && oldCaseMap.get(cs.Id).status == 'On Hold')
                    MRChangedFromOnHoldSet.add(cs.Id);
             }
            
        }
        
        if(!MRChangedToOnHoldIdSet.isEmpty()){
            
            for(Id caseId : MRChangedToOnHoldIdSet){
                
                    newCaseMap.get(caseId).Last_On_Hold_Date_Time__c = System.now();
                    System.debug('last on hold date for '+ caseId +' updated to : '  + newCaseMap.get(caseId).Last_On_Hold_Date_Time__c);
            	}
      		}
        if(!MRChangedFromOnHoldSet.isEmpty()){
           
           
            Long diffMs;
            Decimal hoursDiff;
            Decimal daysDiff;
            Decimal dayHrs;
            for(Id caseId : MRChangedFromOnHoldSet){
          
              		 diffMs = 0;
                     hoursDiff = 0.0;
                     daysDiff = 0.0;
                	 dayHrs = 0.0;
                    
                        
                     System.debug('Not Null BusinessHours Id : ');
                     System.debug(newCaseMap.get(caseId).BusinessHoursId);
                     dayHrs = getOneDayHrs(newCaseMap.get(caseId).BusinessHoursId);
					 diffMs= BusinessHours.diff(newCaseMap.get(caseId).BusinessHoursId,newCaseMap.get(caseId).Last_On_Hold_Date_Time__c,System.Now()); 
                     hoursDiff = diffMs/MSCALCULATE;
                     daysDiff = hoursDiff/dayHrs;
                   
                    
                    
                    
                    System.debug('on hold duration in between ' + newCaseMap.get(caseId).Last_On_Hold_Date_Time__c + ' and ' + System.now() + ' is : ' + hoursDiff);
                    System.debug('days difference : ' + daysDiff);
                    System.debug('Hours Difference : ' + hoursDiff);
                    if(newCaseMap.get(caseId).Total_On_Hold_Time_Days__c != null && newCaseMap.get(caseId).Total_On_Hold_Time_Hours__c != null)
                    {
                        newCaseMap.get(caseId).Total_On_Hold_Time_Days__c = newCaseMap.get(caseId).Total_On_Hold_Time_Days__c + daysDiff.setScale(2);
                        newCaseMap.get(caseId).Total_On_Hold_Time_Hours__c = newCaseMap.get(caseId).Total_On_Hold_Time_Hours__c + hoursDiff.setScale(2);
                    }else{
                        newCaseMap.get(caseId).Total_On_Hold_Time_Days__c = daysDiff.setScale(2);
                        newCaseMap.get(caseId).Total_On_Hold_Time_Hours__c = daysDiff.setScale(2);
                    }  
                  }
        	}
      
        }
            
        
    //function is to calculate business hours in one day.
    public static Double getOneDayHrs(String businessHoursId){
        				
       	datetime date1 = datetime.newInstanceGmt(2024,11,19,00,00,00);
		datetime date2 = datetime.newInstanceGmt(2024,11,20,00,00,00);
        Double diffMs= BusinessHours.diff(businessHoursId,date1,date2);
        Double hoursDiff = diffMs/3600000.00;
        System.debug('1 business day = ' + hoursDiff);
        return hoursDiff;
    }
   

}