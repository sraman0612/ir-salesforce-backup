@isTest
public class CTS_NCP_EmailMessage_TrigerhandlerTest {
    // Method to test account trigger
    static testMethod void testreopenCase() {    
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
        Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        settings.Email_Message_Trigger_Enabled__c = true;
        insert settings;  
        Contact ct1 = new Contact();
        ct1.FirstName = 'Jim';
        ct1.LastName = 'Test1';
        ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Mr.';
        ct1.Phone = '123456788';
        ct1.RecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'Customer_Contact'].Id;         
        insert new Contact[]{ct1};      
        Case case1 = TestDataUtility.createCase(true,caseRecordTypeId,ct1.Id, null, 'Test 1', 'New');
        case1.CTS_User_Department__c = 'CTS OM EU';
        case1.Status = 'Closed';
        update case1;
        Test.startTest();
        {
            EmailMessage email1 = TestDataUtility.createEmailMessages(true, case1.Id, 'abc@123.com', 'support@abc.com', 'First Email1', 'Message body', true);
        }
        Test.stopTest(); 
    }
}