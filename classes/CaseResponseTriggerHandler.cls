/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 08-22-2023
 * @last modified by  : RISHAB GOYAL
 * trying git Change 4 to see Selective Deploy 
**/
public class CaseResponseTriggerHandler {

    private static Set<Id> europeQueueIds = new Set<Id>{Label.TriContinent_EMEIA, Label.Welch_EMEIA};
    
    public static void copyOnHoldReasonToSubStatus(List<Case> cases) {
        
        for (Case myCase : cases) {
            
            if (myCase.Status == 'On Hold' && myCase.Product_Category__c == 'Medical') {
                myCase.Sub_Status__c = myCase.On_Hold_Reason__c;
            }
        }
    }
    
   /* private static string emailLocation(Case myCase, Map<Id, String> brandNames) {
        
        Id ownerId = Id.valueOf(myCase.OwnerId);
        
        if (ownerId == Id.valueOf(Label.Thomas)) {
            return 'Customer Service | '+ brandNames.get(myCase.OwnerId) +'<br/>'+
                'Ingersoll Rand Medical<br/>'+
                '4601 Central Ave<br/>'+
                'Monroe, LA 71203<br/>'+
                'Office: 920-457-4891 | Fax: 920-451-8553';
        } 
        else if (ownerId == Id.valueOf(Label.TriContinent)) {
            return 'Customer Service | '+ brandNames.get(myCase.OwnerId) +'<br/>'+
                'Ingersoll Rand Medical<br/>'+
                '4601 Central Ave<br/>'+
                'Monroe, LA 71203<br/>'+
                'Office: 530-273-8888 | Fax: 920-451-8553';
        } 
        else if (ownerId == Id.valueOf(Label.TriContinent_EMEIA)) {
            return 'Customer Service | '+ brandNames.get(myCase.OwnerId) +'<br/>'+
                'Karatasstraße 4<br/>'+
                '87700 Memmingen<br/>'+
                'Germany';
        } 
        else if (ownerId == Id.valueOf(Label.Welch_EMEIA)) {
            return 'Customer Service | '+ brandNames.get(myCase.OwnerId) +'<br/>'+
                'Am Vogelherd 20<br/>'+
                '98693 Ilmenau<br/>'+
                'Germany<br/>' + 
                '+49 3677 604 0<br/>' + 
                '+49 3677 604 110<br/>' + 
                '<a href="www.welchvacuum.com">www.welchvacuum.com</a>';
        } 
        else {
            return 'Customer Service | '+ brandNames.get(myCase.OwnerId) +'<br/>'+
                'Ingersoll Rand Medical<br/>'+
                '1601 Feehanville Drive, Suite 550<br/>'+
                'Mount Prospect, IL  60056<br/>'+
                'Office: 847-676-8800';
        }
    }
    
    private static void customizeMessage(Messaging.SingleEmailMessage message, Case myCase, Map<Id, String> brandNames) {
        message.setSubject(brandNames.get(myCase.OwnerId) + ' Case ' + myCase.CaseNumber + ' : ' + mycase.Subject);
        message.setHtmlBody(
            '<html><body>' +
            'Dear Valued Customer,'+
            '<br/><br/>'+
            'Thank you for contacting '+ brandNames.get(myCase.OwnerId) +'. I have created '+ 
            'a case for your request and will be in contact with you as soon as possible. '+
            'Your case number is ' + myCase.CaseNumber + '. Please reference this case number any time when '+
            'contacting us regarding this particular inquiry.'+
            '<br/><br/>'+
            'Each case is addressed in the order it is received and we do our best to '+
            'achieve a 24-hour response time, if not sooner. If you wish to follow up on the '+
            'status of this case, please respond to this email by selecting "Reply" or "Reply '+
            'All" and reference this case number. Referencing the subject case number will '+
            'ensure all case history records are maintained and foster the quickest resolution time.'+
            '<br/><br/>'+
            'Thank you,'+
            '<br/><br/>'+
            emailLocation(myCase, brandNames)+
            '</body></html>'
        );
    }
    
    public static void reply(List<Case> cases) {
		//2023-10-06 BSJ - This is not currently firing due to changes made to when Product_Category__c gets set to medical.
		//Confirming if this is needed at all
		//Steps to fix if it is needed - Change to after trigger. Only fire when Department changes to Medical
        List<OrgWideEmailAddress> adds = [select Id,DisplayName, Address from OrgWideEmailAddress];
        Map<String, OrgWideEmailAddress> adds2 = new Map<String, OrgWideEmailAddress>();
        
        for(OrgWideEmailAddress add : adds) {
            if(add.DisplayName=='testAddress'){
            	adds2.put('testAddress',add);
            }else{
                adds2.put(add.Address, add);
            }
        }
        
        Map<String, OrgWideEmailAddress> adds3 = new Map<String, OrgWideEmailAddress>{
            Label.Thomas => adds2.get('thomasna.cs@gardnerdenver.com'),
            Label.TriContinent => adds2.get('liquidhandling.tcs@gardnerdenver.com'),
            Label.Welch => adds2.get('welch.na@gardnerdenver.com'),
            Label.Welch_EMEIA => adds2.get('welch.orders@irco.com'),
            Label.TriContinent_EMEIA => adds2.get('trico.orders@irco.com')
        };
                
            adds3.put('testAddress',adds2.get('testAddress'));
            
        List<Group> queues = [SELECT Id, Name FROM Group WHERE type='Queue'];
        Map<Id, String> brandNames = new Map<Id, String>();
        
        for (Group g : queues) {

            if (Id.valueOf(Label.Thomas) == g.Id) {
                brandNames.put(g.Id, 'Thomas');
            } 
            else if (Id.valueOf(Label.TriContinent) == g.Id || Id.valueOf(Label.TriContinent_EMEIA) == g.Id) {
                brandNames.put(g.Id, 'TriContinent');
            } 
            else if (Id.valueOf(Label.Welch) == g.Id || Id.valueOf(Label.Welch_EMEIA) == g.Id){
                brandNames.put(g.Id, 'Welch');
            }
        }
        Organization a_ORG = [Select Id, Name, IsSandbox from Organization];
        
        Boolean isSandbox = a_ORG.IsSandbox;
        System.debug('Rishab Test 97'+isSandbox);
        
        try{
            for (Case myCase : cases) {
                if(myCase.Product_Category__c == 'Medical'){
                    if (adds3.get(myCase.OwnerId) != null) {
                        System.debug('Rishab Test 98'+isSandbox);
                        sendEmail(myCase, adds3, brandNames,isSandbox);
                    }else if(isSandbox){
                        System.debug('Rishab Test 99'+isSandbox);
                        sendEmail(myCase, adds3, brandNames,isSandbox);
                    }
                    
                }
            }
        }catch(Exception e){
            System.debug(e.getMessage());
           	insert new Apex_Log__c(Class_Name__c = 'CaseResponseTriggerHandler',Method_Name__c = 'reply',Exception_Stack_Trace_String__c = e.getStackTraceString(),Message__c = e.getMessage());
        }
    }
    
    private static void sendEmail(Case myCase, Map<String, OrgWideEmailAddress> adds, Map<Id, String> brandNames, Boolean isSandbox) {
         
        if (myCase.SuppliedEmail != null && myCase.OwnerId != null && (adds.containsKey(myCase.OwnerId)||isSandbox)) {
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            System.debug('Rishab Test 100'+isSandbox);
            System.debug('Rishab Test 101'+adds.containsKey(myCase.OwnerId));
            System.debug('Rishab Test 102'+adds.get('testAddress').Id);
            System.debug('Rishab Test 103'+adds);
            
            if(!Test.isRunningTest()&&!isSandbox){
                message.setOrgWideEmailAddressId((adds.get(myCase.OwnerId)==null && isSandbox)? adds.get('testAddress').Id:adds.get(myCase.OwnerId).Id);
                message.setReplyTo((adds.get(myCase.OwnerId)==null && isSandbox) ? adds.get('testAddress').Address: adds.get(myCase.OwnerId).Address);
            }

            customizeMessage(message, myCase, brandNames);
            message.setUseSignature(false); 
            message.setBccSender(false);
            message.toAddresses = new String[] { myCase.SuppliedEmail };
            message.setSaveAsActivity(true);    
            Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { message });   
        }
    }*/
    
  /*  public static void calculateBusinessHours(List<Case> cases) {
        
        BusinessHours stdBH = [SELECT Id, Name FROM BusinessHours WHERE Name='Standard Business Hours'];
        BusinessHours eurBH = [SELECT Id, Name FROM BusinessHours WHERE Name='Europe'];
        
        for (Case myCase : cases) {

            BusinessHours bhToUse = europeQueueIds.contains(myCase.OwnerId) ? eurBH : stdBH;
            myCase.Business_Time__c = BusinessHours.nextStartDate(bhToUse.id, System.now());
            myCase.EightHourSLA__c = BusinessHours.add(bhToUse.id, System.now(), 1000 * 60 * 60 * 8);
        }
    }*/
}