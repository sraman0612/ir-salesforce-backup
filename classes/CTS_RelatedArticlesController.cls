public class CTS_RelatedArticlesController {
     
    @AuraEnabled 
    public static List<KnowledgeArticleVersion> getRelatedArticles(Id knowledgeId){
        
        List<Knowledge__kav> knowledgeList = [select id, title, urlName, (SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections) from Knowledge__kav where id = : knowledgeId];
        String relatedArticleQueryFilter = '';
        if(!knowledgeList.isEmpty()){
           String grpName;
           String category;
           for(Knowledge__DataCategorySelection dc : knowledgeList.get(0).DataCategorySelections){
                        grpName = dc.DataCategoryGroupName + '__c';
                        category = dc.DataCategoryName + '__c';
           }
           if(grpName != null && category != null){
                String soqlQuery = 'Select Id, KnowledgeArticleId,summary, Title, urlName from KnowledgeArticleVersion where  PublishStatus = \'Online\' WITH DATA CATEGORY ' + grpName + ' AT ' + category  + ' LIMIT 5';
                return (List<KnowledgeArticleVersion>)Database.query(soqlQuery);
            }
            return new List<KnowledgeArticleVersion>();
            
           /* String[] urlSplit = knowledgeList[0].urlName.split('-');
            for(String s : urlSplit){
                if(s.length() > 2){
                    relatedArticleQueryFilter = ' Title like \'% '+ s + '%\' OR '; 
                }
            }
            if(relatedArticleQueryFilter.indexOf('OR') > 0){
                relatedArticleQueryFilter = relatedArticleQueryFilter.trim().subString(0, relatedArticleQueryFilter.lastIndexOf('OR') - 1);
            }
            
            String knowledgQuery = 'SELECT id, title, urlName FROM Knowledge__kav WHERE ' + relatedArticleQueryFilter;
            System.debug(':knowledgQuery'+knowledgQuery);
            */
            
            
        }
        return new List<KnowledgeArticleVersion>();
    }
}