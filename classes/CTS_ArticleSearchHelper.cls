/***********************************************
 * CTS_ArticleSearchHelper
 * Helper class of Knowledge Article search 
 **********************************************/
public class CTS_ArticleSearchHelper {

    private static User usr = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
    private static String description = usr.Contact.AccountId != null ? [SELECT Description FROM Account where Id = :usr.Contact.AccountId].Description : '';

    /**
     * build Knowledge Article Query
     * @param fieldsForQuery, filters
     * @return String 
     */
    public static String buildKnowledgArticleQuery(List<String> fieldsForQuery,
                                                List<CTS_Article_Search_Filter_Config__mdt> filters,
                                                String selectedProduct, String selectedCategory,
                                                String lang, String queryType, String orderByField,
                                                String primaryWhere, String type) {

        
		CTS_Article_Search_Config__c searchConfigSetting = CTS_ArticleSearchConfig.getSearchConfig();

		String objectName = 'Knowledge__kav';
        String kbFields = 'Id';
        for (String fd: fieldsForQuery) {
            kbFields += ',' + fd;
        }
        if (String.isBlank(orderByField)) orderByField = 'Title';
        if (String.isBlank(queryType)) queryType = 'SOSL';

        String kbFilter = buildFilters(filters, lang, primaryWhere, type);
        String kbQuery = '';
        if (queryType == 'SOSL') kbQuery += objectName + ' (' + kbFields + (!String.isBlank(kbFilter) ? (' WHERE ' + kbFilter) : '');
        else if (queryType == 'SOQL') kbQuery += 'SELECT ' + kbFields + ' FROM ' + objectName + (!String.isBlank(kbFilter) ? (' WHERE ' + kbFilter) : '');
        
		if(orderByField == 'LastModifiedDate') kbQuery += ' AND LastModifiedDate = LAST_N_DAYS:' + searchConfigSetting.Recent_Days_Count__c.intValue();
		
		if (queryType == 'SOSL') kbQuery += ' LIMIT ' + searchConfigSetting.Query_Limit__c.intValue();
        if (queryType == 'SOSL') kbQuery += ')';
        String dataCatFilter = '';
                                                    
        if (!String.isBlank(selectedProduct) || !String.isBlank(selectedCategory)) {
            kbQuery += ' WITH DATA CATEGORY ';
            if (!String.isBlank(selectedProduct)) dataCatFilter += ' IR_Global_Products_Group__c BELOW ' + selectedProduct + '__c';
            if (!String.isBlank(selectedCategory) && !String.isBlank(dataCatFilter)) dataCatFilter += ' AND ';
            if (!String.isBlank(selectedCategory)) dataCatFilter += ' IR_Global_Category_Group__c BELOW ' + selectedCategory + '__c';
        }
        kbQuery += dataCatFilter;
		if (queryType == 'SOQL') kbQuery += ' LIMIT ' + searchConfigSetting.Query_Limit__c.intValue();
        System.debug('kbQuery**' + kbQuery);
        return kbQuery;
    }
    /**
     * build Knowledge Article Query
     * @param fieldsForQuery, filters
     * @return String 
     */
    public static String buildKnowledgArticleQuery(List <String>
        fieldsForQuery,
        List <CTS_Article_Search_Filter_Config__mdt> filters,
        String selectedProduct, String selectedCategory,
        String lang, String queryType, String type) {

        return buildKnowledgArticleQuery(fieldsForQuery, filters,
            selectedProduct, selectedCategory, lang, queryType, null, null, type);
    }

    /**
     * build filter for query
     * @param filters
     * @return String 
     */
    public static String buildFilters(List <CTS_Article_Search_Filter_Config__mdt> filters, String lang, String primaryWhere, String type) {

        Boolean isAnd = false;
        String filter = '';
		//A direct where clause
        if (primaryWhere != null) {
            if (isAnd) {
                filter += ' AND ';
            } else {
                isAND = true;
            }
            filter += ' ' + primaryWhere + ' ';
        }
		//Dynamic filters from Custom Metadata CTS_Article_Search_Filter_Config__mdt
        for (CTS_Article_Search_Filter_Config__mdt sfilter: filters) {
            if(!sfilter.Active__c) continue;
            if (sfilter.filter_Type__c == 'Auto') {                

                String profileField = sfilter.Filter_Customer_Profile_Field__c;

                // Skip SLA filter if the account does not have a description/SLA specified
                if(profileField == 'Contact.Account.Description' && String.isBlank(description)){
                    continue;
                }

                String val = getValue(sfilter.Data_Type__c, sfilter.filter_Values__c, profileField);
                if(val != 'null' && val != '' && val != null) {
                    if (isAnd) {
                        filter += ' AND ';
                    } else {
                        isAND = true;
                    }
                    filter += ' ' + sfilter.Matching_Field__c + getOperator(sfilter.Operator__c) + val;
                }
            }
        }
        //Language filter. Could be Auto or user selected        
        if(!String.isBlank(lang)) {
        	if (isAnd) {
                filter += ' AND ';
            } else {
                isAND = true;
            }
            filter += ' Language = \'' + lang + '\'';
        }
        
        //If Type was selected
        if(!String.isBlank(type)) {
        	if (isAnd) {
                filter += ' AND ';
            } else {
                isAND = true;
            }
            filter += ' CTS_TechDirect_Article_Type__c = \'' + type + '\'';
        }
        
        return filter;
    }
    /**
     * get operator for filter 
     * @param optStr
     * @return String 
     */
    @TestVisible
    private static String getOperator(String optStr) {
        if (optStr == CTS_ArticleSearchConstants.EQUALS) {
            return '=';
        } else if (optStr == CTS_ArticleSearchConstants.NOT_EQUALS) {
            return '!=';
        } else if (optStr == CTS_ArticleSearchConstants.CONTAINS) {
            return ' like ';
        } else if (optStr == CTS_ArticleSearchConstants.NOT_CONTAINS) {
            return 'not like';
        } else if (optStr == 'Includes') {
            return ' INCLUDES ';
        } 
        return '=';
    }
    /**
     * get Value for filter
     * @param dataType, value
     * @return String 
     */
    @TestVisible
    private static String getValue(String dataType, String value, String profileField) {
        if (profileField != null) {
            if (profileField == 'LanguageLocaleKey') value = UserInfo.getLanguage();
            else if(profileField == 'Contact.Account.Description') {
                
                List<String> accDesc = null;
                if(usr.ContactId != null) {
                                
                    if(String.isNotBlank(description)) {
                        accDesc = description.split(',');
                        value = ' (\'' + String.join(accDesc, '\',\'') +'\') '; // TO PREVENT NULL POINTER
                    } else {
                        value = ' (\'\') ';
                    }
                }
                
            }
        }
        if (dataType == 'String') {
            return '\'' + value + '\'';
        } else {
            return value;
        }

    }
}