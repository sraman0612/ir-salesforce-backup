// 
// (c) 2015 Appirio, Inc.
//
//Generate an Extension Class for VF Page
//
// 01 Apr 2015  Rahul Arawal    

// Utility class used for common purpose for this object

global class UtilityClass{ 
    //Generic API name to quickly plug and play Delete Approver Workflow
    public static final string GENERIC_APPROVAL_API_NAME = 'Is_Approved__c';

    // Webservice method for disable time based workflow during lead conversion
    webService static void updateLead(String leadId) { 
        Lead obj = new Lead(Id = leadId, Cancel_WorflowRule__c = true); 
        update obj;
    }
    
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName)
    {
        //Generate a map of tokens for all the Record Types for the desired object
        map<String,Schema.RecordTypeInfo> recordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosByName();
        system.debug(recordTypeInfo);
        if(!recordTypeInfo.containsKey(recordTypeName))
            return null;

        //Retrieve the record type id by name
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }
    
    public static void deleteApprovedRecords(List<sObject> recs, String table, String approvalField)
    {
        Set<Id> setToDel = new Set<Id>();
        for (sObject o :recs)
        {
            if ((boolean) o.get(approvalField))
                setToDel.add(o.Id);
        }
        if (setToDel.size() > 0)
        {
            sObject[] recsToDel = Database.Query('SELECT Id FROM ' + String.escapeSingleQuotes(table) + ' WHERE Id IN :setToDel');
            delete recsToDel;
        }
    }
    
    webservice static void setOwnerFields(Id recordId , String table , String DeleteApproverField)
    {
        System.debug('function to  test user  record');
        string tableName = String.escapeSingleQuotes(table);
        string initialQueryPart = 'SELECT Id, OwnerId, ';
        
        // for custom objects
        if(tableName.endsWith('__c')){
            initialQueryPart = 'SELECT Id, CreatedById, ';
        }
        
        sObject recordLookup = Database.Query(initialQueryPart + DeleteApproverField + ' FROM ' + tableName  + ' WHERE Id = :recordId');
        
        User u =  [select id , managerId from User where id =: UserInfo.getUserId()  LIMIT 1];
        System.debug('the value: ' + DeleteApproverField );
        if(u.managerId != null) {
            recordLookup.put(DeleteApproverField , u.managerId);
        }
        else{
            recordLookup.put(DeleteApproverField , Label.Default_Admin_User_For_Delete_Approval);
        }
        
         update recordLookup;


       
    }    
    
}