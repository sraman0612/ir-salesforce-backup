/**
 * Test class of CTS_IOT_MyProfileCoverPageController
 **/
@isTest
private class CTS_IOT_MyProfileCoverPageControllerTest {
    
    @testSetup
    static void setup(){
        User accOwner = CTS_TestUtility.createUser(false);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        accOwner.UserRoleId = portalRole.Id;
        Database.insert(accOwner);
        system.runAs(accOwner){
            Account acc = CTS_TestUtility.createAccount('Test Account', false);
            acc.Siebel_ID__c = '1234';
            acc.OwnerId = accOwner.Id;
            database.insert(acc) ;
            Contact con = CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false);
            con.CTS_IOT_Community_Status__c = 'Approved';
            Database.insert(con);
            CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
            Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
            User communityUser = CTS_TestUtility.createUser(con.Id, profileId, false);
            Database.insert(communityUser);
        }
    }
    
    @isTest
    static void testGetInit(){

        List<User> users = [SELECT id FROM User WHERE Contact.lastName = 'Contact123' LIMIT 1];

        if(!users.isEmpty()){

            System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_My_Profile_Cover_PageController.InitResponse response = CTS_IOT_My_Profile_Cover_PageController.getInit();
                System.assert(response != null);
                Test.stopTest();
           }
        }
    }
    
    @isTest
    static void testException(){

        List<User> users = [SELECT id FROM User WHERE Contact.lastName = 'Contact123' LIMIT 1];

        if(!users.isEmpty()){

            System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_My_Profile_Cover_PageController.forceError = true;
                CTS_IOT_My_Profile_Cover_PageController.InitResponse response = CTS_IOT_My_Profile_Cover_PageController.getInit();
                Test.stopTest();
           }
        }
    }
}