/**
 * Created by wenboliu on 25/10/2024.
 */

@isTest
public class B2BSelfRegistrationControllerTest {
    public static B2BSelfRegistrationController.RegistrationWrapper createTestRegistrationWrapper() {
        B2BSelfRegistrationController.RegistrationWrapper regData = new B2BSelfRegistrationController.RegistrationWrapper();
        regData.accountName = 'Test Account';
        regData.accountVATNumber = '123456789';
        regData.accountFiscalCode = 'FISC123';
        regData.accountSDINumber = 'SDI12345';
        regData.accountInvoiceEmail = 'test@invoice.com';
        regData.accountPECEmail = 'pec@test.com';
        regData.billingStreetAddress = '123 Billing St';
        regData.billingCity = 'Test City';
        regData.billingState = 'AG';
        regData.billingPostalCode = '12345';
        regData.billingCountry = 'IT';
        regData.shippingStreetAddress = '456 Shipping Ave';
        regData.shippingCity = 'Ship City';
        regData.shippingState = 'AG';
        regData.shippingPostalCode = '67890';
        regData.shippingCountry = 'IT';
        regData.contactPointAddressShippingState = 'AG';
        regData.contactFirstName = 'John';
        regData.contactLastName = 'Doe';
        regData.contactEmail = 'john.doe@test.com';
        regData.contactPhoneNumber = '123456789';
        regData.username = 'testuser@example.com';
        regData.password = 'P@ssw0rd!';
        regData.passwordConfirmation = 'P@ssw0rd!';
        regData.accountOwnerId = UserInfo.getUserId();

        return regData;
    }


    @isTest
    static void testRegisterUser() {
        User usr = [SELECT Id from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        B2BSelfRegistrationController.RegistrationWrapper regData = createTestRegistrationWrapper();
        String pageUrl;
        System.runAs(usr) {
            Test.startTest();
            pageUrl = B2BSelfRegistrationController.registerUser(regData);
            Test.stopTest();
        }
        // Verify the URL returned by registerUser is not null
        System.assertNotEquals(null, pageUrl, 'Page URL should not be null after user registration.');
        // Query and validate Account data
        Account acct = [SELECT Id, Name, VAT_ID__c FROM Account WHERE Name = :regData.accountName LIMIT 1];
        System.assertEquals(regData.accountName, acct.Name, 'Account Name should match the provided registration data.');
        System.assertEquals(regData.accountVATNumber, acct.VAT_ID__c, 'Account VAT ID should match.');

        // Query and validate ContactPointAddress data
        ContactPointAddress cpa = [SELECT Id, ParentId, AddressType FROM ContactPointAddress WHERE ParentId = :acct.Id LIMIT 1];
        System.assertEquals('Shipping', cpa.AddressType, 'ContactPointAddress type should be Shipping.');

        // Query and validate BuyerAccount data
        BuyerAccount buyerAcct = [SELECT Id, BuyerId FROM BuyerAccount WHERE BuyerId = :acct.Id LIMIT 1];
        System.assertEquals(acct.Id, buyerAcct.BuyerId, 'BuyerAccount should be linked to the created Account.');

        // Query and validate Contact data
        Contact contact = [SELECT Id, Email, AccountId FROM Contact WHERE Email = :regData.contactEmail LIMIT 1];
        System.assertEquals(acct.Id, contact.AccountId, 'Contact should be linked to the created Account.');
    }

    @isTest
    static void testValidatePassword() {
        User usr = [SELECT Id from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        B2BSelfRegistrationController.RegistrationWrapper regData = createTestRegistrationWrapper();
        System.runAs(usr) {
            // Call validatePassword - expect no exceptions with valid data
            Test.startTest();
            try {
                B2BSelfRegistrationController.validatePassword(regData);
                System.assert(true, 'Password validation passed with matching passwords.');
            } catch (Exception ex) {
                System.assert(false, 'Password validation should not throw an exception with matching passwords.');
            }
            Test.stopTest();
        }

    }

    @isTest
    static void testLoginUser() {
        User usr = [SELECT Id, userName from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        System.runAs(usr) {
            // Call the @TestVisible loginUser method with the test user's credentials
            String pageUrl = B2BSelfRegistrationController.loginUser('testUsername', 'P@ssw0rd!', 'cartId');

            // Assertions to verify the login process returned a valid URL
            System.assertNotEquals(null, pageUrl, 'The page URL should not be null after a successful login.');
            System.assert(pageUrl.contains(Site.getBaseSecureUrl()), 'The returned URL should be the secure site URL.');
        }
    }

    @isTest
    static void testGenerateCommunityNickname() {
        String username = 'testuser@example.com';
        User usr = [SELECT Id from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        String generatedNickname;
        System.runAs(usr) {
            Test.startTest();
            generatedNickname = B2BSelfRegistrationController.generateCommunityNickname(username);
            Test.stopTest();
        }

        System.assertNotEquals(null, generatedNickname, 'Generated nickname should not be null.');
        System.assertNotEquals('', generatedNickname, 'Generated nickname should not be empty.');

        System.assert(generatedNickname.length() <= 40, 'Generated nickname should not exceed 40 characters.');

        Long currentTimeMillis = System.now().getTime();
        System.assert(generatedNickname.contains(String.valueOf(currentTimeMillis).substring(0, 6)),
                'Generated nickname should contain a timestamp to ensure uniqueness.');

        System.assert(generatedNickname.contains(username), 'Generated nickname should include the username portion if within length constraints.');
    }
    // Test for getCountryAndSateList method
    @IsTest
    static void testGetCountryAndStateList() {
        User usr = [SELECT Id from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        List<B2B_Country__mdt> countries = new List<B2B_Country__mdt>();

        System.runAs(usr) {
            Test.startTest();
            countries = B2BSelfRegistrationController.getCountryAndSateList();
            Test.stopTest();
        }

        System.assertNotEquals(0, countries.size(), 'Country list should not be empty.');
        System.assert(countries[0].B2B_States__r != null, 'State records should be associated with the country.');
    }

}