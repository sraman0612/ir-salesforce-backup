// 
// (c) 2015 Appirio, Inc.
//
// Handler Class for Task trigger
//
// Apr 05, 2015      Surabhi Sharma        Original: T-375998
// Apr 30, 2015      Barkha Jain           T-382779: Added populateOpportunityStage method
// July 28, 2015     Surabhi Sharma        T-422313: Updated Task Creation and Type picklist Value
public with sharing class Task_TriggerHandler{

    public static void beforeInsert(List<Task> newList){    
        //validateTaskCreation(newList);
        populateOpportunityStage(newList);
        changeTaskSubject(newList);
    }
    
    public static void afterInsert(List<Task> newList){
        //changeTaskSubject(newList);
    }
    
    private static void changeTaskSubject(List<Task> newTasks){
        List<Task> updatedTaskList = new List<Task>();
        System.debug('------------------ Start ----------------');
        for(Task t : newTasks){
        
            if(t.WhatId != null && String.valueOf(t.WhatId).startsWith('006')){        
                String taskId = t.WhatId;
                System.debug('------------------ t.WhatId ---------------- : '+taskId);
                String opptId = taskId.substring(0,taskId.length()-3);
                System.debug('------------------ opptId ---------------- : '+opptId);
                //Opportunity opt = [select Account.Name, Name, Type from Opportunity where Id = :opptId];
                for(Opportunity opt : [select Account.Name, Name, Type from Opportunity where Id = :opptId]){
                    System.debug('------------------ opt.Type ---------------- : '+opt.Type);
                    System.debug('------------------ opt.Account ---------------- : '+opt.Account.Name);
                    System.debug('------------------ opt.Name ---------------- : '+opt.Name);
                    System.debug('------------------ t.Subject ---------------- : '+t.Subject);
                    if(opt.Type != null && opt.Type.equals('New Complete Machine')){
                        t.Subject = opt.Account.Name +'/'+ opt.Name +': '+ t.Subject;
                        System.debug('-------------- New Subject ----------: '+t.Subject);
                        updatedTaskList.add(t);
                    }else{
                        System.debug('------------------ Oppt not found or Type is not New Product Opportunity ----------------');
                    }
                }
            }
            
        }
    }
    
    // Method to auto populate Opportunity Stage field on Tasks
    private static void populateOpportunityStage(List<Task> newTasks){
        list<Task> opportunityTasks = new list<Task>();
        set<Id> oppIdSet = new set<Id>();
        for(Task task : newTasks){
            if(task.WhoId != null && String.valueOf(task.WhoId).startsWith('00Q')){
                task.Opportunity_Stage__c = 'Target';
            }else if(task.WhatId != null && String.valueOf(task.WhatId).startsWith('006')){
                opportunityTasks.add(task);
                oppIdSet.add(task.WhatId);
            }
        }
        
        map<Id, Opportunity> oppMap = new map<Id, Opportunity>([SELECT Id, StageName FROM Opportunity
                                                                WHERE  Id IN :oppIdSet]);
                                                                
        for(Task task : opportunityTasks){
            task.Opportunity_Stage__c = oppMap.get(task.WhatId).StageName;
        }
    }  
    
    //Created by CG to check inactive account on new Tasks created
    public static void checkInactiveTaskAccount(Set<ID> accIDs, List<Task> newTasks){
    
    Map<ID,Account> taskAccountMap = new Map<ID,Account>([SELECT Status__c,Id, recordtypeid FROM Account WHERE Id IN :accIDs]);
    Id accRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
     
   // List<Account> a = [SELECT Status__c,Id, recordtypeid FROM Account WHERE Id IN :accIDs];
       // for(Account acc : a){
        //    taskAccountMap.put(acc.id, acc);
       // }
        for (Task t : newTasks) {
            if(t.WhatId != null){
                if(String.valueOf(t.WhatId).startswith('001') && taskAccountMap.get(t.WhatId).status__c == 'Inactive' && taskAccountMap.get(t.WhatId).recordtypeId == accRecordTypeId){
                   t.addError('This Account is Inactive'); 
                }
   
    }
    }
    }
    
}