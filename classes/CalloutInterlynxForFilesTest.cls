@isTest
public class CalloutInterlynxForFilesTest {
	@testSetup
    static void setupTestData() {
        // Create Leads
        List<Lead> leads = new List<Lead>();
        for (Integer i = 0; i < 3; i++) {
            leads.add(new Lead(FirstName = 'Test', LastName = 'Lead' + i, Email = 'abc'+i+'@gmail.com' , Company = 'Test Company'));
        }
        insert leads;
        
        // Create ContentVersions
        List<ContentVersion> contentVersions = new List<ContentVersion>();
        for (Lead l : leads) {
            ContentVersion cv = new ContentVersion(
                Title = 'TestFile' + l.Id,
                VersionData = Blob.valueOf('Test file content for ' + l.Id),
                PathOnClient = 'TestFile' + l.Id + '.txt'
            );
            contentVersions.add(cv);
        }
        insert contentVersions;
        
        List<ContentDocument> documents = [
            SELECT Id, Title, LatestPublishedVersionId 
            FROM ContentDocument
        ];
        
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
        for (Lead l : leads) {
            for(ContentDocument contDoc : documents){
                if(contDoc.Title.contains(l.Id)){
                    //create ContentDocumentLink  record 
                    ContentDocumentLink cdl = new ContentDocumentLink();
                    cdl.LinkedEntityId = l.id;
                    cdl.ContentDocumentId = contDoc.Id;
                    cdl.ShareType = 'V';
                    contentDocumentLinks.add(cdl);
                } 
            }
        }
        insert contentDocumentLinks;
        
    }
    
    @isTest
    static void testExecuteMethod() {
        // Test data setup
        List<Lead> leads = [SELECT Id FROM Lead LIMIT 2];
        List<ContentVersion> contentVersions = [SELECT Id,Lead_Ids_ready_to_sync_with_Interlynx__c FROM ContentVersion LIMIT 2];
                
        // Mock the HTTP response for the callout
        Test.startTest();
        // Mock HTTP Callout
        HttpResponse mockResponse = new HttpResponse();
        mockResponse.setStatusCode(200);
        mockResponse.setBody('{"key1":"value1"}');
        MockCalloutInterlynxGenerator mock = new MockCalloutInterlynxGenerator(mockResponse);
        Test.setMock(HttpCalloutMock.class, mock);
		
        for(lead l : leads){
            for(ContentVersion cv:contentVersions){
                cv.Lead_Ids_ready_to_sync_with_Interlynx__c = l.Id;
			}
        }
        
        update contentVersions;
        
        ContentVersion objCntNote = new ContentVersion();
        objCntNote.Title = 'Test Content Note';
        objCntNote.PathOnClient = objCntNote.Title + '.snote';
        objCntNote.VersionData = Blob.valueOf('This is Test Content Note');
        objCntNote.FirstPublishLocationId =leads[0].Id;  // ParentId
        insert objCntNote;
        
		objCntNote.Title = 'Untitled Note';
        update objCntNote;
        Test.stopTest();

    }
    
    @isTest
    static void testExceptionHandling() {
        // Test exception handling for file upload that exceeds heap size
        List<Lead> leads = [SELECT Id FROM Lead LIMIT 1];
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion LIMIT 1];
        
        Test.startTest();
        // Mock HTTP Callout with an exception
        HttpResponse mockResponse = new HttpResponse();
        mockResponse.setStatusCode(500);
        mockResponse.setBody('{"Message":"Callout failed"}');
        MockCalloutInterlynxGenerator mock = new MockCalloutInterlynxGenerator(mockResponse);
        Test.setMock(HttpCalloutMock.class, mock);
        
        for(lead l : leads){
            for(ContentVersion cv:contentVersions){
                cv.Lead_Ids_ready_to_sync_with_Interlynx__c = l.Id;
			}
        }
        update contentVersions;
		
        Test.stopTest();
    }
    
    @isTest
    static void testResponse400Exception() {
        // Test exception handling for file upload that exceeds heap size
        List<Lead> leads = [SELECT Id FROM Lead LIMIT 1];
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion LIMIT 1];
        
        Test.startTest();
        // Mock HTTP Callout with an exception
        HttpResponse mockResponse = new HttpResponse();
        mockResponse.setStatusCode(400);
        mockResponse.setBody('{"CrmName": "Salesforce","Message": "Attachment as different type of extensions.","StatusCode": 200,"Exception": "Attachment as different type of extensions.","InterlynxFileId": 70}');
        MockCalloutInterlynxGenerator mock = new MockCalloutInterlynxGenerator(mockResponse);
        Test.setMock(HttpCalloutMock.class, mock);
        
        for(lead l : leads){
            for(ContentVersion cv:contentVersions){
                cv.Lead_Ids_ready_to_sync_with_Interlynx__c = l.Id;
			}
        }
        update contentVersions;
		
        Test.stopTest();
        
	}
    
    @isTest
    static void testResponse200Exception() {
        // Test exception handling for file upload that exceeds heap size
        List<Lead> leads = [SELECT Id FROM Lead LIMIT 1];
        List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion LIMIT 1];
        
        Test.startTest();
        // Mock HTTP Callout with an exception
        HttpResponse mockResponse = new HttpResponse();
        mockResponse.setStatusCode(200);
        mockResponse.setBody('{"CrmName": "Salesforce","Message": "Attachment as different type of extensions.","StatusCode": 200,"Exception": "Attachment as different type of extensions.","InterlynxFileId": 70}');
        MockCalloutInterlynxGenerator mock = new MockCalloutInterlynxGenerator(mockResponse);
        Test.setMock(HttpCalloutMock.class, mock);
        
        for(lead l : leads){
            for(ContentVersion cv:contentVersions){
                cv.Lead_Ids_ready_to_sync_with_Interlynx__c = l.Id;
			}
        }
        update contentVersions;
		
        CalloutInterlynxForFiles calloutInterlynx = new CalloutInterlynxForFiles(new Map<String, List<String>>());
        System.enqueueJob(calloutInterlynx);
        
        Test.stopTest();
        
	}
    
}