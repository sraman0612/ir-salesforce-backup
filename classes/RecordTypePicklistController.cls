/**
	@author Providity
	@date 28AUG19
	@description controller for RecordTypePicklist aura component
*/ 

public class RecordTypePicklistController {

    
    /**
     * Given an API object and field name, returns list of the picklist values for use in select input.
     */
    @AuraEnabled
    public static List<Object> getRecordTypes(String objectName) {
        
        List<PicklistOption> options = new List<PicklistOption>();
        List<Object> result = new List<Object>();
        Map<String,Schema.RecordTypeInfo> rtByName = Schema.getGlobalDescribe().get( objectName ).getDescribe().getRecordTypeInfosByName();            
            
        for( String rtName : rtByName.keySet()) {
            if(rtByName.size() != 1 && rtName != 'Master'){
            	options.add(new PicklistOption(rtName,rtByName.get(rtName.trim()).getRecordTypeId()));
            }
        }
        result = options;
        System.debug('result**' + result);
        return result;
    }        
    
    /**
     * The system class PicklistEntry is not aura enabled so cannot be returned from @AuraEnabled method.
     * Workaround is to define our own class with aura enabled properties.
     */
    public class PicklistOption {
        @AuraEnabled public String label { get; set; }        
        @AuraEnabled public String value { get; set; }
        
        public PicklistOption( String label, String value ) {
            this.label = label;
            this.value = value;
        }
    }
}