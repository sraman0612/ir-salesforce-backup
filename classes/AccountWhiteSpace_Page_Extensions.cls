/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the controller class for AccountWhiteSpace_Page which has various methods for the required functionality.
------------------------------------------------------------*/
public with sharing class AccountWhiteSpace_Page_Extensions {
	
	public account acct{get;set;}
	//public Map<string,Map<String,Account_Whitespace__c>> locationOfMap{get;set;}
	public string location{get;set;}
	public boolean ShowGenPage{get;set;}
	public string accountid{get;set;}
	public string deletelocation{get;set;}
	public string accountname{get;set;}

	//Getting all the offerings in account whitespace object
	/*public List<String> getOfferings()
		{
		  List<String> options = new List<String>();
		        
		   Schema.DescribeFieldResult fieldResult =
		   Account_Whitespace__c.Offering__c.getDescribe();
		   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		        
		   for( Schema.PicklistEntry f : ple)
		   {
		      options.add(f.getValue());
		   }    
		   return options;  
		}*/
	//Construtor for AccountWhiteSpace_Page_Extensions class
	public AccountWhiteSpace_Page_Extensions(ApexPages.StandardController stdCtrl)
	   {
	   		ShowGenPage=false;
		   	acct=(account)stdCtrl.getRecord();
		   	accountid=acct.id;
		   	accountname=[select name from account where id=:accountid].name;
		   	/*Integer count=[Select count() from Account_Whitespace__c where AccountName__c=:acct.id];
	   		if(count==0){
	   			ShowGenPage=false;
	   		}
	   		else{
	   			ShowGenPage=true;
		   		/*list <Account_Whitespace__c> allWhitespace=new list<Account_Whitespace__c>([Select id,Location__c,Offering__c,Potential_IR_Business__c,Existing_IR_Business__c from Account_Whitespace__c where AccountName__c=:acct.id and Location__c!='' ]);
		   		locationOfMap=new Map<string,Map<String,Account_Whitespace__c>>();
		   		for(Account_Whitespace__c accountWhiteSpace :allWhitespace){
		   			Map<String,Account_Whitespace__c> offeringOfMap=new Map<String,Account_Whitespace__c>();
		   			if(locationOfMap.containsKey(accountWhiteSpace.Location__c)){
		   				offeringOfMap=locationOfMap.get(accountWhiteSpace.Location__c);
		   			}	
		   			offeringOfMap.put(accountWhiteSpace.Offering__c,accountWhiteSpace);
		   			locationOfMap.put(accountWhiteSpace.Location__c,offeringOfMap);	   			 
		   		}	
	   		}*/																																			 																																					
	   }
	   
	   //This method is used to create new account whitespace record with the location entered
	   public void createAccountWhiteSpace() {
        
        /*list<Account_Whitespace__c> newAWS=new list<Account_Whitespace__c>();
        Schema.DescribeFieldResult fieldResult = Account_Whitespace__c.Offering__c.getDescribe();
        List<Schema.PicklistEntry> offering = fieldResult.getPicklistValues();
        string category='';
        for( Schema.PicklistEntry f : offering)
        {
            if(f.getValue()=='Air Compressors' || f.getValue()=='Treatment' || f.getValue()=='Controls')
                category='Supply';  
            else if(f.getValue()=='Piping' || f.getValue()=='Inflow' || f.getValue()=='Tanks')
                category='Network';
            else if(f.getValue()=='Parts' || f.getValue()=='Service' || f.getValue()=='Rental' || f.getValue()=='Turnkey' || f.getValue()=='Assessments' || f.getValue()=='Agreements / Contracts')
                category='Services';
            else
                category='Other';
            newAWS.add(new Account_Whitespace__c(Name=f.getValue(),Category__c=category,Offering__c=f.getValue(),AccountName__c=accountid,Existing_IR_Business__c=false,Potential_IR_Business__c=false,Location__c=location));
        }  
        
        insert newAWS; */      
    }
    
    //This method is used to take back the user to the account details page on clicking Cancel Whitespace Button
     public pagereference CancelWP() {
     	
     	return(new pagereference('/'+accountid));
     }
     
     //This method is used to Save the whitespace records on clicking Save Whitespace Button
     public pagereference SavewhiteSpace() {
     	
     	/*list<Account_Whitespace__c> updateList=new list<Account_Whitespace__c>();
     	for(string s:locationOfMap.keyset()){
     		updateList.addall(locationOfMap.get(s).values());
     	}
     	update updateList;*/
     	return(new pagereference('/'+accountid));
     }
     
     //This method is used to delete one row in location column
     public void deleteLocation() {
     	
     	//list<Account_Whitespace__c> deleteWhiteSpace=new list<Account_Whitespace__c>([Select id from Account_Whitespace__c 
     	//																	   where AccountName__c=:accountid and Location__c=:deletelocation]);
     																		   
        //delete deleteWhiteSpace;
     }
}