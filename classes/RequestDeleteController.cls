public class RequestDeleteController {
	
    static public Id id {get; set;}
    static public boolean isInApproval  {get; set;}
  

    @AuraEnabled
    static public void setDeleteRequest(Id recordId, String reason) {
        String objectName=recordId.getSObjectType().getDescribe().getName();
        UtilityClass.setOwnerFields(recordId, objectName ,'Delete_Approver__c');        
    
        
        //Sets The Delete Request Flag to true which kicks off the approval process.
          sObject record = Database.Query('Select Request_Delete__c, delete_reason__c, Id, name FROM '+String.escapeSingleQuotes(objectName)+' WHERE Id = :recordId');       
       	  record.put('Request_Delete__c', true);   
          record.put('Delete_reason__c', reason);  
          update(record);
          ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), recordId , ConnectApi.FeedElementType.FeedItem, 'Reason for the delete request: '+reason);
    	
    }
   
   	@AuraEnabled
      public static String getRecordName(Id recordId) {
          String name='null';
          isInApproval=false;
          
     System.debug('ID = '+recordId);  
          //Pulls the name off the specific object
          String sObjName = recordId.getSObjectType().getDescribe().getName();
          if(sObjName=='Account'){
               Account obj = [select id, name, Request_Delete__c from Account where id = :recordId];
              if (null!=obj) {
                  	name=obj.name;
                  isInApproval=obj.Request_Delete__c;
                  	
              }
          }else if(sObjName=='Opportunity'){
               Opportunity obj = [select id, name, Request_Delete__c from Opportunity where id = :recordId];
              if (null!=obj) {
                  	name=obj.name;
                  isInApproval=obj.Request_Delete__c;
              }
              
          }else if(sObjName=='Contact'){
               Contact obj = [select id, name, Request_Delete__c from Contact where id = :recordId];
              if (null!=obj) {
                  	name=obj.name;
                  isInApproval=obj.Request_Delete__c;
              }
          }else if(sObjName=='Lead'){
               Lead obj = [select id, name, Request_Delete__c from Lead where id = :recordId];
              if (null!=obj) {
                  	name=obj.name;
                  isInApproval=obj.Request_Delete__c;
              }
          }else{
              name='Unknown Object Name';
          }
     
      System.debug('Name = '+name);      
        return name;
    }
    
    	@AuraEnabled
      public static boolean isInDeleteRequestApproval(Id recordId) {
          Boolean isInApproval=false;
     System.debug('ID = '+recordId);  
          //Pulls the name off the specific object
          String sObjName = recordId.getSObjectType().getDescribe().getName();
          if(sObjName=='Account'){
               Account obj = [select id, request_delete__c from Account where id = :recordId];
              if (null!=obj) {
                  	isInApproval=obj.request_delete__c;
              }
          }else if(sObjName=='Opportunity'){
               Opportunity obj = [select id, request_delete__c from Opportunity where id = :recordId];
              if (null!=obj) 	isInApproval=obj.request_delete__c;
          }else if(sObjName=='Contact'){
               Contact obj = [select id, request_delete__c from Contact where id = :recordId];
              if (null!=obj) 	isInApproval=obj.request_delete__c;
          }else if(sObjName=='Lead'){
               Lead obj = [select id, request_delete__c from Lead where id = :recordId];
              if (null!=obj) 	isInApproval=obj.request_delete__c;         
          }else{
              	isInApproval=null;
          }
     
       System.debug('is in delete request = '+isInApproval);      
        return 	isInApproval;
    }
}