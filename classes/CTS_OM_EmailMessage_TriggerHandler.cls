public with sharing class CTS_OM_EmailMessage_TriggerHandler {
    
    @TestVisible private static Id acctMgmtRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
    
    private static List<String> OM_Emailcase_RecordType_Name =  new List<String>();
    //private static Map<String,string> OM_Emailcase_RecordType_Name = new Map<String,string>();
    private static Set<Id> recordTypeIds =  new Set<Id>();
    public static void beforeInsert(List<EmailMessage> emails) {
        
        system.debug('--CTS_OM_EmailMessage_TriggerHandler');

        //Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        //added by @Capgemini on 23 December 2021 
        // Email_Message_Setting__mdt settings = Email_Message_Setting__mdt.getInstance('Email_Message_Setting_For_Case');
        Email_Message_Setting__mdt  settings = [SELECT DeveloperName,CTS_OM_Email_Case_Record_Type_IDs__c,CTS_OM_Email_Case_Record_Type_IDs_1__c,CTS_OM_Email_Case_Record_Type_IDs_2__c,	CTS_OM_Email_Case_Record_Type_IDs_3__c,	CTS_OM_Email_Case_Record_Type_IDs_4__c,CTS_OM_Contact_Record_Type_IDs_to_Match__c,CTS_OM_Case_Contact_Trigger_Enabled__c,CTS_Default_Case_Owner_ID__c FROM Email_Message_Setting__mdt Where DeveloperName ='Email_Message_Setting_For_Case'];
       // String CTS_OM_Email_Case_Record_Type_IDs = settings.CTS_OM_Email_Case_Record_Type_IDs_1__c+settings.CTS_OM_Email_Case_Record_Type_IDs_2__c+settings.CTS_OM_Email_Case_Record_Type_IDs_3__c+settings.CTS_OM_Email_Case_Record_Type_IDs_4__c;
        //end
        system.debug('settings: ' + settings);
        //system.debug('%%%%record type settings%%%%'+ (settings.CTS_OM_Email_Case_Record_Type_IDs_1__c).unescapeJava()) ;
        OM_Emailcase_RecordType_Name = String.isNotBlank(settings.CTS_OM_Email_Case_Record_Type_IDs__c) ? new List<String>((List<String>)settings.CTS_OM_Email_Case_Record_Type_IDs__c.split(',')) : new List<String>();
        //  OM_Emailcase_RecordType_Name.put(settings..split(','),settings.CTS_OM_Email_Case_Record_Type_IDs_1__c.split(','));
        system.debug('****Email message OM_case_RecordType_Name****'+ OM_Emailcase_RecordType_Name);
        
        for(String rt : OM_Emailcase_RecordType_Name){
            system.debug('***recodrtype developer name****'+ Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rt).getRecordTypeId());
            //if(rt != 'Internal_Case' && rt !='Action_Item'){
            recordTypeIds.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rt).getRecordTypeId());
            // }
            
        } 
        
        System.debug('settings.>>>'+settings.CTS_OM_Email_Case_Record_Type_IDs__c);
        System.debug('settings.Split>>>'+settings.CTS_OM_Email_Case_Record_Type_IDs__c.split(','));
        //instead of this converted setid to developer name above.
        //Set<Id> recordTypeIds = settings != null && String.isNotBlank(settings.CTS_OM_Email_Case_Record_Type_IDs_1__c) ? new Set<Id>((List<Id>)settings.CTS_OM_Email_Case_Record_Type_IDs__c.split(',')) : new Set<Id>(); 
        System.debug('recordTypeIds>>>'+recordTypeIds);
        Id defaultCaseOwnerId = settings != null && String.isNotBlank(settings.CTS_Default_Case_Owner_ID__c) ? settings.CTS_Default_Case_Owner_ID__c : null;        
        
        if (!recordTypeIds.isEmpty()){
            EmailMessage_TriggerHandlerHelper.preventDuplicateEmails(emails, recordTypeIds);
            EmailMessage_TriggerHandlerHelper.createNewCaseAndReparentEmail(emails,recordTypeIds, defaultCaseOwnerId, EmailMessage_TriggerHandlerHelper.CompanyDivision.CTS_OM);
        }
    } 
}