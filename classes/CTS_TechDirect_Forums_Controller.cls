public with sharing class CTS_TechDirect_Forums_Controller {
    
    private class CTS_TechDirect_Forums_ControllerException extends Exception{}
    
    @TestVisible
    private static Boolean forceError = false;
    
    @TestVisible
    private virtual class ResponseBase{
    	public Boolean success = true;
    	public String errorMessage = '';    
    }
    
    @TestVisible
    private class Init extends ResponseBase{
    
    	public CTS_TechDirect_Community_Forums_TC__c termsForumSettings;
    	public User userRecord = new User();
    	public String splashHeading = '';
    	public String splashTerms = '';
    	public String splashInstructions = '';
    	public String splashAcceptButtonLabel = '';
    	public Boolean termsUserAccepted = false;
    	public Decimal termsUserAcceptedVersion;
    }
    
    @AuraEnabled
    public static String getInit(){
    	
    	Init init = new Init();
    	
    	try{

			if (Test.isRunningTest() && forceError){
				throw new CTS_TechDirect_Forums_ControllerException('Error test');
			}
			
    		init.termsForumSettings = CTS_TechDirect_Community_Forums_TC__c.getOrgDefaults();
    		init.userRecord = [Select Id, CTS_TechDirect_T_C_Accepted__c, CTS_TechDirect_T_C_Accepted_Version__c, CTS_TechDirect_T_C_Accepted_DateTime__c, CTS_TechDirect_T_C_Accepted_IP__c From User Where Id = :UserInfo.getUserId()];
    		init.splashHeading = Label.CTS_TechDirect_ChatterForum_Heading;
    		init.splashTerms = Label.CTS_TechDirect_ChatterForum_Terms + Label.CTS_TechDirect_ChatterForum_Terms2;
    		init.splashInstructions = Label.CTS_TechDirect_ChatterForum_Instructions;
    		init.splashAcceptButtonLabel = Label.CTS_TechDirect_ChatterForum_AcceptButton;
    	}
		catch (Exception e){
			
			system.debug('error: ' + e.getMessage());
			init.success = false;
			init.errorMessage = e.getMessage();
		}
    	
    	return JSON.serialize(init);
    }       
    
    @AuraEnabled
    public static String acceptTerms(){
    	
    	ResponseBase response = new ResponseBase();
    	
    	try{
    		
			if (Test.isRunningTest() && forceError){
				throw new CTS_TechDirect_Forums_ControllerException('Error test');
			}    		
    		
    		User userToUpdate = new User(
    			Id = UserInfo.getUserId(),
    			CTS_TechDirect_T_C_Accepted__c = true,
    			CTS_TechDirect_T_C_Accepted_Version__c = CTS_TechDirect_Community_Forums_TC__c.getOrgDefaults().Current_Version__c,
    			CTS_TechDirect_T_C_Accepted_DateTime__c = DateTime.now(),
    			CTS_TechDirect_T_C_Accepted_IP__c = [Select SourceIp, APIType, Browser, LoginTime From LoginHistory Where UserId = :UserInfo.getUserId() Order By LoginTime DESC LIMIT 1].SourceIp
    		);
			
			update userToUpdate;
    	}
		catch (Exception e){
			
			system.debug('error: ' + e.getMessage());
			response.success = false;
			response.errorMessage = e.getMessage();
		}
    	
    	return JSON.serialize(response);
    }         
}