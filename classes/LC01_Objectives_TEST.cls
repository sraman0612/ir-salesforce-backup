@IsTest
public with sharing class LC01_Objectives_TEST {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Test class for LC01_Objectives
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	static User mainUser;
	static List<Account> lstAccount = new List<Account>();
	static Account testAcc = new Account();
	static List<PT_DSMP__c> lstPTDsmp = new List<PT_DSMP__c>();
	static List<PT_DSMP_Year__c> lstPTDsmpYear = new List<PT_DSMP_Year__c>();
	static List<PT_DSMP_NPD__c> lstPTDsmpNPD = new List<PT_DSMP_NPD__c>();
	static List<PT_DSMP_Objectives__c> lstPTDsmpObjective = new List<PT_DSMP_Objectives__c>();
	static List<PT_DSMP_Shared_Objectives__c> lstPTDsmpSharedObjective = new List<PT_DSMP_Shared_Objectives__c>();
	static List<PT_DSMP_Strategic_Product__c> lstPTDsmpStratagicProd;

    static Id rtAccPowertools;

	static {

		rtAccPowertools = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();

		mainUser = PT_DSMP_TestFactory.createAdminUser('PT_DSMP_BAT01_createRecAccountDVP@test.COM', 
														PT_DSMP_Constant.getProfileIdAdmin());
        insert mainUser;

        System.runAs(mainUser){
        	testAcc = TestDataFactory.createAccount('TestAcc1','IR Comp OEM EU');
        	testAcc.PT_BO_Customer_Number__c = '12345';
            testAcc.PT_Is_DVP__c = 'DVP';
            testAcc.PT_Account_Segment__c = 'STAR';
            //testAcc.recordTypeId = rtAccPowertools;
        	insert testAcc;

        	lstPTDsmpYear.add(PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() + 1 ) ));
        	insert lstPTDsmpYear;

        	lstPTDsmp.add(PT_DSMP_TestFactory.createDSMP('## test ' ,
        												testAcc.Id,
        												lstPTDsmpYear[0].Id
        												));
        	insert lstPTDsmp;

        	lstPTDsmpSharedObjective.add(PT_DSMP_TestFactory.createDSMPSharedObjectives('## objectives Number One',
        																				'STAR', 1, lstPTDsmpYear[0].Id));
        	insert lstPTDsmpSharedObjective;

        	for(integer i=1;i<=6;i++){
        		PT_DSMP_Objectives__c c = PT_DSMP_TestFactory.createDSMPObjectives('# Objective-by this year -'+i,
        																			testAcc.Id,
        																			String.valueOf(i),
        																			lstPTDsmp[0].Id);
        		if(i==1){
        			c.PT_DSMP_Shared_Objectives__c = lstPTDsmpSharedObjective[0].Id;
        		}
        		c.Objective__c = '# Objective-by this year -'+i;
        		lstPTDsmpObjective.add(c);
        	}
        	insert lstPTDsmpObjective;

        }

	}


	@isTest static void test_runBatch_retrieveObjectives(){
        System.runAs(mainUser){ 
        	Test.startTest();
        		LC01_Objectives.retrieveObjectives(testAcc.Id, lstPTDsmp[0].id);
        	Test.stopTest();
        }
    }


    @isTest static void test_runBatch_updateObjectives(){
        System.runAs(mainUser){ 
        	Test.startTest();
        		List<PT_DSMP_Objectives__c> lstObjective = new List<PT_DSMP_Objectives__c>();
        		List<PT_DSMP_Objectives__c> lstSuggested = new List<PT_DSMP_Objectives__c>();

        		for(Integer i=0;i<lstPTDsmpObjective.size();i++){
        			if(i<3)
        				lstObjective.add(lstPTDsmpObjective[i]);
        			else 
        				lstSuggested.add(lstPTDsmpObjective[i]);

        		}

        		LC01_Objectives.updateObjectives(lstObjective, lstSuggested);
        	Test.stopTest();
        }
    }


    @isTest static void test_runBatch_updateObjectives_Exception(){
        System.runAs(mainUser){ 
        	Test.startTest();
        		List<PT_DSMP_Objectives__c> lstObjective = new List<PT_DSMP_Objectives__c>();
        		List<PT_DSMP_Objectives__c> lstSuggested = new List<PT_DSMP_Objectives__c>();

        		for(Integer i=0;i<lstPTDsmpObjective.size();i++){
        			if(i<3){
        				lstPTDsmpObjective[i].Objective__c = 'test';
        				lstObjective.add(lstPTDsmpObjective[i]);
        			}
        			else 
        				lstSuggested.add(lstPTDsmpObjective[i]);

        		}

        		LC01_Objectives.updateObjectives(lstSuggested, lstObjective);
        	Test.stopTest();
        }
    }


}