@isTest
private class PT_EventTriggerHandlerTest {
  
  static testMethod void testBlockEventOnKeyAcct(){
    Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
    Id ptEventRTID = [SELECT Id FROM RecordType WHERE sObjectType='Event' and DeveloperName='PT_Powertools'].Id;
    Account a = new Account(RecordTypeId = ptAcctRTID,Name = 'Key Account Test',Type = 'Customer',PT_Status__c = 'New',PT_IR_Territory__c = 'North',PT_IR_Region__c = 'EMEA');
    insert a;
    Event e = new Event(
      WhatId=a.Id,
      RecordTypeId=ptEventRTID,
      Subject='Test Block Event', 
      StartDateTime=system.now(), 
      EndDateTime=system.now()+1, 
      Description='Test Block Event Desc', 
      Type='1. PLAN account actions and objectives');
    try {
      insert e;
    } catch(Exception error) {
      System.Assert(error.getMessage().contains('Events cannot be created against Key Accounts.'));
    }
  }
  
  static testMethod void testEventPlanDateRollup(){
    Id PTEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    Id PTLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('PT_Lead').getRecordTypeId();
    lead l = new Lead(RecordTypeId=PTLeadRecordTypeId, FirstName='PTLead', LastName='Test', Company='EYE_R_SEE_OH', Country='Belgium');
    insert l;
    system.assertEquals(null, [SELECT PT_Event_Planned_Date__c FROM Lead WHERE Id = :l.Id].PT_Event_Planned_Date__c);
    Event e = new Event(WhoId=l.Id,RecordTypeId=PTEventRecordTypeId,Subject='Test PT Event',StartDateTime=system.now(),EndDateTime=system.now()+1,Description='Test Block Event Desc',Type='1. PLAN account actions and objectives');
    insert e;
    system.assertEquals(e.StartDateTime.Date(), [SELECT PT_Event_Planned_Date__c FROM Lead WHERE Id = :l.Id].PT_Event_Planned_Date__c);
  }
  
    static testMethod void testEventPlanDateRollupNoLead(){
    Id PTEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    Id PTLeadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('PT_Lead').getRecordTypeId();
    Event e = new Event(RecordTypeId=PTEventRecordTypeId,Subject='Test PT Event',StartDateTime=system.now(),EndDateTime=system.now()+1,Description='Test Block Event Desc',Type='1. PLAN account actions and objectives');
    insert e;
  }
    
    public static testMethod void testLinkOutlookEvent(){
        Id PTEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
		PT_Outlook_Event__c outlookEvent = new PT_Outlook_Event__c();
        outlookEvent.Subject__c = 'Test PT Event';
        outlookEvent.Start_Date_Time__c = DateTime.newInstance(2020, 1, 31, 7, 0, 0);
        outlookEvent.End_Date_Time__c = DateTime.newInstance(2020, 1, 31, 7, 30, 0);
        insert outlookEvent;
        
        Event e = new Event(
            RecordTypeId=PTEventRecordTypeId,
            Subject='Test PT Event',
            StartDateTime=DateTime.newInstance(2020, 1, 31, 7, 0, 0),
            EndDateTime=DateTime.newInstance(2020, 1, 31, 7, 30, 0),
            Description='Test Block Event Desc',
            Type='1. PLAN account actions and objectives'
       	);
        insert e;
        
        outlookEvent = [Select id, Event_Id__c from PT_Outlook_Event__c where Id =:outlookEvent.Id];
        
        //System.assert(!String.isEmpty(outlookEvent.Event_Id__c), 'Outlook Event should have event id if linked');
    }
}