// (c) 2015, Appirio Inc.
//
// Handler Class for Asset_Trigger
//
// August27,2015    Surabhi Sharma    T-429707

public class Asset_TriggerHandler
{  
    public static void beforeInsert(List<Asset> newList)
    {
        populateAssetDistrict(newList);
    }
    
    public static void afterUpdate(List<Asset> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    public static void afterInsert(List<Asset> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    public static void afterDelete(List<Asset> newList)
    {
        UpdateAccountAttributes(newList);
    }
    
    //method to set Asset district according to custom setting mapping (District_Division__c)
    public static void populateAssetDistrict(List<Asset> newList)
    {
    
        Map<Id, List<Asset>> mapAccListAsset = new Map<Id, List<Asset>>();
        Map<Id, String> accWithDiv = new Map<Id, String>();
        map<String, String> mapDivisionToDistrict =  new  map<String, String>();
        map<Id, String> mapAccIdToDivision = new map<Id, String>();
        
        for(District_Division__c disDiv : [SELECT Id, division__c, district__c
                                            FROM District_Division__c]){
                 mapDivisionToDistrict.put(disDiv.division__c, disDiv.district__c);                             
        }
        
        for(Asset asset : newList) {
            if(!mapAccListAsset.containsKey(asset.AccountId)) {
                mapAccListAsset.put(asset.AccountId, new List<Asset>());
            }
            mapAccListAsset.get(asset.AccountId).add(asset);
        }
        
        for(Account acc : [Select Id, division__c 
                            FROM Account
                            Where Id IN : mapAccListAsset.keySet()]) {
            mapAccIdToDivision.put(acc.Id, acc.division__c);
        }
        
        //Create new Asset and insert District field.
        for(Asset asset : newList) {
            String division = mapAccIdToDivision.get(asset.AccountId);
            if(division != null) {
                asset.District__c = mapDivisionToDistrict.get(division);
            } else {
                asset.District__c = 'Unassigned';
            }      
        }     
    }
    
    //Added by Dhilip as part of case 00007751 on 18th Aug 2017
    //method to update Account Attributes
    public static void UpdateAccountAttributes(List<Asset> newList)
    {
        List<Id> AssetAccountIds = new List<Id> ();
        for (Asset ast : newList)
        {
            if(!AssetAccountIds.contains(ast.AccountId))
                AssetAccountIds.add(ast.AccountId);
        }
        
        List<Asset> AssetsList = new List<Asset> ();
        List<Account> AccountsList = new List<Account> ();
        AssetsList = [Select Id, AccountId, HP__c from Asset where AccountId in :AssetAccountIds AND Status = 'Active' AND Operating_Status__c IN ('Operating', 'Backup')];
        AccountsList = [Select Id, Total_Site_Asset_Count__c, Total_Site_Horse_Power__c from Account where Id in :AssetAccountIds];                
        List<Account> AccountUpdateList = new List<Account> ();
        
        for(Account al: AccountsList)
        {
            decimal HPtotal = 0;
            decimal SiteCount = 0;
            for(Asset asl: AssetsList)
            {
                if(al.Id == asl.AccountId)
                {
                    string vHPStr = String.ValueOf(asl.get('HP__c'));
                    if(asl.get('HP__c')!=null)
                    HPTotal += (Decimal.ValueOf(vHPStr.replaceAll('[^.\\d]','')) !=null ? Decimal.ValueOf(vHPStr.replaceAll('[^.\\d]','')) : 0);
                    SiteCount = SiteCount + 1;
                }
            }
            al.Total_Site_Asset_Count__c = SiteCount;
            al.Total_Site_Horse_Power__c = HPTotal;
            AccountUpdateList.add(al);       
        }
                   
        if(AccountUpdateList.size() > 0)
        {
            update(AccountUpdateList);
        }
    }
}