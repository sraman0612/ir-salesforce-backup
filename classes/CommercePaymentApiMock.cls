public class CommercePaymentApiMock implements ICommercePaymentApi {
    private Id paymentId;
    private Id refundId;
    private Id paymentAuthorizationId;

    public CommercePaymentApiMock(Id recordId) {
        this.paymentId = recordId;
        this.refundId = recordId;
        this.paymentAuthorizationId = recordId;
    }

    public CommercePaymentApi.ResponseWrapper capture(CommercePaymentApi.RequestWrapper input, String paymentAuthorizationId) {
        CommercePaymentApi.ResponseWrapper mockResponse = new CommercePaymentApi.ResponseWrapper();
        mockResponse.paymentId = this.paymentId;
        return mockResponse;
    }

    public CommercePaymentApi.ResponseWrapper refundPayment(CommercePaymentApi.RequestWrapper input, String paymentId) {
        CommercePaymentApi.ResponseWrapper mockResponse = new CommercePaymentApi.ResponseWrapper();
        mockResponse.refundId = this.refundId;
        return mockResponse;
    }

    public CommercePaymentApi.ResponseWrapper reverseAuthorization(CommercePaymentApi.RequestWrapper input, String authorizationId) {
        CommercePaymentApi.ResponseWrapper mockResponse = new CommercePaymentApi.ResponseWrapper();
        mockResponse.paymentAuthorizationId = this.paymentAuthorizationId;
        return mockResponse;
    }
}