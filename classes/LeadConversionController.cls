// 
// (c) 2015 Appirio, Inc.
//
// Extension for LeadConversionPage
//
// May 13, 2015      Surabhi Sharma           Original: T-393554
//
// 11/16/15 - apex class edits migrated to Production environment via change set - SU
public with sharing class LeadConversionController {

    // variables
    public Lead sObjLead { get; set; }
    public Task sObjTask { get; set; }
    public String accId { get; set; }
    public String OppTypeValue{get;set;}
    public String accName { get; set; }
    public Boolean sendMailToOwner { get; set; }  
    public String cStatus { get; set; }
    public String accountName { get; set; }
    public String oppName { get; set; }
    public Boolean newOppFlag { get; set; }
    public List<SelectOption> accountList { get; set; }

    //constructor
    public LeadConversionController(ApexPages.StandardController controller) {
    //String leadId = ApexPages.currentPage().getParameters().get('Id');
     sObjLead = (Lead)controller.getRecord();
        if(sObjLead != null){
        // * 4/14/16 SU - Added Closed Won Total field to the lead conversion process data pull to add to the newly created opportunity record
            List<Lead> lstLead = [SELECT Id,AIRD_Closed_Won_Total__c,OwnerId,Company, Opportunity_Type__c FROM Lead WHERE Id =:sObjLead.Id LIMIT 1];
            sObjLead = lstLead.size() > 0 ? lstLead.get(0) : null; 

        }
        
        if(sObjLead == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide valid Lead Id.'));
            return;
        }

        sObjTask  = new Task();
        sObjTask.IsReminderSet = true;
        sObjTask.ReminderDateTime = System.now();
        sendMailToOwner = false;
        newOppFlag = true;
        oppName = sObjLead.Company+'-';
        
        OppTypeValue = sObjLead.Opportunity_Type__c;
        
        accId = '';
        accName = '';
        populateAccountList();

    }

    // A Method which populate default Account name that are similiar to Company field of Lead
    private void populateAccountList() {
        accountList = new List<SelectOption>();
        accountList.add(new SelectOption('New','Create New Account: '+sObjLead.Company));
        for(Account acc :  [SELECT Id,Name FROM Account WHERE Name Like :''+sObjLead.Company+'%']){
            accountList.add(new SelectOption(acc.Id,'Attach To Existing: '+acc.Name));
        }
    }

    // method to update lead by cancelling Workflow
    public void updateLead(){
        Lead obj = new Lead(Id = sObjLead.Id, Cancel_WorflowRule__c = true);
        update obj;

    }

    // return back to lead page
    public PageReference cancel() {
        return new PageReference('/'+sObjLead.Id);
    }
    
    //get the picklist Values of Opportunity Type field of Lead 
    public List<SelectOption> getOppType(){
        system.debug('In opp typemethod');
        List<SelectOption> options = new List<SelectOption>();
        
    /*   
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(Lead);
        Sobject Object_name = targetType.newSObject(); 
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); 
         Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
         //describe the 
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
        
        List<Schema.PicklistEntry> ple = field_map.get(Opportunity_Type__c).getDescribe().getPickListValues(); 
    */
    
    
        Schema.DescribeFieldResult fieldResult = Lead.Opportunity_Type__c.getDescribe();
        system.debug('Check' + fieldResult);
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple){
                options.add(new SelectOption(f.getLabel(), f.getValue()));
              //  sObjOpp.Type = sObjLead.Opportunity_Type__c;
        }       
        
        system.debug('Check *********************' + options);
        
     return options;
     
    }
    

    // A Method which execute on Click of convert button and perform Lead conversion
    public PageReference performConvert() {

        // Create a savepoint while LeadConversion not performed
        Savepoint leadConvertSavePoint = Database.setSavepoint();

        try{
            updateLead();
            system.debug([select id, Cancel_WorflowRule__c from Lead where id = :sObjLead.id]);
            Database.LeadConvert lConvert = new database.LeadConvert();
            lConvert.setLeadId(sObjLead.id);
            lConvert.setOwnerId(sObjLead.OwnerId);

            // set optional opportunity creation flag
            /*
             * I swapped these two. It ws defaulting to NOT create the oppty
             * SGL
             */
            if(newOppFlag)
                lConvert.setOpportunityName(oppName);
            else
                lConvert.setDoNotCreateOpportunity(true);
                


            lConvert.setSendNotificationEmail(sendMailToOwner);
            //lConvert.setOpportunityType(OppTypeValue);

            /*
             *  This was commented out (next three lines). Could have been a defect. Code SHOULD set an existing account if one exist
             * SGL
             */
           if(accountName != 'New'){
                lConvert.setAccountId(accountName);
            }    

            lConvert.setConvertedStatus(cStatus);

            // performing lead conversion
            Database.LeadConvertResult lcResult = Database.convertLead(lConvert);
            if(lcResult.isSuccess()){
                system.debug('convertLead=='+lConvert);
               /*
                * Now that the conversion process is creating the Opportunity I needed to modify the below statement to UPDATE not CREATE
                * I added the 'ID = lcResult.getOpportunityId()' part to the SOQL to make sure we grab an existing oppty
                * ALSO PLEASE MOVE THIS TO A WORKFLOW AND ELIMINATE THE APEX
                */
               
               
                //This section should all be replaced with workflows in the near future - SU: 11/12/15
                //what to do for lead source fields?
                if(lcResult.getOpportunityId() != null){
                    System.debug('------------------ lcResult.getAccountId() --------------'+lcResult.getAccountId());
                        Opportunity opp = new Opportunity(ID = lcResult.getOpportunityId() ,AccountId = lcResult.getAccountId(),Name =  oppName, Type = OppTypeValue);
                        // * 4/14/16 SU - Lead Source 1-4 moved to Lead field mapping
                            if(OppTypeValue == 'Service(Installs&Lrg.Comissionable Svce)'){
                            opp.StageName = 'Stage 1.Target/Qualify';
                            opp.CloseDate = System.today() + 90;
                        }
                        else if(OppTypeValue == 'Parts & Service'){
                            opp.StageName = 'Stage 5.Closed Won';
                            opp.Closed_Lost_Reason__c = 'Due to Lead Conversion, Opportunity Stage updated to Closed Won';
                            // * 4/18/16 SU - Set opportunity amount to lead amount for converted inside sales leads
                            opp.Amount = sObjLead.AIRD_Closed_Won_Total__c;
                            opp.CloseDate = System.today();
                        }
                        else {
                            opp.StageName = 'Stage 1.Target/Qualify'; 
                        }
                        //why not a workflow?
                        /*
                         * Was an insert, changed to update
                         * SGL
                         */
                        update opp;
                        
                    }

                // check that if subject is available then insert task record
                if(sObjTask.Subject != null){
                    if(lcResult.getOpportunityId() == null)
                    sObjTask.WhatId = lcResult.getAccountId();
                    else
                    
                    sObjTask.WhatId = lcResult.getOpportunityId();
                    
                    

                    sObjTask.WhoId = lcResult.getContactId();
                    sObjTask.OwnerId = sObjLead.OwnerId;

                    insert sObjTask;
                    
                     // set the picklist Value of Lead Opportunity Type field to Opportunity - Type field. 
                    
                    
                   
                }
                return new PageReference('/'+lcResult.getAccountId());
            }else{
                Database.rollback(leadConvertSavePoint);
            }

        }catch(Exception ex){
            
            ApexPages.addMessages(ex);
            System.debug('exception---------------------------------' +ex);
            // Rollback lead conversion
            Database.rollback(leadConvertSavePoint);
            return null;
        }
        return new PageReference('/'+sObjLead.Id);

    }

    // A Method which adding account Name and Id into AccountList
    public PageReference updateAccountList(){
        accountList.add(new SelectOption(accId,'Attach To Existing: '+accName));
        return null;
    }

    // It returns Conversion Status List which contain Master label
    public List<SelectOption> getConversionStatus() {
            List<SelectOption> options = new List<SelectOption>();
            for(LeadStatus convertStatus : [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true]){
                options.add(new SelectOption(convertStatus.MasterLabel,convertStatus.MasterLabel));
            }

            return options;
    }
    
 
        
}