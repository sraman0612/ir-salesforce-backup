/**
 * Created by wenboliu on 20/11/2024.
 */

@isTest
public class B2BLandingPageControllerTest {

    @testSetup
    static void setupTestData() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
        User usr = CTS_TestUtility.createUser(false);
        usr.UserRoleId = r1.Id;
        usr.LastName = 'test user';
        insert usr;
        PermissionSet ps1 = [SELECT Id FROM PermissionSet WHERE Name = 'B2BAdmin'];
        PermissionSet ps2 = [SELECT Id FROM PermissionSet WHERE Name = 'B2B_Admin_Additional_Access'];
        PermissionSetAssignment psa1 = new PermissionSetAssignment
                (AssigneeId = usr.Id, PermissionSetId = ps1.Id);
        insert psa1;
        PermissionSetAssignment psa2 = new PermissionSetAssignment
                (AssigneeId = usr.Id, PermissionSetId = ps2.Id);
        insert psa2;
        System.runAs(usr) {
            Account accountRecord = B2BTestDataFactory.createAccount('Customer');
            insert accountRecord;

            Contact contactRecord = B2BTestDataFactory.createContact(accountRecord.Id, 'First Name', 'Last Name');
            insert contactRecord;
            WebStore webstore = B2BTestDataFactory.createWebStore('Test Store');
            insert webstore;

            ProductCatalog catalog = B2BTestDataFactory.createCatalog('Products');
            insert catalog;

            WebStoreCatalog webstoreCatalog = B2BTestDataFactory.createWebStoreCatalog(webstore.Id, catalog.Id);
            insert webstoreCatalog;

            ProductCategory category = B2BTestDataFactory.createProductCategory('Products', catalog.Id);
            insert category;
        }
    }

    @IsTest
    static void testGetCategory() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
        User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id  AND LastName = 'test user' LIMIT 1];
        // Test the getCategoryId method
        System.runAs(user){
            Test.startTest();
            List<ProductCategory> categories  = B2BLandingPageController.getCategoryId([SELECT Id FROM WebStore LIMIT 1].Id, [SELECT Id, Name FROM ProductCategory LIMIT 1].Name);
            Test.stopTest();

            // Verify the categoryId
            System.assertEquals([SELECT Id FROM ProductCategory LIMIT 1].Id, categories[0].Id, 'The returned category should match the test category');
        }
    }

    @IsTest
    static void testGetLandingPageConfiguration() {
        Test.startTest();
        List<B2BLandingPageController.LandingPageConfigurationWrapper> result =
                B2BLandingPageController.getLandingPageConfiguration();
        Test.stopTest();

        // Assertions
        System.assertNotEquals(null, result, 'Result should not be null');
    }

    @isTest
    static void testGetCategoryWithInvalidWebStoreId() {
        // Test with a non-existing category
        Test.startTest();
        List<ProductCategory> categories = B2BLandingPageController.getCategoryId('InvalidWebStoreId',[SELECT Id, Name FROM ProductCategory LIMIT 1].Name);
        Test.stopTest();

        // Assertions
        System.assertEquals(0, categories.size(), 'Expected to retrieve 0 categories');
    }

    @isTest
    static void testGetCategoryWithInvalidCategoryName() {
        // Test with a non-existing category
        Test.startTest();
        List<ProductCategory> categories = B2BLandingPageController.getCategoryId([SELECT Id FROM WebStore LIMIT 1].Id,'InvalidCategoryName');
        Test.stopTest();

        // Assertions
        System.assertEquals(0, categories.size(), 'Expected to retrieve 0 categories');
    }
}