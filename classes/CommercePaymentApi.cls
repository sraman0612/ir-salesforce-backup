public inherited sharing class CommercePaymentApi implements ICommercePaymentApi {
    public class RequestWrapper {
        public String accountId;
        public Decimal amount;
    }

    public class ResponseWrapper {
        public String paymentId;
        public String refundId;
        public String paymentAuthorizationId;

    }

    public ResponseWrapper capture(RequestWrapper input, String paymentAuthorizationId) {
        ConnectApi.CaptureRequest connectApiRequest = new ConnectApi.CaptureRequest();
        connectApiRequest.accountId = input.accountId;
        connectApiRequest.amount = input.amount;
        ResponseWrapper response = new ResponseWrapper();

        if(Test.isRunningTest()) {
            response.paymentId = 'MockPaymentId';
        } else {
            ConnectApi.CaptureResponse connectApiResponse = ConnectApi.Payments.capture(connectApiRequest, paymentAuthorizationId);
            response.paymentId = connectApiResponse.payment.id;
        }
        return response;
    }

    public ResponseWrapper refundPayment(RequestWrapper input, String paymentId) {
        ConnectApi.ReferencedRefundRequest request = new ConnectApi.ReferencedRefundRequest();
        request.amount = input.amount;
        ResponseWrapper response = new ResponseWrapper();

        if(Test.isRunningTest()) {
            response.refundId = 'MockRefundId';
        } else {
            ConnectApi.ReferencedRefundResponse connectApiResponse = ConnectApi.Payments.refund(request, paymentId);
            response.refundId = connectApiResponse.refund.id;
        }

        return response;
    }

    public ResponseWrapper reverseAuthorization(RequestWrapper input, String paymentAuthorizationId) {
        ConnectApi.AuthorizationReversalRequest connectApiRequest = new ConnectApi.AuthorizationReversalRequest();

        connectApiRequest.amount = input.amount;
        ResponseWrapper response = new ResponseWrapper();

        if(Test.isRunningTest()) {
            response.paymentAuthorizationId = 'MockPaymentAuthorizationId';
        } else {
            ConnectApi.AuthorizationReversalResponse connectApiResponse = ConnectApi.Payments.reverseAuthorization(connectApiRequest, paymentAuthorizationId);
            response.paymentAuthorizationId = connectApiResponse.paymentAuthAdjustment.id;

        }
        return response;
    }
}