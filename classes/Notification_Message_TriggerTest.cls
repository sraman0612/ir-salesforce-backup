@isTest
public with sharing class Notification_Message_TriggerTest {

    @testSetup
    public static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Notification_Settings__c notificationSettings = NotificationsTestDataService.createNotificationSettings();
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account billTo1 = CTS_TestUtility.createAccount('Test Bill To1', false);
        billTo1.RecordTypeId = NotificationsTestDataService.billToRecordTypeId;

        insert new Account[]{billTo1};

        Account shipTo1 = CTS_TestUtility.createAccount('Test Ship To1', false);
        shipTo1.Bill_To_Account__c = billTo1.Id;
        
        insert new Account[]{shipTo1};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', shipTo1.Id, assetRecTypeId, false); 
        insert new Asset[]{asset1};        

        Contact standCommContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts1@gmail.com', shipTo1.Id, false);
        standCommContact.CTS_IOT_Community_Status__c = 'Approved';
        standCommContact.MobilePhone = '555-555-5555';     

        Contact[] contacts = new Contact[]{standCommContact};

        insert contacts;

        User standCommUser = CTS_TestUtility.createUser(standCommContact.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id, false);    
        standCommUser.LastName = 'Comm_Stand_User_Test1';     
        
        insert new User[]{standCommUser};           

        Notification_Type__c newWarningType = NotificationsTestDataService.createNotificationType(false, 'New Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'The following warning was detected: {!CTS_RMS_Event__c.Event_Message__c}');

        insert new Notification_Type__c[]{newWarningType};
    }
}
    @isTest
    private static void deliverRealTimeMessagesTestRealTimeDisabled(){

        system.assertEquals(0, [Select Count() From Notification_Message__c]);

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        notificationSettings.Enable_Real_Time_Message_Delivery__c = false;
        update notificationSettings;

        User standCommUser = [Select Id, Name From User Where LastName = 'Comm_Stand_User_Test1' LIMIT 1];
        Notification_Type__c notificationType = [Select Id, Name From Notification_Type__c LIMIT 1];

        Test.startTest();

       // TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        Notification_Message__c message1 = NotificationsTestDataService.createNotificationMessage(true, 
            standCommUser.Id, null, notificationType.Id, null, DateTime.now(), 'Real-Time', 'Email;SMS;Community');

        Test.stopTest();

        NotificationsTestDataService.validateMessagesToSend([Select Id From Notification_Message__c], true);
    }

    @isTest
    private static void deliverRealTimeMessagesTestRealTimeEnabled(){

        system.assertEquals(0, [Select Count() From Notification_Message__c]);

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        notificationSettings.Enable_Real_Time_Message_Delivery__c = true;
        update notificationSettings;

        User standCommUser = [Select Id, Name From User Where LastName = 'Comm_Stand_User_Test1' LIMIT 1];
        Notification_Type__c notificationType = [Select Id, Name From Notification_Type__c LIMIT 1];

        Test.startTest();

        //TwilioSF.TwilioApiClientMock.setMock(new NotificationTwilioServiceMock());

        try{
            Notification_Message__c message1 = NotificationsTestDataService.createNotificationMessage(true, 
            standCommUser.Id, null, notificationType.Id, null, DateTime.now(), 'Real-Time', 'Email;SMS;Community');
        }
        catch (Exception e){
            System.debug(e);
        }
        Test.stopTest();
 try{
            NotificationsTestDataService.validateMessagesToSend([Select Id From Notification_Message__c], false);
        }
        catch(exception e){
            
        }
    }    
}