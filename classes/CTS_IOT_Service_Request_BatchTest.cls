@isTest
public with sharing class CTS_IOT_Service_Request_BatchTest {

    @TestSetup
    private static void createData(){

        Account a = CTS_TestUtility.createAccount('Test Account1', true);
        RecordType assetRecordType = [Select Id From RecordType Where sObjectType = 'Asset' and DeveloperName = 'CTS_TechDirect_Asset'];

        Asset asset = CTS_TestUtility.createAsset('Test Asset1', 'ABC-123', a.Id, assetRecordType.Id, false);
        asset.Siebel_ID__c = 'XYZ-543';
        insert asset;

        Contact c = CTS_TestUtility.createContact('Smith', 'Steve', 'ssmith@abc123.com', a.Id, true);
        CTS_TestUtility.setDefaultSetting(true);
    }

    @isTest
    private static void testSchedule(){

        Test.startTest();

            system.schedule('Test Service Request Job ABC123', '0 0 * * * ?', new CTS_IOT_Service_Request_Batch());

        Test.stopTest();     
        
        system.assertEquals(1, [Select Count() From CronTrigger Where CronJobDetail.Name = 'Test Service Request Job ABC123']);     
    }

    @isTest
    private static void testScheduleRunOnCompletion(){
        
        CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        communitySettings.Service_Request_Batch_Run_on_Completion__c = true;
        communitySettings.Service_Request_Batch_Run_Complete_Min__c = 5;
        update communitySettings;

        Test.startTest();

            system.schedule('Test Service Request Job ABC123', '0 0 * * * ?', new CTS_IOT_Service_Request_Batch());

        Test.stopTest();     
        
        system.assertEquals(1, [Select Count() From CronTrigger Where CronJobDetail.Name = 'Test Service Request Job ABC123']);     
    }    

    @isTest
    private static void testRunOnCompletion(){
        
        CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        communitySettings.Service_Request_Batch_Run_on_Completion__c = true;
        communitySettings.Service_Request_Batch_Run_Complete_Min__c = 5;
        update communitySettings;

        Test.startTest();

            Database.executeBatch(new CTS_IOT_Service_Request_Batch());

        Test.stopTest();
        
        // Verify the job was scheduled to run again 5 minutes in the future
        CronTrigger ct = [Select NextFireTime From CronTrigger Where CronJobDetail.Name Like 'Test_CTS_IOT_Service_Request_Batch_%'];
        system.assert(DateTime.now().addMinutes(6) > ct.NextFireTime);
    }

    @isTest
    private static void testMigratedRequests(){

        Account a = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry From Account];
        Asset asset = [Select Id, SerialNumber, Siebel_ID__c From Asset];
        Contact c = [Select Id, FirstName, LastName, Email, Title, Phone From Contact];

        // Create some migrated requests
        CTS_IOT_Service_Request__c[] requests = new CTS_IOT_Service_Request__c[]{};

        for (Integer i = 0; i < 5; i++){
            requests.add(CTS_TestUtility.createCTSIOTServiceRequest(a.Id, c.Id, asset, 'Migrated', 'Upgrade', '2 - High', 'Request ' + i));
        }

        insert requests;

        Test.startTest();

            Database.executeBatch(new CTS_IOT_Service_Request_Batch());

        Test.stopTest();

        // Verify the statuses did not change
        for (CTS_IOT_Service_Request__c request : [Select Id, Status__c From CTS_IOT_Service_Request__c]){
            system.assertEquals('Migrated', request.Status__c);
        }

        // Verify no leads were created
        system.assertEquals(0, [Select Count() From Lead]);

        // Verify the job was not scheduled to run again
        system.assertEquals(0, [Select Count() From CronTrigger Where CronJobDetail.Name Like 'Test_CTS_IOT_Service_Request_Batch_%']);        
    }

    @isTest
    private static void testOpenRequests(){

        Account a = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry From Account];
        Asset asset = [Select Id, SerialNumber,Siebel_ID__c From Asset];
        Contact c = [Select Id, FirstName, LastName, Email, Title, Phone From Contact];

        // Create some migrated requests
        CTS_IOT_Service_Request__c[] requests = new CTS_IOT_Service_Request__c[]{};

        for (Integer i = 0; i < 5; i++){
            requests.add(CTS_TestUtility.createCTSIOTServiceRequest(a.Id, c.Id, asset, 'Migrated', 'Upgrade', '2 - High', 'Service request description ' + i));
        }

        // Create some open requests
        for (Integer i = 5; i < 10; i++){
            requests.add(CTS_TestUtility.createCTSIOTServiceRequest(a.Id, c.Id, asset, 'Open', 'Upgrade', '2 - High', 'Service request description ' + i));
        }        

        insert requests;

        Test.startTest();

            Database.executeBatch(new CTS_IOT_Service_Request_Batch());

        Test.stopTest();

        CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();

        // Verify the open requests were migrated
        CTS_IOT_Service_Request__c[] migratedRequests = [Select Id, Migrated_DateTime__c, Lead__c From CTS_IOT_Service_Request__c Where Status__c = 'Migrated' and Lead__c != null and Migrated_DateTime__c != null];
        system.assertEquals(5, migratedRequests.size());

        // Verify leads were created for the open requests
        Lead[] leads = [Select OwnerId, RecordTypeId, FirstName, LastName, Company, Email, Title, Phone, Street, City, State, PostalCode, Country, Description, Product__c, Assignment_Key__c,
                        LeadSource, Lead_Source_1__c, Lead_Source_2__c, SBU_ID_Workflow__c, Opportunity_Type__c, Request_Type__c
                        From Lead];
        system.assertEquals(5, leads.size());

        for (Lead l : leads){

            system.debug('validating lead: ' + l);

            system.assertEquals(c.FirstName, l.FirstName);
            system.assertEquals(c.LastName, l.LastName);
            system.assertEquals(a.Name, l.Company);
            system.assertEquals(c.Email, l.Email);
            system.assertEquals(c.Phone, l.Phone);
            system.assertEquals(c.Title, l.Title);
            system.assertEquals(a.BillingStreet, l.Street);
            system.assertEquals(a.BillingCity, l.City);
            system.assertEquals(a.BillingState, l.State);
            system.assertEquals(a.BillingPostalCode, l.PostalCode);
            system.assertEquals(a.BillingCountry, l.Country);                    
            system.assertEquals(asset.SerialNumber, l.Product__c);            
            system.assert(l.Description.contains('Helix Service Request - '));
            system.assert(l.Description.contains('Serial Number: ' + asset.SerialNumber));            
            system.assert(l.Description.contains('Severity: 2 - High')); 
            system.assert(l.Description.contains('Type: Upgrade'));
            system.assert(l.Description.contains('Asset Siebel_ID__c: XYZ-543')); 
            system.assert(l.Description.contains('Service Request Description: '));
            system.assertEquals(communitySettings.New_Service_Request_Record_Type_ID_NA__c, l.RecordTypeId);
            system.assertEquals(communitySettings.Default_Lead_Source__c, l.LeadSource);
            system.assertEquals(communitySettings.Default_Lead_Source1_Service_Requests__c, l.Lead_Source_1__c);
            system.assertEquals(communitySettings.Default_Lead_Source2_Service_Requests__c, l.Lead_Source_2__c);
            system.assertEquals(communitySettings.Default_SBU_ID_Workflow__c, l.SBU_ID_Workflow__c);
            system.assertEquals(communitySettings.Default_Lead_Opportunity_Type__c, l.Opportunity_Type__c);
            system.assertEquals(communitySettings.Default_Request_Type_Service_Requests__c, l.Request_Type__c);
        }

        // Verify the job was not scheduled to run again
        system.assertEquals(0, [Select Count() From CronTrigger Where CronJobDetail.Name Like 'Test_CTS_IOT_Service_Request_Batch_%']);                
    }    
}