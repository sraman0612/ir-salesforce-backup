/*
    @Author : Snehal Chabukswar
    @BuiltDate : 12 March 2020
    @Description : called from CTS_CSITrigger
    @Company : cognizant
*/
public class CTS_CSITriggerHandler implements ITriggerHandler {

    ///Use this variable to disable this trigger from transaction
    public static Boolean TriggerDisabled = false;
     
    //check if the trigger is disabled from transaction
    public Boolean isDisabled(){
        return TriggerDisabled;
    }
     
    public void beforeInsert(List<sObject> newList) {}
     
    public void afterInsert( Map<Id, sObject> newMap) {
        system.debug('-----Start of After Insert--------');
        getaccIdLatestCSIMap(newMap);
        system.debug('-----End of After Insert--------');
    }
     
    public void beforeUpdate( Map<Id, sObject> newMap, Map<Id, sObject> oldMap) {}
     
    public void afterUpdate(Map<Id, sObject> newMap,Map<Id, sObject> oldMap) {
        system.debug('-----Start of After Update--------');
        getaccIdLatestCSIMap(newMap);
        system.debug('-----End of After Update--------');
    }
     
    public void beforeDelete(Map<Id, sObject> oldMap) {}
     
    public void afterDelete( Map<Id, sObject> oldMap) {
        system.debug('-----Start of After Delete--------');
        getaccIdLatestCSIMap(oldMap);
        system.debug('-----End of After Delete--------');
    }
     
    public void afterUnDelete( Map<Id, sObject> newMap) {
        system.debug('-----Start of After unDelete--------');
        getaccIdLatestCSIMap(newMap);
        system.debug('-----End of After unDelete--------');
    }
    
    /*
        @Author : Snehal Chabukswar
        @Description : called to get map of latest CSI record for respective account
        @params : Set<Id> accIdSet
    */
    public void getaccIdLatestCSIMap(Map<Id,sObject> recordMap){
        Set<Id> accIdSet = new Set<Id>();
        for(sObject Obj:recordMap.values()){
            Customer_Satisfaction_Results__c csiObj = (Customer_Satisfaction_Results__c)Obj;
            accIdSet.add(csiObj.Account__c);
        }
        Map<Id,Customer_Satisfaction_Results__c> accIdLatestCSIMap = new Map<Id,Customer_Satisfaction_Results__c>();
        
        for(Account accObj : [SELECT Id,(SELECT Id,Survey_Comment__c,
                                         CTS_OSAT_Satisfaction__c,
                                         CTS_Promoted_Score__c,
                                         CTS_Committed_Score__c,
                                         CTS_Commenter__c,
                                         CTS_Email__c,
                                         Response_Date__c 
                                         FROM Customer_Satisfaction_Indexes__r
                                         ORDER BY  Response_Date__c DESC)
                              FROM Account WHERE Id IN:accIdSet]){
                                  if(!accObj.Customer_Satisfaction_Indexes__r.isEmpty()){
                                      system.debug('---In If-----');
                                  	 accIdLatestCSIMap.put(accObj.Id,accObj.Customer_Satisfaction_Indexes__r[0]);      
                                  }else{
                                     /*to handle last csi record deletion*/ 
                                      system.debug('---In else-----');
                                  	 accIdLatestCSIMap.put(accObj.Id,null);
                                  }
        					 }
        updateAccount(accIdLatestCSIMap);
        
    }
    
    /*
        @Author : Snehal Chabukswar
        @Description : called to update account with latest Customer satisfaction fields from latest CSI record
        @params : Map<Id,Customer_Satisfaction_Results__c> accIdLatestCSIMap
    */
    public void updateAccount(Map<Id,Customer_Satisfaction_Results__c> accIdLatestCSIMap){
        List<Account>updatedAccList = new List<Account>();
        try{
            for(Id accId:accIdLatestCSIMap.keySet()){
                Account acc = new Account(Id= accId);
                Customer_Satisfaction_Results__c csiObj = accIdLatestCSIMap.get(accId);
                if(csiObj!=null){
                	acc.CTS_OSAT_Satisfaction__c = csiObj.CTS_OSAT_Satisfaction__c;
                    acc.CTS_Promoted_Score__c = csiObj.CTS_Promoted_Score__c;
                    acc.CTS_Committed_Score__c = csiObj.CTS_Committed_Score__c;
                    acc.CTS_Survey_Date__c = csiObj.Response_Date__c;
                    acc.CTS_CSI_Comments__c = csiObj.Survey_Comment__c;
                    acc.CTS_Commenter__c = csiObj.CTS_Commenter__c;
                    acc.CTS_Email__c = csiObj.CTS_Email__c;
                }else{
                    acc.CTS_OSAT_Satisfaction__c = null;
                    acc.CTS_Promoted_Score__c = null;
                    acc.CTS_Committed_Score__c = null;
                    acc.CTS_Survey_Date__c = null;
                    acc.CTS_CSI_Comments__c = null;
                    acc.CTS_Commenter__c = null;
                    acc.CTS_Email__c = null;
                }
                
                updatedAccList.add(acc);
            }
            if(!updatedAccList.isEmpty()){
                update updatedAccList;
            }   
        }Catch(Exception ex){
            system.debug('Exception is---->'+ex);
        }
        
    }
    

}