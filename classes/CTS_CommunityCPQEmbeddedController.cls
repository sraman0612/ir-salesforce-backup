public class CTS_CommunityCPQEmbeddedController {

  public string canvasParams{get;set;}
  public boolean isEmbedded{get;set;}
  public boolean hasError{get;set;}
  public cafsl__Oracle_Quote__c quote{get;set;}
  
  public CTS_CommunityCPQEmbeddedController (ApexPages.StandardController controller){
    isEmbedded = TRUE;
    hasError = FALSE;
    String oppid ='';
    String refererURL = ApexPages.currentPage().getHeaders().get('Referer');
    if(refererURL.contains('quoteid')){
      String [] refererArr = refererURL.split('&quoteid=');
      quote = [SELECT Id FROM cafsl__Oracle_Quote__c WHERE Id = :refererArr[1]];
      oppid = refererArr[0].split('id=')[1];
    } else {
      String [] refererArr = refererURL.split('id=');
      oppid=refererArr[1];
    }
    string acctid = [SELECT AccountId FROM Opportunity WHERE Id = :oppid].AccountId;
    String paramStr = '{';
    for (OracleCPQCanvasParam__mdt param : [Select MasterLabel, Value__c FROM OracleCPQCanvasParam__mdt]){
        paramStr += '"' + param.MasterLabel + '":';
    	if (param.MasterLabel == '_partnerAccountId'){
    		paramStr += '"' + acctid + '",';
    	} else if (param.MasterLabel == '_partnerOpportunityId') {
    		paramStr += '"' + oppid + '",';
    	} else if (param.Value__c=='null'){
    		paramStr += 'null,';
    	} else {
    		paramStr += '"' + param.Value__c + '",';
    	}
    }
    paramStr = paramStr.removeEnd(',');
    paramStr += '}';
    canvasParams = paramStr;  
  }
}