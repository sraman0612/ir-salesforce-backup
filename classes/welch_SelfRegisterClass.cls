/*
* Revisions:
10-Jan-2025: #03272509(Mahesh) To reduce errors while closing the Case by populating Department details on case.
*/
public without sharing class welch_SelfRegisterClass {    

    @InvocableMethod(label='Self Register Contractor' description='Creates a partner user')
    public static SelfRegisterActionResponse[] invoke(SelfRegisterActionRequest[] requests){
     List<User> userList = [Select Id, isActive, ContactId from User where UserName = :requests[0].email_address];
        
        List<SelfRegisterActionResponse> responses = new List<SelfRegisterActionResponse>();
            if(userList.size() == 0){
                Savepoint sp = Database.setSavepoint();
                try{
                String countryCode = '';
                BuyerGroup buyGroup = [SELECT Id, Name FROM BuyerGroup WHERE Name =: System.Label.Welch_Default_BuyerGroup ];
                /*Schema.DescribeFieldResult fieldResult = User.Countrycode.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry f : ple){
                    if(f.getLabel() == requests[0].country_address){
                        countryCode = f.getValue();
                        break;
                    }
                }*/
                // build new account
                // give base pricelist
                Account newAccount = new Account();
                if(requests[0].country_address == 'United States'){
                	newAccount.Name = requests[0].company_name;
                    newAccount.ShippingStreet = requests[0].street_address;
                    newAccount.ShippingCity = requests[0].city_address;
                    newAccount.ShippingCountry = requests[0].country_address;
                        //BillingCountry = countryCode,
                    newAccount.ShippingState = requests[0].state_address;
                    newAccount.ShippingPostalCode = requests[0].zip_address;
                        //BillingCountry = 'United States',
                        //added by JS 1/5/21
                        // Type = requests[0].business_type,
                    newAccount.Phone = requests[0].phone;
                    insert newAccount;    
                    System.debug('--newAccount-- ' + newAccount);
                }else{
                    throw new NotValidCountryException('Country Outsite US is not allowed');
                }
                
                
                List<ContactPointAddress> addressesToInsert = new List<ContactPointAddress>();
                ContactPointAddress billingAddress = new ContactPointAddress();
                ContactPointAddress shippingAddress = new ContactPointAddress();
                billingAddress.Name = requests[0].first_name + ' ' + requests[0].last_name + ' Billing';
                billingAddress.ParentId = newAccount.Id;
                billingAddress.AddressType = 'Billing';
                billingAddress.IsDefault = true;
                billingAddress.Country = requests[0].country_address;
                //billingAddress.CountryCode = countryCode;
                billingAddress.City = requests[0].city_address;
                billingAddress.State = requests[0].state_address;
                billingAddress.PostalCode = requests[0].zip_address;
                billingAddress.Street = requests[0].street_address;
                addressesToInsert.add(billingAddress);
                shippingAddress.Name = requests[0].first_name + ' ' + requests[0].last_name + ' Shipping';
                shippingAddress.ParentId = newAccount.Id;
                shippingAddress.AddressType = 'Shipping';
                shippingAddress.Country = requests[0].country_address;
                //shippingAddress.CountryCode = countryCode;
                shippingAddress.City = requests[0].city_address;
                shippingAddress.State = requests[0].state_address;
                shippingAddress.PostalCode = requests[0].zip_address;
                shippingAddress.Street = requests[0].street_address;
                addressesToInsert.add(shippingAddress);
                insert addressesToInsert;

                //CREATE BUYER GROUP, ENTITLEMENT POLICY AND PRICE BOOK
                //insert buyer account for Commerce
                BuyerAccount newBuyerAccout = new BuyerAccount (
                    BuyerId = newAccount.Id,
                    Name = requests[0].company_name,
                    IsActive = true
                );
                
                insert newBuyerAccout;

                BuyerGroup defaultGroup = [SELECT Name, Id FROM BuyerGroup WHERE Name =: System.Label.Welch_Default_BuyerGroup LIMIT 1];
                
                BuyerGroupMember groupMember = new BuyerGroupMember();
                groupMember.BuyerId = newAccount.Id;
                groupMember.BuyerGroupId = defaultGroup.Id;
                insert groupMember;
                
                // PermissionSet  b2bPermissionSet = [SELECT Id, Name FROM PermissionSet WHERE Name = 'B2B_Commerce_User_LE'];
                //assign default profile
                // build new user.
                User newUser = new User();
                newUser.Username = requests[0].username;
                newUser.Email = requests[0].email_address;
                newUser.FirstName = requests[0].first_name;
                newUser.LastName = requests[0].last_name;
                newUser.Phone = requests[0].phone;
                newUser.CompanyName = requests[0].company_name;

                // Default ui on mobile is set to S1 for user created using site object.
                // Enable this perm to change it to community.
                newUser.UserPreferencesHideS1BrowserUI = true;
                // generating unique value for community nickname.
                String nickname = newUser.FirstName.substring(0,1) + newUser.LastName.substring(0,1);
                nickname += String.valueOf( Crypto.getRandomInteger() ).substring(1,7);
                
                newUser.CommunityNickname = nickname;            
                //debug
                // newUser.Username = nickname + '@s.com';
                // newUser.Email = nickname + '@s.com';

                

                
                
                    // System.debug('Trying to create user');
                    // build response that gets passed back to the flow
                    SelfRegisterActionResponse resp = new SelfRegisterActionResponse();
                    resp.errorMessage = 'Successfully Created';
                    resp.userId = Site.createExternalUser(newUser, newAccount.Id);
                    System.debug('--newUser-- ' + newUser);
                    resp.accountId = newAccount.Id;
                    resp.contactId = newUser.ContactId;
                    // PermissionSetAssignment psa = new PermissionSetAssignment (PermissionSetId = b2bPermissionSet.Id, AssigneeId = resp.userId);
                    // insert psa;
					
                    if(!Test.isRunningTest()){
                    	Contact userContact = [SELECT Id, RecordTypeId FROM Contact WHERE Id=:newUser.ContactId];
    	                userContact.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Contact' AND Name='LFS Contact'].Id;
	                    update userContact;    
                    }
                    
                    Group queue = [SELECT Id, Type FROM Group WHERE Type = 'Queue' AND Name = 'LFS Welch Queue' Limit 1];
                    // 10-Jan-2025: #03272509 Query on user,OrgWideEmailAddress and Business Hours.
					List<User> currUserList = [SELECT Id, Business_Hours__c, Assigned_From_Email_Address__c FROM User WHERE Id=: UserInfo.getUserId()];
                                        
					List<OrgWideEmailAddress> orgEmailAddressList = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Address=: currUserList.get(0).Assigned_From_Email_Address__c];
                    
                 	List<BusinessHours> BusinessHoursList = [SELECT Id, Name FROM BusinessHours WHERE Name=: currUserList.get(0).Business_Hours__c];
                    // 10-Jan-2025: #03272509 populating Assigned_From_Address_Picklist__c,Org_Wide_Email_Address_ID__c,BusinessHoursId
                    Case userCreatedCase = new Case();
                    userCreatedCase.AccountId = newAccount.Id;
                    userCreatedCase.ContactId = newUser.ContactId;
                    userCreatedCase.Status = 'New';
                    userCreatedCase.Subject = 'New Account Created for User ' + newUser.FirstName + ' ' + newUser.LastName;
                    userCreatedCase.Region__c = 'NA';
                    userCreatedCase.Brand__c = 'Welch';
                    userCreatedCase.Priority = 'Medium';
                    userCreatedCase.Origin = 'Community';
                    userCreatedCase.OwnerId = queue.Id;
                    userCreatedCase.Type = 'Question';
                    userCreatedCase.GDI_Department__c = 'Medical';
                    userCreatedCase.Product_Category__c = 'Medical';
                    userCreatedCase.Assigned_From_Address_Picklist__c = !currUserList.isEmpty() && currUserList.get(0)?.Assigned_From_Email_Address__c != null ? currUserList.get(0).Assigned_From_Email_Address__c : '';
                    userCreatedCase.Org_Wide_Email_Address_ID__c = !orgEmailAddressList.isEmpty() && orgEmailAddressList.get(0)?.id != null ? orgEmailAddressList.get(0).id : '';
                    userCreatedCase.BusinessHoursId =  !BusinessHoursList.isEmpty() && BusinessHoursList.get(0)?.Id != null? BusinessHoursList.get(0).Id : '';
                    userCreatedCase.Description = 'New Account Created for User ' + newUser.FirstName + ' ' + newUser.LastName;
                    userCreatedCase.RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType='Case' AND Name='IR Comp OM Account Management'].Id;
                    
                    insert userCreatedCase;
                    
                    Business_Relationship__c br = new Business_Relationship__c();
                    br.Account__c = newAccount.Id;
                    br.Business__c = 'Welch';
                    br.Brand__c = 'Welch';
                    br.CTS_Channel__c  = 'Direct';
                    br.Type__c = 'Prospect';
                    insert br;

                    responses.add(resp);
                                        
                }catch(Site.ExternalUserCreateException ex){
                    System.debug('Failed to create user');                
                    System.debug(ex.getMessage());
                    System.debug(ex.getDisplayMessages());
                    System.debug('CAUSE ::: ' + ex.getCause());
                    System.debug('LINE :: ' + ex.getLineNumber());
                    // System.debug('STACK TRACE :: ' + ex.getStackTraceString  ());
                    Database.rollback(sp);
                    
                    // build response that gets passed back to the flow
                    SelfRegisterActionResponse resp = new SelfRegisterActionResponse();
                    resp.errorMessage = ex.getMessage();
                    responses.add(resp);
                }catch(Exception e){
                    System.debug('Failed to create user');                
                    System.debug(e.getMessage());
                    System.debug('LINE :: ' + e.getLineNumber());
                    Database.rollback(sp);
                    
                    // build response that gets passed back to the flow
                    SelfRegisterActionResponse resp = new SelfRegisterActionResponse();
                    resp.errorMessage = e.getMessage();
                    responses.add(resp);
                    
                    
                    
                }
                    
            }else{
                
                String isActive = 'inactive';
                
                if(userList[0].isActive) isActive = 'active';
                
                SelfRegisterActionResponse resp = new SelfRegisterActionResponse();
                resp.errorMessage = 'User already exists and is '+isActive;     
                responses.add(resp);
            }
        return responses;
    }
    
    /*
    Data structure for the request made from the flow to this invocable action
    */
    public class SelfRegisterActionRequest {

        @InvocableVariable(required=true)
        public String first_name;

        @InvocableVariable(required=true)
        public String last_name;

        @InvocableVariable(required=true)
        public String company_name;

        @InvocableVariable(required=true)
        public String email_address;
        
        @InvocableVariable(required=true)
        public String username;
        
        @InvocableVariable(required=false)
        public String phone;

        @InvocableVariable(required=true)
        public String street_address;

        @InvocableVariable(required=true)
        public String city_address;

        @InvocableVariable(required=true)
        public String state_address;

        @InvocableVariable(required=true)
        public String zip_address;

        @InvocableVariable(required=true)
        public String country_address;
        
        
    }

    /*
        Data structure for the response passed back from this invocable action to the calling flow
    */
    public class SelfRegisterActionResponse {

        public Account account;

        @InvocableVariable
        public String userId;

        @InvocableVariable
        public String accountId;

        @InvocableVariable
        public String contactId;
        
        @InvocableVariable
        public String errorMessage;
    }
}