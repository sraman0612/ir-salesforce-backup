public with sharing class PT_DSMP_AP03_UpdateAccountObjectives {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Update Account objectives
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 	

	public static void updateRecords(List<PT_DSMP_Shared_Objectives__c> lstSharedObj, Set<String> setSharedKey){
		Map<String, List<PT_DSMP_Objectives__c>> mapSharedKey = new Map<String, List<PT_DSMP_Objectives__c>>();
		List<PT_DSMP_Objectives__c> lstObjToUpdate = new List<PT_DSMP_Objectives__c>();

		for(PT_DSMP_Objectives__c current : [SELECT Id, Tech_SharedKey__c
											 FROM PT_DSMP_Objectives__c
											 WHERE Tech_SharedKey__c IN: setSharedKey AND
											 	   PT_DSMP_Shared_Objectives__c = null
											]){

			System.debug('mgr mapSharedKey current ' + current.Tech_SharedKey__c);


			if(mapSharedKey.containsKey(current.Tech_SharedKey__c)){
				mapSharedKey.get(current.Tech_SharedKey__c).add(current);
			} else {
				mapSharedKey.put(current.Tech_SharedKey__c, new List<PT_DSMP_Objectives__c>{current});
			}

		}

		System.debug('mgr mapSharedKey ' + mapSharedKey);


		//lstObjToUpdate
		for(PT_DSMP_Shared_Objectives__c current : lstSharedObj){

			if(mapSharedKey.containsKey(current.Ext_ID__c))
				for(PT_DSMP_Objectives__c currentDSMPObj : mapSharedKey.get(current.Ext_ID__c)){
					lstObjToUpdate.add(new PT_DSMP_Objectives__c(
						Id = currentDSMPObj.Id,
						PT_DSMP_Shared_Objectives__c = current.Id,
						Objective__c = current.Text__c
					));
				}

		}

		System.debug('mgr lstObjToUpdate ' + lstObjToUpdate);

		if(lstObjToUpdate.size() >0)
			update lstObjToUpdate;


	}


}