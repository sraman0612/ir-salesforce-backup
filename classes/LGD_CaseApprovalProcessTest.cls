@IsTest
private class LGD_CaseApprovalProcessTest {
    @IsTest
    public static void unitTest1(){
        User u = CTS_TestUtility.createUser(true);
        Case c = new Case();
        c.subject = 'subject test'; 
        c.OwnerId = u.Id;
        c.Status = 'New';
        c.GDI_Department__c = u.Department__c;
        c.Brand__c = u.Brand__c;
        c.Business__c='Low Pressure';
        c.Product_Category__c = u.Default_Product_Category__c;
        c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
        c.Case_Accepted__c = false;
        c.SuppliedEmail = 'johnH@gmail.com';
        c.List_Price__c=4500;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
        insert c;
        List<Id> addId = new List<Id>();
        addId.add(c.Id);
        System.runAs(u){
            
            test.startTest();
            LGD_CaseApprovalProcess.updateApprover(addId);
            test.stopTest();
        }
    }
     @IsTest
    public static void unitTest2(){
        User u = CTS_TestUtility.createUser(true);
        
        Case c = new Case();
        c.subject = 'subject test'; 
        c.OwnerId = u.Id;
        c.Status = 'New';
        c.GDI_Department__c = u.Department__c;
        c.Brand__c = u.Brand__c;
        c.Business__c='Low Pressure';
        c.Product_Category__c = u.Default_Product_Category__c;
        c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
        c.Case_Accepted__c = false;
        c.SuppliedEmail = 'johnH@gmail.com';
        c.List_Price__c=55000;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
        insert c;
        List<Id> addId = new List<Id>();
        addId.add(c.Id);
        System.runAs(u){
            
            test.startTest();
            LGD_CaseApprovalProcess.updateApprover(addId);
            test.stopTest();
        }
    }
     @IsTest
    public static void unitTest3(){
        User u = CTS_TestUtility.createUser(true);
        
        Case c = new Case();
        c.subject = 'subject test'; 
        c.OwnerId = u.Id;
        c.Status = 'New';
        c.GDI_Department__c = u.Department__c;
        c.Brand__c = u.Brand__c;
        c.Business__c='Low Pressure';
        c.Product_Category__c = u.Default_Product_Category__c;
        c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
        c.Case_Accepted__c = false;
        c.SuppliedEmail = 'johnH@gmail.com';
        c.List_Price__c=350000;
        c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
        insert c;
        List<Id> addId = new List<Id>();
        addId.add(c.Id);
        System.runAs(u){
            
            test.startTest();
            LGD_CaseApprovalProcess.updateApprover(addId);
            test.stopTest();
        }
    }
}