/**
* Assign topics to Knowledge articles
* 
**/
public class CTS_TechDirect_AssignTopicsToArticles {
    @InvocableMethod(label='Assign Topics' description='Assign Topics to Published Articles')
    public static void assignTopicsToPublishArticles(List<Id> knowledgeIds){
        try{
            //get Knowledge Article record
            List<Knowledge__kav> knowledgeList = 
                [Select Id, CTS_TechDirect_Article_Type__c, IsVisibleInPkb  
                 From Knowledge__kav 
                 Where Id IN: knowledgeIds];
            
            Knowledge__kav knowledgeVar = knowledgeList.get(0);
            
            //get Topic assigned to Knowledge Article
            List<TopicAssignment> topicAssignmntList = 
                [SELECT Id, EntityId, TopicId 
                 FROM TopicAssignment 
                 Where EntityId =: knowledgeIds AND TopicId != null];
            
            //if there is already topic assigned, delete it
            if(topicAssignmntList != null && topicAssignmntList.size() > 0 ){
                delete topicAssignmntList;
            } 
            
            assignTopicInAuthCommunity(knowledgeVar);
            
            if(knowledgeVar.IsVisibleInPkb){
                assignTopicInPublicCommunity(knowledgeVar);
            }
            
        } catch(Exception ex){
            System.debug('##Exception occured while assigning topics to article: ' + ex.getMessage() + '\n in class \'CTS_TechDirect_AssignTopicsToArticles\' at line# '+ex.getLineNumber());
        }
        
    }
    
    //assign Topics in Authenticated community
    private static void assignTopicInAuthCommunity(Knowledge__kav knowledgeVar){
        //get authenticated community to get related Topic from that community
        Network authCommunity = [SELECT Id, Name 
                                 FROM Network 
                                 WHERE UrlPathPrefix =: Label.CTS_TechDirect_Community_Path_Name Limit 1];
        
        //create new Topic Assignment to assign it to Knowledge Article
        TopicAssignment topicAssignmnt = new TopicAssignment();
        topicAssignmnt.EntityId = knowledgeVar.Id;
        
        Topic topicVar = null;
        if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Tech Flash'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Tech Flashes' AND NetworkId =: authCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Tech Tube'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Tech Tubes' AND NetworkId =: authCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'FSB'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Field Service Bulletins' AND NetworkId =: authCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Manuals'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Manual' AND NetworkId =: authCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        }
        
        insert topicAssignmnt;
    }
    
    //assign Topics in Public community
    private static void assignTopicInPublicCommunity(Knowledge__kav knowledgeVar){
        //get public community to get related Topic from that community
        Network publicCommunity = [SELECT Id, Name 
                                   FROM Network 
                                   WHERE UrlPathPrefix =: Label.CTS_TechDirect_Public_Community_Path_Name Limit 1];
        
        //create new Topic Assignment to assign it to Knowledge Article
        TopicAssignment topicAssignmnt = new TopicAssignment();
        topicAssignmnt.EntityId = knowledgeVar.Id;
        
        Topic topicVar = null;
        if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Tech Flash'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Tech Flashes' AND NetworkId =: publicCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Tech Tube'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Tech Tubes' AND NetworkId =: publicCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'FSB'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Field Service Bulletins' AND NetworkId =: publicCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        } else if(knowledgeVar.CTS_TechDirect_Article_Type__c == 'Manuals'){
            topicVar = [Select Id, Name From Topic 
                        Where Name = 'Latest Manual' AND NetworkId =: publicCommunity.Id Limit 1];
            
            topicAssignmnt.TopicId = topicVar.Id;
        }
        
        insert topicAssignmnt;
    }
}