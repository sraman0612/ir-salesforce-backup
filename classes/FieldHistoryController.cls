public class FieldHistoryController {
  @AuraEnabled
  public static List<FieldHistoryWrapper> getHistoryTable(String sObjName, Id parentRecordId){
      String query = '';
      String parentField = 'ParentId';
      List<FieldHistoryWrapper> fieldHistories = new List<FieldHistoryWrapper>();
      if(sObjName.endsWithIgnoreCase('__c')){
          sObjName = sObjName.removeEndIgnoreCase('__c') + '__History';
      }else{
          parentField = sObjName +'Id';
          if(sObjName=='Opportunity'){
            sObjName = sObjName + 'FieldHistory';
          } else {
            sObjName = sObjName + 'History';
          }
      }
      query += 'SELECT Id, ' + parentField + ', CreatedBy.Name, CreatedDate, Field, NewValue, OldValue ';
      query += 'FROM ' + sObjName + ' ';
      query += 'WHERE ' + parentField + ' = :parentRecordId';
      
      List<sObject> objRecordHistory = Database.query(query);
      for(sObject r: objRecordHistory){
          Id idVal = (Id)r.get('Id');
          Datetime createddtVal = (Datetime)r.get('CreatedDate');
          /* below null check done for test purposes */
          String createdNameVal = null != r.getSobject('CreatedBy') ? (String)r.getSobject('CreatedBy').get('Name') : '';
          String fieldVal = (String)r.get('Field');
          String oldVal = String.valueOf(r.get('OldValue'));
          String newVal = String.valueOf(r.get('NewValue'));
          FieldHistoryWrapper objHistory = new FieldHistoryWrapper(idVal,createddtVal,createdNameVal,fieldVal,oldVal,newVal);
          fieldHistories.add(objHistory);
      }
      //Commented as part of EMEIA Cleanup.
     /* for(CC_GPSI_Field_History__c customFieldHistory : [SELECT Id, CreatedDate, CreatedBy.Name, Field__c, Old_Value__c, New_Value__c 
                                                           FROM CC_GPSI_Field_History__c 
                                                          WHERE Parent_ID__c =: parentRecordId]){
          FieldHistoryWrapper customObjHistory = new FieldHistoryWrapper(customFieldHistory.ID,customFieldHistory.CreatedDate,
                                                                      customFieldHistory.CreatedBy.Name,customFieldHistory.Field__c,
                                                                      customFieldHistory.Old_Value__c,customFieldHistory.New_Value__c);
          fieldHistories.add(customObjHistory);
      }*/
      fieldHistories.sort();
      return fieldHistories;
  }
}