public without sharing class PT_DSMP_AP01_CountTasksRecords {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Count Number of Tasks (Open, Close, Due)
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 03-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	public static void countTaskRecords(Set<String> setAccountObjectiveId){
		Map<Id, PT_DSMP_Objectives__c> mapAccountObjectiveToUpdate = new Map<Id, PT_DSMP_Objectives__c>();

		for(String current : setAccountObjectiveId){
			mapAccountObjectiveToUpdate.put(current, new PT_DSMP_Objectives__c(Id = current,
																			   Due_Tasks__c = 0,
																			   Open_Tasks__c = 0,
																			   Complete_Tasks__c = 0));
		}

		// Open/Closed tasks
		AggregateResult[] groupedResults = new List<AggregateResult>([
			SELECT count(id),
				   isclosed,
				   PT_DSMP_Account_Objectives__c
			FROM Task
			WHERE PT_DSMP_Account_Objectives__c in: setAccountObjectiveId AND
				  ((isDueTask__c = false AND isclosed = false) OR isclosed = true)
			group by PT_DSMP_Account_Objectives__c, isclosed
		]);

        for (AggregateResult ar : groupedResults)  {

        	if(ar.get('IsClosed').equals(false)){
                mapAccountObjectiveToUpdate.get((id)ar.get('PT_DSMP_Account_Objectives__c')).Open_Tasks__c = integer.valueOf(ar.get('expr0'));
            }

            if(ar.get('IsClosed').equals(true)){
                mapAccountObjectiveToUpdate.get((id)ar.get('PT_DSMP_Account_Objectives__c')).Complete_Tasks__c = integer.valueOf(ar.get('expr0'));
            }
            
		}

		// Due Tasks
		AggregateResult[] groupedResultsDueTasks = new List<AggregateResult>([
			SELECT count(id), isDueTask__c, PT_DSMP_Account_Objectives__c 
			FROM Task 
			WHERE isDueTask__c = true AND PT_DSMP_Account_Objectives__c in: setAccountObjectiveId
			Group by isDueTask__c, PT_DSMP_Account_Objectives__c
		]);

		for (AggregateResult ar : groupedResultsDueTasks)  {

            mapAccountObjectiveToUpdate.get((id)ar.get('PT_DSMP_Account_Objectives__c')).Due_Tasks__c = integer.valueOf(ar.get('expr0'));
            
		}


		System.debug('mapAccountObjectiveToUpdate ' + mapAccountObjectiveToUpdate);

		if (mapAccountObjectiveToUpdate.size() > 0){
            update mapAccountObjectiveToUpdate.values();
        }

	}

}