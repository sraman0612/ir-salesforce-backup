public inherited sharing class CommonUtility {
    public static String getConnectApiError(String jsonErrorMsg) {
        // Deserialize JSON into a map
        Map<String, Object> parsedJson = (Map<String, Object>) JSON.deserializeUntyped(jsonErrorMsg);
        System.debug(JSON.serialize(parsedJson));

        // Navigate through the JSON to get the error message
        Map<String, Object> output = (Map<String, Object>) parsedJson.get('output');
        Map<String, Object> error = (Map<String, Object>) output.get('error');

        // Extract the error message
        String errorMessage = (String) error.get('message');
        System.debug(errorMessage);
        return errorMessage;
    }
}