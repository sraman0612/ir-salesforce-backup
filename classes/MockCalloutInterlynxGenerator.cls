@isTest
global class MockCalloutInterlynxGenerator implements HttpCalloutMock {
    global HttpResponse mockResponse;
    
    global MockCalloutInterlynxGenerator(HttpResponse mockResponse){
        this.mockResponse = mockResponse;
    }
    
	global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        
        if(this.mockResponse == null){
			// Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}');
            res.setStatusCode(200);
            return res;
        }
        else{
			return this.mockResponse;
        }
        
    }
}