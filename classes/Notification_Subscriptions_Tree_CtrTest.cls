@isTest
public with sharing class Notification_Subscriptions_Tree_CtrTest {

    @TestSetup
    private static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account acc1 = CTS_TestUtility.createAccount('Test Account1', false);
        Account acc2 = CTS_TestUtility.createAccount('Test Account2', false);

        insert new Account[]{acc1, acc2};

        Contact communityContact1 = CTS_TestUtility.createContact('Contact123','Test', 'testct1@gmail.com', acc1.Id, false);
        communityContact1.CTS_IOT_Community_Status__c = 'Approved';
        communityContact1.MobilePhone = '555-555-5555';
        
        Contact communityContact2 = CTS_TestUtility.createContact('Contact456','Test', 'testcts2@gmail.com', acc2.Id, false);
        communityContact2.CTS_IOT_Community_Status__c = 'Approved';
        communityContact2.MobilePhone = '555-555-5555';        
        
        insert new Contact[]{communityContact1, communityContact2};

        Id profileId = [SELECT Id FROM Profile WHERE Name = :settings.Default_Standard_User_Profile_Name__c Limit 1].Id;

        User communityUser1 = CTS_TestUtility.createUser(communityContact1.Id, profileId, false);    
        communityUser1.LastName = 'Comm_User_Test1';

        User communityUser2 = CTS_TestUtility.createUser(communityContact2.Id, profileId, false);    
        communityUser2.LastName = 'Comm_User_Test2';        
        
        insert new User[]{communityUser1, communityUser2};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('CTS_Global_Asset').getRecordTypeId();
        
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc1.Id, assetRecTypeId, false); 
        asset1.IRIT_RMS_Flag__c = false; 

        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', acc1.Id, assetRecTypeId, false); 
        asset2.IRIT_RMS_Flag__c = true; 

        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '87550', acc2.Id, assetRecTypeId, false); 
        asset3.IRIT_RMS_Flag__c = true;          

        insert new Asset[]{asset1, asset2, asset3};

        Notification_Type__c notificationType1 = NotificationsTestDataService.createNotificationType(false, 'Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test');

        Notification_Type__c notificationType2 = NotificationsTestDataService.createNotificationType(false, 'Trip Test', false, 'Trip',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test'); 
            
        insert new Notification_Type__c[]{notificationType1, notificationType2};       
    }
}    
    @isTest
    private static void testInitGlobal(){

        Account[] accts = [Select Id, Name, Account_Name_Displayed__c From Account Order By Name];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Global subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, null, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser1){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(true, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());
            system.assert(response.sites[0].label.startsWith(accts[0].Name));
            system.assertEquals('success', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(accts[0].Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(accts[0].Id, response.sites[0].value);
            system.assertEquals(true, response.sites[0].subscribed);
            system.assertEquals(false, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle); 
            
            system.assertEquals(2, response.sites[0].assets.size());

            system.assertEquals(assets[0].Asset_Name_Displayed__c, response.sites[0].assets[0].label);
            system.assertEquals(assets[0].Id, response.sites[0].assets[0].value);
            system.assertEquals(false, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[0].subscribed);
            system.assertEquals(false, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle); 
            
            system.assertEquals(assets[1].Asset_Name_Displayed__c, response.sites[0].assets[1].label);
            system.assertEquals(assets[1].Id, response.sites[0].assets[1].value);
            system.assertEquals(true, response.sites[0].assets[1].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[1].subscribed);
            system.assertEquals(false, response.sites[0].assets[1].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[1].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].detailsLinkTitle);                
        }

        Test.stopTest();
    }   

    @isTest
    private static void testInitGlobalWithSiteOptOut(){

        User communityUser1 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test2'];
        Account acct = [Select Id, Name, Account_Name_Displayed__c From Account Where Id = :communityUser1.AccountId];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];


        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Global subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, null, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, null, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      

        // Site opt out
        Notification_Subscription__c subscription3 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, acct.Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');    
        subscription3.Opt_Out__c = true;            
            
        insert new Notification_Subscription__c[]{subscription1, subscription2, subscription3};

        Test.startTest();

        System.runAs(communityUser1){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(true, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());

            system.assert(response.sites[0].label.startsWith(acct.Name));
            system.assertEquals('', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(acct.Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(acct.Id, response.sites[0].value);
            system.assertEquals(false, response.sites[0].subscribed);
            system.assertEquals(true, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle);   
            
            system.assertEquals(2, response.sites[0].assets.size());

            system.assertEquals(assets[0].Asset_Name_Displayed__c, response.sites[0].assets[0].label);
            system.assertEquals(assets[0].Id, response.sites[0].assets[0].value);
            system.assertEquals(false, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(false, response.sites[0].assets[0].subscribed);
            system.assertEquals(true, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle); 
            
            system.assertEquals(assets[1].Asset_Name_Displayed__c, response.sites[0].assets[1].label);
            system.assertEquals(assets[1].Id, response.sites[0].assets[1].value);
            system.assertEquals(true, response.sites[0].assets[1].subscriptionEnabled);
            system.assertEquals(false, response.sites[0].assets[1].subscribed);
            system.assertEquals(true, response.sites[0].assets[1].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[1].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].detailsLinkTitle);              
        }

        Test.stopTest();
    }     
    
    @isTest
    private static void testInitSite(){

        Account[] accts = [Select Id, Name, Account_Name_Displayed__c From Account Order By Name];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Site subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, communityUser1.AccountId, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, communityUser2.AccountId, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser1){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(false, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());

            system.assert(response.sites[0].label.startsWith(accts[0].Name));
            system.assertEquals('success', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(accts[0].Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(accts[0].Id, response.sites[0].value);
            system.assertEquals(true, response.sites[0].subscribed);
            system.assertEquals(false, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle);  
            
            system.assertEquals(2, response.sites[0].assets.size());

            system.assertEquals(assets[0].Asset_Name_Displayed__c, response.sites[0].assets[0].label);
            system.assertEquals(assets[0].Id, response.sites[0].assets[0].value);
            system.assertEquals(false, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[0].subscribed);
            system.assertEquals(false, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle);    
            
            system.assertEquals(assets[1].Asset_Name_Displayed__c, response.sites[0].assets[1].label);
            system.assertEquals(assets[1].Id, response.sites[0].assets[1].value);
            system.assertEquals(true, response.sites[0].assets[1].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[1].subscribed);
            system.assertEquals(false, response.sites[0].assets[1].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[1].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].detailsLinkTitle);              
        }

        Test.stopTest();
    }  
    
    @isTest
    private static void testInitSiteHideHelixDisabled(){

        Account[] accts = [Select Id, Name, Account_Name_Displayed__c From Account Order By Name];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Site subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, communityUser1.AccountId, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, communityUser2.AccountId, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser1){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(false);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(false, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());

            system.assert(response.sites[0].label.startsWith(accts[0].Name));
            system.assertEquals('success', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(accts[0].Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(accts[0].Id, response.sites[0].value);
            system.assertEquals(true, response.sites[0].subscribed);
            system.assertEquals(false, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle);  
            
            system.assertEquals(1, response.sites[0].assets.size());

            system.assertEquals(assets[1].Asset_Name_Displayed__c, response.sites[0].assets[0].label);
            system.assertEquals(assets[1].Id, response.sites[0].assets[0].value);
            system.assertEquals(true, response.sites[0].assets[0].subscribed);
            system.assertEquals(false, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle);              
        }

        Test.stopTest();
    }     

    @isTest
    private static void testInitSiteWithAssetOptOut(){

        Account[] accts = [Select Id, Name, Account_Name_Displayed__c From Account Order By Name];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id, AccountId From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Site subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, communityUser1.AccountId, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, communityUser2.AccountId, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        // Asset opt out
        Notification_Subscription__c subscription3 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');    
        subscription3.Opt_Out__c = true;    

        insert new Notification_Subscription__c[]{subscription1, subscription2, subscription3};

        Test.startTest();

        System.runAs(communityUser1){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(false, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());

            system.assert(response.sites[0].label.startsWith(accts[0].Name));
            system.assertEquals('success', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(accts[0].Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(accts[0].Id, response.sites[0].value);
            system.assertEquals(true, response.sites[0].subscribed);
            system.assertEquals(false, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle);  
            
            system.assertEquals(2, response.sites[0].assets.size());

            system.assertEquals(assets[0].Asset_Name_Displayed__c, response.sites[0].assets[0].label);
            system.assertEquals(assets[0].Id, response.sites[0].assets[0].value);
            system.assertEquals(false, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(false, response.sites[0].assets[0].subscribed);
            system.assertEquals(true, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle);    
            
            system.assertEquals(assets[1].Asset_Name_Displayed__c, response.sites[0].assets[1].label);
            system.assertEquals(assets[1].Id, response.sites[0].assets[1].value);
            system.assertEquals(true, response.sites[0].assets[1].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[1].subscribed);
            system.assertEquals(false, response.sites[0].assets[1].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[1].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[1].label, response.sites[0].assets[1].detailsLinkTitle);              
        }

        Test.stopTest();
    }       
    
    @isTest
    private static void testInitAssetSuccess(){

        Account[] accts = [Select Id, Name, Account_Name_Displayed__c From Account Order By Name];
        Asset[] assets = [Select Id, Name, Asset_Name_Displayed__c, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Asset subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, assets[2].Id, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        System.runAs(communityUser2){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(true, response.success);
            system.assertEquals('', response.errorMessage);

            system.assertEquals(false, response.hasGlobalSubscriptions);
            system.assertEquals(1, response.sites.size());

            system.assert(response.sites[0].label.startsWith(accts[1].Name));
            system.assertEquals('', response.sites[0].variant);
            system.assertEquals(false, response.sites[0].expanded);
            system.assertEquals(true, response.sites[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].getHasAssets());
            system.assertEquals(accts[1].Account_Name_Displayed__c, response.sites[0].label);
            system.assertEquals(accts[1].Id, response.sites[0].value);
            system.assertEquals(false, response.sites[0].subscribed);
            system.assertEquals(false, response.sites[0].fullOptOut);
            system.assertEquals(false, response.sites[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].label, response.sites[0].manageIconTitle);
            system.assertEquals('View Site Details For ' + response.sites[0].label, response.sites[0].detailsLinkTitle);   
            
            system.assertEquals(1, response.sites[0].assets.size());
            system.assertEquals(true, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(assets[2].Asset_Name_Displayed__c, response.sites[0].assets[0].label);            
            system.assertEquals(assets[2].Id, response.sites[0].assets[0].value);
            system.assertEquals(true, response.sites[0].assets[0].subscriptionEnabled);
            system.assertEquals(true, response.sites[0].assets[0].subscribed);
            system.assertEquals(false, response.sites[0].assets[0].fullOptOut);
            system.assertEquals(false, response.sites[0].assets[0].partialOptOut);
            system.assertEquals('Manage Subscriptions For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].manageIconTitle);
            system.assertEquals('View Asset Details For ' + response.sites[0].assets[0].label, response.sites[0].assets[0].detailsLinkTitle); 
        }

        Test.stopTest();
    }   
    
    @isTest
    private static void testInitAssetFailure(){

        Account[] accts = [Select Id, Name From Account Order By Name];
        Asset[] assets = [Select Id, Name, AccountId From Asset Order By Name ASC];
        User communityUser1 = [Select Id From User Where LastName = 'Comm_User_Test1'];
        User communityUser2 = [Select Id From User Where LastName = 'Comm_User_Test2'];
        Notification_Type__c[] notificationTypes = [Select Id From Notification_Type__c Order By Triggering_Field_Value__c];

        // Asset subscriptions
        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser1.Id, assets[0].Id, notificationTypes[0].Id,
            'Daily;Real-Time', 'Email;SMS');          
            
        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, communityUser2.Id, assets[1].Id, notificationTypes[1].Id,
            'Daily;Real-Time', 'Email;SMS');      
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};

        Test.startTest();

        // Force error
        Notification_Subscriptions_Tree_View_Ctr.forceError = true;

        System.runAs(communityUser2){
            
            Notification_Subscriptions_Tree_View_Ctr.InitResponse response = Notification_Subscriptions_Tree_View_Ctr.getInit(true);
            system.assertEquals(false, response.success);
            system.assert(String.isNotBlank(response.errorMessage));

            system.assertEquals(false, response.hasGlobalSubscriptions);
            system.assertEquals(0, response.sites.size());
        }

        Test.stopTest();
    }     
}