/**
 * Created by wenboliu on 18/11/2024.
 */

public with sharing class  B2BLandingPageController {
    public class LandingPageConfigurationWrapper {
        @AuraEnabled public String label_en;
        @AuraEnabled public String label_fr;
        @AuraEnabled public String label_it;
        @AuraEnabled public Integer order;
        @AuraEnabled public String value;

        public LandingPageConfigurationWrapper(String label_en, String label_fr, String label_it, Integer order, String value) {
            this.label_en = label_en;
            this.label_fr = label_fr;
            this.label_it = label_it;
            this.order = order;
            this.value = value;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<LandingPageConfigurationWrapper> getLandingPageConfiguration() {
        try {
            Map<String, B2B_Landing_Page_Configuration__mdt> recordsByDeveloperName = B2B_Landing_Page_Configuration__mdt.getAll();
            List<LandingPageConfigurationWrapper> sortedRecords = new List<LandingPageConfigurationWrapper>();
            for (String developerName : recordsByDeveloperName.keySet()) {
                B2B_Landing_Page_Configuration__mdt metadataRecord = recordsByDeveloperName.get(developerName);
                sortedRecords.add(new LandingPageConfigurationWrapper(
                        metadataRecord.Label_en__c,
                        metadataRecord.Label_fr__c,
                        metadataRecord.Label_it__c,
                        Integer.valueOf(metadataRecord.Order__c),
                        metadataRecord.Value__c
                ));
            }
            return sortedRecords;
        } catch (Exception ex) {
            throw ex;
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<ProductCategory> getCategoryId(String webstoreId, String categoryName) {
        SharingHelper helper = new SharingHelper();
        return helper.getCategoryId(webstoreId, categoryName);
    }

    public without sharing class SharingHelper {
        public  List<ProductCategory> getCategoryId(String webstoreId, String categoryName){
            try{
                List<ProductCategory> category = [SELECT Id, Name FROM ProductCategory WHERE Name = :categoryName
                AND CatalogId IN (SELECT ProductCatalogId FROM WebStoreCatalog
                WHERE SalesStoreId = :webstoreId) LIMIT 1];
                return category;
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}