//
// (c) 2015, Appirio Inc
//
// Test class for Parent Account Extension
//
// Created By : Surabhi Sharma(Appirio)
//
@isTest
private class ParentAccountExtension_Test {

    static testMethod void testParentAccountExtensions() {
        Account acc = createTestData('testParent6756343', 'sr_myCity', 'USA', '56384656364', 'CA', '13tyr, sr_myteststreet', 'city2', 'USA', 'CA'
                                       , '14, test Street1', '1234', 'testCounty');
        Account child = createTestData('testChildTrue', 'child_sr_1city3', 'USA', '458634753', 'CA', '15fghg, ty_test pny_street2', 'city4', 'USA', 'CA'
                                       , '16, test Street6', '12348', 'testCounty1');
        child.CTS_Global_Shipping_Address_1__c = '13';
        child.CTS_Global_Shipping_Address_2__c = 'street2';                                       
        insert child;
        System.currentPageReference().getParameters().put('childacc', child.Id);
        Apexpages.StandardController stdController = new Apexpages.StandardController(acc); 
        ParentAccountExtension parentAcc = new ParentAccountExtension(stdController);
        parentAcc.save();
        
        child = [SELECT parentId FROM Account WHERE id = :child.id];
        System.assertEquals(child.parentId,acc.Id);
    }
    
    private static Account createTestData(string name, string city, string country, string billCode, string billState, string billStreet,
                                          string shipcity, string shipCountry, string shipState, string shipStreet, string shipCode, string county ){

        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'Site_Account_NA_Air' 
                                    and SobjectType='Account' limit 1].Id;
        Account acc = new Account();
        acc.Name = name;
        acc.BillingCity = city;
        acc.BillingCountry = country;
        acc.BillingPostalCode = billCode;
        acc.BillingState = billState;
        acc.BillingStreet = billStreet;
        acc.ShippingCity = shipcity;
        acc.ShippingCountry = shipCountry;
        acc.ShippingState = shipState;
        acc.ShippingStreet = shipStreet;
        acc.ShippingPostalCode = shipCode; 
        acc.County__c = county;  
        acc.recordtypeid=AirNAAcctRT_ID;
        return acc;
    }
}