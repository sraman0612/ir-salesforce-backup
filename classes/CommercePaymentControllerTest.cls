@IsTest
private class CommercePaymentControllerTest {
    static PaymentAuthorization placeOrderTestSetup() {
        // Create necessary data records (Accounts, WebCart, OrderSummary, etc.)
        Account accountRecord = B2BTestDataFactory.createAccount('Customer');
        insert accountRecord;

        WebStore webStoreRecord = B2BTestDataFactory.createWebStore('Store');
        insert webStoreRecord;

        Contact contactRecord = B2BTestDataFactory.createContact(accountRecord.Id, 'First Name', 'Last Name');
        insert contactRecord;

        B2BTestDataFactory.setupStandardPricebook();

        Product2 productRecord1 = B2BTestDataFactory.createProduct('Product', '1234');
        productRecord1.Family = 'Chocolate';
        Product2 productRecord2 = B2BTestDataFactory.createProduct('Product', '4321');
        productRecord2.Family = 'Chocolate';
        Product2 productRecord3 = B2BTestDataFactory.createProduct('Product', 'abcde');
        productRecord3.Family = 'Chocolate';
        Product2 shippingProduct = B2BTestDataFactory.createProduct('Shipping', 'Shipping');
        insert new List<Product2>{productRecord1, productRecord2, productRecord3, shippingProduct};

        Order orderRecord = B2BTestDataFactory.createOrder(accountRecord.Id, 'Draft', 'CAD', Date.today());
        insert orderRecord;

        OrderDeliveryMethod odMethodRecord = B2BTestDataFactory.createOrderDeliveryMethod(shippingProduct.Id, 'Delivery Method');
        insert odMethodRecord;

        OrderDeliveryGroup odgRecord = B2BTestDataFactory.createOrderDeliveryGroup(orderRecord.Id, odMethodRecord.Id, 'Name', 'Street', 'Quebec', 'QC', 'Postal Code', 'CA', Date.today());
        insert odgRecord;

        OrderItem orderItem1 = B2BTestDataFactory.createOrderItem(orderRecord.Id, productRecord1.Id, 10.00, 100.00, 100.00);
        orderItem1.Type = 'Order Product';
        orderItem1.OrderDeliveryGroupId = odgRecord.Id;
        OrderItem orderItem2 = B2BTestDataFactory.createOrderItem(orderRecord.Id, productRecord2.Id, 10.00, 100.00, 100.00);
        orderItem2.OrderDeliveryGroupId = odgRecord.Id;
        orderItem2.Type = 'Order Product';
        insert new List<OrderItem>{orderItem1, orderItem2};

        orderRecord.Status = 'Activated';
        update orderRecord;

        ConnectApi.OrderSummaryInputRepresentation orderSummaryInputRepresentation = new ConnectApi.OrderSummaryInputRepresentation();
        orderSummaryInputRepresentation.orderId = orderRecord.Id;
        orderSummaryInputRepresentation.orderLifeCycleType = 'UNMANAGED';

        ConnectApi.OrderSummaryOutputRepresentation orderSummaryOutputRepresentation = ConnectApi.OrderSummaryCreation.createOrderSummary(orderSummaryInputRepresentation);
        OrderSummary orderSummary = new OrderSummary(
                Id = orderSummaryOutputRepresentation.orderSummaryId,
                SalesStoreId = webStoreRecord.Id,
                OrderedDate = Date.today(),
                CurrencyIsoCode = 'EUR'
        );
        update orderSummary;

        OrderPaymentSummary ops = B2BTestDataFactory.createOrderPaymentSummary(orderSummary.Id, 'Pending', 'CreditCard', 'EUR');
        insert ops;

        // Create test data for Payment
        Payment payment = B2BTestDataFactory.createPayment(accountRecord.Id, ops.Id, 100.00, 'Processed', 'Sale', 'External', 'EUR');
        insert payment;

        CardPaymentMethod cm = B2BTestDataFactory.createCardPaymentMethod(accountRecord.Id, 'Inactive', 'External', 'CreditCard');
        insert cm;

        PaymentAuthorization pa = B2BTestDataFactory.createPaymentAuthorization(accountRecord.Id, ops.Id, cm.Id, 100.00,'Pending','External','EUR');
        insert pa;

        Refund re = B2BTestDataFactory.createRefund(accountRecord.Id, ops.Id, payment.Id, 0.00, 'Draft', 'Referenced', 'External', 'EUR');
        insert re;

        return pa;
    }

    @IsTest(SeeAllData=true)
    static void testCapturePayment() {
        PaymentAuthorization paymentAuthorization = placeOrderTestSetup();
        Payment payment = [SELECT Id, Amount FROM Payment WHERE OrderPaymentSummaryId =: paymentAuthorization.OrderPaymentSummaryId LIMIT 1];

        // Inject the mock API
        CommercePaymentApiMock mockApi = new CommercePaymentApiMock(payment.Id);
        CommercePaymentController service = new CommercePaymentController(mockApi);


        // Call the method and verify the response
        Test.startTest();
        service.capturePayment(paymentAuthorization.Id);
        Test.stopTest();
        // Validate the update
        Payment updatePayment = [SELECT Id, Amount, OrderPaymentSummaryId FROM Payment WHERE Id =: payment.Id LIMIT 1];
        System.assertEquals(paymentAuthorization.OrderPaymentSummaryId, updatePayment.OrderPaymentSummaryId, 'The OrderPaymentSummaryId should match.');
    }

    @IsTest(SeeAllData=true)
    static void testRefundPayment() {
        PaymentAuthorization paymentAuthorization = placeOrderTestSetup();
        Payment payment = [SELECT Id, Amount, OrderPaymentSummaryId FROM Payment WHERE OrderPaymentSummaryId =: paymentAuthorization.OrderPaymentSummaryId LIMIT 1];
        Refund re = [SELECT Id, Amount, OrderPaymentSummaryId FROM Refund WHERE PaymentId =:payment.Id LIMIT 1];
        // Inject the mock API
        CommercePaymentApiMock mockApi = new CommercePaymentApiMock(re.Id);
        CommercePaymentController service = new CommercePaymentController(mockApi);

        // Call the method and verify the response
        Test.startTest();
        service.refundPayment(payment.Id);
        Test.stopTest();
        // Validate the update
        System.assertEquals(payment.OrderPaymentSummaryId, re.OrderPaymentSummaryId, 'The OrderPaymentSummaryId should match.');
    }


    @IsTest(SeeAllData=true)
    static void testReverseAuthorization() {
        PaymentAuthorization paymentAuthorization = placeOrderTestSetup();

        // Inject the mock API
        CommercePaymentApiMock mockApi = new CommercePaymentApiMock(paymentAuthorization.Id);
        CommercePaymentController service = new CommercePaymentController(mockApi);

        // Call the method and verify the response
        Test.startTest();
        service.reverseAuthorization(paymentAuthorization.Id);
        Test.stopTest();
        System.assertEquals(paymentAuthorization.Amount, [SELECT Amount FROM PaymentAuthorization WHERE Id =: paymentAuthorization.Id LIMIT 1].Amount, 'The Amount should match.');
    }

    @IsTest(SeeAllData=true)
    static void testReverseAuthorizationFromOrderSummary() {
        PaymentAuthorization paymentAuthorization = placeOrderTestSetup();
        OrderPaymentSummary op = [SELECT Id, OrderSummaryId FROM OrderPaymentSummary WHERE Id =:paymentAuthorization.OrderPaymentSummaryId LIMIT 1];
        // Inject the mock API
        CommercePaymentApiMock mockApi = new CommercePaymentApiMock(paymentAuthorization.Id);
        CommercePaymentController service = new CommercePaymentController(mockApi);

        // Call the method and verify the response
        Test.startTest();
        service.reverseAuthorizationFromOrderSummary(op.OrderSummaryId);
        Test.stopTest();
        System.assertEquals(paymentAuthorization.Amount, [SELECT Amount FROM PaymentAuthorization WHERE OrderPaymentSummaryId =: op.Id LIMIT 1].Amount, 'The Amount should match.');
    }

    @IsTest(SeeAllData=true)
    static void testRefundPaymentWithInputs() {
        PaymentAuthorization paymentAuthorization = placeOrderTestSetup();
        Payment payment = [SELECT Id, Amount, OrderPaymentSummaryId FROM Payment WHERE OrderPaymentSummaryId =: paymentAuthorization.OrderPaymentSummaryId LIMIT 1];
        Refund re = [SELECT Id, Amount, OrderPaymentSummaryId FROM Refund WHERE PaymentId =:payment.Id LIMIT 1];
        // Inject the mock API
        CommercePaymentApiMock mockApi = new CommercePaymentApiMock(re.Id);
        CommercePaymentController service = new CommercePaymentController(mockApi);

        // Call the method and verify the response
        Test.startTest();
        service.refundPaymentWithInputs(payment.Id, 100.00, 'VALID_COMMENTS');
        Test.stopTest();
        // Validate the update
        System.assertEquals(payment.OrderPaymentSummaryId, re.OrderPaymentSummaryId, 'The OrderPaymentSummaryId should match.');
    }
}