/***********************************************************************
 Class          : ServiceAgreementTriggerHandler_Test 
 Created Date   : 1 July 2015
 Descritption   : Test Class for ServiceAgreementTriggerHandler
 Modified By    : Surabhi Sharm(6 Aug'15)
 ************************************************************************/
@isTest
public class ServiceAgreementTriggerHandler_Test {

    @isTest
    public static void createOpportunityTrue(){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_OEM_EU').getRecordTypeId();//IR Comp OEM EU
        Account acct = new Account(recordtypeid=devRecordTypeId,Name = 'test1111', Phone = '213123421',ShippingStreet = 'dfg', ShippingState='CA', ShippingCountry = 'USA', ShippingCity = 'TY', ShippingpostalCode = '45', County__c = 'testCounty');
        acct.CTS_Global_Shipping_Address_1__c = '13';
        acct.CTS_Global_Shipping_Address_2__c = 'street2';        
        insert acct;
        List<Service_Agreement__c> newList = new List<Service_Agreement__c>();
        Service_Agreement__c newAgreement = new Service_Agreement__c();
        newAgreement.Name = 'test300615_1';
        
        newAgreement.Account__c = acct.Id;
        newAgreement.Agreement_Number__c = 'testNum123';
        newAgreement.Agreement_Start_Date__c = system.now();
        newAgreement.Agreement_End_Date__c = system.now().addDays(90);
        newList.add(newAgreement);
        insert newList;
        
        newAgreement.X90DaysPriorEndDate__c = true;
        update newList;
        
        //ServiceAgreementTriggerHandler.afterUpdate(newList);
        Test.startTest();
        List<Opportunity> opp = [SELECT Name FROM Opportunity where Name =: 'test300615_1'];
        System.debug(opp);
        system.assertEquals('test300615_1', [SELECT Name FROM Opportunity where Name =: 'test300615_1'].Name);
        Test.stopTest();
    }
    
    @isTest
    public static void createOpportunityFalse(){
        try{
            Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_OEM_EU').getRecordTypeId();//IR Comp OEM EU
            Account acct = new Account(recordtypeid=devRecordTypeId,Name = 'test2222', Phone = '213123421',ShippingStreet = 'dfg', ShippingState='CA', ShippingCountry = 'USA', ShippingCity = 'TY', ShippingpostalCode = '45', County__c = 'testCounty');
            acct.CTS_Global_Shipping_Address_1__c = '13';
            acct.CTS_Global_Shipping_Address_2__c = 'street2';            
            insert acct;
            List<Service_Agreement__c> newList = new List<Service_Agreement__c>();
            Service_Agreement__c newAgreement = new Service_Agreement__c();
            newAgreement.Name = 'test300615_21';
            
            newAgreement.Account__c = acct.Id;
            newAgreement.Agreement_Number__c = 'testNum1234';
            newAgreement.Agreement_Start_Date__c = system.now();
            newAgreement.Agreement_End_Date__c = system.now().addDays(90);
            newList.add(newAgreement);
            insert newList;
            
             newAgreement.X90DaysPriorEndDate__c = false;
             update newList;
             
            //ServiceAgreementTriggerHandler.afterUpdate(newList);
            Test.startTest();
            List<Opportunity> opp = [SELECT Name FROM Opportunity where Name =: 'test300615_21'];
            System.debug(opp);
            system.assertEquals('test300615_21', [SELECT Name FROM Opportunity where Name =: 'test300615_21'].Name);
            Test.stopTest();
        }catch(QueryException e){
            System.debug(e);
        }
        
    }
}