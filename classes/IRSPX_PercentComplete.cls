// IRSPX_PercentComplete - This class calculates the percentage of IRSPX object fields (ie Account Plan, Opportunity Win Plan, and Opportunity Sales Call Plan) 
// that have been populated by the user. This percentage is used in data quality reporting for IRSPX.
// 
// Created on April 5, 2019 by Ronnie Stegall
// Updated on August 21, 2019 by Ronnie Stegall to fix "Attempt to de-reference a null object" bug if record type isn't set on user's profile
public class IRSPX_PercentComplete {
    
    private static Map<String, IRSPX_Page_Layout_Definitions__mdt[]> recordtypeLayouts = new Map<String, IRSPX_Page_Layout_Definitions__mdt[]>();
    
    private static String getRecordTypeDeveloperName(sObject IRSPXObject){
        Map<Id,Schema.RecordTypeInfo> recordTypeInfo;
        Id recordTypeId;
        AccountPlanTypeInfo AccountPlanType;
        OpportunityWinPlanTypeInfo opportunityWinPlanType;
        OpportunitySalesCallPlanTypeInfo opportunitySalesCallPlanType;
        String developerName = null;
        String sObjectName;
        
        recordTypeId = (Id)IRSPXObject.get('RecordTypeId');
        // If recordTypeId is null then user's profile doesn't have record type assigned
        if (recordTypeId != null){
            
            sObjectName = IRSPXObject.getSObjectType().getDescribe().getName();
       
            switch on sObjectName {
                when 'AccountPlan__c' {
                    AccountPlanType = AccountPlanTypeInfo.getInstance(); 
                    recordTypeInfo = AccountPlanType.recordTypeInfoById;
                    developerName = recordTypeInfo.get((Id)IRSPXObject.get('RecordTypeId')).getDeveloperName();
                }
                when 'OpportunityWinPlan__c' {
                    OpportunityWinPlanType = OpportunityWinPlanTypeInfo.getInstance();  
                    recordTypeInfo = OpportunityWinPlanType.recordTypeInfoById;
                    developerName = recordTypeInfo.get((Id)IRSPXObject.get('RecordTypeId')).getDeveloperName();
                }
                when 'Opportunity_Sales_Call_Plan__c' {
                    opportunitySalesCallPlanType = OpportunitySalesCallPlanTypeInfo.getInstance();  
                    recordTypeInfo = opportunitySalesCallPlanType.recordTypeInfoById;
                    developerName = recordTypeInfo.get((Id)IRSPXObject.get('RecordTypeId')).getDeveloperName();
                }
                when else {
                    developerName = null;
                }
            }
        }
        
        return developerName;
    }

    /* 
     * This is the method that actually does the percentage completed calculation.  It does this by pulling the list of fields that should be populated for the given record type 
     * of the given object from the IRSPX_Page_Layout_Definitions__mdt custom metadata type.  It then counts of the number of these fields that has actually been 
     * populated.  The percentage is calculated by the dividing the count of fields populated by the total count of fields that should be populated.
     * 
    */
    private static Decimal getPercentageCompleted(sObject IRSPXObject){
        String sobjectName;
        String developerName;
        String recordTypeLayoutKey;
        Decimal fieldPopulatedCount = 0;
        Decimal pageLayoutFieldCount = 0;
        Decimal percentageCompleted = 0;
        Integer rollupSummaryCount = 0;
        Map <String, Object> sobjectFieldswithValue;
        
        sobjectName = IRSPXObject.getSObjectType().getDescribe().getName();
        developerName = getRecordTypeDeveloperName(IRSPXObject);
        // Get map of fields that have been populated
        sobjectFieldswithValue = IRSPXObject.getPopulatedFieldsAsMap();
            
        // Only do calculation if developer name has a value.  If developer name is null then it means this is being called on an object that hasn't been defined in 
        // the getRecordTypeDeveloperName method or the user's profile doesn't have a record type defined for the given IRSPX object.
        if (String.isNotEmpty(developerName)){
            recordTypeLayoutKey = sobjectName + '-' + developerName;
            system.debug('recordTypeLayoutKey: ' + recordTypeLayoutKey);
            
            // Only need to query for the fields that should be populated by the user if the fields haven't already been added to the recordtypeLayouts map.
            if (!recordtypeLayouts.containsKey(recordTypeLayoutKey)){
                system.debug('Key does not exist.');         
                // Pull list of fields that should be populated from the IRSPX_Page_Layout_Definitions__mdt custom metadata type for the given object and record type.
                IRSPX_Page_Layout_Definitions__mdt[] pageLayoutMappings = [SELECT MasterLabel, DeveloperName, OBJECT_name__c, field_name__c, Rollup_Summary_Field__c FROM IRSPX_Page_Layout_Definitions__mdt 
                                                                           where record_type_name__c = :developerName 
                                                                           and object_name__c = :sobjectName];
                recordtypeLayouts.put(recordTypeLayoutKey,pageLayoutMappings);     
            }
            
            // Count fields that have been populated based on the fields that should be populated by the user.
            for(IRSPX_Page_Layout_Definitions__mdt layoutDefinition : recordtypeLayouts.get(recordTypeLayoutKey)){
                if (sobjectFieldswithValue.containsKey(layoutDefinition.field_name__c)){
                    system.debug(layoutDefinition.field_name__c + ' value: ' + sobjectFieldswithValue.get(layoutDefinition.field_name__c));
                    if (!layoutDefinition.Rollup_Summary_Field__c){
                        fieldPopulatedCount = fieldPopulatedCount + 1;  
                    }else{
                        rollupSummaryCount = Integer.valueOf(sobjectFieldswithValue.get(layoutDefinition.field_name__c));
                        if (rollupSummaryCount > 0){
                            fieldPopulatedCount = fieldPopulatedCount + 1; 
                        }
                    }
                }       
            }
                    
            system.debug('fieldPopulatedCount: ' + fieldPopulatedCount);
            
            pageLayoutFieldCount = recordtypeLayouts.get(recordTypeLayoutKey).size();
            
            system.debug('pageLayoutFieldCount ' + pageLayoutFieldCount);
            
            If (pageLayoutFieldCount > 0){
                percentageCompleted = (fieldPopulatedCount / pageLayoutFieldCount) * 100;
                percentageCompleted = percentageCompleted.round(System.RoundingMode.HALF_UP);
                system.debug('percentageCompleted: ' + percentageCompleted);
            }           
        }
            
        return percentageCompleted;
        
    }
    
    public static void updatePercentageCompleted(List<sObject> IRSPXObjects ){
        Decimal percentageCompleted;
        List<sObject> IRSPXObjectsPercentCalculated = new List<sObject>();
        
        system.debug('List size: ' + IRSPXObjects.size());
        
        try{
            for(sobject s : IRSPXObjects){
                percentageCompleted = getPercentageCompleted(s);
                s.put('Percentage_of_Fields_Populated__c', percentageCompleted);
                system.debug('s: ' + s);
            }  
        } catch(SObjectException e){
            system.debug('Exception occurred: ' + e.getMessage() );
            System.debug('Line Number: ' + e.getLineNumber());
        }
    }
}