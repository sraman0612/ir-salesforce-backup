@isTest
public class welch_SelfRegisterClassTest {
    @TestSetup
    static void makeData(){
        BuyerGroup buyGroup = new BuyerGroup();
        buyGroup.Name = 'Welch Vacuum Shop Buyer Group';
        insert buyGroup;
    }

    @isTest static void invokeAsPartnerUser(){

        welch_SelfRegisterClass.SelfRegisterActionRequest[] requests = new welch_SelfRegisterClass.SelfRegisterActionRequest[]{};
        welch_SelfRegisterClass.SelfRegisterActionRequest request = new welch_SelfRegisterClass.SelfRegisterActionRequest();

        request.username = 'rickie.bobby@nascar.com.testuser';
        request.first_name = 'rickie';
        request.last_name = 'bobby';
        request.email_address = 'rickie.bobby@nascar.com';

        request.company_name = 'Test Company';
        request.street_address = '123 Street St.';
        request.city_address = 'Anytown';
        request.state_address = 'FL';
        request.country_address = 'United States';
        request.zip_address = '32164';

        requests.add(request);
        User communityUser = [
            SELECT  Id
            FROM    User
            WHERE   Profile.Name LIKE '%System Administrator%'
                AND isActive = true
        Limit 1];

        System.runAs(communityUser){
            System.Test.startTest();
                welch_SelfRegisterClass.SelfRegisterActionResponse[] responses = welch_SelfRegisterClass.invoke(requests);
            System.Test.stopTest();

            System.assertNotEquals(true, responses.isEmpty());

            // since Site.createExternalUser() returns null in a test context, we can only assert on the accountId
            //System.assertNotEquals(null, responses[0].accountId);
       }
    }
}