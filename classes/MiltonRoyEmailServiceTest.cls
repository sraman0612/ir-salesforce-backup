@isTest
public class MiltonRoyEmailServiceTest {

	
    static testMethod void testMiltonRoyEmailService() 
    {
       // Create a new email and envelope object.    
       
       Messaging.InboundEmail email = new Messaging.InboundEmail() ;
       Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    
       // Create Test record.
        String PSTContactRecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Milton_Roy_Contact' LIMIT 1].Id;
       Contact cont = new Contact(firstName='john', lastName='smith', Email='test123@test.com',Phone='1234567890',recordTypeId = PSTContactRecordTypeId);
       insert cont ;
       //create team escalation record
       Team_Escalation__c mrTeamEsc = new Team_Escalation__c(Name = 'MR Customer Service');
        insert mrTeamEsc;
        
       // Test with the subject that matches the unsubscribe statement.
       email.subject = 'Test MiltonRoy Voicemail Email';
       //email.plainTextBody = 'Test Contact Email';
       email.htmlBody = 'Test1'+'\n'
						+'Test2' +'\n'
						+'Test3' +'\n'
						+'Test4' +'\n'
 						+'Test5' +'\n'
						+'ContactId  : 1234567890';
	   email.fromName = 'Test User';
       email.fromAddress = 'test123@test.com';
        
       //create binaryAttachment
       Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        String blobValue = 'This is a test attachment for Milton Roy Voicemail testing.';
        attachment.body = blob.valueOf(blobValue.repeat(600));
        attachment.fileName = 'TestFile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
       //envolope information
       env.fromAddress = 'test@test.com';
        Test.startTest();
       		MiltonRoyEmailService obj= new MiltonRoyEmailService();
       Test.stopTest();
       obj.handleInboundEmail(email,env);
                            
    }
     
}