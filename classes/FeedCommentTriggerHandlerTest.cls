@isTest
public class FeedCommentTriggerHandlerTest {
    // Method to test account trigger
    static testMethod void testreopenCase() {    
        Id caseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
        Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        settings.Email_Message_Trigger_Enabled__c = true;
        insert settings;  
        Contact ct1 = new Contact();
        ct1.FirstName = 'Jim';
        ct1.LastName = 'Test1';
        ct1.Email = 'abc1@testing.com';
        ct1.Title = 'Mr.';
        ct1.Phone = '123456788';
        ct1.RecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'Customer_Contact'].Id;         
        insert new Contact[]{ct1};  
            String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName=uniqueUserName);
        
        
        Test.startTest();
        {
            Case case1 = TestDataUtility.createCase(true,caseRecordTypeId,ct1.Id, null, 'Test 1', 'New');
            FeedItem fi = new FeedItem(ParentId = case1.Id, Body = 'Test Body');
            insert fi;
            System.runAs(u) {
                //Create Related Feed Item Record              
                
                //Create Feed Comment Record
                FeedComment fc = new FeedComment(FeedItemId = fi.Id, CommentBody = 'Test Comment');
                insert fc;
                Case caseObj = [SELECT Id,Unread_Email_box__c FROM Case WHERE Id =: case1.Id limit 1];
                System.assertEquals(false,caseObj.Unread_Email_box__c);
                fc.CommentBody = 'Test Comment1';
                update fc;
                delete fc;
                
            }
            
        }
        Test.stopTest(); 
    }
}