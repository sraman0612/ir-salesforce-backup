public with sharing class CTS_IOT_Site_HierarchyController {

    private class CTS_IOT_Site_HierarchyControllerException extends Exception{}

    @TestVisible private static Boolean forceError = false;    

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Data_Service.SiteHierarchyResponse siteHierarchy;
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public CTS_IOT_Community_Administration__c userSettings = CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId());
        @AuraEnabled public Integer totalRecordCount = 0;
    }

    @AuraEnabled
    public static InitResponse getInit(String searchTerm, Integer pageSize, Integer offSet, Integer totalRecordCount){

        InitResponse response = new InitResponse();

        try{

            if (Test.isRunningTest() && forceError){
                throw new CTS_IOT_Site_HierarchyControllerException('test error');
            }

            if (pageSize == null){
                pageSize = response.userSettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.userSettings.User_Selected_List_Page_Size__c) : (response.communitySettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.communitySettings.User_Selected_List_Page_Size__c) : null);
            }            

            response.siteHierarchy = CTS_IOT_Data_Service.getCommunityUserSiteHierarchy(searchTerm, pageSize, offSet);
            response.totalRecordCount = CTS_IOT_Data_Service.getCommunityUserSitesCount(searchTerm);

            if (pageSize != null && (response.userSettings.User_Selected_List_Page_Size__c == null || pageSize != response.userSettings.User_Selected_List_Page_Size__c)){
                response.userSettings.User_Selected_List_Page_Size__c = pageSize;
                upsert response.userSettings;
            }            
        }
        catch(Exception e){
            system.debug('exception: ' + e);
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}