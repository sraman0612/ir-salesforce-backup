public class Assignment_Group_Queues_TriggerHandler {
    //Determine if the value of the Queue Name has been updated.  If so Break.
    
    public static void beforeInsert(Assignment_Group_Queues__c[] newList)
    {
        validateQueues(newList,null);
    }
    
    public static void beforeUpdate(Assignment_Group_Queues__c[] newList, Map<Id,Assignment_Group_Queues__c> oldMap)
    {
        validateQueues(newList,oldMap);
    }
    
    public static void validateQueues(Assignment_Group_Queues__c[] newList, Map<Id,Assignment_Group_Queues__c> oldMap)
    {
        Map<Integer,String> queueIds = new Map<Integer,String>();
        Integer idx = 0;
        for (Assignment_Group_Queues__c cs : newList)
        {
            if(oldMap != null)
            {  
                if(cs.name <> oldMap.get(cs.id).name)
                {
                    queueIds.put(idx, cs.name);
                }           
            }
            else
            {
                queueIds.put(idx,  cs.name);
            }   
            idx++;
        }
        System.debug('>>>>>queueIds: '+queueIds);
        if (queueIds.isEmpty()) return;
        
        //Setup Queue lookup Xref map
        //NOTE: the number of Queues supported is limited by Map size (ie. 1000)
        Map<String,Group> Queues = new Map<String,Group>();     //Queue name --> Queue
        
        for (Group[] q :  [SELECT Name FROM Group WHERE Type= 'Queue']) {
            for (Integer i = 0; i < q.size() ; i++) {
                Queues.put(q[i].Name, q[i]);
            }
        }
    
        
        Map<String,String> agNames = new Map<String,String> ();
        for (Assignment_Group_Queues__c agq : [SELECT Name, Assignment_Group_Name__r.Name
                                                FROM Assignment_Group_Queues__c 
                                                WHERE Active__c = 'True']) {
            agNames.put(agq.Name, agq.Assignment_Group_Name__r.Name);
        }
        
        //Find Queues matching on name
        for (Assignment_Group_Queues__c agq : newList)
        {
            if (Queues.containsKey(agq.Name))
            {
                Id qId = Queues.get(agq.Name).Id;
                System.debug('>>>>>Queue Id for name ' + agq.Name + ': '+qId);
                
                //Check if Queue is already assigned to an Assignment_Group_Queues__c record
                if (agNames.containsKey(agq.Name)) {
                    agq.Valid_Queue__c = false;
                    agq.addError('Queue "'+agq.Name+'" already assigned to another Assignment Group "'+agNames.get(agq.Name)+'".');
                } else {
                    agq.QueueId__c = qId;
                    agq.Valid_Queue__c = true;
                }
            } else {
                //Error: queue not found
                agq.Valid_Queue__c = false;
                agq.addError('Invalid Queue name: Queue name ' + agq.Name + ' cannot be found.');
            }   
        }
    }
}