@isTest
public class DeleteProspectAccountController_TEST {
    
    @testSetup
    static void setupData(){
        User user = CTS_TestUtility.createUser(false);
        user.Email = 'VPTECH1@test.com';
        user.FirstName = 'CPI';
        user.LastName = 'Integration';
        user.Assigned_From_Email_Address__c =null;
        insert user;
        
        User user2 = CTS_TestUtility.createUser(false);
        user2.Email = 'VPTECH2@test.com';
        
        user2.Assigned_From_Email_Address__c =null;
        insert user2;
        
        System.runAs(user){
            List<Account> accList = new List<Account>();
            List<Business_Relationship__c> brList = new List<Business_Relationship__c>();
            
            Account acct = new Account();
            acct.name = 'VPTECH Test Account';
            acct.Type = 'Ship To';
            acct.SAP_Account_External_ID__c = 'Parent';
            acct.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            insert acct;
            
            Business_Relationship__c br = new Business_Relationship__c();
            br.BR_Owner__c = UserInfo.getUserId();
            br.Account__c = acct.id;
            br.Type__c = 'Ship To';
            insert br;
            
            
        }
        System.runAs(user2){
            List<Business_Relationship__c> brList = new List<Business_Relationship__c>();
            List<Account> accList = new List<Account>();
            List<Contact> conList = new List<Contact>();
            
            Account acct1 = new Account();
            acct1.name = 'VPTECH Test Account1';
            acct1.Type = 'Prospect';
            acct1.ShippingCity = 'City';
            acct1.ShippingState = 'State';
            acct1.ShippingPostalCode = '12345';
            acct1.ShippingStreet = 'street';
            acct1.ShippingCountry  = 'United States';
            acct1.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            accList.add(acct1);
            
            Account acct2 = new Account();
            acct2.name = 'VPTECH Test Account2';
            acct2.Type = 'Prospect';
            acct2.ShippingCity = 'City';
            acct2.ShippingState = 'State';
            acct2.ShippingPostalCode = '12345';
            acct2.ShippingStreet = 'street';
            acct2.ShippingCountry  = 'United States';
            acct2.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            accList.add(acct2);
            
            Account acct3 = new Account();
            acct3.name = 'VPTECH Test Account3';
            acct3.Type = 'Prospect';
            acct3.ShippingCity = 'City';
            acct3.ShippingState = 'State';
            acct3.ShippingPostalCode = '12345';
            acct3.ShippingStreet = 'street';
            acct3.ShippingCountry  = 'United States';
            acct3.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            accList.add(acct3);
            
            Account acct4 = new Account();
            acct4.name = 'VPTECH Test Account4';
            acct4.Type = 'Prospect';
            acct4.ShippingCity = 'City';
            acct4.ShippingState = 'State';
            acct4.ShippingPostalCode = '12345';
            acct4.ShippingStreet = 'street';
            acct4.ShippingCountry  = 'United States';
            acct4.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            accList.add(acct4);
            
            Account acct5 = new Account();
            acct5.name = 'VPTECH Test Account5';
            acct5.Type = 'Prospect';
            acct5.ShippingCity = 'City';
            acct5.ShippingState = 'State';
            acct5.ShippingPostalCode = '12345';
            acct5.ShippingStreet = 'street';
            acct5.ShippingCountry  = 'United States';
            acct5.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            accList.add(acct5);
            
            insert accList;
            
            
            Contact con1 = new Contact();
            con1.Business__c = 'Vacuum';
            con1.Email = 'Test@Test.com';
            con1.FirstName = 'First name';
            con1.LastName = 'Last name';
            con1.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            con1.Relationship__c = 'Vendor';
            con1.Type__c = 'End-user';
            con1.AccountId = acct3.Id;
            conList.add(con1);
            
            Contact con2 = new Contact();
            con2.Business__c = 'Vacuum';
            con2.Email = 'Test@Test.com';
            con2.FirstName = 'First name1';
            con2.LastName = 'Last name1';
            con2.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            con2.Relationship__c = 'Vendor';
            con2.Type__c = 'End-user';
            con2.AccountId = acct4.Id;
            conList.add(con2);
            
            Contact con3 = new Contact();
            con3.Business__c = 'Vacuum';
            con3.Email = 'Test@Test.com';
            con3.FirstName = 'First name1';
            con3.LastName = 'Last name1';
            con3.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            con3.Relationship__c = 'Vendor';
            con3.Type__c = 'End-user';
            con3.AccountId = acct5.Id;
            conList.add(con3); 
            insert conList;
            
            
            Opportunity opp = new Opportunity();
            opp.AccountId = acct4.Id;
            opp.Business__c = 'Vacuum';
            opp.CloseDate  = Date.today(); 
            opp.Name = 'VPTECH Test Account4';
            opp.StageName = 'Stage 1. Qualify';
            opp.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
            insert opp;
            
            
            
            Business_Relationship__c br1 = new Business_Relationship__c();
            br1.BR_Owner__c = UserInfo.getUserId();
            br1.Account__c = acct1.id;
            br1.Type__c = 'Prospect';
            brList.add(br1);
            
            Business_Relationship__c br2 = new Business_Relationship__c();
            br2.BR_Owner__c = UserInfo.getUserId();
            br2.Account__c = acct2.id;
            br2.Type__c = 'Prospect';
            brList.add(br2);
            
            Business_Relationship__c br3 = new Business_Relationship__c();
            br3.BR_Owner__c = UserInfo.getUserId();
            br3.Account__c = acct2.id;
            br3.Type__c = 'Prospect';
            brList.add(br3);
            
            Business_Relationship__c br4 = new Business_Relationship__c();
            br4.BR_Owner__c = UserInfo.getUserId();
            br4.Account__c = acct3.id;
            br4.Type__c = 'Prospect';
            br4.Business__c = 'Vacuum';
            brList.add(br4);
            
            Business_Relationship__c br5 = new Business_Relationship__c();
            br5.BR_Owner__c = UserInfo.getUserId();
            br5.Account__c = acct3.id;
            br5.Type__c = 'Prospect';
            br5.Business__c = 'Low Pressure';
            brList.add(br5);
            
            Business_Relationship__c br6 = new Business_Relationship__c();
            br6.BR_Owner__c = UserInfo.getUserId();
            br6.Account__c = acct4.id;
            br6.Type__c = 'Prospect';
            br6.Business__c = 'Vacuum';
            brList.add(br6);
            
            Business_Relationship__c br7 = new Business_Relationship__c();
            br7.BR_Owner__c = UserInfo.getUserId();
            br7.Account__c = acct5.id;
            br7.Type__c = 'Prospect';
            br7.Business__c = 'Low Pressure';
            brList.add(br7);
            
            Insert brList;
        }
        
    }
    
    private static testmethod void checkOwnerTest(){
        
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH1@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            test.startTest();
            Business_Relationship__c br = [Select id  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account' LIMIT 1];
            
            DeleteProspectAccountController.checkOwner(br.id);
            test.stopTest();
        }
    }
    
    private static testmethod void markAccountForDeletionSenerio1(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br = [Select id  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account1' LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br.id);
        }
    }
    
    private static testmethod void markAccountForDeletionSenerio2(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br1 = [Select id  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account2' LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br1.id);
        }
    }
    
    private static testmethod void markAccountForDeletionSenerio3(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br2 = [Select id  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account3' LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br2.id);
        }
    }
    
    private static testmethod void markAccountForDeletionSenerio4(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br3 = [Select id,Business__c  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account3'  AND Business__c ='Low Pressure' LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br3.id);
        }
    }
    
    
    private static testmethod void markAccountForDeletionSenerio5(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br4 = [Select id,Business__c  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account4'  LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br4.id);
        }
    }
    
    private static testmethod void markAccountForDeletionSenerio6(){
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH2@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
            Business_Relationship__c br5 = [Select id,Business__c  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account5'  LIMIT 1];
            DeleteProspectAccountController.markAccountForDeletion(br5.id);
        }
    }
}