global class SchedulerApex implements Schedulable {

 global void execute(SchedulableContext ctx) {
 faceToFaceBatchUpdate bCon = new faceToFaceBatchUpdate();
 
 //Batch size is 150, records will be split into batches 
 Database.ExecuteBatch(bCon,150);
 } 
}