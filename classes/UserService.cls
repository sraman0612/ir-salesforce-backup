public with sharing class UserService {

    // This method will return all users that report to (direct and indirect reports) for the given user Id
    public static Set<User> getAllReportingToUsers(Id userId){

        Set<User> reportingUsers = new Set<User>();
        Set<Id> managerIds = new Set<Id>{userId};

        while (managerIds.size() > 0){
            
            system.debug('managerIds: ' + +managerIds);

            Set<Id> managerIdsToCheck = new Set<Id>(managerIds);

            // Reset each run
            managerIds.clear();

            for (User u : [Select Id, Name, FirstName, LastName, UserRole.Name, Profile.Name, ManagerId, DelegatedApproverId From User Where ManagerId in :managerIdsToCheck]){
                
                reportingUsers.add(u);
                managerIds.add(u.Id);
            }
        }    

        system.debug('reportingUsers: ' + reportingUsers);

        return reportingUsers;
    }

    public static Set<User> getDelegateUsers(Id userId){

        Set<User> delegateUsers = new Set<User>();

        for (User u : [Select Id, Name, FirstName, LastName, UserRole.Name, Profile.Name, ManagerId, DelegatedApproverId From User Where DelegatedApproverId = :userId]){        
            delegateUsers.add(u);
        }  

        system.debug('delegateUsers: ' + delegateUsers);

        return delegateUsers;
    }    
}