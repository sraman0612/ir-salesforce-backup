@isTest
public Class CTS_CommunityCPQEmbeddedEditCTLRTEST{
  public testmethod static void test1(){
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for CPQ Embedded CTLR';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
    a.ShippingStreet = '13, street2';
    a.ShippingPostalCode = '123';  
    a.CTS_Global_Shipping_Address_1__c = '13';
    a.CTS_Global_Shipping_Address_2__c = 'street2';    
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
    insert a;    
    Test.startTest();
    PageReference testPage = Page.CTS_CommunityCPQEmbeddedEdit;
    testPage.getHeaders().put('Referer', 'https://www.salesforce.com?id=somerandm18charid');
    Test.setCurrentPage(testPage);
    ApexPages.StandardController sc =new ApexPages.StandardController(a);
    CTS_CommunityCPQEmbeddedEditCTLR scext =new CTS_CommunityCPQEmbeddedEditCTLR(sc);
    scext.redirector();
  }
}