public with sharing class PT_EventTriggerHandler
{
    public class PT_EventTriggerHandlerException extends Exception {}
    

    
    public static Map<String, PT_Outlook_Event__c> getOutlookEvents(Event[] events)
    {
        /*
        	Query length could be a problem
			30 events gives query 3841 long
			Limit is 20,000
			Max events will be around 150
			Solution would be to create multiple queries and then query more than once.
        */
        String query = 'SELECT Id, Subject__c, Start_Date_Time__c, End_Date_Time__c, Type__c, Event_Relations__c, Account__c, Whom__c FROM PT_Outlook_Event__c ';
        query += 'WHERE ';
        for(Event thisEvent: events)
        {
            //Consider similar subjects and not just the same, Maybe first x letters the same
            query += '\n((Subject__c = \'' + thisEvent.Subject + '\' OR ';
            
            //Consider people editing each others events
            query += 'CreatedById = \'' + thisEvent.CreatedById + '\') AND ';
            query += 'Start_Date_Time__c = ' + thisEvent.StartDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'','Etc/GMT') + ' AND ';  
            query += 'End_Date_Time__c = ' + thisEvent.EndDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'','Etc/GMT') + ') OR ' ;
        }
        query = query.removeEnd(' OR ');
        System.debug('Query:'+query);

        PT_Outlook_Event__c[] outlookEvents = Database.query(query);
        
        Map<String, PT_Outlook_Event__c> result = new Map<String, PT_Outlook_Event__c>();
        for(PT_Outlook_Event__c thisOutlookEvent: outlookEvents)
        {
            String uniqueKey = thisOutlookEvent.Subject__c + thisOutlookEvent.Start_Date_Time__c + thisOutlookEvent.End_Date_Time__c;
            result.put(uniqueKey, thisOutlookEvent);
        }
        
        return result;
    }
    
    public static void linkOutlookEvent(Event[] events)
    {
        try
        {
            Id powerToolsEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
            
            Set<Id> eventIds = new Set<Id>();
            for(Event thisEvent: events) 
            {
                if(thisEvent.RecordTypeId == powerToolsEventRecordTypeId)
                {
                    eventIds.add(thisEvent.Id);
                }
            }
            
            events = [Select id, StartDateTime, EndDateTime, Subject, PT_Outlook_Event_Id__c, CreatedById from Event where Id In :eventIds];
            
            EventRelation[] eventRelationsToInsert = new EventRelation[]{};                
            Map<Id,Map<Id,EventRelation>> eventRelationContactIds = PT_EventRelationService.getExistingEventRelationIds(eventIds);
            
            Map<String, PT_Outlook_Event__c> outlookEvents = getOutlookEvents(events);
            for(Event thisEvent: events)
            {
                if(!String.isEmpty(thisEvent.PT_Outlook_Event_Id__c)) continue;
                
				String eventKey = thisEvent.Subject + thisEvent.StartDateTime + thisEvent.EndDateTime;
                
                if(!outlookEvents.containsKey(eventKey))
                {
                    log('DEBUG','PT_EventTriggerHandler.linkOutlookEvent: No outlook events found with Subject:'+ thisEvent.Subject +' and startdate:'+thisEvent.StartDateTime);
                    continue;
                }
                
                PT_Outlook_Event__c outlookEvent = outlookEvents.get(eventKey);
                
                Event eventToUpdate = new Event();
                eventToUpdate.Id = thisEvent.Id;
                eventToUpdate.PT_Outlook_Event_Id__c = outlookEvent.Id;
                eventToUpdate.WhatId = outlookEvent.Account__c;
                eventToUpdate.Type = outlookEvent.Type__c;
                eventToUpdate.PT_Whom__c = outlookEvent.Whom__c;
                update eventToUpdate;
                
                outlookEvent.Event_Id__c = thisEvent.Id; 
                update outlookEvent;
                
                log('DEBUG','PT_EventTriggerHandler.linkOutlookEvent: Outlook Event '+outlookEvent.Id + ' linked to SF Event '+ thisEvent.Id);

                if(!String.isEmpty(outlookEvent.Event_Relations__c)){
                    Map<Id,EventRelation> existingEventRelationContactIds = eventRelationContactIds.get(thisEvent.Id);
                    EventRelation[] eventRelations = (EventRelation[])JSON.deserialize(outlookEvent.Event_Relations__c, List<EventRelation>.class);
                    for(EventRelation thisEventRelation: eventRelations)
                    {
                        thisEventRelation.EventId = thisEvent.Id;
                        if(!String.isEmpty(thisEventRelation.RelationId)) eventRelationsToInsert.add(thisEventRelation);
                    }                    
                }

            }
            Insert eventRelationsToInsert;
        } 
        catch(Exception e)
        {
        	log('ERROR',e.getTypeName() + ' - ' + e.getMessage() + ' - ' + e.getStackTraceString());
        }   
    }
    
    public static void log(String logType, String message)
    {
        Apex_Log_Setting__c logSetting = Apex_Log_Setting__c.getInstance(UserInfo.getUserId());
        if(logSetting.Apex_Logging_Enabled__c)
        {
            insert new Apex_Log__c(Class_Name__c = 'PT_EventTriggerHandler', Message__c = message, Log_Type__c=logType);
        }
        //Log_Type__c	
        //ERROR, DEBUG
        
    }
    
    
    /*
    	When account is saved, if the related to is blank and the event.who is a contact 
		this method sets the related to as the account of the contact that is specified in event.who
    */
    public static void setWhatIdToContactAccount(Event[] events)
    {
        Id powerToolsEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    	
        Set<Id> whoContactIds = new Set<Id>();
        for(Event thisEvent: events)
        {
            if(thisEvent.RecordTypeId == powerToolsEventRecordTypeId)
            {
                String whoIdObjectName;
                if(thisEvent.WhoId != null) whoIdObjectName = thisEvent.WhoId.getSObjectType().getDescribe().getName();
                if(whoIdObjectName == 'Contact' && String.isEmpty(thisEvent.WhatId)) whoContactIds.add(thisEvent.WhoId);
            }
        }
        
        Map<Id, Contact> contactsWithAccount = new Map<Id,Contact>([SELECT Id, AccountId FROM Contact WHERE Id IN :whoContactIds]);
        for(Event thisEvent: events)
        {
            if(thisEvent.RecordTypeId == powerToolsEventRecordTypeId)
            {
                String whatIdObjectName;
                String whoIdObjectName;
                if(thisEvent.WhatId != null) whatIdObjectName = thisEvent.WhatId.getSObjectType().getDescribe().getName();
				if(thisEvent.WhoId != null) whoIdObjectName = thisEvent.WhoId.getSObjectType().getDescribe().getName();
                
                if(whoIdObjectName == 'Contact' && String.isEmpty(whatIdObjectName)) 
                {
                    Contact thisEventContact = contactsWithAccount.get(thisEvent.WhoId);
                    thisEvent.WhatId = thisEventContact.AccountId;
                }
                
            }
        }
    }
        

  public static void blockEventsOnKeyAccounts(List<Event> newLst){
    
    Id PT_Task_RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Event' and DeveloperName='PT_powertools'].Id;
    Set<Id> acctIds = new Set<Id>();
    Set<Id> keyAcctIds = new Set<Id>();
    
    for (Event e: newLst) {
      if (null!=e.RecordTypeId && null!=e.WhatId && e.RecordTypeId==PT_Task_RecordTypeId && String.valueOf(e.WhatId).substring(0,3)=='001'){
        acctIds.add(e.WhatId);
      }
    }
    for (Account a : [SELECT Id, Name FROM Account WHERE Id IN :acctIds and Name LIKE 'Key Account%']){
      keyAcctIds.add(a.Id);
    }
      
    for (Event e: newLst) {
      if (keyAcctIds.contains(e.WhatId)){
        e.addError('Events cannot be created against Key Accounts.');
      }
    }
  }
  
  public static void updateEventPlannedDate (List<Event> newLst){
    Id PTEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    Map<Id, Date> LeadEventDateMap = new Map<Id, Date>();
    Lead [] leads2Update = new List<Lead>();
    for(Event e : newLst){
      if(e.RecordTypeId==PTEventRecordTypeId && null != e.WhoId && String.valueOf(e.WhoId).startsWith('00Q')){
        LeadEventDateMap.put(e.WhoId,e.StartDateTime.Date());  
      }
    }
    for(Lead l : [SELECT Id, PT_Event_Planned_Date__c FROM Lead WHERE Id IN :LeadEventDateMap.keyset()]){
      if(null==l.PT_Event_Planned_Date__c || LeadEventDateMap.get(l.Id) > l.PT_Event_Planned_Date__c){
        l.PT_Event_Planned_Date__c = LeadEventDateMap.get(l.Id);
        leads2Update.add(l);
      }
    }
    if(!leads2Update.isEmpty()){
      update leads2Update;
    }
  }
}