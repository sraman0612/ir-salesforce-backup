public class EMEIA_AssetTriggerHandler {
    
    public static Id EUAssetRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_EU_Asset' and SobjectType='Asset' limit 1].Id;
    public static Id EUIndirectAssetRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_EU_Indirect_Asset' and SobjectType='Asset' limit 1].Id;
    public static Id MEIAAssetRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_MEIA_Asset' and SobjectType='Asset' limit 1].Id;
    public static Id MEIAIndirAssetRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_MEIA_Indirect_Asset' and SobjectType='Asset' limit 1].Id;
    public static List<Asset> airNAAssetNew = new List<Asset>();
    public static List<Asset> airNAAssetOld = new List<Asset>();
    
    public static void beforeInsert(List<Asset> newAsset, List<Asset> oldAsset){ 
        getDetails(newAsset,oldAsset);    
        if(!airNAAssetNew.isEmpty()){
            Asset_TriggerHandler.beforeInsert(airNAAssetNew);
        }        
    }
    
    public static void afterInsert(List<Asset> newAsset, List<Asset> oldAsset){ 
        getDetails(newAsset,oldAsset);     
        if(!airNAAssetNew.isEmpty()){
            Asset_TriggerHandler.afterInsert(airNAAssetNew);
        }    
    }
    
    public static void afterUpdate(List<Asset> newAsset, List<Asset> oldAsset){ 
        getDetails(newAsset,oldAsset); 
        if(!airNAAssetNew.isEmpty()){
            Asset_TriggerHandler.afterUpdate(airNAAssetNew);
        }    
    }
    public static void afterDelete(List<Asset> newAsset, List<Asset> oldAsset){ 
        getDetails(newAsset,oldAsset); 
        if(!airNAAssetOld.isEmpty()){
            Asset_TriggerHandler.afterDelete(airNAAssetOld);
        }
    }
    
    public static void getDetails(List<Asset> newAsset, List<Asset> oldAsset){ 
        if(null!=newAsset){
            for (Asset a : newAsset){
                if (a.RecordTypeId==EUAssetRT_ID 
                    || a.RecordTypeId==EUIndirectAssetRT_ID 
                    || a.RecordTypeId==MEIAAssetRT_ID 
                    || a.RecordTypeId==MEIAIndirAssetRT_ID)
                {
                    airNAAssetNew.add(a);
                }
            }
        }
        
        //Separate old values by record types
        if(null!=oldAsset){
            for (Asset a : oldAsset){
                if ( a.RecordTypeId==EUAssetRT_ID 
                    || a.RecordTypeId==EUIndirectAssetRT_ID 
                    || a.RecordTypeId==MEIAAssetRT_ID 
                    || a.RecordTypeId==MEIAIndirAssetRT_ID)   
                {
                    airNAAssetOld.add(a);
                } 
            }
        }
        
    }
    
}