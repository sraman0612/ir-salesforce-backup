// Changes by Capgemini Devender Singh Date 7/6/2023. START US SGGC-232
public with sharing class MyTeam {
    @AuraEnabled(cacheable=true)
    public static String getMyTeamAccount(Integer pageSize, Integer pageNumber){
        Id userId = UserInfo.getUserId();
        system.debug('userId--> '+userId);
        Set<Id> allUsersId= RoleHeirarchyUtility.getRoleSubordinateUsers(userId);
        system.debug('allUsersId--> '+allUsersId);
        String jsonObjItm = '';
        Integer offset = (pageNumber - 1) * pageSize;         
        
        Integer totalRecords = [SELECT COUNT() FROM Business_Relationship__c WHERE BR_Owner__c in: allUsersId];
         if(pageSize==100)
             totalRecords=pageSize;
        Integer recordEnd = pageSize * pageNumber;
        
        List< DataWrapper > listDataWrapper = new List< DataWrapper >();
        
        for(Business_Relationship__c  br: [SELECT Id,Name,createdDate,Account__c, Account__r.Name,Account__r.Type,Account__r.ShippingAddress,
                                           Account__r.ShippingStreet, Account__r.ShippingCity, Account__r.ShippingState, 
                                           Account__r.ShippingPostalCode, Account__r.ShippingCountry,
                                           Account__r.Status__c, Type__c,CTS_Channel__c,Business__c,
                                           BR_Owner__c,BR_Owner__r.Name,Status__c 
                                           FROM Business_Relationship__c WHERE BR_Owner__c in: allUsersId
                                           ORDER BY createdDate DESC
                                           LIMIT :pageSize OFFSET :offset]){
                                               DataWrapper objDataWrapper = new DataWrapper();
                                               objDataWrapper.AccountName = br.Account__r.Name;
                                               objDataWrapper.AccountUrl =URL.getSalesforceBaseUrl().toExternalForm()+'/'+ br.Account__c;
                                               objDataWrapper.AccountType = br.Account__r.Type;
                                               objDataWrapper.AccountStatus = br.Account__r.Status__c;
                                               objDataWrapper.ShippingAddress = br.Account__r.ShippingStreet!=null?br.Account__r.ShippingStreet:''+' '+br.Account__r.ShippingCity!=null?br.Account__r.ShippingCity:''+' '+br.Account__r.ShippingState!=null?br.Account__r.ShippingState:''+' '+br.Account__r.ShippingPostalCode!=null?br.Account__r.ShippingPostalCode:''+' '+br.Account__r.ShippingCountry!=null?br.Account__r.ShippingCountry:'';
                                               objDataWrapper.BRnumber = br.Name;
                                               objDataWrapper.BRUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+br.Id;
                                               objDataWrapper.BRtype = br.Type__c;
                                               objDataWrapper.BRowner = br.BR_Owner__r.Name;
                                               objDataWrapper.BRstatus = br.Status__c;
                                               objDataWrapper.Channel = br.CTS_Channel__c;
                                               listDataWrapper.add(objDataWrapper);
                                           }
        
        ContactItemWrapper conObj =  new ContactItemWrapper();  
        conObj.pageNumber = pageNumber;
        conObj.pageSize = pageSize;        
        conObj.recordStart = offset + 1;
        conObj.recordEnd = totalRecords >= recordEnd ? recordEnd : totalRecords;
        conObj.totalRecords = totalRecords;
        conObj.businessRelationship = listDataWrapper;
        jsonObjItm = JSON.serialize(conObj);
        return jsonObjItm;
    }  
    
    public class ContactItemWrapper {       
        public Integer recordStart {get;set;}
        public Integer pageNumber {get;set;}
        public Integer totalRecords {get;set;}
        public Integer recordEnd {get;set;}
        public Integer pageSize {get;set;}       
        public List<DataWrapper> businessRelationship {get;set;}
    }
    public class DataWrapper {
        public string AccountName{get;set;}
        public string AccountUrl{get;set;}
        public string AccountType{get;set;}
        public string AccountStatus{get;set;}
        public string ShippingAddress{get;set;}
        public string BRnumber{get;set;}
        public string BRUrl{get;set;}
        public string BRtype{get;set;}
        public string BRowner{get;set;}
        public string BRstatus{get;set;}
        public string Channel{get;set;}
        
    }
    
}
// Changes by Capgemini Devender Singh Date 7/6/2023. END US SGGC-232