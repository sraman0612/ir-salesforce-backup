/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ActionUtil {
    global ActionUtil() {

    }
    @Future(callout=true)
    global static void cache(Id actionId, String apiName) {

    }
    global static Map<String,Object> fireAction(String actionName, String dataSet, Map<String,Object> params, Map<String,Object> data, String auth) {
        return null;
    }
}
