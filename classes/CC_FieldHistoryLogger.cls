Public Class CC_FieldHistoryLogger {
    private Schema.FieldSet loggedFields;


    public CC_FieldHistoryLogger(Schema.FieldSet fs) {
        this.loggedFields = fs;
    }


    public void logHistory(Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {         
        cc_gpsi_field_history__c[] history = new cc_gpsi_field_history__c[] {};
        
        for (SObject n :newMap.values()) {
            SObject o = oldMap.get(n.Id);
            if ((n.getSObjectType() != loggedFields.getSObjectType()) ||
                (o.getSObjectType() != loggedFields.getSobjectType())) {
               throw new CC_FieldHistoryLogger.FieldHistoryLoggerException('SObject type not compatible with Field Set type');
            }
            
            for(Schema.FieldSetMember f : loggedFields.getFields()) {
                String fieldName = f.getFieldPath();
                String newVal = (null == n.get(fieldName)) ? '' : '' + n.get(fieldName);
                String oldVal = (null == o.get(fieldName)) ? '' : '' + o.get(fieldName);
                System.Debug('@@@ Field: ' + fieldName + '  Old: ' + oldVal + '  New: ' + newVal);
                if (!newVal.equals(oldval)) {
                    cc_gpsi_field_history__c fieldChange = new cc_gpsi_field_history__c();
                    fieldChange.Parent_Id__c  = n.Id;
                    fieldChange.Field__c      = fieldName;
                    fieldChange.Old_Value__c  = oldVal.left(255);
                    fieldChange.New_Value__c  = newVal.left(255);
                    fieldChange.Created_By__c = Userinfo.getName();
                    history.add(fieldChange);
                }
            }           
        }
        
        if (!history.isEmpty()) {
            insert history;
        }       
    }
    

    public Class FieldHistoryLoggerException extends Exception {
    }
    
}