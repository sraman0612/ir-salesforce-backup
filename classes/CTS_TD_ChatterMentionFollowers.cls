/**
    @author Providity
    @date 25MAR2019
    @description @mention followers of an article
*/

public class CTS_TD_ChatterMentionFollowers {    
    
    public static void postToFollowers(List<Knowledge__kav> newList, Map<Id,Knowledge__kav> oldMap) {
        
        CTS_Article_Search_Config__c  config = CTS_Article_Search_Config__c.getInstance();
        String ctsTDRTName = config.Record_Type__c != null ? config.Record_Type__c : 'IR Comp TechDirect Knowledge Article';
        
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName();
        Id rtId =  rtMapByName.containsKey(ctsTDRTName) ? rtMapByName.get(ctsTDRTName).getRecordTypeId() : null;
        
        String ctsTDNetworkId = [SELECT Id FROM Network WHERE Name = 'IR Comp TechDirect'].Id; //For CTS TD Community
        Map<Id,Knowledge__kav> knowledgeMap = new Map<Id,Knowledge__kav>();
        List<String> kaIds = new List<String>();
        
        for(Knowledge__kav kb: newList) {
            Knowledge__kav oldKb = oldMap.get(kb.Id);
            if(kb.RecordTypeId == rtId && kb.CTS_Custom_Version_Number__c != oldKb.CTS_Custom_Version_Number__c) {
                knowledgeMap.put(kb.KnowledgeArticleId, kb);
            }
        }        
        //Post chatter with @mention to all followers        
        List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>();
        for(EntitySubscription sub: [SELECT ParentId, SubscriberId FROM EntitySubscription WHERE ParentId IN: knowledgeMap.keySet()]) {
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();
            
            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
            
            mentionSegmentInput.id = sub.SubscriberId;
            messageBodyInput.messageSegments.add(mentionSegmentInput);                        
                
            textSegmentInput.text = System.Label.CTS_TechDirect_ArticleNotification_Updated + ' ' + knowledgeMap.get(sub.ParentId).Title + ' (' + knowledgeMap.get(sub.ParentId).ArticleNumber + ')';
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = sub.ParentId;
            feedItemInput.visibility = ConnectApi.FeedItemVisibilityType.AllUsers;
            
            //ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(ctsTDNetworkId, feedItemInput);
            ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
            batchInputs.add(batchInput);
        }
        if(!batchInputs.isEmpty())
            ConnectApi.ChatterFeeds.postFeedElementBatch(ctsTDNetworkId, batchInputs);
    }        
}