@IsTest
public with sharing class CTS_OM_EmailMessage_Trigger_Test {
    
    @TestSetup
    private static void createData(){
        EmailMessage_TriggerTestHelper.createData();
    }
    
    private static testMethod void createNewCaseAndReparentEmail(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmail(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);
    }
    //Updated by AMS Team (Mahesh) - #03118925 - 7 June 2024
    private static testMethod void createNewCaseAndReparentEmail_1(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmail_1(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);
    }
    
    private static testMethod void createNewCaseAndReparentEmailPreexistingOpenChildCase(){
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailPreexistingOpenChildCase(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);
    }    
    
    private static testMethod void createNewCaseAndReparentEmailInactiveOwner(){
    	Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailInactiveOwner(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, settings.CTS_Default_Case_Owner_ID__c);        
    }    
    
    private static testMethod void createNewCaseAndReparentEmailOwnedByQueue(){
      //  EmailMessage_TriggerTestHelper.createNewCaseAndReparentEmailOwnedByQueue(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId);        
    }        
    
    private static testMethod void testPreventDuplicateEmailsDisabled(){
    	
        Email_Message_Settings__c settings = Email_Message_Settings__c.getOrgDefaults();
        settings.CTS_OM_Email_Trigger_Enabled__c = false;
        upsert settings;       	
    	
        EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, false);         
    }    
    
    private static testMethod void testPreventDuplicateEmailsEnabled(){
        EmailMessage_TriggerTestHelper.testPreventDuplicateEmails(CTS_OM_EmailMessage_TriggerHandler.acctMgmtRecordTypeId, true);         
    }
}