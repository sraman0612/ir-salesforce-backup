/**
@author Ben Lorenz
@date 1OCT2019
@description Test class for CTS_LosantEventCaseEmailServiceHandler
*/

@isTest
private class CTS_LosantCaseEmailHandlerTest {
    
    @TestSetup
    static void setupData() {
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Non_Warranty_Claims').getRecordTypeId();
        Case c = new Case();
        c.Subject = 'Test';
        c.Status = 'New';
        c.CTS_RMS_Assigned_Thru_Email_Response__c = false;
        c.RecordTypeId = recTypeId;
        insert c;
    }
    static testMethod void testEmail1() {       
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
                        
        Case c = [SELECT Id, Subject FROM Case];
        String threadId = CTS_LosantEventCaseEmailHandler.getThreadId(c.Id);
            
        email.subject = 'Test Email ' + threadId;
        email.plainTextBody = 'Test Email ' + threadId;        
        env.fromAddress = 'test@test.com';
        
        CTS_LosantEventCaseEmailServiceHandler obj= new CTS_LosantEventCaseEmailServiceHandler();
        obj.handleInboundEmail(email, env);                            
    }
    
    static testMethod void testEmail2() {       
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'Test Email ';
        email.plainTextBody = 'Test Email ';        
        env.fromAddress = 'test@test.com';
        
        CTS_LosantEventCaseEmailServiceHandler obj= new CTS_LosantEventCaseEmailServiceHandler();
        obj.handleInboundEmail(email, env);        
    }
}