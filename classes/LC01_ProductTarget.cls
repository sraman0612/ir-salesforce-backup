public with sharing class LC01_ProductTarget {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Lightning controller for LC01_ProductTarget
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 12-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	@AuraEnabled
  public static Map<String, Object> retrieveTarget(String accountId, String currentYear,String currentYear_1,String currentYear_2){
  	Map<String, Object> mapResult = new Map<String, Object>();
  	List<PT_DSMP_Strategic_Product__c> lstProductTarget = new List<PT_DSMP_Strategic_Product__c>();
  	Map<String, wrapperProductTarget> mapProdToTarget = new Map<String, wrapperProductTarget>();
  	List<String> lstYear = new List<String>{currentYear, currentYear_1, currentYear_2};
  	PT_DSMP_CS_Table_Plan__c ptrt = PT_DSMP_CS_Table_Plan__c.getOrgDefaults();
  	Set<String> currentYearProducts = new Set <String>();
  	for(PT_DSMP_NPD__c r : [SELECT Name, Hide__c FROM PT_DSMP_NPD__c WHERE Year2__c = :currentYear]){
  	  if (!r.Hide__c){currentYearProducts.add(r.Name);}
  	}
  
  	try {
  		Set<String> setCurrentYear = new Set<String>();
  
  		lstProductTarget = [SELECT Id, Name, Quantity__c, Actual__c, DSMP_NPD__c, PT_DSMP__r.PT_DSMP_Year__c,
  								   PT_DSMP__r.PT_DSMP_Year__r.Name, DSMP_NPD__r.Product__c, DSMP_NPD__r.Product__r.Name,
  								   Hide__c, Year__c,Product_Name__c
  							  FROM PT_DSMP_Strategic_Product__c
  							 WHERE PT_DSMP__r.PT_DSMP_Year__c != null AND 
  							 	   PT_DSMP__r.PT_DSMP_Year__r.Name IN: lstYear AND 
  							 	   PT_DSMP__r.Account__c =: accountId
  						  ORDER BY Name DESC, Year__c DESC];
  
  		for(PT_DSMP_Strategic_Product__c current : lstProductTarget){ 
  			if(current.PT_DSMP__r.PT_DSMP_Year__r.Name == currentYear && currentYearProducts.contains(current.Name)){
  				setCurrentYear.add(current.Name);
  		  	}
  		}
  
  		for(PT_DSMP_Strategic_Product__c current : lstProductTarget){
  			
  			if(setCurrentYear.contains(current.Name)){
  			
  				if(current.PT_DSMP__r.PT_DSMP_Year__r.Name == currentYear){
  
  		  			if(mapProdToTarget.containsKey(current.Name)){
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd = mapProdToTarget.get(current.Name);
  		  				currentProd.actualY = current.Actual__c != null ? current.Actual__c : 0;
  		  				currentProd.targetY = current.Quantity__c != null ? current.Quantity__c : 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			} else {
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd.id = current.id;
  		  				currentProd.name = current.Name;
  		  				currentProd.actualY = current.Actual__c != null ? current.Actual__c : 0;
  		  				currentProd.targetY = current.Quantity__c != null ? current.Quantity__c : 0;
  		  				currentProd.actualY_1 = 0;
  		  				currentProd.actualY_2 = 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			}
  		  		}
  
  		  		if(current.PT_DSMP__r.PT_DSMP_Year__r.Name == currentYear_1){
  		  			if(mapProdToTarget.containsKey(current.Name)){
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd = mapProdToTarget.get(current.Name);
  		  				currentProd.actualY_1 = current.Actual__c != null ? current.Actual__c : 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			} else {
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd.id = current.id;
  		  				currentProd.name = current.Name;
  		  				currentProd.actualY_1 = current.Actual__c != null ? current.Actual__c : 0;
  		  				currentProd.actualY_2 = 0;
  		  				currentProd.actualY = 0;
  		  				currentProd.targetY = 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			}
  		  		}
  
  		  		if(current.PT_DSMP__r.PT_DSMP_Year__r.Name == currentYear_2){
  		  			if(mapProdToTarget.containsKey(current.Name)){
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd = mapProdToTarget.get(current.Name);
  		  				currentProd.actualY_2 = current.Actual__c != null ? current.Actual__c : 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			} else {
  		  				wrapperProductTarget currentProd = new wrapperProductTarget();
  		  				currentProd.id = current.id;
  		  				currentProd.name = current.Name;
  		  				currentProd.actualY_2 = current.Actual__c != null ? current.Actual__c : 0;
  		  				currentProd.actualY_1 = 0;
  		  				currentProd.actualY = 0;
  		  				currentProd.targetY = 0;
  		  				mapProdToTarget.put(current.Name, currentProd);
  		  			}
  		  		}
  	  		}
  	  	}
  	  	mapResult.put('error', false);
  		mapResult.put('lstProductTarget', mapProdToTarget.values());
  
  	} catch(Exception e){
  		mapResult.put('error', true);
  		mapResult.put('message', ptrt.TablePlan_SalesforceError__c);
  	}
  	return mapResult;
  }


	@AuraEnabled
	public static Map<String, Object> saveTarget(String lstTarget){
		Map<String, Object> mapResult = new Map<String, Object>();
		List<PT_DSMP_Strategic_Product__c> lstProductTargetToUpdate = new List<PT_DSMP_Strategic_Product__c>();
		List<wrapperProductTarget> lstProductTarget = new List<wrapperProductTarget>();

		PT_DSMP_CS_Table_Plan__c ptrt = PT_DSMP_CS_Table_Plan__c.getOrgDefaults();

		try {

			lstProductTarget = (List<wrapperProductTarget>) System.JSON.deserialize(lstTarget, List<wrapperProductTarget>.class);

			for(wrapperProductTarget current : lstProductTarget){

				lstProductTargetToUpdate.add(new PT_DSMP_Strategic_Product__c(Id = current.id,
																			  Quantity__c = current.targetY
																			));
			}

			System.debug('mgr lstProductTargetToUpdate ' + lstProductTargetToUpdate);

			if(lstProductTargetToUpdate.size() > 0)
				update lstProductTargetToUpdate;

			mapResult.put('error', false);
			//mapResult.put('message', 'Products Target saved!');
			//mapResult.put('message', Label.PT_DSMP_TablePlan_ProductSaved);
			mapResult.put('message', ptrt.TablePlan_ProductSaved__c);

		  	System.debug('mgr mapResult ' + mapResult);

		} catch(Exception e){

			System.debug('mgr e '+ e);
			mapResult.put('error', true);
			//mapResult.put('message', Label.PT_DSMP_TablePlan_SalesforceError);
			mapResult.put('message', ptrt.TablePlan_SalesforceError__c);
		}

		return mapResult;
	}


	public class wrapperProductTarget {
		@AuraEnabled public String id;
		@AuraEnabled public String name;
		@AuraEnabled public Decimal actualY_1;
		@AuraEnabled public Decimal actualY_2;
		@AuraEnabled public Decimal actualY;
		@AuraEnabled public Decimal targetY;

		public wrapperProductTarget(){}

	}

}