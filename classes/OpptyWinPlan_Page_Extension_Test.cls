/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the Test class for OpptyWinPlan_Page_Extension.
------------------------------------------------------------*/
@istest(seeAllData=false)
public with sharing class OpptyWinPlan_Page_Extension_Test {
    
    @testSetup
    static void dataSetup(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        System.runAs(u){
            account testAcc=TestDataFactory.createAccount('TestAcc1','IR Comp EU');
            testAcc.ownerid=u.id;
            insert testAcc;
            opportunity testOpp=TestDataFactory.createOpportunity('TestOpty1','IR Comp MEIA',testAcc.id,'No','Budgetary','Stage 1. Qualify','Pipeline');
            testOpp.ownerid=u.id;
            insert testOpp;
            //commented by CG as part of descoped object
            /*list<QuestionsList__c> questionList=new list<QuestionsList__c>();
            
            questionList.add(TestDataFactory.createQuestionList('Is The Opportunity Real?','Opportunity WIN Plan','Q1',u.id));
            questionList.add(TestDataFactory.createQuestionList('Do We Have a Solution?','Opportunity WIN Plan','Q2',u.id));
            questionList.add(TestDataFactory.createQuestionList('Can We Win the Opportunity?','Opportunity WIN Plan','Q3',u.id));
            questionList.add(TestDataFactory.createQuestionList('Is the Value There?','Opportunity WIN Plan','Q4',u.id));
            
            insert questionList;*/
            Is_the_opp_real__c     rec1=TestDataFactory.createisTheOppReal(testOpp.id);
            rec1.ownerid=u.id;
            insert rec1;
            //commented by CG as part of descoped object
            /*Do_We_Have_a_Sol__c    rec2=TestDataFactory.createDoWeHaveSoln(testOpp.id);
            rec2.ownerid=u.id;
            insert rec2;*/
            //Can_We_Win_the_Opp__c  rec3=TestDataFactory.createCanWeWin(testOpp.id);
            //rec3.ownerid=u.id;
            //insert rec3;
            Is_the_Value_There__c  rec4=TestDataFactory.createisTheValueThere(testOpp.id);
            rec4.ownerid=u.id;
            insert rec4;
            //commented by CG as part of descoped object
            /*Competitive_Anaysis__c rec5=TestDataFactory.createCompAnalysis(testOpp.id);
            rec5.ownerid=u.id;
            insert rec5;*/
        }
    }
    
    public static testMethod void OpptyWinPlan_Page_Extension_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            //optWin.rowToRemove=1;
            optWin.showForm=true;
            optWin.selectedContactName='test';
            optWin.selectedTitleName='test';
            optWin.selectedUserName='test';
            //optWin.lstPolAndRelDelete=new List<Politics_and_Relationships__c>();
            //optWin.lstActAndTacInnerDelete=new List<actionAndTacticsInnerClass>();
            //optWin.lstBuyDecProcessInnerDelete=new List<buyDecisionProcessInnerClass>();
            //optWin.lstPriEvalCriteriaInnerDelete=new List<keyWinThemesInnerClass>();
            optWin.selectedContactId=null;
            optWin.selectedUserId=null;
            optWin.allContTitle=new list<contact>();
            optWin.allUser=new list<user>();
            optWin.rowFromVF1=1;
            List<SelectOption> op1=new List<SelectOption>();
            /*op1.addall(optWin.getRoles());
            op1.addall(optWin.getVendorPreferences());
            op1.addall(optWin.getTypeOfBuyer());
            op1.addall(optWin.getBuyerLocation());*/
            system.assertequals(optWin.oppty.id,testOpp.id);
            OpptyWinPlan_Page_Extension.questionWrapper qw= new OpptyWinPlan_Page_Extension.questionWrapper();
            qw.question='Test';
            qw.dates=String.ValueOf(Date.today());
            qw.evidence='Test Evidenece';          
        }
        Test.Stoptest();
    }
    
    public static testMethod void SaveOptyWin_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            optWin.SaveOptyWin();
            ApexPages.StandardController stdCtrl1=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin1=new OpptyWinPlan_Page_Extension(stdCtrl1);
            optWin1.SaveOptyWin();
            system.assertequals(optWin.oppty.isNew__c,true);
            optWin1.cancel();
            OpptyWinPlan_Page_Extension.questionWrapperDS qwd = new OpptyWinPlan_Page_Extension.questionWrapperDS();
            qwd.question='Test';
            qwd.dates=String.valueOf(Date.today());
            qwd.evidence='Test Evidenece';
        }
        Test.Stoptest();
    }
    public static testMethod void addNewRow_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            //apexpages.currentpage().getparameters().put('addRowObjectName','Politics_and_Relationships__c');
            optWin.addNewRow();
            system.assertequals(optWin.addRow,'addRow');
            OpptyWinPlan_Page_Extension.questionWrapperCO qwc = new OpptyWinPlan_Page_Extension.questionWrapperCO();
            qwc.question='Test';
            qwc.dates=String.valueOf(Date.today());
            qwc.evidence='Test Evidenece';
        }
        Test.Stoptest();
    }
    
    public static testMethod void removeNewRow_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            //optWin.rowToRemove=1;
            optWin.addNewRow();
            //apexpages.currentpage().getparameters().put('addRowObjectName','Politics_and_Relationships__c');
            optWin.removeNewRow();
            //system.assertequals(optWin.count,1);
            OpptyWinPlan_Page_Extension.questionWrapperIV qwV = new OpptyWinPlan_Page_Extension.questionWrapperIV();
            qwV.question='Test';
            qwV.dates=String.valueOf(Date.today());
            qwV.evidence='Test Evidenece';
        }
        Test.Stoptest();
    }
    
    public static testMethod void showPopup_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            optWin.objName='Contact';
            optWin.showPopup();
            optWin.objName2='User';
            optWin.showPopup();
            system.assertequals(optWin.displayNamePopup,true);
            OpptyWinPlan_Page_Extension.compAnalysisWrapper cqw = new OpptyWinPlan_Page_Extension.compAnalysisWrapper();
            cqw.contention='Test';
            cqw.parity='Test';
            cqw.differentiation='Test diff';
        }
        Test.Stoptest();
    }
    
    public static testMethod void closePopup_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            optWin.closePopup();
            system.assertequals(optWin.displayNamePopup,false);
        }
        Test.Stoptest();
    }
    
    public static testMethod void getId_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            optWin.getId();
            system.assertequals(optWin.displayNamePopup,false);
            OpptyWinPlan_Page_Extension.actionAndTacticsInnerClass atc= new OpptyWinPlan_Page_Extension.actionAndTacticsInnerClass();
            atc.rowNumber=0;
            system.assertequals(atc.rowNumber,0);
        }
        Test.Stoptest();
    }
    
    public static testMethod void searchRecord_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            opportunity testOpp=[Select id from opportunity limit 1];
            apexpages.currentpage().getparameters().put('id',testOpp.id);
            ApexPages.StandardController stdCtrl=new ApexPages.StandardController(testOpp);
            OpptyWinPlan_Page_Extension optWin=new OpptyWinPlan_Page_Extension(stdCtrl);
            optWin.searchString='test';
            optWin.objName='Contact';
            optWin.searchRecord();
            optWin.objName2='User';
            optWin.searchRecord();
            system.assertequals(optWin.objName,'Contact');
            system.assertequals(optWin.objName2,'User');
            OpptyWinPlan_Page_Extension.innerClass ic= new OpptyWinPlan_Page_Extension.innerClass();
            ic.rowNumber=0;
            system.assertequals(ic.rowNumber,0);
        }
        Test.Stoptest();
    }

}