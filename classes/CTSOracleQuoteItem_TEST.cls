@isTest
Public class CTSOracleQuoteItem_TEST {
  static testmethod void test1(){
    Division__c divn = new Division__c();
    divn.Name = 'Test Division';
    divn.Division_Type__c = 'Customer Center';
    divn.EBS_System__c = 'Oracle 11i';     
    insert divn;     
    
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for Oracle Quote Item Deletion';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
    a.ShippingStreet = '13, street2';
    a.CTS_Global_Shipping_Address_1__c = '13';
    a.CTS_Global_Shipping_Address_2__c = 'street2';
    a.ShippingPostalCode = '123';  
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
 // a.Account_Division__c= 'a2c4Q000006rW5NQAU'; //Ritesh:Added division to avoid division validation on Accounts.
    a.Account_Division__c = divn.Id;
    insert a;
    Id airOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('NA_Air').getRecordTypeId();
    Opportunity o = new Opportunity();
    o.name = 'Test Opportunity';
    o.stagename = 'Qualify';
    o.amount = 0;
    o.closedate = system.today();
    o.Accountid=a.Id;
    o.RecordTypeId = airOppRTID;
    insert o;    
    cafsl__Oracle_Quote__c oq  = new cafsl__Oracle_Quote__c();
    oq.cafsl__Opportunity__c = o.Id;
    oq.Quote_Amount__c = 1;
    oq.Primary__c = true;
    insert oq;
    cafsl__Oracle_Quote_Line_Item__c oqi = new cafsl__Oracle_Quote_Line_Item__c();
    oqi.cafsl__Oracle_Quote__c=oq.Id;
    oqi.cafsl__CPQ_External_Id__c = '1';
    insert oqi;
    Test.startTest();
    Database.DeleteResult result= Database.delete(oqi, false);
    test.stopTest();
  }
}