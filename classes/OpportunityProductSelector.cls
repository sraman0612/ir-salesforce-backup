/**
 * @File Name          : OpportunityProductSelector.cls
 * @Description        : 
 * @Author             : Murthy Tumuluri
 * @Group              : 
 * @Last Modified By   : Murthy Tumuluri
 * @Last Modified On   : 7/26/2019, 6:32:39 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0    6/19/2019, 10:29:45 AM   Murthy Tumuluri     Initial Version
**/
public class OpportunityProductSelector {
    //tktransport_model__c field Commented by Yadav Vineet capgemini for descoped field
    //Commented as part of EMEIA cleanup.
    //public static string OPPORTUNITY_RECORDTYPE_NA = 'TK_Transport_NA';
    //public static string OPPORTUNITY_RECORDTYPE_EMEA = 'TK_Transport_EMEA';
   
  /*  @AuraEnabled
    public static List<selectOption> getBaseModels(Id oppId){
        
        List<selectOption> selectList = new List<selectOption>();
        String objName = String.valueOf(oppId.getsobjecttype());
        String modelValue;

        if(objName == 'Opportunity'){

            Opportunity opp = getOppDetails(oppId);

            //System.debug('opp.TKTransport_Model__c'+opp.TKTransport_Model__c);
            //modelValue = opp.TKTransport_Model__c;

            System.debug('@@ modelValue'+modelValue);
            Product2 baseProd = [SELECT Id, Name 
                                FROM Product2 
                                WHERE Name =: modelValue];

            selectOption sOptionBase = new selectOption(baseProd.Name, 
                                                        baseProd.Id, 
                                                        true);
            selectList.add(sOptionBase);

            for(Product2 prod: [SELECT Id, Name,//TK_Sort_Description__c,
                                RecordType.Name,
                             //   TK_UnitPrice__c,tk_tkproductfamilymulti__c 
                                FROM Product2 
                                WHERE Type__c='Base Product' 
                                AND RecordType.DeveloperName 
                                    IN ('TK_Transport_NA')]){
//code commented by Yadav Vineet Capgemini for descoped field as per Prashanth 
                //if(prod.tk_tkproductfamilymulti__c.containsIgnoreCase(opp.TK_ProductFamily__c)){
                                        {
                    selectOption sOption = new selectOption(prod.Name, 
                                                            prod.Id, 
                                                            false);
                    selectList.add(sOption);
                }

            }
        }else if(objName == 'TK_Transport_PDR__c'){
            TK_Transport_PDR__c pdr = getPDRDetails(oppId);


            modelValue = pdr.Model__c;

            System.debug('@@ modelValue'+modelValue);
            Product2 baseProdPDR = [SELECT Id, Name 
                                FROM Product2 
                                WHERE Name =: modelValue];

            selectOption sOptionPDRBase = new selectOption(baseProdPDR.Name, 
                                                        baseProdPDR.Id, 
                                                        true);
            selectList.add(sOptionPDRBase);

            for(Product2 prodPRD: [SELECT Id, Name,TK_Sort_Description__c,
                                RecordType.Name,
                                TK_UnitPrice__c,tk_tkproductfamilymulti__c 
                                FROM Product2 
                                WHERE Type__c='Base Product' 
                                AND RecordType.DeveloperName 
                                    IN ('TK_Transport_EMEA')]){

                if(prodPRD.tk_tkproductfamilymulti__c.containsIgnoreCase(
                                                pdr.Product_Family__c)){

                    selectOption sOptionPDR = new selectOption(prodPRD.Name, 
                                                            prodPRD.Id, 
                                                            false);
                    selectList.add(sOptionPDR);
                }

            }
        }
        

        
        return selectList;
    }*/


   /* @AuraEnabled
    public static List<Product2> getProductByOppModel(Id oppId){
        System.debug('@@ oppId'+oppId);

        String objName = String.valueOf(oppId.getsobjecttype());
        Id baseProdId;

        if(objName == 'Opportunity'){
        
            Opportunity opp = getOppDetails(oppId);

            //baseProdId = [SELECT Id FROM Product2 WHERE Name =: opp.TKTransport_Model__c].Id;
        }else if(objName == 'TK_Transport_PDR__c'){
            TK_Transport_PDR__c pdr = getPDRDetails(oppId);
            baseProdId = [SELECT Id 
                        FROM Product2 
                        WHERE Name =: pdr.Model__c].Id;
        }

        return getProductByModel(baseProdId);
        
    }*/

  /*  @AuraEnabled
    public static List<Product2> getProductByModel(Id prodId){
        System.debug('@@ prodId'+prodId);
        String recordTypeNA  = [SELECT RecordType.DeveloperName FROM Product2 WHERE Id = : prodId].RecordType.DeveloperName;
        List<Product2> prodList = [SELECT Id, Name, TK_UnitPrice__c, TK_Sub_Type__c,
                                    TK_Sort_Description__c,TK_Tab_Category__c,Type__c,
                                    TKM_Model_Group__c,TK_Base_Product__c,TK_TKProductFamilyMULTI__c,
                                    RecordType.Name,TK_Dealer_Option_Cost__c
                                    FROM Product2 
                                    WHERE  TK_Base_Product__c =: prodId 
                                    ORDER BY TK_Sort_Description__c];
        if(recordTypeNA == 'TK_Transport_NA'){

            List<Product2> dealerProdList = [SELECT Id, Name, TK_UnitPrice__c, TK_Sub_Type__c,
                                        TK_Sort_Description__c,TK_Tab_Category__c,Type__c,
                                        TKM_Model_Group__c,TK_Base_Product__c,TK_TKProductFamilyMULTI__c,
                                        RecordType.Name,TK_Dealer_Option_Cost__c
                                        FROM Product2 
                                        WHERE Type__c = 'Dealer Option' 
                                        AND RecordType.DeveloperName = 'TK_Transport_NA'
                                        ORDER BY TK_Sort_Description__c];

            System.debug('@@ dealerProdList : '+dealerProdList);
            prodList.addAll(dealerProdList);
        }
        System.debug('@@ prodList : '+prodList);
        return prodList;
    }*/

    @AuraEnabled
    public static Product2 getCardProduct(Id prodId, Id recId){
        System.debug('@@ prodId: '+prodId);
        Product2 prod = [SELECT Id, Name,// TK_UnitPrice__c,Model__c,
                        RecordType.Name//TK_TKProductFamilyMULTI__c,
                       /* TKM_Model_Group__c,TK_Sort_Description__c,TK_ProductImage__c,
                        TK_Base_Product__c,TK_Tab_Category__c, TK_Sub_Type__c*/
                        FROM Product2 
                        WHERE  Id =: prodId];
        

        String objName = String.valueOf(recId.getsobjecttype());
        if(objName == 'Opportunity'){

            Opportunity opp = getOppDetails(recId);
            //code commented by Yadav Vineet Capgemini for descoped field as per Prashanth 
            //opp.TK_ProductFamily__c = prod.TK_TKProductFamilyMULTI__c;
            //opp.TKTransport_Model__c = prod.Model__c;
            update opp;
        }/*else if(objName == 'TK_Transport_PDR__c'){
            TK_Transport_PDR__c pdr = getPDRDetails(recId);

            pdr.Product_Family__c= prod.TK_TKProductFamilyMULTI__c;
            pdr.Model__c = prod.Model__c;
            update pdr;
        }*/
        System.debug('@@ prod: '+prod);
        return prod;
    }
    @AuraEnabled
    public static Opportunity getOppDetails(Id oppId){
        //code commented by Yadav Vineet Capgemini for descoped field as per Prashanth 
        Opportunity opp = [SELECT Id, //TK_ProductFamily__c, 
                           StageName,
                            //TKTransport_Model__c,
                           RecordType.DeveloperName/*, 
                            (SELECT Id, Opportunity__c, Model__c, 
                            Product_Family__c FROM PDR_s__r)*/
                            FROM Opportunity 
                            WHERE Id =: oppId];
        return opp;
    }/*Commented as part of EMEIA cleanup.
    @AuraEnabled
    public static TK_Transport_PDR__c getPDRDetails(Id pdrId){
        TK_Transport_PDR__c pdr = [SELECT Id, Model__c, 
                            Product_Family__c,Opportunity_Stage__c
                            FROM TK_Transport_PDR__c 
                            WHERE Id =: pdrId];
        return pdr;
    }*/
    @AuraEnabled
    public static Boolean getPriceCheck(Id recId){
        System.debug('@@ recId : '+recId);
        
        String objName = String.valueOf(recId.getsobjecttype());
        Boolean  priceCheck;
        if(objName == 'Opportunity'){
            Opportunity opp = getOppDetails(recId);
            if(opp.StageName == 'Stage 3. Propose/Quote'
            || opp.StageName == 'Stage 5. Closed Lost'
            || opp.StageName == 'Stage 5. Closed Won' ){
                priceCheck = true;
            }else{
                priceCheck = false;
            }
        }/*else if(objName == 'TK_Transport_PDR__c'){
            TK_Transport_PDR__c pdr = getPDRDetails(recId);
            if(pdr.Opportunity_Stage__c == 'Stage 3. Propose/Quote'
            || pdr.Opportunity_Stage__c == 'Stage 0. Closed Lost'
            || pdr.Opportunity_Stage__c == 'Stage 0. Closed Lost'
            || pdr.Opportunity_Stage__c == 'Stage 5. Closed Won' ){
                priceCheck = true;
            }else{
                priceCheck = false;
            }
        }*/else{
            priceCheck = false;
        }

        System.debug('@@ priceCheck : '+priceCheck);
        return priceCheck;
    }
    @AuraEnabled
    public static String addOppLineItems(List<Product2> prodList,
                                        Id oppId,
                                        Product2 baseModelProd){
        
        String returnMessage;
        Map<Id,Product2> prodMap = new Map<Id,Product2>(prodList);
        Map<String,OpportunityLineItem> oppLineMap = new Map<String,OpportunityLineItem>();
        List<OpportunityLineItem> oppLineInsertList = new List<OpportunityLineItem>();
        List<OpportunityLineItem> oppLineUpdateList = new List<OpportunityLineItem>();
//Commented as part of EMEIA cleanup.
      /*  Map<String,TK_Transport_PDR_Line_Item__c> exisitngPDRLineMap 
                        = new Map<String,TK_Transport_PDR_Line_Item__c>();

        List<TK_Transport_PDR_Line_Item__c> updatePDRLine 
                        = new List<TK_Transport_PDR_Line_Item__c>();

        List<TK_Transport_PDR_Line_Item__c> insertPDRLine 
                        = new List<TK_Transport_PDR_Line_Item__c>();*/

        String objName = String.valueOf(oppId.getsobjecttype());

        System.debug('@@ baseModelProd'+baseModelProd);
        
        if(objName == 'Opportunity'){
            
            Opportunity thisOpp = [SELECT CurrencyIsoCode, Pricebook2Id, 
                                RecordType.DeveloperName 
                                   //TKTransport_Model__c
                                FROM Opportunity 
                                WHERE Id =: oppId];


            System.debug('@@ Opp Item : '+prodList+'  @@ oppId : '+oppId);
            System.debug('@@ prodMap : '+prodMap);

            for(OpportunityLineItem oppLine : [SELECT Id, Product2Id, Quantity,
                                                    Product2.Name, PricebookEntryId 
                                                    FROM OpportunityLineItem 
                                                    WHERE OpportunityId =: oppId]){
                oppLineMap.put(oppLine.Product2.Name, oppLine);
            }
             //code commented by Yadav Vineet Capgemini for descoped field as per Prashanth 
            //thisOpp.TK_ProductFamily__c = baseModelProd.TK_TKProductFamilyMULTI__c;
            //thisOpp.TKTransport_Model__c = baseModelProd.Model__c;
            Update thisOpp;
            for(PricebookEntry pbe: [SELECT Id, Product2Id, Pricebook2Id, 
                                    UnitPrice, Product2.Name, PRODUCT2.Type__c // PRODUCT2.TK_Tab_Category__c
                                    FROM PriceBookEntry 
                                    WHERE (Product2Id IN : prodMap.keySet()
                                        OR Product2Id =: baseModelProd.Id)
                                    AND CurrencyIsoCode =: thisOpp.CurrencyISOCode
                                    AND Pricebook2.Name != 'Standard Price Book']){
                Product2 prod = prodMap.get(pbe.Product2Id);
                if(!oppLineMap.containsKey(pbe.Product2.Name)){
                    System.debug('@@ pbe : '+pbe);
                    OpportunityLineItem oppLine = new OpportunityLineItem();
                    oppLine.OpportunityId = oppId;
                    oppLine.PricebookEntryId = pbe.Id;
                    oppLine.Quantity = 1;
                   //Commented as part of EMEIA cleanup.
                    if(pbe.PRODUCT2.Type__c == 'Dealer Option' ){
                      // || pbe.PRODUCT2.TK_Tab_Category__c == 'Dealer Option' ){
                         //   oppLine.UnitPrice = prod.TK_Dealer_Option_Cost__c;
                    }else{
                            oppLine.UnitPrice = pbe.UnitPrice;
                    }
                    oppLineInsertList.add(oppLine);
                }else{
                    OpportunityLineItem oppLineExisting = oppLineMap.get(pbe.Product2.Name);
                    oppLineExisting.Quantity = oppLineExisting.Quantity+1;
                    //Commented as part of EMEIA cleanup.
                    if(pbe.PRODUCT2.Type__c == 'Dealer Option' ){
                     //   || pbe.PRODUCT2.TK_Tab_Category__c == 'Dealer Option' ){
                       //     oppLineExisting.UnitPrice = prod.TK_Dealer_Option_Cost__c;
                    }else{
                            oppLineExisting.UnitPrice = pbe.UnitPrice;
                    }
                    oppLineUpdateList.add(oppLineExisting);
                }
            }
            
            System.debug('@@ oppLineList : '+oppLineInsertList);
            if(!oppLineInsertList.isEmpty()){
                try{
                    insert oppLineInsertList;
                    returnMessage = oppId;
                }catch(DMLException e){
                    returnMessage = e.getMessage();
                }
            }
            if(!oppLineUpdateList.isEmpty()){
                try{
                    update oppLineUpdateList;
                    returnMessage = oppId;
                }catch(DMLException e){
                    returnMessage = e.getMessage();
                }
            }
        }
         //commented by CG as part of descoped object
       /* else if(objName == 'TK_Transport_PDR__c'){
            System.debug('@@ baseModelProd'+baseModelProd);
            //Commented as part of EMEIA cleanup.
            TK_Transport_PDR__c thisPDR = [SELECT Id, Model__c, Product_Family__c,
                                Opportunity__r.CurrencyISOCode
                                FROM TK_Transport_PDR__c 
                                WHERE Id =: oppId];
            System.debug('@@ thisPDR'+thisPDR);
            for(TK_Transport_PDR_Line_Item__c pdrLine: [SELECT Id, Product__c, PDR__c, 
                                                        Product__r.Name,
                                                        Price_Book__c, Quantity__c, 
                                                        List_Price__c,CCN_Part_Number__c 
                                                        FROM TK_Transport_PDR_Line_Item__c 
                                                        WHERE PDR__c =: thisPDR.Id]){
                exisitngPDRLineMap.put(pdrLine.Product__r.Name,pdrLine);
            }
            System.debug('@@ exisitngPDRLineMap'+exisitngPDRLineMap);
            thisPDR.Product_Family__c= baseModelProd.TK_TKProductFamilyMULTI__c;
            thisPDR.Model__c = baseModelProd.Model__c;
            update thisPDR;
            System.debug('@@ thisPDR'+thisPDR);
            for(PricebookEntry pbe: [SELECT Id, Product2Id, Pricebook2Id,
                                    UnitPrice, Product2.Name,  PRODUCT2.Type__c, PRODUCT2.TK_Tab_Category__c,
                                    Product2.TK_TKProductFamilyMULTI__c,
                                    Product2.Model__c
                                    FROM PriceBookEntry 
                                    WHERE (Product2Id IN : prodMap.keySet()
                                        OR Product2Id =: baseModelProd.Id)
                                    //AND CurrencyIsoCode =: thisPDR.Opportunity__r.CurrencyISOCode
                                    AND Pricebook2.Name != 'Standard Price Book']){
                Product2 prod = prodMap.get(pbe.Product2Id);
                if(exisitngPDRLineMap.containsKey(pbe.Product2.Name)){
                    TK_Transport_PDR_Line_Item__c existingPDRLine = exisitngPDRLineMap.get(pbe.Product2.Name);
                    existingPDRLine.Quantity__c = existingPDRLine.Quantity__c+1;
                    if(pbe.PRODUCT2.Type__c == 'Dealer Option' 
                        || pbe.PRODUCT2.TK_Tab_Category__c == 'Dealer Option' ){
                            existingPDRLine.List_Price__c = prod.TK_Dealer_Option_Cost__c;
                    }else{
                            existingPDRLine.List_Price__c = pbe.UnitPrice;
                    }
                    updatePDRLine.add(existingPDRLine);
                }else{
                    TK_Transport_PDR_Line_Item__c newPDRLine = new TK_Transport_PDR_Line_Item__c();
                    newPDRLine.Product__c = pbe.Product2Id;
                    newPDRLine.Price_Book__c = pbe.Pricebook2Id;
                    newPDRLine.Quantity__c = 1;
                    if(pbe.PRODUCT2.Type__c == 'Dealer Option' 
                        || pbe.PRODUCT2.TK_Tab_Category__c == 'Dealer Option' ){
                            newPDRLine.List_Price__c = prod.TK_Dealer_Option_Cost__c;
                    }else{
                            newPDRLine.List_Price__c = pbe.UnitPrice;
                    }
                    newPDRLine.PDR__c = thisPDR.Id;
                    insertPDRLine.add(newPDRLine);
                }
            }
            //System.debug('@@ insertPDRLine'+insertPDRLine);
            //ystem.debug('@@ updatePDRLine'+updatePDRLine);
            if(!updatePDRLine.isEmpty()){
                try{
                    update updatePDRLine;
                    returnMessage = oppId;
                }catch(DMLException e){
                    returnMessage = e.getMessage();
                }
            }
            if(!insertPDRLine.isEmpty()){
                try{
                    insert insertPDRLine;
                    returnMessage = thisPDR.Id;
                }catch(DMLException e){
                    returnMessage = e.getMessage();
                }
                
            }
            else if(insertPDRLine.isEmpty() && updatePDRLine.isEmpty()){
                returnMessage = 'Nothing to Insert';
            }

        } */
        return returnMessage;
    }
    public class selectOption {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public boolean selected;
        public selectOption(String label, Id id, boolean selected){
            this.label = label;
            this.id = id;
            this.selected = selected;
        }
    }
}