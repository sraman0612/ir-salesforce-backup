public with sharing class CTS_IOT_Site_DetailsController {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public Account site;
    }

    @AuraEnabled
    public static InitResponse getInit(Id siteId){

        InitResponse response = new InitResponse();

        try{
            response.site = CTS_IOT_Data_Service.getCommunityUserSites(siteId)[0];            
        }
        catch(Exception e){
            system.debug('exception: ' + e);
        }

        return response;
    }
}