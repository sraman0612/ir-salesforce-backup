@isTest
public class FlowApexMixinsServiceTest {
	public static testMethod void testExecute()
    {
        FlowApexMixinsService.Request request = new FlowApexMixinsService.Request();
        request.method = 'splitString';
        request.target = 'test@test.com;example@example.com';
        
        FlowApexMixinsService.Request[] requests = new FlowApexMixinsService.Request[]{request};
            
        FlowApexMixinsService.execute(requests);
    }
}