public class CTS_TechDirectArticleFileVisibility {
 
    public static void ShareFilesWithCommunityUsers(List<ContentDocumentLink> newCDLList){
        try{
            //get CTS RecordTypeIds
            List<Id> ctsKnowledge_kavRecordTypeIdList = new List<Id>();
            ctsKnowledge_kavRecordTypeIdList.add(Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().get('IR Comp TechDirect Knowledge Article').getRecordTypeId());
            
            //get Knowledge__kav prefix i.e. 500
            Schema.DescribeSObjectResult r = Knowledge__kav.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            
            //get attachments related to Knowledge__kavs
            List<Id> Knowledge_kavIdList = new List<Id>();
            for(ContentDocumentLink cdlVar: newCDLList){
                if((String.valueOf(cdlVar.LinkedEntityId)).startsWith(keyPrefix)){
                    Knowledge_kavIdList.add(cdlVar.LinkedEntityId);
                }
            }
            
            //Added by MR Team to avoid SOQL LIMIT Exceed Exception - 05/02/2025
            if(Knowledge_kavIdList.size() > 0){
               //Link the CTSRecordtypes with Knowledge__kav
                Map<Id, Knowledge__kav> ctsKnowledge_kavMap = new Map<Id, Knowledge__kav>([Select Id, RecordTypeId  From Knowledge__kav  Where Id IN: Knowledge_kavIdList AND RecordTypeId IN: ctsKnowledge_kavRecordTypeIdList]);
                
                Set<Id> Knowledge_kavIdsList = ctsKnowledge_kavMap.keySet();
                
                //Load the documents with defined ShareType and Visibility
                for(ContentDocumentLink cdl: newCDLList){
                    if(Knowledge_kavIdsList.contains(cdl.LinkedEntityId)){
                        cdl.ShareType = 'I';
                        cdl.Visibility = 'AllUsers';
                    }
                } 
            }
            
        } catch(Exception ex){
            System.debug('##Exception occured while Updating File Sharing: ' + ex.getMessage() + '\n in class \'CTS_TechDirectArticleFileVisibility\' at line# '+ex.getLineNumber());
        }
    }
}