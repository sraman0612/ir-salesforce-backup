public class CTSCommunityCacheCaller {

    private static final String PARTITION_NAME = 'local.CTSOpportunityId';
    private static final String KEY_CALLER = 'OppId';
    
    public static Id getOppId(){
    
        if (getPartition().contains(KEY_CALLER)){
            return (Id)getPartition().get(KEY_CALLER);    
        }
        return null;
    }
    
    public static void setOppId(Id callerId){
        if (callerId != null){
            getPartition().put(KEY_CALLER, callerId);    
        }
    }
    
    public static void clearCache(){
        getPartition().remove(KEY_CALLER);
    }
    
    private static Cache.SessionPartition getPartition(){
        return Cache.Session.getPartition(PARTITION_NAME);
    }
    
    public static String getAsJson() {
    
        Map<String, String> ids = new Map<String, String>();
        ids.put('callerId', getOppId());
        return JSON.serialize(ids, true);
    }


}