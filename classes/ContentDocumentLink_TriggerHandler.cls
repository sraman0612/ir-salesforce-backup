// Handler Class for ContentDocumentLinkTrigger
//
// Created on May 5, 2024 by Artur Poiata
public class ContentDocumentLink_TriggerHandler {
    
    public static void afterInsert(map<Id, ContentDocumentLink> newMap){
       
        updateSalesCallPlanNotesTitle(newMap);
        
    }
    
    public static void updateSalesCallPlanNotesTitle (map<Id, ContentDocumentLink> newMap){
        set<Id> targetDocumentIds = new set<Id>();
        
        for(ContentDocumentLink currDocLink : newMap.values()){
            targetDocumentIds.add(currDocLink.ContentDocumentId);   
        }
        
        List<ContentDocument> cdList = [SELECT Id, FileType FROM ContentDocument WHERE Id IN:targetDocumentIds AND FileType=:'SNOTE'];
        set<Id> filteredContentDocList = new set<Id>();
        
        for(ContentDocument currentContentDoc : cdList){
            filteredContentDocList.add(currentContentDoc.Id);
        }
        
        Schema.sObjectType objectType = Schema.getGlobalDescribe().get('Opportunity_Sales_Call_Plan__c');
        String salesCallPlanPrefix =  objectType.getDescribe().getKeyPrefix();
        
        map<Id, set<Id>> salesCallPlanByContDocId = new map<Id, set<Id>>();
        
        for(ContentDocumentLink currDocLink : newMap.values()){
            if(filteredContentDocList.contains(currDocLink.ContentDocumentId) && String.valueOf(currDocLink.LinkedEntityId).contains(salesCallPlanPrefix)){
                set<Id> localList = new set<Id>();
                if(!salesCallPlanByContDocId.containsKey(currDocLink.LinkedEntityId)){
                    localList.add(currDocLink.ContentDocumentId);
                    salesCallPlanByContDocId.put(currDocLink.LinkedEntityId, localList);
                }else{
                    salesCallPlanByContDocId.get(currDocLink.LinkedEntityId).add(currDocLink.ContentDocumentId);
                }
            }
        }
        
        if(salesCallPlanByContDocId.size() > 0){            
            map<Id, String> salesCallPlanNameById = new map<Id, String>();
            
            List<Opportunity_Sales_Call_Plan__c> salesCallPlanList = [SELECT Id, Name FROM Opportunity_Sales_Call_Plan__c where Id IN:salesCallPlanByContDocId.keySet()];  
            
            for(Opportunity_Sales_Call_Plan__c currRecord : salesCallPlanList){
                salesCallPlanNameById.put(currRecord.Id, currRecord.Name);
            }
                        
            List<ContentDocumentLink> targetContentDocLink = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink where LinkedEntityId IN:salesCallPlanByContDocId.keySet()];
            
            map<Id, set<Id>> salesCallPlanByContentDoc = new map<Id, set<Id>>();
            
            for(ContentDocumentLink currRecord : targetContentDocLink){
                if(!salesCallPlanByContentDoc.containsKey(currRecord.LinkedEntityId)){
                    set<Id> localList = new set<Id>();
                    localList.add(currRecord.ContentDocumentId);
                    salesCallPlanByContentDoc.put(currRecord.LinkedEntityId, localList);
                }else{
                    salesCallPlanByContentDoc.get(currRecord.LinkedEntityId).add(currRecord.ContentDocumentId);
                }
            }
            
            //Store values for each note title 
            map<Id, String> noteTitleById = new map<Id, String>();
            
            for(Id salesCallPlanId : salesCallPlanByContDocId.keySet()){
                for(Id contentDocId : salesCallPlanByContDocId.get(salesCallPlanId)){
                    noteTitleById.put(contentDocId, salesCallPlanNameById.get(salesCallPlanId) + ' ' + salesCallPlanByContentDoc.get(salesCallPlanId).size());
                }
            }
                        
            List<ContentDocument> finalList = [SELECT Id, Title from ContentDocument where Id IN:salesCallPlanByContDocId.values()[0]];
            
            for(ContentDocument currentRecord: finalList){
                currentRecord.Title = noteTitleById.get(currentRecord.Id);
            }
                        
            update finalList;
        }
    }
}