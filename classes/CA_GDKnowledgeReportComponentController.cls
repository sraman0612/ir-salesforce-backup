/*
* Revisions:
08-Oct-2024 : Update code against Case #03123284 - AMS Team
*/
public class CA_GDKnowledgeReportComponentController {   
    Set<Id>articalIds = new Set<Id>();
    
/*
* Revisions:
08-Oct-2024 : Update code against Case #03123284 - AMS Team
*/
    public String getArticlesQuery() {
        /** Changes Done for case #02994530
// To segregate the data based on Data Category */
        
        try{
            List<Knowledge__kav> knowledgeList =[SELECT id, ArticleNumber, (SELECT Id, DataCategoryName, DataCategoryGroupName FROM DataCategorySelections) FROM Knowledge__kav ORDER BY LastPublishedDate DESC limit 500 ];
            Set<String> filterCategories = new Set<String>();
            List<String> articleNos = new  List<String>();		// Create a list to store the relevant article numbers: #03123284 - AMS Team
            for (CA_GDProductCategory__mdt categoryRecord : [SELECT FilterCategory__c FROM CA_GDProductCategory__mdt]) {
                filterCategories.add(categoryRecord.FilterCategory__c);
            }
            if(knowledgeList !=null && !knowledgeList.isEmpty()){
                for (Knowledge__kav knowledge : knowledgeList) {
                    
                    for (Knowledge__DataCategorySelection dc : knowledge.DataCategorySelections) {
                        if (filterCategories.contains(dc.DataCategoryName)) {
                            articleNos.add(knowledge.ArticleNumber) ;
                        }
                    }
                }
            }
            // Storing articles Id with same article numbers irrespective of their translated versions: #03123284 - AMS Team
            if(knowledgeList !=null && !knowledgeList.isEmpty()){
                for(Knowledge__kav knowledgeVar:knowledgeList){
                    if( articleNos.contains(knowledgeVar.ArticleNumber) ){
                        articalIds.add(knowledgeVar.Id);
                    }  
                }
            }     
            //system.debug(articalIds.size());
            //system.debug('article Ids are>>'+articalIds);
            
       }catch(Exception e){
            System.debug('Error Message>>'+e.getMessage());   
       }
        String query = 'SELECT Id, ArticleNumber, Title, URLName, ValidationStatus, PublishStatus, CTS_TechDirect_Article_Type__c,CTS_TechDirect_Access_Level__c, CTS_TechDirect_Author_Reviewer__r.Name,CreatedBy.Name, LastModifiedBy.Name, Language, IsVisibleInApp, IsVisibleInCsp, IsVisibleInPkb, IsVisibleInPrm FROM Knowledge__kav WHERE PublishStatus = \'Online\'';        
        if(!Test.isRunningTest()) {
            query = query + ' AND LastModifiedDate = YESTERDAY' ; // LAST_N_DAYS:15
        }         
        query =query+' AND Id IN:articalIds';
        query = query + ' AND IsVisibleInCsp = true';
        query = query + ' AND IsLatestVersion = true';
        query = query + ' AND CTS_TechDirect_Article_Type__c  != \'Tech Note\'';
        query = query + ' AND CTS_TechDirect_Include_on_Email__c  = true';
        query = query + ' limit 500';
        return query;
    }
    
    public List < KnowledgeReportWrapper > getArticleList() {
        List < KnowledgeReportWrapper > articleWrapperList = new List < KnowledgeReportWrapper > ();
        System.debug('##knowledgeList size::'+Database.query(getArticlesQuery()).size());
        for(Knowledge__kav knowledgeObj: (List < Knowledge__kav >) Database.query(getArticlesQuery())) {
            articleWrapperList.add(new KnowledgeReportWrapper(knowledgeObj));
        }
        return articleWrapperList;
    }
    
    @testVisible
    private class KnowledgeReportWrapper {
        public Knowledge__kav knowledgeObj { get; set; }
        
        public String getLanguageName() {
            String languageName = '';
            if(knowledgeObj != null && String.isNotBlank(knowledgeObj.language)) {
                if(knowledgeObj.language == 'en_US') {                        
                    languageName = 'English';
                } else if(knowledgeObj.language == 'zh_CN') {
                    languageName = 'Chinese (Simplified)';
                } else if(knowledgeObj.language == 'zh_TW') {
                    languageName = 'Chinese (Traditional)';
                } else if(knowledgeObj.language == 'nl_NL') {
                    languageName = 'Dutch';
                } else if(knowledgeObj.language == 'da') {
                    languageName = 'Danish';
                } else if(knowledgeObj.language == 'fi') {
                    languageName = 'Finnish';
                } else if(knowledgeObj.language == 'fr') {
                    languageName = 'French';
                } else if(knowledgeObj.language == 'de') {
                    languageName = 'German';
                } else if(knowledgeObj.language == 'it') {
                    languageName = 'Italian';
                } else if(knowledgeObj.language == 'ja') {
                    languageName = 'Japanese';
                } else if(knowledgeObj.language == 'ko') {
                    languageName = 'Korean';
                } else if(knowledgeObj.language == 'no') {
                    languageName = 'Norwegian';
                } else if(knowledgeObj.language == 'pt_BR') {
                    languageName = 'Portuguese (Brazil)';
                } else if(knowledgeObj.language == 'ru') {
                    languageName = 'Russian';
                } else if(knowledgeObj.language == 'es') {
                    languageName = 'Spanish';
                } else if(knowledgeObj.language == 'es_MX') {
                    languageName = 'Spanish (Mexico)';
                } else if(knowledgeObj.language == 'sv') {
                    languageName = 'Swedish';
                } else if(knowledgeObj.language == 'th') {
                    languageName = 'Thai';
                } else {
                    languageName = 'Unknown';
                }
            }
            return languageName;
        }
        
        public String getArticleURL() {
            String urlName = '';
            if(knowledgeObj != null) {
                if(knowledgeObj.IsVisibleInCsp) {
                    urlName = communitySiteURL + '/s/detail/' + knowledgeObj.Id;
                } else {
                    urlName = internalSiteURL + '/one/one.app?#/sObject/' + knowledgeObj.Id + '/view';
                }
            }
            return urlName;
        }
        
        public KnowledgeReportWrapper(Knowledge__kav knowledgeObj) {
            this.knowledgeObj = knowledgeObj;
        }    
        
        public String internalSiteURL {
            get {
                if(String.isBlank(internalSiteURL)) {
                    //internalSiteURL = Url.getSalesforceBaseUrl().toExternalForm();
                    internalSiteURL =   url.getOrgDomainUrl().toExternalForm();
                    System.debug('internalSiteURL'+internalSiteURL);
                    System.debug('internalSiteURLtest'+url.getCurrentRequestUrl().toExternalForm());  
                    
                    
                    
                }
                return internalSiteURL;
            } set;
        }
        
        public String communitySiteURL {
            get {
                if(String.isBlank(communitySiteURL)) {
                    communitySiteURL = getCommunityURL(Label.CTS_TechDirect_Community_Path_Name);
                }
                return communitySiteURL;
            } set;
        } 
        
        private String getCommunityURL(String communityName) {
            //Commented below code to avoid using 'seeAllData = true' in test class
            //which is required if we use ConnectApi in code
            /*
ConnectApi.CommunityPage communityPage = ConnectApi.Communities.getCommunities();
for(ConnectApi.Community community: communityPage.Communities) {
if(community.status == ConnectApi.CommunityStatus.Live && community.urlPathPrefix == communityName) {
return community.siteURL;
}
}
*/
            
            //commnunity URL saved in Label
            return Label.CTS_TechDirect_Community_URL;
        }
    }
    
}