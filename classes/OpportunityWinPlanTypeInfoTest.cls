@isTest
public class OpportunityWinPlanTypeInfoTest {
     // Account Record Types
    public static final String RS_HVAC_RT_DEV_NAME = 'CTS_MEIA';
    public static final String AirNAAcct_RT_DEV_NAME = 'CTS_MEIA';
    
    // Opportunity Record Types
    public static final String RS_New_Business_RT_DEV_NAVE = 'CTS_MEIA';
    public static final String CTS_Global_New_RT_DEV_NAME = 'CTS_MEIA';
    
    //Opportunity Win Plan Record Types
    public static final String RS_Win_Plan_RT_DEV_NAME = 'CTS_Opportunity_Win_Plan';
    public static final String CTS_Win_Plan_RT_DEV_NAME = 'CTS_Opportunity_Win_Plan';
    
    @testSetup static void setup(){
        OpportunityWinPlan__c oppyWinPlan;
        String currentYear = Date.Today().Year().format().remove(',');
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        System.runAs(u){
            
            Account airdTestAccount = IRSPXTestDataFactory.createAccount('TestAcc1',AirNAAcct_RT_DEV_NAME);
            airdTestAccount.ownerid=u.id;
            insert airdTestAccount;
            
            opportunity testOpp1 = IRSPXTestDataFactory.createOpportunity('TestOpty1',CTS_Global_New_RT_DEV_NAME,airdTestAccount.id,'Stage 1. Qualify',50000);
            testOpp1.ownerid=u.id;
            insert testOpp1;
            
            oppyWinPlan = IRSPXTestDataFactory.createOpportunityWinPlan(testOpp1.id, CTS_Win_Plan_RT_DEV_NAME);
            insert oppyWinPlan;
			
            /*Commented as part of EMEIA Cleanup
            Account rhvacTestAccount = IRSPXTestDataFactory.createAccount('TestAcc2',RS_HVAC_RT_DEV_NAME);
            rhvacTestAccount.ownerid=u.id;
            insert rhvacTestAccount;
            
            opportunity testOpp2 = IRSPXTestDataFactory.createOpportunity('TestOpty2',RS_New_Business_RT_DEV_NAVE,rhvacTestAccount.id,'Request',50000);
            testOpp2.ownerid=u.id;
            insert testOpp2;
            
            oppyWinPlan = IRSPXTestDataFactory.createOpportunityWinPlan(testOpp2.id, RS_Win_Plan_RT_DEV_NAME);
            insert oppyWinPlan;
            */
        }
    }
    
    @isTest static void OpportunityWinPlanTypeTest(){
        OpportunityWinPlanTypeInfo OpportunityWinPlanType;
        Map<Id,Schema.RecordTypeInfo> recordTypeInfo;
        String developerName;
        OpportunityWinPlan__c owp;
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt.1'];
        
        System.runAs(u){
            owp = [select Id, Name, RecordTypeId from OpportunityWinPlan__c where opportunity__r.account.name = 'TestAcc1'];    
            OpportunityWinPlanType = OpportunityWinPlanTypeInfo.getInstance();  
            recordTypeInfo = OpportunityWinPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)owp.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            system.assertEquals(CTS_Win_Plan_RT_DEV_NAME, developerName);
            
               Account rhvacTestAccount = IRSPXTestDataFactory.createAccount('TestAcc2',RS_HVAC_RT_DEV_NAME);
            rhvacTestAccount.ownerid=u.id;
            insert rhvacTestAccount;
            
             try{
            owp = [select Id, Name, RecordTypeId from OpportunityWinPlan__c where opportunity__r.account.name = 'TestAcc2'];
            OpportunityWinPlanType = OpportunityWinPlanTypeInfo.getInstance();  
            recordTypeInfo = OpportunityWinPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)owp.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            system.assertEquals(RS_Win_Plan_RT_DEV_NAME, developerName);
            }
            catch(exception e){
                
            }
        }
        Test.Stoptest();
    }

}