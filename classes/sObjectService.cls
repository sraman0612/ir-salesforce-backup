public without sharing class sObjectService {    	 
    
    /* DML Methods */
    
    public static Database.SaveResult[] insertRecordsAndLogErrors(sObject[] records, String className, String methodName){
        
        Database.SaveResult[] saveResults = Database.insert(records, false);
        
        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
        
        for (Database.SaveResult saveResult : saveResults){
            
            if (!saveResult.isSuccess() && saveResult.getErrors().size() > 0){
                apexLogHandler logHandler = new apexLogHandler(className, methodName, saveResult.getErrors()[0].getMessage());
                apexLogs.add(logHandler.logObj);
            }
        }
        
        system.debug('errors to log: ' + apexLogs);
        
        if (apexLogs.size() > 0){
            insert apexLogs;
        }  
        
        return saveResults;      
    }    
    
    public static Database.SaveResult[] updateRecordsAndLogErrors(sObject[] records, String className, String methodName){
        
        Database.SaveResult[] saveResults = Database.update(records, false);
        
        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
        
        for (Database.SaveResult saveResult : saveResults){
            
            if (!saveResult.isSuccess() && saveResult.getErrors().size() > 0){
                apexLogHandler logHandler = new apexLogHandler(className, methodName, saveResult.getErrors()[0].getMessage());
                apexLogs.add(logHandler.logObj);
            }
        }
        
        system.debug('errors to log: ' + apexLogs);
        
        if (apexLogs.size() > 0){
            insert apexLogs;
        }        
        
        return saveResults;
    }
    
    // Only currently supports upserting using the record Id field
    public static Database.UpsertResult[] upsertRecordsAndLogErrors(sObject[] records, String className, String methodName){
        
        Database.UpsertResult[] saveResults = Database.upsert(records, false);
        
        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
        
        for (Database.UpsertResult saveResult : saveResults){
            
            if (!saveResult.isSuccess() && saveResult.getErrors().size() > 0){
                apexLogHandler logHandler = new apexLogHandler(className, methodName, saveResult.getErrors()[0].getMessage());
                apexLogs.add(logHandler.logObj);
            }
        }
        
        system.debug('errors to log: ' + apexLogs);
        
        if (apexLogs.size() > 0){
            insert apexLogs;
        }        
        
        return saveResults;
    }    

    public static Database.DeleteResult[] deleteRecordsAndLogErrors(sObject[] records, String className, String methodName){
        
        Database.DeleteResult[] deleteResults = Database.delete(records, false);
        
        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
        
        for (Database.DeleteResult deleteResult : deleteResults){
            
            if (!deleteResult.isSuccess() && deleteResult.getErrors().size() > 0){
                apexLogHandler logHandler = new apexLogHandler(className, methodName, deleteResult.getErrors()[0].getMessage());
                apexLogs.add(logHandler.logObj);
            }
        }
        
        system.debug('errors to log: ' + apexLogs);
        
        if (apexLogs.size() > 0){
            insert apexLogs;
        }  
        
        return deleteResults;      
    }       
    
    public static Approval.ProcessResult[] processApprovalRequestsAndLogErrors(Approval.ProcessRequest[] approvalRequests, String className, String methodName){
        
        Approval.ProcessResult[] approvalResults = Approval.process(approvalRequests, false);
        
        List<Apex_Log__c> apexLogs = new List<Apex_Log__c>();
        
        for (Approval.ProcessResult approvalResult : approvalResults){
            
            if (!approvalResult.isSuccess() && approvalResult.getErrors().size() > 0){
                apexLogHandler logHandler = new apexLogHandler(className, methodName, approvalResult.getErrors()[0].getMessage());
                apexLogs.add(logHandler.logObj);
            }
        }
        
        system.debug('errors to log: ' + apexLogs);
        
        if (apexLogs.size() > 0){
            insert apexLogs;
        }   
        
        return approvalResults;     
    }    
    
    /* Describe Methods */
    
    public static Id getRecordTypeId(sObjectType oType, String recordTypeName){
    	return oType.getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    }   
    
    public static Map<String, Schema.SObjectField> getObjectFieldTokenMap(sObjectType objType){
    	return objType.getDescribe().fields.getMap();    	
    }
    
    public static Map<String, DescribeFieldResult> getObjectFieldDescribeMap(sObjectType objType){
    	
    	Map<String, DescribeFieldResult> fieldDescMap = new Map<String, DescribeFieldResult>();
    	
    	for (Schema.SObjectField token : getObjectFieldTokenMap(objType).values()){  		
    		fieldDescMap.put(String.valueOf(token), token.getDescribe());
    	}
    	
    	return fieldDescMap;
    }
    
    public static List<String> getAllCreateableFields(SObjectType objType){
    	
    	List<String> createableFields = new List<String>();
    	
    	for (DescribeFieldResult fieldDesc : getObjectFieldDescribeMap(objType).values()){
    		
    		if (fieldDesc.isCreateable()){
    			createableFields.add(fieldDesc.getName());
    		}
    	}
    	
    	return createableFields;
    }
}