/**
 * Test class of CTS_TechDirect_ProcessOOOSettings
* Revisions:

14-Jan-2025 : SOQL issue resolved for Test class only case #03394776 - AMS Team
*/
@isTest
public class CTS_TechDirect_ProcessOOOSettingsTest {
	
    @isTest
    public static void testProcessCases(){
        test.startTest();
        List<Case> cases = [select id, Subject, OwnerId, RecordTypeId from Case];
        
        List<Case> resultCases = CTS_TechDirect_ProcessOOOSettings.processCases(cases);
        
        test.stopTest();
    }
    
    @testSetup
    public static void createTestData(){
        Account acc = CTS_TestUtility.createAccount('Test Account', true);
        
        Contact con = CTS_TestUtility.createContact('Contact', 'Test ', 'test@gmail.com', acc.id, true);
        
        User bkupUser = CTS_TestUtility.createUser(False);
        bkupUser.Business_Hours__c = 'IR Comp Business Hours - ME + India';
        insert bkupUser;
        
        List<Case> cases = new List<Case>();
        cases.add(CTS_TestUtility.createCase('Test Case', acc.Id, con.Id, false));
        cases.add(CTS_TestUtility.createCase('Test Case 1', acc.Id, con.Id, false));
        insert cases;
        
        Out_of_Office_Settings__c ooo = CTS_TestUtility.createOutOfOfficeSetting(true, UserInfo.getUserId(), true);
        
        Out_of_Office_Case_Routings__c caseRouting = CTS_TestUtility.createOutOfOfficeCaseRouting(UserInfo.getUserId(), bkupUser.id, true);
    }
    
}