/**
@author Ben Lorenz
@date 2OCT2019
@description Test class for CTS_LosantEventCaseEmailHandler
*/
@isTest
private class CTS_LosantEventCaseEmailHandlerTest {
    @TestSetup
    static void setupData() {
        Id accRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp EU').getRecordTypeId();
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.Currency__c = 'USD';
        acc.RecordTypeId = accRecTypeId;
        insert acc;
        system.debug('acc'+acc);
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset a = new Asset();
        a.Name = 'TestAsset';
        a.AccountId = acc.Id;
        a.SerialNumber = '001TestAsset001';
        a.RecordTypeId = assetRecTypeId;
        a.Division__c = 'Upper Midwest Area';
        insert a;
        
        Id recTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IR Comp RMS').getRecordTypeId();
        Case c = new Case();
        c.Subject = 'Test';
        c.RecordTypeId = recTypeId;
        c.AssetId = a.Id;
        insert c;
        
        LosantSettings__c hcs = new LosantSettings__c();
        hcs.OrgWideEmailId__c = UserInfo.getUserId();
        hcs.CTS_RMS_CASE_RT_ID__c = recTypeId;
        hcs.EmailTemplateName__c = 'CTS_RMS_Losant_Event_Case';
        insert hcs;
    }
    @isTest static void testCase1() {
        Case c = [SELECT Id, AssetId, Subject, CaseNumber FROM Case];
        CTS_LosantEventCaseEmailHandler.sendEmails(new List<Case>{c});
    }
}