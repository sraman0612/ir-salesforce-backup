@isTest
public class ShareContentWithCommunityUsersBatch_Test {
    
    static testMethod void batchTest(){
       
        Account acc =new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId(); 
        acc.Name = 'Lily and loo';
        insert acc;
        
        Contact con = new Contact();
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Contact').getRecordTypeId();
        con.LastName = 'David Lui' ;
        con.Email ='david_lui@gmail.com' ;
        con.AccountId = acc.Id;
        con.CTS_TechDirect_Support_Sub_Region__c = 'Angola';
        con.CTS_TechDirect_Support_Region__c = 'EMEIA';
        insert con;
        
        Product2 prod = new Product2();
        prod.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Product').getRecordTypeId();
        prod.Name = 'Centac';
        prod.Customizable__c=FALSE;
        insert prod;
        
        Asset assetRec = new Asset(); 
        assetRec.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Asset').getRecordTypeId();
        assetRec.Name = 'Asset 1';
        assetRec.AccountId = acc.Id;
        assetRec.Product2Id = prod.Id;
        assetRec.CTS_TechDirect_Model_Number__c = 'M-123';
        insert assetRec; 
        
        Case caseRec = New Case();
        caseRec.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Start_Up').getRecordTypeId();        
        caseRec.assetId = assetRec.Id;
        caseRec.Status = 'High';
        caseRec.contactId = con.Id;
        caseRec.Description = 'hello Test Class';
        caseRec.Subject = 'How may i help you';
        caseRec.CTS_TechDirect_Category__c = 'Startup';
        caseRec.Origin = 'Phone';
        caseRec.CTS_TechDirect_Trouble_Free__c = 'Yes';
        caseRec.CTS_TechDirect_Start_Up_Date__c = system.today() + 5 ;
        insert caseRec;
        
        String before = 'Testing base 64 encode';            
        Blob beforeblob = Blob.valueOf(before);
        
        //Insert contentdocument data
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv; 
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        
        ContentDocumentLink contentlink = new ContentDocumentLink();
        contentlink.LinkedEntityId = caseRec.id;
        contentlink.ContentDocumentId = testcontent.ContentDocumentId;
        insert contentlink;
        
        Test.startTest();
        ShareContentWithCommunityUsers_Batch batchVar = new ShareContentWithCommunityUsers_Batch();
        Database.executeBatch(batchVar);
        
        ContentDocumentLink cdl = [Select Id, Visibility From ContentDocumentLink Where Id =: contentlink.Id];
        System.assertEquals('AllUsers', cdl.Visibility);
        
        Test.stopTest();    
    }
}