/**
    Class to hold constant value for search controller and related classes
*/

public class CTS_ArticleSearchConstants {
    public static final String KNOWLEDGE_OBJECT = 'Knowledge__kav';
    public static final String KNOWLEDGE = 'Knowledge';    
    public static final String AUTO = 'Auto';
    public static final String USER_SELECTED = 'User-Selected';
    public static final String EQUALS  = 'Equals';
    public static final String NOT_EQUALS = 'Not Equals';
    public static final String CONTAINS = 'Contains';
    public static final String NOT_CONTAINS = 'Not Contains'; 
    
    public CTS_ArticleSearchConstants(){}
}