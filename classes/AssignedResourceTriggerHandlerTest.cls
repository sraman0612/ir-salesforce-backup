/**
 * Created by CloudShapers on 11/19/2020.
 * Providity EHARDIN 2023-06-02
 * Removing class as field service cloud is being removed
 */

@IsTest
private class AssignedResourceTriggerHandlerTest {
/*
    static final Id CTS_RECORDTYPE_WORKORDER = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Work_Order_CTS_Field_Service').getRecordTypeId();
    static final Id CTS_RECORDTYPE_SERVICEAPPOINTMENT = Schema.SObjectType.ServiceAppointment.getRecordTypeInfosByDeveloperName().get('CTS_Service_Appointment_Record_Type').getRecordTypeId();

    @testSetup static void setup() {
        List<Account> accounts = CTS_TestDataFactory.createLatamAccounts(1);
        if (accounts != null) {
            List<WorkOrder> workOrders = CTS_TestDataFactory.createTestWorkOrder(1, false, new Map<String, String>{
                    'RecordTypeId' => CTS_RECORDTYPE_WORKORDER
            });
            workOrders[0].AccountId = accounts[0].id;
            insert workOrders;
            if (workOrders != null) {
                WorkOrderLineItem woLineItem = new WorkOrderLineItem();
        		woLineItem.WorkOrderId = workOrders[0].Id;
        
                Id ctsWoliRecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByDeveloperName().get('CTS_WOLI_Record_Type').getRecordTypeId();
                woLineItem.RecordTypeId=ctsWoliRecordTypeId;
                insert woLineItem;
                
                List<WorkOrderLineItem> workOrderLineItems = new WorkOrderLineItem[]{woLineItem};

                List<Product2> products = CTS_TestDataFactory.createTestProduct(1, true, new Map<String, String>());

                CTS_TestDataFactory.createTestProductRequired(1, true, new Map<String, Object>{
                        'Product2Id' => products[0].Id,
                        'ParentRecordId' => workOrderLineItems[0].id,
                        'QuantityRequired' => 2
                });

                List<OperatingHours> operatingHours = CTS_TestDataFactory.createOperatingHours(1, true, new Map<String, String>{
                        'Name' => 'Test Operating Hours',
                        'TimeZone' => 'America/Sao_Paulo'
                });

                List<ServiceTerritory> serviceTerritories = CTS_TestDataFactory.createServiceTerritories(1, true, new Map<String, Object>{
                        'Name' => 'Test Territory',
                        'OperatingHoursId' => operatingHours[0].id,
                        'IsActive' => true
                });

                CTS_TestDataFactory.createServiceAppointments(1, true, new Map<String, Object>{
                        'ParentRecordId' => workOrderLineItems[0].Id,
                        'RecordTypeId' => CTS_RECORDTYPE_SERVICEAPPOINTMENT,
                        'SchedStartTime' => Datetime.now().addDays(5),
                        'SchedEndTime' => Datetime.now().addDays(7),
                        'EarliestStartTime' => Datetime.now(),
                        'DueDate' => Datetime.now().addDays(7),
                        'ServiceTerritoryId' => serviceTerritories[0].Id
                });

                User sampleUser = CTS_TestDataFactory.createTestUser('System Administrator', true);

                List<Schema.Location> locations = CTS_TestDataFactory.createTestLocation(1, true, new Map<String, Object>{
                        'Name' => 'Test Location',
                        'IsMobile' => true,
                        'IsInventoryLocation' => true,
                        'LocationType' => 'Warehouse'
                });

                List<ServiceResource> serviceResources = CTS_TestDataFactory.createServiceResources(1, true, new Map<String, String>{
                        'RelatedRecordId' => sampleUser.Id,
                        'Name' => 'Test Tech Direct',
                        'LocationId' => locations[0].Id
                });

                CTS_TestDataFactory.createServiceTerritoryMemberships(1, true, new Map<String, Object>{
                        'ServiceTerritoryId' => serviceTerritories[0].Id,
                        'ServiceResourceId' => serviceResources[0].Id,
                        'TerritoryType' => 'P',
                        'EffectiveStartDate' => Datetime.now().addDays(-2)
                });
            }
        }
    }

    @isTest static void testMethod1() {
        List<ServiceAppointment> serviceAppointments = [
                SELECT Id
                FROM ServiceAppointment
        ];
        List<ServiceResource> serviceResources = [
                SELECT Id
                FROM ServiceResource
        ];

        List<ProductItem> productItems = new List<ProductItem>();

        Test.startTest();
        CTS_TestDataFactory.createServiceAssignmentResources(1, true, new Map<String, String>{
                'ServiceResourceId' => serviceResources[0].Id,
                'ServiceAppointmentId' => serviceAppointments[0].Id
        });
        productItems = [
                SELECT Id
                FROM ProductItem
        ];
        Test.stopTest();
        //System.assertEquals(productItems.size(), 0);
    }
    */
    
}