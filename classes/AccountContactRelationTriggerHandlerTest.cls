@isTest
public with sharing class AccountContactRelationTriggerHandlerTest {

    @TestSetup
    private static void createData(){
        UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
            Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        upsert notificationSettings;

        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account acc1 = CTS_TestUtility.createAccount('Test Account1', false);
        Account acc2 = CTS_TestUtility.createAccount('Test Account2', false);

        insert new Account[]{acc1, acc2};

        Contact communityContact1 = CTS_TestUtility.createContact('Contact123','Test', 'testcts1@gmail.com', acc1.Id, false);
        communityContact1.CTS_IOT_Community_Status__c = 'Approved';

        Contact communityContact2 = CTS_TestUtility.createContact('Contact456','Test', 'testcts2@gmail.com', acc2.Id, false);
        communityContact2.CTS_IOT_Community_Status__c = 'Approved';        

        insert new Contact[]{communityContact1, communityContact2};

        AccountContactRelation acr1 = new AccountContactRelation(ContactId = communityContact1.Id, AccountId = acc2.Id);
        AccountContactRelation acr2 = new AccountContactRelation(ContactId = communityContact2.Id, AccountId = acc1.Id);

        insert new AccountContactRelation[]{acr1, acr2};
system.debug('User >>'+UserInfo.getUserRoleId());
        User communityUser1 = CTS_TestUtility.createUser(communityContact1.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id, false);    
        communityUser1.LastName = 'Comm_User_Test1';
        communityUser1.PortalRole = 'Manager';

        User communityUser2 = CTS_TestUtility.createUser(communityContact2.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserAllSitesCommunityProfile.Id, false);    
        communityUser2.LastName = 'Comm_User_Test2';   
		communityUser2.PortalRole = 'Manager';
            
        insert new User[]{communityUser1, communityUser2};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc1.Id, assetRecTypeId, false); 
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', acc1.Id, assetRecTypeId, false);   
        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '67444', acc2.Id, assetRecTypeId, false);
        insert new Asset[]{asset1, asset2, asset3};

        Notification_Type__c notificationType1 = NotificationsTestDataService.createNotificationType(true, 'Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test');
                    
        Notification_Subscription__c globalSubscription = NotificationsTestDataService.createSubscription(false, communityUser1.Id, null, notificationType1.Id,
            'Daily;Real-Time', 'Email;SMS');  
        
        Notification_Subscription__c site1Subscription = globalSubscription.clone();
        site1Subscription.Record_ID__c = acc1.Id;
        
        Notification_Subscription__c site2Subscription = globalSubscription.clone();
        site2Subscription.Record_ID__c = acc2.Id;

        Notification_Subscription__c assetSubscription1 = globalSubscription.clone();
        assetSubscription1.Record_ID__c = asset1.Id;

        Notification_Subscription__c assetSubscription2 = globalSubscription.clone();
        assetSubscription2.Record_ID__c = asset3.Id;
            
        Notification_Subscription__c assetSubscription3 = globalSubscription.clone();
        assetSubscription3.OwnerId = communityUser2.Id;
        assetSubscription3.Record_ID__c = asset1.Id;

        insert new Notification_Subscription__c[]{globalSubscription, site1Subscription, site2Subscription, assetSubscription1, assetSubscription2, assetSubscription3};        
    }
}

    @isTest
    private static void standardUserTestDisabledTrigger(){

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();
        notificationSettings.Account_Contact_Relation_Trigger_Enabled__c = false;
        update notificationSettings;        

        Contact communityContact1 = [Select Id, Name, AccountId From Contact Where LastName = 'Contact123' LIMIT 1];
        User communityUser1 = [Select Id, Name, ContactId From User Where ContactId = :communityContact1.Id];
        Account acctToRemove = [Select Id, Name From Account Where Name = 'Test Account2' LIMIT 1];
        Asset[] assetsToRemove = [Select Id, Name From Asset Where AccountId = :acctToRemove.Id];
        AccountContactRelation acrToRemove = [Select Id From AccountContactRelation Where AccountId = :acctToRemove.Id and ContactId = :communityContact1.Id];
        Notification_Subscription__c[] siteSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser1.Id and Record_ID__c = :acctToRemove.Id];

        Set<Id> assetIdsToRemove = new Set<Id>();

        for (Asset asset : assetsToRemove){
            assetIdsToRemove.add(asset.Id);
        }

        Notification_Subscription__c[] assetSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser1.Id and Record_ID__c in :assetIdsToRemove];

        system.assertEquals(1, assetsToRemove.size());
        system.assertEquals(1, siteSubscriptionsToRemove.size());
        system.assertEquals(1, assetSubscriptionsToRemove.size());
        system.assertEquals(6, [Select Count() From Notification_Subscription__c]);

        Test.startTest();

        delete acrToRemove;

        system.assertEquals(1, [Select Count() From Notification_Subscription__c Where Id in :siteSubscriptionsToRemove]);
        system.assertEquals(1, [Select Count() From Notification_Subscription__c Where Id in :assetSubscriptionsToRemove]); 
        system.assertEquals(6, [Select Count() From Notification_Subscription__c]);       

        Test.stopTest();
    }    

    @isTest
    private static void standardUserTest(){

        Contact communityContact1 = [Select Id, Name, AccountId From Contact Where LastName = 'Contact123' LIMIT 1];
        User communityUser1 = [Select Id, Name, ContactId From User Where ContactId = :communityContact1.Id];
        Account acctToRemove = [Select Id, Name From Account Where Name = 'Test Account2' LIMIT 1];
        Asset[] assetsToRemove = [Select Id, Name From Asset Where AccountId = :acctToRemove.Id];
        AccountContactRelation acrToRemove = [Select Id From AccountContactRelation Where AccountId = :acctToRemove.Id and ContactId = :communityContact1.Id];
        Notification_Subscription__c[] siteSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser1.Id and Record_ID__c = :acctToRemove.Id];

        Set<Id> assetIdsToRemove = new Set<Id>();

        for (Asset asset : assetsToRemove){
            assetIdsToRemove.add(asset.Id);
        }

        Notification_Subscription__c[] assetSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser1.Id and Record_ID__c in :assetIdsToRemove];

        system.assertEquals(1, assetsToRemove.size());
        system.assertEquals(1, siteSubscriptionsToRemove.size());
        system.assertEquals(1, assetSubscriptionsToRemove.size());
        system.assertEquals(6, [Select Count() From Notification_Subscription__c]);

        Test.startTest();

        delete acrToRemove;

        system.assertEquals(0, [Select Count() From Notification_Subscription__c Where Id in :siteSubscriptionsToRemove]);
        system.assertEquals(0, [Select Count() From Notification_Subscription__c Where Id in :assetSubscriptionsToRemove]); 
        system.assertEquals(4, [Select Count() From Notification_Subscription__c]);       

        Test.stopTest();
    }

    @isTest
    private static void hybridUserTest(){

        Contact communityContact2 = [Select Id, Name, AccountId From Contact Where LastName = 'Contact456' LIMIT 1];
        User communityUser2 = [Select Id, Name, ContactId From User Where ContactId = :communityContact2.Id];
        Account acctToRemove = [Select Id, Name From Account Where Name = 'Test Account1' LIMIT 1];
        Asset[] assetsToRemove = [Select Id, Name From Asset Where AccountId = :acctToRemove.Id];
        AccountContactRelation acrToRemove = [Select Id From AccountContactRelation Where AccountId = :acctToRemove.Id and ContactId = :communityContact2.Id];
        Notification_Subscription__c[] siteSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser2.Id and Record_ID__c = :acctToRemove.Id];

        Set<Id> assetIdsToRemove = new Set<Id>();

        for (Asset asset : assetsToRemove){
            assetIdsToRemove.add(asset.Id);
        }

        Notification_Subscription__c[] assetSubscriptionsToRemove = [Select Id, Name, Record_ID__c From Notification_Subscription__c Where OwnerId = :communityUser2.Id and Record_ID__c in :assetIdsToRemove];

        system.assertEquals(2, assetsToRemove.size());
        system.assertEquals(0, siteSubscriptionsToRemove.size());
        system.assertEquals(1, assetSubscriptionsToRemove.size());
        system.assertEquals(6, [Select Count() From Notification_Subscription__c]);

        Test.startTest();

        delete acrToRemove;

        system.assertEquals(0, [Select Count() From Notification_Subscription__c Where Id in :siteSubscriptionsToRemove]);
        system.assertEquals(1, [Select Count() From Notification_Subscription__c Where Id in :assetSubscriptionsToRemove]); 
        system.assertEquals(6, [Select Count() From Notification_Subscription__c]);       

        Test.stopTest();
    }    
}