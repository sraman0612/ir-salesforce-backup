public class C_UpdateParent{
    @InvocableMethod
    public static void updateCase(List<Id> recordIds){     
        // Updated by Artur Poiata - #03180077 - 17JUL2024
        Set<Case> clist = new Set<Case>();
        
        for(Case c : [Select id, status, Action_Item_Status_Rollup__c,recordtypeid, (select id from Cases where status!='Closed') from Case Where Id IN :recordIds])
        {
            system.debug('****Action_Item_Status_Rollup__c  '+c.Action_Item_Status_Rollup__c );
            system.debug('****recordtype id '+c.recordtypeid);
            
            if(c.Cases.IsEmpty()){
                c.Status ='Open';
                // Updated by Mahesh Karadkar - #02900279 - 14Mar2024
                c.Unread_Action_Items__c = false;
                c.Last_Commenter__c = '';
                cList.add(c);                  
            }
        }
        system.debug('****  clist'+clist.size()); 
        if(!clist.IsEmpty()){ 
            // Updated by Artur Poiata - #03180077 - 17JUL2024
            update new List<Case> (clist); 
        }
        
        // Updated by Mahesh Karadkar - #02900279 - 14Mar2024
        // Updated by Artur Poiata - #03180077 - 17JUL2024 - BEGIN
        set<Case> parentList = new set<Case>();
        // Updated by Artur Poiata - #03180077 - 17JUL2024 - END
     
        for(Case objChildCase :[SELECT id, parentId, status
                                FROM Case
                                WHERE parentId =: recordIds
                                AND (status = 'Pending Approval' or status = 'More Info Required')
                                ORDER BY status DESC])
        {
            if(objChildCase.status=='Pending Approval'){
                case pcase = new case(id = objChildCase.parentId);
                pcase.Unread_Action_Items__c = true;
                parentList.add(pcase);
                break;
            }
            else if(objChildCase.status=='More Info Required'){
                case pcase = new case(id = objChildCase.parentId);
                pcase.Unread_Action_Items__c = false;
                parentList.add(pcase);
            }
        }
        
        if(!parentList.isEmpty()){
            // Updated by Artur Poiata - #03180077 - 17JUL2024
            update new List<Case> (parentList); 
        }
    }
}