public class CTS_CommunityCPQEmbeddedEditCTLR {

  public CTS_CommunityCPQEmbeddedEditCTLR (ApexPages.StandardController controller){}
  
  public PageReference redirector(){
    String refererURL = ApexPages.currentPage().getHeaders().get('Referer');
    String [] refererArr = refererURL.split('id=');
    String quoteId = refererArr[1];
    String URLStr = '/apex/cafsl__EmbeddedTransaction';
    PageReference newPage = new PageReference(URLStr);
    newPage.getParameters().put('mode','edit');
    newPage.getParameters().put('quoteId',quoteId);
    return newPage;
  }
}