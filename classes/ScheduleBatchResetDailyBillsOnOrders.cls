/*------------------------------------------------------------
Author:       Santiago Colman
Description:  Class used to schedule the batch "BatchResetDailyBillsOnOrders"
------------------------------------------------------------*/
global class ScheduleBatchResetDailyBillsOnOrders implements Schedulable{

    public static String SCHEDULE_INTERVAL = '0 1 0 * * ?';  // Every Day, one minute past midnight
    public static String DEFAULT_JOB_NAME = 'Batch to reset daily bills on Orders';

    global void execute(SchedulableContext sc) {
        // Instantiate the batch and execute it
        //BatchResetDailyBillsOnOrders batch = new BatchResetDailyBillsOnOrders();
        //Database.executebatch(batch);
    }

    global static String scheduleMe(String jobName) {
        // Create an instance of the Schedulable class
        ScheduleBatchResetDailyBillsOnOrders scheduleInstance = new ScheduleBatchResetDailyBillsOnOrders();
        // Schedule it for execution
        jobName = String.isNotBlank(jobName) ? jobName : DEFAULT_JOB_NAME;
        return System.schedule(jobName, SCHEDULE_INTERVAL, scheduleInstance);
    } 
}