@isTest(SeeAllData=true)
public with sharing class orderConfirmationEmailControllerTest {
    //deploy everything
    //setup checkout flow
    //go through as test user to create order summary
    //do second deployment of test class and then activate process builder and flow
    // @TestSetup
    // static void makeData(){
        
    // }

    @isTest
    static void getOrderSummaryTest(){
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User sysAdminUser = new User(LastName = 'test user 1', 
                             Username = 'userName.1@example.gov', 
                             Email = 'test.1@irco.com', 
                             Alias = 'testu1', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US');
        Test.startTest();
            System.runAs(sysAdminUser){
                Account testAccount = welch_TestDataFactory.createAccount(true);
                Contact testContact = welch_TestDataFactory.createContact(testAccount, true);
                List<Product2> prodsToCreate = welch_TestDataFactory.createProducts(true);
                List<PricebookEntry> priceBookEntries = welch_TestDataFactory.createPriceBookEntries(prodsToCreate, true);
                Order testOrder = welch_TestDataFactory.createOrder(testAccount, true);
                OrderDeliveryMethod devMethod = welch_TestDataFactory.createOrderDeliveryMethod(true);
                OrderDeliveryGroup odg = welch_TestDataFactory.createOrderDeliveryGroup(testOrder, devMethod, true);
                List<OrderItem> ordItems = welch_TestDataFactory.createOrderItemList(prodsToCreate, testOrder, true);
                for(OrderItem oitm: ordItems){
                    oitm.OrderDeliveryGroupId = odg.Id;
                }
                update ordItems;
                testOrder = [SELECT ID FROM Order Where Id=:testOrder.Id];
                testOrder.Status = 'Activated';
                update testOrder;
                
                ConnectApi.OrderSummaryInputRepresentation osir = new ConnectApi.OrderSummaryInputRepresentation();
                osir.orderId=testOrder.Id;
                osir.orderLifeCycleType='UNMANAGED';
                ConnectApi.OrderSummaryOutputRepresentation osor = ConnectApi.OrderSummaryCreation.createOrderSummary(osir);
                System.debug('TEST OUTPUT :: ' + osor);
                //ConnectApi.ConnectApiException: Error 1 of 1: REQUIRED_FIELD_MISSING - Required fields are missing: [OrderDeliveryGroupSummaryId]
                //Got this error but you cannot create orderdeliverygroupsummary without a order summary. Can't create order summary without connectapi, so nowhere to go.
                //Need to just query 

                OrderSummary ordSummary = [SELECT Id, OwnerId FROM OrderSummary LIMIT 1];
                User owner = [SELECT Id FROM User WHERE Id=:ordSummary.OwnerId LIMIT 1];
                System.debug('ORDER SUMMARY :: ' + ordSummary);
                    // OrderSummary ordSummary = welch_TestDataFactory.createOrderSummary(testOrder, testAccount, true);
                OrderDeliveryGroupSummary groupSummary = welch_TestDataFactory.createOrderDeliveryGroupSummary(ordSummary, devMethod, true);
                OrderItemSummary itemSummary = welch_TestDataFactory.createOrderItemSummary(prodsToCreate[0], ordSummary, groupSummary, null, true);
                //OrderSummary ordSum = [SELECT Id FROM OrderSummary LIMIT 1];
                orderConfirmationEmailController controller = new orderConfirmationEmailController();
                controller.recordId = ordSummary.Id;
                controller.user = owner;
                controller.getOrderSummary();
                controller.getOrderDeliveryGrpSummary();
                controller.getProductInfo();
                controller.getOrderDate();
                controller.getUserInfo();
                orderConfirmationEmailController.OrderInfo orderInfoClass = new orderConfirmationEmailController.OrderInfo();
            }
        Test.stopTest();
    }
}