// 
// (c) 2015 Appirio, Inc.
//
//T-376323 Generate an Extension Class for VF Page Opportunity line item
//
// 07 April 2015  Surabhi Sharma  
// 24 August 2015    T-429065(Modified)    Surabhi Sharma   
// 10 Dec 2016  removed unused collection deleteOppLineList and catching FIELD_CUSTOM_VALIDATION_EXCEPTION in save to get code coverage > 75%


public with sharing class OpportunityLineExtensions{
  String oppPageId;
  string pagId;
  String errorMessage;
      
  public list<Opportunity_Line_Item__c> oppLineItem{get;set;}    
  
  public OpportunityLineExtensions(ApexPages.StandardController stdctrl){
    pagId = ApexPages.currentPage().getURL();    
    oppPageId  = pagId.substring(pagId.indexof('lkid=')+5,pagId.indexof('lkid=')+20);   
    this.oppLineItem= new list<Opportunity_Line_Item__c >();    
    AddRow();   
  }
  
  public  PageReference AddRow() {
    oppLineItem.add(new Opportunity_Line_Item__c(Opportunity__c = oppPageId ));
    return null;  
  }
   
  public void removeOppLineItem(){
    Integer indexVal = Integer.valueof(system.currentpagereference().getparameters().get('index'));
    oppLineItem.remove(indexVal - 1);     
  }   

  public PageReference Save(){    
    Integer occurence;
    if(!oppLineItem.isEmpty()){
      try{
        insert oppLineItem;        
      }catch(DMLException e) {
        errorMessage = e.getMessage();
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Unexpected Error Processing... Please Contact Your Administrator.' ));
        return null;
      }
    }
    return new PageReference('/' + oppPageId  );    //redirects the page to opportunity Id
  }
}