public class C_Files_Extension {

    public String fileID {get; set;}
    public List<ContentDocument> theFiles {get; private set;}
    public List<ContentDocumentLink> theLinks {get; private set;}
    public List<EmailMessage> theEmails {get; private set;}
    public Set<String> currFileIds = new Set<String>();
    public Set<String> linkedEntityIds = new Set<String>();
    public List<FileWrapper> thewrappers {
        get;
        private set;}
    public Case theCase {get; private set;}

    public C_Files_Extension(ApexPages.StandardController controller) {
        this.theCase = (Case) controller.getRecord();
        this.theEmails = [SELECT Id FROM EmailMessage WHERE HasAttachment = TRUE AND ParentId = :this.theCase.Id];
        for (EmailMessage em : theEmails) {
            linkedEntityIds.add(em.Id);
        }
        //Add Id for the Case to the list as well
        linkedEntityIds.add(this.theCase.Id);
        System.debug('linkedEntityIds ====> '+linkedEntityIds);
        //this.theLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :this.theCase.Id and LinkedEntity.Type='Case'];
        this.theLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
        for (ContentDocumentLink cdl : theLinks) {
            currFileIds.add(cdl.ContentDocumentId);
        }
        this.theFiles = [SELECT Id, Title, Description, FileExtension, CreatedDate, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :currFileIds];
        System.debug('theFiles ====> '+theFiles);
        this.thewrappers = new List<FileWrapper>();
        for(ContentDocument cd : theFiles){
            thewrappers.add(new FileWrapper(cd));
        }
    }

    public void redo(){
        //this.theLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :this.theCase.Id and LinkedEntity.Type='Case'];
        this.theLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds];
        for (ContentDocumentLink cdl : theLinks) {
            currFileIds.add(cdl.ContentDocumentId);
        }
        this.theFiles = [SELECT Id, Title, Description, FileExtension, CreatedDate, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :currFileIds];
        this.thewrappers = new List<FileWrapper>();
        for(ContentDocument cd : theFiles){
            thewrappers.add(new FileWrapper(cd));
        }
    }

    public class FileWrapper{
        public ContentDocument sfdcFile {get; private set;}
        public Boolean cBox {get; set;}

        public FileWrapper(ContentDocument f){
            this.sfdcFile = f;
            this.cBox = false;
        }
    }

    public void cloneFiles()
    {
        Set<String> cdIds = new Set<String>();
        List<ContentDocumentLink> toCloneCDL = new List<ContentDocumentLink>();

        for(FileWrapper fw : theWrappers)
        {
            if(fw.cBox){
                cdIds.add(fw.sfdcFile.Id);
            }
        }

        for(ContentDocumentLink cdl : [SELECT ContentDocumentId,Id,IsDeleted,LinkedEntityId,ShareType,SystemModstamp,Visibility
                                        FROM ContentDocumentLink WHERE LinkedEntityId IN :linkedEntityIds])
        {
            if(theCase.ParentId!=null && cdIds.contains(cdl.ContentDocumentId))
            {
                cdl.Id=null;
                cdl.LinkedEntityId = this.theCase.ParentId;
                toCloneCDL.add(cdl);
            }
        }
        if (toCloneCDL.size() > 0) Database.insert(toCloneCDL,false);
    }

    public String getTopLevelCase(String caseId) {
        Boolean topLevelParent = false;
        while (!topLevelParent) {
            Case c = [SELECT Id, ParentId FROM Case WHERE Id = :caseId LIMIT 1];
            if ( c.ParentId != null ) {
                caseId = c.ParentId;
            }
            else {
                topLevelParent = true;
            }
        }
        return caseId;
    }

    public void deleteFiles(){
        List<ContentDocument> toDelete = new List<ContentDocument>();
        List<FileWrapper> fwTemp = new List<FileWrapper>();
        for(FileWrapper fw : theWrappers){
            if(fw.cBox){
                System.debug('To Delete: ' + fw.sfdcFile.Id);
                toDelete.add(fw.sfdcFile);
            }
            else{
                fwTemp.add(fw);
            }
        }
        if(toDelete.size() > 0){
            try{
                delete toDelete;
                theWrappers = fwTemp;
            }
            catch(Exception e){
                System.debug(e);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            }
        }
    }

    public void createNewFiles_Stage () {
        createNewFiles(fileID);
    }

    public void createNewFiles(String fileId) {
        //Get file
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :fileId].ContentDocumentId;

        if (conDocId != null) {
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocId; //Add ContentDocumentId
            cDocLink.LinkedEntityId = theCase.Id; //Add file parentId
            cDocLink.ShareType = 'I'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers'; //AllUsers, InternalUsers, SharedUsers
            insert cDocLink;
        }
        //Call method to refresh file lists used when reloading the VF page
        redo();
    }
}