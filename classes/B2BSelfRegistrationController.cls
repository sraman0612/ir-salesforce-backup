public with sharing class B2BSelfRegistrationController {
    public class RegistrationWrapper {
        @AuraEnabled
        public String accountName {get; set;}
        @AuraEnabled
        public String accountVATNumber {get; set;}
        @AuraEnabled
        public String accountFiscalCode {get; set;}
        @AuraEnabled
        public String accountSDINumber {get; set;}
        @AuraEnabled
        public String accountInvoiceEmail {get; set;}
        @AuraEnabled
        public String accountPECEmail {get; set;}
        @AuraEnabled
        public String billingStreetAddress {get; set;}
        @AuraEnabled
        public String billingCity {get; set;}
        @AuraEnabled
        public String billingState {get; set;}
        @AuraEnabled
        public String billingPostalCode {get; set;}
        @AuraEnabled
        public String billingCountry {get; set;}
        @AuraEnabled
        public String shippingStreetAddress {get; set;}
        @AuraEnabled
        public String shippingCity {get; set;}
        @AuraEnabled
        public String shippingState {get; set;}
        @AuraEnabled
        public String shippingPostalCode {get; set;}
        @AuraEnabled
        public String shippingCountry {get; set;}
        @AuraEnabled
        public String contactPointAddressShippingState {get; set;}
        @AuraEnabled
        public String contactFirstName {get; set;}
        @AuraEnabled
        public String contactLastName {get; set;}
        @AuraEnabled
        public String contactEmail {get; set;}
        @AuraEnabled
        public String contactPhoneNumber {get; set;}
        @AuraEnabled
        public String username {get; set;}
        @AuraEnabled
        public String password {get; set;}
        @AuraEnabled
        public String passwordConfirmation {get; set;}
        @AuraEnabled
        public String accountOwnerId {get; set;}
        @AuraEnabled
        public String cartId {get; set;}
    }

    public static final String paymentDefaultOption = 'Credit Card;PO (credit)';

    //helper without sharing as the guest user doesn't have many permissions that are needed
    public without sharing class SharingHelper {
        public SObject insertRecord(SObject record) {
            insert record;
            return record;
        }
    }

    @AuraEnabled
    public static String registerUser(RegistrationWrapper formData) {
        try {
            validatePassword(formData);

            SharingHelper helper = new SharingHelper();

            Account account = new Account(
                    Name = formData.accountName,
                    VAT_ID__c = formData.accountVATNumber,
                    Fiscal_Code__c = formData.accountFiscalCode,
                    SDI_Number__c = formData.accountSDINumber,
                    PEC_Email__c = formData.accountPECEmail,
                    Email_Address__c = formData.accountInvoiceEmail,
                    BillingStreet = formData.billingStreetAddress,
                    BillingState = formData.billingState,
                    BillingPostalCode = formData.billingPostalCode,
                    BillingCity = formData.billingCity,
                    BillingCountry = formData.billingCountry,
                    ShippingStreet = formData.shippingStreetAddress,
                    ShippingState = formData.shippingState,
                    ShippingPostalCode = formData.shippingPostalCode,
                    ShippingCity = formData.shippingCity,
                    ShippingCountry = formData.shippingCountry,
                    LFS_account_type__c = 'Prospect',
                    Robuschi_B2B_Store_Payment_Option__c = paymentDefaultOption,
                    OwnerId = formData.accountOwnerId,
                    RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId()
            );
            helper.insertRecord(account);

            ContactPointAddress shippingContactPointAddress = new ContactPointAddress(
                    Name = 'Shipping',
                    AddressType = 'Shipping',
                    Street = formData.shippingStreetAddress,
                    State = formData.contactPointAddressShippingState,
                    PostalCode = formData.shippingPostalCode,
                    City = formData.shippingCity,
                    Country = formData.shippingCountry,
                    ParentId = account.Id,
                    IsDefault = true
            );
            helper.insertRecord(shippingContactPointAddress);

            BuyerAccount buyerAccount = new BuyerAccount(
                    Name = formData.accountName,
                    IsActive = true,
                    BuyerId = account.Id,
                    CommerceType = 'Buyer'
            );
            helper.insertRecord(buyerAccount);

            String buyerGroupId = B2B_Buyer_Group_Setting__mdt.getInstance(formData.shippingCountry)?.Buyer_Group_Id__c;

            if (!Test.isRunningTest()){
                BuyerGroupMember buyerGroupMember = new BuyerGroupMember(
                        BuyerId = account.Id,
                        BuyerGroupId = buyerGroupId
                );
                helper.insertRecord(buyerGroupMember);
            }

            Contact contact = new Contact(
                    FirstName = formData.contactFirstName,
                    LastName = formData.contactLastName,
                    Email = formData.contactEmail,
                    MobilePhone = formData.contactPhoneNumber,
                    Phone = formData.contactPhoneNumber,
                    AccountId = account.Id,
                    RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId()
            );
            helper.insertRecord(contact);

            User user = new User();
            user.CommunityNickname = generateCommunityNickname(formData.username);
            user.Username = formData.username;
            user.FirstName = formData.contactFirstName;
            user.LastName = formData.contactLastName;
            user.Email = formData.contactEmail;
            user.Phone = formData.contactPhoneNumber;

            Site.createExternalUser(user, contact.AccountId, formData.password, false);
            return loginUser(formData.username, formData.password, formData.cartId);
        } catch(Site.ExternalUserCreateException ex) {
            throw new AuraHandledException(ex.getMessage());
        } catch(AuraHandledException ex) {
            throw ex;
        } catch(Exception ex) {
            throw ex;
        }
    }

    /**
     * @description Validates that the submitted password matches the Org's password policy.
     * @param formData - form data from the UI
     */
    @TestVisible
    private static void validatePassword(RegistrationWrapper formData) {

        try {
            User user = new User();
            user.CommunityNickname = generateCommunityNickname(formData.username);
            user.Username = formData.username;
            user.FirstName = formData.contactFirstName;
            user.LastName = formData.contactLastName;
            user.Email = formData.contactEmail;
            user.Phone = formData.contactPhoneNumber;

            if(!Test.isRunningTest()) { //NOTE: This method is buggy and always fails during Apex Tests so bypass it! See: https://help.salesforce.com/s/articleView?id=000391105&type=1
                Site.validatePassword(user, formData.password, formData.passwordConfirmation);
            }
        }
        catch (SObjectException ex) {
            throw new AuraHandledException('An unknown error has occurred, please contact us for further assistance.');
        }
        catch (Exception ex) {
            //This is deliberate as the Site.Validate function returns useful information about the password failure
            throw new AuraHandledException(ex.getMessage());
        }
    }

    /**
     * @description Attempts to login the newly registered user. Redirects to the home page on success.
     * @param username - Passes in the submitted username field from the self registration form.
     * @param password - Passes in the submitted password field from the self registration form.
     * @param cartId - the id of the user shopping cart
     * @return String PageURL to redirect to upon successful login.
     */
    @TestVisible
    private static String loginUser(String username, String password, String cartId) {
        String pageUrl = null;

        ApexPages.PageReference homePageRef = new PageReference(Site.getBaseSecureUrl());
        String startUrl;
        if (String.isNotEmpty(cartId)) {
            startUrl = homePageRef.getUrl() + '/cart';
        } else {
            startUrl = homePageRef.getUrl();
        }

        ApexPages.PageReference startPageRef = Site.login(username, password, startUrl);

        if (startPageRef != null) {
            pageUrl = startPageRef.getUrl();
        } else {
            ApexPages.PageReference pageRef = new PageReference(Site.getBaseSecureUrl());
            pageUrl = pageRef.getUrl();
        }
        return pageUrl;
    }

    /**
     * @description Generate a unique nickname for the user trying to register
     * @param username - Passes in the submitted username field from the self registration form.
     * @return String of the generated Nickname to use for the user.
     */
    @TestVisible
    private static String generateCommunityNickname(String username) {
        String communityNickname = null;

        Datetime currentTime = System.now();
        Long timeInMls = currentTime.getTime();

        communityNickname = timeInMls + username;

        if(communityNickname.length() > 40) { //Nickname field is limited to 40 characters
            communityNickname = communityNickname.substring(0,40);
        }

        return communityNickname;
    }

    @AuraEnabled(Cacheable=true)
    public static List<B2B_Country__mdt> getCountryAndSateList() {
        List<String> countryFields = new List<String>(B2B_Country__mdt.SObjectType.getDescribe().fields.getMap().keySet());
        List<String> stateFields = new List<String>(B2B_State__mdt.SObjectType.getDescribe().fields.getMap().keySet());

        String query = 'SELECT ' + String.join(countryFields, ',') + ', ' +
                '(SELECT ' + String.join(stateFields, ',') +
                ' FROM B2B_States__r ORDER BY MasterLabel ASC) FROM B2B_Country__mdt ORDER BY MasterLabel ASC';

        return (List<B2B_Country__mdt>) Database.query(query);
    }
}