/*******************************************************************************************
* @Name         welch_TestDataFactory 
* @Author       Gregory Dorsey <gregory.dorsey@cognizant.com>
* @Date         2/3/2022
* @Description  This class contains methods related to creating test data for b2b test classes.
*******************************************************************************************/


@isTest
public with sharing class welch_TestDataFactory {
    public static Account createAccount(Boolean dbAction) {
        Account account = new Account();
        account.Name = 'Test Account';
        account.AccountNumber = '4453';
        account.BillingCity = 'Springfield';
        //account.BillingCountryCode = 'US';
        account.BillingPostalCode = '19064';
        //account.BillingStateCode = 'PA';
        account.BillingStreet = '816 W. Springfield Rd';
        account.ShippingCity = 'Springfield';
        //account.ShippingCountryCode = 'US';
        account.ShippingPostalCode = '19064';
        //account.ShippingStateCode = 'PA';
        account.ShippingStreet = '816 W. Springfield Rd';
        account.Phone = '(610) 325-3535';
        //account.GD_Medical_Region__c = 'NA';
        //account.Gardner_Denver_Brand__c = 'Welch';
        if (dbAction) {
            insert account;
        }
        return account;
    }

    public static Contact createContact(Account acc, Boolean dbAction) {
        //contact 
        Contact testContact = new Contact();
        testContact.FirstName = 'Test First Name';
        testContact.LastName = 'Test Last Name';
        testContact.AccountId = acc.Id;
        if (dbAction) {
            insert testContact;
        }
        return testContact;
    }

    public static User createUser(Contact con, Profile prof, Boolean dbAction){
        
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        String uniqueName = 'testUser_' + dateString;
        Integer i = Integer.valueof((Math.random() * 100));
        User testUser = new User(
                            firstname = 'Test',
                            lastName = 'User ' + (i+1),
                            Username = uniqueName + (i+1) + '@test' + '.org',
                            EmailEncodingKey = 'UTF-8',
                            Alias = uniqueName.substring(18, 23),
                            CommunityNickname = 'test.user ' + (i+1),
                            ContactId = con?.Id,
                            email = uniqueName + '@test' + '.org',
                            IsActive = true,
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = prof?.Id
                        );
        if(dbAction){
            insert testUser;
        }
        return testUser;
    }

    public static User createSystemUser(Profile prof, Boolean dbAction){
        
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        String uniqueName = 'testUser_' + dateString;
        Integer i = Integer.valueof((Math.random() * 100));
        User testUser = new User(
                            firstname = 'Test',
                            lastName = 'User ' + (i+1),
                            Username = uniqueName + (i+1) + '@test' + '.org',
                            EmailEncodingKey = 'UTF-8',
                            Alias = uniqueName.substring(18, 23),
                            CommunityNickname = 'test.user ' + (i+1),
                            email = uniqueName + '@test' + '.org',
                            IsActive = true,
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US',
                            ProfileId = prof?.Id
                        );
        if(dbAction){
            insert testUser;
        }
        return testUser;
    }

    public static BuyerGroup createBuyerGroup(Boolean dbAction){
        BuyerGroup buyGroup = new BuyerGroup();
        buyGroup.Name = 'Default Portal Buyer Group';
        if(dbAction){
            insert buyGroup;
        }
        return buyGroup;
    }

    public static BuyerAccount createBuyerAccount(String companyName, Account newAccount, Boolean dbAction){
        BuyerAccount newBuyerAccount = new BuyerAccount (
                    BuyerId = newAccount?.Id,
                    Name = companyName,
                    IsActive = true
                );
        if(dbAction){
            insert newBuyerAccount;
        }
        return newBuyerAccount;
    }

    public static List<PricebookEntry> createPriceBookEntries(List<Product2> products, Boolean dbAction) {
        List<PricebookEntry> pbe = new List<PricebookEntry>();
        for (Product2 p : products) {
            pbe.add(createPriceBookEntry(p, false));
        }

        if (dbAction) {
            insert pbe;
        }
        return pbe;
    }

    public static PricebookEntry createPriceBookEntry(Product2 product, Boolean dbAction) {
        //pricebook entry
        PricebookEntry testPbEntry = new PricebookEntry();
        testPbEntry.Pricebook2Id = Test.getStandardPricebookId();
        testPbEntry.Product2Id = product?.Id;
        testPbEntry.UnitPrice = 10;
        testPbEntry.IsActive = true;

        if (dbAction) {
            insert testPbEntry;
        }
        return testPbEntry;
    }

    public static List<Product2> createProducts(Boolean dbAction) {
        List<Product2> products = new List<Product2>();

        products.add(new Product2(
            Name = 'Digital Package',
            Description = 'Digital Package',
            IsActive = true,
            ProductCode = 'Digital Package',
            StockKeepingUnit = 'Digital Package')
        );

        products.add(new Product2(
            Name = 'Single Panel Insert w/Dig',
            Description = 'Single Panel Insert w/ Digital',
            IsActive = true,
            ProductCode = 'Wrapper_Bundle_Single_Panel_Dig',
            StockKeepingUnit = 'Wrapper_Bundle_Single_Panel_Dig')
        );

        products.add(new Product2(
            Name = 'Local',
            Description = 'Coupon, Premium, 4/4, Coated Local',
            IsActive = true,
            ProductCode = 'CP44_Local',
            StockKeepingUnit = 'CP44_Local')
        );

        products.add(new Product2(
            Name = 'Call Tracking',
            Description = 'Call Tracking Products',
            IsActive = true,
            ProductCode = 'Call Tracking',
            StockKeepingUnit = 'Call Tracking')
        );

        products.add(new Product2(
            Name = 'Single Panel Insert 2-Sided',
            Description = 'Coupon, Premium, 4/4, Coated',
            IsActive = true,
            ProductCode = 'CP44',
            StockKeepingUnit = 'CP44')
        );

        if (dbAction) {
            insert products;
        }
        return products;
    }

    public static List<Product2> createChildProducts(Boolean dbAction) {
        List<Product2> products = new List<Product2>();

        products.add(new Product2(
            Name = 'Digital Package',
            Description = 'Digital Package',
            IsActive = true,
            ProductCode = 'Digital Package',
            StockKeepingUnit = 'Digital Package',
            LFS_welch_Built_in_Adjustable_Vacuum__c = 'Yes',
	        LFS_welch_Chemical_Resistant__c = 'Yes', 
            LFS_welch_Explosion_Proof__c = 'No',
	        LFS_welch_Gas_Ballast__c = 'No')
        );

        products.add(new Product2(
            Name = 'Single Panel Insert w/Dig',
            Description = 'Single Panel Insert w/ Digital',
            IsActive = true,
            ProductCode = 'Wrapper_Bundle_Single_Panel_Dig',
            StockKeepingUnit = 'Wrapper_Bundle_Single_Panel_Dig',
            LFS_welch_Built_in_Adjustable_Vacuum__c = 'Yes',
	        LFS_welch_Chemical_Resistant__c = 'Yes', 
            LFS_welch_Explosion_Proof__c = 'No',
	        LFS_welch_Gas_Ballast__c = 'No')
        );

        products.add(new Product2(
            Name = 'Local',
            Description = 'Coupon, Premium, 4/4, Coated Local',
            IsActive = true,
            ProductCode = 'CP44_Local',
            StockKeepingUnit = 'CP44_Local',
            LFS_welch_Built_in_Adjustable_Vacuum__c = 'Yes',
	        LFS_welch_Chemical_Resistant__c = 'Yes',
            LFS_welch_Explosion_Proof__c = 'No',
	        LFS_welch_Gas_Ballast__c = 'No')
        );

        if (dbAction) {
            insert products;
        }
        return products;
    }

    public static Product2 createVariationParent(Boolean dbAction){
        Product2 varParent = new Product2(
            Name = 'Local Parent',
            Description = 'Coupon, Premium, 4/4, Coated Local Parent',
            IsActive = true,
            ProductCode = 'CP44_LocalParent',
            StockKeepingUnit = 'CP44_LocalParent',
            Type='Base',
            LFS_welch_Built_in_Adjustable_Vacuum__c = 'Yes',
	        LFS_welch_Chemical_Resistant__c = 'Yes',
            LFS_welch_Explosion_Proof__c = 'No',
	        LFS_welch_Gas_Ballast__c = 'No');

        if(dbAction){
            insert varParent;
        }
        return varParent;
    }

    public static List<ProductAttribute> createProductAttributes(List<Product2> childProds, Product2 parentProd, Boolean dbAction){
        List<ProductAttribute> pAttribute = new List<ProductAttribute>();
        for(Product2 prod : childProds){
            ProductAttribute productAttribute = new ProductAttribute();
            productAttribute.ProductId = prod.Id;
            productAttribute.VariantParentId = parentProd.Id;
            productAttribute.welch_model__c = 'DUOSEAL, 115V 60Hz 1Ph, 0.9 CFM';
            pAttribute.add(productAttribute);
        }
        if(dbAction){
            insert pAttribute;
        }
        return pAttribute;
    }

    public static ProductAttributeSet createProductAttributeSet(Boolean dbAction){
        ProductAttributeSet attSet = new ProductAttributeSet();
        attSet.DeveloperName = 'Vac_Pumps';
        attSet.MasterLabel = 'Vac Pumps';
        if(dbAction){
            insert attSet;
        }
        return attSet;
    }

    public static ProductAttributeSetProduct createProductAttributeSetProduct(ProductAttributeSet prodAttSet, Product2 prod, Boolean dbAction){
        ProductAttributeSetProduct prodAttSetProd = new ProductAttributeSetProduct();
        prodAttSetProd.ProductId = prod.Id;
        prodAttSetProd.ProductAttributeSetId = prodAttSet.Id;
        
        if(dbAction){
            insert prodAttSetProd;
        }
        return prodAttSetProd;
    }

    public static ProductAttributeSetItem createProductAttributeSetItem(ProductAttributeSet prodAttSet, FieldDefinition fieldDef, Boolean dbAction){
        String strippedDurableId = '';
        String durableId = fieldDef.DurableId;
        String apiName = fieldDef.EntityDefinitionId + '.';
        strippedDurableId = durableId.remove(apiName);
        ProductAttributeSetItem setItem = new ProductAttributeSetItem();
        setItem.ProductAttributeSetId = prodAttSet.Id;
        setItem.Field = strippedDurableId;
        setItem.Sequence = 0;

        if(dbAction){
            insert setItem;
        }
        return setItem;
    }

    public static ProductCatalog createProductCatalog(Boolean dbAction){
        ProductCatalog testCatalog = new ProductCatalog();
        testCatalog.Name = 'test catalog';

        if(dbAction){
            insert testCatalog;
        }
        return testCatalog;
    }

    public static ProductCategory createProductCategory(ProductCatalog catalog, Boolean dbAciton){
        ProductCategory category = new ProductCategory();
        category.CatalogId = catalog.Id;
        category.Name = 'test Category';

        if(dbAciton){
            insert category;
        }
        return category;
    }

    public static ProductCategoryProduct createProductCategoryProduct(Product2 prod, ProductCategory category, Boolean dbAction){
        ProductCategoryProduct prodCatProd = new ProductCategoryProduct();
        prodCatProd.ProductId = prod.Id;
        prodCatProd.ProductCategoryId = category.Id;
        prodCatProd.IsPrimaryCategory = true;

        if(dbAction){
            insert prodCatProd;
        }
        return prodCatProd;
    }

    public static OrderSummary createOrderSummary(Order testOrder, Account account, Boolean dbAction){
        OrderSummary ordSum = new OrderSummary();
        ordSum.AccountId = account?.Id;
        ordSum.OrderNumber = testOrder?.Name;
        ordSum.Status = 'Created';
        ordSum.BillingCity = 'St. Petersburg';
        //ordSum.BillingCountryCode = 'US';
        ordSum.BillingPostalCode = '33701';
       // ordSum.BillingStateCode = 'FL';
        ordSum.BillingStreet = '312 3rd St S';
        ordSum.CurrencyIsoCode = 'USD';
        ordSum.OrderedDate = Date.today();

        if(dbAction){
            insert ordSum;
        }
        return ordSum;
    }

    public static Order createOrder(Account account, Boolean dbAction) {

        Order order = new Order();
        order.AccountId = account?.Id;
        order.BillingCity = 'St. Petersburg';
        //order.BillingCountryCode = 'US';
        order.BillingPostalCode = '33701';
        //order.BillingStateCode = 'FL';
        order.BillingStreet = '312 3rd St S';
        order.CurrencyIsoCode = 'USD';
        order.EffectiveDate = Date.today();
        order.EndDate = Date.today().addMonths(12);
        // order.Pricebook2Id = Test.getStandardPricebookId();
        order.ShippingCity = 'St. Petersburg';
        //order.ShippingCountryCode = 'US';
        order.ShippingPostalCode = '33701';
        //order.ShippingStateCode = 'FL';
        order.ShippingStreet = '312 3rd St S';
        order.Status = 'Draft';
        order.Name = 'Test Order';

        System.debug(LoggingLevel.WARN, 'order: ' + order);

        if (dbAction) {
            insert order;
        }
        return order;
    }

    public static OrderDeliveryGroupSummary createOrderDeliveryGroupSummary(OrderSummary ordSum, OrderDeliveryMethod deliveryMethod, Boolean dbAction){
        OrderDeliveryGroupSummary groupSum = new OrderDeliveryGroupSummary();
        groupSum.DeliverToStreet = '312 3rd St S'; 
        groupSum.DeliverToCity = 'St. Petersburg'; 
        //groupSum.DeliverToCountryCode = 'US';                                        
        groupSum.OrderSummaryId = ordSum?.Id; 
        //groupSum.DeliverToStateCode = 'FL';
        groupSum.DeliverToPostalCode = '33701'; 
        groupSum.DeliveryInstructions = 'Gate Code is: 01564';
        groupSum.isGift = false;
        groupSum.DeliverToName = 'Test User';
        groupSum.OrderDeliveryMethodId = deliveryMethod?.Id;

        if(dbAction){
            insert groupSum;
        }
        return groupSum;
    }

    public static OrderItemSummary createOrderItemSummary(Product2 prod, OrderSummary orderSum, OrderDeliveryGroupSummary devGroupSum, OrderItem orderItem, Boolean dbAction){
        OrderItemSummary prodInfo = new OrderItemSummary();
        prodInfo.Product2Id = prod?.Id;
        prodInfo.Name = prod?.Name;
        prodInfo.OrderSummaryId = orderSum?.Id;
        prodInfo.OriginalOrderItemId = orderItem?.Id; //not required so might just remove
        prodInfo.Quantity = 1; 
        prodInfo.OrderDeliveryGroupSummaryId = devGroupSum?.Id;
        prodInfo.QuantityAllocated = 0;
        prodInfo.QuantityCanceled = 0;
        prodInfo.QuantityFulfilled = 0;
        prodInfo.QuantityOrdered = 1;
        prodInfo.QuantityReturned = 0;
        prodInfo.QuantityReturnInitiated = 0;
        prodInfo.UnitPrice = 5000;
        prodInfo.ListPrice = 0;
        prodInfo.TotalLineAmount = 5000;

        if(dbAction){
            insert prodInfo;
        }
        return prodInfo;
    }

    public static OrderDeliveryMethod createOrderDeliveryMethod(Boolean dbAction){
        OrderDeliveryMethod devMethod = new OrderDeliveryMethod();
        devMethod.Name = 'Fed Ground';
        devMethod.isActive = true;
        //product not required

        if(dbAction){
            insert devMethod;
        }
        return devMethod;
    }

    public static OrderDeliveryGroup createOrderDeliveryGroup(Order o, OrderDeliveryMethod om, Boolean dbAction){
        OrderDeliveryGroup odg = new OrderDeliveryGroup();
        odg.OrderId = o.Id;
        odg.OrderDeliveryMethodId = om.Id;
        odg.DeliverToName='The Shipping Address';

        if(dbAction){
            insert odg;
        }
        return odg;
    }

    public static List<OrderItem> createOrderItemList(List<Product2> products, Order ord, Boolean dbAction){
        List<OrderItem> orderItems = new List<OrderItem>();
        for(Product2 prod : products){
            orderItems.add(createOrderItem(prod, ord, false));
        }

        if(dbAction){
            insert orderItems;
        }
        return orderItems;
    }

    public static OrderItem createOrderItem(Product2 prod, Order ord, Boolean dbAction){
        OrderItem oItem = new OrderItem();
        oItem.OrderId = ord.Id;
        oItem.Product2Id = prod.Id;
        oItem.Quantity = 1.0;
        oItem.UnitPrice = 10;
        oItem.ListPrice = 10;
        if(dbAction){
            insert oItem;
        }
        return oItem;
    }
}