/* 
*  Snehal Chabukswar || Date 02/07/2020 
*  Test Class for CTS_LeadAssignedDateTimeCalculation class
*  
*/
@isTest
private class CTS_LeadAssignedDateTimeCalculation_Test {
    
    @isTest static void testLeadOwnerWithoutBHUpdate(){
        List<User>userlist = new List<User>();
        Profile prof = [select id from profile where name = 'System Administrator'];
        User u1 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test1@test.ingersollrand.com',Alias = 'batman',
                           Email='test1@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test', Lastname='Test',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA');
        
        userlist.add(u1);
        User u2 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                           Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test2', Lastname='Test2',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa');
        
        userlist.add(u2);
        User u3 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test3@test.ingersollrand.com',Alias = 'batman',
                           Email='test3@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test3', Lastname='Test3',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa');
        
        userlist.add(u3);
        insert userlist;
        string defaultBusinessHourId;
        Map<String,Id>bhsNameIdMap = new Map<String,Id>();
        for(BusinessHours bhs:[SELECT Id,Name, IsDefault, IsActive FROM BusinessHours]){
            if(bhs.IsDefault == true){
                defaultBusinessHourId = bhs.Id;
            }  
            bhsNameIdMap.put(bhs.Name,bhs.Id);
            system.debug('business hours-->'+bhs.Name+'---id-----'+bhs.Id);
        }
        system.runas(u1){
            Lead leadObj1 = new Lead();
            leadObj1.LastName = 'TestElapsedTime';
            leadObj1.Email = 'Test@test.com';
            leadObj1.Opportunity_Type__c = 'Budgetary';
            leadObj1.Company = 'IR';
            leadObj1.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('CTS_New_Lead').getRecordTypeId();
            leadObj1.Status = 'New';
            Test.startTest();
            insert leadObj1;
            leadObj1.OwnerId = u2.Id;
            update leadObj1;
            leadObj1.Status = 'Accepted';
            update leadObj1;
            Lead leadObj = [SELECT id,CTS_isAssignedToOwner__c,Business_Hours__c FROM Lead WHERE Id=:leadObj1.Id];
            //system.debug('leadObj1.OwnerId------>'+leadObj1.Business_Hours__c);
           // system.debug('u2.Id------->'+u2.Business_Hours__c);
            System.assertEquals(leadObj.CTS_isAssignedToOwner__c,true);
            //System.assertEquals(bhsNameIdMap.get('CTS NA Product Support'),leadObj.Business_Hours__c);
            leadObj1.OwnerId = u3.Id;
            
            try{
               update leadObj1;    
            }
            catch (Exception e){
               System.debug(e); 
            }
           
            
            Test.stopTest();
        }
        
    }
    @isTest static void testLeadOwnerWithBHUpdate(){
        string defaultBusinessHourId;
        Map<String,Id>bhsNameIdMap = new Map<String,Id>();
        for(BusinessHours bhs:[SELECT Id,Name, IsDefault, IsActive FROM BusinessHours]){
            if(bhs.IsDefault == true){
                defaultBusinessHourId = bhs.Id;
            }  
            bhsNameIdMap.put(bhs.Name,bhs.Id);
        }
        List<User>userlist = new List<User>();
        Profile prof = [select id from profile where name = 'System Administrator'];
        User u1 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test1@test.ingersollrand.com',Alias = 'batman',
                           Email='test1@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test', Lastname='Test',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA');
        
        userlist.add(u1);
        User u2 = new User( ProfileId = prof.Id,
                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                           Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                           Firstname='test', Lastname='Test',
                           LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                           TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa');
        
        userlist.add(u2);
        insert userlist;
        system.runAs(u2){
            Lead leadObj1 = new Lead();
            leadObj1.LastName = 'TestElapsedTime';
            leadObj1.Email = 'Test@test.com';
            leadObj1.Opportunity_Type__c = 'Budgetary';
            leadObj1.Company = 'IR';
            leadObj1.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('CTS_New_Lead').getRecordTypeId();
            leadObj1.Status = 'New';
            Test.startTest();
            insert leadObj1;
            leadObj1.OwnerId = u1.id;
            Lead leadObj = [SELECT Id,Business_Hours__c from Lead WHERE Id=:leadObj1.Id limit 1];
            leadObj1.Status = 'Accepted';
            try{
               update leadObj1;    
            }
            catch (Exception e){
               System.debug(e); 
            }
            Lead leadObjupdated = [SELECT id,CTS_isAssignedToOwner__c,Business_Hours__c FROM Lead WHERE Id=:leadObj1.Id];
            System.assertEquals(leadObjupdated.CTS_isAssignedToOwner__c,true);
            //System.assertEquals(leadObjupdated.Business_Hours__c,defaultBusinessHourId);
            Test.stopTest();
        }
    }
}