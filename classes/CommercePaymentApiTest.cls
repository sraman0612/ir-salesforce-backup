@isTest
public class CommercePaymentApiTest {
    @IsTest(SeeAllData=true)
    static void testCapture() {
        CommercePaymentApi.RequestWrapper request = new CommercePaymentApi.RequestWrapper();
        request.accountId = '001XXXXXXXXXXXXXXX';
        request.amount = 100.00;

        String mockPaymentAuthId = 'mockPaymentAuthId';
        String mockPaymentId = 'MockPaymentId';

        Test.startTest();
        CommercePaymentApi cp = new CommercePaymentApi();
        CommercePaymentApi.ResponseWrapper response = cp.capture(request, mockPaymentAuthId);
        Test.stopTest();

        System.assertEquals(mockPaymentId, response.paymentId, 'Payment ID should match');
    }

    @IsTest(SeeAllData=true)
    static void testRefundPayment() {
        CommercePaymentApi.RequestWrapper request = new CommercePaymentApi.RequestWrapper();
        request.amount = 50.00;

        String mockPaymentId = 'MockPaymentId';
        String mockRefundId = 'MockRefundId';

        Test.startTest();
        CommercePaymentApi cp = new CommercePaymentApi();
        CommercePaymentApi.ResponseWrapper response = cp.refundPayment(request, mockPaymentId);
        Test.stopTest();

        System.assertEquals(mockRefundId, response.refundId, 'The refundId should match.');
    }

    @IsTest(SeeAllData=true)
    static void testReverseAuthorization() {
        CommercePaymentApi.RequestWrapper request = new CommercePaymentApi.RequestWrapper();
        request.amount = 30.00;

        String mockPaymentAuthId = 'MockPaymentAuthorizationId';

        Test.startTest();
        CommercePaymentApi cp = new CommercePaymentApi();
        CommercePaymentApi.ResponseWrapper response = cp.reverseAuthorization(request, mockPaymentAuthId);
        Test.stopTest();

        System.assertEquals(mockPaymentAuthId, response.paymentAuthorizationId, 'Authorization Adjustment ID should match');
    }

}