// 
// (c) 2015 Appirio, Inc.
//
// Test Class for testing LeadConversionController
//
// May 14, 2015      Surabhi Sharma           Original: T-393554
// Aug 10, 2015      Surabhi Sharma            Modified
//
@isTest(seeAllData = false)
public with sharing class LeadConversionController_Test {

    private static String convertedLeadStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true Limit 1].MasterLabel;

    @testSetup
    private static void createData(){

        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
        where DeveloperName = 'CTS_OEM_EU' 
        and SobjectType='Account' limit 1].Id;

        Account acc = new Account();
        acc.Name = 'TestCompany';
        acc.BillingCity = 'city';
        acc.BillingCountry = 'MyCountry';
        acc.BillingPostalCode = '12345';
        acc.BillingState = 'state';
        acc.BillingStreet = '12, street1';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'testCounty';    
        acc.recordtypeid=AirNAAcctRT_ID;     
        insert acc;

        Id AirNAirLeadRT_ID = [Select Id, DeveloperName from RecordType 
                where DeveloperName = 'NA_Air' 
                and SobjectType='Lead' limit 1].Id;

        Lead testLead = new Lead(LastName = 'Contact1', FirstName = 'Test', MobilePhone = '123', Company = 'TestCompany'
            ,City = 'TestCity', Country = 'USA', Street = 'TestStreet', State = 'CA'
            ,PostalCode = '00', County__c = 'test County',recordtypeid=AirNAirLeadRT_ID);
        insert testLead;
    }

    private static testMethod void testLeadConversion1(){

        Lead testLead = [Select Id From Lead LIMIT 1];
        Account acc = [Select Id From Account LIMIT 1];

        Test.startTest();

        // set current page for test
        PageReference pageRef1 = new PageReference('/apex/LeadConversion');
        Test.setCurrentPage(pageRef1);
         
        // instantiate controller
        ApexPages.StandardController con1 = new ApexPages.StandardController(new Lead());
        LeadConversionController controller = new LeadConversionController(con1);
 
        controller.getOppType();

        Test.stopTest();           
    }

    private static testMethod void testLeadConversion2(){

        Lead testLead = [Select Id From Lead LIMIT 1];
        Account acc = [Select Id From Account LIMIT 1];
        
        PageReference pageRef = new PageReference('/apex/LeadConversion?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
         
        // instantiate controller
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        LeadConversionController controller = new LeadConversionController(con);        
 
        Test.startTest();
        
        List<SelectOption> lstOptions = controller.getConversionStatus();
        controller.accName = 'TestCompany';
        controller.updateAccountList();
        controller.accountName = acc.id;
        controller.sObjTask.Subject = 'Other';
        controller.cStatus = convertedLeadStatus;
        controller.updateLead();

        PageReference pRefCancel = controller.cancel();
        System.assert(pRefCancel != null,'cancel return null '+Apexpages.getMessages());
            
        Test.stopTest();

        System.assert([SELECT Id FROM Account].size() <> 0,'Account is not created');
    }    

    private static testMethod void testLeadConversion3(){

        Lead testLead = [Select Id From Lead LIMIT 1];
        Account acc = [Select Id From Account LIMIT 1];
        
        PageReference pageRef = new PageReference('/apex/LeadConversion?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
         
        // instantiate controller
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        LeadConversionController controller = new LeadConversionController(con);        

        List<SelectOption> lstOptions = controller.getConversionStatus();
        controller.accName = 'TestCompany';
        controller.updateAccountList();
        controller.accountName = acc.id;
        controller.sObjTask.Subject = 'Other';
        controller.cStatus = convertedLeadStatus;
        controller.updateLead();
 
        Test.startTest();

        PageReference pRef = controller.performConvert();
            
        Test.stopTest();

        System.assert([SELECT Id FROM Account].size() <> 0,'Account is not created');
    }  
}