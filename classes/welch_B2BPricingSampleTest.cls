@isTest
public class welch_B2BPricingSampleTest {
    @testSetup static void setup() {
        Account testAccount = new Account(Name='TestAccount');
        insert testAccount;
        WebStore testWebStore = new WebStore(Name='TestWebStore');
        insert testWebStore;

        Account account = [SELECT Id FROM Account WHERE Name='TestAccount' LIMIT 1];
        WebStore webStore = [SELECT Id FROM WebStore WHERE Name='TestWebStore' LIMIT 1];
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;
        
        CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId=cart.Id, Name='Default Delivery');
        insert cartDeliveryGroup;
        //List<CartItem> cartitems = new List<CartItem>();
        CartItem cartItem1 = new CartItem(CartId=cart.Id, Sku='SKU_Test1', SalesPrice=2.00, Quantity=3.0, Type='Product', Name='TestProduct', CartDeliveryGroupId=cartDeliveryGroup.Id);
        //CartItem cartItem2 = new CartItem(CartId=cart.Id,Quantity=1.0,Type='Product',Name='Surcharge Fee',Sku='surchargefee',CartDeliveryGroupId=cartDeliveryGroup.Id);
        //cartItems.add(cartItem1);
        //cartItems.add(cartItem2);
        insert cartItem1;
    }
    
    @isTest static void testWhenSalesPriceIsCorrectSuccessStatusIsReturned() {
        //We don't actually call out so this should just check that we're updating the cart surcharge
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        // StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        // mock.setStaticResource('GetSalesPricesResource');
        // mock.setStatusCode(200);
        // mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
        WebCart webCart = [SELECT Id, GrandTotalAmount, TotalProductAmount FROM WebCart LIMIT 1];
        Id prod = [SELECT Id, StockKeepingUnit
            FROM Product2
            WHERE StockKeepingUnit LIKE '%surcharge%' LIMIT 1]?.Id;
        if(prod == null){
            Product2 temp=    new Product2(
                Name = 'Surcharge Fee',
                Description = 'surchargefeetest',
                IsActive = true,
                ProductCode = 'surchargefeetest',
                StockKeepingUnit = 'surchargefeetest'
            ); 
            insert temp; 
        }
        List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :webCart.Id LIMIT 1];
        CartItem cartItem2 = new CartItem(CartId=webCart.Id,Quantity=1.0,Type='Product',Name='test prod',Sku='testprod123', ListPrice = 10.00, CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem2;
        
        // Associate the callout with a mock response.
        // Test.setMock(HttpCalloutMock.class, mock);
        
        // Test: execute the integration for the test cart ID.
        welch_B2BPricingSample apexSample = new welch_B2BPricingSample();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;        
        sfdc_checkout.IntegrationStatus integrationResult = apexSample.startCartProcessAsync(integInfo, webCart.Id);
       // System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
        Test.stopTest();
    }

    @isTest static void testWhenSalesPriceIsCorrectSuccessStatusIsReturned2() {
        //We don't actually call out so this should just check that we're updating the cart surcharge
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        // StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        // mock.setStaticResource('GetSalesPricesResource');
        // mock.setStatusCode(200);
        // mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
        WebCart webCart = [SELECT Id, GrandTotalAmount, TotalProductAmount FROM WebCart LIMIT 1];
        Id prod = [SELECT Id, StockKeepingUnit
            FROM Product2
            WHERE StockKeepingUnit LIKE '%surcharge%' LIMIT 1]?.Id;
        if(prod == null){
            Product2 temp=    new Product2(
                Name = 'Surcharge Fee',
                Description = 'surchargefeetest',
                IsActive = true,
                ProductCode = 'surchargefeetest',
                StockKeepingUnit = 'surchargefeetest'
            ); 
            insert temp; 
        }
        Product2 surProd = [SELECT Id, StockKeepingUnit, Name
            FROM Product2
            WHERE StockKeepingUnit LIKE '%surcharge%' LIMIT 1];
        List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :webCart.Id LIMIT 1];
        CartItem cartItem2 = new CartItem(
            CartId=webCart.Id,
            Quantity=1.0,
            Type='Product',
            Name='test prod',
            Sku='testprod123',
            ListPrice = 10.00,
            TotalListPrice = 10.00,
            TotalLineAmount = 10.00,
            TotalPrice = 10.00,
            CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem2;
        update webCart;
        Decimal price = 50.00 - webCart.GrandTotalAmount;
        CartItem cartItem3 = new CartItem(
            CartId=webCart.Id,
            Quantity=1.0,
            Type='Product',
            Name=surProd.Name,
            Sku=surProd.StockKeepingUnit,
            ListPrice = price,
            TotalListPrice = price,
            TotalLineAmount = price,
            TotalPrice = price,
            Product2Id = surProd.Id,
            CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem3;
        update webCart;
        // Associate the callout with a mock response.
        // Test.setMock(HttpCalloutMock.class, mock);
        
        // Test: execute the integration for the test cart ID.
        welch_B2BPricingSample apexSample = new welch_B2BPricingSample();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;        
        sfdc_checkout.IntegrationStatus integrationResult = apexSample.startCartProcessAsync(integInfo, webCart.Id);
       // System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
        Test.stopTest();
    }
    
    @isTest static void testWhenSalesPriceIsCorrectSuccessStatusIsReturned3() {
        //We don't actually call out so this should just check that we're updating the cart surcharge
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        // StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        // mock.setStaticResource('GetSalesPricesResource');
        // mock.setStatusCode(200);
        // mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
        WebCart webCart = [SELECT Id, GrandTotalAmount, TotalProductAmount FROM WebCart LIMIT 1];
        Id prod = [SELECT Id, StockKeepingUnit
            FROM Product2
            WHERE StockKeepingUnit LIKE '%surcharge%' LIMIT 1]?.Id;
        if(prod == null){
            Product2 temp=    new Product2(
                Name = 'Surcharge Fee',
                Description = 'surchargefeetest',
                IsActive = true,
                ProductCode = 'surchargefeetest',
                StockKeepingUnit = 'surchargefeetest'
            ); 
            insert temp; 
        }
        Product2 surProd = [SELECT Id, StockKeepingUnit, Name
            FROM Product2
            WHERE StockKeepingUnit LIKE '%surcharge%' LIMIT 1];
        List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :webCart.Id LIMIT 1];
        Product2 temp2=    new Product2(
                Name = 'test prod',
                Description = 'testprod123',
                IsActive = true,
                ProductCode = 'testprod123',
                StockKeepingUnit = 'testprod123'
            ); 
            insert temp2; 
        CartItem cartItem2 = new CartItem(
            CartId=webCart.Id,
            Quantity=1.0,
            Type='Product',
            Name=temp2.Name,
            Sku=temp2.StockKeepingUnit,
            ListPrice = 70.00,
            TotalListPrice = 70.00,
            TotalLineAmount = 70.00,
            TotalPrice = 70.00,
            Product2Id = temp2.Id,
            CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem2;
        //update webCart;
        Decimal price = 20.00;
        CartItem cartItem3 = new CartItem(
            CartId=webCart.Id,
            Quantity=1.0,
            Type='Product',
            Name=surProd.Name,
            Sku=surProd.StockKeepingUnit,
            ListPrice = price,
            TotalListPrice = price,
            TotalLineAmount = price,
            TotalPrice = price,
            Product2Id = surProd.Id,
            CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem3;
        CartItem cartItem4 = new CartItem(
            CartId=webCart.Id,
            Quantity=2.0,
            Type='Product',
            Name=surProd.Name,
            Sku=surProd.StockKeepingUnit + 'test',
            ListPrice = price,
            TotalListPrice = price,
            TotalLineAmount = price,
            TotalPrice = price,
            Product2Id = surProd.Id,
            CartDeliveryGroupId=cartDeliveryGroups[0].Id);
        insert cartItem4;
        update webCart;
        // Associate the callout with a mock response.
        // Test.setMock(HttpCalloutMock.class, mock);
        
        // Test: execute the integration for the test cart ID.
        welch_B2BPricingSample apexSample = new welch_B2BPricingSample();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;        
        sfdc_checkout.IntegrationStatus integrationResult = apexSample.startCartProcessAsync(integInfo, webCart.Id);
       // System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
        Test.stopTest();
    }
    
    @isTest static void testWhenExternalServiceCallFailsAFailedStatusIsReturnedAndACartValidationOutputEntryIsCreated() {
        // Because test methods do not support Web service callouts, we create a mock response based on a static resource.
        // To create the static resource from the Developer Console, select File | New | Static Resource
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('GetSalesPricesResource');
        // The web service call returns an error code.
        mock.setStatusCode(404);
        mock.setHeader('Content-Type', 'application/json;charset=UTF-8');
        Test.startTest();
        // Associate the callout with a mock response.
        Test.setMock(HttpCalloutMock.class, mock);
        
        // Test: execute the integration for the test cart ID and integration info.
        welch_B2BPricingSample apexSample = new welch_B2BPricingSample();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;
        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        sfdc_checkout.IntegrationStatus integrationResult = apexSample.startCartProcessAsync(integInfo, webCart.Id);
        
        // Validate: IntegrationStatus.Status is FAILED
        // and a new CartValidationOutput record with level 'Error' was created.
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id FROM CartValidationOutput WHERE Level = 'Error'];
        System.assertEquals(1, cartValidationOutputs.size());
        Test.stopTest();
    }

    @isTest static void testProductsWithNoSkuHasError() {
        Test.startTest();

        WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
        List<CartDeliveryGroup> cartDeliveryGroups = [SELECT Id FROM CartDeliveryGroup WHERE CartId = :webCart.Id LIMIT 1];
        System.Debug('cartDeliveryGroups' + cartDeliveryGroups);
        // Insert a cart item without a SKU
        CartItem cartItemWithNoSku = new CartItem(
            CartId=webCart.Id,
            Quantity=1.0,
            Type='Product',
            Name='TestProductNoSku',
            CartDeliveryGroupId=cartDeliveryGroups.get(0).Id
        );
        insert cartItemWithNoSku;

        welch_B2BPricingSample apexSample = new welch_B2BPricingSample();
        sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
        integInfo.jobId = null;
        sfdc_checkout.IntegrationStatus integrationResult = apexSample.startCartProcessAsync(integInfo, webCart.Id);

        // Validate: IntegrationStatus.Status is FAILED.
        // and a new CartValidationOutput record with level 'Error' was created.
        System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
        List<CartValidationOutput> cartValidationOutputs = [SELECT Id, Message FROM CartValidationOutput WHERE Level = 'Error'];
        System.assertEquals(1, cartValidationOutputs.size());
        
        // Validate: The sample text that a product SKU is missing is returned as the failure output 
        System.Debug('cartValidationOutputs' + cartValidationOutputs);
        //System.assertEquals('The SKUs for all products in your cart must be defined.', cartValidationOutputs.get(0).Message);
        Test.stopTest();

        // Remove the invalid cart item
        delete cartItemWithNoSku;
    }
}