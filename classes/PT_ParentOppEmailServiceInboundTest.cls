@isTest
public class PT_ParentOppEmailServiceInboundTest {
  static testMethod void test1l() {
    Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
    Account a = new Account(
      RecordTypeId = ptAcctRTID,
      Name = 'TestAcc',
      Type = 'Customer',
      PT_Status__c = 'New',
      PT_IR_Territory__c = 'North',
      PT_IR_Region__c = 'EMEA'
    );
    insert a;
    PT_Parent_Opportunity__c p = TestUtilityClass.createParentOpp(a.Id);
    p.Lead_SourcePL__c='Other';
    insert p;
    Messaging.InboundEmail email = new Messaging.InboundEmail() ;
    Messaging.InboundEnvelope env    = new Messaging.InboundEnvelope();
    Messaging.InboundEmail.TextAttachment attachTxt = new Messaging.InboundEmail.TextAttachment();
    email.subject = 'Test Parent Opp Eml Inbound';
    email.plainTextBody = 'theBody  {ref:' + p.Id + '}';
    String [] toAddys = new List<String>{'ptparentappemailservice@salesforce.irco.com'};
    email.toAddresses = toAddys;
    env.fromAddress = 'powertoolsparentopptest@irco.com';
    attachTxt.body = 'thebodyoftheattachment';
    attachTxt.fileName = 'ptattachtext.txt';
    attachTxt.mimeTypeSubType = 'text/plain';
    email.textAttachments = new Messaging.inboundEmail.TextAttachment[] { attachTxt };
    PT_ParentOppEmailServiceInbound es = new PT_ParentOppEmailServiceInbound();
    es.handleInboundEmail(email,env);
  }
}