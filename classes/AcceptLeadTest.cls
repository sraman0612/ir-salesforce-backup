@IsTest
private class AcceptLeadTest {
    @IsTest
    public static void unitTest1(){

        User u = CTS_TestUtility.createUser(true);
        User us = CTS_TestUtility.createUser(true);

        system.runAs(us){
            Account ac = CTS_TestUtility.createAccount('Test Account1', true);
            Contact ct = CTS_TestUtility.createContact('Henry', 'John','johnH@gmail.com', ac.Id, true);
        }
        
        Lead l = new Lead();
        l.LastName = 'Test Lead 1';
        l.Business__c = 'Vacuum';
        l.email = 'test@test.com';
        l.Company = 'Test Lead 1';
        insert l;

        l = [SELECT Id, OwnerId FROM Lead WHERE Id = :l.Id];
        system.assertNotEquals(u.Id, l.OwnerId);

        Test.startTest();

        System.runAs(u){
           
           AcceptLead.changeLeadOwner(l.id); 
           
           l = [SELECT Id, OwnerId FROM Lead WHERE Id = :l.Id];
           system.assertEquals(u.Id, l.OwnerId);
        }

        Test.stopTest();
    }
   
}