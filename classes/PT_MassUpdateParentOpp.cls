global class PT_MassUpdateParentOpp implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select Id,Sales_Rep__c, OwnerId from PT_Parent_Opportunity__c';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, list<PT_Parent_Opportunity__c> scope) {
        List<PT_Parent_Opportunity__c> updates = new List<PT_Parent_Opportunity__c>();
        List<User> users = [select Id, Name from User ];
        for(PT_Parent_Opportunity__c opp : scope){            
            for(User u : users){
                if(opp.Sales_Rep__c == u.Name){
                    opp.OwnerId = u.Id;
                    updates.add(opp);
                } 
            }
        }
        if(updates.size() > 0){
            update updates;
        }
        
        system.debug('updates : '+updates);
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('**** Finish');
    }
    
}