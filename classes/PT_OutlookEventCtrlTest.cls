@isTest
public class PT_OutlookEventCtrlTest {
	
    public class PT_OutlookEventCtrlTestException extends Exception {}
	public static testMethod void testSearch()
    {
        PT_OutlookEventCtrl.search('test', new String[]{}, '');
    }
    public static testMethod void testUpdateEvent()
    {
        Id ptAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
    	Account testAccount = new Account();
        testAccount.recordTypeId = ptAccountRecordTypeId;
        testAccount.Name = 'TestLastName';
        insert testAccount;
        
        Id ptContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
        
        /*
        Contact testContact = new Contact();
        testContact.RecordTypeId = ptContactRecordTypeId;
        testContact.LastName = 'TestLastName';
        testContact.Email = 'TestEmail@email.com';
        testContact.AccountId = testAccount.Id;
        insert testContact;
		*/
        
        String createEventContext = '';
        createEventContext += '{';
        createEventContext += '"type":"1. PLAN account actions and objectives",';
        createEventContext += '"whom":"End-user",';
        createEventContext += '"accountId":"'+testAccount.Id+'",';
        createEventContext += '"dates":{';
        createEventContext += '"start":"2020-04-28T07:00:00.000Z",';
        createEventContext += '"end":"2020-04-28T07:30:00.000Z"';
        createEventContext += '},';
        createEventContext += '"subject":"Tz Test 2",';
        createEventContext += '"contacts":[';
        createEventContext += '{"name":"carl Sagan","email":"TestNewContact@test.com","checked":true,"disabled":false,"hasCheckbox":true,"statusCode":"003","Id":""}';
        //createEventContext += '{"name":"Joe Bloggs","email":"TestJoeBloggs@test.com","checked":true,"disabled":false,"hasCheckbox":true,"statusCode":"004","statusLabel":"Reparent Contact","Id":""}';
        createEventContext += ']';
        createEventContext += '}';
        Map<String, Object> createEventResult = PT_OutlookEventCtrl.updateEvent(createEventContext);
		
        if(!(Boolean)createEventResult.get('isSuccess')) 
        {
            throw new PT_OutlookEventCtrlTestException((String)createEventResult.get('errorMessage'));
        }
        
        PT_Outlook_Event__c[] outlookEvents = [Select id, Start_Date_Time__c, End_Date_Time__c from PT_Outlook_Event__c];
        System.debug('outlookEvents:'+outlookEvents);
        Contact[] contacts = [Select id from Contact];
        
        System.assertEquals(1, outlookEvents.size(),'One outlook event should have been created');
        System.assertEquals(1, contacts.size(),'One contact should have been created');
        
        String updateEventContext = '';
        updateEventContext += '{';
        updateEventContext += '"id":"'+outlookEvents[0].Id+'",';
        updateEventContext += '"type":"1. PLAN account actions and objectives",';
        updateEventContext += '"whom":"End-user",';
        updateEventContext += '"accountId":"'+testAccount.Id+'",';
        updateEventContext += '"dates":{';
        updateEventContext += '"start":"2020-04-28T07:00:00.000Z",';
        updateEventContext += '"end":"2020-04-28T07:30:00.000Z"';
        updateEventContext += '},';
        updateEventContext += '"subject":"Tz Test 2",';
        updateEventContext += '"contacts":[';
        updateEventContext += ']';
        updateEventContext += '}';
        
        Map<String, Object> updateEventResult = PT_OutlookEventCtrl.updateEvent(updateEventContext);
        
        Map<String, Object> getContactStatusesResult = PT_OutlookEventCtrl.getContactStatuses(new String[]{'TestNewContact@test.com','missing@test.com'},  testAccount.Id);
        
        String getEventContext = '';
        getEventContext += '{';
        getEventContext += '"dates":{';
        getEventContext += '"start":"2020-04-28T07:00:00.000Z",';
        getEventContext += '"end":"2020-04-28T07:30:00.000Z"';
        getEventContext += '},';
        getEventContext += '"subject":"Tz Test 2"';
        getEventContext += '}';
        
        Map<String, Object> getEventResult = PT_OutlookEventCtrl.getEvent(getEventContext);
        
        //Create an event and check they are linked!
        Id ptEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
        Event ptEvent = new Event();
        ptEvent.RecordTypeId = ptEventRecordTypeId;
        ptEvent.StartDateTime = DateTime.valueOf('2020-04-28 07:00:00');
		ptEvent.EndDateTime = DateTime.valueOf('2020-04-28 07:30:00');
        
        insert ptEvent;
        
        Event[] events = [Select id, StartDateTime, EndDateTime, PT_Outlook_Event_Id__c from Event];
        System.debug('Events:'+events);
        System.assertEquals(1, events.size(), 'One event should have been inserted');
        //System.assertEquals(outlookEvents[0].Id, events[0].PT_Outlook_Event_Id__c, 'Event should be linked');
    }
}