@isTest
public with sharing class CTS_IOT_Site_Helix_MessageControllerTest {

    @TestSetup
    private static void createData(){

        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account acc1 = CTS_TestUtility.createAccount('Test Account1', false);
        Account acc2 = CTS_TestUtility.createAccount('Test Account2', false);
        insert new Account[]{acc1, acc2};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acc1.Id, assetRecTypeId, false); 
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', acc1.Id, assetRecTypeId, false);   
        asset2.IRIT_RMS_Flag__c = true;
        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '67890', acc2.Id, assetRecTypeId, false);   
        insert new Asset[]{asset1, asset2, asset3};
    }

    @isTest
    private static void hasConnectedAssetsTest1(){

        Account acct = [Select Id, Name From Account Where Name = 'Test Account1'];

        Test.startTest();

        system.assertEquals(true, CTS_IOT_Site_Helix_MessageController.hasConnectedAssets(acct.Id));

        Test.stopTest();
    }

    @isTest
    private static void hasConnectedAssetsTest2(){

        Account acct = [Select Id, Name From Account Where Name = 'Test Account2'];

        Test.startTest();

        system.assertEquals(false, CTS_IOT_Site_Helix_MessageController.hasConnectedAssets(acct.Id));

        Test.stopTest();
    }    
}