public without sharing class NotificationDeliveryBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Schedulable{

    private DateTime runTime = DateTime.now();
    private String deliveryFrequency; // Required to initiate batch, must match the Notification_Message__c.Delivery_Frequency__c

    public NotificationDeliveryBatch(String deliveryFrequency){
        system.assert(String.isNotBlank(deliveryFrequency), 'Delivery frequency is required.');
        this.deliveryFrequency = deliveryFrequency;
    }

    public void execute(SchedulableContext context){

        NotificationDeliveryBatch batchJob = new NotificationDeliveryBatch(deliveryFrequency);

        Integer batchSize = deliveryFrequency == NotificationService.REAL_TIME ? LIMITS.getLimitCallouts() : 2000;

        Database.executeBatch(batchJob, batchSize);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {

        Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults();       

        if (notificationSettings != null){

            Boolean isSandbox = [Select IsSandbox From Organization].IsSandbox; 

            Boolean batchJobEnabled = false;
                
            if (isSandbox){
                batchJobEnabled = notificationSettings.Enable_Notification_Batch_Job_Sandbox__c;
            }
            else{
                batchJobEnabled = notificationSettings.Enable_Notification_Batch_Job_Prod__c;
            }      
            
            system.debug('batchJobEnabled: ' + batchJobEnabled);
            
            if (batchJobEnabled){        
                return Database.getQueryLocator(NotificationDeliveryService.getNotificationDeliveryQuery(deliveryFrequency, null));
            }
        }
        
        return Database.getQueryLocator('Select Id From Account LIMIT 0');
    } 
    
    public void execute(Database.BatchableContext BC, List<Notification_Message__c> notificationScope) {
        NotificationDeliveryService.deliverNotificationMessages(notificationScope);                
    }
    
    public void finish(Database.BatchableContext BC) {
                  
    }
}