//
// (c) 2015, Appirio Inc.
//
// Handler class for Account Trigger
//
// April 16,2015    Surabhi Sharma    T-377640
//
// April 28, 2015     Barkha Jain       Modified (@T-379785)
//
public class Account_TriggerHandler{
    
    // Method to delete the record once Approved by Approver
    public static void afterUpdate(List<Account> newList){    //List of Account being fetched after being updated by Approver
        UtilityClass.deleteApprovedRecords(newList,'Account',UtilityClass.GENERIC_APPROVAL_API_NAME);    //Record deleted
       // updateAccountAddresOnLeadConversion(newList);
        
    }
    
    public static void beforeUpdate(List<Account> newList, Map<Id,Account> oldMap){    //List of Account being fetched after being updated by Approver
        for (Account a :newList)
        {
            if (a.IRIT_Dealer_Approved_By_text__c != null && a.IRIT_Dealer_Approved_By_text__c != oldMap.get(a.Id).IRIT_Dealer_Approved_By_text__c)
            {
                try
                {
                    a.IRIT_Dealer_Approved_By__c = (Id) a.IRIT_Dealer_Approved_By_text__c;
                }
                catch(Exception e)
                {
                    a.addError('Invalid ID entered for Approver: "' +  a.IRIT_Dealer_Approved_By_text__c + '"');
                }
            }
        }
    } 
    
    public static void afterDelete(List<Account> accList){    
        createOBMNotifiers(accList);
    }
    
    // Method to create OBM_Notifier record on acount delete
    // Added by Barkha Jain on April 28, 2015 @T-379785
    //Modified by Surabhi Sharma on July 14, 2015 @T-418696
    private static void createOBMNotifiers(List<Account> accList){ 
        list<OBM_Notifier__c> newRecords = new list<OBM_Notifier__c>();
        
        Set<Id> masterIdSet = new set<Id>();
        for(Account accObj : accList){      
            if(Account.MasterRecordId != null){
                masterIdSet.add(accObj.MasterRecordId);
            }
        }
        
        map<Id, String> masterExtIdMap = new map<Id, String>();
        if(!masterIdSet.isEmpty())
        {
            // Retrieve all master accounts
            for(Account masterAccount : [SELECT Id, Siebel_ID__c FROM Account
                                         WHERE  Id IN :masterIdSet]){
                masterExtIdMap.put(masterAccount.Id, masterAccount.Siebel_ID__c);
            }   
        }
        
        OBM_Notifier__c notifier;        
        for(Account accObj : accList){      
            //if(accObj.MasterRecordId == null){
                
            System.debug('==newList==' +accList);
            System.debug('SiebelId====' +accObj.Siebel_ID__c);
            
                // Create OBM_Notifier__c record
                notifier = new OBM_Notifier__c(); 
                notifier.Name = accObj.Name ;        
                notifier.External_Id__c = accObj.Siebel_ID__c ;
                notifier.Operation__c = 'Delete';
                notifier.Object_Type__c = 'Account';
                //notifier.RecordTypeId   = CC_Utility.naairRectypeId;
                if (accObj.MasterRecordId != null && masterExtIdMap.containsKey(accObj.MasterRecordId))
                {
                    notifier.SFDC_Id__c = accObj.MasterRecordId;
                    notifier.Master_External_Id__c = masterExtIdMap.get(accObj.MasterRecordId);
                }
                newRecords.add(notifier);
            //}
        }    
        if(!newRecords.isEmpty()){
            insert newRecords;
            System.debug('recordinserted==' +newRecords);
        } 
    }
    
    //Added this method to populate shipping addess of account on lead converiosn, as we cannot map standard address in standard lead mapping
    //Balmukund Janginr 1/13/2023
    public static void updateAccountAddresOnLeadConversion(List<Account> newList){
        /*Set<Id> accountIds = new Set<Id>();
        for(Account acc:newList){
            if(acc.WasConverted__c){
                accountIds.add(acc.id);
            }
        }        
        system.debug('accountIds==='+accountIds);
        List<Lead> convertedLeads;
        if(accountIds.size() > 0){
        	convertedLeads = [select id,Street,City,PostalCode,State,Country,ConvertedAccountId from Lead where ConvertedAccountId IN:accountIds];
        }
        system.debug('Updating Shipping Address************'+convertedLeads);
        List<Account> updateAccList;
        for(Account acc:newList){
            for(Lead led :convertedLeads){
                if(acc.WasConverted__c && acc.id == led.ConvertedAccountId){
                    acc.ShippingStreet = led.Street;
                    acc.ShippingCity = led.City;
                    acc.ShippingPostalCode = led.PostalCode;
                    acc.ShippingState = led.State;
                    acc.ShippingCountry = led.Country;
                    updateAccList.add(acc);
                }                
            }
        }*/
        //Calling quaueable class to update WasConverted flag to false on account so that validation rule will be fired on update on the shippinf address
        System.enqueueJob(new AccountQuaueableUpdateFlag(newList));
    }
    
    /*
    @author       	: AMS Team (Artur)
    @description    : method is used to set two digits country for Welch accounts : #03217504
    @param          : list of accounts, Id of recordtype will be always LFS Account Record Type
    @date         	: 18 Sep 2024
    */  
    public static void welchCountryUpdate(List<Account> accList, Id recordTypeId){
        for(Account acc : accList){
            acc.ShippingCountry = 'US';
            acc.RecordTypeId = recordTypeId;
        }
    }
     
}