public class AccountPlanTypeInfo {

    private static AccountPlanTypeInfo instance = null;
    public Map<Id,Schema.RecordTypeInfo> recordTypeInfoById {get; private set;}
    
    private AccountPlanTypeInfo(){
            Schema.DescribeSObjectResult accountPlanSchema = AccountPlan__C.sObjectType.getDescribe();
			// Get all record types for object mapped by record type Id
			recordTypeInfoById = accountPlanSchema.getRecordTypeInfosById();     
    }
    
    public static AccountPlanTypeInfo getInstance(){
        
        // There should be only one instance of this object so implemented via the singleton design pattern.
        if (instance == null){
            instance = new AccountPlanTypeInfo();
        }
        
        return instance;
    }
}