public without sharing class welch_TaxExemptUploaderController {
    private static final String  BASE_URL = URL.getSalesforceBaseUrl().toExternalForm();
    //Updated by AMS Team (Artur) - #03055954 - 09JUL2024
    private static final String  BASE_URL_INTERNAL_SF = URL.getOrgDomainURL().toExternalForm();

    @AuraEnabled
    public static List<File> getEntityRecordFiles(string recordId, Boolean isRecordUploaded)
    {        
        String userId = UserInfo.getUserId();
        User sysAdminUser = [SELECT Id, Name FROM User WHERE Profile.Name = 'System Administrator' AND IsActive=true LIMIT 1];

        //query for existing files where owner is user and then change owner id field on all of them in a for loop and update.
        List<ContentDocumentLink> links=[SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId=:recordId];
        Set<Id> ids=new Set<Id>();
        for(ContentDocumentLink link:links)
        {
            ids.add(link.ContentDocumentId);
        }
        
        List<ContentDocument> contentDocuments = [SELECT Id, OwnerId FROM ContentDocument WHERE OwnerId =:userId];

        for(ContentDocument condoc : contentDocuments){
            condoc.OwnerId = sysAdminUser.Id;
        }
        if(!contentDocuments.isEmpty()){
            update contentDocuments;
        }

        List<File> files=new List<File>();
        for(ContentVersion attach:[SELECT ContentDocumentId, ContentDocument.Title, ContentDocument.CreatedDate, ContentDocument.ContentSize,
                                            ContentDocument.FileType, FileExtension, CreatedDate, Title, Id
                                            FROM ContentVersion 
                                            WHERE ContentDocumentId IN : ids AND IsLatest = true ORDER BY ContentDocument.CreatedDate DESC])
         {
            File file=new File();
            file.Title = attach.ContentDocument.Title;
            file.contentDocumentId = attach.ContentDocument.Id;
            file.Id = attach.Id;
            file.Size = attach.ContentDocument.ContentSize;
            file.CreatedDate = attach.ContentDocument.CreatedDate;
            file.Type = attach.ContentDocument.FileType;
            file.ContentType = ContentType(attach.FileExtension);
            file.FileExtension = attach.FileExtension;
            String url = '/sfc/servlet.shepherd/version/renditionDownload?rendition=ORIGINAL_Jpg&versionId='+attach.Id;
            if(attach.FileExtension == 'docx'){
                url =  '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId='+attach.Id;
            }
              else if(attach.FileExtension == 'pdf'){
                url =  '/sfc/servlet.shepherd/version/renditionDownload?rendition=PDF&versionId='+attach.Id;
              }
            file.DownloadUrl =BASE_URL + '/sfc/servlet.shepherd/document/download/'+attach.ContentDocumentId;
            file.previewUrl =BASE_URL + url;
            files.add(file);
        }
        
        system.debug('isRecordUploaded ' + isRecordUploaded);
        
        //Updated by AMS Team (Artur) - #03055954 - 09JUL2024 - BEGIN 
        if(isRecordUploaded){
			String emailBody = System.label.Welch_Email_Body + ' ' + BASE_URL_INTERNAL_SF + '/' + recordId;
            sendMail(System.label.Welch_Email_Address, System.label.Welch_Email_Subject, emailBody);
        }
        //Updated by AMS Team (Artur) - #03055954 - 09JUL2024 - END 
        
        return files;
    }

    public static string ContentType(string fileType)
    {
        switch on fileType.toLowerCase()
        {
            when 'docx'
            {
                return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
            }
            when 'csv'
            {
                return 'application/vnd.ms-excel';
            }
            when 'wav'
            {
                return 'audio/wav';
            }
            when 'wmv'
            {
                return 'video/x-ms-wmv';
            }
            when 'png'
            {
                return 'image/png';
                
            }
            when 'pdf'
            {
                return 'application/pdf';
                
            }
            when else {
                return 'image/jpeg';
            }
        }
    }

    public class File{
        @AuraEnabled public String Title;
        @AuraEnabled public String Type;
        @AuraEnabled public Id Id;
        @AuraEnabled public Datetime CreatedDate;
        @AuraEnabled public String url;
        @AuraEnabled public String previewUrl;
        @AuraEnabled public String contentDocumentId;
        @AuraEnabled public Integer Size;
        @AuraEnabled public String ContentType;
        @AuraEnabled public String fullDownloadUrl;
        @AuraEnabled public String textPreview;
        @AuraEnabled public String DownloadUrl;
        @AuraEnabled public String FileExtension;
        @AuraEnabled public String fileSize;
    }
    
    /*
    @author       	: AMS Team (Artur)
    @description    : method is used to send an email to Welch Admins in order to Approve Tax Exemption file : Case #03055954
    @param          : address, subject, body
    @date         	: 7 July 2024
	*/  
    public static void sendMail(String address, String subject, String body) {
        
        // Create an email message object
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {address};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        
        //Send Email
        Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }
}