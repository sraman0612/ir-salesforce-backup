@IsTest
private class B2BCustomSearchControllerTest {
    @TestSetup
    static void testSetup() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
        User usr = CTS_TestUtility.createUser(false);
        usr.UserRoleId = r1.Id;
        usr.LastName = 'test user';
        usr.IsActive = true;
        insert usr;
        System.runAs(usr) {
            Account account = CTS_TestUtility.createAccount('TestAccount', false);
            account.OwnerId = usr.Id;
            insert account;
            B2BTestDataFactory.setupStandardPricebook();

            WebStore webstore = B2BTestDataFactory.createWebStore('Test Store');
            insert webstore;

            ProductCatalog productCatalog = B2BTestDataFactory.createCatalog('Catalog');
            insert productCatalog;

            WebStoreCatalog webstoreCatalog = B2BTestDataFactory.createWebStoreCatalog(webstore.Id, productCatalog.Id);
            insert webstoreCatalog;

            ProductCategory productCategory = B2BTestDataFactory.createProductCategory('Kits', productCatalog.Id);
            insert productCategory;

            RecordType recordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Product_ROB_B2B'];

            Product2 product = B2BTestDataFactory.createProduct('Product1', '1');
            product.Models__c = 'ES 65/66;ES 95;ES 145/155;ES 15/25';
            product.Landing_Page_Model__c = 'Robox Lobe 1 #15/25';
            product.RecordTypeId = recordType.Id;
            insert product;
            Product2 product1 = B2BTestDataFactory.createProduct('Product1', 'test');
            product1.Models__c = 'ES 145/155';
            product1.RecordTypeId = recordType.Id;
            product1.Landing_Page_Model__c = 'Robox Lobe 2 #55/65';
            insert product1;

            Models_Mapper__c modelsMapper = new Models_Mapper__c(Name = 'Model 1', Robox_Lobe_Model__c = 'Robox Lobe 1 #15/25');
            insert modelsMapper;
        }
    }

    @IsTest
    static void testGetCategoryId() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
        User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id AND LastName = 'test user' LIMIT 1];
        // Test the getCategoryId method
        System.runAs(user) {
            Test.startTest();
            String categoryId = B2BCustomSearchController.getCategoryId([SELECT Id FROM WebStore LIMIT 1].Id, 'Kits');
            Test.stopTest();

            System.assertNotEquals(null, categoryId, 'There should be a category with name Kits');
            System.assertEquals('Kits', [SELECT Id, Name FROM ProductCategory LIMIT 1].Name, 'There should be a category with name Kits');
        }
    }

    @IsTest
    static void testGetRelevantModels() {
        Test.startTest();
        List<B2BCustomSearchController.searchResultsWrapper> resultsWrappers = B2BCustomSearchController.getRelevantAssets('ES');
        Test.stopTest();

        System.assertNotEquals(null, resultsWrappers, 'There should be at least one result');
        System.assertNotEquals(5, resultsWrappers.size(), 'There should be 5 results');
        for (B2BCustomSearchController.searchResultsWrapper searchResultsWrapper : resultsWrappers) {
            System.assertEquals(true, searchResultsWrapper.name.contains('ES'), 'The name should contain ES');
            System.assertEquals(true, searchResultsWrapper.modelsToFilter.contains('ES'), 'The modelsToFilter should contain ES');
            System.assertEquals(true, searchResultsWrapper.modelName.contains('ES'), 'The modelName should contain ES');
        }
    }

    @IsTest
    static void testGetRelevantLandingPageModels() {
        Test.startTest();
        List<B2BCustomSearchController.searchResultsWrapper> resultsWrappers = B2BCustomSearchController.getRelevantAssets('Robox Lobe');
        Test.stopTest();

        System.assertEquals(2, resultsWrappers.size(), 'There should be 2 results');
        for (B2BCustomSearchController.searchResultsWrapper searchResultsWrapper : resultsWrappers) {
            System.assertEquals(true, searchResultsWrapper.name.contains('Robox Lobe'), 'The name should contain Robox Lobe');
            System.assertEquals(true, searchResultsWrapper.modelsToFilter.contains('Robox Lobe'), 'The modelsToFilter should contain Robox Lobe');
            System.assertEquals(true, searchResultsWrapper.modelName.contains('Robox Lobe'), 'The modelName should contain Robox Lobe');
        }
    }

    @IsTest
    static void testSearchSerialNumbers() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new B2BCustomSearchCalloutMock());

        Test.startTest();
        List<B2BCustomSearchController.searchResultsWrapper> resultsWrappers = B2BCustomSearchController.getRelevantAssets('RB1234567');
        Test.stopTest();

        System.assertEquals(1, resultsWrappers.size(), 'There should be 1 result');
        System.debug(resultsWrappers);
        for (B2BCustomSearchController.searchResultsWrapper searchResultsWrapper : resultsWrappers) {
            System.assertEquals(true, searchResultsWrapper.name.containsIgnoreCase('RB'), 'The name should contain RB1234567');
            System.assertEquals(true, searchResultsWrapper.modelsToFilter.contains('Robox Lobe'), 'The modelsToFilter should contain Robox Lobe');
            System.assertEquals(true, searchResultsWrapper.modelName.contains('Robox Lobe'), 'The modelName should contain Robox Lobe');
        }
    }

    @IsTest
    static void testGetRelevantAssetFail() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new B2BCustomSearchCalloutMock404());

        Test.startTest();
        try {
            List<B2BCustomSearchController.searchResultsWrapper> resultsWrappers = B2BCustomSearchController.getRelevantAssets('RB2409586');
        }
        catch(B2BCustomSearchController.CustomException e) {
            System.assertEquals(e.message, 'No asset found');
        }
        Test.stopTest();
    }
}