//
// (c) 2015, Appirio Inc
//
// Test class for Opportunity Line Item Trigger
//
// Apr 28, 2015     George Acker     original
// Aug 5, 2015      Surabhi Sharma   Modified(I-175039)
// 
@isTest
private class Opportunity_Line_Item_Trigger_Test {
    
    // Method to test opportunity line item trigger
    static testMethod void testOLITrigger() {
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.closedate = system.today();
        o.stagename =  'Stage 1. Qualify';
        o.RecordTypeId=[SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        insert o;
        Opportunity_Line_Item__c oli = new Opportunity_Line_Item__c();
        oli.opportunity__c = o.id;
        oli.amount__c = 100000;
        oli.quantity__c = 2;
        insert oli; 
        
        Test.startTest();
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
        System.assertEquals(oli.amount__c * oli.quantity__c,o.amount);
        
        Opportunity_Line_Item__c oli2 = new Opportunity_Line_Item__c();
        oli2.opportunity__c = o.id;
        oli2.amount__c = 10000;
        oli2.quantity__c = 10;
        insert oli2; 
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
        System.assertEquals(oli.amount__c * oli.quantity__c + oli2.amount__c * oli2.quantity__c,o.amount);
        
        oli.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
        update oli;
        
        sObject[] sObjQuery = [SELECT Id FROM Opportunity_Line_Item__c WHERE Id = :oli.Id];
        System.assertEquals(0, sObjQuery.size());  
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
        System.assertEquals(oli2.amount__c * oli2.quantity__c,o.amount);
        
        Quote__c q = new Quote__c();
        q.name = 'Test Quote';
        q.opportunity__c = o.id;
        //q.Quote_Sales_Price__c = 500000;
        insert q;
        
        Quote_Line_Item__c qlt = new Quote_Line_Item__c();
        qlt.Name = 'test qlt';
        qlt.Quote__c = q.id;
        qlt.NRC_Subtotal__c = 1000;
        insert qlt;
        
         // Query the Quote__c again to get the updated rollup summary field.
        Quote__c QuoteAfterInsert = [Select Quote_SalesPrice__c from Quote__c where Id= :q.Id];
            System.assertEquals(1000, QuoteAfterInsert.Quote_SalesPrice__c);
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
      //  System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c,o.amount);
        
        oli2.amount__c = 11000;
        update oli2;
        
        o = [SELECT amount FROM Opportunity WHERE Id = :o.Id];
        //System.assertEquals(QuoteAfterInsert.Quote_SalesPrice__c,o.amount);
        
        delete q;
        delete oli2;
           
        Test.stopTest();
    }
}