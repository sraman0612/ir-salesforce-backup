/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the Test class for AccountWhiteSpace_Page_Extensions.
------------------------------------------------------------*/
@istest(seeAllData=false)
public with sharing class AccountWhiteSpace_Page_Extensions_Test {
    
    @testSetup
    static void dataSetup(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        //Ashwini: 27-SEP-17 :System.runAs used ,to avoid MIXED_DML_OPERATION
        System.runAs(u){
        account testAcc=TestDataFactory.createAccount('TestAcc1','IR Comp EU');
        testAcc.ownerid=u.id;
        insert testAcc;
        }
    }
    
    public static testMethod void AccountWhiteSpace_Page_Extensions_Test(){
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            Account testAcc=[Select id from account limit 1];
            ApexPages.StandardController controller=new ApexPages.StandardController(testAcc);
            AccountWhiteSpace_Page_Extensions aWSpace1=new AccountWhiteSpace_Page_Extensions(controller);
            system.assertequals(aWSpace1.ShowGenPage,false);
            //aWSpace1.getOfferings();
            aWSpace1.location='Mumbai';
            aWSpace1.createAccountWhiteSpace();
            AccountWhiteSpace_Page_Extensions aWSpace2=new AccountWhiteSpace_Page_Extensions(controller);
            //system.assertequals(aWSpace2.ShowGenPage,true);
            aWSpace2.CancelWP();
            aWSpace2.SavewhiteSpace();
            aWSpace2.deletelocation='Mumbai';
            aWSpace2.deleteLocation();
        }
        Test.Stoptest();
    }

}