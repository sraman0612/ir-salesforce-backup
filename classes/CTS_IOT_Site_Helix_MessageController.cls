public with sharing class CTS_IOT_Site_Helix_MessageController {
    
    @AuraEnabled(cacheable=true)
    public static Boolean hasConnectedAssets(Id siteId){
        Account site = [Select Id, Name, (Select Id, Name From Assets Where Helix_Connectivity_Status__c = 'Active') From Account Where Id = :siteId];
        return site.Assets.size() > 0;
    }
}