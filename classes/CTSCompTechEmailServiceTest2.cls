@isTest
Public class CTSCompTechEmailServiceTest2 {
    @isTest
    Static void validateCTSCompTechEmailService() {
        
        // Create Contact record
        Contact cont = new Contact();
        cont.FirstName = 'John';
        cont.LastName = 'Contact';
        cont.Phone =  '3143013675';
        cont.Title = 'Any Title';
        cont.Email = 'mgcantoni.es@gmail.com';
        insert cont;

        // Email
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.fromAddress = 'mgcantoni.es@gmail.com';
        email.subject = 'Compressor 4 Pre-sale Missed Contact';
        String [] htmlText = new List<String>();
        htmlText.add('<P>You have a new voice message from a caller from 3143013675</P>');
        htmlText.add('');
        htmlText.add('<P>Contact ID: 167814331699</P');
        htmlText.add('<P>Date/Time: 10/19/2021 4:30:04 PM</P><P>Skill: 846941 - </P>');
        htmlText.add('<P>Number Dialed:');
        htmlText.add('<P></P>');
        email.htmlBody = string.join(htmlText,'\n');

        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        CTSCompTechEmailService testCTSCompTechEmailService = new CTSCompTechEmailService();
        testCTSCompTechEmailService.handleInboundEmail(email, env);

        List<Case> createdCases = [Select Id from Case];

        System.assert(createdCases.isEmpty(),'List should be empty');

        System.assert(createdCases.size() == 0,'0 case record should be returned');

    }
}