/**
 * Test class of CTS_IOT_Submit_Service_RequestController
 **/
@isTest
private class CTS_IOT_Submit_Service_RequestCntrlTest {
    
    @testSetup
    static void setup(){
        
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        List<Account> accounts = new List<Account>();
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        accounts.add(acc);
        accounts.add(CTS_TestUtility.createAccount('Test Account 1', false));
        insert accounts;
        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(false);
        setting.Account_Matching_Record_Types__c = accounts.get(0).recordTypeId;
        setting.Asset_Matching_Record_Types__c = assetRecTypeId;
        setting.Asset_Matching_Record_Types2__c = assetRecTypeId;
        insert setting;
        Asset asset = CTS_TestUtility.createAsset('Test Asset', '12345', accounts.get(0).Id, assetRecTypeId, false);
        asset.IRIT_RMS_Flag__c = true;
        insert asset;
        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', accounts.get(0).Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', accounts.get(0).Id, false));
        for(Contact con : contacts){
            con.CTS_IOT_Community_Status__c = 'Approved';
        }
        insert contacts;
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(contacts.get(0).Id, profileId, true);
        
    }
}
    @isTest
    static void testGetInit(){
        List<Account> accounts = [SELECT id, Name FROM Account WHERE Name = 'Test Account'];
        if(!accounts.isEmpty()){
            List<Asset> assets = [SELECT id, Name FROM Asset where Name = 'Test Asset'];
            if(!assets.isEmpty()){
                Test.startTest();
                CTS_IOT_Submit_Service_RequestController.InitResponse initRes = CTS_IOT_Submit_Service_RequestController.getInit(assets.get(0).Id, accounts.get(0).Id);
                System.assert(initRes != null);
                initRes = CTS_IOT_Submit_Service_RequestController.getInit('', accounts.get(0).Id);
                System.assert(initRes != null);
                Test.stopTest();
            }
        }
    }
    @isTest
    static void testSubmitServiceRequest(){
        Test.startTest();
        List<Asset> assets = [SELECT id, Name FROM Asset where Name = 'Test Asset'];
        if(!assets.isEmpty()){
            CTS_IOT_Service_Request__c serviceRequest = new CTS_IOT_Service_Request__c(Asset__c = assets.get(0).Id);
            String serviceRequestJSON = (String)JSON.serialize(serviceRequest);
            LightningResponseBase responseBase = CTS_IOT_Submit_Service_RequestController.submitServiceRequest(serviceRequestJSON);
            System.assert(responseBase != null);
        }
        Test.stopTest();
    }
}