public with sharing class AccountContactRelationTriggerHandler {

    private Map<Id, AccountContactRelation> triggerNewMap, triggerOldMap;
    
    public AccountContactRelationTriggerHandler(Map<Id, AccountContactRelation> triggerNewMap, Map<Id, AccountContactRelation> triggerOldMap) {
        this.triggerNewMap = triggerNewMap;
        this.triggerOldMap = triggerOldMap;
    }

    public void onAfterDelete(){

        if (triggerOldMap != null){
        
            // Get all accounts removed from contacts
            Map<Id, Set<Id>> contactRemovedAccountIds = new Map<Id, Set<Id>>();
            Set<Id> allRemovedAccountIds = new Set<Id>();
            Set<Id> allContactIds = new Set<Id>();

            for (AccountContactRelation acr : triggerOldMap.values()){

                allContactIds.add(acr.ContactId);
                allRemovedAccountIds.add(acr.AccountId);

                if (contactRemovedAccountIds.containsKey(acr.ContactId)){
                    contactRemovedAccountIds.get(acr.ContactId).add(acr.AccountId);
                }
                else{
                    contactRemovedAccountIds.put(acr.ContactId, new Set<Id>{acr.AccountId});
                }
            }

            // Find standard community contacts impacted by the account removals
            Map<Id, User> communityContactUsers = new Map<Id, User>(
                [Select Id, Name, ContactId, AccountId
                 From User 
                 Where ContactId in :allContactIds and ProfileId = :CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id]
            );

            system.debug('community users found: ' + communityContactUsers.size());
            
            if (communityContactUsers.size() > 0){

                // Look for any site specific subscriptions for the user/site removal combinations
                Notification_Subscription__c[] subscriptionsToRemove = new Notification_Subscription__c[]{};

                Notification_Subscription__c[] siteSpecificSubscriptions = [Select Id, Name, OwnerId, Record_ID__c From Notification_Subscription__c Where OwnerId in :communityContactUsers.keySet() and Record_ID__c in :allRemovedAccountIds];

                if (siteSpecificSubscriptions.size() > 0){

                    for (Notification_Subscription__c siteSubscription : siteSpecificSubscriptions){

                        User communityContactUser = communityContactUsers.get(siteSubscription.OwnerId);
                        Set<Id> removedAccountIds = contactRemovedAccountIds.get(communityContactUser.ContactId);

                        if (removedAccountIds != null && removedAccountIds.contains(siteSubscription.Record_ID__c)){
                            subscriptionsToRemove.add(siteSubscription);
                        }
                    }
                }

                Notification_Subscription__c[] childSpecificSubscriptions =
                    [Select Id, Name, OwnerId, Record_ID__c
                     From Notification_Subscription__c 
                     Where OwnerId in :communityContactUsers.keySet() and Notification_Type__r.Parent_Object_Grandparent_Type__c = 'Account' and Record_ID__c != null];

                if (childSpecificSubscriptions.size() > 0){

                    Set<Id> childIds = new Set<Id>();

                    for (Notification_Subscription__c childSpecificSubscription : childSpecificSubscriptions){
                        childIds.add(childSpecificSubscription.Record_ID__c);
                    }

                    // TODO: add logic to support any possible account child object type
                    Map<Id, Id> childAccountMap = new Map<Id, Id>();

                    for (Asset asset : [Select Id, AccountId From Asset Where Id in :childIds]){
                        childAccountMap.put(asset.Id, asset.AccountId);
                    }

                    for (Notification_Subscription__c childSpecificSubscription : childSpecificSubscriptions){

                        User communityContactUser = communityContactUsers.get(childSpecificSubscription.OwnerId);
                        Set<Id> removedAccountIds = contactRemovedAccountIds.get(communityContactUser.ContactId);

                        Id childParentAccountId = childAccountMap.get(Id.valueOf(childSpecificSubscription.Record_ID__c));

                        if (childParentAccountId != null && removedAccountIds != null && removedAccountIds.contains(childParentAccountId)){
                            subscriptionsToRemove.add(childSpecificSubscription);
                        }
                    }                    
                }

                system.debug('subscriptionsToRemove: ' + subscriptionsToRemove.size());

                if (subscriptionsToRemove.size() > 0){
                    Database.delete(subscriptionsToRemove, false);
                }
            }
        }
    }    
}