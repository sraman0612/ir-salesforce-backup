@isTest
public class RecordTypeUtilityClassTest {
	@isTest
    static void testGetRecordTypeList(){
        Test.startTest();
        List<RecordType>recordTypeList1 = new List<RecordType>();
        List<RecordType>recordTypeList2 = new List<RecordType>();
        recordTypeList1 = RecordTypeUtilityClass.getRecordTypeList('Account');
        recordTypeList2 = RecordTypeUtilityClass.getRecordTypeList('CTS_Centac_Parts_Quote_ReRate__c');
        if(recordTypeList1!=null && !recordTypeList1.isEmpty()){
            system.assertEquals(recordTypeList1[0].SobjectType, 'Account');
        }else{
            system.assertEquals(recordTypeList1, null);
        }
        if(recordTypeList2!=null && !recordTypeList2.isEmpty()){
            system.assertEquals(recordTypeList2[0].SobjectType, 'CTS_Centac_Parts_Quote_ReRate__c');
        }else{
            system.assertEquals(recordTypeList2, null);
        }
        
        Test.stopTest();
    }
}