public with sharing class PT_DSMP_AP02_CreateStrategicProduct {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Create Strategic Products Record
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/     

    public static void createRecords(List<PT_DSMP_NPD__c> lstNPD, Set<String> setYear){
        List<PT_DSMP_Strategic_Product__c> lstStrategicProduct = new List<PT_DSMP_Strategic_Product__c>();
        Map<String, List<PT_DSMP__c>> mapYearTolstDSMP = new Map<String, List<PT_DSMP__c>>();

        for(PT_DSMP__c current : [SELECT Id,
                                         PT_DSMP_Year__c
                                  FROM PT_DSMP__c
                                  WHERE PT_DSMP_Year__c IN: setYear]){

            if(mapYearTolstDSMP.containsKey(current.PT_DSMP_Year__c)){
                mapYearTolstDSMP.get(current.PT_DSMP_Year__c).add(current);
            } else {
                mapYearTolstDSMP.put(current.PT_DSMP_Year__c, new List<PT_DSMP__c>{current});
            }

        }

        for(PT_DSMP_NPD__c current : lstNPD){

            if(mapYearTolstDSMP.containsKey(current.PT_DSMP_Year__c)){
                for(PT_DSMP__c currentDSMP : mapYearTolstDSMP.get(current.PT_DSMP_Year__c)){

                    lstStrategicProduct.add(new PT_DSMP_Strategic_Product__c(
                        Name = current.Name,
                        DSMP_NPD__c = current.Id,
                        PT_DSMP__c = currentDSMP.Id,
                        Hide__c = current.Hide__c
                    ));
                
                }

                //AsyncInsert(JSON.serialize(lstStrategicProduct));
                //lstStrategicProduct = new List<PT_DSMP_Strategic_Product__c>();

            }

        }

        System.debug('mgr lstStrategicProduct ' + lstStrategicProduct.size() );

        if(lstStrategicProduct.size() > 0){
            //database.insert(lstStrategicProduct, false);
            //insert lstStrategicProduct;
            //
            PT_DSMP_BAT03_InsertStrategicProducts batch = new PT_DSMP_BAT03_InsertStrategicProducts(lstStrategicProduct);
            Database.executebatch(batch);

            //Integer bs = 200;
            //for(Integer i = 0 ; i < (lstStrategicProduct.size() / bs)+1 ; i++){
            //    List<PT_DSMP_Strategic_Product__c> lstStrategicProductToInsert = new List<PT_DSMP_Strategic_Product__c>();
                
            //    for(Integer j=(i*bs);(j<(i*bs)+bs) && j<lstStrategicProduct.size() ; j++){
            //        lstStrategicProductToInsert.add(lstStrategicProduct.get(j));
            //    }
                
            //    System.debug('mgr lstStrategicProductToInsert ' + lstStrategicProductToInsert.size());
            //    AsyncInsert(JSON.serialize(lstStrategicProductToInsert));

            //}
            
        }

    }

    //@future
    //public static void AsyncInsert(String recordIds) {

    //    list<sobject> lstSObject =(list<sobject>)JSON.deserialize(recordIds, List<sobject>.class);

    //    System.debug('mgr lstSObject ' + lstSObject.size());
    //    if(lstSObject.size() > 0){
    //        //database.insert(lstSObject, false);
    //        insert lstSObject;
            

            //Integer bs = 200;
            //for(Integer i = 0 ; i < (lstSObject.size() / bs)+1 ; i++){
            //    List<sobject> lstStrategicProductToInsert = new List<sobject>();
                
            //    for(Integer j=(i*bs);(j<(i*bs)+bs) && j<lstSObject.size() ; j++){
            //        lstStrategicProductToInsert.add(lstSObject.get(j));
            //    }
                
            //    System.debug('mgr future lstStrategicProductToInsert ' + lstStrategicProductToInsert.size());

            //    insert lstStrategicProductToInsert;

            //}
            
    //    }
    //}




}