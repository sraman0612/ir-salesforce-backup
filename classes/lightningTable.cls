public class lightningTable {
    @AuraEnabled
    public tableColumns [] tableColumnsLst {get;set;}
    @AuraEnabled
    public sObject [] tableDataLst {get;set;}
    
    public lightningTable(){
        tableColumnsLst = new List<tableColumns>();
        tableDataLst = new List<sObject>();
    }
}