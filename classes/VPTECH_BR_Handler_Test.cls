@isTest
public class VPTECH_BR_Handler_Test {

  static testMethod void testBRValidation() {
      	Profile p = [Select id from Profile where Name = 'Standard Sales'];
   		User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                Business__c ='Low Pressure',timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
        System.runas(user) {
    	Account testAccount = new Account(Name='Test Account');
    	insert testAccount;
    	Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Low Pressure');
    	insert testBr;
    
        List<Business_Relationship__c> BR = new List<Business_Relationship__c>();
    	Test.startTest();
            VPTECH_BusinessRelatioshipHandler.userBusinessValidation(BR);
            System.assertEquals(0, testBr.getErrors().size());
        	//System.assertEquals('You can not create Business Relationships for businesses other than yours.', testBr.getErrors()[0].getMessage());
    Test.stopTest();
  }
}

}