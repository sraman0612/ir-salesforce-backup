@isTest
private class IREmployeeEventHandler_Test 
{
    static testMethod void UnitTest_IREmployeeEventHandler()
    {   
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        //psettingList = TestUtilityClass.createPavilionSettings();
                
        //insert psettingList;
                
        List<IR_Employee__c> irEmployeeList = new List<IR_Employee__c>();
        irEmployeeList = TestUtilityClass.createIREmployeeEvents(1);
        
        insert irEmployeeList ;
         
        Test.startTest();
        
        Id p = [select id from profile where name= 'System Administrator'].id;
            
        User u = new User();
        u = TestUtilityClass.createNonPortalUser(p);
        u.FederationIdentifier = 'cccphktest';
            
        insert u;
        
        irEmployeeList[0].Active_Directory_ID__c = 'CCCPHKTEST';
        update irEmployeeList ;
        delete irEmployeeList ;
        undelete irEmployeeList ;
        Test.stopTest();
    }
    
    static testMethod void UnitTest_ContactSync() {
        List<IR_Employee__c> irEmployeeList1 = TestUtilityClass.createIREmployeeEvents(1);
        irEmployeeList1[0].Active_Directory_ID__c = '8675309JENNY';
        irEmployeeList1[0].Employee_ID__c = '8675309';
        insert irEmployeeList1;
        String AD_Id1 = irEmployeeList1[0].Active_Directory_ID__c;      
        //IREmployeeEventHandler.upsertContactFromEmployee(irEmployeeList1);
        // no club car business unit desc means no contact is created
        system.assert([SELECT Id from Contact WHERE CORP_Id__c = :AD_Id1].size() == 0);       
      
        List<IR_Employee__c> irEmployeeList = new List<IR_Employee__c>();
        irEmployeeList = TestUtilityClass.createIREmployeeEvents(1);
        irEmployeeList[0].Business_Unit_Description__c = 'Club Car';
        insert irEmployeeList;
        String AD_Id = irEmployeeList[0].Active_Directory_ID__c;      
        // 2 Upserts should only result in one record
        //IREmployeeEventHandler.upsertContactFromEmployee(irEmployeeList);
        //IREmployeeEventHandler.upsertContactFromEmployee(irEmployeeList);
        //system.assert([SELECT Id from Contact WHERE CORP_Id__c = :AD_Id].size() == 1);       
    }

    static testMethod void UnitTest_DeactivateUsers(){
    	
    	String[] federationIds = new String[]{
    		'1234560',
    		'1234567',
    		'1234568',
    		'1234569'
    	};
    	
    	/*User communityUser = TestUtilityClass.createPortalUserCC();
    	communityUser.FederationIdentifier = federationIds[0];
    	//communityUser.UserRoleId = [Select Id From UserRole Where PortalType = 'Partner' Order By Name ASC LIMIT 1].Id;
    	communityUser.DB_Region__c = 'EMEIA';
    	communityUser.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';
    	*/
    	Id profileId = [Select Id From Profile Where Name = 'System Administrator'].Id;
    	
    	User sysAdmin2;
    	
    	system.runAs([Select Id, Name From User Where isActive = true and Profile.Name = 'System Administrator' LIMIT 1][0]){
    	
	    	User user1 = TestUtilityClass.createNonPortalUser(profileId);
	    	user1.Username += '1';
	    	user1.FederationIdentifier = federationIds[1];
    		user1.DB_Region__c = 'EMEIA';
    		user1.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';	    	
	    	
	    	User user2 = TestUtilityClass.createNonPortalUser(profileId);
	    	user2.Username += '2';
	    	user2.FederationIdentifier = federationIds[2];
    		user2.DB_Region__c = 'EMEIA';
    		user2.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';		    	
	    	
	    	User user3 = TestUtilityClass.createNonPortalUser(profileId);
	    	user3.Username += '3';
	    	user3.FederationIdentifier = federationIds[3];  
    		user3.DB_Region__c = 'EMEIA';
    		user3.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';		    	
    	
    		sysAdmin2 = TestUtilityClass.createNonPortalUser([Select Id From Profile Where Name = 'System Administrator' LIMIT 1].Id);
    		sysAdmin2.Username += '4';
    		sysAdmin2.DB_Region__c = 'EMEIA';
    		sysAdmin2.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';	    		
    	
    		User[] users = new User[]{ user1, user2, user3, sysAdmin2}; 

    		insert users;
    	} 	
    	
    	system.runAs(sysAdmin2){
    	
	    	User_Automation__c settings = User_Automation__c.getOrgDefaults();
	    	
	    	settings.Deactivation_Trigger_Active__c = true;
	    	settings.Deactivation_Statuses__c = 'T;E';
	    	
	    	upsert settings;    	    	
	    
	    	IR_Employee__c[] newEmployees;
	    
	    	Test.startTest();
	    	{
		    	newEmployees = TestUtilityClass.createIREmployeeEvents(4);
		    	
		    	// Termination date with qualifying status
		    	newEmployees[0].Date_of_Termination__c = Date.today().addDays(-1);
		    	newEmployees[0].Empl_Status__c = 'T';
		    	newEmployees[0].Active_Directory_ID__c = federationIds[1];
		    	
		    	// Termination date with non-qualifying status
		    	newEmployees[1].Date_of_Termination__c = Date.today().addDays(-1);
		    	newEmployees[1].Empl_Status__c = 'A';
		    	newEmployees[1].Active_Directory_ID__c = federationIds[2];
		    	
		    	// No termination date with qualifying status
		    	newEmployees[2].Date_of_Termination__c = null;
		    	newEmployees[2].Empl_Status__c = 'T';	 
		    	newEmployees[2].Active_Directory_ID__c = federationIds[3]; 
	
		    	// Termination date with qualifying status for a community user
		    	newEmployees[3].Date_of_Termination__c = Date.today().addDays(-1);
		    	newEmployees[3].Empl_Status__c = 'T'; 		
		    	newEmployees[3].Active_Directory_ID__c = federationIds[0];    	
		    	
		    	insert newEmployees;
		    	
		    	system.assertEquals(newEmployees.size(), [Select Count() From IR_Employee__c]);
	    	}
	    	Test.stopTest();
	    	
	    	// Verify that 1 of the users was deactivated
	    	User[] deactivatedUsers = [Select Id, FederationIdentifier From User Where isActive = False and FederationIdentifier in :federationIds];
	    	
	    	system.assertEquals(1, deactivatedUsers.size());
	    	system.assertEquals(federationIds[1], deactivatedUsers[0].FederationIdentifier);
	    	
	    	// Update another one of them to qualify and one of them to fail so it will be frozen
	    	newEmployees[1].Empl_Status__c = 'T';
	    	newEmployees[2].Date_of_Termination__c = Date.today().addDays(-1);
	    	
	    	IREmployeeEventHandler.federationIdsToFreeze.add(federationIds[3]);
	    	
	    	update new IR_Employee__c[]{newEmployees[1], newEmployees[2]};
	    	
	    	deactivatedUsers = [Select Id, FederationIdentifier From User Where isActive = False and FederationIdentifier in :federationIds Order By FederationIdentifier ASC];
	    	
	    	system.assertEquals(2, deactivatedUsers.size());
	    	system.assertEquals(federationIds[1], deactivatedUsers[0].FederationIdentifier);
	    	system.assertEquals(federationIds[2], deactivatedUsers[1].FederationIdentifier);	
	    	
	    	// Verify the last one was frozen as it should have failed
	    	system.assertEquals(1, [Select Count() From UserLogin Where isFrozen = true and UserId in (Select Id From User Where FederationIdentifier in :federationIds)]);    	
    	}
    }
    static testMethod void UnitTest_updateUserFromEmployee() {
        Id p = [select id from profile where name= 'System Administrator'].id;
        User u = new User();
        u = TestUtilityClass.createNonPortalUser(p);
        u.FederationIdentifier = '8675309JENNY';
            
        insert u;

        system.runAs(u){
        
            Set<String> corpIDs2Update  = new Set<String>();
            corpIDs2Update.add('8675309JENNY');
            List<IR_Employee__c> irEmployeeList1 = TestUtilityClass.createIREmployeeEvents(1);
            irEmployeeList1[0].Active_Directory_ID__c = '8675309JENNY';
            irEmployeeList1[0].Employee_ID__c = '8675309';
            insert irEmployeeList1;
            IREmployeeEventHandler.updateUserFromEmployee(corpIDs2Update);
        }    
        
        
    }
}