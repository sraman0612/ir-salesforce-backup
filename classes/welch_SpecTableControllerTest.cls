@isTest
public with sharing class welch_SpecTableControllerTest {
    @TestSetup
    static void makeData(){
        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User sysAdminUser = new User(LastName = 'test user 1', 
                             Username = 'userName.1@example.gov', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US'); 
       	System.runAs(sysAdminUser){
            List<Product2> prods = welch_TestDataFactory.createChildProducts(true);
            ProductAttributeSet attSet = welch_TestDataFactory.createProductAttributeSet(true);
            Product2 parentProd = welch_TestDataFactory.createVariationParent(true);
            FieldDefinition modelField = [SELECT Id, EntityDefinition.Label, QualifiedApiName, DurableId 
                                        FROM FieldDefinition 
                                        WHERE EntityDefinition.QualifiedApiName = 'ProductAttribute' 
                                        AND QualifiedApiName = 'welch_model__c'];
    
            ProductAttributeSetItem setItem = welch_TestDataFactory.createProductAttributeSetItem(attSet, modelField, true);
            ProductAttributeSetProduct prodAttSetProd = welch_TestDataFactory.createProductAttributeSetProduct(attSet, parentProd, true);
            List<ProductAttribute> prodAttr = welch_TestDataFactory.createProductAttributes(prods, parentProd, true);
            ProductCatalog testCatalog = welch_TestDataFactory.createProductCatalog(true);
            ProductCategory category = welch_TestDataFactory.createProductCategory(testCatalog, true);
            ProductCategoryProduct prodCatProd = welch_TestDataFactory.createProductCategoryProduct(parentProd, category, true);
        }
        //create product to be parent
        //create product to be child
        //create product attribute?
        //create product attribute set item
        //use model field to 
    }
    @isTest
    static void testGetSpecData(){
        Product2 tempProd = [SELECT Id, LFS_welch_Built_in_Adjustable_Vacuum__c, LFS_welch_Chemical_Resistant__c, StockKeepingUnit, Name FROM Product2 WHERE Name != 'Local Parent' LIMIT 1];
        List<Product2> prodInfo = welch_SpecTableController.getSpecData(tempProd.Id);
        System.assertNotEquals(0, prodInfo.size(), 'Spec data was returned');
        Product2 parentProd = [SELECT Id, LFS_welch_Built_in_Adjustable_Vacuum__c, LFS_welch_Chemical_Resistant__c, StockKeepingUnit, Name FROM Product2 WHERE Name = 'Local Parent' LIMIT 1];
        List<Product2> prods = welch_SpecTableController.getSpecData(parentProd.Id);
        System.assertNotEquals(0, prods.size(), 'Spec data was returned');
    }
    @isTest
    static void testGetFieldNameValues(){
        welch_SpecTableController.fieldNameData fieldName = new welch_SpecTableController.fieldNameData();
        fieldName.apiName = 'TestApiName';
        fieldName.label = 'Test Field Label';
        List<Product2> prodInfo = [SELECT Id, LFS_welch_Built_in_Adjustable_Vacuum__c, LFS_welch_Chemical_Resistant__c, StockKeepingUnit, Name FROM Product2 WHERE Name != 'Local Parent'];
        List<welch_SpecTableController.fieldNameData> controller = new List<welch_SpecTableController.fieldNameData>();
        controller = welch_SpecTableController.getFieldNameValues(prodInfo);
        System.assertNotEquals(0, controller.size(), 'There are fields populated on the products');
    }
}