@istest
private class SurveyAndQuestionController_Test {

    @TestSetup
    static void dataSetup(){
        User surveyUser = CTS_TestUtility.createUser(true);//email=test@test.com
        PermissionSet surveyPerm = [SELECT Id from PermissionSet WHERE PermissionsManageSurveys = true LIMIT 1];
        PermissionSetAssignment assignedPS = new PermissionSetAssignment();
        assignedPS.PermissionSetId = [SELECT Id from PermissionSet WHERE PermissionsManageSurveys = true LIMIT 1].id;
        assignedPS.AssigneeId = surveyUser.id;
        Database.insert(assignedPS);
    }

    //aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
    //test constructor and calls within it
    @isTest
    private static void testCreateQuestionController(){
        SurveyTestingUtil tu = new SurveyTestingUtil();
        Apexpages.currentPage().getParameters().put('id',tu.surveyId);
        Apexpages.Standardcontroller std;
        SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
        cqc.addQuestion();
        cqc.getNewQuestionNum();
        cqc.makeNewQuestionLink();
        System.assert(cqc.surveyId == tu.surveyId);
    }

    @isTest
    //test constructor and calls within it
    private static void testEditQuestion(){
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller std;
            SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
            cqc.editQuestion();
            cqc.questionReference = tu.questionIds[0];
            cqc.editQuestion();
            System.assert(cqc.questionType==tu.allQuestions[0].Type__c);
            cqc.questionReference = tu.questionIds[1];
            cqc.editQuestion();
            System.assert(cqc.questionType==tu.allQuestions[1].Type__c);
            cqc.questionReference = tu.questionIds[2];
            cqc.editQuestion();
            System.assert(cqc.questionType==tu.allQuestions[2].Type__c);
            cqc.questionReference = tu.questionIds[3];
            cqc.editQuestion();
            System.assert(cqc.questionType==tu.allQuestions[3].Type__c);
            System.assert(cqc.editQuestion()==null);
        }

    }

    @isTest
    //test the saving of new questions
    private static void testsaveAndNewQuestion(){
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller std;
            SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
            //test saving new question
            cqc.qQuestion = 'THIS IS A NEW QUESTION';
            cqc.qChoices = '1\\n2\\n3\\3';
            cqc.qRequired=true;
            cqc.questionType='Single Select--Vertical';
            cqc.saveAndNewQuestion();
            System.assertEquals(5, cqc.getNewQuestionNum());
            //edit existing question
            SurveyAndQuestionController cqcI = new SurveyAndQuestionController(std);
            cqcI.questionReference = tu.questionIds[0];
            cqcI.editQuestion();
            cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW THIS IS A NEW';
            cqcI.qChoices = '1\\n2\\n3\\3';
            cqcI.qRequired=true;
            cqcI.questionType='Single Select--Vertical';
            cqcI.saveAndNewQuestion();
            System.assertEquals(5, cqcI.getNewQuestionNum());
        }
    }

    @isTest
    private static void testsavesaveQuestion(){
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller std;
            SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);
            //test saving new question
            cqc.qQuestion = 'THIS IS A NEW QUESTION';
            cqc.qChoices = '1\\n2\\n3\\3';
            cqc.qRequired=true;
            cqc.questionType='Single Select--Vertical';
            cqc.controllerSavQuestion();
            System.assertEquals(5, cqc.getNewQuestionNum());
            //edit existing question
            SurveyAndQuestionController cqcI = new SurveyAndQuestionController(std);
            cqcI.questionReference = tu.questionIds[0];
            cqcI.editQuestion();
            cqcI.qQuestion = 'THIS IS A NEW QUESTION THAT IS EXTRA LONG SO THE NAME SHORTENING CALL WILL BE USED, THIS SHOULD BE LONG ENOUGH NOW';
            cqcI.qChoices = '1\\n2\\n3\\3';
            cqcI.qRequired=true;
            cqcI.questionType='Single Select--Vertical';
            cqcI.controllerSavQuestion();
            System.assertEquals(5, cqcI.getNewQuestionNum());
        }
    }

    @isTest
    //test constructor and calls within it
    private static void testPreviewQuestion(){
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller std;
            SurveyAndQuestionController cqc = new SurveyAndQuestionController(std);

            cqc.questionReference = tu.questionIds[0];
            cqc.editQuestion();
            cqc.previewQuestion();
            System.assertEquals(cqc.showRowQuestionPreview, 'True','The showRowQuestionPreview should be true for Single Select--Horizontal');

            cqc.questionReference = tu.questionIds[1];
            cqc.editQuestion();
            cqc.previewQuestion();
            System.assertEquals(cqc.showMultiSelectPreview, 'True','The showMultiSelectPreview should be true for Multi-Select--Vertical');

            cqc.questionReference = tu.questionIds[2];
            cqc.editQuestion();
            cqc.previewQuestion();
            System.assertEquals(cqc.showSingleSelectPreview, 'True','The showSingleSelectPreview should be true for Single Select--Vertical');

            cqc.questionReference = tu.questionIds[3];
            cqc.editQuestion();
            cqc.previewQuestion();
            System.assertEquals(cqc.showFreeTextPreview, 'True','The showFreeTextPreview should be true for Free Text');

            System.assert(cqc.previewQuestion()==null);
        }



    }


    @isTest
    private static void testUpdateSurveyName() {
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller stc;
            SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);
            vsc.surveyName = 'new name';
            vsc.updateSurveyName();
            Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:vsc.surveyId]; 
            system.assert(vsc.updateSurveyName() == null);
            system.assert(s.Name == 'new name');
        }

    }


    @isTest
    private static void testupdateSurveyThankYouAndLink() {
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller stc;
            SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);
            vsc.surveyThankYouText = 'new stuff';
            vsc.surveyThankYouURL = 'more new stff';
            vsc.updateSurveyThankYouAndLink();

            Survey__c s = [SELECT Name, Id, URL__c, thankYouText__c, thankYouLink__c FROM Survey__c WHERE Id =:vsc.surveyId];
            System.assertEquals(s.thankYouText__c,  vsc.surveyThankYouText, 'Thank you text should be: ' +
            vsc.surveyThankYouText + ' But was: ' + s.thankYouText__c);
            System.assertEquals(s.thankYouLink__c,  vsc.surveyThankYouURL, 'Thank you link should be: ' +
            vsc.surveyThankYouURL + ' But was: ' + s.thankYouLink__c);
            system.assert(vsc.updateSurveyThankYouAndLink()==null);
        }
    }

    //------------------------------------------------------------------------------//

    @isTest
    private static void testRefreshQuestionList() {
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller stc;
            SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);
            vsc.getAQuestion();

            // Retrieve questions for this survey
            List<Survey_Question__c> sq = new List<Survey_Question__c>();
            sq = [Select id, orderNumber__c from Survey_Question__c];

            // get question with orderNumber 1
            Survey_Question__c first = [Select id, orderNumber__c from Survey_Question__c Where orderNumber__c =: 1 and Survey__c =: tu.surveyId];
            System.assert(first.orderNumber__c == 1 );

            // Specify the new order
            vsc.newOrderW = vsc.allQuestions[2].id + ',' +
                    vsc.allQuestions[0].id + ',' +
                    vsc.allQuestions[1].id + ',' +
                    vsc.allQuestions[3].id + ',';

            vsc.updateOrderList();

            // Verify that the question with order 1 is not the same as the one retrieved previously
            Survey_Question__c second = [Select id, orderNumber__c from Survey_Question__c Where orderNumber__c =: 1 and Survey__c =: tu.surveyId];
            System.assert(second.id != first.id);

            // update the question list, and make sure it has been modified as well
            vsc.refreshQuestionList();
            System.assert(vsc.allQuestions[1].id != first.id);
        }

    }

//------------------------------------------------------------------------------//

    @isTest
    private static void testDeleteQuestion() {
        user u = [SELECT id from user where username like 'test@test.com.hasjsa78%' limit 1];
        System.runAs(u){
            SurveyTestingUtil tu = new SurveyTestingUtil();
            Apexpages.currentPage().getParameters().put('id',tu.surveyId);
            Apexpages.Standardcontroller stc;
            SurveyAndQuestionController vsc = new SurveyAndQuestionController(stc);


            // Get a question to delete
            Survey_Question__c sq = [Select id, orderNumber__c from Survey_Question__c Where orderNumber__c = 1 and Survey__c =: tu.surveyId];
            vsc.questionReference = sq.Id;
            vsc.deleteRefresh();

            Survey_Question__c sq2 = [Select id, orderNumber__c from Survey_Question__c Where orderNumber__c = 1 and Survey__c =: tu.surveyId];
            System.assert(sq.Id != sq2.Id);
        }


    }
//------------------------------------------------------------------------------//


/**/
}