public with sharing class CTS_IOT_Losant_Connected_App_Handler extends Auth.ConnectedAppPlugin{
      
    // Return additional attributes in the SAML response       
    public override Map<String,String> customAttributes(Id userId, Id connectedAppId, Map<String,String> formulaDefinedAttributes, Auth.InvocationContext context){  
        
        try{

            User user = [Select Id, Name, ProfileId, ContactId, AccountId, Account.Siebel_Ship_To_Id__c, Account.Bill_To_Account__c, LanguageLocaleKey, TimeZoneSidKey From User Where Id = :userId];
            Boolean isSuperUser = CTS_IOT_Data_Service_WithoutSharing.getIsSuperUser(userId);
            Contact contact = user.ContactId != null ? [Select Id, Name From Contact Where Id = :user.ContactId] : null;            
            CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();

            formulaDefinedAttributes.put('Persona', 'Standard');
            formulaDefinedAttributes.put('Language', user.LanguageLocaleKey);
            formulaDefinedAttributes.put('Timezone', user.TimeZoneSidKey);

            if (communitySettings != null){
                formulaDefinedAttributes.put('Timeout', communitySettings.Losant_Timeout_Minutes__c != null ? String.valueOf(communitySettings.Losant_Timeout_Minutes__c) : '120');
            }
            else{
                formulaDefinedAttributes.put('Timeout', '120');
            }

            if (user.AccountId != null){

                // Add the Siebel ID of all accounts the user has access to
                Set<String> sites = new Set<String>();

                if (String.isNotBlank(user.Account.Siebel_Ship_To_Id__c)){
                    sites.add(user.Account.Siebel_Ship_To_Id__c);
                }

                // Super user - additional accounts/sites based on bill to/ship to hierarchy
                if (isSuperUser){

                    for (Account a : [Select Id, Siebel_Ship_To_Id__c From Account Where Bill_To_Account__c = :user.Account.Bill_To_Account__c and Siebel_Ship_To_Id__c != null]){
                        sites.add(a.Siebel_Ship_To_Id__c);
                    }
                }
                // Standard user - additional accounts/sites based on account contact relationship object
                else if (contact != null){

                    for (AccountContactRelation acr : [Select Id, Account.Siebel_Ship_To_Id__c From AccountContactRelation Where ContactId = :contact.Id and Account.Siebel_Ship_To_Id__c != null]){
                        sites.add(acr.Account.Siebel_Ship_To_Id__c);
                    }
                }

                formulaDefinedAttributes.put('Sites', JSON.serialize(sites));
            }
        }
        catch(Exception e){
            system.debug('exception caught: ' + e);
        }
        
        return formulaDefinedAttributes;     
    } 
}