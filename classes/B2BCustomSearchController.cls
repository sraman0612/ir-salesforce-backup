public without sharing class B2BCustomSearchController {
    private static final String ModelsImageURL = B2B_Robuschi_Store_Setting__mdt.getInstance('Robuschi_Models_Logo_URL').value__c;

    public class searchResultsWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String nameDisplayed;
        @AuraEnabled public String modelName;
        @AuraEnabled public String image;
        @AuraEnabled public String modelsToFilter;
        @AuraEnabled public Boolean isSerialNumberSearch;
        @AuraEnabled public Boolean isLandingPageModel;
    }

    public class CustomException extends Exception{
        @TestVisible String message;
    }

    @AuraEnabled
    public static String getCategoryId(String webstoreId, String categoryName) {
        String categoryId;
        List<ProductCategory> productCategories = B2BLandingPageController.getCategoryId(webstoreId, categoryName);
        categoryId = productCategories?.size() > 0 ? productCategories[0].Id : null;

        return categoryId;
    }


    @AuraEnabled
    public static List<searchResultsWrapper> getRelevantAssets(String searchText) {
        try {
            List<searchResultsWrapper> resultsWrappers = new List<B2BCustomSearchController.searchResultsWrapper>();

            if (searchText.startsWithIgnoreCase('rb') && searchText.length() == 9 && checkSerialNumberPattern(searchText.trim())) {
                List<searchResultsWrapper> resultsFromSerialNumberSearch = searchSerialNumbers(searchText);
                if ( resultsFromSerialNumberSearch.size() > 0) {
                    resultsWrappers.addAll(resultsFromSerialNumberSearch);
                }
            }
            else {
                List<searchResultsWrapper> resultsFromModelSearch = searchModels(searchText);
                if (resultsFromModelSearch.size() > 0) {
                    resultsWrappers.addAll(resultsFromModelSearch);
                }
            }

            System.debug(JSON.serializePretty(resultsWrappers));
            return resultsWrappers;
        }
        catch (Exception e) {
            System.debug(e);
            throw e;
        }
    }

    private static Boolean checkSerialNumberPattern(String searchText) {
        //pattern is string starts with RB followed by 7 digits
        Pattern MyPattern = Pattern.compile('[Rr][Bb][0-9]{7}');
        Matcher MyMatcher = MyPattern.matcher(searchText);

        System.debug('Match?' + MyMatcher.matches());
        return MyMatcher.matches();
    }

    private static searchResultsWrapper createResultWrapper(String name, String nameDisplayed, String modelName, String image, String modelsToFilter, Boolean isSerialNumberSearch, Boolean isLandingPageModel) {
        searchResultsWrapper resultsWrapper = new searchResultsWrapper();
        resultsWrapper.name = name;
        resultsWrapper.nameDisplayed = nameDisplayed;
        resultsWrapper.image = image;
        resultsWrapper.isSerialNumberSearch = isSerialNumberSearch;
        resultsWrapper.modelsToFilter = modelsToFilter;
        resultsWrapper.modelName = modelName;
        resultsWrapper.isLandingPageModel = isLandingPageModel;
        return resultsWrapper;
    }

    public static HTTPResponse doCallout(String searchText) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint('callout:AssetSearch/' + searchText);

        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res;
    }

    @TestVisible
    private static List<searchResultsWrapper> searchSerialNumbers(String searchText) {
        List<searchResultsWrapper> resultsWrappers = new List<B2BCustomSearchController.searchResultsWrapper>();

        HTTPResponse resp = doCallout(searchText);
        String model;

        if(resp.getStatusCode() == 200 ) { //response
            Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(resp.getBody());
            model = (String)m.get('modelName');
        }
        if (resp.getStatusCode() == 404) {
            CustomException e = new CustomException();
            e.message = 'No asset found';
            throw e;
        }
        if (resp.getStatusCode() == 401 || resp.getStatusCode() == 500) {
            CustomException e = new CustomException();
            e.message = 'Unauthorized, Token not valid or Unexpected error occurred';
            throw e;
        }

        if(String.isNotEmpty(model)) {
            List<Models_Mapper__c> modelsMappers = [SELECT Name, Robox_Lobe_Model__c FROM Models_Mapper__c WHERE Name = :model];

            if (modelsMappers.size() > 0 && String.isNotEmpty(modelsMappers[0].Robox_Lobe_Model__c)) {
                searchResultsWrapper resultsWrapper = createResultWrapper(searchText, modelsMappers[0].Robox_Lobe_Model__c, modelsMappers[0].Robox_Lobe_Model__c, ModelsImageURL, modelsMappers[0].Robox_Lobe_Model__c, true, true);
                resultsWrappers.add(resultsWrapper);
            }
        }
        return resultsWrappers;
    }

    private static List<searchResultsWrapper> searchModels(String searchText) {
        List<searchResultsWrapper> resultsWrappers = new List<B2BCustomSearchController.searchResultsWrapper>();
        List<Product2> products = new List<Product2>();
        String query = 'SELECT Name, StockKeepingUnit, Models__c, Landing_Page_Model__c FROM Product2 WHERE RecordType.DeveloperName = \'Product_ROB_B2B\' ';
        products = Database.query(query);

        List<String> models = new List<String>();
        List<String> landingPageModels = new List<String>();
        for (Product2 product : products) {
            if (String.isNotEmpty(product.Models__c)) {
                models.addAll(product.Models__c.split(';'));
            }
            if (String.isNotEmpty(product.Landing_Page_Model__c)) {
                landingPageModels.addAll(product.Landing_Page_Model__c.split(';'));
            }
        }

        Set<String> modelsSet = new Set<String>();
        modelsSet.addAll(models);
        resultsWrappers.addAll(createModelsResultsWrappers(searchText, modelsSet, false));

        Set<String> landingPageModelsSet = new Set<String>();
        landingPageModelsSet.addAll(landingPageModels);
        resultsWrappers.addAll(createModelsResultsWrappers(searchText, landingPageModelsSet, true));
        return resultsWrappers;
    }

    private static List<searchResultsWrapper> createModelsResultsWrappers(String searchText, Set<String> models, Boolean isLandingPageModel) {
        List<searchResultsWrapper> resultsWrappers = new List<B2BCustomSearchController.searchResultsWrapper>();
        for (String model : models) {
            if (String.isNotEmpty(model) && model.containsIgnoreCase(searchText)) {
                searchResultsWrapper resultsWrapper = createResultWrapper(model, model, model, ModelsImageURL, model, false, isLandingPageModel);
                resultsWrappers.add(resultsWrapper);
            }
        }
        return resultsWrappers;
    }
}