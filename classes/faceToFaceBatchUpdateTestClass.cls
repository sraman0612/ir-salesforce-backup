@isTest
public class faceToFaceBatchUpdateTestClass {
    @testSetup public static void testdata(){
        
        list<Account> lstAccs = new list<Account>();
        list<Task> lstTask = new list<Task>();
        
        
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        
        /*Division__c divn = new Division__c();
        divn.Name = 'Test Division';
        divn.Division_Type__c = 'Customer Center';
        divn.EBS_System__c = 'Oracle 11i';     
        insert divn; */ 
        
        for (Integer i=0;i<5;i++) {
            Account a = new Account();
            a.Name = 'Test F2F Batch' +i;
            a.BillingCity = 'srs_!city';
            a.BillingCountry = 'USA';
            a.BillingPostalCode = '674564569';
            a.BillingState = 'CA';
            a.BillingStreet = '12, street1678';
            //a.Siebel_ID__c = '123456';
            a.ShippingCity = 'city1';
            a.ShippingCountry = 'United States';
            a.ShippingState = 'CA';
            a.ShippingStreet = '13, street2';
            a.ShippingPostalCode = '123';  
            a.CTS_Global_Shipping_Address_1__c = '13';
            a.CTS_Global_Shipping_Address_2__c = 'street2';            
            a.County__c = 'testCounty';
            a.RecordTypeId = AirNAAcctRT_ID;
            //a.Account_Division__c = divn.Id;
            lstAccs.add(a);
        }
        insert lstAccs;
        system.debug('Account list is ' +lstAccs);
        User u = [Select id from User where Profile.name = 'System Administrator' Limit 1];
        integer i = 0;
        for(Account a : lstAccs){
            Task tsk = new Task();
            tsk.Subject = 'Task' +i;
            tsk.Status = 'Completed';
            tsk.Priority = 'Normal';
            tsk.Type = 'Face To Face Call';
            tsk.WhatId = a.id ;
            tsk.OwnerId = u.id;
            tsk.ActivityDate = system.today();
            tsk.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CTS_EU_Task').getRecordTypeId();
            i++;         
            lstTask.add(tsk);
        }
        insert lstTask;
        
        
        
    } 
    
    
    public static testmethod void BatchTest(){
        /* test.startTest();
faceToFaceBatchUpdate F2F = new faceToFaceBatchUpdate();
Database.executeBatch(F2F);
test.stopTest();*/
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        test.startTest();
        list<Event> lstEvent = new list<Event>();
        integer j = 0;
        for(Account a : [SELECT Id FROM Account where RecordTypeId =:AirNAAcctRT_ID]){
            Event eve = new Event();
            DateTime dt = DateTime.now();
            eve.Subject = 'Event' +j;
            eve.Type = 'Phone Call';
            eve.WhatId = a.id ;
            //eve.OwnerId = '005j000000ByTx4AAF';
            eve.StartDateTime = dt+j;
            eve.EndDateTime = dt+j;
            eve.Activity_Type__c = 'Face To Face Call';
            
            j++;        
            lstEvent.add(eve);
        }
        insert lstEvent;
        SchedulerApex  sh1 = new SchedulerApex ();
        //String sch = '0 0 23 * * ?';
        sh1.execute(null);
        test.stopTest();
    }
    
    
}