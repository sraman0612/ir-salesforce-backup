@isTest
    public class AccountQuaueableUpdateFlagTest {
        
        static testMethod void testAccountQueueableUpdateFlag() {

            User user  = [Select id,Name,Business__c,Channel__c,UserRole.Name, profile.name FROM USER WHERE id=:UserInfo.getUserId()];
            user.Business__c = 'Low Pressure';
            update user;

            Id GD_Parent_RT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();

            // Create a list of accounts to pass to the queueable class
            List<Account> accList = new List<Account>();
            for (Integer i = 0; i < 10; i++) {
                Account acc = new Account(Name = 'Test Account ' + i);
                acc.RecordTypeId = GD_Parent_RT_ID;
                acc.wasconverted__c = true;
                accList.add(acc);
            }
            // Insert the accounts to the database
            insert accList;
            
            BusinessHours bhs=[select id from BusinessHours where name = 'IR Comp Business Hours - EU + Africa'];
            System.debug(bhs.id);
            
            Contact testContact = new Contact();
            testContact.FirstName = 'Test Contact1';
            testContact.LastName = 'Test Contact';
            testContact.Business__c = 'Low Pressure';
            testContact.Lead_Convert_Bypass__c = 'false';
            testContact.AccountId = accList[0].id;
            testContact.email='testuser@test.com';
            testContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
            insert testContact;
            
            
            Lead l3 = new Lead();
            l3.firstname = 'George';
            l3.lastname = 'Acker';
            l3.phone = '1234567890';
            l3.City = 'city';
            l3.Country = 'US';
            l3.PostalCode = '12345';
            l3.State = 'AK';
            l3.Street = '12, street1';
            l3.company = 'ABC Corp';
            l3.county__c = 'test county';
            l3.Business__c = 'Low Pressure';
            l3.Business_Hours__c = bhs.id;
            l3.recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'New_Standard_Lead' LIMIT 1].Id;
            insert l3;
            
            Opportunity o = new Opportunity();
            o.name = 'Test Opportunity';
            o.CurrencyIsoCode = 'USD';
            o.stagename = 'Stage 1. Qualify';
            o.amount = 1000000;
            o.closedate = system.today();
            o.Business__c = 'Low Pressure';
            o.Framework_Flag__c = true;
            o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
            o.AccountId = accList[0].Id;
            insert o;
            
            
            Database.LeadConvert lc = new database.LeadConvert();  
            lc.setLeadId( l3.Id );
            lc.setDoNotCreateOpportunity(false);
            lc.setConvertedStatus('Converted');
            lc.setAccountId(accList[0].Id);
            lc.setContactId(testContact.Id);
            lc.setOpportunityId(o.id);
            Database.LeadConvertResult lcr = Database.convertLead(lc);  
            
            // Verify the accounts wasconverted__c field is true
            for(Account acc : accList) {
                System.assertEquals(true, acc.wasconverted__c);
            }
            
            // Instantiate the queueable class and add it to the queue
            System.enqueueJob(new AccountQuaueableUpdateFlag(accList));
            
        }
    }