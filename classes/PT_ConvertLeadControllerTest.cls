@isTest
public class PT_ConvertLeadControllerTest {
    
    // Creation of Test Data - version 1.1
    @testsetup static void setup()
    {
        Id ptAcctRTId = [Select Id, DeveloperName from RecordType where DeveloperName = 'PT_powertools' and SobjectType='Account' limit 1].Id;
        Account a = new Account();
        a.RecordTypeId = ptAcctRTId;
        a.BillingCountry='Belgium';
        a.Name = 'EYE_R_SEE_OH';
        a.Type = 'Customer';
        a.PT_Status__c = 'New';
        a.PT_IR_Territory__c = 'North';
        a.PT_IR_Region__c = 'EMEA';
        insert a;
        Id ptLeadRTId = [SELECT Id FROM RecordType WHERE sobjecttype='Lead' and Developername='PT_Lead'].Id;
        Lead testLead = new Lead(RecordTypeId=ptLeadRTId, FirstName='PTLead', LastName='Test', Company='EYE_R_SEE_OH', Country='Belgium',LeadSource='Other', Email='test@test.com');
        insert testLead;
        Task t = new Task(WhoId=testLead.Id,subject='testpttask');
        insert t;
        Event e = new Event(WhoId=testLead.Id,Subject= 'testptevent', STARTDATETIME=System.today(),ENDDATETIME=System.today());
        insert e;
    }
    
    static testmethod void Test1()
    {
        Lead testLead = [select Id, RecordTypeId, Name, FirstName, LastName, Company, Country, LeadSource, Email,PT_Lead_ID__c, UTM_Campaign__c, PT_Campaign__c from lead where FirstName = 'PTLead'];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/PT_LeadConvert?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        PT_ConvertLeadController controller = new PT_ConvertLeadController(con);
        Schema.DescribeFieldResult fieldResult = PT_Parent_Opportunity__c.Year__c .getDescribe();       
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){controller.oppInput.Year__c=ple.getValue();break;}
        controller.oppInput.Type__c='5.Repeated Business';
        controller.oppInput.Product_Group__c='Assembly';
        controller.oppInput.Name='OPPINPUTTEST';
        controller.fireContactInsertEception = TRUE;
        PageReference prtest1 = controller.convert();
        Test.stopTest();
    }
    
    static testmethod void Test2()
    {
		Lead testLead = [select Id, RecordTypeId, Name, FirstName, LastName, Company, Country, LeadSource, Email,PT_Lead_ID__c, UTM_Campaign__c, PT_Campaign__c from lead where FirstName = 'PTLead'];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/PT_LeadConvert?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        PT_ConvertLeadController controller = new PT_ConvertLeadController(con);
        Schema.DescribeFieldResult fieldResult = PT_Parent_Opportunity__c.Year__c .getDescribe();       
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){controller.oppInput.Year__c=ple.getValue();break;}
        controller.oppInput.Type__c='5.Repeated Business';
        controller.oppInput.Product_Group__c='Assembly';
        controller.oppInput.Name='OPPINPUTTEST';
        controller.fireOppInputInsertEception = TRUE;
        PageReference prtest2 = controller.convert();
        Test.stopTest();
    } 

    private static testmethod void Test3()
    {
		Lead testLead = [select Id, RecordTypeId, Name, FirstName, LastName, Company, Country, LeadSource, Email,PT_Lead_ID__c, UTM_Campaign__c, PT_Campaign__c from lead where FirstName = 'PTLead'];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/PT_LeadConvert?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        PT_ConvertLeadController controller = new PT_ConvertLeadController(con);
        Schema.DescribeFieldResult fieldResult = PT_Parent_Opportunity__c.Year__c .getDescribe();       
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){controller.oppInput.Year__c=ple.getValue();break;}
        controller.oppInput.Type__c='5.Repeated Business';
        controller.oppInput.Product_Group__c='Assembly';
        controller.oppInput.Name='OPPINPUTTEST';
        controller.fireActivityInsertEception = TRUE;
        PageReference prtest3 = controller.convert();
        Test.stopTest();
    }  

    private static testmethod void Test4()
    {
		Lead testLead = [select Id, RecordTypeId, Name, FirstName, LastName, Company, Country, LeadSource, Email,PT_Lead_ID__c, UTM_Campaign__c, PT_Campaign__c from lead where FirstName = 'PTLead'];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/PT_LeadConvert?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        PT_ConvertLeadController controller = new PT_ConvertLeadController(con);
        Schema.DescribeFieldResult fieldResult = PT_Parent_Opportunity__c.Year__c .getDescribe();       
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){controller.oppInput.Year__c=ple.getValue();break;}
        controller.oppInput.Type__c='5.Repeated Business';
        controller.oppInput.Product_Group__c='Assembly';
        controller.oppInput.Name='OPPINPUTTEST';
        controller.fireConvertEception = TRUE;
        PageReference prtest4 = controller.convert();
        Test.stopTest();
    }  
    
    private static testmethod void Test5()
    {
        Lead testLead = [select Id, RecordTypeId, Name, FirstName, LastName, Company, Country, LeadSource, Email,PT_Lead_ID__c, UTM_Campaign__c, PT_Campaign__c from lead where FirstName = 'PTLead'];
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/PT_LeadConvert?Id='+testLead.Id+'');
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController con = new ApexPages.StandardController(testLead);
        PT_ConvertLeadController controller = new PT_ConvertLeadController(con);
        Schema.DescribeFieldResult fieldResult = PT_Parent_Opportunity__c.Year__c .getDescribe();       
        for(Schema.PicklistEntry ple : fieldResult.getPicklistValues()){controller.oppInput.Year__c=ple.getValue();break;}
        controller.oppInput.Type__c='5.Repeated Business';
        controller.oppInput.Product_Group__c='Assembly';
        controller.oppInput.Name='OPPINPUTTEST';
        PageReference prtest5 = controller.convert();
        Test.stopTest();
    }  
	
}