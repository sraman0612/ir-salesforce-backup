@isTest
private class OpportunityProductSelector_Test {
      
    @testSetup
    private static void setupData() {
      //  Id RecordTypeNAId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'TK_Transport_NA' AND SobjectType = 'Product2'].Id;
       Id RecordTypeEMEAId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_TechDirect_Product' AND  SobjectType = 'Product2'].Id; 
        
        Id RecordTypeNAAccId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Account'].Id;
        Id RecordTypeNAOppId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        
        Pricebook2 NAPricebook = new Pricebook2(
    
            Name = 'TK NA Pricebook',
            CurrencyISOCode = 'USD',
            IsActive = true
            
        );
        insert NAPricebook;
        Pricebook2 EMEAPricebook = new Pricebook2(
    
            Name = 'TK EMEA Pricebook',
            CurrencyISOCode = 'EUR',
            IsActive = true
            
        );
        insert EMEAPricebook;
        
       
        
        List<Product2> prodList = new List<Product2>();
        
       /* Product2 prodNA1 = new Product2 ( Name ='V-220-10',
                                        // TK_TKProductFamilyMULTI__c = 'TRUCK SMALL',
                                         RecordTypeId = RecordTypeNAId,
                                         Model__c = 'V-220-10',
                                        isActive = TRUE);
        Product2 prodBaseNA1 = new Product2 ( Name ='TestModeBaseNAl1',
                                           //  tk_tkproductfamilymulti__c = 'TRUCK SMALL',
                                             RecordTypeId = RecordTypeNAId,
                                          //   TK_Base_Product__c = prodNA1.ID,
                                             Type__c='Base Product',
                                         	Model__c = 'TestModeBaseNAl1',
                                        isActive = TRUE);
        Product2 prodBaseNA2 = new Product2 ( Name ='TestModeDealerNAl1',
                                             RecordTypeId = RecordTypeNAId,
                                             Type__c = 'Dealer Option',
                                        isActive = TRUE);
        prodList.add(prodNA1);
        prodList.add(prodBaseNA1);
        prodList.add(prodBaseNA2);
        */
        Product2 prodEMEA1 = new Product2 ( Name ='SLXi 100 30',
                                          // TK_TKProductFamilyMULTI__c = 'Trailer', 
                                           RecordTypeId = RecordTypeEMEAId,
                                        isActive = TRUE);
        Product2 prodBaseEMEA1 = new Product2 ( Name ='TestModeBaseEMEAl1',
                                             //  tk_tkproductfamilymulti__c = 'Trailer',
                                               RecordTypeId = RecordTypeEMEAId,
                                           //    TK_Base_Product__c = prodEMEA1.ID,
                                               Type__c='Base Product',
                                        isActive = TRUE);
        
        prodList.add(prodEMEA1);
        prodList.add(prodBaseEMEA1);
        
        insert prodList;
        List<PriceBookEntry> pbeList = new List<PriceBookEntry>();
        for(Product2 prod: prodList){
          /*  if(prod.RecordTypeId == RecordTypeNAId){
                PriceBookEntry pbes = new PriceBookEntry(
                    Product2Id = prod.Id,
                    Pricebook2Id = Test.getStandardPricebookId(),
                    CurrencyISOCode = 'USD',
                    UnitPrice = 0
                );
                PriceBookEntry pbe = new PriceBookEntry(
                    Product2Id = prod.Id,
                    Pricebook2Id = NAPricebook.Id,
                    CurrencyISOCode = 'USD',
                    isActive = TRUE,
                    UnitPrice = 1
                );
                pbeList.add(pbes);
                pbeList.add(pbe);
            }*/
            if(prod.RecordTypeId == RecordTypeEMEAId){
                
                PriceBookEntry pbes = new PriceBookEntry(
                    Product2Id = prod.Id,
                   Pricebook2Id = Test.getStandardPricebookId(),
                 // Pricebook2Id = EMEAPricebook.Id,
                    CurrencyISOCode = 'EUR',
                    UnitPrice = 0
                );
                PriceBookEntry pbe = new PriceBookEntry(
                    Product2Id = prod.Id,
                    Pricebook2Id = EMEAPricebook.Id,
                    CurrencyISOCode = 'EUR',
                    isActive = TRUE,
                    UnitPrice = 1
                );
                pbeList.add(pbes);
                pbeList.add(pbe);
            }
        }
		insert pbeList;
        
        Account account1 = new Account(Name = 'Test Dealer', 
                                       //TK_Primary_Fleet_Type__c ='Distirbution-Grocery', 
                                       //TK_Competitive_Position__c ='100% - TK Loyal', 
                                       RecordTypeId = RecordTypeNAAccId);
        insert account1;
        
        Opportunity opp = new Opportunity(AccountId = account1.Id, 
                                          StageName = 'Stage 1. Qualify',
                                          Name = 'Test1',
                                          CloseDate = System.Today(),
                                          //TK_Planned_Contact_Date__c  =  System.Today(),
                                          //TK_Product_of_Interest__c = 'Trailer',
                                          //TK_ProductFamily__c = 'TRUCK SMALL',
                                          //TKTransport_Model__c = 'V-220-10',
                                          RecordTypeId = RecordTypeNAOppId);
        insert opp;
        /*

        
        Contact contact = TestUtil.getContact();
        contact.AccountId = account.Id;
        insert contact;
        
        Case caseRecord = TestUtil.getCase(account.Id, null, null);
        insert caseRecord; */
    }
    private static testMethod void testNAOpp() {
                
        Id RecordTypeNAAccId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Account'].Id;
        Id RecordTypeNAOppId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        Account account1 = new Account(Name = 'Test Dealer', 
                                       //TK_Primary_Fleet_Type__c ='Distirbution-Grocery', 
                                       //TK_Competitive_Position__c ='100% - TK Loyal', 
                                       RecordTypeId = RecordTypeNAAccId);
        insert account1;
        
        Opportunity opp = new Opportunity(AccountId = account1.Id, 
                                          StageName = 'Stage 1. Qualify',
                                          Name = 'Test1',
                                          CloseDate = System.Today(),
                                          //TK_Planned_Contact_Date__c  =  System.Today(),
                                          //TK_Product_of_Interest__c = 'Trailer',
                                          //TK_ProductFamily__c = 'TRUCK SMALL',
                                          //TKTransport_Model__c = 'V-220-10',
                                          RecordTypeId = RecordTypeNAOppId);
        
        insert opp;
        
         
      /*  Id RecordTypeNAprodId = [SELECT ID 
                                 FROM RecordType 
                                 WHERE DeveloperName  = 'TK_Transport_NA' 
                                 AND SobjectType = 'Product2'].Id;
        
        Product2 baseprod = [SELECT ID, Name, TK_TKProductFamilyMULTI__c, Model__c  
                             FROM Product2 
                             WHERE Name = 'V-220-10' 
                             AND RecordTypeId = : RecordTypeNAprodId LIMIT 1];
        
        Product2 childprod = [SELECT ID, Name, TK_TKProductFamilyMULTI__c, Model__c  
                             FROM Product2 
                             WHERE Name = 'TestModeBaseNAl1' 
                             AND RecordTypeId = : RecordTypeNAprodId LIMIT 1];
        */
        //OpportunityProductSelector.getBaseModels(opp.Id);
       	Opportunity oppty = OpportunityProductSelector.getOppDetails(opp.Id);
      //  List<Product2> prodList = OpportunityProductSelector.getProductByOppModel(opp.Id);
      //  List<Product2> prodList1 = OpportunityProductSelector.getProductByModel(baseprod.Id);
      //  Product2 cardProd = OpportunityProductSelector.getCardProduct(baseprod.Id, opp.Id);
        boolean priceC = OpportunityProductSelector.getPriceCheck(opp.Id);
       // String check = OpportunityProductSelector.addOppLineItems(prodList1,opp.Id,baseprod);
      //  List<Product2> prodList2 = OpportunityProductSelector.getProductByModel(childprod.Id);
     //   String check1 = OpportunityProductSelector.addOppLineItems(prodList2,opp.Id,baseprod);
    }
    
    
    private static testMethod void testEMEAOpp() {
                
        Id RecordTypeNAAccId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Account'].Id;
        Id RecordTypeEMEAOppId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        Account account1 = new Account(Name = 'Test Dealer', 
                                       //TK_Primary_Fleet_Type__c ='Distirbution-Grocery', 
                                       //TK_Competitive_Position__c ='100% - TK Loyal', 
                                       RecordTypeId = RecordTypeNAAccId);
        insert account1;
        
        Opportunity opp = new Opportunity(AccountId = account1.Id, 
                                          StageName = 'Stage 1. Qualify',
                                          Name = 'Test1',
                                          CloseDate = System.Today(),
                                          CurrencyISOCode = 'EUR',
                                          //TK_Planned_Contact_Date__c  =  System.Today(),
                                          //TK_Product_of_Interest__c = 'Trailer',
                                          RecordTypeId = RecordTypeEMEAOppId);
        
        insert opp;
        
     /*   TK_Transport_PDR__c  pdr = new TK_Transport_PDR__c (Opportunity__c = opp.ID,
                                                           Model__c ='SLXi 100 30',
                                                           Product_Family__c = 'Trailer');*/
        
       // insert pdr;
        Id RecordTypeEMEAprodId = [SELECT ID 
                                 FROM RecordType 
                                 WHERE DeveloperName  = 'CTS_TechDirect_Product' 
                                 AND SobjectType = 'Product2'].Id;
        
        Product2 baseprod = [SELECT ID, Name,  Model__c  
                             FROM Product2 
                             WHERE Name = 'SLXi 100 30' 
                             AND RecordTypeId = : RecordTypeEMEAprodId LIMIT 1];

        Product2 childprod = [SELECT ID, Name, Model__c  
                             FROM Product2 
                             WHERE Name = 'TestModeBaseEMEAl1' 
                             AND RecordTypeId = : RecordTypeEMEAprodId LIMIT 1];
        List<Product2> prodList = new List<Product2>();
        prodList.add(baseprod);
        Pricebook2 EMEAPricebook = new Pricebook2(
    
            Name = 'TK EMEA Pricebook',
            CurrencyISOCode = 'EUR',
            IsActive = true
            
        );
        insert EMEAPricebook;
        PriceBookEntry pbe = new PriceBookEntry(
                    Product2Id = childprod.Id,
                    Pricebook2Id = EMEAPricebook.Id,
                    CurrencyISOCode = 'EUR',
                    isActive = TRUE,
                    UnitPrice = 1);
        insert pbe;
            
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.Product2Id = childprod.id;
        oppLineItem.OpportunityId = opp.id;
        oppLineItem.PricebookEntryId = pbe.id;
        oppLineItem.Quantity = 2;
        oppLineItem.TotalPrice = 3467;
        insert oppLineItem;

      //  OpportunityProductSelector.getBaseModels(pdr.Id);
       	Opportunity oppty = OpportunityProductSelector.getOppDetails(opp.Id);
      //  List<Product2> prodList = OpportunityProductSelector.getProductByOppModel(pdr.Id);
      //  List<Product2> prodList1 = OpportunityProductSelector.getProductByModel(baseprod.Id);
        Product2 cardProd = OpportunityProductSelector.getCardProduct(baseprod.Id, opp.ID);
        boolean priceC = OpportunityProductSelector.getPriceCheck(opp.Id);
     //   String check = OpportunityProductSelector.addOppLineItems(prodList1,pdr.Id,baseprod);
       // List<Product2> prodList2 = OpportunityProductSelector.getProductByModel(childprod.Id);
        String check1 = OpportunityProductSelector.addOppLineItems(prodList,opp.Id,baseprod);
    }
    
    
  /*  private static testMethod void testUpdateNAOpp() {
                
        
        Id RecordTypeNAAccId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Account'].Id;
        Id RecordTypeNAOppId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Opportunity'].Id;
        Account account1 = new Account(Name = 'Test Dealer', 
                                       //TK_Primary_Fleet_Type__c ='Distirbution-Grocery', 
                                       //TK_Competitive_Position__c ='100% - TK Loyal', 
                                       RecordTypeId = RecordTypeNAAccId);
        insert account1;
        Id pricebookNAId = [SELECT ID FROM Pricebook2 WHERE Name = 'TK NA Pricebook' LIMIT 1].Id;
        Opportunity opp = new Opportunity(AccountId = account1.Id, 
                                          StageName = 'Stage 1. Qualify',
                                          Name = 'Test1',
                                          CloseDate = System.Today(),
                                          //TK_Planned_Contact_Date__c  =  System.Today(),
                                          //TK_Product_of_Interest__c = 'Trailer',
                                          //TK_ProductFamily__c = 'TRUCK SMALL',
                                          //TKTransport_Model__c = 'V-220-10',
                                          Pricebook2Id = pricebookNAId,
                                          RecordTypeId = RecordTypeNAOppId);
        
        insert opp;
        
        Id RecordTypeNAprodId = [SELECT ID 
                                 FROM RecordType 
                                 WHERE DeveloperName  = 'TK_Transport_NA' 
                                 AND SobjectType = 'Product2'].Id;
        
        Product2 baseprod = [SELECT ID, Name, TK_TKProductFamilyMULTI__c, Model__c  
                             FROM Product2 
                             WHERE Name = 'V-220-10' 
                             AND RecordTypeId = : RecordTypeNAprodId LIMIT 1];
        
        Product2 childprod = [SELECT ID, Name, TK_TKProductFamilyMULTI__c, Model__c  
                             FROM Product2 
                             WHERE Name = 'TestModeBaseNAl1' 
                             AND RecordTypeId = : RecordTypeNAprodId LIMIT 1];
        
        List<OpportunityLineItem> insertLineList 
                        = new List<OpportunityLineItem>();
        for(PricebookEntry pbe: [SELECT Id, Product2Id, Pricebook2Id,
                                 UnitPrice, Product2.Name,  PRODUCT2.Type__c, 
                                 PRODUCT2.TK_Tab_Category__c,
                                 Product2.TK_TKProductFamilyMULTI__c,
                                 Product2.Model__c
                                 FROM PriceBookEntry 
                                 WHERE Product2Id =: childprod.Id
                                 AND CurrencyIsoCode = 'USD'
                                 AND Pricebook2.Name != 'Standard Price Book']){
       		OpportunityLineItem newLine = new OpportunityLineItem();
            newLine.Product2Id = pbe.Product2Id;
            newLine.PricebookEntryId = pbe.Id;
            newLine.Quantity = 1;
            newLine.UnitPrice = pbe.UnitPrice;
            newLine.OpportunityId = opp.Id;
            insertLineList.add(newLine);
       }
       insert insertLineList;
       //List<Product2> prodList1 = OpportunityProductSelector.getProductByModel(baseprod.Id);
       //String check = OpportunityProductSelector.addOppLineItems(prodList1,opp.Id,baseprod);
    }*/
    
    private static testMethod void testUpdateEMEAOpp() {
                
        Id RecordTypeNAAccId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU' AND  SobjectType = 'Account'].Id;
        Id RecordTypeEMEAOppId = [SELECT ID FROM RecordType WHERE DeveloperName  = 'CTS_EU_Indirect' AND  SobjectType = 'Opportunity'].Id;
        Account account1 = new Account(Name = 'Test Dealer', 
                                       //TK_Primary_Fleet_Type__c ='Distirbution-Grocery', 
                                       //TK_Competitive_Position__c ='100% - TK Loyal', 
                                       RecordTypeId = RecordTypeNAAccId);
        insert account1;
        
        Opportunity opp = new Opportunity(AccountId = account1.Id, 
                                          StageName = 'Stage 1. Qualify',
                                          Name = 'Test1',
                                          CloseDate = System.Today(),
                                          //TK_Planned_Contact_Date__c  =  System.Today(),
                                          //TK_Product_of_Interest__c = 'Trailer',
                                          RecordTypeId = RecordTypeEMEAOppId);
        
        insert opp;
        
                
      /*  TK_Transport_PDR__c  pdr = new TK_Transport_PDR__c (Opportunity__c = opp.ID,
                                                           Model__c ='SLXi 100 30',
                                                           Product_Family__c = 'Trailer');
        */
       // insert pdr;
        
        Id RecordTypeEMEAprodId = [SELECT ID 
                                 FROM RecordType 
                                 WHERE DeveloperName  = 'CTS_TechDirect_Product' 
                                 AND SobjectType = 'Product2'].Id;
        
        Product2 baseprod = [SELECT ID, Name, Model__c  
                             FROM Product2 
                             WHERE Name = 'SLXi 100 30' 
                             AND RecordTypeId = : RecordTypeEMEAprodId LIMIT 1];
        
        Product2 childprod = [SELECT ID, Name,  Model__c  
                             FROM Product2 
                             WHERE Name = 'TestModeBaseEMEAl1' 
                             AND RecordTypeId = : RecordTypeEMEAprodId LIMIT 1];
      /*  List<TK_Transport_PDR_Line_Item__c> insertPDRLine 
                        = new List<TK_Transport_PDR_Line_Item__c>();*/
        for(PricebookEntry pbe: [SELECT Id, Product2Id, Pricebook2Id,
                                 UnitPrice, Product2.Name,  PRODUCT2.Type__c, 
                                // PRODUCT2.TK_Tab_Category__c,
                                 //Product2.TK_TKProductFamilyMULTI__c,
                                 Product2.Model__c
                                 FROM PriceBookEntry 
                                 WHERE Product2Id =: childprod.Id
                                 AND CurrencyIsoCode = 'EUR'
                                 AND Pricebook2.Name != 'Standard Price Book']){
       	/*	TK_Transport_PDR_Line_Item__c newPDRLine = new TK_Transport_PDR_Line_Item__c();
            newPDRLine.Product__c = pbe.Product2Id;
            newPDRLine.Price_Book__c = pbe.Pricebook2Id;
            newPDRLine.Quantity__c = 1;
            newPDRLine.List_Price__c = pbe.UnitPrice;
            newPDRLine.PDR__c = pdr.Id;
            insertPDRLine.add(newPDRLine);*/
       }
       //insert insertPDRLine;
       //List<Product2> prodList1 = OpportunityProductSelector.getProductByModel(baseprod.Id);
       //String check = OpportunityProductSelector.addOppLineItems(prodList1,pdr.Id,baseprod);
    }
}