@isTest
public class CTS_TestDataFactory {
    //Method to create LATAM Accounts
    public static List<Account> createLatamAccounts(Integer numAccts) {
        List<Account> accts = new List<Account>();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();//IR Comp EU
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            a.RecordTypeId = RecordTypeIdAccount;
            a.ShippingCountry = 'Brazil';
            a.ShippingCity = 'ARACATUBA';
            a.ShippingStreet = 'RUA ANTONIO PEDRUCCI, 293';
            a.ShippingState = 'SP';
            a.ShippingPostalCode = '16072610';
            a.CurrencyIsoCode = 'USD';
            a.Type = 'Prospect';
            a.CTS_Unique_Tax_ID__c  = '21.316.324/0001-03';
            
            accts.add(a);
        }
        insert accts;
        return accts;
    }
    //Method to create IR Comp TechDirect Accounts
    public static List<Account> createNAAccounts(Integer numAccts) {
        List<Account> accts = new List<Account>();
        Id RecordTypeIdAccount = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId();
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i);
            a.RecordTypeId = RecordTypeIdAccount;
            a.ShippingCountry = 'USA';
            a.ShippingCity = 'Anaheim';
            a.ShippingStreet = '424 S. Atchison Street';
            a.ShippingState = 'CA';
            a.ShippingPostalCode = '92805';
            a.CurrencyIsoCode = 'USD';
            a.Type = 'Prospect';
            a.County__c = 'ORANGE';
            a.CTS_Global_Shipping_Address_1__c = '13';
            a.CTS_Global_Shipping_Address_2__c = 'street2';            
            accts.add(a);
        }
        insert accts;
        return accts;
    }
    //commented by CG as part of descoped object
    //Method to create Customer Satisfaction 
    /*public static List<Customer_Satisfaction_Results__c> createCSI(Integer numCSI,String accountId,String csiScore){
        List<Customer_Satisfaction_Results__c> csiList = new List<Customer_Satisfaction_Results__c>();
        for(Integer i=0;i<numCSI;i++) {
            Customer_Satisfaction_Results__c csiObj = new Customer_Satisfaction_Results__c();
            csiObj.CTS_Promoted_Score__c = csiScore;
            csiObj.CTS_Committed_Score__c = csiScore;
            csiObj.CTS_OSAT_Satisfaction__c = csiScore;
            csiObj.Survey_Comment__c = 'Test';
            csiObj.Response_Date__c = system.today()+i;
            csiObj.Account__c = accountId;
            csiList.add(csiObj);
        }
        insert csiList;
        return csiList;
    }*/
    public static List<Opportunity> createLatamOppoty(Integer numOppty,Account acc){
        List<Opportunity> opptys = new List<Opportunity>();
        Id RecordTypeIdOpportunity = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp EU').getRecordTypeId();
        for(Integer i=0;i<numOppty;i++) {
            Opportunity opp = new Opportunity(Name='TestOpportunity' + i);
            opp.AccountId = acc.Id;
            opp.CurrencyIsoCode = 'USD';
            opp.Type = 'Budgetary';
            opp.CloseDate = System.today();
            opp.StageName = 'Stage 1. Qualify';
            opp.Confidence__c = '10';
            opptys.add(opp);
        }
        insert opptys;
        return opptys;
    }
    //commented by CG as part of descoped object
    /*public static List<CTS_Centac_Parts_Quote_ReRate__c> createCentacPartQuote(Integer numCPQ,Opportunity oppty){
        List<CTS_Centac_Parts_Quote_ReRate__c>centacPartList = new List<CTS_Centac_Parts_Quote_ReRate__c>();
        for(Integer i=0;i<numCPQ;i++){
            CTS_Centac_Parts_Quote_ReRate__c CPQObj = new CTS_Centac_Parts_Quote_ReRate__c(Name='Test-'+i);
            CPQObj.Date_Requested__c = System.today();
            CPQObj.Opportunity__c = oppty.Id;
            CPQObj.Reason_for_the_Quote__c = 'Service and repair- Priority 2 High';
            CPQObj.Service_Due_Date_SLA__c = System.today();
            CPQObj.Serial_Number__c = '1232432';
            CPQObj.Model_Number__c = '4564565';
            centacPartList.add(CPQObj);
        }
        insert centacPartList;
        return centacPartList;
    }*/

    public static List<Product2> createTestProduct(Integer numberOfRecords, Boolean isInsert, Map<String, String> fieldValueByFieldName) {
        List<Product2> listOfTestProduct = new List<Product2>();

        for (Integer i = 0; i < numberOfRecords; i++) {
            Product2 product = new Product2();
            product.Name = getFieldValues(fieldValueByFieldName, 'Name', 'Test Product' + i);
            product.Family = getFieldValues(fieldValueByFieldName, 'Name', 'Test Family' + i);
            product.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByDeveloperName().get('CTS_Field_Service_Lightning').getRecordTypeId();
            listOfTestProduct.add(product);
        }

        if (isInsert) {
            insert listOfTestProduct;
        }

        return listOfTestProduct;
    }

    public static List<ProductRequired> createTestProductRequired(Integer numberOfRecords, Boolean isInsert, Map<String,Object> fieldValueByFieldName) {
        List<ProductRequired> listOfTestProductRequired = new List<ProductRequired>();

        for (Integer i = 0; i < numberOfRecords; i++) {
            ProductRequired productRequiredRec = new ProductRequired();
            for(String property:fieldValueByFieldName.keySet()){
                productRequiredRec.put(property,fieldValueByFieldName.get(property));
            }
            listOfTestProductRequired.add(productRequiredRec);
        }

        if (isInsert) {
            insert listOfTestProductRequired;
        }

        return listOfTestProductRequired;
    }


    public static List<User> createTestUsers(Id profileId, String username) {
        List<User> listOfUser = new List<User>();

        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User dummyUser = new User(UserRoleId = portalRole.Id,
                ProfileId = profileId,
                Username = username,
                Alias = 'User1',
                Email = username,
                EmailEncodingKey = 'UTF-8',
                Firstname = 'Jane',
                Lastname = 'Doe',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                TimeZoneSidKey = 'America/Chicago'
        );
        listOfUser.add(dummyUser);
        insert listOfUser;
        return listOfUser;
    }


   /* public static List<ServiceResource> createServiceResources(Integer numberOfRecords, Boolean isInsert,Map<String,String> fieldValueByFieldName){
        List<ServiceResource> serviceResourcesList = new List<ServiceResource>();

        for(Integer index = 0;index<numberOfRecords;index++){
            ServiceResource sr = new ServiceResource();
            sr.isActive = true;
            for(String property:fieldValueByFieldName.keySet()){
                sr.put(property,fieldValueByFieldName.get(property));
            }
            serviceResourcesList.add(sr);
        }

        if(isInsert)
            insert serviceResourcesList;

        return serviceResourcesList;
    }


    public static List<Schema.Location> createTestLocation(Integer numberOfRecords, Boolean isInsert, Map<String,Object> fieldValueByFieldName) {
        List<Schema.Location> listOfTestLocations = new List<Schema.Location>();

        for (Integer i = 0; i < numberOfRecords; i++) {
            Schema.Location locationRec = new Schema.Location();
            for(String property:fieldValueByFieldName.keySet()){
                locationRec.put(property,fieldValueByFieldName.get(property));
            }
            listOfTestLocations.add(locationRec);
        }

        if (isInsert) {
            insert listOfTestLocations;
        }

        return listOfTestLocations;
    }
    */
    private static String getFieldValues(Map<String, String> fieldValueByFieldName, String fieldName, String fieldValue) {
        if (fieldValueByFieldName != null && fieldValueByFieldName.containsKey(fieldName)) {
            return fieldValueByFieldName.get(fieldName);
        }
        return fieldValue;
    }
    /*
    public static List<ServiceAppointment> createServiceAppointments(Integer numberOfRecords, Boolean isInsert,Map<String,Object> fieldValueByFieldName){
        List<ServiceAppointment> serviceApptList = new List<ServiceAppointment>();

        for(Integer index = 0;index<numberOfRecords;index++){
            ServiceAppointment sa = new ServiceAppointment();
            sa.EarliestStartTime = Date.valueOf('2020-05-27T07:00:00.000+0000');
            sa.DueDate = Date.valueOf('2020-06-17T06:59:00.000+0000');
            sa.SchedStartTime = Date.valueOf('2020-05-27T07:00:00.000+0000');
            sa.SchedEndTime = Date.valueOf('2020-06-17T06:59:00.000+0000');
            for(String property:fieldValueByFieldName.keySet()){
                sa.put(property,fieldValueByFieldName.get(property));
            }
            serviceApptList.add(sa);
        }

        if(isInsert)
            insert serviceApptList;

        return serviceApptList;
    }
    */
    public static List<WorkOrderLineItem> createTestWorkOrderLineItem(Integer numberOfRecords, Boolean isInsert,  List<WorkOrder> listOfWorkOrder) {
        List<WorkOrderLineItem> listOfTestWorkOrderLineItem = new List<WorkOrderLineItem>();
        for (Integer i = 0; i < numberOfRecords; i++) {
            WorkOrderLineItem woLineItem = new WorkOrderLineItem();
            if (listOfWorkOrder.size() >= numberOfRecords) {
                woLineItem.WorkOrderId = listOfWorkOrder[i].Id;
            } else if (listOfWorkOrder.size() == 1 && numberOfRecords > 1) {
                woLineItem.WorkOrderId = listOfWorkOrder[0].Id;
            }
            listOfTestWorkOrderLineItem.add(woLineItem);
        }
        if (isInsert) {
            insert listOfTestWorkOrderLineItem;
        }
        return listOfTestWorkOrderLineItem;
    }

    public static List<WorkOrder> createTestWorkOrder(Integer numberOfRecords, Boolean isInsert, Map<String, String> fieldValueByFieldName) {
        List<WorkOrder> listOfTestWorkOrder = new List<WorkOrder>();
        String workOrderRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Work_Order_CTS_Field_Service').getRecordTypeId();
        for (Integer i = 0; i < numberOfRecords; i++) {
            WorkOrder wo = new WorkOrder();
            wo.RecordTypeId= getFieldValues(fieldValueByFieldName, 'RecordTypeId', workOrderRecordTypeId);
            listOfTestWorkOrder.add(wo);
        }
        if (isInsert) {
            insert listOfTestWorkOrder;
        }
        return listOfTestWorkOrder;
    }

    public static List<OperatingHours> createOperatingHours(Integer numberOfRecords, Boolean isInsert,Map<String,String> fieldValueByFieldName){
        List<OperatingHours> operatingHours = new List<OperatingHours>();

        for(Integer index = 0;index<numberOfRecords;index++){
            OperatingHours operatingHoursRec = new OperatingHours();
            for(String property:fieldValueByFieldName.keySet()){
                operatingHoursRec.put(property,fieldValueByFieldName.get(property));
            }
            operatingHours.add(operatingHoursRec);
        }

        if(isInsert)
            insert operatingHours;

        return operatingHours;
    }
    /*
    public static List<AssignedResource> createServiceAssignmentResources(Integer numberOfRecords, Boolean isInsert,Map<String,String> fieldValueByFieldName){
        List<AssignedResource> assignedResourcesList = new List<AssignedResource>();

        for(Integer index = 0;index<numberOfRecords;index++){
            AssignedResource ar = new AssignedResource();
            for(String property:fieldValueByFieldName.keySet()){
                ar.put(property,fieldValueByFieldName.get(property));
            }
            assignedResourcesList.add(ar);
        }

        if(isInsert)
            insert assignedResourcesList;

        return assignedResourcesList;
    }
    
    public static List<ServiceTerritory> createServiceTerritories(Integer numberOfRecords, Boolean isInsert,Map<String,Object> fieldValueByFieldName){
        List<ServiceTerritory> serviceTerritoryList = new List<ServiceTerritory>();

        for(Integer index = 0;index<numberOfRecords;index++){
            ServiceTerritory st = new ServiceTerritory();
            for(String property:fieldValueByFieldName.keySet()){
                st.put(property,fieldValueByFieldName.get(property));
            }
            serviceTerritoryList.add(st);
        }

        if(isInsert)
            insert serviceTerritoryList;

        return serviceTerritoryList;
    }

    public static List<ServiceTerritoryMember> createServiceTerritoryMemberships(Integer numberOfRecords, Boolean isInsert,Map<String,Object> fieldValueByFieldName){
        List<ServiceTerritoryMember> serviceTerritoryMembers = new List<ServiceTerritoryMember>();

        for(Integer index = 0;index<numberOfRecords;index++){
            ServiceTerritoryMember serviceTerritoryMember = new ServiceTerritoryMember();
            for(String property:fieldValueByFieldName.keySet()){
                serviceTerritoryMember.put(property,fieldValueByFieldName.get(property));
            }
            serviceTerritoryMembers.add(serviceTerritoryMember);
        }

        if(isInsert)
            insert serviceTerritoryMembers;

        return serviceTerritoryMembers;
    }*/


    public static User createTestUser(String ProfileName,Boolean isInsert)
    {
        Id p = [select id from profile where name=:ProfileName].id;
        User SampleUser = new User(alias = 'Sample', email='SampleUser@gmail.com',
                emailencodingkey='UTF-8', lastname='Sample User', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='SampleUser@gmail.com'+randomWithLimit(8)
                );

        if(isInsert)
            insert sampleUser;

        return SampleUser;

    }

    public static Integer randomWithLimit(Integer upperLimit){
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }

}