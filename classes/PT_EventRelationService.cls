public without sharing class PT_EventRelationService {
    
    /*
    	Gets all EventRelations for a set of Ids and returns 
		a map of the eventIds to a set of the contactIds that are present in the eventreleation records
		Map<EventId,Map<RelationId,EventRelation>>
    */
    public static Map<Id,Map<Id, EventRelation>> getExistingEventRelationIds(Set<Id> eventIds)
    {
        Set<String> existingEventRelationContactIds = new Set<String>();
        EventRelation[] existingEventRelations = [
            select 	AccountId,EventId,IsDeleted,IsInvitee,IsParent,IsWhat,RelationId,RespondedDate,Response,Status 
            from 	eventrelation
            where	Relation.Type != 'User' AND
            		EventId IN :eventIds AND
            		IsWhat = false
        ];
        
        Map<Id,Map<Id, EventRelation>> eventIdToContactIdAndEventRelation = new Map<Id,Map<Id, EventRelation>>();
        
        for(EventRelation thisEventRelation: existingEventRelations)
        {
            if(!String.isEmpty(thisEventRelation.RelationId))
            {
                if(eventIdToContactIdAndEventRelation.containsKey(thisEventRelation.EventId))
                {
                    eventIdToContactIdAndEventRelation.get(thisEventRelation.EventId).put(thisEventRelation.RelationId, thisEventRelation);
                }
                else
                {
                    Map<Id, EventRelation> contactIdToEventRelation = new Map<Id, EventRelation>();
                    contactIdToEventRelation.put(thisEventRelation.RelationId,thisEventRelation);
                    eventIdToContactIdAndEventRelation.put(thisEventRelation.EventId,contactIdToEventRelation);
                }
            }                
        }
        return eventIdToContactIdAndEventRelation;
    }
}