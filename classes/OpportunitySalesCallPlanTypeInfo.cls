public class OpportunitySalesCallPlanTypeInfo {

    private static OpportunitySalesCallPlanTypeInfo instance = null;
    public Map<Id,Schema.RecordTypeInfo> recordTypeInfoById {get; private set;}
    
    private OpportunitySalesCallPlanTypeInfo(){
            Schema.DescribeSObjectResult opportunitySalesCallPlanSchema = Opportunity_Sales_Call_Plan__c.sObjectType.getDescribe();
			// Get all record types for object mapped by record type Id
			recordTypeInfoById = opportunitySalesCallPlanSchema.getRecordTypeInfosById();
    }
    
    public static OpportunitySalesCallPlanTypeInfo getInstance(){
        
        // There should be only one instance of this object so implemented via the singleton design pattern.
        if (instance == null){
            instance = new OpportunitySalesCallPlanTypeInfo();
        }
        
        return instance;
    }
}