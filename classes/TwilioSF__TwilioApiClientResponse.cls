/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioApiClientResponse {
    global TwilioApiClientResponse(System.HttpRequest request, System.HttpResponse response) {

    }
    global String getErrorMessage() {
        return null;
    }
    global Integer getHtttpStatusCode() {
        return null;
    }
    global String getQueryString() {
        return null;
    }
    global Map<String,String> getRequestHeaders() {
        return null;
    }
    global String getResponseBody() {
        return null;
    }
    global Map<String,String> getResponseHeaders() {
        return null;
    }
    global List<TwilioSF.TwilioJsonParser> getTwilioArrayJsonParser(String arrayString) {
        return null;
    }
    global TwilioSF.TwilioJsonParser getTwilioJsonParser() {
        return null;
    }
    global String getUrl() {
        return null;
    }
    global Boolean hasError() {
        return null;
    }
    global Boolean isJson() {
        return null;
    }
    global Map<String,Object> toMap() {
        return null;
    }
}
