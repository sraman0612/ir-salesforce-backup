@isTest
public class CTS_TestUtility {
    
    public static Account createAccount(String accName, Boolean isInsert){
        
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType where DeveloperName = 'CTS_TechDirect_Account' and SobjectType='Account' limit 1].Id;
        //Division__c division = new Division__c(Division_Type__c= 'Customer Center',EBS_System__c='Oracle 11i');
        //insert division;
        Account acc = new Account(
            Name = accName, 
            BillingStreet = '123 Sesame Street',
            BillingCity = 'Miami',
            BillingState = 'FL',
            BillingPostalCode = '47372',
            BillingCountry = 'USA',
            ShippingCountry  = 'United States',
            County__c = 'Orange',
            RecordTypeId=AirNAAcctRT_ID,
            Account_Region__c = 'Oracle 11i'
            //Account_Division__c = division.Id
        );
        
        if(isInsert){
            insert acc;
        }

        return acc;
    }
    
    public static Contact createContact(String lastName, String firstName, String email, String accountId, Boolean isInsert){
        
        Contact con = new Contact(
            lastName = lastName, 
            firstName = firstName , 
            email = email, 
            accountId = accountId,
            Title = 'Account Manager',
            Phone = '555-555-5555'
        );
        
        if(isInsert){
            insert con;
        }
        
        return con;
    }
   
    public static Case createCase(String subject, String accId, String conId, Boolean isInsert){
        Case cs = new Case(subject = subject, accountId = accId, contactId = conId);
        if(isInsert){
            insert cs;
        }
        return cs;
    }
    
    public static Out_of_Office_Settings__c createOutOfOfficeSetting(Boolean isEnabled, String setupOwnerId, Boolean isInsert){
        Out_of_Office_Settings__c ooo = new Out_of_Office_Settings__c(SetupOwnerId = setupOwnerId, Enabled__c = isEnabled,
                                        Start_Date__c = DateTime.now().addDays(-1), End_Date__c = DateTime.now().addDays(1));
        if(isInsert){
            insert ooo;
        }
        return ooo;
    }
    
    public static Out_of_Office_Case_Routings__c createOutOfOfficeCaseRouting(String userId, String backupUserId, Boolean isInsert){
        Out_of_Office_Case_Routings__c caseRouting = new Out_of_Office_Case_Routings__c(Name = userId + '_', User_ID__c = userId, 
                                                     Backup_User_Queue_ID__c = userId);
        if(isInsert){
            insert caseRouting;
        }
        return caseRouting;
    }
    
    public static User createUser(Boolean isInsert) {
        User u = new User();
        u.Username = 'test@test.com.hasjsa78' + String.valueOf(Math.random()*1000000);
        u.Alias = 'has' + String.valueOf(Math.random()*1000000).substring(0,4);
        u.Email = 'test@test.com';
        u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Test';
		u.TimeZoneSidKey = 'America/Los_Angeles';
        u.Assigned_From_Email_Address__c = 'cx_dublin_support@irco.com';
        u.Brand__c = 'IR';
        u.Default_Product_Category__c = 'Compressors';
        u.Department__c = 'IR Comp OM EU (Dublin)';
        if(isInsert) insert u;
        return u;
    }
    
    public static User createUser(Id contactId, Id profileId, Boolean isInsert) {
        User u = new User();
        u.Username = 'test@test.com.hasjsa78' + String.valueOf(Math.random()*1000000);
        u.Alias = 'has' + String.valueOf(Math.random()*1000000).substring(0,4);
        u.Email = 'test@test.com';
        u.ProfileId = profileId;
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_US';
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = 'Test';
		u.TimeZoneSidKey = 'America/Los_Angeles';
        u.contactId = contactId;
        
        if(isInsert){            
            update new Contact(Id = contactId, CTS_IOT_Community_Status__c = 'Approved');   
            insert u;
        }
        
        return u;
    }
    
    public static Asset createAsset(String assetName, String serialNumber, Id accountId, Id recTypeId, Boolean isInsert){
        Asset asset = new Asset();
        asset.Name = assetName;
        asset.AccountId = accountId;
        asset.SerialNumber = serialNumber; 
        if(recTypeId != null)
        	asset.RecordTypeId = recTypeId;
        if(isInsert)
        	insert asset;
       	return asset;
    }

    public static CTS_IOT_Service_Request__c createCTSIOTServiceRequest(Id accountId, Id contactId, Asset asset, String status, String type, String severity, String description){

        return new CTS_IOT_Service_Request__c(
            Type__c = type,
            Status__c = status,
            Account__c = accountId,
            Asset__c = asset.Id,
            Opened_Date__c = DateTime.now(),
            Contact__c = contactId,
            Siebel_ID__c = asset.Siebel_ID__c,
            Severity__c = severity,
            Problem_Description__c = description              
        );
    }

    public static CTS_IOT_Frame_Type__c createFrameType(Boolean doInsert, String name, String type){

        CTS_IOT_Frame_Type__c frameType = new CTS_IOT_Frame_Type__c();
        frameType.Name = name;
        frameType.CTS_IOT_Type__c = type;

        if (doInsert){
            insert frameType;
        }

        return frameType;
    }
    
    public static CTS_IOT_Community_Administration__c setDefaultSetting(Boolean isInsert){

        RecordType leadRecordType = [Select Id From RecordType Where sObjectType = 'Lead' and DeveloperName = 'CTS_Community_User_Global_Registration_Lead'];
        RecordType serviceRequestRecordType = [Select Id From RecordType Where sObjectType = 'Lead' and DeveloperName = 'NA_Air'];

        CTS_IOT_Community_Administration__c communityAdmin = new CTS_IOT_Community_Administration__c(
            Community_EULA_Current_Version__c='Version 1.0',
            Default_Lead_Opportunity_Type__c='Prospect',
            Default_Lead_Source__c = 'Web',
            Default_Lead_Source1__c = 'CTS Customer Portal',
            Default_Lead_Source1_Service_Requests__c = 'Connectivity.IngersollRand.com',
            Default_Lead_Source2__c='New CTS Customer Portal User Request',
            Default_Lead_Source2_Service_Requests__c = 'New Service Request',
            Default_SBU_ID_Workflow__c= 'NA Air',
            Default_Request_Type_Service_Requests__c = 'Request for Service',
            Default_Standard_User_Profile_Name__c='CTS Customer Community - Standard User',
            Default_Standard_User_All_Profile_Name__c='CTS Customer Community - Standard User (All Sites)',
            Default_Super_User_Profile_Name__c ='CTS Customer Community - Super User',
            New_Lead_Record_Type_ID_NA__c = leadRecordType.Id,    
            New_Service_Request_Record_Type_ID_NA__c = serviceRequestRecordType.Id,                                                                                                                                                                                                
            Losant_Dashboard_Enabled__c = true,
            Losant_Timeout_Minutes__c = 120,
            Service_Request_Batch_Run_on_Completion__c = false,
            Service_Request_Batch_Run_Complete_Min__c = 5
        );

        if(isInsert){
            insert communityAdmin;
        }

        return communityAdmin;                                                                                             
    }
}