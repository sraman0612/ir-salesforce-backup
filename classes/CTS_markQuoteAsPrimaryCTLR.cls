public class CTS_markQuoteAsPrimaryCTLR{
      
    public List<cafsl__Oracle_Quote__c> selQuoteLst;
    public String quoteId;
    public String oppId;
    
    public CTS_markQuoteAsPrimaryCTLR(ApexPages.StandardSetController cntlr){
        oppId = ApexPages.currentPage().getParameters().get('id');
        selQuoteLst = cntlr.getSelected(); 
        if(selQuoteLst.size()==1){quoteId = selQuoteLst[0].Id;}
    } 
    
    public PageReference updateQuotes(){
        if(null !=quoteId){
        cafsl__Oracle_Quote__c oq = new cafsl__Oracle_Quote__c(id=quoteId,Primary__c=TRUE);
        update oq;
        }
        PageReference pageRef = new PageReference('/lightning/r/'+ oppId + '/related/cafsl__Oracle_Quotes__r/view');
        return pageRef;    
    }

 }