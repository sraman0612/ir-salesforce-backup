@isTest
public class PT_SellingsTest {
    
   static testMethod void testPT_LeadSellings(){
       
        TestDataUtility dataUtil = new TestDataUtility();
        
        Account thisAccount = dataUtil.createAccount('PT_powertools');
        thisAccount.BillingCountry = 'USA';
        thisAccount.BillingCity = 'Augusta';
        thisAccount.BillingState = 'GA';
        thisAccount.BillingStreet = '2044 Forward Augusta Dr';
        thisAccount.BillingPostalCode = '566';
        insert thisAccount;
       
       Test.startTest();
       
       PT_Sellings.getYears(thisAccount.Id);
       List<PT_Selling__c> ptSellings = PT_Sellings.getSellings(thisAccount.Id, '2018');
       PT_Sellings.savePTSellings(thisAccount.Id, '2018', ptSellings);
       
       Test.stopTest();
       
   }

}