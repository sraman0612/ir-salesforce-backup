public with sharing class CTS_TechDirect_TaxonomyController1 {  
    @AuraEnabled
    public static List< PicklistEntryWrapper > getPicklistValues(String object_name, String field_name, String default_val) {
        List<PicklistEntryWrapper> options = new List<PicklistEntryWrapper>(); //new list for holding all of the picklist options       
        Schema.SObjectType sobject_type = Schema.getGlobalDescribe().get(object_name); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list       
            if(a.isActive()) {
                if(a.getValue() == default_val) {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), true)); //add the value and label to our final list    
                } else {
                    options.add(new PicklistEntryWrapper(a.getLabel(), a.getValue(), false)); //add the value and label to our final list    
                }    
            }                        
        }
        return options; //return the List
    }
  
    @AuraEnabled    
    public static List <Knowledge__kav> getAllArticles(String searchKeyword, String language, String product, String category) {
        // Update user language to selected language on knowledge search page to avoid "Invalid Page" error
        if(String.isNotBlank(language)) {
            User userObj = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() limit 1];
            userObj.LanguageLocaleKey = language;
            try {
                update userObj;
            } catch(Exception ex) {
                System.debug('Exception has raised while updating user langauge ## ' + ex.getMessage());
            }
        }        
        
        // Bypass public community user
        
        User userObj = [SELECT Id, Name, Profile.Name, isPortalEnabled FROM User WHERE Id = :UserInfo.getUserId() limit 1];
        String [] descMulti = null;
        if(userObj.Profile.Name != 'CTS TechDirect Public Profile' && userObj.isPortalEnabled) {
            Id accountId = [SELECT Contact.AccountId FROM User WHERE Id=:UserInfo.getUserId()].Contact.AccountId;
            String description = [SELECT Description FROM Account where Id = :accountId ].Description;            
            if(String.isNotBlank(description)) {
                descMulti = description.split(',');
            }            
        }
        System.debug('descMulti ## ' + descMulti);    
        String publishStatus = 'Online';
        String validationStatus = 'Publish';
        
        List<Knowledge__kav> contentListToFilter = new List<Knowledge__kav>();
        String searchString1 = String.escapeSingleQuotes(searchKeyword); 
        
        final String specialChars = '& | ! - + ( ) { } [ ] ^ " ~ * ? : \'';
        String searchString = searchString1.replaceAll('\\\\', '');
        for (String sc: specialChars.split(' ')) 
        {
            searchString = searchString.replaceAll('\\' + sc, '\\\\' + sc);
        }
    
        String query = 'FIND {%' + searchString + '} IN ALL FIELDS ' +
                         'RETURNING  Knowledge__kav (Id, ArticleNumber, Title, URLName, ValidationStatus, PublishStatus, CTS_TechDirect_Article_Type__c, CTS_TechDirect_Access_Level__c, Language ' +
                         'WHERE PublishStatus = \'' + publishStatus + '\'';
 
        query = query + ' AND IsLatestVersion = true';     
//        query = query + ' AND validationStatus = \'' + validationStatus + '\'';  
        
        if(descMulti != null && !descMulti.isEmpty()) {
            query = query + ' AND CTS_TechDirect_Access_Level__c INCLUDES (\''+ String.join(descMulti, '\',\'') +'\')';
        }
        if(String.isNotBlank(language)) {
            query = query + ' AND language = \'' + language + '\')';        
        }
        
        if(String.isNotBlank(product) || String.isNotBlank(category)) {
            query = query + ' WITH DATA CATEGORY';
            Boolean isAndCondition = false;
            if(String.isNotBlank(product)) {                
                query = query + ' IR_Global_Products_Group__c ABOVE_OR_BELOW (' + product + '__c' + ')';
                isAndCondition = true;
            }
            if(String.isNotBlank(category)) {
                if(isAndCondition) {
                    query = query + ' AND';    
                }
                query = query + ' IR_Global_Category_Group__c BELOW (' + category + '__c' + ')';
            }
        }
        
        for(List<Knowledge__kav> cList : search.query(query)) 
        {
            for(Knowledge__kav c : cList) 
            {
                contentListToFilter.add(c);
            }
        } 
        
        System.debug('Query -- ' + query);
        return contentListToFilter;
    }
    
    /* Start - Dhilip commented for trying SOSL
    @AuraEnabled    
    public static List <Knowledge__kav> getAllArticles(String searchKeyword, String language, String product, String category) {
        // Update user language to selected language on knowledge search page to avoid "Invalid Page" error
        if(String.isNotBlank(language)) {
            User userObj = [SELECT Id, LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId() limit 1];
            userObj.LanguageLocaleKey = language;
            try {
                update userObj;
            } catch(Exception ex) {
                System.debug('Exception has raised while updating user langauge ## ' + ex.getMessage());
            }
        }        
        
        // Bypass public community user
        
        User userObj = [SELECT Id, Name, Profile.Name, isPortalEnabled FROM User WHERE Id = :UserInfo.getUserId() limit 1];
        String [] descMulti = null;
        if(userObj.Profile.Name != 'CTS TechDirect Public Profile' && userObj.isPortalEnabled) {
            Id accountId = [SELECT Contact.AccountId FROM User WHERE Id=:UserInfo.getUserId()].Contact.AccountId;
            String description = [SELECT Description FROM Account where Id = :accountId ].Description;            
            if(String.isNotBlank(description)) {
                descMulti = description.split(',');
            }            
        }
        System.debug('descMulti ## ' + descMulti);    
        String publishStatus = 'Online';
        String validationStatus = 'Publish';
        
        String query = 'SELECT Id, ArticleNumber, Title, URLName, ValidationStatus, PublishStatus, CTS_TechDirect_Article_Type__c,' +
            ' CTS_TechDirect_Access_Level__c, Language FROM Knowledge__kav ' +
            ' WHERE PublishStatus = \'' + publishStatus + '\''; 
        query = query + ' AND IsLatestVersion = true';     
        query = query + ' AND validationStatus = \'' + validationStatus + '\'';  
        
        if(descMulti != null && !descMulti.isEmpty()) {
            query = query + ' AND CTS_TechDirect_Access_Level__c INCLUDES (\''+ String.join(descMulti, '\',\'') +'\')';
        }
        if(String.isNotBlank(language)) {
            query = query + ' AND language = \'' + language + '\'';        
        }
        if(String.isNotBlank(searchKeyword)) {
            query = query + ' AND (title like \'%' + searchKeyword + '%\'';
            query = query + ' OR  CTS_Techdirect_Keyword__c like \'%' + searchKeyword + '%\'';
            query = query + ' OR UrlName like \'%' + searchKeyword  + '%' +  '\')';    
        }
        if(String.isNotBlank(product) || String.isNotBlank(category)) {
            query = query + ' WITH DATA CATEGORY';
            Boolean isAndCondition = false;
            if(String.isNotBlank(product)) {                
                query = query + ' IR_Global_Products_Group__c ABOVE_OR_BELOW (' + product + '__c' + ')';
                isAndCondition = true;
            }
            if(String.isNotBlank(category)) {
                if(isAndCondition) {
                    query = query + ' AND';    
                }
                query = query + ' IR_Global_Category_Group__c BELOW (' + category + '__c' + ')';
            }
        }
        System.debug('Query -- ' + query);
        return Database.query(query);
    }
    End - Dhilip commented for trying SOSL */
    
    @AuraEnabled
    public static List < DataCategoryWrapper > getDataCategoryStructure(String object_name, String data_category_group_name) {
        List < DataCategoryWrapper > dataCategoryList = new List < DataCategoryWrapper > ();
        DataCategoryWrapper outerWrapObj;
        try {
            //Making the call to the describeDataCategoryGroups to get the list of category groups associated
            List<DescribeDataCategoryGroupResult> describeCategoryResult = new List<DescribeDataCategoryGroupResult> ();            
            for(DescribeDataCategoryGroupResult obj: Schema.describeDataCategoryGroups(new List < String > {object_name})) {
                if(obj.getName() == data_category_group_name) {
                    describeCategoryResult.add(obj);
                }
            }                        
            //Creating a list of pair objects to use as a parameter for the describe call
            List<DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();            
            //Looping throught the first describe result to create the list of pairs for the second describe call
            for(DescribeDataCategoryGroupResult singleResult : describeCategoryResult){
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }            
            //Getting data from the result
            for(DescribeDataCategoryGroupStructureResult singleResult : Schema.describeDataCategoryGroupStructures(pairs, false)){                
                for(Schema.DataCategory category: singleResult.getTopCategories()) {
                    outerWrapObj = new DataCategoryWrapper(category.getLabel(), category.getName(), false);  
                    outerWrapObj = prepareChildCategories(outerWrapObj, category.getChildCategories());
                }
                dataCategoryList.add(outerWrapObj);
            }
        } catch (Exception e){
            System.debug('### Exeception ### ' + e.getMessage() +': at Line# '+e.getLineNumber());
        }
        return dataCategoryList;
    }
    
    private static DataCategoryWrapper prepareChildCategories(DataCategoryWrapper parentWrapper, DataCategory[] categories) {
        for(DataCategory subCategory: categories) {
            DataCategoryWrapper innerWrapObj = new DataCategoryWrapper(subCategory.getLabel(), subCategory.getName(), false);            
            parentWrapper.items.add(innerWrapObj);             
            if(subCategory.getChildCategories() != null && subCategory.getChildCategories().size() > 0) {
                prepareChildCategories(innerWrapObj, subCategory.getChildCategories());
            }            
        }                
        return parentWrapper;
    }
    
    public class PicklistEntryWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String value { get; set; }
        @AuraEnabled
        public Boolean selected { get; set; }      
        
        // Default Constructor
        public PicklistEntryWrapper() { }
        
        // Parameterized Constructor
        public PicklistEntryWrapper(String label, String value, Boolean selected) {
            this.label = label;
            this.value = value;
            this.selected = selected;
        }
    }
    
    public class DataCategoryWrapper {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public Boolean expanded { get; set; }           
        @AuraEnabled
        public List < DataCategoryWrapper > items { get {
            if(items == null) {
                items = new List < DataCategoryWrapper > ();
            }    
            return items;
        } set; } 
        
        // Default Constructor
        public DataCategoryWrapper() { }
        
        // Parameterized Constructor
        public DataCategoryWrapper(String label, String name, Boolean expanded) {
            this.label = label;
            this.name = name;
            this.expanded = expanded;
        }
    }
}