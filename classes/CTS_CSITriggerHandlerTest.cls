@isTest
private class CTS_CSITriggerHandlerTest {
    @isTest static void testPartInsert() {
        List<Account> accList = CTS_TestDataFactory.createNAAccounts(1);
        Test.startTest();
        List<Customer_Satisfaction_Results__c> csiList = CTS_TestDataFactory.createCSI(1, accList[0].Id,'3');
        Account updatedAcc = [SELECT Id,CTS_OSAT_Satisfaction__c,
                              CTS_Promoted_Score__c,
                              CTS_Committed_Score__c,
                              CTS_CSI_Comments__c,
                              CTS_Survey_Date__c ,
                              CTS_Commenter__c,
                              CTS_Email__c
                              FROM Account 
                              WHERE Id =:accList[0].Id limit 1];
        system.assertEquals(updatedAcc.CTS_OSAT_Satisfaction__c, csiList[0].CTS_OSAT_Satisfaction__c);
        system.assertEquals(updatedAcc.CTS_Promoted_Score__c , csiList[0].CTS_Promoted_Score__c);
        system.assertEquals(updatedAcc.CTS_Committed_Score__c , csiList[0].CTS_Committed_Score__c);
        system.assertEquals(updatedAcc.CTS_Survey_Date__c , csiList[0].Response_Date__c);
        system.assertEquals(updatedAcc.CTS_CSI_Comments__c , csiList[0].Survey_Comment__c);
        Test.stopTest();
    }
    @isTest static void testPartDelete() {
        List<Account> accList = CTS_TestDataFactory.createNAAccounts(1);
        List<Customer_Satisfaction_Results__c> csiList = CTS_TestDataFactory.createCSI(1, accList[0].Id,'2');
        Test.startTest();
        delete csiList[0];
        Account updatedAcc = [SELECT Id,CTS_OSAT_Satisfaction__c,
                              CTS_Promoted_Score__c,
                              CTS_Committed_Score__c,
                              CTS_CSI_Comments__c,
                              CTS_Survey_Date__c 
                              FROM Account 
                              WHERE Id =:accList[0].Id limit 1];
        system.assertEquals(updatedAcc.CTS_OSAT_Satisfaction__c, null);
        system.assertEquals(updatedAcc.CTS_Promoted_Score__c , null);
        system.assertEquals(updatedAcc.CTS_Committed_Score__c , null);
        system.assertEquals(updatedAcc.CTS_Survey_Date__c , null);
        system.assertEquals(updatedAcc.CTS_CSI_Comments__c , null);
        Test.stopTest();
    }
    @isTest static void testPartUpdate() {
        List<Account> accList = CTS_TestDataFactory.createNAAccounts(1);
        List<Customer_Satisfaction_Results__c> csiList = CTS_TestDataFactory.createCSI(2, accList[0].Id,'1');
        Test.startTest();
        Customer_Satisfaction_Results__c csiObj = csiList[1];
        csiObj.CTS_Promoted_Score__c = '2';
        update csiObj;
        Account updatedAcc = [SELECT Id,CTS_OSAT_Satisfaction__c,
                              CTS_Promoted_Score__c,
                              CTS_Committed_Score__c,
                              CTS_CSI_Comments__c,
                              CTS_Survey_Date__c 
                              FROM Account 
                              WHERE Id =:accList[0].Id limit 1];
        system.assertEquals(updatedAcc.CTS_OSAT_Satisfaction__c, csiObj.CTS_OSAT_Satisfaction__c);
        system.assertEquals(updatedAcc.CTS_Promoted_Score__c , csiObj.CTS_Promoted_Score__c);
        system.assertEquals(updatedAcc.CTS_Committed_Score__c , csiObj.CTS_Committed_Score__c);
        system.assertEquals(updatedAcc.CTS_Survey_Date__c , csiObj.Response_Date__c);
        system.assertEquals(updatedAcc.CTS_CSI_Comments__c , csiObj.Survey_Comment__c);
        Test.stopTest();
    }
        @isTest static void testPartUnDelete() {
        List<Account> accList = CTS_TestDataFactory.createNAAccounts(1);
        List<Customer_Satisfaction_Results__c> csiList = CTS_TestDataFactory.createCSI(1, accList[0].Id,'2');
        delete csiList[0];
        Test.startTest();
        Account updatedAcc = [SELECT Id,CTS_OSAT_Satisfaction__c,
                              CTS_Promoted_Score__c,
                              CTS_Committed_Score__c,
                              CTS_CSI_Comments__c,
                              CTS_Survey_Date__c 
                              FROM Account 
                              WHERE Id =:accList[0].Id limit 1];
        system.assertEquals(updatedAcc.CTS_OSAT_Satisfaction__c, null);
        undelete csiList[0];
        Account updatedAcc2 = [SELECT Id,CTS_OSAT_Satisfaction__c,
                              CTS_Promoted_Score__c,
                              CTS_Committed_Score__c,
                              CTS_CSI_Comments__c,
                              CTS_Survey_Date__c 
                              FROM Account 
                              WHERE Id =:accList[0].Id limit 1];
        system.assertEquals(updatedAcc2.CTS_OSAT_Satisfaction__c, '2');
        Test.stopTest();
    }
}