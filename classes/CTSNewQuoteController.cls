public class CTSNewQuoteController {
    
    @AuraEnabled
    public static String getTriageSessionInfo(){
        return CTSCommunityCacheCaller.getAsJson();
    }
    
    @AuraEnabled
    public static void setTriageSessionInfo(String oppId){
        CTSCommunityCacheCaller.setOppId(oppId);
    }
    @AuraEnabled
    public static ObjectWrapper fetchData(String infoJSON) {
        ComponentInfo info = (ComponentInfo)JSON.deserialize(infoJSON, ComponentInfo.class);
		    System.debug('info');
        System.debug(info);
        System.debug(info.parentRecordId);
        if (String.isNotEmpty(info.objectType)) {
            String countQuery = getCountQueryString(info);
            String queryObjects = getObjectQueryString(info);
            System.debug(countQuery);

            return new ObjectWrapper(
                    info.recordToDisplay,
                    info.pageNumber,
                    Database.countQuery(countQuery),
                    Database.query(queryObjects)
            );
        } else {
            return new ObjectWrapper();
        }
    }

    private static String getCountQueryString(ComponentInfo info) {
        String objectType = String.escapeSingleQuotes(info.objectType);
        String parentField = info.parentField;
        Id parentRecordId = info.parentRecordId;

        String countQuery = 'SELECT count() FROM ';
        countQuery += objectType;

        if (String.isNotEmpty(parentField) && parentRecordId != null) {
            parentField = String.escapeSingleQuotes(parentField);
            countQuery += ' WHERE ' + parentField + ' = \''+ parentRecordId +'\'';
        }

        return countQuery;
    }

    private static String getObjectQueryString(ComponentInfo info) {
        String objectType = String.escapeSingleQuotes(info.objectType);
        String parentField = info.parentField;
        Id parentRecordId = info.parentRecordId;

        String queryObjects = 'SELECT Id, Name, CreatedDate,Primary__c, cafsl__Transaction_ID__c FROM ';
        queryObjects += objectType;

        if (String.isNotEmpty(parentField) && parentRecordId != null) {
            parentField = String.escapeSingleQuotes(parentField);
            queryObjects += ' WHERE ' + parentField +' = \''+ parentRecordId +'\' ORDER BY CreatedDate DESC LIMIT ' + info.recordToDisplay;
        } else {
            queryObjects += ' ORDER BY CreatedDate DESC LIMIT ' + info.recordToDisplay
                    + ' OFFSET ' + (info.pageNumber - 1) * info.recordToDisplay;
        }

        return queryObjects;
    }

    @AuraEnabled
    public static ObjectWrapper deleteRecord(String infoJSON) {
        ComponentInfo info = (ComponentInfo)JSON.deserialize(infoJSON, ComponentInfo.class);
        UserRecordAccess recAccess = [
                SELECT
                        HasEditAccess,
                        HasDeleteAccess,
                        RecordId
                FROM UserRecordAccess
                WHERE UserId =: UserInfo.getUserId()
                AND RecordId =: info.recordId
        ];

        if (recAccess != null && recAccess.HasDeleteAccess) {
            try {
                sObject sObj = Schema.getGlobalDescribe().get(info.objectType).newSObject();
                sObj.Id = info.recordId;
                delete sObj;
            } catch (DmlException e) {
                if(!test.isRunningTest())
                  throw new AuraHandledException('Error while deleting data.');
            }
        }

        return fetchData(infoJSON);
    }
    
    @AuraEnabled
    public static void markPrimary(String qid) {
      cafsl__Oracle_Quote__c oq = new cafsl__Oracle_Quote__c(id=qid,Primary__c=TRUE);
      update oq;
    }    

    public class ObjectWrapper {
        @AuraEnabled public Integer pageSize {get;set;}
        @AuraEnabled public Integer page {get;set;}
        @AuraEnabled public Integer total {get;set;}
        @AuraEnabled public List<sObject> data {get;set;}

        ObjectWrapper(Integer pageSize, Integer page, Integer total, List<sObject> data) {
            this.pageSize = pageSize;
            this.page = page;
            this.total = total;
            this.data = data;
        }

        ObjectWrapper() {}
    }

    public class ComponentInfo {
        Integer pageNumber;
        Integer recordToDisplay;
        String objectType;
        String parentField;
        Id parentRecordId;
        Id recordId;
    }

}