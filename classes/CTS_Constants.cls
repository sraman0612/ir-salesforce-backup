public class CTS_Constants {
	public static final String STATUS_NEW = 'New';
    public static final String PRIORITY_LOW = 'Low';
    public static final String CATEGORY_FEEDBACK = 'Feedback';
    public static final String CASE_TYPE_FEEDBACK = 'IR Comp Tech Direct Article Feedback';
    public static final String CASE_SUBJECT = 'Feedback for Article ';
    public static final String CASE_ORIGIN_COMMUNITY = 'Community';
    
    public CTS_Constants(){}
}