@isTest
public with sharing class LightningResponseBaseTest {
	
    @isTest
    private static void testClass(){
        LightningResponseBase response = new LightningResponseBase();
        system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
    }
}