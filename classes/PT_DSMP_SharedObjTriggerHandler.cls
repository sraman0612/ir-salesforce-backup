public with sharing class PT_DSMP_SharedObjTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : PT_DSMP_NPD Trigger Handler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	public void handleAfterInsert(List<PT_DSMP_Shared_Objectives__c> lstNewSharedObj){
		List<PT_DSMP_Shared_Objectives__c> lstSharedObj = new List<PT_DSMP_Shared_Objectives__c>();
		Set<String> setSharedKey = new Set<String>();

		//for(PT_DSMP_Shared_Objectives__c current : lstNewSharedObj){
		for(Integer i=0;i<lstNewSharedObj.size();i++){

			System.debug('mgr lstNewSharedObj[i].Objective_Number__c ' + lstNewSharedObj[i].Objective_Number__c);
			System.debug('mgr lstNewSharedObj[i].Ext_ID__c ' + lstNewSharedObj[i].Ext_ID__c );
			
			if(lstNewSharedObj[i].Objective_Number__c != null &&
				lstNewSharedObj[i].Segment__c != null &&
			    lstNewSharedObj[i].Ext_ID__c != null){

				lstSharedObj.add(lstNewSharedObj[i]);
				setSharedKey.add(lstNewSharedObj[i].Ext_ID__c);

			}

		}

		/*System.debug('handleAfterInsert lstNewSharedObj ' + lstNewSharedObj);
		System.debug('handleAfterInsert ' + lstSharedObj);
		System.debug('handleAfterInsert setSharedKey ' + setSharedKey);*/

		if(!lstSharedObj.isEmpty())
			PT_DSMP_AP03_UpdateAccountObjectives.updateRecords(lstSharedObj, setSharedKey);

	}

}