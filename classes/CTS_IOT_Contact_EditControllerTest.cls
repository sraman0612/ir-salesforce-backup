/**
 * Test class of CTS_IOT_Contact_EditController
 **/
@isTest
private class CTS_IOT_Contact_EditControllerTest {
    
    @testSetup
    static void setup(){
        UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        acc.Siebel_ID__c = '1234';
        insert acc;
        List<Contact> contacts = new List<Contact>();
        contacts.add(CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', acc.Id, false));
        contacts.add(CTS_TestUtility.createContact('Contact456','Test', 'testcts1@gmail.com', acc.Id, false));
        for(Contact con : contacts){
            con.CTS_IOT_Community_Status__c = 'Approved';
        }
        insert contacts;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
        Id profileId = [SELECT Id FROM Profile WHERE Name = :setting.Default_Standard_User_Profile_Name__c Limit 1].Id;
        User communityUser = CTS_TestUtility.createUser(contacts.get(0).Id, profileId, true);
    }
    }
    @isTest
    static void testGetInit(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
           Test.startTest();
           CTS_IOT_Contact_EditController.InitResponse response = CTS_IOT_Contact_EditController.getInit(contacts.get(0).Id);
           System.assert(response != null);
           // Exception Handling testing 
           response = CTS_IOT_Contact_EditController.getInit('001000000000aqw');
           System.assert(response != null);
           System.assert(response.errorMessage == 'List index out of bounds: 0',response.errorMessage);
           Test.stopTest();
        }
    }
    
   @isTest
    static void testUpdateUser(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
            List<User> users = [select id from User where ContactId = : contacts.get(0).Id LIMIT 1];
            if(!users.isEmpty()){
                System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_Contact_EditController.InitResponse initResponse = CTS_IOT_Contact_EditController.getInit(contacts.get(0).Id);
                System.assert(initResponse.contact != null);
                String userStr = (String)JSON.serialize(initResponse.contact);
                LightningResponseBase lResponse = CTS_IOT_Contact_EditController.updateUser(userStr);
                System.assert(lResponse != null);   
                // Exception Handling testing 
                lResponse = CTS_IOT_Contact_EditController.updateUser('{contact:null}');
                Test.stopTest();
            }
         }
      }
    }
    @isTest
    static void testUpdateUserSiteAccess(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
            List<User> users = [select id from User where ContactId = : contacts.get(0).Id LIMIT 1];
            if(!users.isEmpty()){
                System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_Contact_EditController.InitResponse initResponse = CTS_IOT_Contact_EditController.getInit(contacts.get(0).Id);
                System.assert(initResponse.userSites != null);
                if(!initResponse.userSites.isEmpty()){
                    initResponse.userSites.get(0).isUpdateable = true;
                    initResponse.userSites.get(0).hasAccess = true;
                    String userStr = (String)JSON.serialize(initResponse.userSites);
                    LightningResponseBase lResponse = CTS_IOT_Contact_EditController.updateUserSiteAccess(contacts.get(0).Id, userStr);
                    System.assert(lResponse != null);   
                    // Exception Handling testing 
                    lResponse = CTS_IOT_Contact_EditController.updateUserSiteAccess(contacts.get(0).Id,'{contact:null}');
                }
                Test.stopTest();
            }
         }
      }
    }
    @isTest
    static void testApprovalAction(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
            List<User> users = [select id from User where ContactId = : contacts.get(0).Id LIMIT 1];
            if(!users.isEmpty()){
                System.runAs(users.get(0)){
                Test.startTest();
                CTS_IOT_Contact_EditController.ApprovalActionResponse approvalResponse = CTS_IOT_Contact_EditController.approvalAction(contacts.get(0).Id, true,'Unknown Identity');
                System.assert(approvalResponse != null);
                // Exception Handling testing 
                approvalResponse = CTS_IOT_Contact_EditController.approvalAction(contacts.get(0).Id, true,'Others');
                System.assert(approvalResponse != null); 
                Test.stopTest();
            }
         }
      }
    }
    
    @isTest
    static void testActivateUser(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
            List<User> users = [select id from User where ContactId = : contacts.get(0).Id LIMIT 1];
            if(!users.isEmpty()){
                System.runAs(users.get(0)){
                Test.startTest();
                LightningResponseBase responseBase = CTS_IOT_Contact_EditController.activateUser(contacts.get(0).Id);
                System.assert(responseBase != null);
                // Exception Handling testing 
                responseBase = CTS_IOT_Contact_EditController.activateUser('003000000awe123WAE');
                System.assert(responseBase != null); 
                //contacts = [SELECT id FROM Contact WHERE email = 'testcts1@gmail.com' LIMIT 1];
                // test no user 
                //if(!contacts.isEmpty()){
                    //responseBase = CTS_IOT_Contact_EditController.activateUser(contacts.get(0).Id);
                    //System.assert(responseBase != null);
                //}
                Test.stopTest();
            }
         }
      }
    }
    @isTest
    static void testdeActivateUser(){
        List<Contact> contacts = [SELECT id FROM Contact WHERE lastName = 'Contact123' LIMIT 1];
        if(!contacts.isEmpty()){
            List<User> users = [select id from User where ContactId = : contacts.get(0).Id LIMIT 1];
            if(!users.isEmpty()){
                System.runAs(users.get(0)){
                Test.startTest();
                LightningResponseBase responseBase = CTS_IOT_Contact_EditController.deactivateUser(contacts.get(0).Id);
                System.assert(responseBase != null);
                // Exception Handling testing 
                responseBase = CTS_IOT_Contact_EditController.deactivateUser('003000000awe123WAE');
                System.assert(responseBase != null); 
                Test.stopTest();
            }
         }
      }
    }
}