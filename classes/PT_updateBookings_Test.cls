@isTest
public class PT_updateBookings_Test {
  static testmethod void test1(){
    Date theDate = system.today();
    String Month = String.valueOf(theDate.Month());
    String year = String.valueOf(theDate.Year());
    Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
    Account a = new Account(
      RecordTypeId = ptAcctRTID,
      Name = 'TestAcc',
      Type = 'Customer',
      PT_Status__c = 'New',
      PT_IR_Territory__c = 'North',
      PT_IR_Region__c = 'EMEA'
    );
    insert a;
    Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();     
    Opportunity o =  new Opportunity(name= 'testOppty',PT_Invoice_date__c=theDate,closedate=thedate,stagename='2.Qualify',RecordTypeId=PT_OpptyRecordTypeId, Amount=1);
    insert o;
    PT_Booking__c b = new PT_Booking__c(Month__c=Month, Year__c=Year);
    insert b;
    SchedulableContext sc;
    String qryStr = 'SELECT  Date__c, Future_Bookings__c, OwnerId, winrate__c FROM PT_Booking__c';
    PT_updateBookings scheduleBookingUpdate=new PT_updateBookings(qryStr);
    test.startTest();
    scheduleBookingUpdate.execute(sc);   
    test.stopTest();
  }   
}