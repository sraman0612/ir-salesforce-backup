@IsTest(SeeAllData=true)
private class CommonUtilityTest {
    @IsTest(SeeAllData=true)
    static void testGetConnectApiError() {
        String output = '{"output" : {"error" : {"message" : "Testing exception"}}}';

        Test.startTest();
        String message = CommonUtility.getConnectApiError(output);
        Test.stopTest();

        System.assertEquals(true, String.isNotEmpty(message), 'A message should there.');
        System.assertEquals('Testing exception', message);
    }
}