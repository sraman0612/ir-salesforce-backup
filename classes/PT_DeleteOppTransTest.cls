@isTest
public class PT_DeleteOppTransTest {

    @testSetup
    private static void createData(){

        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;

        Account a = new Account(
            RecordTypeId=ptAcctRTID,
            Name = 'TestAcc',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );     

        insert a;

        PT_CreateScheduleOpp.testMonths = 1;

        List<PT_Parent_Opportunity__c> parents = new List<PT_Parent_Opportunity__c>();
        PT_Parent_Opportunity__c p1 = TestUtilityClass.createParentOpp(a.Id);
        p1.Lead_SourcePL__c='Other';
        parents.add(p1);
        
        PT_Parent_Opportunity__c p2 = TestUtilityClass.createParentOpp(a.Id);
        parents.add(p2);
        p2.Lead_SourcePL__c='Other';
        insert parents;
        
        // 8feb2018 CG:PT Org merge
        Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();
        Opportunity o = new Opportunity(Name='test1234',PT_Invoice_date__c=system.today(),RecordTypeId=PT_OpptyRecordTypeId,AccountId=a.Id,PT_parent_Opportunity__c=p1.Id,Type='1.NPD',Amount=1000,StageName='1.Target',CloseDate=system.today().addYears(2));
        insert o;        
    }
    
    private static testmethod void testDelete(){
        
        PT_Parent_Opportunity__c[] parentOpps = [Select Id From PT_Parent_Opportunity__c];
        
        test.startTest();

        delete parentOpps;

        test.stopTest();
    }
}