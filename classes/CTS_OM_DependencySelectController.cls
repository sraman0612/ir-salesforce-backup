/**
    @author Providity
    @date 30MAR2019
    @description Controller class for CTS_OM_DependencySelect.cmp
*/

public class CTS_OM_DependencySelectController {        
    
    @AuraEnabled
    public static Map<String,Map<String,List<String>>> populatePicklists(List<String> fields, String objectName) {
        Map<String,Map<String,List<String>>> depMap = new Map<String,Map<String,List<String>>>();
        for(Integer i=0;i<fields.size() - 1;i++) {
            depMap.put(i + '==' + fields[i] + '==' + fields[i + 1], CTS_OM_DependentPicklistUtil.getDependentOptionsImpl(objectName, fields[i], fields[i + 1]));
        }        
        return depMap;
    }    
    
    @AuraEnabled
    public static Map<String,List<String>> populateFirstLvlPicklists(List<String> fields, String objectName) {
        Map<String,List<String>> depMap = new Map<String,List<String>>();
        depMap.putAll(CTS_OM_DependentPicklistUtil.getDependentOptionsImpl(objectName, fields[0], fields[1]));
        return depMap;
    }
}