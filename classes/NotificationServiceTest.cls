@isTest
public with sharing class NotificationServiceTest {

    @testSetup
    private static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Notification_Settings__c notificationSettings = NotificationsTestDataService.createNotificationSettings();
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account shipTo1 = CTS_TestUtility.createAccount('Test Ship To1', false);

        Account distributor1 = CTS_TestUtility.createAccount('Test Distributor1', false);
        distributor1.RecordTypeId = NotificationsTestDataService.distributorRecordTypeId;

        insert new Account[]{shipTo1, distributor1};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', shipTo1.Id, assetRecTypeId, false); 
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', shipTo1.Id, assetRecTypeId, false); 
        asset2.IRIT_RMS_Flag__c = true;  
        insert new Asset[]{asset1, asset2};        

        Contact communityContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts@gmail.com', shipTo1.Id, false);
        communityContact.CTS_IOT_Community_Status__c = 'Approved';
        communityContact.MobilePhone = '555-555-5555';        

        Contact partnerCommContact = CTS_TestUtility.createContact('Contact678', 'Test', 'testcts3@gmail.com', distributor1.Id, false);
        partnerCommContact.MobilePhone = '555-555-5555';        

        insert new Contact[]{communityContact, partnerCommContact};

        Id profileId = [SELECT Id FROM Profile WHERE Name = :settings.Default_Standard_User_Profile_Name__c Limit 1].Id;
        Id airdDistributorPartnerProfileId = [Select Id From Profile Where Name = 'Customer Portal Super User'].Id;

        User communityUser = CTS_TestUtility.createUser(communityContact.Id, profileId, false);    
        communityUser.LastName = 'Comm_User_Test1';         
        
        User partnerCommUser = CTS_TestUtility.createUser(partnerCommContact.Id, airdDistributorPartnerProfileId, false);    
        partnerCommUser.LastName = 'Partner_Comm_User_Test1';

        insert new User[]{communityUser, partnerCommUser}; 

        Notification_Type__c notificationType1 = NotificationsTestDataService.createNotificationType(false, 'Warning Test', false, 'Warning',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test');

        Notification_Type__c notificationType2 = NotificationsTestDataService.createNotificationType(false, 'Trip Test', false, 'Trip',
            'CTS_RMS_Event__c', 'Event_Type__c', 'Asset', 'Asset__c', 'Event_Timestamp__c', 'RMS_Event__r', 'Account', 'AccountId', 'This is a test'); 

        insert new Notification_Type__c[]{notificationType1, notificationType2};

        Notification_Subscription__c subscription1 = NotificationsTestDataService.createSubscription(false, communityUser.Id, null, notificationType1.Id,
            'Daily;Real-Time', 'Email;SMS'); 

        Notification_Subscription__c subscription2 = NotificationsTestDataService.createSubscription(false, UserInfo.getUserId(), null, notificationType1.Id,
            'Daily;Real-Time', 'Email;SMS');     
            
        insert new Notification_Subscription__c[]{subscription1, subscription2};
    }
}
    @isTest
    private static void getOrgUrlBaseTest(){

        User customerUser = [Select Id, UserType From User Where LastName = 'Comm_User_Test1'];
        User partnerUser = [Select Id, UserType From User Where LastName = 'Partner_Comm_User_Test1'];

        String orgCommunityDomain = [Select Domain From Domain Where HttpsOption = 'CommunityAlt'].Domain;
        Network CUSTOMER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Customer Portal'];
        Network PARTNER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Global Partner Community'];

        Test.startTest();

        system.assert(NotificationService.getOrgUrlBase(customerUser).contains(orgCommunityDomain));
        system.assert(NotificationService.getOrgUrlBase(customerUser).contains(CUSTOMER_COMMUNITY.UrlPathPrefix));

        system.assert(NotificationService.getOrgUrlBase(partnerUser).contains(orgCommunityDomain));
        system.assert(NotificationService.getOrgUrlBase(partnerUser).contains(PARTNER_COMMUNITY.UrlPathPrefix));   
        
        User internalUser = [Select Id, Name, UserType From User Where Id = :UserInfo.getUserId()];
        system.assert(NotificationService.getOrgUrlBase(internalUser).contains(URL.getOrgDomainUrl().toExternalForm()));

        Test.stopTest();
    }

    @isTest
    private static void getRecordLinkURLTest(){

        Account shipTo1 = [Select Id, Name From Account Where Name = 'Test Ship To1'];
        User customerUser = [Select Id, Name, UserType From User Where LastName = 'Comm_User_Test1'];
        User partnerUser = [Select Id, Name, UserType From User Where LastName = 'Partner_Comm_User_Test1'];

        String orgCommunityDomain = [Select Domain From Domain Where HttpsOption = 'CommunityAlt'].Domain;
        Network CUSTOMER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Customer Portal'];
        Network PARTNER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Global Partner Community'];

        Test.startTest();

        system.assert(NotificationService.getRecordLinkURL(customerUser, shipTo1.Id).contains(orgCommunityDomain));
        system.assert(NotificationService.getRecordLinkURL(customerUser, shipTo1.Id).contains('/s/detail'));
        system.assert(NotificationService.getRecordLinkURL(customerUser, shipTo1.Id).contains(CUSTOMER_COMMUNITY.UrlPathPrefix));
        system.assert(NotificationService.getRecordLinkURL(customerUser, shipTo1.Id).endsWith('/' + shipTo1.Id));

        system.assert(NotificationService.getRecordLinkURL(partnerUser, shipTo1.Id).contains(orgCommunityDomain));
        system.assert(NotificationService.getRecordLinkURL(partnerUser, shipTo1.Id).contains('/s/detail'));
        system.assert(NotificationService.getRecordLinkURL(partnerUser, shipTo1.Id).contains(PARTNER_COMMUNITY.UrlPathPrefix));  
        system.assert(NotificationService.getRecordLinkURL(partnerUser, shipTo1.Id).endsWith('/' + shipTo1.Id)); 
        
        User internalUser = [Select Id, Name, UserType From User Where Id = :UserInfo.getUserId()];
        system.assert(NotificationService.getRecordLinkURL(internalUser, shipTo1.Id).contains(URL.getOrgDomainUrl().toExternalForm()));
        system.assert(!NotificationService.getRecordLinkURL(internalUser, shipTo1.Id).contains('/s/detail'));
        system.assert(NotificationService.getRecordLinkURL(internalUser, shipTo1.Id).endsWith('/' + shipTo1.Id)); 

        Test.stopTest();
    }    

    @isTest
    private static void getNotificationTypesTest(){

        Notification_Type__c[] notificationTypes = [Select Id, Name From Notification_Type__c];

        Test.startTest();

        system.assertEquals(notificationTypes.size(), NotificationService.getNotificationTypes().size());

        Test.stopTest();
    } 
    
    @isTest
    private static void getCurrentUserNotificationSubscriptionsTest1(){

        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];

        Test.startTest();

        System.runAs(communityUser){

            Notification_Subscription__c[] subscriptions = NotificationService.getCurrentUserNotificationSubscriptions(null);
            system.assertEquals(1, subscriptions.size());
        }

        Test.stopTest();
    }

    @isTest
    private static void getCurrentUserNotificationSubscriptionsTest2(){

        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Type__c notificationType = [Select Id, Name From Notification_Type__c LIMIT 1];
        Asset asset = [Select Id From Asset LIMIT 1];

        Notification_Subscription__c assetSubscription = NotificationsTestDataService.createSubscription(true, communityUser.Id, asset.Id, notificationType.Id,
            'Daily;Real-Time', 'Email;SMS');         

        Test.startTest();

        System.runAs(communityUser){

            Notification_Subscription__c[] subscriptions = NotificationService.getCurrentUserNotificationSubscriptions(asset.Id);
            system.assertEquals(1, subscriptions.size());
        }

        Test.stopTest();
    }    

    @isTest
    private static void getSubscriptionsByNotificationTypesTest(){

        Notification_Type__c[] notificationTypes = [Select Id, Name From Notification_Type__c];

        Test.startTest();

        system.assertEquals(1, NotificationService.getSubscriptionsByNotificationTypes(notificationTypes).size());

        Test.stopTest();
    }   
    
    @isTest
    private static void convertHtmlToPlainTextTest(){

        Test.startTest();

        String htmlString = '<html><div>this is the text</div></html>';
        String plainText = NotificationService.convertHtmlToPlainText(htmlString);
        system.assertEquals('this is the text', plainText);

        Test.stopTest();
    }    

    @isTest
    private static void unsubscribeUsersFromSMSTest(){

        User communityUser = [Select Id From User Where LastName = 'Comm_User_Test1'];
        Notification_Subscription__c[] communityUserSMSSubscriptions = [Select Id From Notification_Subscription__c Where OwnerId = :communityUser.Id and Delivery_Methods__c Includes ('SMS')];
        system.assertEquals(1, communityUserSMSSubscriptions.size());

        Test.startTest();

        NotificationService.unsubscribeUsersFromSMS(new Set<Id>{communityUser.Id});

        Test.stopTest();

        communityUserSMSSubscriptions = [Select Id From Notification_Subscription__c Where OwnerId = :communityUser.Id and Delivery_Methods__c Includes ('SMS')];
        system.assertEquals(0, communityUserSMSSubscriptions.size());        
    }    
    
    @isTest
    private static void getSubscriptionLevelTestGlobal(){

        Notification_Type__c warningType = [Select Id, Triggering_Object__c, Triggering_Object_Parent_Type__c, Parent_Object_Grandparent_Type__c
            From Notification_Type__c
            Where Name = 'Warning Test'];

        Notification_Subscription__c subscription = NotificationsTestDataService.createSubscription(true, UserInfo.getUserId(), null, warningType.Id, 'Daily;Real-Time', 'Email;SMS'); 
        
        Test.startTest();

        system.assertEquals('Global', NotificationService.getSubscriptionLevel(subscription, warningType));

        Test.stopTest();
    }  
    
    @isTest
    private static void getSubscriptionLevelTestRecord(){

        Asset asset = [Select Id From Asset LIMIT 1];
        CTS_RMS_Event__c rmsEvent = NotificationsTestDataService.createRmsEvent(true, asset.Id, 'Warning', 'This is a warning!');

        Notification_Type__c warningType = [Select Id, Triggering_Object__c, Triggering_Object_Parent_Type__c, Parent_Object_Grandparent_Type__c
            From Notification_Type__c
            Where Name = 'Warning Test'];

        Notification_Subscription__c subscription = NotificationsTestDataService.createSubscription(true, UserInfo.getUserId(), rmsEvent.Id, warningType.Id, 'Daily;Real-Time', 'Email;SMS'); 
        
        Test.startTest();

        system.assertEquals('Record', NotificationService.getSubscriptionLevel(subscription, warningType));

        Test.stopTest();
    }   
    
    @isTest
    private static void getSubscriptionLevelTestParent(){

        Asset asset = [Select Id From Asset LIMIT 1];

        Notification_Type__c warningType = [Select Id, Triggering_Object__c, Triggering_Object_Parent_Type__c, Parent_Object_Grandparent_Type__c
            From Notification_Type__c
            Where Name = 'Warning Test'];

        Notification_Subscription__c subscription = NotificationsTestDataService.createSubscription(true, UserInfo.getUserId(), asset.Id, warningType.Id, 'Daily;Real-Time', 'Email;SMS'); 
        
        Test.startTest();

        system.assertEquals('Parent', NotificationService.getSubscriptionLevel(subscription, warningType));

        Test.stopTest();
    }     

    @isTest
    private static void getSubscriptionLevelTestGrandparent(){

        Account acct = [Select Id From Account LIMIT 1];

        Notification_Type__c warningType = [Select Id, Triggering_Object__c, Triggering_Object_Parent_Type__c, Parent_Object_Grandparent_Type__c
            From Notification_Type__c
            Where Name = 'Warning Test'];

        Notification_Subscription__c subscription = NotificationsTestDataService.createSubscription(true, UserInfo.getUserId(), acct.Id, warningType.Id, 'Daily;Real-Time', 'Email;SMS'); 
        
        Test.startTest();

        system.assertEquals('Grandparent', NotificationService.getSubscriptionLevel(subscription, warningType));

        Test.stopTest();
    }     

    @isTest
    private static void logErrorTest(){

        Integer logCount = [Select Count() From Apex_Log__c];
        system.assertEquals(0, logCount);

        Test.startTest();

        try{
            throw new System.ListException('test');
        }
        catch(Exception e){
            NotificationService.logError(e, 'TestClass', 'TestMethod');
        }

        Test.stopTest();

        logCount = [Select Count() From Apex_Log__c];
        system.assertEquals(1, logCount);     
    }      
}