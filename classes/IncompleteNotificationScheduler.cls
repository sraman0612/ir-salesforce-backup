/**
 * Created by CloudShapers on 03/15/2021.
 */

global class IncompleteNotificationScheduler implements Schedulable {
    static final String ptRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();

    global void execute(SchedulableContext SC) {
        processOpportunities();
    }

    /*
* @Name	: processOpportunities
* @Description: Queries Opportunities with RFQ Incomplete and Send Email Reminders to the Owners
* @Date   	: 15-March-2021
* @Author 	: CloudShapers
*/
    public static void processOpportunities() {
        try {
            Map<String, Set<String>> ownerIdToOppString = new Map<String, Set<String>>();
            Map<String, String> ownerIdToEmailMap = new Map<String, String>();
            Map<Id, Opportunity> opportunities = new Map<Id, Opportunity>([
                    SELECT Id,Name,OwnerId,Owner.Email,Account.Name,
                            Description,PT_Project_Number__c,PT_Solution_Status_Age__c
                    FROM Opportunity
                    WHERE PT_Solution_Center__c = '3.RFQ incomplete'
                    AND PT_Solution_Status_Age__c >= 5
                    AND RecordTypeId = :ptRecordTypeId
            ]);

            if (opportunities.isEmpty())
                return;

            for (Opportunity oppRec : opportunities.values()) {
                if (!ownerIdToOppString.containsKey(oppRec.OwnerId))
                    ownerIdToOppString.put(oppRec.OwnerId, new Set<String>());
                ownerIdToOppString.get(oppRec.OwnerId).add(oppRec.Id + '∼' + createLink(oppRec));
                ownerIdToEmailMap.put(oppRec.OwnerId, oppRec.Owner.Email);
            }

            if (!ownerIdToEmailMap.isEmpty())
                sendEmail(ownerIdToOppString,
                        ownerIdToEmailMap,
                        opportunities);
        } catch (Exception er) {
            System.debug('Error in Processing Opportunity Records for Notifications' + er.getMessage());
        }
    }

    /*
* @Name	: createLink
* @Description: Creates Opportunity Links
* @Date   	: 15-March-2021
* @Author 	: CloudShapers
*/
    private static String createLink(Opportunity oppRec) {
        return ('<a href=\'' +
                URL.getSalesforceBaseUrl().toExternalForm() + '/' + oppRec.Id
                + '\'>' + oppRec.Name + '</a>');
    }

    /*
* @Name	: sendEmail
* @Description: Sends Reminder Emails to the Opportunity Owners
* @Date   	: 15-March-2021
* @Author 	: CloudShapers
*/
    private static void sendEmail(Map<String, Set<String>> ownerIdToOppString,
            Map<String, String> ownerIdToEmailMap,
            Map<Id, Opportunity> opportunityMap) {

        List<Messaging.SingleEmailMessage> mails =
                new List<Messaging.SingleEmailMessage>();

        for (String ownerId : ownerIdToOppString.keySet()) {
            Messaging.SingleEmailMessage mail =
                    new Messaging.SingleEmailMessage();

            List<String> sendTo = new List<String>();
            sendTo.add(ownerIdToEmailMap.get(ownerId));
            mail.setToAddresses(sendTo);

            mail.setSenderDisplayName('Solution Center Admin');

            mail.setSubject('RFQ Incomplete Reminders');
            String body = 'You have an opportunity that is RFQ incomplete. Click here to see the opportunity(s):<br/>';
            for (String oppName : ownerIdToOppString.get(ownerId)) {
                String oppId = (oppName.split('∼'))[0];
                String oppLink = (oppName.split('∼'))[1];
                body += oppLink + ': ' + '[' + opportunityMap.get(oppId).Account.Name + ']' + ' & ' +
                        'Project Number ' + '[' + opportunityMap.get(oppId).PT_Project_Number__c + ']' + ' & ' +
                        '- [' + opportunityMap.get(oppId).Description + ']' + ' & ' +
                        'waiting since [' + opportunityMap.get(oppId).PT_Solution_Status_Age__c+'] Days <br/>';
            }
            ownerIdToOppString.get(ownerId);
            mail.setHtmlBody(body);

            mails.add(mail);
        }

        if (!mails.isEmpty() && !Test.isRunningTest())
            Messaging.sendEmail(mails);
    }
}