/*************************************
 * CTS_ArticleSearchConfig
 * Contains methods related to custom setting and custom metadata
 **************************************/
public class CTS_ArticleSearchConfig {
    
    /**
     * get fields to add in SOSL query
     * @param objectNames
     * @return Map
     */
    public static Map<String, List<String>> getFields(List<String> objectNames, Boolean isSummary) {
        Map<String, List<String>> fieldMap = new Map<String, List<String>>();
        for(String objName : objectNames){
            fieldMap.put(objName, new List<String>());
        }
        String q = 'SELECT Field_API_Name__c,Object_Name__c from CTS_Article_Search_Field_Config__mdt where Object_Name__c IN: objectNames AND Active__c = true';
        if(isSummary) q += ' AND Details__c = false';
        for(CTS_Article_Search_Field_Config__mdt fieldConfig : Database.query(q) ){
            fieldMap.get(fieldConfig.Object_Name__c).add(fieldConfig.Field_API_Name__c);
        }
        return fieldMap;
    }        
    
    /**
     * get filter congifuration
     * @param objectNames, filterType
     * @return Map
     */
    public static Map<String, List<CTS_Article_Search_Filter_Config__mdt>> getFilterConfig(List<String> objectNames, String filterType){
        Map<String, List<CTS_Article_Search_Filter_Config__mdt>> filterMap = new Map<String, List<CTS_Article_Search_Filter_Config__mdt>>();
        for(String objName : objectNames){
            filterMap.put(objName, new List<CTS_Article_Search_Filter_Config__mdt>());
        }
        for(CTS_Article_Search_Filter_Config__mdt filterConfig : [select Data_Type__c,Filter_Type__c,filter_Values__c,Matching_Field__c,Object_Name__c,Operator__c,Filter_Customer_Profile_Field__c, Active__c from CTS_Article_Search_Filter_Config__mdt where Object_Name__c in :objectNames AND filter_Type__c = : filterType]){
            filterMap.get(filterConfig.Object_Name__c).add(filterConfig);
        }
        return filterMap;
    }
    
     /**
     * get Search config
     * @return Object
     */

    public static CTS_Article_Search_Config__c getSearchConfig(){
        CTS_Article_Search_Config__c searchConfig = CTS_Article_Search_Config__c.getInstance();
        if(searchConfig == null){
            searchConfig = new CTS_Article_Search_Config__c(Available_Options_In_The_Page_Size__c = '20,50,100', 
                                                            Default_Page_Size__c=30,Show_Search_Filter_Options__c = false,
                                                            Record_Type__c = 'IR Comp TechDirect Knowledge Article',
                                                            Vote_Stat_Min_Score__c = 2.5, Recent_Days_Count__c = 30,
                                                            Min_View_Count__c = 20
                                                           );
        }
        return searchConfig;        
    }
}