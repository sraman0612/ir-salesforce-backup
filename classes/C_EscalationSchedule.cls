public with sharing class C_EscalationSchedule implements Schedulable {
    
    /* Use this code to schedule the initial execution for this repeating job.

    System.schedule('Case Escalations', '0 0 * * * ?', new C_EscalationSchedule() );
    
    */
    
    
    public void execute (SchedulableContext sc) {
        String q = 'Select Id, isEscalated, Escalation_Count__c From Case Where isEscalated = true';
        Id batchInstanceId = Database.executeBatch(new C_EscalationBatch(q), 1); 
    }
    
}