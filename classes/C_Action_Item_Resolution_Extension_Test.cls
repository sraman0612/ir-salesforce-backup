@isTest
private class C_Action_Item_Resolution_Extension_Test {
    
    @testSetup
    static void setupData(){
        Account acct = new Account();
        acct.name = 'Canpango Test Account';
        acct.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert acct;
        
        Contact cont = new Contact();
        cont.firstName = 'Can';
        cont.lastName = 'Pango';
        cont.email = 'service-gdi@canpango.com';
        cont.Phone='9066784577';
        cont.Title='Mr';
        cont.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('CTS_EU_Contact').getRecordTypeId();
        insert cont;
        
        Id caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Customer_Service').getRecordTypeId();
        
        Case cse = new Case();
        cse.Account = acct;
        cse.Contact = cont;
        cse.Origin = 'Email';
        cse.Type = 'Question';
        cse.Subject = 'Test Case';
        cse.recordTypeId = caseRecordType;
        //insert cse;
        
        
        //User uu = new User();
        //uu.Username = 'Can'+'@test.com'; 
        //uu.ManagerId = '005j000000DL7xg';
        //insert uu;
        
        Case cse2 = new Case();
        cse2.Account = acct;
        cse2.Contact = cont;
        cse2.Subject = 'Test Case 2';
        cse2.Origin = 'Email';
        cse2.Type = 'Question';
        cse2.GDI_Department__c = 'Customer Service';
        cse2.Action_Item_Status_Rollup__c = 0;
        cse2.recordTypeId = caseRecordType;
        //insert cse2;

        insert new Case[]{cse, cse2};
        
        Attachment att = new Attachment();
        att.Name = 'Attachment Test';
        att.ParentId = cse.Id;
        att.Body = Blob.valueOf('VGVzdFN0cmluZw==');
        att.ContentType = 'image/jpg';
        insert att;
        
        ContentVersion cv = new ContentVersion();
        cv.ContentLocation = 'S';
        cv.PathOnClient = 'File Test';
        cv.Origin = 'H';
        cv.Title = 'File Test';
        cv.VersionData = blob.valueOf('Test content for file');
        insert cv;
        
        ContentVersion cv2 = new ContentVersion();
        cv2.ContentLocation = 'S';
        cv2.PathOnClient = 'File Test 2';
        cv2.Origin = 'H';
        cv2.Title = 'File Test 2';
        cv2.VersionData = blob.valueOf('Test content for file 2');
        insert cv2;
        
        Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv2.Id].ContentDocumentId;
        
        ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.ContentDocumentId = conDocId;
        cdl.LinkedEntityId = cse.Id;
        cdl.ShareType = 'V'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cdl.Visibility = 'AllUsers';
        //insert cdl;
        
        ContentDocumentLink cdl2 = new ContentDocumentLink();
        cdl2.ContentDocumentId = conDocId;
        cdl2.LinkedEntityId = cse2.Id;
        cdl2.ShareType = 'V'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
        cdl2.Visibility = 'AllUsers';
        //insert cdl2;
        
        insert new ContentDocumentLink[]{cdl, cdl2};
    }
    
    static testMethod void testEmails() {       
        Case c = [SELECT Id FROM Case WHERE Subject = 'Test Case'];
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Internal Action Item' LIMIT 1];
        
        Test.setCurrentPageReference(new PageReference('C_Action_Item_Resolution_Extension'));       
        System.currentPageReference().getParameters().put('ParentId', c.Id);
        System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
        System.currentPageReference().getParameters().put('OrgBody', 'test');
        System.currentPageReference().getParameters().put('Subject', 'test');
        System.currentPageReference().getParameters().put('isFollowUp', 'false');
        System.currentPageReference().getParameters().put('OriginatingId', c.Id);
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        C_Action_Item_Resolution_Extension  aie  = new C_Action_Item_Resolution_Extension (sc);
        
        Boolean hpf = aie.hasParentFiles;
        Contact con = [SELECT Id, Email FROM Contact];
        aie.theCase.ContactId = con.Id;
        aie.theCase.Contact = con;
        //Email_Field_Map__c efm = [SELECT Id FROM Email_Field_Map__c];
        //aie.theCase.Send_To_Queue__c = efm.Id;
        aie.editorContent = 'Test Editor Content';
        aie.AIeditorContent = 'Test';
        
        for(C_Action_Item_Resolution_Extension.FileWrapper fw : aie.filesWrapper){
            fw.include = true;
        }
        
        
        
        /*aie.AIeditorContent = 'Text<br/>Text<badhtmltag>';
aie.theCase.Subject = 'Testing Case';
hpf = aie.hasParentFiles;
aie.editorContent = 'Test Editor Content';

aie.save();
aie.cancel();*/
        
        Case c2 = [SELECT Id, Brand__c, Product_Category__c, Assigned_From_Address_Picklist__c FROM Case WHERE Subject = 'Test Case 2'];
        RecordType rt2 = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Internal Action Item' LIMIT 1];
        system.debug('c2 ====> '+c2);
        
        
        C_Action_Item_Resolution_Extension.saveImage('test','VGVzdFN0cmluZw==');
        
        
 
    Test.setCurrentPageReference(new PageReference('C_Action_Item_Resolution_Extension'));       
    System.currentPageReference().getParameters().put('ParentId', c2.Id);
    System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
    System.currentPageReference().getParameters().put('OrgBody', 'test');
    System.currentPageReference().getParameters().put('Subject', 'test');
    System.currentPageReference().getParameters().put('isFollowUp', 'true');
    System.currentPageReference().getParameters().put('OriginatingId', c2.Id);
    
    ApexPages.StandardController nsc = new ApexPages.StandardController(c);
    aie  = new C_Action_Item_Resolution_Extension (nsc);
    
    aie.theCase.ContactId = con.Id;
    aie.theCase.Contact = con;
    //aie.theCase.Send_To_Queue__c = efm.Id;
    aie.theCase.Brand__c = c2.Brand__c;
    aie.theCase.Product_Category__c = c2.Product_Category__c;
    aie.theCase.Assigned_From_Address_Picklist__c = c2.Assigned_From_Address_Picklist__c;
    
    for(C_Action_Item_Resolution_Extension.FileWrapper fw : aie.filesWrapper){
        fw.include = true;
    }
    
    aie.AIeditorContent = 'Text<br/>Text<badhtmltag>';
    aie.theCase.Subject = 'Testing Case';
    hpf = aie.hasParentFiles;
    aie.editorContent = 'Test Editor Content';
    
    ContentDocument cd = [select id, Title from ContentDocument limit 1];
    ContentDocument cd2 = [select id, Title from ContentDocument limit 1 offset 1];
    aie.filesWrapper = new List<C_Action_Item_Resolution_Extension.FileWrapper>();
    aie.filesWrapper.add(new C_Action_Item_Resolution_Extension.FileWrapper(cd, true, aie));
    aie.filesWrapper.add(new C_Action_Item_Resolution_Extension.FileWrapper(cd2, false, aie));
    aie.filesWrapper.add(new C_Action_Item_Resolution_Extension.FileWrapper(cd2, true, aie));
    
    aie.sendContent();
    aie.save();
    aie.cancel();
}

static testMethod void testFileCreation() {
    Case c = [SELECT Id, AccountId, ContactId FROM Case WHERE Subject = 'Test Case'];
    ContentVersion cv = [SELECT Id, ContentLocation, PathOnClient, Origin, Title, VersionData FROM ContentVersion WHERE Title = 'File Test'];
    RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Case' AND Name='Internal Action Item' LIMIT 1];
    
    Test.setCurrentPageReference(new PageReference('C_Action_Item_Resolution_Extension'));
    System.currentPageReference().getParameters().put('ParentId', c.Id);
    System.currentPageReference().getParameters().put('RecordTypeId', rt.Id);
    System.currentPageReference().getParameters().put('OrgBody', 'test');
    System.currentPageReference().getParameters().put('Subject', 'test');
    System.currentPageReference().getParameters().put('isFollowUp', 'false');
    System.currentPageReference().getParameters().put('OriginatingId', c.Id);
    
    
    
    ApexPages.StandardController sc = new ApexPages.StandardController(c);
    C_Action_Item_Resolution_Extension  aie  = new C_Action_Item_Resolution_Extension (sc);
    
    
    Contact con = [SELECT Id, Email FROM Contact];
    aie.theCase.ContactId = con.Id;
    aie.theCase.Contact = con;
    
    
    for(C_Action_Item_Resolution_Extension.FileWrapper wrapf : aie.filesWrapper){
        wrapf.include = true;
    }
    
    aie.fileID = cv.Id;
    aie.createNewFiles_Stage();
    aie.hasParentFiles = true;
    
}


}