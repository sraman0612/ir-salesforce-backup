/* 
 *  Snehal Chabukswar ||Date 31/01/2020 
 *  Called from Cts_CentacPartTable componet
 *
*/
public with sharing class CTS_CentacPartTableController {
    //Method to get data and column list for data table 
    //@param Centac Quote Id
    //@param boolean flag isUpdateTab to identify tab for which method had been called
    //@param boolean flag isInit to identify is it called for page load or page refresh
    //@return DataTableWrapper Object consisting of data and column list for data table 
    @AuraEnabled
    public static CTS_CentacPartTableController.DataTableWrapper init(String centacQuoteId,boolean isUpdateTab,boolean isInit){
        List<LabelDescriptionWrapper>labelList = new List<LabelDescriptionWrapper>(); 
        List<Centac_RFQ_to_Fill__c>partDataList = new List<Centac_RFQ_to_Fill__c>();
        String query = 'SELECT Id,';
        List<selectOptions>selectOptionsList; 
        /*Code to form query from custom metadata*/
        for(CTS_Centac_Requested_Part_Cloumn__mdt clm: [SELECT Field_Label__c,fieldType__c,API_Name__c,
                                                        CTS_Sequence_Number__c,isMandatory__c,Tab_Option__c 
                                                        FROM CTS_Centac_Requested_Part_Cloumn__mdt
                                                        ORDER BY CTS_Sequence_Number__c ASC]){
                                                           
                                                            selectOptionsList = new List<selectOptions>();
                                                            if(clm.fieldType__c == 'picklist'){
                                                                selectOptionsList = getPicklistValues('Centac_RFQ_to_Fill__c',clm.API_Name__c);
                                                            }
                                                            LabelDescriptionWrapper LabelDescriptionWrapperObj = new LabelDescriptionWrapper(clm.Field_Label__c,clm.API_Name__c,clm.fieldType__c,true,true,clm.isMandatory__c,true,selectOptionsList);                
                                                            if(isUpdateTab && clm.Tab_Option__c =='Update Parts Tab' ){  
                                                                labelList.add(LabelDescriptionWrapperObj);
                                                                query+=clm.API_Name__c+',';   
                                                            }else if(!isUpdateTab && clm.Tab_Option__c=='Add Parts Tab'){
                                                                labelList.add(LabelDescriptionWrapperObj);
                                                                query+=clm.API_Name__c+',';   
                                                            }
                                                       }
        query = query.removeEnd(',');
        query += ' ,Centac_Parts_Quote__c,Centac_Parts_Quote__r.listUpdateComplete__c FROM Centac_RFQ_to_Fill__c WHERE Centac_Parts_Quote__c=\''+centacQuoteId+'\''+'order by CreatedDate Asc';
        
        partDataList = Database.query(query);
        /*Instantiate Datatable wrapper Object consisting of data list and column List*/
        DataTableWrapper dataWrapperObj = new DataTableWrapper(partDataList,labelList);
        return dataWrapperObj;
    }
    
    /*Method to get List of selectOptions wrapper class object to populate picklist fields options*/
    @AuraEnabled        
    public static List<selectOptions> getPicklistValues(String objectAPIName, String fieldAPIName){
        List<selectOptions> selectOptionsList = new List<selectOptions>();
        Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectAPIName);
        Schema.DescribeSObjectResult descResult = convertToObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = descResult.fields.getMap().get(fieldAPIName).getDescribe();
        Boolean isFieldNotRequired = fieldResult.isNillable();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        selectOptions selectOptionObj; 
        selectOptionObj = new selectOptions('--None--', '');
        selectOptionsList.add(selectOptionObj);
        for(Schema.PicklistEntry pickListVal : ple){
            if(pickListVal.isActive()){
                selectOptionObj = new selectOptions(pickListVal.getLabel(), pickListVal.getValue());
            }
            selectOptionsList.add(selectOptionObj);    
        }
        return selectOptionsList;
    }
    
    /*Method to update records from datatable to salesforce
     * @param jsonString -Updated record in Json format
     * @Param centacQuoteId
     * @Param deleted records Id
     * @Param boolean flag isUpdateTab to identify tab for which method had been called
     * @return updated data table wrapper with datalist
	*/
    
    @AuraEnabled
    public static CTS_CentacPartTableController.DataTableWrapper updateRecords(String jsonString,String centacQuoteId,List<String>deletedRecordsId,boolean listUpdateComplete,boolean isUpdateTab){
        try{
            List<Centac_RFQ_to_Fill__c> partsToUpsertList = (List<Centac_RFQ_to_Fill__c>) JSON.deserialize(jsonString, List<Centac_RFQ_to_Fill__c>.class);
            List<Centac_RFQ_to_Fill__c>partsToBeupdated = new List<Centac_RFQ_to_Fill__c>();
            for(Centac_RFQ_to_Fill__c part:partsToUpsertList){
                system.debug('part------>'+part.id);
                system.debug('part------>'+part.Centac_Parts_Quote__c);
                if(part.Id == null){
                    part.Centac_Parts_Quote__c = centacQuoteId;
                   
                }
            }
            system.debug('listUpdateComplete----->'+listUpdateComplete);
            CTS_Centac_Parts_Quote_ReRate__c centacQuote = new CTS_Centac_Parts_Quote_ReRate__c(id=centacQuoteId,listUpdateComplete__c=listUpdateComplete);
            if(!partsToUpsertList.isEmpty()){
            	upsert partsToUpsertList;    
            }
            update centacQuote;
        }catch(Exception ex){
            throw new AuraHandledException('Error while upserting records:'+ex.getMessage());
        }
        if(!deletedRecordsId.isEmpty()){
           deletePartsRequest(deletedRecordsId); 
        }
        return init(centacQuoteId,isUpdateTab,false);
       
    }
    
    /*
     * Method  to delete record from salesforce
     * @param partIdList-deleted recordslist
     * 
	*/
    @AuraEnabled
    public static void deletePartsRequest(List<String>partIdList){
        try{
            List<Centac_RFQ_to_Fill__c>deletedPartsList = new List<Centac_RFQ_to_Fill__c>();
            for(String partId:partIdList){
               Centac_RFQ_to_Fill__c partToBeDeleted = new Centac_RFQ_to_Fill__c(id= partId);
               deletedPartsList.add(partToBeDeleted); 
            }
            delete deletedPartsList;
            
        }catch(Exception ex){
            throw new AuraHandledException('Error while deleting data:'+ex.getMessage());
        }
    }
    
    /*wrapper class for Data table data list and columns list*/
    public class DataTableWrapper{
        @AuraEnabled 
        public List<Centac_RFQ_to_Fill__c>dataList;
        @AuraEnabled
        public List<LabelDescriptionWrapper>labelList;
        /*constructor for DatatTablewrapper class*/
        public DataTableWrapper(List<Centac_RFQ_to_Fill__c> partDataList,List<LabelDescriptionWrapper> labelWrapperList){
            dataList = partDataList;
            labelList = labelWrapperList;
        }
        
    }
    /*wrapper class for columns list with different attributes*/
    public class LabelDescriptionWrapper {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String fieldName;
        @AuraEnabled
        public String type;
        @AuraEnabled
        public boolean sortable;
        @AuraEnabled
        public boolean editable;
        @AuraEnabled
        public Boolean mandatory;
        @AuraEnabled
        public Boolean resizable;
        /*attribute to mentioned picklist value option */
        @AuraEnabled
        public List<selectOptions>selectOptionsList;
        
        /*Constructor for LabelDescriptionWrapper wrapper class*/
        public LabelDescriptionWrapper(String labelTemp, String fieldNameTemp, 
                                       String typeTemp, boolean sortableTemp, 
                                       boolean isEditable,boolean isMandatory,
                                       boolean isResizable,
                                       List<selectOptions>selectOptions) {
                                           label      = labelTemp;
                                           fieldName = fieldNameTemp;
                                           type       = typeTemp;
                                           sortable  = sortableTemp;
                                           editable  = isEditable;
                                           mandatory = isMandatory;
                                           resizable = isResizable;
                                           selectOptionsList = selectOptions;                              
                                       }
    }
    /*Wrapper class to store picklist value and label as selectOption is not supported in Lightning component*/
    public class selectOptions{
        @AuraEnabled
        public String label;        
        @AuraEnabled
        public String value;
        /*Constructor of selectOptions wrapper class*/
        public selectOptions(String OptLabel,String optvalue){
            label = OptLabel;
            value = optvalue;
        }
        
        
    }
}