/***********************************************************************
 Class          : SmartAssetSearchExtension_Test
 Created Date   : 1 July 2015
 Descritption   : Test Class for SmartAssetSearchExtensions
 ************************************************************************/
@isTest
public class SmartAssetSearchExtensions_Test {
  
    @isTest
    static void testCSV(){
        Test.startTest();
        // Creating object to populate fieldset CSV string
        SmartAssetSearchExtensions controller = new SmartAssetSearchExtensions();
        
        //Assertion will fail if CSV string fields are null
            system.assert(controller.assetFieldCsv != null , 'Assertion Failed, Asset Field CSV is null');
        
            controller.assetNameToSearch = 'test';
            controller.assetSerialNumberToSearch = 'test321';
            controller.performSearch();
            controller.resetSearchStatus();
        //After calling resetSearchStatus() filterCondition should be blank
            system.assertEquals(controller.filterCondition, '', 'Assertion Failed, Filter condition not set to blank');
        
            ApexPages.StandardController std = new ApexPages.StandardController(new Asset(Name = 'Test_123 Asset'));
            controller = new SmartAssetSearchExtensions(std);
            controller.createNewAsset();
        
        Test.stopTest();
    }
    
}