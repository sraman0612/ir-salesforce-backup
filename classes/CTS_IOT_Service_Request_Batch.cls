public without sharing class CTS_IOT_Service_Request_Batch implements Schedulable, Database.Batchable<sObject>{
    
    // Schedulable implementation
    public void execute(SchedulableContext sc) {
        
        CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();

        // Check to see if the batch job is either queued or running, otherwise execute it as a fail safe
        if (communitySettings != null && communitySettings.Service_Request_Batch_Run_on_Completion__c){

            Integer batchJobRunCount = getBatchJobCount();

            if (batchJobRunCount == 0){
                Database.executeBatch(new CTS_IOT_Service_Request_Batch());
            }
        }
        // Execute the batch job only on the defined job schedule
        else{
            Database.executeBatch(new CTS_IOT_Service_Request_Batch());
        }        
    }

    // Batchable implementation
   	public Database.QueryLocator start(Database.BatchableContext BC){
        
        // Find all unprocessed service requests
        return Database.getQueryLocator(
            'Select Id, OwnerId, Type__c, Sub_Status__c, Sub_Area__c, Opened_Date__c, Severity__c, Problem_Description__c, Siebel_ID__c , ' + 
                'Asset__c, Asset__r.SerialNumber, Asset__r.Account.BillingStreet, Asset__r.Account.BillingCity, Asset__r.Account.BillingState, Asset__r.Account.BillingPostalCode, Asset__r.Account.BillingCountry, ' +
                'Contact__c, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, Contact__r.Phone, Contact__r.Title, ' +
                'Account__c, Account__r.Name ' +
            'From CTS_IOT_Service_Request__c ' + 
            'Where Status__c = \'Open\''
        );
   	}

    public void execute(Database.BatchableContext BC, List<CTS_IOT_Service_Request__c> serviceRequests){
   		
   		try{

            // Create leads from the service requests and mark the service request as processed
            Lead[] leads = new Lead[]{};

            DateTime migrationTime = DateTime.now();
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;              

            Map<Id, CTS_IOT_Service_Request__c> serviceRequestMap = new Map<Id, CTS_IOT_Service_Request__c>();
            Map<Id, Lead> serviceRequestLeadMap = new Map<Id, Lead>();            
            
            for (CTS_IOT_Service_Request__c sr : serviceRequests){

                sr.Status__c = 'Migrated';
                sr.Migrated_DateTime__c = migrationTime;

                serviceRequestMap.put(sr.Id, sr);

                Lead l = new Lead(
                    OwnerId = sr.OwnerId,
                    FirstName = sr.Contact__r.FirstName,
                    LastName = sr.Contact__r.LastName,
                    Company = sr.Account__r.Name,
                    Email = sr.Contact__r.Email,
                    Phone = sr.Contact__r.Phone,
                    Title = sr.Contact__r.Title,
                    Street = sr.Asset__r.Account.BillingStreet,
                    City = sr.Asset__r.Account.BillingCity,
                    State = sr.Asset__r.Account.BillingState,
                    PostalCode = sr.Asset__r.Account.BillingPostalCode,
                    Country = sr.Asset__r.Account.BillingCountry,                    
                    Product__c = sr.Asset__r.SerialNumber
                );

                l.Description = 
                    'Helix Service Request - ' + 
                    'Type: '+ sr.Type__c + '; ' +
                    'Serial Number: ' + sr.Asset__r.SerialNumber + '; ' + 
                    'Asset Siebel_ID__c: ' + sr.Siebel_ID__c + '; ' + 
                    'Severity: ' + sr.Severity__c + '; ' + 
                    'Service Request Description: ' + sr.Problem_Description__c;

                if (String.isBlank(l.Country)){
                    l.Country = 'USA';
                }

                l = CTS_IOT_Self_RegisterController.assignCommunitySettingsToNewLead(l, CTS_IOT_Community_Administration__c.getOrgDefaults(), true);

                // Make sure assignment rules fire
                l.setOptions(dmo);

                serviceRequestLeadMap.put(sr.Id, l);
            }

            system.debug('serviceRequestLeadMap: ' + serviceRequestLeadMap);    
                  
            insert serviceRequestLeadMap.values();

            for (Id srId : serviceRequestLeadMap.keyset()){

                CTS_IOT_Service_Request__c sr = serviceRequestMap.get(srId);
                Lead l = serviceRequestLeadMap.get(srId);
                
                system.debug('lead found to assign to service request: ' + l);
                sr.Lead__c  = l.Id;                
            }    

            update serviceRequests;            
        }
   		catch(Exception e){
   			system.debug('exception: ' + e.getMessage());
   			ApexLogHandler logHandler = new ApexLogHandler('CTS_IOT_Service_Request_Batch', 'execute', e.getMessage());
   			insert logHandler.logObj;
   		}           
    }        

	public void finish(Database.BatchableContext BC){
        
        CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();

        // Reschedule itself immediately if enabled and another batch job is not queued/running
        if (communitySettings != null && communitySettings.Service_Request_Batch_Run_on_Completion__c){        
            
            // Make sure the job is not already queued/running
            Integer batchJobRunCount = getBatchJobCount();

            system.debug('finish batchJobRunCount: ' + batchJobRunCount);

            if (batchJobRunCount == 0){

                Integer minFromNow = communitySettings.Service_Request_Batch_Run_Complete_Min__c != null ? Integer.valueOf(communitySettings.Service_Request_Batch_Run_Complete_Min__c) : 1;
                System.scheduleBatch(new CTS_IOT_Service_Request_Batch(), (Test.isRunningTest() ? 'Test_CTS_IOT_Service_Request_Batch_' : 'CTS_IOT_Service_Request_Batch_') + String.valueOf(System.currentTimeMillis()), minFromNow);
            }            
        }
    }   
       
    private static Integer getBatchJobCount(){

        String batchJobName = Test.isRunningTest() ? 'Test_CTS_IOT_Service_Request_Batch_%' : 'CTS_IOT_Service_Request_Batch_%';
         
        String query = 'Select Count() From CronTrigger ' + 
                       'Where CronJobDetail.Name Like :batchJobName and State != \'COMPLETE\'';

        return Database.countQuery(query);
    }
}