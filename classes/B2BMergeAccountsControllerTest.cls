@IsTest
private class B2BMergeAccountsControllerTest {

    @TestSetup
    static void testSetup() {
        UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
        User usr = CTS_TestUtility.createUser(false);
        usr.UserRoleId = r1.Id;
        usr.LastName = 'test user';
        usr.IsActive = true;
        insert usr;
        System.runAs(usr) {
            Account account = CTS_TestUtility.createAccount('Existing Account', false);
            account.OwnerId = usr.Id;
            account.LFS_account_type__c = 'Prospect';
            account.ShippingCountry = 'IT';
            account.ShippingCity = 'city';
            account.ShippingPostalCode = '123';
            account.ShippingStreet = 'street';
            account.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            insert account;

        }
    }

    @IsTest
    static void testMerge() {
        Account existingAccount = [SELECT Id, Name, Robuschi_B2B_Store_Payment_Option__c, IsBuyer, ShippingCountry, IsCustomerPortal FROM Account WHERE Name = 'Existing Account'];

        User usr = [SELECT Id from user where name ='Robuschi B2B Store Site Guest User' LIMIT 1];
        B2BSelfRegistrationController.RegistrationWrapper regData = B2BSelfRegistrationControllerTest.createTestRegistrationWrapper();
        String pageUrl;
        System.runAs(usr) {
            pageUrl = B2BSelfRegistrationController.registerUser(regData);
        }

        Account prospectAccount = [SELECT Id, Name, Robuschi_B2B_Store_Payment_Option__c, ShippingCountry FROM Account WHERE Name = 'Test Account'];

        WebStore webstore = B2BTestDataFactory.createWebStore('Test Store');
        insert webstore;

        WebCart testCart = new WebCart(Name = 'Cart', WebStoreId = webStore.Id, AccountId = prospectAccount.Id, Status = 'Active');
        insert testCart;

        Order orderRecord = B2BTestDataFactory.createOrder(prospectAccount.Id, 'Active', 'CAD', Date.today());
        insert orderRecord;

        Case aCase = new Case(AccountId = prospectAccount.Id, Status = 'Working');
        insert aCase;

        Test.startTest();
        B2BMergeAccountsController.mergeAccountsFromLwc(prospectAccount, existingAccount);
        Test.stopTest();

        existingAccount = [SELECT Id, Name, Robuschi_B2B_Store_Payment_Option__c, IsBuyer, ShippingCountry FROM Account WHERE Name = 'Existing Account'];
        System.assertEquals(true, existingAccount.IsBuyer, 'The existing account should be buyer enabled');
        System.assertEquals('Credit Card', existingAccount.Robuschi_B2B_Store_Payment_Option__c, 'The existing account Payment option should be Credit Card');

        // Query and validate ContactPointAddress data
        ContactPointAddress cpa = [SELECT Id, ParentId, AddressType FROM ContactPointAddress WHERE ParentId = :existingAccount.Id LIMIT 1];
        System.assertEquals(true, cpa <> null, 'ContactPointAddress should be related to existingAccount.');

        // Query and validate BuyerAccount data
        BuyerAccount buyerAcct = [SELECT Id, BuyerId FROM BuyerAccount WHERE BuyerId = :existingAccount.Id LIMIT 1];
        System.assertEquals(existingAccount.Id, buyerAcct.BuyerId, 'BuyerAccount should be linked to the existingAccount.');

        // Query and validate Contact data
        Contact contact = [SELECT Id, Email, AccountId FROM Contact WHERE Email = :regData.contactEmail LIMIT 1];
        System.assertEquals(existingAccount.Id, contact.AccountId, 'Contact should be linked to the existingAccount.');

        // Query and validate Order data
        Order order = [SELECT Id, AccountId, Status FROM Order WHERE AccountId = :existingAccount.Id LIMIT 1];
        System.assertEquals(existingAccount.Id, order.AccountId, 'Order should be linked to the existingAccount.');
        System.assertEquals('Active', order.Status, 'Order status should be Active.');

        // Query and validate WebCart data
        WebCart cart = [SELECT Id, AccountId FROM WebCart WHERE AccountId = :existingAccount.Id LIMIT 1];
        System.assertEquals(existingAccount.Id, cart.AccountId, 'Cart should be linked to the existingAccount.');

        // Query and validate Case data
        Case case1 = [SELECT Id, AccountId FROM Case WHERE AccountId = :existingAccount.Id LIMIT 1];
        System.assertEquals(existingAccount.Id, case1.AccountId, 'Case should be linked to the existingAccount.');
    }
}