@isTest
public with sharing class welch_TaxExemptUploaderControllerTest {
    @isTest
    static void testGetEntityRecordFiles(){
        
        //Contact testContact = welch_TestDataFactory.createContact(testAccount, true);
        //create user and give it an account
        //Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='B2B Customer Community Plus User' LIMIT 1];
        //User testUser = welch_TestDataFactory.createUser(testContact, communityUserProfile, true);

        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User sysAdminUser = new User(LastName = 'test user 1', 
                             Username = 'userName.1@example.gov', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US'); 
                             
        Test.startTest();
            System.runAs(sysAdminUser){
                Account testAccount = welch_TestDataFactory.createAccount(true);
                Contact testContact = welch_TestDataFactory.createContact(testAccount, true);
                
                ContentVersion contentVersion = new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Penguins'),
                    IsMajorVersion = true
                );
                insert contentVersion;
                ContentVersion pngContentVersion = new ContentVersion(
                    Title = 'Cows',
                    PathOnClient = 'Cows.png',
                    VersionData = Blob.valueOf('Test Cows'),
                    IsMajorVersion = true
                );
                insert pngContentVersion;  
                ContentVersion pdfContentVersion = new ContentVersion(
                    Title = 'Tigers',
                    PathOnClient = 'Tigers.pdf',
                    VersionData = Blob.valueOf('Test Tigers'),
                    IsMajorVersion = true
                );
                insert pdfContentVersion;  
                ContentVersion docxContentVersion = new ContentVersion(
                    Title = 'Lions',
                    PathOnClient = 'Lions.docx',
                    VersionData = Blob.valueOf('Test Lions'),
                    IsMajorVersion = true
                );
                insert docxContentVersion;
                ContentVersion csvContentVersion = new ContentVersion(
                    Title = 'Chickens',
                    PathOnClient = 'Chickens.csv',
                    VersionData = Blob.valueOf('Test Chickens'),
                    IsMajorVersion = true
                );
                insert csvContentVersion;  
                List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                //create ContentDocumentLink  record 
                List<ContentDocumentLink> conLinkList = new List<ContentDocumentLink>();
                for(ContentDocument conDoc : documents){
                    ContentDocumentLink cdl = New ContentDocumentLink();
                    cdl.LinkedEntityId = testAccount.id;
                    cdl.ContentDocumentId = conDoc.Id;
                    cdl.shareType = 'V';
                    conLinkList.add(cdl);
                    
                }
                insert conLinkList;
                
                Account acc = [SELECT Id, Name FROM Account WHERE Id=: conLinkList[0].LinkedEntityId];
                
                List<welch_TaxExemptUploaderController.File> files = welch_TaxExemptUploaderController.getEntityRecordFiles(testAccount.Id, false);
                System.assertNotEquals(0, files.size(), 'There were files returned');
            }
        Test.stopTest();
    }
    @isTest
    static void testContentVersionTriggerHandler(){
        
        //Contact testContact = welch_TestDataFactory.createContact(testAccount, true);
        //create user and give it an account
        //Profile communityUserProfile = [SELECT Id FROM Profile WHERE Name='B2B Customer Community Plus User' LIMIT 1];
        //User testUser = welch_TestDataFactory.createUser(testContact, communityUserProfile, true);

        Profile sysAdmin = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        User sysAdminUser = new User(LastName = 'test user 1', 
                             Username = 'userName.1@example.gov', 
                             Email = 'test.1@example.com', 
                             Alias = 'testu1', 
                             TimeZoneSidKey = 'GMT', 
                             LocaleSidKey = 'en_GB', 
                             EmailEncodingKey = 'ISO-8859-1', 
                             ProfileId = sysAdmin.Id, 
                             LanguageLocaleKey = 'en_US'); 
                             
        Test.startTest();
            System.runAs(sysAdminUser){
                Account testAccount = welch_TestDataFactory.createAccount(true);
                Contact testContact = welch_TestDataFactory.createContact(testAccount, true);
                
                List<ContentVersion> converList = new List<ContentVersion>();
                ContentVersion contentVersion = new ContentVersion(
                    Title = 'Penguins',
                    PathOnClient = 'Penguins.jpg',
                    VersionData = Blob.valueOf('Test Penguins'),
                    IsMajorVersion = true
                );
                insert contentVersion;
                List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                //create ContentDocumentLink  record 
                List<ContentDocumentLink> conLinkList = new List<ContentDocumentLink>();
                for(ContentDocument conDoc : documents){
                    ContentDocumentLink cdl = New ContentDocumentLink();
                    cdl.LinkedEntityId = testAccount.id;
                    cdl.ContentDocumentId = conDoc.Id;
                    cdl.shareType = 'V';
                    conLinkList.add(cdl);
                    
                    ContentVersion wmvContentVersion = new ContentVersion(
                        Title = 'Elephants',
                        PathOnClient = 'Elephants.wmv',
                        VersionData = Blob.valueOf('Test Elephants'),
                        IsMajorVersion = true,
                        ContentDocumentId = conDoc.Id
                    );
                    converList.add(wmvContentVersion);
                }
                insert conLinkList;
                
                Account acc = [SELECT Id, Name FROM Account WHERE Id=: conLinkList[0].LinkedEntityId];
                                
                insert converList;  
                
                List<welch_TaxExemptUploaderController.File> files = welch_TaxExemptUploaderController.getEntityRecordFiles(testAccount.Id, true);
                System.assertNotEquals(0, files.size(), 'There were files returned');
            }
        Test.stopTest();
    }
}