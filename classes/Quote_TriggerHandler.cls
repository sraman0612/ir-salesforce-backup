// 
// (c) 2015 Appirio, Inc.
//
//T-376328 Generate a Trigger handler Class for Quote Object
//
// Apr 4, 2015     George Acker     original
// April 16,2015    Surabhi Sharma    T-377640(modified)
// July 30, 2015    Surabhi Sharma    I-173544(modified)
// Aug 5, 2015      Surabhi Sharma    T-424219(modified)

public class Quote_TriggerHandler
{
    public static final string CLOSED_WON = 'Closed Won';   //sets the string CLOSED_WON with the value
   // public static final string CLOSED_WON_Reason = 'Promoted To Order';     //sets the sting CLOSED_WON_Reason with the value

    public static void beforeInsert(Quote__c[] newList)     //method defining new List  of quote Object
    {
        Set<Id> optys = new Set<Id>();    //set of  Id  declared  with new instance
        for (Quote__c q : newList)      // defines loop for the list  of quote object and  provide it a variable
        {
            optys.add(q.opportunity__c);    //adds the list of  quotes which are present in opportunity to a  set 
            if (q.promoted_to_order__c)     //checks  that if field of Quote  object named Promoted To Order is true
                q.primary__c = true;    // if above field  is true,  set Primary field  to true
        }
        
        //list defined to store all  the opportunities which have id = optys & that are present  in Quote  
        Quote__c[] existingQuotes = [SELECT opportunity__c FROM Quote__c WHERE Opportunity__c IN :optys];
        Set<Id> optysWithQuotes = new Set<Id>();    //defines  a  new  set of Id 
        for (Quote__c q :existingQuotes)    //list  of quote  object  which have  opportunity associated  with it
        {
            optysWithQuotes.add(q.opportunity__c);    
        }
        
        for (Quote__c q :newList)
        {
            if (!optysWithQuotes.contains(q.opportunity__c))        
                q.primary__c = true;                            //sets the checkbox field Primary to true.
        }
        
        //handleQuoteWithoutAmount(newList);
    }
    
    public static void afterInsert(Quote__c[] newList)        //method defining functionality once the quote is inserted 
    {
        Set<Id> optys = new Set<Id>();
        Set<Id> optysWithPromotedQuotes = new Set<Id>();    //set of Opportunities for which Quotes have Promoted field set to true
        for (Quote__c q : newList)
        {
            optys.add(q.opportunity__c);
            if (q.promoted_to_order__c)    //checks that field named 'Promoted to order' is true 
                optysWithPromotedQuotes.add(q.opportunity__c);    //list of all opportunities in which Quotes field named 'Promoted to Order' is true
        }
        handleOptysWithPromotedQuotes(optysWithPromotedQuotes);
        //  recalcOptys(optys); Dhilip commented as part of Case 00011332
    }
    
    public static void beforeUpdate(Quote__c[] newList, Map<Id,Quote__c> oldMap)
    {
        for (Quote__c q : newList)
        {
            if (q.promoted_to_order__c)
                q.primary__c = true;
        }
    }
    
    public static void afterUpdate(Quote__c[] newList, Map<Id,Quote__c> oldMap)
    {
        Set<Id> optys = new Set<Id>();
        Set<Id> optysWithPromotedQuotes = new Set<Id>();
        for (Quote__c q : newList)
        {
            if (q.Quote_SalesPrice__c != oldMap.get(q.Id).Quote_SalesPrice__c || q.primary__c != oldMap.get(q.Id).primary__c)
                optys.add(q.opportunity__c);
            if (q.promoted_to_order__c != oldMap.get(q.Id).promoted_to_order__c && q.promoted_to_order__c)
                optysWithPromotedQuotes.add(q.opportunity__c);
        }
        handleOptysWithPromotedQuotes(optysWithPromotedQuotes);
        //  recalcOptys(optys); Dhilip commented as part of Case 00011332
        UtilityClass.deleteApprovedRecords(newList,'Quote__c',UtilityClass.GENERIC_APPROVAL_API_NAME);    //record deleted once approved by approver
    }
    
    public static void afterDelete(Quote__c[] oldList)
    {
        Set<Id> optys = new Set<Id>();
        for (Quote__c q : oldList)
             optys.add(q.opportunity__c);
        //  recalcOptys(optys); Dhilip commented as part of Case 00011332
    }
    
    public static void handleOptysWithPromotedQuotes(Set<Id> optys)
    {
    //list of all the Quotes which have  Primary field set to true and 'Promoted Order' field set to false
        Quote__c[] quotesToUnmark = [SELECT primary__c FROM Quote__c WHERE opportunity__c IN :optys AND primary__c = true AND promoted_to_order__c = false];
        for (Quote__c q :quotesToUnmark)
        {
            q.primary__c = false;    //set 'Primary' to false
        }
        update quotesToUnmark;
        
        //list of Opportunities in which Promoted to Order set true and StageName is not 'Closed_Won'
        Opportunity[] optysToUpdate = [SELECT stagename, Closed_Lost_Reason__c, Amount  FROM opportunity WHERE Id IN :optys 
                                        AND stagename != :CLOSED_WON ];
        for (Opportunity o :optysToUpdate)
        {
            if (o.Amount == null) o.addError('You must have an amount defined on the Opportunity');     //add error to Quote if Amount is not present in Opportunity.
            o.stagename = CLOSED_WON;    //if Promoted Order is set true, StageName field is set to 'Closed Won'
            //o.Closed_Lost_Reason__c = CLOSED_WON_Reason;
        }
        update optysToUpdate;
    } 
    
    public static void recalcOptys(Set<Id> optysToCalc)
    {
        Opportunity[] optys = [SELECT amount FROM Opportunity WHERE Id IN :optysToCalc];     //Amount field being fetched from Opportunity
        
        //Opportunity'Quote Sales Price' fetched from Quote where Primary field set to true and fetched from Above Opportunity List
        Quote__c[] qs = [SELECT opportunity__c,Quote_SalesPrice__c FROM Quote__c WHERE primary__c = true AND Opportunity__c IN :optysToCalc];
        
        Map<Id,Decimal> optyAmounts = new Map<Id,Decimal>();    //All opportunities which have Amount in it
        for (Quote__c q :qs)
        {
            Decimal amt = q.Quote_SalesPrice__c;    //instance for sales Price field of Quote Object
            if (amt == null)
                amt = 0;
            if (optyAmounts.containsKey(q.opportunity__c))
                amt += optyAmounts.get(q.opportunity__c);
            optyAmounts.put(q.opportunity__c,amt);     //Amount field being updated with Sales Price field
        }
        
        for (Opportunity o : optys)
        {
            if (optyAmounts.containsKey(o.Id))    //if Opportunity contains Amount
                o.amount = optyAmounts.get(o.Id);    //Amount field gets updated
            else
                o.amount = 0;
        }
        
        update optys;    //Update Opportunity
    }
    
    //add error while creating Quotes whose Opportunity do not have Amount.
   /* private static void handleQuoteWithoutAmount(List<Quote__c> optys){
        Map<Id, List<Quote__c>> newQuotesCreated = new Map<Id, List<Quote__c>>();
        List<Opportunity> relatedOpp = new List<Opportunity>();
        
        for(Quote__c quote : optys){
            if(!newQuotesCreated.containsKey(quote.Opportunity__c)){
                newQuotesCreated.put(quote.Opportunity__c,new List<Quote__c>()); 
            }
            newQuotesCreated.get(quote.Opportunity__c).add(quote);
            
        }
        
        System.debug('newQuotesCreated >>' + newQuotesCreated);
        
        for(Opportunity opp : [SELECT Id, Name, Amount FROM Opportunity 
                               where id In :newQuotesCreated.keySet() AND Amount = Null ]){
                                
                for(Quote__c quote : newQuotesCreated.get(opp.Id) ){
                    quote.addError('You must have an amount defined on the Opportunity');
                    
                }               
            
        }
        
        //for(Opportunity relatedOpp = [SELECT Id, Name, Amount FROM Opportunity WHERE Id IN newQuotesCreated.key ]
    }   */ 
}