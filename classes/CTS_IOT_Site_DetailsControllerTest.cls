/**
 * Test class of CTS_IOT_Site_DetailsController
 **/
@isTest
private class CTS_IOT_Site_DetailsControllerTest {
    
    @testSetup
    static void setup(){
        Account acc = CTS_TestUtility.createAccount('Test Account', false);
        insert acc;
        CTS_IOT_Community_Administration__c setting = CTS_TestUtility.setDefaultSetting(true);
    }
    @isTest
    static void testGetInit(){
        List<Account> accounts = [select id from Account limit 1];
        if(!accounts.isEmpty()){
            Test.startTest();
            CTS_IOT_Site_DetailsController.InitResponse initRes = CTS_IOT_Site_DetailsController.getInit(accounts.get(0).Id);
            System.assert(initRes != null);
            System.assert(initRes.site != null);
            Test.stopTest();
        }
    }
}