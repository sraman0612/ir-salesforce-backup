@isTest
private class OpportunityLineExtensions_Test {
    
    static testMethod void testOpportunityLineExtensions() {
        
        Opportunity opp1 = new Opportunity();
        opp1.name = 'Test Opty1';
        opp1.stagename = 'Stage 1. Qualify';
        opp1.amount = 8000;
        opp1.closedate = system.today();
        opp1.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        insert opp1;
        
        
        Test.setCurrentPageReference(new PageReference('Page.Opportunity_Line_Item'));
        System.currentPageReference().getParameters().put('lkid', opp1.Id);
        system.currentpagereference().getparameters().put('index','01');
        
        Opportunity_Line_Item__c oppLine  = new  Opportunity_Line_Item__c();
         
        Apexpages.standardController stdCtrl  =  new Apexpages.standardController(oppLine);
        
        OpportunityLineExtensions oppLineExt = new OpportunityLineExtensions(stdCtrl);
        OpportunityLineExtensions oppLineExt2 = new OpportunityLineExtensions(stdCtrl);
        OpportunityLineExtensions oppLineExt3 = new OpportunityLineExtensions(stdCtrl);
      
        oppLineExt.oppLineItem[0].amount__c = 1000;
        oppLineExt.oppLineItem[0].quantity__c = 2;
        
        oppLineExt2.oppLineItem[0].amount__c = 10001;
        oppLineExt2.oppLineItem[0].quantity__c = 20;
        
        oppLineExt2.save();
        oppLineExt2.removeOppLineItem();
        
        oppLineExt.Save();
        
        Opportunity_Line_Item__c [] oppLineLst = [SELECT Id FROM Opportunity_Line_Item__c WHERE Opportunity__c =: opp1.Id];
        System.assert(false == oppLineLst.isEmpty());
        
        oppLineExt3.oppLineItem[0].amount__c = 10001;
        oppLineExt3.Save();
      
    
        
    }
    
    
    static testMethod void testOpportunityLineExtensions1() {
        Id NaAirOppRecordTypeID = [SELECT Id FROM RecordType WHERE sObjectType='Opportunity' and DeveloperName='CTS_EU'].Id;
        
        String errorMessage;
        
        Opportunity opp1 = new Opportunity();
        opp1.name = 'Test Opty1';
        opp1.stagename = 'Stage 3. Propose/Quote';
        opp1.amount = 9000;
        opp1.closedate = system.today();
        opp1.RecordTypeId = NaAirOppRecordTypeID;
        try{
        insert opp1;
        }Catch(DMLException e){
         System.assert( e.getMessage().contains('Insert failed. First exception on ' +
                'row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION'), 
                e.getMessage() );
              }
        
        Opportunity opp2 = new Opportunity();
        opp2.name = 'Test Opty2';
        opp2.stagename = 'Stage 1. Qualify';
        opp2.amount = 9000;
        opp2.closedate = system.today();
        opp2.RecordTypeId=NaAirOppRecordTypeID;
        
        insert opp2;
        
        Opportunity_Line_Item__c oppLine  = new  Opportunity_Line_Item__c();
        oppLine.amount__c = 1000;
        oppLine.quantity__c = 2;
        oppLine.Opportunity__c = opp2.id;
        try{
        opp2.stagename = 'Stage 3. Propose/Quote';
        update opp2;
        insert oppLine;
        }Catch(DMLException e){
         System.assert( e.getMessage().contains('first error: FIELD_CUSTOM_VALIDATION_EXCEPTION'), 
                e.getMessage() );
              }
    }
    
        
}