//
// Dhilip created this class to check for Recursive Triggers on 9th Nov 2016
//
//
//
public Class checkRecursive
{
    @TestVisible private static boolean run = true;
    public static boolean runOnce()
    {
        if(run)
        {
             run=false;
             return true;
        }
        else
        {
            return run;
        }
    }
}