/**
* Share case attachments with Community Users
* 
* */
public class ShareFilesWithCommunityUser_TrgrHandlr {
    
    public static void ShareFilesWithCommunityUsers(List<ContentDocumentLink> newCDLList){
        try{
            //get CTS RecordTypeIds
            List<Id> ctsCaseRecordTypeIdList = new List<Id>();
            ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Ask_a_Question').getRecordTypeId());
            ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Issue_Escalation').getRecordTypeId());
            ctsCaseRecordTypeIdList.add(Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Start_Up').getRecordTypeId());
            
            //get Case prefix i.e. 500
            Schema.DescribeSObjectResult r = Case.sObjectType.getDescribe();
            String keyPrefix = r.getKeyPrefix();
            
            //get attachments related to cases
            List<Id> caseIdList = new List<Id>();
            for(ContentDocumentLink cdlVar: newCDLList){
                if((String.valueOf(cdlVar.LinkedEntityId)).startsWith(keyPrefix)){
                    caseIdList.add(cdlVar.LinkedEntityId);
                }
            }
            
            //Added by MR Team to avoid SOQL LIMIT Exceed Exception - 05/02/2025
            if(caseIdList.size() > 0){
                //Link the CTSRecordtypes with Case
                Map<Id, Case> ctsCaseMap = new Map<Id, Case>([Select Id, RecordTypeId  From Case  Where Id IN: caseIdList AND RecordTypeId IN: ctsCaseRecordTypeIdList]);
                
                Set<Id> caseIdsList = ctsCaseMap.keySet();
                
                //Load the documents with defined ShareType and Visibility
                for(ContentDocumentLink cdl: newCDLList){
                    if(caseIdsList.contains(cdl.LinkedEntityId)){
                        cdl.ShareType = 'I';
                        cdl.Visibility = 'AllUsers';
                    }
                }
            }
            
        } catch(Exception ex){
            System.debug('##Exception occured while assigning topics to article: ' + ex.getMessage() + '\n in class \'ShareFilesWithCommunityUser_TrgrHandlr\' at line# '+ex.getLineNumber());
        }
    }
}