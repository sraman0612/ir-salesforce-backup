//
// Create a case from CTS Care Support Form VF Tab
//
//  18 Dec 2019  Snehal Chabukswar  Original
// 
//

public class CTS_CaseControllerExt {
    public Case caseObj{get;set;}
    public Transient Blob afile {get; set;}
    public Transient String contentType {get; set;}
    public Transient String fileName {get; set;}
    public boolean displayPopup {get; set;}
    public String caseNumber{get;set;}    
    static final String OPEN_STATUS = 'Open';
    static final String WEB_ORIGIN = 'Open';
    static final String CTS_CARE_QUEUE = 'CTS Care Support Queue';
    //static final String CTS_CARE_RECORDTYPE = 'CTS_CARE_Support';
    
    //Constructor
    //  @param stdController standard controller reference
    public CTS_CaseControllerExt(ApexPages.StandardController stdController){
        initiateCase((Case)stdController.getRecord());
    }
    
    //  Intialize Case attributes on page load
    //  @param newCase Case Object
    public void initiateCase(Case newCase){
        this.caseObj= newCase;
        caseObj.Status = OPEN_STATUS;
        caseObj.Origin = WEB_ORIGIN;
        //caseObj.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(CTS_CARE_RECORDTYPE).getRecordTypeId();        
    }
    
    //Method to Insert case called on submit button clicked 
    public void createCase() {
        try{
            List<Group>queueList=[SELECT Id 
                                  FROM Group 
                                  WHERE Type = 'Queue' AND 
                                  NAME =: CTS_CARE_QUEUE 
                                  limit 1];
            
            if(!queueList.isEmpty()){
                caseObj.ownerId = queueList[0].Id;
                insert(caseObj);
            }
            attachFileToCase(caseObj);
        }catch(System.DMLException e){
            ApexPages.addMessages(e);
        }
    }     
    
    //Method to attach file to newly created case
    //@param newly created case reference
    public void attachFileToCase(Case caseObj){
        try {
            if(afile!=null ){
                ContentVersion cv = new ContentVersion();
                cv.versionData = afile;
                cv.title = fileName;
                system.debug('contentType--------->'+contentType);
                cv.pathOnClient = fileName;
                insert cv;
                Map<Id,ContentVersion> contentIdObjMap = new Map<Id,ContentVersion>([SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id=:cv.Id]);
                ContentDocumentLink cdl = new ContentDocumentLink();
                if(!contentIdObjMap.isEmpty()){
                    cdl.ContentDocumentId = contentIdObjMap.get(cv.Id).ContentDocumentId;
                    cdl.LinkedEntityId = caseObj.Id;
                    cdl.ShareType = 'I';
                    insert cdl;
                }
            }
            Case newCaseObj = [SELECT caseNumber, 
                            Id 
                            FROM Case 
                            WHERE Id =:caseObj.Id 
                            limit 1];
            displayPopup = true;
            caseNumber = newCaseObj.CaseNumber;
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);            
        }
    }
    
    //Closes case creation acknowledgment modal popup and reset case on VF Page
    //@return page reference of current VF page
    public PageReference closePopup() {        
        displayPopup = false;    
        PageReference pg = new PageReference(System.currentPageReference().getURL());            
        pg.setRedirect(false);
        initiateCase(new Case());
        return pg;
        
        
    } 
}