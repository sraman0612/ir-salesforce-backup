@IsTest
private class FileUploadAtCheckoutControllerTest {
    @isTest
    static void testGetCartDetails() {
        Account testAccount = new Account(
                Name = 'Test Account',
                Robuschi_B2B_Store_Payment_Option__c = 'PO (credit)'
        );
        insert testAccount;

        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;

        WebCart testCart = new WebCart(
                Name='Cart',
                WebStoreId=webStore.Id,
                AccountId=testAccount.Id
        );
        insert testCart;

        Test.startTest();
        FileUploadAtCheckoutController.CartDetails result = FileUploadAtCheckoutController.getCartDetails(testCart.Id);
        Test.stopTest();

        System.assertNotEquals(null, result, 'Result should not be null');
        System.assertEquals(0, result.totalAmount, 'Amount should be 0');
        System.assertEquals('PO (credit)', result.paymentOption, 'Payment option should be "Credit Card"');
    }

    @IsTest
    static void uploadFileTest() {
        Test.startTest();
        try {
            FileUploadAtCheckoutController.uploadFile('test script', 'Test', null);
            System.assert(false, 'Expected AuraHandledException was not thrown.');
        } catch (AuraHandledException e) {
            System.assert(true, 'Expected AuraHandledException was thrown.');
        }
        Test.stopTest();
    }

    @IsTest
    static void uploadFileTestWithNullCardId() {
        String contentVersionId = '068XXXXXXXXXXXX'; // Use a valid dummy ID for testing
        Test.startTest();
        try {
            FileUploadAtCheckoutController.updateCart(null, contentVersionId);
            System.assert(false, 'Expected AuraHandledException was not thrown.');
        } catch (AuraHandledException e) {
            System.assert(true, 'Expected AuraHandledException was thrown.');
        }
        Test.stopTest();
    }

    @IsTest
    static void uploadFileTestWithEmptyCardId() {
        String contentVersionId = '068XXXXXXXXXXXX'; // Use a valid dummy ID for testing
        Test.startTest();
        try {
            FileUploadAtCheckoutController.updateCart('', contentVersionId);
            System.assert(false, 'Expected AuraHandledException was not thrown.');
        } catch (AuraHandledException e) {
            System.assert(true, 'Expected AuraHandledException was thrown.');
        }
        Test.stopTest();
    }

    @IsTest
    static void createContentLinkTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        ContentVersion contentVersion = FileUploadAtCheckoutController.createContentVersion('test script', 'Test');
        Test.startTest();
        FileUploadAtCheckoutController.createContentLink(contentVersion.Id, account.Id);
        Test.stopTest();
        ContentDocumentLink cdl = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :account.Id];
        List<ContentVersion> contentVersions = [ SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.assertEquals(cdl.ContentDocumentId, contentVersions[0].ContentDocumentId);

    }

    @IsTest
    static void updateCartTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        ContentVersion contentVersion = FileUploadAtCheckoutController.createContentVersion('test script', 'Test');
        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;
        Test.startTest();
        FileUploadAtCheckoutController.updateCart(cart.Id, contentVersion.Id);
        Test.stopTest();
        WebCart cart2 = [SELECT Id, Content_Document_Id__c FROM WebCart WHERE Id = :cart.Id];
        System.assertEquals(cart2.Content_Document_Id__c, contentVersion.Id);

    }

    @IsTest
    static void testCreateContentVersionDMLException() {
        String base64Content = ''; // This is an invalid base64
        String filename = 'InvalidFile.pdf';

        Test.startTest();
        try {
            FileUploadAtCheckoutController.createContentVersion(base64Content, filename);
            System.assert(false, 'Expected DMLException was not thrown.');
        } catch (AuraHandledException e) {
            System.assert(true, 'Expected DMLException was not thrown.');
        }
        Test.stopTest();
    }

    // Test to verify handling of invalid base64 encoding
    @IsTest
    static void testCreateContentVersionInvalidBase64() {
        String base64Content = 'InvalidBase64!';
        String filename = 'InvalidFile.pdf';

        Test.startTest();
        try {
            FileUploadAtCheckoutController.createContentVersion(base64Content, filename);
            System.assert(false, 'Expected Exception was not thrown.');
        } catch (Exception e) {
            System.assert(e.getMessage().contains('base64'), 'Expected exception related to base64 encoding.');
        }
        Test.stopTest();
    }

    // Test for generic Exception (if there are other unexpected issues)
    @IsTest
    static void testCreateContentVersionUnexpectedException() {
        String base64Content = 'ValidBase64Data';
        String filename = null; // filename is null, which will cause an exception.

        Test.startTest();
        try {
            FileUploadAtCheckoutController.createContentVersion(base64Content, filename);
            System.assert(false, 'Expected Exception was not thrown.');
        } catch (Exception e) {
            System.assert(true, 'Expected Exception was thrown.');
        }
        Test.stopTest();
    }
}