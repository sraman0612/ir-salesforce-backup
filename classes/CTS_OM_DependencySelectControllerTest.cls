/**
	@author Providity
	@date 5MAY2019
	@description Test class for CTS_OM_DependencySelectController.cls
*/

@isTest
private class CTS_OM_DependencySelectControllerTest {

    static testMethod void testDependency() {
        //call the method to get dependent values for CTS_OM_Category__c and CTS_OM_Category2__c
        Map<String,Map<String,List<String>>> depMap = CTS_OM_DependencySelectController.populatePicklists(new List<String>{'CTS_OM_Category__c', 'CTS_OM_Category2__c'}, 'Case');
        
        //call the method to get dependent values for CTS_OM_Category__c and CTS_OM_Disposition_Category__c
        Map<String,List<String>> depMap2 = CTS_OM_DependencySelectController.populateFirstLvlPicklists(new List<String>{'CTS_OM_Category__c', 'CTS_OM_Disposition_Category__c'}, 'Case');
    }
}