public with sharing class CC_Case_Trigger_Handler {
        
    private static Id transportRequestRecordTypeId = sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car - Transportation Request');
    private static Id ccCaseRecordTypeId = sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car');
    private static final String trDeliveryReason = 'Short Term Lease Delivery';
    private static final String trPickupReason = 'Short Term Lease Pick-up';

    @TestVisible
    private static Boolean getIsTriggerEnabled(){
        return Case_Settings__c.getOrgDefaults() != null ? Case_Settings__c.getOrgDefaults().CC_Case_Trigger_Handler_Enabled__c : false;    
    }
    
    public static void onBeforeInsert(List<Case> cases){
        
        if (getIsTriggerEnabled()){
            copyRGAInfoToTRcase(cases);
            getTRPickupInfoFromLease(cases);
        }
    }
    
    public static void onBeforeUpdate(List<Case> cases, Map<Id, Case> oldMap){
        
        if (getIsTriggerEnabled()){

        }
    }        

    public static void onAfterInsert(List<Case> cases){
        
        if (getIsTriggerEnabled()){
        	processTRPriceChanges(cases, null);
        	createTRPickupCases(cases, null);
        }
    }
    
    public static void onAfterUpdate(List<Case> cases, Map<Id, Case> oldMap){
        
        if (getIsTriggerEnabled()){        
        	processTRPriceChanges(cases, oldMap);
        	createTRPickupCases(cases, oldMap);
        }
    }
    
    // Copy over information to the new TR case that requires code (some due to cross-reference limits, some due to parent/child relationship)
    private static void copyRGAInfoToTRcase(List<Case> cases){
    	
    	system.debug('--copyRGAInfoToTRcase');
    	
    	try{
            
            // Filter the list of cases down to the ones that qualify
            Set<Id> parentCaseIds = new Set<Id>();

            for (Case c : cases){
                
                if (c.RecordTypeId == transportRequestRecordTypeId && c.ParentId != null){
                    parentCaseIds.add(c.ParentId);
                }
            }

            if (parentCaseIds.size() > 0){

                Map<Id, Case> parentCaseMap = new Map<Id, Case>([Select Id, RecordTypeId, Sub_Reason__c, CC_Order__c, (Select Serial_Numbers__c From CC_Case_Order_Items__r) From Case Where Id in :parentCaseIds]);

                Case[] casesToProcess = new Case[]{};

                for (Case c : cases){
                    
                    if (c.RecordTypeId == transportRequestRecordTypeId && c.ParentId != null){
                        
                        Case parentCase = parentCaseMap.get(c.ParentId);

                        if (parentCase != null && parentCase.RecordTypeId == ccCaseRecordTypeId && parentCase.Sub_Reason__c == 'RGA'){
                            casesToProcess.add(c);
                        }
                    }
                }

                system.debug('casesToProcess: ' + casesToProcess);

                if (casesToProcess.size() > 0){

                    // Collect information from orders
                    Set<Id> orderIds = new Set<Id>();
                    
                    for (Case c : casesToProcess){
                                        
                        Case parentCase = parentCaseMap.get(c.ParentId);

                        if (parentCase.CC_Order__c != null){
                            orderIds.add(parentCase.CC_Order__c);
                        }
                    }

                    Map<Id, CC_Order__c> orderMap = new Map<Id, CC_Order__c>();

                    if (orderIds.size() > 0){
                        orderMap = new Map<Id, CC_Order__c>([Select Id, Total_Invoice_Amount_With_Tax__c From CC_Order__c Where Id in :orderIds]);
                    }

                    for (Case c : casesToProcess){
                        
                        Case rgaCase = parentCaseMap.get(c.ParentId);
                                                
                        // Copy over order data from the order related to the rga case           
                        if (rgaCase.CC_Order__c != null){

                            CC_Order__c order = orderMap.get(rgaCase.CC_Order__c);
                            
                            system.debug('related order: ' + order);

                            if (order != null){
                                c.CC_Bill_Amount__c = order.Total_Invoice_Amount_With_Tax__c;
                            }
                        }

                        // Copy over data from case order items associated to the RGA case
                        if (rgaCase.CC_Case_Order_Items__r != null && rgaCase.CC_Case_Order_Items__r.size() > 0){
                            
                            String serialNumbers = '';

                            for (CC_Case_Order_Item__c caseOI : rgaCase.CC_Case_Order_Items__r){

                                if (String.isNotBlank(caseOI.Serial_Numbers__c)){

                                    serialNumbers += String.isBlank(serialNumbers) ? 'Serial Numbers: ' : ';';
                                    serialNumbers += caseOI.Serial_Numbers__c;
                                }
                            }

                            c.Description = serialNumbers;
                        }
                    }                    
                }
            }
    	}
        catch(Exception e){
        	apexLogHandler log = new apexLogHandler('CC_Case_Trigger_Handler', 'copyRGAInfoToTRcase', e.getMessage());
        	log.saveLog();
        }    	
    }      

    /* This is needed because we are out of relationships on case so it cannot be mapped in the quick action */
    private static void getTRPickupInfoFromLease(List<Case> cases){

        system.debug('--getTRPickupInfoFromLease');

        try{
    		
            Case[] casesToProcess = new Case[]{};
            Set<Id> leaseIds = new Set<Id>();
    		
	        for (Case c : cases){

                if (c.RecordTypeId == transportRequestRecordTypeId && c.CC_Short_Term_Lease__c != null && c.CC_Pickup_Name__c == null){
                    casesToProcess.add(c);
                    leaseIds.add(c.CC_Short_Term_Lease__c);
                }
            }

            system.debug('casesToProcess: ' + casesToProcess.size());

            if (casesToProcess.size() > 0){

                Map<Id, CC_Short_Term_Lease__c> leaseMap = new Map<Id, CC_Short_Term_Lease__c>(
                    [Select Id, Total_Number_of_Cars__c,  
                        Del_Sched_Location__r.Name, Del_Sched_Location__r.Primary_Contact_Name__c, Del_Sched_Location__r.Phone__c,
                        Del_Sched_Location__r.Address__c, Del_Sched_Location__r.City__c, Del_Sched_Location__r.State__c, Del_Sched_Location__r.Zip__c                        
                     From CC_Short_Term_Lease__c 
                     Where Id in :leaseIds]
                );

                for (Case c : casesToProcess){

                    CC_Short_Term_Lease__c lease = leaseMap.get(c.CC_Short_Term_Lease__c);

                    if (lease != null){

                        c.CC_Car_Quantity__c = lease.Total_Number_of_Cars__c;                        

                        if (lease.Del_Sched_Location__r != null){

                            c.CC_Pickup_Name__c = lease.Del_Sched_Location__r.Name;
                            c.CC_Pickup_Address__c = lease.Del_Sched_Location__r.Address__c;
                            c.CC_Pickup_City__c = lease.Del_Sched_Location__r.City__c;
                            c.CC_Pickup_State__c = lease.Del_Sched_Location__r.State__c;
                            c.CC_Pickup_Zip__c = lease.Del_Sched_Location__r.Zip__c;
                            c.CC_Pickup_Phone__c = lease.Del_Sched_Location__r.Phone__c;                        
                            c.CC_Pickup_Contact__c = lease.Del_Sched_Location__r.Primary_Contact_Name__c;
                        }
                    }
                }
            }
        }
        catch(Exception e){
        	apexLogHandler log = new apexLogHandler('CC_Case_Trigger_Handler', 'getTRPickupInfoFromLease', e.getMessage());
        	log.saveLog();
        }          
    }
    
    private static void createTRPickupCases(List<Case> cases, Map<Id, Case> oldMap){
    	
    	system.debug('--createTRPickupCases');
    	
    	try{
    		
    		Case[] casesToProcess = new Case[]{};
    		
	        for (Case c : cases){
	            
	            Case oldCase = oldMap != null ? oldMap.get(c.Id) : null;
	            
	            system.debug('new case: ' + c);
	            system.debug('old case: ' + oldCase);
	            
	            if (c.Create_Pickup_Case__c && (oldCase == null || c.Create_Pickup_Case__c != oldCase.Create_Pickup_Case__c)){
	            	casesToProcess.add(c);	            	
	            }
	        }    		
	        
	        system.debug('casesToProcess: ' + JSON.serialize(casesToProcess));
	        
	        if (casesToProcess.size() > 0){
	        	CC_CreateTRPickupFromDeliveryController.cloneDeliveryCases(JSON.serialize(casesToProcess));
	        }
    	}
        catch(Exception e){
        	apexLogHandler log = new apexLogHandler('CC_Case_Trigger_Handler', 'createTRPickupCases', e.getMessage());
        	log.saveLog();
        }    	
    }
    
    // Identify quoted price and bill amount changes on transportation requests and update appropriate dependencies (i.e. CC_Short_Term_Lease__c)
    private static void processTRPriceChanges(List<Case> cases, Map<Id, Case> oldMap){
        
        system.debug('--processTRPriceChanges');
        
        try{
        
            Set<Id> stlIds = new Set<Id>();
            Map<Id, Case> leaseDelvCaseMap = new Map<Id, Case>();
	        	        
	        for (Case c : cases){
	            
	            Case oldCase = oldMap != null ? oldMap.get(c.Id) : null;
	            
	            system.debug('new case: ' + c);
	            system.debug('old case: ' + oldCase);
	            
	            // Only process transportation request cases that have a short term lease assigned or changing
	            if (c.RecordTypeId == transportRequestRecordTypeId || (oldCase != null && oldCase.RecordTypeId == transportRequestRecordTypeId)){
                    
                    /* Pickup case update checks */

                    // Detect changes to the following fields for delivery cases: Sub_Reason__c, RecordTypeId, CC_Short_Term_Lease__c, CC_Quoted_Price__c
                    if (c.RecordTypeId == transportRequestRecordTypeId && c.CC_Short_Term_Lease__c != null && c.Sub_Reason__c == trDeliveryReason){

                        if ( (oldCase == null && (c.CC_Quoted_Price__c != null || c.CC_Quote_Date__c != null)) || 
                             (oldCase != null && (c.RecordTypeId != oldCase.RecordTypeId || c.Sub_Reason__c != oldCase.Sub_Reason__c || c.CC_Short_Term_Lease__c != oldCase.CC_Short_Term_Lease__c || c.CC_Quoted_Price__c != oldCase.CC_Quoted_Price__c))
                        ){
                            leaseDelvCaseMap.put(c.CC_Short_Term_Lease__c, c);
                        }
                    }

                    /* Lease update checks */

	                // Detect changes to the following fields: Sub_Reason__c, RecordTypeId, CC_Short_Term_Lease__c, CC_Quoted_Price__c, CC_Bill_Amount__c
	                
	                // New records
	                if (oldCase == null && c.Sub_Reason__c != null && c.CC_Short_Term_Lease__c != null && (c.CC_Quoted_Price__c != null || c.CC_Bill_Amount__c != null)){
	                    stlIds.add(c.CC_Short_Term_Lease__c);
	                }             
	                // Updated records
	                else if (oldCase != null && (c.RecordTypeId != oldCase.RecordTypeId || c.Sub_Reason__c != oldCase.Sub_Reason__c || c.CC_Short_Term_Lease__c != oldCase.CC_Short_Term_Lease__c || c.CC_Quoted_Price__c != oldCase.CC_Quoted_Price__c || c.CC_Bill_Amount__c != oldCase.CC_Bill_Amount__c)){
	                    
	                    if (c.CC_Short_Term_Lease__c != null){
	                        stlIds.add(c.CC_Short_Term_Lease__c);
	                    }
	                    
	                    if (oldCase.CC_Short_Term_Lease__c != null){
	                        stlIds.add(oldCase.CC_Short_Term_Lease__c);
	                    }                     
	                }              
	            }
	        }
            
            system.debug('leaseDelvCaseMap: ' + leaseDelvCaseMap);	        
            
            if (leaseDelvCaseMap.size() > 0){

                // Find any pickup cases for the same leases assigned to the updated delivery cases
                Case[] pickupCasesToUpdate = [Select Id, CC_Short_Term_Lease__c, CC_Quoted_Price__c, CC_Quote_Date__c 
                                              From Case 
                                              Where RecordTypeId = :transportRequestRecordTypeId and 
                                                    Sub_Reason__c = :trPickupReason and 
                                                    CC_Short_Term_Lease__c in :leaseDelvCaseMap.keySet()];

                // Go through each pickup case found and copy over the delivery quoted date and price if not assigned to the case yet
                for (Case c : pickupCasesToUpdate){

                    Case delvCase = leaseDelvCaseMap.get(c.CC_Short_Term_Lease__c);
                    
                    system.debug('delvCase found: ' + delvCase);

                    if (delvCase != null){
                        c.CC_Quoted_Price__c = delvCase.CC_Quoted_Price__c;
                        c.CC_Quote_Date__c = delvCase.CC_Quote_Date__c;
                    }
                }

                sObjectService.updateRecordsAndLogErrors(pickupCasesToUpdate, 'CC_Case_Trigger_Handler', 'processTRPriceChanges: delivery case updates');
            }

            system.debug('stlIds: ' + stlIds);

	        if (stlIds.size() > 0){
	            
	            // Build an aggregated total of the internal costs on all cases assigned to the short term leases based on the case sub reason (delivery/pickup)
	            AggregateResult[] aggResults = [Select CC_Short_Term_Lease__c, Sub_Reason__c/*, SUM(CC_Quoted_Price__c) Total_Quoted_Price*/, SUM(CC_Bill_Amount__c) Total_Internal_Cost 
	                                            From Case 
	                                            Where RecordTypeId = :transportRequestRecordTypeId and CC_Short_Term_Lease__c in :stlIds and CC_Quoted_Price__c != null 
	                                            Group By CC_Short_Term_Lease__c, Sub_Reason__c];
	        
	            system.debug('aggResults: ' + aggResults);
	            
	            // Get the short term lease records with their current transportation prices           
	        	Map<Id, CC_Short_Term_Lease__c> stlUpdates = new Map<Id, CC_Short_Term_Lease__c>([Select Id, /*Delivery_Quoted_Rate__c, Pickup_Quoted_Rate__c,*/ Delivery_Internal_Cost__c, Pickup_Internal_Cost__c From CC_Short_Term_Lease__c Where Id in :stlIds]);
	            Set<Id> stlIdsInitialized = new Set<Id>();
	            
	            for (AggregateResult aggResult : aggResults){
	                
	                Id stlId = (Id)aggResult.get('CC_Short_Term_Lease__c');
	                String caseReason = (String)aggResult.get('Sub_Reason__c');
	                //Double totalQuotedPrice = aggResult.get('Total_Quoted_Price') != null ? (Double)aggResult.get('Total_Quoted_Price') : 0;
	                Double totalInternalCost = aggResult.get('Total_Internal_Cost') != null ? (Double)aggResult.get('Total_Internal_Cost') : 0;
	
	                CC_Short_Term_Lease__c stlUpdate = stlUpdates.get(stlId);
	                
	                if (stlUpdate != null){
	                    
                        // Must reset each field to zero before the rollup is recalculated
                        // Lease quoted fields should not be overriden by transportation per Shirley (12/9/2020)
	                    if (!stlIdsInitialized.contains(stlId)){
	                        //stlUpdate.Delivery_Quoted_Rate__c = 0;
	                        //stlUpdate.Pickup_Quoted_Rate__c = 0;
	                        stlUpdate.Delivery_Internal_Cost__c = 0;
	                        stlUpdate.Pickup_Internal_Cost__c = 0;                        
	                        stlIdsInitialized.add(stlId);
	                    }
	                    
	                    if (caseReason == trDeliveryReason){
	                        //stlUpdate.Delivery_Quoted_Rate__c += totalQuotedPrice;
	                        stlUpdate.Delivery_Internal_Cost__c += totalInternalCost;
	                    }
	                    else if (caseReason == trPickupReason){
	                        //stlUpdate.Pickup_Quoted_Rate__c += totalQuotedPrice;
	                        stlUpdate.Pickup_Internal_Cost__c += totalInternalCost;
	                    }
	                    
	                    stlUpdates.put(stlId, stlUpdate); 
	                }
	            }
	            
                // sObjectService.updateRecordsAndLogErrors(stlUpdates.values(), 'CC_Case_Trigger_Handler', 'processTRPriceChanges: lease updates');
                // Switch to all-or-none update to enforce lease validation rules when the case updates occur
                update stlUpdates.values();
	        }
        }
        catch(Exception e){
        	apexLogHandler log = new apexLogHandler('CC_Case_Trigger_Handler', 'processTRPriceChanges', e.getMessage());
        	log.saveLog();
        }
    }
}