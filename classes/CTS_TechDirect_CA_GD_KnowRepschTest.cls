@isTest
public class CTS_TechDirect_CA_GD_KnowRepschTest {
    @testSetup 
    public static void loadData() {
        User u3 = [SELECT Id FROM User WHERE UserRole.Name LIKE 'CTS TechDirect%' and profile.name like '%Admin%' and isactive = true limit 1];
        System.runAs(u3) 
        {  
            List < Knowledge__kav > articleList = new List < Knowledge__kav > ();
            for(Integer i=1; i <= 30; i++) {
                Knowledge__kav knowledgeObj = new Knowledge__kav();
                knowledgeObj.RecordTypeId = '0120a0000011AllAAE';
                knowledgeObj.ValidationStatus = 'Private';
                knowledgeObj.Title = 'Test Article - ' + i;
                knowledgeObj.UrlName = 'Test' + i + String.valueOf(System.currentTimeMillis());
                knowledgeObj.Summary = 'Test Summary - ' + i;
                knowledgeObj.CTS_TechDirect_Article_Type__c = 'Tech Note';
                knowledgeObj.CTS_TechDirect_Question__c = 'Test Question - ' + i;
                knowledgeObj.CTS_TechDirect_Answer__c = 'Test Question - ' + i;
                knowledgeObj.CTS_TechDirect_Access_Level__c = 'Standard Products';
                knowledgeObj.CTS_TechDirect_Author_Reviewer__c = u3.Id;
                if(i < 10) {
                    knowledgeObj.IsVisibleInPkb=true;
                } else if(i > 10 && i < 20) {
                    knowledgeObj.IsVisibleInCsp=true;    
                } else {
                    knowledgeObj.IsVisibleInPrm =true;
                }
                knowledgeObj.language='en_US';
                articleList.add(knowledgeObj);
            }        
            insert articleList;
            for(Knowledge__kav knowledgeObj: [SELECT Id, KnowledgeArticleId FROM Knowledge__kav WHERE PublishStatus = 'Draft']) {
                KbManagement.PublishingService.publishArticle(knowledgeObj.KnowledgeArticleId, true);
            } 
        }  
        
    }
       @isTest
    static void testGetUserQuery() {
        Test.startTest();
        CTS_TechDirect_CA_GD_KnowRepsch schedule = new CTS_TechDirect_CA_GD_KnowRepsch();
        String resultQuery = schedule.getUserQuery();
        Test.stopTest();
        String expectedQuery = 'SELECT Id, Email FROM User WHERE UserRole.Name = \'CTS TechDirect NA - IR Support Agents\' AND isActive = true';
        System.assertEquals(expectedQuery, resultQuery, 'The generated query does not match the expected query');
    }
  
       
    
    @isTest
    public static void testExecute(){
        
        Test.startTest();
        User u3 = [SELECT Id FROM User WHERE UserRole.Name LIKE 'CTS TechDirect%' and isactive = true limit 1];
         
        
            CTS_TechDirect_CA_GD_KnowRepsch schedule = new CTS_TechDirect_CA_GD_KnowRepsch();
            schedule.execute(null);
            
            List < Knowledge__kav > articleList = new List < Knowledge__kav > ();
            for(Integer i=1; i <= 20; i++) {
                Knowledge__kav knowledgeObj = new Knowledge__kav();
                knowledgeObj.RecordTypeId = Schema.SObjectType.Knowledge__kav.getRecordTypeInfosByName().get('IR Comp TechDirect Knowledge Article').getRecordTypeId();
                knowledgeObj.ValidationStatus = 'Private';
                knowledgeObj.Title = 'Test Article - ' + i;
                knowledgeObj.UrlName = 'Test' + i + String.valueOf(System.currentTimeMillis());
                knowledgeObj.Summary = 'Test Summary - ' + i;
                knowledgeObj.CTS_TechDirect_Article_Type__c = 'Tech Note';
                knowledgeObj.CTS_TechDirect_Access_Level__c = 'Standard Products';
                knowledgeObj.IsVisibleInPkb=true;
                knowledgeObj.CTS_TechDirect_Author_Reviewer__c = u3.Id;
                if(i==1){
                    knowledgeObj.language='zh_CN';
                } else if(i == 2){
                    knowledgeObj.language='zh_TW';
                } else if(i == 3){
                    knowledgeObj.language='nl_NL';
                } else if(i == 4){
                    knowledgeObj.language='da';
                } else if(i == 5){
                    knowledgeObj.language='fi';
                } else if(i == 6){
                    knowledgeObj.language='fr';
                } else if(i == 7){
                    knowledgeObj.language='de';
                } else if(i == 8){
                    knowledgeObj.language='it';
                } else if(i == 9){
                    knowledgeObj.language='ja';
                } else if(i == 10){
                    knowledgeObj.language='ko';
                } else if(i == 11){
                    knowledgeObj.language='no';
                } else if(i == 12){
                    knowledgeObj.language='pt_BR';
                } else if(i == 13){
                    knowledgeObj.language='ru';
                } else if(i == 14){
                    knowledgeObj.language='es';
                } else if(i == 15){
                    knowledgeObj.language='es_MX';
                } else if(i == 16){
                    knowledgeObj.language='sv';
                } else if(i == 17){
                    knowledgeObj.language='th';
                    knowledgeObj.IsVisibleInCsp = false;
                } else if(i == 18){
                    knowledgeObj.language='ir';
                }            
                articleList.add(knowledgeObj);
            }
            
            for(Knowledge__kav articleObj: articleList){
                CA_GDKnowledgeReportComponentController.KnowledgeReportWrapper ctrlWrapper = 
                    new CA_GDKnowledgeReportComponentController.KnowledgeReportWrapper(articleObj);
                ctrlWrapper.getLanguageName();
                ctrlWrapper.getArticleURL();
            }
            
            Test.stopTest();
        
    }
}