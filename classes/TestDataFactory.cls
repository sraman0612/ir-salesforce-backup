@isTest
public with sharing class TestDataFactory {
	
	public static account createAccount(string accname,string recType){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
		Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
		return (new account(name=accname,recordtypeid=AccountRecordTypeInfo .get(recType).getRecordTypeId()));
	}

	public static account createAccountByDevName(string accname,string recTypeDevName){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
		Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(recTypeDevName).getRecordTypeId();
		return (new account(name=accname,recordtypeid=devRecordTypeId));
	}
	
	public static opportunity createOpportunity(string oppName,string recType,id accid,string wAssesment,string typeInput,string stage,string forcast){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.opportunity; 
		Map<String,Schema.RecordTypeInfo> opportunityRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
		return (new opportunity(name=oppName,recordtypeid=opportunityRecordTypeInfo .get(recType).getRecordTypeId(),Accountid=accid,Was_this_the_result_of_an_assessment__c=wAssesment,CloseDate=date.today(),Type=typeInput,StageName=stage,ForecastCategoryName=forcast));
	}
	
	public static contact createContact(string confName,string conlName,id accid,string recType,string phoneNo){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.contact; 
		Map<String,Schema.RecordTypeInfo> ContactRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Contact c = new contact(firstname=confName,lastname=conlName,accountid=accid,recordtypeid=ContactRecordTypeInfo.get(recType).getRecordTypeId(),phone=phoneNo);
        c.Email = c.Email != null ? c.Email : 'abc123@test.com';
        c.Title = c.Title != null ? c.Title : 'Test';
        c.Phone = c.Phone != null ? c.Phone : '555-555-5555';
		return(c);
	}
    
	//commented by CG as part of descoped object

	/*public static QuestionsList__c createQuestionList(string catagory,string tmp,string qL,id ownid){
		
		return(new QuestionsList__c(Category__c=catagory,Template__c=tmp,QuestionList_SalesCall__c=qL,ownerid=ownid));
	}*/
	
	public static Is_the_opp_real__c createisTheOppReal(id oppid){
		
		return(new Is_the_opp_real__c(Opportunity__c=oppid));
	}
	//commented by CG as part of descoped object
	/*public static Do_We_Have_a_Sol__c createDoWeHaveSoln(id oppid){
		
		return(new Do_We_Have_a_Sol__c(Opportunity__c=oppid));
	}
	
	public static Can_We_Win_the_Opp__c createCanWeWin(id oppid){
		
		return(new Can_We_Win_the_Opp__c(Opportunity__c=oppid));
	}*/
	public static Is_the_Value_There__c createisTheValueThere(id oppid){
		
		return(new Is_the_Value_There__c(Opportunity__c=oppid));
	}
	//commented by CG as part of descoped object
	/*public static Competitive_Anaysis__c createCompAnalysis(id oppid){
		
		return(new Competitive_Anaysis__c(Opportunity__c=oppid));
	}*/
    
}