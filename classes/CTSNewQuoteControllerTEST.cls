@isTest
public Class CTSNewQuoteControllerTEST{
  public testmethod static void test1(){
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for CPQ Embedded CTLR';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
    a.ShippingStreet = '13, street2';
    a.ShippingPostalCode = '123';  
    a.CTS_Global_Shipping_Address_1__c = '13';
    a.CTS_Global_Shipping_Address_2__c = 'street2';    
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
    insert a;
    Id airOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('NA_Air').getRecordTypeId();
    Opportunity o = new Opportunity();
    o.name = 'Test Opportunity';
    o.stagename = 'Qualify';
    o.amount = 1000000;
    o.closedate = system.today();
    o.RecordTypeId = airOppRTID;
    o.AccountId = a.Id;
    insert o;    
    cafsl__Oracle_Quote__c oq  = new cafsl__Oracle_Quote__c();
    oq.cafsl__Opportunity__c = o.Id;
    oq.Quote_Amount__c = 1;
    insert oq;
    Map<String, Object> jsonobj = new Map<String, Object>();
    jsonobj.put('pageNumber', '1');
    jsonobj.put('recordToDisplay', '5');
    jsonobj.put('objectType', 'cafsl__Oracle_Quote__c');
    jsonobj.put('objectType', 'cafsl__Oracle_Quote__c');
    jsonobj.put('parentRecordId', o.Id);
    jsonobj.put('parentField', 'cafsl__Opportunity__c');
    jsonobj.put('recordId', oq.Id);
    String infoJSON = JSON.serialize(jsonobj);
    Test.startTest();
    CTSNewQuoteController.setTriageSessionInfo(o.Id);
    CTSNewQuoteController.getTriageSessionInfo();
    CTSNewQuoteController.fetchData(infoJSON);
    CTSNewQuoteController.deleteRecord(infoJSON);
    Test.stopTest();
  }
}