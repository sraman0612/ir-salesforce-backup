@IsTest
public class CTS_TechDirectCustomerSupportTest {
    @testSetup 
    public static void loadData() {  
        Account accountObj = new Account(
            Name = 'End Customer Account - EMEIA',
            Description = 'Standard Products',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp TechDirect Account').getRecordTypeId()                
        );
        Database.insert(accountObj);
        System.assertNotEquals(accountObj, null);
        System.assertNotEquals(accountObj.Id, null);
        
        Contact contactObj = new Contact(
            FirstName = 'Test',
            Lastname = 'Contact',
            Phone = '1234546545',
            Email = 'testContact@test.com',
            AccountId = accountObj.Id,
            RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IR Comp TechDirect Contact').getRecordTypeId(),
            CTS_TechDirect_Support_Sub_Region__c = 'UAE',
            CTS_TechDirect_Support_Region__c = 'EMEIA'
        );
        Database.insert(contactObj);
        System.assertNotEquals(contactObj, null);
        System.assertNotEquals(contactObj.Id, null); 
        
        Product2 productObj = new Product2(
            Name = 'Test Product',
            Family = 'Product Family',
            isActive = true,
            RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('CTS TechDirect Product').getRecordTypeId() 
            
        );
        Database.insert(productObj);
        System.assertNotEquals(productObj, null);
        System.assertNotEquals(productObj.Id, null);
        
        Asset assetObj = new Asset(
            Name = 'Test Asset',
            SerialNumber = '123123',
            product2Id = productObj.Id,
            contactId = contactObj.Id,
            RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp TechDirect Asset').getRecordTypeId()            
        );
        Database.insert(assetObj);
        System.assertNotEquals(assetObj, null);
        System.assertNotEquals(assetObj.Id, null);

    }
    
    @isTest
    public static void testMethod1() {
        Test.startTest();
            Account accountObj = CTS_TechDirectCustomerSupportController.getAccountInfo('EMEIA');
            System.assertNotEquals(accountObj, null);   
        
            Contact contactObj = CTS_TechDirectCustomerSupportController.getContactInfo('testContact@test.com');
            System.assertNotEquals(contactObj, null);   
        
            Product2 productObj = CTS_TechDirectCustomerSupportController.getProductInfo('Test Product');
            System.assertNotEquals(productObj, null);
        
            Asset assetObj = CTS_TechDirectCustomerSupportController.getAssetInfo('Test Asset');
            System.assertNotEquals(assetObj, null);
        
            CTS_TechDirectCustomerSupportController.PicklistEntryWrapper wrapperObj = new CTS_TechDirectCustomerSupportController.PicklistEntryWrapper();
        
            List < CTS_TechDirectCustomerSupportController.PicklistEntryWrapper > picklistOptions = CTS_TechDirectCustomerSupportController.getPicklistValues('Case', 'Origin', 'Email');
            System.assertNotEquals(picklistOptions, null);
            System.assert(picklistOptions.size() > 0);
        Test.stopTest();
    }
    
    @isTest 
    public static void testMethod2() {
        Case caseObj = new Case(
            Subject = 'Test Subject',
            Description = 'Test Description',
            CTS_TechDirect_Secondary_Serial_Number__c = '132424'
        );
        
        Contact contactObj = new Contact(
            FirstName = 'First',
            LastName = 'Last',
            Phone = '1234234245',
            Email = 'testContact@test.com'
        );
        
        Test.startTest();
            Case createCaseObj = CTS_TechDirectCustomerSupportController.createCase('Test Asset', 'Test Product', caseObj, contactObj, 'EMEIA');
           // System.assertNotEquals(createCaseObj, null);
        
            // Updated region and asset name
            createCaseObj = CTS_TechDirectCustomerSupportController.createCase('Asset Test', 'Test Product', caseObj, contactObj, 'APAC');
           // System.assertNotEquals(createCaseObj, null);
        
            // Updated contact email id
            contactObj.Email = 'contactTest@test.com';
            createCaseObj = CTS_TechDirectCustomerSupportController.createCase('Asset Test', 'Test Product', caseObj, contactObj, 'EMEIA');
         //   System.assertNotEquals(createCaseObj, null);
          //  System.assertNotEquals(createCaseObj.Id, null);
        
            Id attachmentId = CTS_TechDirectCustomerSupportController.saveChunk(createCaseObj.Id, 'Case Attachment', 'Text File Content', 'text', '');  
          //  System.assertNotEquals(attachmentId, null);
        
            attachmentId = CTS_TechDirectCustomerSupportController.saveChunk(createCaseObj.Id, 'Case Attachment', 'Append File Content', 'text', String.valueOf(attachmentId)); 
          //  System.assertNotEquals(attachmentId, null);        
        Test.stopTest();
    }
}