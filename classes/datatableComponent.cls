public class datatableComponent {
    
    @AuraEnabled
    public static lightningTable getTableAndData(String sObjectName, List<String> fieldsToDisplay){
        
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(sObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Map<String,Schema.SObjectField> fieldsMap = DescribeSObjectResultObj.fields.getMap();
        Id userId = UserInfo.getUserId();
        String qryFieldLst = 'SELECT ';
        String referenceFields = '';
        String queueWhereClause ='';
        String caseOwnerWhereClause ='';
        tableColumns [] tableColumnsLst = new List<tableColumns>();
        lightningTable response = new lightningTable();
        Set<Id> otherCaseIds = new Set<Id>();
        Set<Id> queueIds = new Set<Id>();
        
        Map<String,String> dataTypeMap = new Map<String,String>();
        dataTypeMap.put('BOOLEAN','boolean');
        dataTypeMap.put('DATE','date');
        dataTypeMap.put('DATETIME','date-local');
        dataTypeMap.put('EMAIL','email');
        dataTypeMap.put('NUMBER','number');
        dataTypeMap.put('PHONE','phone');
        dataTypeMap.put('STRING','text');
        dataTypeMap.put('DOUBLE','number');
        dataTypeMap.put('MULTIPICKLIST','text');
        dataTypeMap.put('CURRENCY','currency');
        dataTypeMap.put('REFERENCE','text');
        dataTypeMap.put('PICKLIST','text');
        
        for(CaseTeamMember caseTeams : [SELECT Id, MemberId, ParentId FROM CaseTeamMember WHERE MemberId =: userId]){
            otherCaseIds.add(caseTeams.ParentId);
        }
        if(!otherCaseIds.isEmpty() && otherCaseIds != NULL){
            caseOwnerWhereClause = ' OR Id IN : otherCaseIds';
        }else{
            caseOwnerWhereClause = '';
        }
            
        for(GroupMember gmb: [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE UserOrGroupId =: userId AND Group.Type = 'Queue']){
            queueIds.add(gmb.GroupId);
        }
        if(!queueIds.isEmpty() && queueIds != NULL){
            queueWhereClause = ' OR OwnerId IN : queueIds';
        }else{
            caseOwnerWhereClause = '';
        }
        for(String field: fieldsToDisplay){
            String lField = field.toLowerCase();
            if(fieldsMap.containsKey(lField) && !lField.contains('.')){
                Schema.DisplayType dType = fieldsMap.get(lField).getDescribe().getType();
                String dataType = String.valueOf(dType);
                String fieldAPI = String.valueOf(fieldsMap.get(lField).getDescribe().getName());
                String fieldLabel = String.valueOf(fieldsMap.get(lField).getDescribe().getLabel());
                fieldLabel = fieldLabel.replace('Id','Name');
                dataType = dataTypeMap.get(dataType);
                tableColumns tblColumns;
                if(dType == Schema.DisplayType.REFERENCE){
                    String fieldAPIref = '';
                    if(!lField.contains('__c') && lField.endsWithIgnoreCase('Id')){                        
                        fieldAPIref = fieldAPI.removeEnd('Id')+'.Name';
                        fieldLabel fLabel = new fieldLabel(fieldAPIref);
                        typeAttributes typeAtt = new typeAttributes(fLabel, '_parent');
                        tblColumns = new tableColumns(fieldLabel, 
                                                         fieldAPIref,
                                                         'url',
                                                         typeAtt);
                    }else{
                        fieldAPIref = fieldAPI.removeEnd('__c')+'__r.Name';
                        fieldLabel fLabel = new fieldLabel(fieldAPIref);
                        typeAttributes typeAtt = new typeAttributes(fLabel, '_parent');
                        tblColumns = new tableColumns(fieldLabel,
                                                         fieldAPIref,
                                                         'url',
                                                         typeAtt);
                    }
                    qryFieldLst += fieldAPIref + ',';
                }else{
                    tblColumns = new tableColumns(fieldLabel, 
                                                 fieldAPI,
                                                 dataType, 
                                                 null);
                }
                tableColumnsLst.add(tblColumns);
                qryFieldLst += fieldAPI + ',';
            }

        }
        
        qryFieldLst = qryFieldLst.removeEnd(',') + ' FROM ' + sObjectName + ' WHERE isClosed=FALSE AND ( OwnerId = \'' + userId + '\'' + caseOwnerWhereClause + queueWhereClause +')';
        List<sObject> tableDataList = Database.query(qryFieldLst);
        response.tableDataLst = tableDataList;
        response.tableColumnsLst = tableColumnsLst;
        return response;
    }
}