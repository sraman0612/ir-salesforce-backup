@IsTest
private class B2BTermsAndConditionsControllerTest {
    @IsTest
    static void updateCartTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        WebStore webStore = new WebStore(Name='TestWebStore');
        insert webStore;
        WebCart cart = new WebCart(Name='Cart', WebStoreId=webStore.Id, AccountId=account.Id);
        insert cart;
        Test.startTest();
        B2BTermsAndConditionsController.updateCart(cart.Id);
        Test.stopTest();
        WebCart cart2 = [SELECT Id, Terms_and_conditions__c FROM WebCart WHERE Id = :cart.Id];
        System.assertEquals(cart2.Terms_and_conditions__c, true);

    }

    @IsTest
    static void updateCartFailTest() {
        Test.startTest();
        try {
            B2BTermsAndConditionsController.updateCart(null);
            System.assert(false);
        }
        catch (AuraHandledException ex) {
            System.assertEquals(ex.getMessage(), 'Script-thrown exception');
        }
        Test.stopTest();

    }

}