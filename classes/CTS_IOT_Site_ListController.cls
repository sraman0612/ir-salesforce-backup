public with sharing class CTS_IOT_Site_ListController {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public SiteDetail[] siteDetails = new SiteDetail[]{};
        @AuraEnabled public Boolean isSuperUser = CTS_IOT_Data_Service_WithoutSharing.getIsSuperUser(UserInfo.getUserId());
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public CTS_IOT_Community_Administration__c userSettings = CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId());
        @AuraEnabled public Integer totalRecordCount = 0;
    }

    public class SiteDetail{
        @AuraEnabled public Account site;
        @AuraEnabled public Boolean rmsEnabled = false;        
    }

    @AuraEnabled
	public static InitResponse getInit(String searchTerm, Integer pageSize, Integer offSet, Integer totalRecordCount){
		
		InitResponse response = new InitResponse();
		
		try{					

            if (pageSize == null){
                pageSize = response.userSettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.userSettings.User_Selected_List_Page_Size__c) : (response.communitySettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.communitySettings.User_Selected_List_Page_Size__c) : null);
            }            

            Account[] sites = CTS_IOT_Data_Service.getCommunityUserSites(null, searchTerm, pageSize, offSet, false);            
            Map<Id, Boolean> remoteMonitoringEnabledSiteMap = CTS_IOT_Data_Service.getRemoteMonitoringSiteMap(sites);
            SiteDetail[] siteDetails = new SiteDetail[]{};

            for (Account site : sites){

                SiteDetail siteDetail = new SiteDetail();
                siteDetail.site = site;
                siteDetail.rmsEnabled = remoteMonitoringEnabledSiteMap.get(site.Id);
                siteDetails.add(siteDetail);
            }

            response.siteDetails = siteDetails;
            response.totalRecordCount = CTS_IOT_Data_Service.getCommunityUserSitesCount(searchTerm);

            if (pageSize != null && (response.userSettings.User_Selected_List_Page_Size__c == null || pageSize != response.userSettings.User_Selected_List_Page_Size__c)){
                response.userSettings.User_Selected_List_Page_Size__c = pageSize;
                upsert response.userSettings;
            }
		}
		catch(Exception e){
			system.debug('exception: ' + e.getMessage());
			response.success = false;
			response.errorMessage = e.getMessage();
		}
		
		return response;
	}   
}