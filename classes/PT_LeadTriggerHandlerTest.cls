@isTest
public class PT_LeadTriggerHandlerTest {
  static testmethod void Test1(){
    User ptUsr = [SELECT Id, Name FROM User WHERE isActive=TRUE and Profile.Name LIKE 'PT_%' LIMIT 1];
    Id ptLeadQueueId = [SELECT Id FROM Group WHERE type='Queue' and Name='PT Leads' LIMIT 1].id;
    Id ptLeadRTId = [SELECT Id FROM RecordType WHERE sobjecttype='Lead' and Developername='PT_Lead'].Id;
    PT_Lead_Sales_Mapping__c lsm = New PT_Lead_Sales_Mapping__c(Country__c='Belgium', Sales__c = ptUsr.Name);
    insert lsm;
    Database.DMLOptions dmo = new Database.DMLOptions();
    dmo.assignmentRuleHeader.useDefaultRule= true;
    lead l   = new Lead(RecordTypeId=ptLeadRTId, FirstName='PTLead', LastName='Test', Company='EYE_R_SEE_OH', Country='Belgium');
    l.setOptions(dmo);
    insert l;
    Lead testlead = [SELECT OwnerId FROM Lead WHERE Id =:l.Id LIMIT 1];
    //system.assertNotEquals(ptLeadQueueId, testlead.OwnerId);
    //system.assertNotEquals(userinfo.getUserId(), testlead.OwnerId);
    //system.assertEquals(ptUsr.Id, testlead.OwnerId);
  }
}