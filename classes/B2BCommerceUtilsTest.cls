/**
 * @author           Amit Datta
 * @description      reusable Methods for B2B COmmerce.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  31/03/2024          Original Version
 **/
@isTest(SeeAllData=true) // Entities like WebStoreNetwork can not be created in test Context
public with sharing class B2BCommerceUtilsTest {
	@isTest
	static void resolveCommunityIdToWebstoreIdTest() {
		WebStoreNetwork wsn = [SELECT WebStoreId, NetworkId FROM WebStoreNetwork LIMIT 1];
		Test.startTest();
		String webStoreId = B2BCommerceUtils.resolveCommunityIdToWebstoreId('');
		Assert.isNull(webStoreId);
		webStoreId = B2BCommerceUtils.resolveCommunityIdToWebstoreId(wsn.NetworkId);
		Assert.areEqual(wsn.WebStoreId, webStoreId);
		Test.stopTest();
	}
	@isTest
	static void getAccountIdFromUserTest() {
		User adminUser = CTS_TestUtility.createUser(true);
		System.runAs(adminUser) {
			Test.startTest();
			B2BCommerceUtils.getAccountIdFromUser();
			Test.stopTest();
		}
	}
}