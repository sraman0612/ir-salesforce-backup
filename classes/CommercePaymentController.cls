public inherited sharing class CommercePaymentController {
    private ICommercePaymentApi paymentApi;
    // Constructor to inject the dependency
    public CommercePaymentController(ICommercePaymentApi paymentApi) {
        this.paymentApi = paymentApi;
    }

    public void capturePayment(String authorizationId) {
        PaymentAuthorization pa = [SELECT Id, OrderPaymentSummaryId, AccountId, Amount FROM PaymentAuthorization WHERE Id = :authorizationId];

        CommercePaymentApi.RequestWrapper input = new CommercePaymentApi.RequestWrapper();
        input.accountId = pa.AccountId;
        input.amount = pa.Amount;

        CommercePaymentApi.ResponseWrapper response = paymentApi.capture(input, pa.Id);

        Payment p = new Payment();
        p.Id = response.paymentId;
        p.OrderPaymentSummaryId = pa.OrderPaymentSummaryId;
        update p;
    }

    public void refundPayment(String paymentId) {
        Payment p = [SELECT Id, OrderPaymentSummaryId, AccountId, Amount FROM Payment WHERE Id = :paymentId];
        CommercePaymentApi.RequestWrapper input = new CommercePaymentApi.RequestWrapper();
        input.amount = p.Amount;
        CommercePaymentApi.ResponseWrapper response = paymentApi.refundPayment(input, p.Id);

        Refund r = new Refund();
        r.Id = response.refundId;
        r.OrderPaymentSummaryId = p.OrderPaymentSummaryId;
        update r;
    }

    public void reverseAuthorization(String authorizationId) {
        PaymentAuthorization pa = [SELECT Id, OrderPaymentSummaryId, AccountId, Amount, TotalAuthReversalAmount FROM PaymentAuthorization WHERE Id = :authorizationId];

        CommercePaymentApi.RequestWrapper input = new CommercePaymentApi.RequestWrapper();
        input.amount = pa.Amount;
        paymentApi.reverseAuthorization(input, pa.Id);
    }

    public void reverseAuthorizationFromOrderSummary(String orderPaymentSummaryId) {
        PaymentAuthorization pa = [
                SELECT Id, OrderPaymentSummaryId, AccountId, Amount, Status, PaymentMethod.PaymentMethodSubType, TotalAuthReversalAmount
                FROM PaymentAuthorization
                WHERE OrderPaymentSummary.OrderSummaryId = :orderPaymentSummaryId
                LIMIT 1
        ];
        CommercePaymentApi.RequestWrapper input = new CommercePaymentApi.RequestWrapper();
        input.amount = pa.Amount;
        paymentApi.reverseAuthorization(input, pa.Id);
    }

    public void refundPaymentWithInputs(String paymentId, Decimal amount, String comments) {
        Payment p = [SELECT Id, OrderPaymentSummaryId, AccountId, Amount FROM Payment WHERE Id = :paymentId];
        CommercePaymentApi.RequestWrapper input = new CommercePaymentApi.RequestWrapper();
        input.amount = amount;
        CommercePaymentApi.ResponseWrapper response = paymentApi.refundPayment(input, p.Id);

        Refund r = new Refund();
        r.Id = response.refundId;
        r.OrderPaymentSummaryId = p.OrderPaymentSummaryId;
        r.Comments = comments;
        new DmlHelper().doUpdate(r);
    }

    private without sharing class DmlHelper {
        public void doUpdate(SObject record) {
            update record;
        }
    }
}