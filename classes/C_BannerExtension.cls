//Used in the Visualforce page 'C_Case_Field_Recommendation_Banner'.  Allows user to stay on Case record page following save.

public class C_BannerExtension {

    ApexPages.StandardController stdController;
    private Case theCase;

    public C_BannerExtension(ApexPages.StandardController controller) {
        stdController = controller;
        this.theCase = (Case) stdController.getRecord();
        
        if(String.isEmpty(this.theCase.Assigned_From_Address_Picklist__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Important: New Case Notification will not be sent until ASSIGNED FROM ADDRESS PICKLIST is set.'));
        }
        
        if(String.isEmpty(this.theCase.Product_Category__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Important: New Case Notification will not be sent until PRODUCT CATEGORY is set.'));
        }
        
        if(this.theCase.Contact.RecordType.Name == 'Incomplete Contact'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Important: This Contact record is INCOMPLETE.  Please relate this Contact to an Account when possible.'));
        }
        
        if(this.theCase.Contact.Email == NULL && this.theCase.RecordType.Name != 'Internal Case' && this.theCase.RecordType.Name != 'Internal Case - Locked' ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Important: No Contact or Contact does not have an EMAIL.  New Case Notification will not be sent until a valid Contact with email.'));
        }
        
        if(this.theCase.Contact.IsEmailBounced == True){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Important: The email address associated to the Case Contact is not valid, please verify the email address is correct on the Contact Record.'));
        }
    }
    
    /*
      <apex:outputText value="Important: New Case Notification will not be sent until BRAND is set."          
          rendered="{!IF((ISBLANK(Case.Brand__c )), TRUE, FALSE)}"/>
      <apex:outputText value="Important: New Case Notification will not be sent until PRODUCT CATEGORY is set."          
          rendered="{!IF((ISBLANK(Case.Product_Category__c )), TRUE, FALSE)}"/>       
      <apex:outputText value="Important: This Contact record is INCOMPLETE.  Please relate this Contact to an Account when possible."          
          rendered="{!IF((ISBLANK(Case.Contact.Account) && Case.RecordType.Name != 'Internal Case' && Case.RecordType.Name != 'Internal Case - Locked'), TRUE, FALSE)}"/>      
    */

    public PageReference save() {
        try{
            stdController.save(); // calling standard save() method
        }
        catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
        }
        return null; // return 'null' to stay on same page    
    }
}