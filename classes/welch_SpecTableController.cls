public without sharing class welch_SpecTableController {
    @AuraEnabled
    public static List<Product2> getSpecData(String productId){
        List<Product2> allProds = new List<Product2>();
        Set<String> filledFields = new Set<String>();
        //ProductCategory category = new ProductCategory();
        // specificationData specsData = new specificationData();
        List<ProductCategoryProduct> productCategoryProducts = new List<ProductCategoryProduct>();
        List<ProductCategoryProduct> categoryProducts = new List<ProductCategoryProduct>();
        List<ProductCategoryProduct> finalCatProdList = new List<ProductCategoryProduct>();
        Product2 parentProd = new Product2();
        System.debug('PRODUCT ID :: ' + productId);
        Product2 prod = [
            SELECT Id, Name, ProductClass
            FROM Product2
            WHERE Id=: productId
        ];
        System.debug('PRODUCT :: ' + prod );
        // Product2 prod = [SELECT Id FROM Product2 WHERE Id = :productId];

        if(prod.ProductClass == 'Variation'){
            // find parent product
            ProductAttribute productAttributes = [
                SELECT Id, ProductId, VariantParentId
                FROM ProductAttribute
                WHERE ProductId =: productId
                LIMIT 1
            ];
            System.debug('ATTRIBUTE :: ' + productAttributes);
            parentProd = [
                SELECT Id, Name, LFS_welch_Free_Air_Displacement__c,
                LFS_welch_Maximum_Pressure__c, LFS_welch_Motor_Power_HP_kW_at_60_Hz__c, ProductClass,
                    StockKeepingUnit, Type, LFS_welch_Ultimate_Pressure__c, LFS_welch_Weight__c, LFS_welch_Oil_Capacity_L__c
                FROM Product2
                WHERE Id =: productAttributes.VariantParentId
                LIMIT 1
            ];
            System.debug('PARENT :: ' + parentProd);
        }
        if(prod.ProductClass == 'VariationParent' || prod.ProductClass == 'Simple'){
            productCategoryProducts = [
                SELECT Id, ProductCategoryId, Product.Name, ProductId,
                    ProductCategory.Name, ProductCategory.SortOrder
                FROM ProductCategoryProduct
                WHERE Product.Name != NULL
                AND IsPrimaryCategory = TRUE
                AND ProductId =: productId
            ];
            //use prod to find category?
            //add sku to list?
            //specsData.parentSku.add(prod.StockKeepingUnit);
        }else{
            productCategoryProducts = [
                SELECT Id, ProductCategoryId, Product.Name, ProductId,
                    ProductCategory.Name, ProductCategory.SortOrder
                FROM ProductCategoryProduct
                WHERE Product.Name != NULL
                AND IsPrimaryCategory = TRUE
                AND ProductId =: parentProd.Id
            ];
            //use product.name like%category.name%
            //user parentProd to find category?
            //find all products from category where they are 
            //add parentProd sku to list?
            //specsData.parentSku.add(parentProd.StockKeepingUnit);
        }
        //find category from list of product category products where sort order is null
        //should i loop through product category products?
        //how to know i am at base category
        //if(!productCategoryProducts.isEmpty()){
        //   category = [SELECT Id, Name FROM ProductCategory WHERE IsParentId =: productCategoryProducts[0].ProductCategoryId];
        //}
         
        //use id to find where parent category id is equal to id
        //is there a way to map id from list to list?
        //check if sort order null
        //then find all categories where parent category is that specific one?
        String categoryName = productCategoryProducts[0].ProductCategory.Name;
        categoryProducts = [
                SELECT Id, ProductCategoryId, ProductId, Product.Name, 
                    ProductCategory.Name, ProductCategory.SortOrder, Product.LFS_welch_115V_Order_Number__c,
                    Product.LFS_welch_Agency_National_Recognized_Lab__c, Product.LFS_welch_Built_in_Adjustable_Vacuum__c,
                    Product.LFS_welch_Diaphragm_Material__c, Product.LFS_welch_Dimensions_LxWxH_in_mm__c,
                    Product.LFS_welch_Free_Air_Displacement__c, Product.LFS_welch_Gas_Ballast__c, Product.LFS_welch_Gauge__c,
                    Product.LFS_welch_Maximum_Pressure__c, Product.LFS_welch_Motor_Power_HP_kW_at_60_Hz__c,
                    Product.LFS_welch_Motor_Speed_RPM_60Hz__c, Product.LFS_welch_Oil_Capacity_L__c, Product.StockKeepingUnit,
                    Product.LFS_welch_Tubing_Needed_I_D_in_mm__c, Product.LFS_welch_Ultimate_Pressure__c,
                    Product.LFS_welch_Vacuum_Regulator_Gauge__c, Product.LFS_welch_Weight__c, Product.LFS_welch_Inlet_Connection__c, Product.LFS_welch_Exhaust_Connection__c
                FROM ProductCategoryProduct
                WHERE Product.Name != NULL
                AND ProductCategoryId =: productCategoryProducts[0].ProductCategoryId
                AND IsPrimaryCategory = true
                AND Product.IsActive = true
        ];

        /**
         * Have list of product category product. Tied to Parent products.
         * Giving me all parent products for given category
         * Find a way to get all child product info? Or maybe 1 child product? to get the spec info
         * Should I do the group method?
         * I don't think that will work
         * if I had map of using variationParent to product attribute?
         * use for loop to put all parent skus in a list
         * then query for attributes in that list
         * then use map to group by that id?
         * get list of attributes that go to that particular sku. check if it is in there first
         */
        Set<String> prodIds = new Set<String>();
        for(ProductCategoryProduct catProd : categoryProducts){
            prodIds.add(catProd.ProductId);
        }
        //some fields will need to come from 
        //welch model
        List<ProductAttribute> prodAttributes = [
                SELECT ProductId, VariantParentId, Product.LFS_welch_115V_Order_Number__c, Product.Name, VariantParent.LFS_welch_115V_Order_Number__c,
                        Product.LFS_welch_Agency_National_Recognized_Lab__c, Product.LFS_welch_Built_in_Adjustable_Vacuum__c,
                        Product.LFS_welch_Diaphragm_Material__c, Product.LFS_welch_Dimensions_LxWxH_in_mm__c,
                        Product.LFS_welch_Free_Air_Displacement__c, Product.LFS_welch_Gas_Ballast__c, Product.LFS_welch_Gauge__c,
                        Product.LFS_welch_Maximum_Pressure__c, Product.LFS_welch_Motor_Power_HP_kW_at_60_Hz__c,
                        Product.LFS_welch_Motor_Speed_RPM_60Hz__c, Product.LFS_welch_Oil_Capacity_L__c, Product.StockKeepingUnit,
                        Product.LFS_welch_Tubing_Needed_I_D_in_mm__c, Product.LFS_welch_Ultimate_Pressure__c,
                        Product.LFS_welch_Vacuum_Regulator_Gauge__c, Product.LFS_welch_Weight__c, Product.LFS_welch_Inlet_Connection__c, Product.LFS_welch_Exhaust_Connection__c
                FROM ProductAttribute
                WHERE VariantParentId IN: prodIds
        ];

        //get field label.
        //build list of all field labels. (set)
        //create map to group products by their
        //find which item has the most fields. then use that to remove label from.

        // List<ProductAttribute> prodAttributes = [SELECT Id, ProductId, VariantParentId FROM ProductAttribute WHERE VariantParentId IN: prodIds];
        Map<Id, List<ProductAttribute>> mapOfAttributes = groupById(prodAttributes, ProductAttribute.VariantParentId);

        for(ProductCategoryProduct catProd : categoryProducts){
            if(mapOfAttributes.containsKey(catProd.ProductId)){
                // List<ProductAttribute> currentAttributes = mapOfAttributes.get(catProd.ProductId);
                // for(ProductAttribute childProd : currentAttributes){
                //     childProd.Product.StockKeepingUnit = catProd.Product.StockKeepingUnit;
                //     childProd.Product.welch_115V_Order_Number__c = catProd.Product.welch_115V_Order_Number__c; 
                //     allProds.add(childProd.Product);
                // }
                allProds.add(catProd.Product);
            }else{
                allProds.add(catProd.Product);
            }
        }
        return allProds;
    }

    /**
     * @description Group a list of SObjects by any field on the SObject
     * This version of the method supports Passing in a reference field and returning a Map using an Id as the key
     * @param items list of SObject
     * @param field Property to get a map by (Lookup, Master-Detail Field, text field which holds a SFID)
     * @return [description]
     */
    public static Map<Id, List<SObject>> groupById(List<SObject> listOfRecords, Schema.SObjectField filterField){
        Map<Id, List<SObject>> mapListSobjectById = new Map<Id, List<SObject>>();
        for(SObject record : listOfRecords){
            Id key = (Id)record.get(filterField);
            List<SObject> tempList;
            if((tempList = mapListSobjectById.get( key )) == null){
                mapListSobjectById.put( key, tempList = new List<SObject>());
            }
            tempList.add ( record );
        }
        return mapListSobjectById;
    }

    public class fieldNameData{
        @AuraEnabled
        public String label{get;set;} 
        @AuraEnabled
        public String apiName{get;set;}
    }
    
    @AuraEnabled
    public static List<fieldNameData> getFieldNameValues(List<Product2> prods){
        List<fieldNameData> fieldNamesList = new List<fieldNameData>();
        Set<String> nonEmptyFields = new Set<String>();
        List<fieldNameData> tempList = new List<fieldNameData>();
        // List<fieldNameData> fieldNamesList = new List<fieldNameData>();
        // Set<fieldNameData> nonEmptyFields = new Set<fieldNameData>();
        // List<fieldNameData> fieldNamesList = new List<fieldNameData>();
        // nonEmptyFields.addAll(tempList);
        // fieldNamesList.addAll(nonEmptyFields);
        for(Product2 p : prods){
            Map<String, Object> fieldsToValue = p.getPopulatedFieldsAsMap();
            for (String fieldName : fieldsToValue.keySet()){
                fieldNameData fieldData = new fieldNameData();
                String fieldLabel = (String)Schema.getGlobalDescribe().get('Product2').getDescribe().fields.getMap().get(fieldName).getDescribe().getLabel();
                //fieldname list contains label
                if(fieldName != 'Id' && !nonEmptyFields.contains(fieldLabel)){
                    fieldData.apiName = fieldName;
                    fieldData.label = fieldLabel;
                    fieldNamesList.add(fieldData);
                    nonEmptyFields.add(fieldLabel);
                    
                }
            }
        }
        return fieldNamesList;
    }
}