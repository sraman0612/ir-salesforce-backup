global class PT_DSMP_BAT01_createRecordsAccountDVP implements Schedulable, Database.Batchable<sObject>  {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Batch to create DVD records : PT_DSMP_BAT01_createRecordsAccountDVP
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 05-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

    String query;
    global String rtAccPTPowertools;
    global List<Account> lstAccount = new List<Account>();

    global PT_DSMP_BAT01_createRecordsAccountDVP(){
        defineParams(null);
    }

    global PT_DSMP_BAT01_createRecordsAccountDVP(List<Account> lstAccount){
        defineParams(lstAccount);
    }

    private void defineParams(List<Account> lstNewAccount){
        PT_Record_types__c ptrt = PT_Record_types__c.getOrgDefaults();
        rtAccPTPowertools = ptrt.Account_RT_Id__c;

        lstAccount = lstNewAccount;

        query = ' SELECT Id, '+
                ' PT_BO_Customer_Number__c, '+
                ' IRIT_Customer_Number__c, '+
                ' PT_Account_Segment__c '+
                ' FROM Account '+
                ' WHERE PT_BO_Customer_Number__c != null AND '+
                '       recordtypeid =: rtAccPTPowertools ';

        if(lstAccount != null && lstAccount.size() > 0)
            query += ' AND Id IN: lstAccount ';

    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> lstAccount) {
        System.debug('mgr execute lstAccount ' + lstAccount.size());

        try {

            //PT_DSMP_AP01_CreateYearlyRecords.createRecords(lstAccount, false, true);
            
            PT_DSMP_CS_Batch__c cs_Batch_config = PT_DSMP_CS_Batch__c.getOrgDefaults();
            Date scheduleDate = cs_Batch_config.Fiscal_Date__c;
            Integer dy = scheduleDate.day();
            Integer mon = scheduleDate.month();
            Boolean isNextYear = false;

            if(System.today().day() >= dy && System.today().month() > mon)
                isNextYear = true;

            PT_DSMP_AP01_CreateYearlyRecords.createRecords1(lstAccount, isNextYear);

        } catch(Exception e){
            System.debug('mgr Exception e ' + e);
        }
    }
    

    global void finish(Database.BatchableContext BC) {
        
    }
    
    // Method to schedule batch
    global void execute(SchedulableContext sc) {
        Database.executeBatch(new PT_DSMP_BAT01_createRecordsAccountDVP(), 20);
    }



}