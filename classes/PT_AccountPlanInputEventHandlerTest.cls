@isTest
public class PT_AccountPlanInputEventHandlerTest {

  static testmethod void test1(){
    PT_Record_types__c settings = PT_Record_types__c.getOrgDefaults();
    settings.Account_RT_Id__c = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();
    upsert settings PT_Record_types__c.Id;
    Account a = new Account();
    a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();
    a.Name = 'TestPTAcct1';
    a.PT_BO_Customer_Number__c = '12345';
    a.PT_Account_Segment__c = 'STAR';
    insert a;
    Account aTest1 = [Select PT_Is_DVP__c, PT_Owner__c FROM Account WHERE Id = :a.Id ];
    system.assertNotEquals('DVP', aTest1.PT_Is_DVP__c);
    system.assertEquals(null, aTest1.PT_Owner__c);
    PT_DSMP_Year__c y  =  PT_DSMP_TestFactory.createDSMPYear( String.valueOf( System.today().year() + 1 ));
    insert y;
    PT_DSMP__c ptdsmp = PT_DSMP_TestFactory.createDSMP('## test ' ,a.Id,y.Id);
    insert ptdsmp;
    ptdsmp.Stage__c='First Level';
    update ptdsmp;
    Account aTest2 = [Select PT_Is_DVP__c, PT_Owner__c FROM Account WHERE Id = :a.Id ];
    system.assertEquals('DVP', aTest2.PT_Is_DVP__c);
    system.assertEquals(UserInfo.getUserId(), aTest2.PT_Owner__c);
  }
  
}