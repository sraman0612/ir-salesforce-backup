@isTest
public class UserEventHandlerTest {
  
    @testSetup 
    private static void setupData() {

        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        try{
        psettingList = TestUtilityClass.createPavilionSettings();
   
        insert psettingList;
        }
        catch(exception e)
        {
            
            system.debug(e);
        }
    }

    private static testmethod void testAddAccTeamMember1(){

        Test.startTest();

        User u = createPartnerUser(99);
        u = [Select Id, AccountId From User Where Id = :u.Id];   
    //Commented as part of EMEIA cleaup
        // u.CC_CustomerVIEW__c = true;    
       // u.CC_Sales__c = false;
      // u.CC_Siebel_Id__c = '12345';
        update u;

        Test.stopTest();

        //system.assertEquals(1, [Select Count() From AccountTeamMember Where AccountId = :u.AccountId and UserId = :u.Id]);
    }
    
    private static testmethod void testAddAccTeamMember2(){

        Test.startTest();
//Commented as part of EMEIA cleaup
        User u = createPartnerUser(99);
        u = [Select Id, AccountId From User Where Id = :u.Id];   
       // u.CC_CustomerVIEW__c = false;    
        //u.CC_Sales__c = true;
        //u.CC_Siebel_Id__c = '12345';
        update u;

        Test.stopTest();

      //  system.assertEquals(1, [Select Count() From AccountTeamMember Where AccountId = :u.AccountId and UserId = :u.Id]);
    }   
    
    private static testmethod void testAddAccTeamMember3(){

        Test.startTest();
//Commented as part of EMEIA cleaup
        User u = createPartnerUser(99);
        u = [Select Id, AccountId From User Where Id = :u.Id];   
        //u.CC_CustomerVIEW__c = false;    
        //u.CC_Sales__c = false;
        //u.CC_Siebel_Id__c = '12345';
        update u;

        Test.stopTest();

        system.assertEquals(0, [Select Count() From AccountTeamMember Where AccountId = :u.AccountId and UserId = :u.Id]);
    }     

    private static testmethod void testInsertPublicGroupMembers1(){

        TestDataUtility testData = new TestDataUtility();
        Account acc = testData.createAccountName('test1' ,'IR Comp MEIA');
        insert acc;
 
        User[] usrlist =new User[]{}; 
        System.assertNotEquals(null, acc.Id);
        Contact con = TestUtilityClass.createContact(acc.Id);
        insert con; 

        System.assertEquals(acc.Id, con.AccountId);
        System.assertNotEquals(null, con.Id);

        Id p = [select id from profile where name='Standard User'].id;
        User u = TestUtilityClass.createNonPortalUser(p);
        //Commented as part of EMEIA cleaup
        // u.CC_View_Parts_Order_Invoice_Pricing__c = true;
        usrlist.add(u);      

        test.startTest();

        insert usrlist;

        List<Id> uids = new List<Id>();
        for(User u1 : usrlist){uids.add(u1.id);}
        update usrlist;

        UserEventHandler userHandler;
        UserEventHandler.sendNotificationEmail(true, u.Username, u.contactid);
        UserEventHandler.sendNotificationEmail(false, u.Username, u.contactid);
        //UserEventHandler.insertPublicGroupMembers(uids);
        test.stopTest();
    }
    
    private static testmethod void testInsertPublicGroupMembers2(){

        user u = createPartnerUser(20);

        system.runAs(u){

            UserEventHandler userHandler;
            UserEventHandler.sendNotificationEmail(true, u.Username, u.contactid);
            UserEventHandler.sendNotificationEmail(false, u.Username, u.contactid);
            //UserEventHandler.insertPublicGroupMembers(new List<Id>{u.id});
        }
    }
  /*Commented as part of EMEIA cleaup
    private static testmethod void testUpdateCases(){

        user u = createPartnerUser(20);
        Id ccCaseRTId = [SELECT Id FROM RecordType WHERE SObjectType='Case' and Developername='Club_Car_Case'].Id;
        Case c = new Case(Reason='User Admin', Sub_reason__c='New User',CC_User__c=u.Id, RecordTypeId=ccCaseRTId);

        system.runAs(u){
            insert c;
            Case caseBeforeUserUpdate = [SELECT CC_CorpId__c FROM Case WHERE Id =:c.Id];
            system.assertEquals(null, caseBeforeUserUpdate.CC_CorpId__c);
        }

        u.federationidentifier='ircotstone';

        test.startTest();
        update u;
        test.stopTest();

        Case caseAfterUserUpdate = [SELECT CC_CorpId__c FROM Case WHERE Id =:c.Id];
        system.assertEquals('ircotstone', caseAfterUserUpdate.CC_CorpId__c);
    }
    */
    
    public static User createPartnerUser(integer i){

        //Commented as part of EMEIA cleaup
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        Account acc =  new Account ();
        acc.Name = 'test account';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        //insert acc;
        Contact con = new Contact();
        con.Email = 'test@test.com';
        con.AccountId = acc.id;
        con.FirstName = 'test';
        con.LastName = 'test1';
        con.Title = 'Mr';
        con.Phone = '624574784';
        con.RecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
        //insert con;
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.ingersollrand.com'+i,
                                            Alias = 'batman',Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',Firstname='test', Lastname='Test',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/New_York',country='USA');

        Database.insert(portalAccountOwner1);
        user user1;

        System.runAs ( portalAccountOwner1 ) {

            Id clubcarRecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            //CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
            //insert county;

            TestDataUtility testData = new TestDataUtility();
            //CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
            //salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
            //insert salesRepacc;

            Account   account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,BillingCity ='Augusta', BillingCountry='USA', 
                                            BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566'/*CC_Billing_County__c = county.id,BillingState = 'GA',
                                            CC_Shipping_Billing_Address_Same__c=true, CC_Sales_Rep__c = salesRepacc.Id*/,Type = 'Distributor'/*,CC_Global_Region__c = 'United States'*/);

            Database.insert(account);

            contact contact = new Contact(FirstName = 'Test',Lastname = 'McTesty',Title = 'Title Test', Phone = '78787878787',AccountId = account.Id,Email = System.now().millisecond() + 'test@test.ingersollrand.com');
            contact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Customer Contact').getRecordTypeId();
            Database.insert(contact);

            Profile portalProfile = [SELECT Id FROM Profile where name ='CTS EU Partner User' LIMIT 1];

            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+i,ContactId = contact.Id,ProfileId = portalProfile.Id,
                            Alias = 'test123',Email = 'test12345@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'McTesty',CommunityNickname = 'test12345',
                            TimeZoneSidKey = 'America/New_York',country='USA',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',
                            /*CC_Pavilion_Navigation_Profile__c='US_CA_LA_AP_DLR_DIST__PRINCIPAL_GM_ADMIN',*/PS_Business_Unit_Description__c = 'CLUB CAR',CC_Department__c='Marketing'
                            /*CC_View_Service_Bulletins__c=true,CC_View_Parts_Pricing__c=true,CC_View_Sales_Pricing__c=true,CC_View_Sales_Bulletins__c=true,CC_Sales__c=true*/);
            
            Database.insert(user1);
        }
    
        return user1; 
    }
    
    //test method for PTL Partner flag update on Contact when user is created or updated.
 	private static testmethod void testPartnerFlagUpdateOnContact(){
        
        
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		String PTLAccountRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('PTL_Account').getRecordTypeId();
         User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,Username = System.now().millisecond() + 'test21@test.ingersollrand.com'+'21',
                                            Alias = 'ironman',Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',Firstname='Test', Lastname='Account Owner 1',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/New_York',country='US');
        
        Database.insert(portalAccountOwner1);
        System.debug('Portal Account Owner inserted');
        System.debug(portalAccountOwner1);
        Contact con;
        User u;
        String userId;
        System.runAs ( portalAccountOwner1 ) {
            Test.startTest();
          Account account = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=PTLAccountRecordType,ShippingCity ='TestCity', ShippingCountry='US', 
                                            ShippingStreet='2044 Forward Augusta Dr', ShippingPostalCode= '566',ShippingState = 'GA');
            
            Database.insert(account);
            account.isPartner = true;
			Database.update(account);
            System.debug(account);
            contact testContact = new Contact(FirstName = 'Test',Lastname = 'Test Partner',Title = 'Title Test', Phone = '78787878787',AccountId = account.Id,Email = System.now().millisecond() + 'test21@test.ingersollrand.com');
            testContact.recordtypeid = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PTL_Contact').getRecordTypeId();
            Database.insert(testContact);
			System.debug(testContact);
            Profile portalProfile = [SELECT Id FROM Profile where name ='Standard Partner' LIMIT 1];

            User user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com'+'21',ContactId = testContact.Id,ProfileId = portalProfile.Id,
                            Alias = 'test123',Email = 'test12345@test.ingersollrand.com',EmailEncodingKey = 'UTF-8',LastName = 'Test Partner',CommunityNickname = 'test12345',
                            TimeZoneSidKey = 'America/New_York',country='US',LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US',Business__c = 'Power Tools Americas');
            
            Database.insert(user1);
            System.debug('user1 info after insert: ' + user1);
            userId = user1.Id;
          Test.stopTest();
        
        }
        
        if(userId != null){
            u = [SELECT Id,ContactId FROM User WHERE Id =:userId LIMIT 1];
            con = [SELECT Id,is_PTL_Partner_User__c FROM Contact WHERE Id =:u.ContactId LIMIT 1];
          System.debug('contact\'s partner flag :'+ con.is_PTL_Partner_User__c );
         system.assertEquals(true, con.is_PTL_Partner_User__c);
            }

    }
}