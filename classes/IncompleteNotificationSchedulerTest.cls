/**
 * Created by CloudShapers on 3/16/2021.
 */

@IsTest
private class IncompleteNotificationSchedulerTest {
    @testSetup static void setup() {
        PTSolutionStatusVisibility__c cs = PTSolutionStatusVisibility__c.getOrgDefaults();
        cs.Solution_Center_User__c = [SELECT Id From User LIMIT 1].Id;
        upsert cs PTSolutionStatusVisibility__c.Id;
        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' and DeveloperName = 'PT_Powertools'].Id;
        Account a = new Account(
                RecordTypeId = ptAcctRTID,
                Name = 'TestAcc',
                Type = 'Customer',
                PT_Status__c = 'New',
                PT_IR_Territory__c = 'North',
                PT_IR_Region__c = 'EMEA'
        );
        insert a;
      /*  PT_Parent_Opportunity__c p = TestUtilityClass.createParentOpp(a.Id);
        p.Lead_SourcePL__c = 'Other';
        p.Type__c = '2.Solution';
        insert p;*/
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities = [
                SELECT PT_Solution_Status_Age__c,PT_Solution_Center__c
                FROM Opportunity
                LIMIT 5
        ];
        for (Opportunity oppRec : opportunities) {
            oppRec.PT_Solution_Center__c = '3.RFQ incomplete';
        }
        update opportunities;
    }
    @IsTest
    static void testProcessOpportunities() {
        Test.startTest();
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities = [
                SELECT PT_Solution_Status_Last_Changed__c,PT_Solution_Center__c
                FROM Opportunity
                LIMIT 5
        ];
        for (Opportunity oppRec : opportunities) {
            oppRec.PT_Solution_Status_Last_Changed__c = Datetime.now().addDays(-6);
        }
        update opportunities;
        IncompleteNotificationScheduler sh1 = new IncompleteNotificationScheduler();
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        Test.stopTest();
    }
}