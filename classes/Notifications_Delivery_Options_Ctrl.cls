public without sharing class Notifications_Delivery_Options_Ctrl {

    @TestVisible
    private class InitResponse extends LightningResponseBase{
        @AuraEnabled public Map<String, String> timeZoneOptions = new Map<String, String>();
    }

    @AuraEnabled(cacheable=true)
    public static InitResponse getInit(){

        InitResponse response = new InitResponse();
                
        for (Schema.PicklistEntry picklistEntry : Schema.SObjectType.User.fields.TimeZoneSidKey.picklistValues){
            response.timeZoneOptions.put(picklistEntry.getLabel(), picklistEntry.getValue());
        }

        return response;
    }

    @AuraEnabled
    public static LightningResponseBase updatePreferences(
        Id contactId, 
        Id userId,
        String deliveryWindowStart, 
        String deliveryWindowEnd, 
        String timezone, 
        String mobile, 
        String email){

        LightningResponseBase response = new LightningResponseBase();
                
        try{

            Integer startHour, startMinute, endHour, endMinute;
            
            if (deliveryWindowStart != null){

                String[] startWindowParts = deliveryWindowStart.split(':');
                startHour = Integer.valueOf(startWindowParts[0]);
                startMinute = Integer.valueOf(startWindowParts[1]);
            }

            if (deliveryWindowEnd != null){
                
                String[] endWindowParts = deliveryWindowEnd.split(':');
                endHour = Integer.valueOf(endWindowParts[0]);
                endMinute = Integer.valueOf(endWindowParts[1]);
            }            

            update new Contact(
                Id = contactId, 
                CTS_Notifications_Delivery_Window_Start__c = deliveryWindowStart != null ? Time.newInstance(startHour, startMinute, 0, 0) : null, 
                CTS_Notifications_Delivery_Window_End__c = deliveryWindowEnd != null ? Time.newInstance(endHour, endMinute, 0, 0) : null 
            );

            update new User(
                Id = userId, 
                TimeZoneSidKey = timezone, 
                MobilePhone = mobile, 
                Email = email
            );
        }
        catch(Exception e){
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }    
}