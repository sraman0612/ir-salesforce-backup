public class RecordTypeUtilityClass {
	static List<RecordType> recordTypeList = new List<RecordType>();
    public static List<BusinessHours> businessHoursList = new List<BusinessHours>();
    public static BusinessHours defaultBusinessHour{get;set;}
    static{
        recordTypeList = [SELECT Id,Name,SobjectType,DeveloperName FROM RecordType];
        businessHoursList = [SELECT Id,Name, IsDefault, IsActive FROM BusinessHours where IsActive= true ];
        for(BusinessHours bhs:businessHoursList){
            if(bhs.IsDefault == true){
               defaultBusinessHour = bhs;
            }
        }
    }
    public Static List<RecordType> getRecordTypeList(String requestedSObject){
        List<RecordType> sObjectRecordTypeList = new List<RecordType>();
        for(RecordType rt:recordTypeList){
            if(rt.SobjectType==requestedSObject){
                sObjectRecordTypeList.add(rt);
            }
        }
        if(!sObjectRecordTypeList.isEmpty()){
            return sObjectRecordTypeList;
        }else{
            return null;
        }
    }
    

}