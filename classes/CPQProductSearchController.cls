public class CPQProductSearchController {

  public string canvasParams{get;set;}
  public boolean isEmbedded{get;set;}
  public boolean hasError{get;set;}
  public cafsl__Oracle_Quote__c quote{get;set;}
  
  public CPQProductSearchController (ApexPages.StandardController stdController){
    isEmbedded = TRUE;
    hasError = FALSE;
    String oppid ='';
    String refererURL = ApexPages.currentPage().getHeaders().get('Referer');
   
    
    String paramStr = '{';
    for (OracleCPQProductSearchParam__mdt param : [Select MasterLabel, Value__c FROM OracleCPQProductSearchParam__mdt]){
        paramStr += '"' + param.MasterLabel + '":';
    	if (param.MasterLabel == '_partnerAccountId'){
    		
    	} else if (param.MasterLabel == '_partnerOpportunityId') {
    		
    	} else if (param.Value__c=='null'){
    		paramStr += 'null,';
    	} else {
    		paramStr += '"' + param.Value__c + '",';
    	}
    }
    paramStr = paramStr.removeEnd(',');
    paramStr += '}';
    canvasParams = paramStr;  
    System.debug(canvasParams);
  }
}