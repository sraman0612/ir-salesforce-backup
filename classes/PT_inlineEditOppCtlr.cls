public class PT_inlineEditOppCtlr {

    public class PT_inlineEditOppCtlrException extends Exception {
    }

    public ApexPages.StandardSetController ssc { get; set; }
    public List<Opportunity> monthlyOpps { get {return (List<Opportunity>) ssc.getRecords();} set; }
    public PT_Parent_Opportunity__c parentOpp;
    public String redirectURL { get; set; }
    public String selectedOppId { get; set; }
    public String lossReasonValue { get; set; }
    public Boolean showModal { get; set; }
    public Boolean showEmails { get; set; }
    public Map<Object, List<String>> picklistDependencies;
    public list<emlWrapper> emlLst { get; set; }

    public PT_inlineEditOppCtlr(ApexPages.StandardController stdCtlr) {
        this.showEmails = false;
        this.showModal = false;
        this.parentOpp = (PT_Parent_Opportunity__c) stdCtlr.getRecord();
        this.picklistDependencies = getDependentPicklistValues(Opportunity.PT_Loss_Reason_Detail__c);
        ssc = new ApexPages.StandardSetController(
                Database.getQueryLocator([
                        SELECT Id, Name, CloseDate, PT_Invoice_Date__c, Amount, StageName, PT_Loss_Reason__c,PT_Competitor__c,PT_Sales_Activity__c,
                                PT_Solution_Center__c,NextStep,OwnerId, Owner.Name, PT_Parent_Opportunity__r.Name, PT_Campaign_Id__c,pt_solution_analyse_date_from_sc__c,
                                pt_solutions_quotation_date__c,pt_solution_all_the_info_date__c,PT_Loss_Reason_Detail__c
                        FROM Opportunity
                        WHERE PT_Parent_Opportunity__c = :parentOpp.Id
                ])
        );
    }

    public List<SelectOption> getDynamiclist() {
        String lossReason;
        for (Opportunity oppRec : monthlyOpps) {
            if (oppRec.Id == selectedOppId) {
                lossReason = oppRec.PT_Loss_Reason__c;
                break;
            }
        }
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for (String option : this.picklistDependencies.get(lossReason)) {
            options.add(new SelectOption(option, option));
        }

        return options;
    }

    public static Map<Object, List<String>> getDependentPicklistValues(Schema.sObjectField dependentField) {
        Map<Object, List<String>> dependentPicklistValues = new Map<Object, List<String>>();
        //Get dependent field result
        Schema.DescribeFieldResult dependentFieldResult = dependentField.getDescribe();
        //Get dependent field controlling field
        Schema.sObjectField controllerField = dependentFieldResult.getController();
        //Check controlling field is not null
        if (controllerField == null) {
            return null;
        }
        //Get controlling field result
        Schema.DescribeFieldResult controllerFieldResult = controllerField.getDescribe();
        //Get controlling field picklist values if controlling field is not a checkbox
        List<Schema.PicklistEntry> controllerValues = (controllerFieldResult.getType() == Schema.DisplayType.Boolean ? null : controllerFieldResult.getPicklistValues());

        //It is used to decode the characters of the validFor fields.
        String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

        for (Schema.PicklistEntry entry : dependentFieldResult.getPicklistValues()) {
            if (entry.isActive()) {
                //The PicklistEntry is serialized and deserialized using the Apex JSON class and it will check to have a 'validFor' field
                List<String> base64chars = String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')).split('');
                for (Integer i = 0; i < controllerValues.size(); i++) {
                    Object controllerValue = (controllerValues == null ? (Object) (i == 1) : (Object) (controllerValues[i].isActive() ? controllerValues[i].getLabel() : null));
                    Integer bitIndex = i / 6;
                    Integer bitShift = 5 - Math.mod(i, 6);
                    if (controllerValue == null || (base64map.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0) {
                        continue;
                    }
                    if (!dependentPicklistValues.containsKey(controllerValue)) {
                        dependentPicklistValues.put(controllerValue, new List<String>());
                    }
                    dependentPicklistValues.get(controllerValue).add(entry.getLabel());
                }
            }
        }
        return dependentPicklistValues;
    }


    public PageReference saveMonthEdits() {

        Boolean hasValidationErrors = false;
        Set<String> salesStages = new Set<String>{
                '2.Qualify', '3.Discover', '4.Proposal', '5.Negotiate'
        };
        for (Opportunity o : monthlyOpps) {
            if (String.isNotBlank(o.PT_Solution_Center__c))
                updateSolutionDates(o);
            if ((o.Amount <= 0 || o.Amount == null) && String.isNotBlank(o.PT_Solution_Center__c)) {
                o.NextStep.addError('Amount Should be greater than 0');
                hasValidationErrors = true;
            }
            if (o.NextStep == null && salesStages.contains(o.StageName) && o.PT_Sales_Activity__c != '5.Base') {
                o.NextStep.addError('Next step cannot be null');
                hasValidationErrors = true;
            }
        }

        if (hasValidationErrors) return null;
        emlLst = new List<emlWrapper>();
        if (parentOpp.Type__c == '2.Solution') {
            User[] theseUsers = [Select id, PT_Location__c from user where Id = :UserInfo.getUserId()];
            User thisUser = theseUsers.isEmpty()
                    ? new User()
                    : theseUsers[0];

            Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>([SELECT Id,PT_Solution_Center__c FROM Opportunity WHERE PT_Parent_Opportunity__c = :parentOpp.Id]);
            PTSolutionStatusVisibility__c cs = PTSolutionStatusVisibility__c.getInstance(userinfo.getUserId());
            System.debug('PTSolutionStatusVisibility__c:' + cs);

            String solutionCenterUserId = thisUser.PT_Location__c == 'INDIA'
                    ? cs.India_Solution_Center_User__c
                    : cs.Solution_Center_User__c;

            User solutionUser = [SELECT Id, NAME,email FROM User WHERE Id = :solutionCenterUserId LIMIT 1];
            System.debug('solutionUser:' + solutionUser);
            for (Opportunity o : monthlyOpps) {
                List<Id> oppOwnerLst = new List<Id>{
                        o.OwnerId
                };
                list<Id> solCenterUsrLst = new List<Id>{
                        solutionUser.Id
                };
                String subjStr = o.Name + ' ~ ' + o.PT_Solution_Center__c + ' ~ ' + o.PT_Parent_Opportunity__r.Name.left(20);
                if (o.PT_Solution_Center__c != oldMap.get(o.Id).PT_Solution_Center__c) {
                    if (o.PT_Solution_Center__c == '1.RFQ attached') {
                        emlWrapper newEmlWrapper = new emlWrapper(solutionUser.Name, subjStr, solCenterUsrLst, userinfo.getUserEmail(), o.id);
                        emlLst.add(newEmlWrapper);
                    } else if (o.PT_Solution_Center__c == '3.RFQ incomplete' || o.PT_Solution_Center__c == '2.RFQ complete') {
                        emlWrapper newEmlWrapper = new emlWrapper(o.Owner.Name, subjStr, oppOwnerLst, solutionUser.email, o.id);
                        emlLst.add(newEmlWrapper);
                    } else if (o.PT_Solution_Center__c == '4.Quote complete') {
                        emlWrapper newEmlWrapper = new emlWrapper(o.Owner.Name, subjStr, oppOwnerLst, solutionUser.email, o.id);
                        emlLst.add(newEmlWrapper);
                    } else if (o.PT_Solution_Center__c == '5.Quote incomplete') {
                        emlWrapper newEmlWrapper = new emlWrapper(solutionUser.Name, subjStr, solCenterUsrLst, userinfo.getUserEmail(), o.id);
                        emlLst.add(newEmlWrapper);
                    }
                }
            }
        }
        PageReference pr = ssc.save();
        if (pr != null) {
            redirectURL = pr.getURL();
            if (!emlLst.isEmpty()) {
                showEmails = true;
            }
        } else {
            redirectURL = '';
        }
        return null;
    }

    public void openModal() {
        selectedOppId = apexpages.currentPage().getParameters().get('selectedOppId');
        showModal = true;

        Opportunity selectedOpp;
        for (Opportunity oppRec : this.monthlyOpps) {
            if (oppRec.Id == selectedOppId) {
                selectedOpp = oppRec;
                break;
            }
        }

        if (String.isBlank(selectedOpp.PT_Loss_Reason__c) || !this.picklistDependencies.get(selectedOpp.PT_Loss_Reason__c)
                .contains(selectedOpp.PT_Loss_Reason_Detail__c))
            selectedOpp.PT_Loss_Reason_Detail__c = null;

    }
    public void closeModal() {
        showModal = false;
        selectedOppId = null;
    }

    private static void updateSolutionDates(Opportunity opp) {
        if (opp.PT_Solution_Center__c == '2.RFQ complete') {
            opp.pt_solution_analyse_date_from_sc__c = Date.today();
            opp.pt_solution_all_the_info_date__c = Date.today();
        }
        else if (opp.PT_Solution_Center__c == '1.RFQ attached') {
            opp.pt_solution_analyse_date_from_sc__c = null;
            opp.pt_solution_all_the_info_date__c = null;
            opp.pt_solutions_quotation_date__c = null;
        }
        else if (opp.PT_Solution_Center__c == '3.RFQ incomplete') {
            opp.pt_solution_analyse_date_from_sc__c = Date.today();
        }
        else if (opp.PT_Solution_Center__c == '4.Quote complete') {
            opp.pt_solutions_quotation_date__c = Date.today();
        }
    }

    public PageReference sendemails() {
        Messaging.SingleEmailMessage[] emailMessages = new Messaging.SingleEmailMessage[]{
        };

        Id emailTemplateId = [SELECT Id FROM EmailTemplate WHERE Name = 'Solution Quote Status HTML'].Id;
        for (emlWrapper ew : emlLst) {
            System.debug('EmailWrapper:' + ew);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            Messaging.SingleEmailMessage templatedEmail = Messaging.renderStoredEmailTemplate(emailTemplateId, userinfo.getUserId(), ew.oppId);
            String bodyStr = templatedEmail.plaintextbody;
            bodyStr = bodyStr.replace('@@COMMENTS@@', ew.Comments);
            bodyStr += '\n' + '{ref:' + ew.oppId + '}';
            system.debug('### email body is ' + bodyStr);
            email.setToAddresses(ew.recipientLst);
            email.setCcAddresses(ew.ccLst);
            email.SetSubject(ew.subject);
            email.setPlainTextBody(bodyStr);
            email.setWhatId(ew.oppId);
            emailMessages.Add(email);
        }
        if (emailMessages.size() > 0 && !test.isRunningTest()) {
            Messaging.SingleEmailMessage[] singleEmailMessage = new Messaging.SingleEmailMessage[]{
            };
            singleEmailMessage.add(emailMessages[0]);
            Messaging.SendEmailResult [] r = Messaging.sendEmail(singleEmailMessage);
        }
        showEmails = false;
        return null;
    }


    public class emlWrapper {
        public string sendto { get; set; }
        public list<Id> recipientLst { get; set; }
        public list<String> ccLst { get; set; }
        public string sendfrom { get; set; }
        public string subject { get; set; }
        public string comments { get; set; }
        public string type { get; set; }
        public Id oppId { get; set; }

        public emlWrapper(string emlto, string subj, list<Id> recipients, String cc, Id oid) {
            this.sendto = emlto;
            this.sendfrom = userinfo.getName();
            this.subject = subj;
            this.ccLst = new List<String>{
                    Label.PT_OppInputEmailServiceAddress
            };
            ccLst.add(cc);
            this.recipientLst = recipients;
            this.oppId = oid;
        }
    }


}