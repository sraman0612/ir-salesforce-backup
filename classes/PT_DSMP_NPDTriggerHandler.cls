public with sharing class PT_DSMP_NPDTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : PT_DSMP_NPD Trigger Handler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 21-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	public void handleAfterInsert(List<PT_DSMP_NPD__c> lstNewNPD){
		List<PT_DSMP_NPD__c> lstNPD = new List<PT_DSMP_NPD__c>();
		Set<String> setYear = new Set<String>();
		
		for(Integer i=0;i<lstNewNPD.size();i++){
			
			if(lstNewNPD[i].PT_DSMP_Year__c != null){
				lstNPD.add(lstNewNPD[i]);
				setYear.add(lstNewNPD[i].PT_DSMP_Year__c);
			}

		}

		if(!lstNPD.isEmpty()){
			PT_DSMP_AP02_CreateStrategicProduct.createRecords(lstNPD, setYear);
		}

	}

	public void handleAfterUpdate(List<PT_DSMP_NPD__c> lstNewNPD, List<PT_DSMP_NPD__c> lstOldNPD){
		List<PT_DSMP_NPD__c> lstNPD = new List<PT_DSMP_NPD__c>();
		Set<String> setYear = new Set<String>();
		
		for(Integer i=0;i<lstNewNPD.size();i++){
			
			if(lstNewNPD[i].PT_DSMP_Year__c != lstOldNPD[i].PT_DSMP_Year__c && 
				lstNewNPD[i].PT_DSMP_Year__c != null){

				lstNPD.add(lstNewNPD[i]);
				setYear.add(lstNewNPD[i].PT_DSMP_Year__c);
			}

		}

		if(!lstNPD.isEmpty()){
			PT_DSMP_AP02_CreateStrategicProduct.createRecords(lstNPD, setYear);
		}
	}

}