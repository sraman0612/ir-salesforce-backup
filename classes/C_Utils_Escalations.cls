global class C_Utils_Escalations{
    
    @InvocableMethod
    global static void sendTemplatedEmail(List<String> Ids){
        System.debug('C_Utils_Escalations.sendTemplatedEmail');
        System.debug('Size of incoming List<String> : ' + Ids.size());
        List<Messaging.Email> emailList = new List<Messaging.Email>();
        List<Case> theCases = new List<Case>();
        for(Id i : Ids){
            System.debug(i);
        } 
        for(integer i = 0; i < Ids.size() ; ++i){
            Case theCase = [SELECT Id, ContactId, Contact.AccountId, Contact.Relationship__c, Escalation_Count__c, SuppliedEmail, Org_Wide_Email_Address_ID__c, Escalation_Contact__c, Escalation_Contact__r.Email, New_Case_Notification_Sent_to_Contact__c  FROM Case WHERE Id =: Ids[i]];
            theCases.add(theCase);
            System.debug(theCase);
            System.debug(theCase.Contact);
            //EmailMessage em = [SELECT Id, ToAddress, CcAddress FROM EmailMessage WHERE Incoming = TRUE AND ParentId =: theCase.Id ORDER BY CreatedDate ASC LIMIT 1];
            Messaging.SingleEmailMessage theEmail = new Messaging.SingleEmailMessage();
            List<String> addresses = new List<String>();
      
            
            Email_Template_Admin__c ET = Email_Template_Admin__c.getOrgDefaults();
            
            theEmail.setCCAddresses(addresses);
            theEmail.setWhatId(theCase.Id);
            if(theCase.Contact.Relationship__c == 'Customer' && theCase.Escalation_Count__c == 1){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Customer_1__c')));
            }
            else if (theCase.Contact.Relationship__c == 'Customer' && theCase.Escalation_Count__c == 2){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Customer_2__c')));
                if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }  
            }
             else if (theCase.Contact.Relationship__c == 'Customer' && theCase.Escalation_Count__c == 3){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Customer_3__c')));
                if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }                 
            }
            
            else if (theCase.Contact.Relationship__c == 'Vendor' && theCase.Escalation_Count__c == 1){
                System.debug('Vendor1');
                theEmail.setTargetObjectId(theCase.ContactId);
                //theEmail.setTargetObjectId('0035C00000aJbHcQAK');
                theEmail.setTemplateId(String.valueOf(ET.get('Vendor_1__c')));
            }
             else if (theCase.Contact.Relationship__c == 'Vendor' && theCase.Escalation_Count__c == 2){
                 System.debug('Vendor2');
                theEmail.setTargetObjectId(theCase.ContactId);
                //theEmail.setTargetObjectId('0035C00000aJbHcQAK');
                theEmail.setTemplateId(String.valueOf(ET.get('Vendor_2__c')));
                 if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }  
            }
             else if (theCase.Contact.Relationship__c == 'Vendor' && theCase.Escalation_Count__c == 3){
                 System.debug('Vendor3');
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Vendor_3__c')));
                if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }
            }
            
            else if (theCase.Contact.Relationship__c == 'Internal' && theCase.Escalation_Count__c == 1){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Internal_1__c')));
            }
             else if (theCase.Contact.Relationship__c == 'Internal' && theCase.Escalation_Count__c == 2){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Internal_2__c')));
                 if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }  
            }
             else if (theCase.Contact.Relationship__c == 'Internal' && theCase.Escalation_Count__c == 3){
                theEmail.setTargetObjectId(theCase.ContactId);
                theEmail.setTemplateId(String.valueOf(ET.get('Internal_3__c')));
                if(!String.isEmpty(theCase.Escalation_Contact__r.Email)){
                    theEmail.setCCAddresses(new String[]{theCase.Escalation_Contact__r.Email});  
                }
            }
          
            theEmail.setOrgWideEmailAddressId(theCase.Org_Wide_Email_Address_ID__c);
            theEmail.setUseSignature(false);
            emailList.add(theEmail);
            System.debug('EmailList: '+emailList);
        }
        
        if(!Test.isRunningTest()){
            Messaging.SendEmailResult[] theResults = Messaging.sendEmail(emailList, false);      
            for(Messaging.SendEmailResult result : theResults){
                System.debug(result.isSuccess());
                if(!result.isSuccess()){
                    for(Messaging.SendEmailError error :result.getErrors()){
                        System.debug(error.getMessage());
                        System.debug(error.getStatusCode());
                        System.debug(error.getFields());
                        System.debug(error.getTargetObjectId());
                    }
                }
            }
            update theCases;
        }
    }
}