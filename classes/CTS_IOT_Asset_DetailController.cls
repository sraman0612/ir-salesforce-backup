public with sharing class CTS_IOT_Asset_DetailController {

    private class CTS_IOT_Asset_DetailControllerException extends Exception{}

    @TestVisible private static Boolean forceError = false;    

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Data_Service.AssetDetail assetDetail;
        @AuraEnabled public Account site;
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
    }

    @AuraEnabled
    public static InitResponse getInit(Id assetId){

        InitResponse response = new InitResponse();

        try{

            if (Test.isRunningTest() && forceError){
                throw new CTS_IOT_Asset_DetailControllerException('test error');
            }

            response.assetDetail = CTS_IOT_Data_Service.getCommunityUserAsset(assetId);      
            response.site = CTS_IOT_Data_Service.getCommunityUserSites(response.assetDetail.asset.AccountId)[0];       
        }
        catch(Exception e){
            system.debug('exception: ' + e.getMessage());
            response.success = false;
            response.errorMessage = e.getMessage();
        }

        return response;
    }
}