/**
 * Created by CloudShapers on 12/11/2020.
 */

public class CTS_WorkOrderTriggerHandler {

    static final Id CTS_RECORDTYPE = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Work_Order_CTS_Field_Service').getRecordTypeId();

    /*
* @Name	: associateEntitlements
* @Description: Creates/Updates the Entitlement Records on Work Orders
* @Date   	: 11-December-2020
* @Author 	: CloudShapers
*/
    public static void associateEntitlements(List<WorkOrder> workOrders) {
        try {
            List<WorkOrder> workOrdersToUpdate = new List<WorkOrder>();
            Map<String, List<WorkOrder>> accountIdToWorkOrderMap = new Map<String, List<WorkOrder>>();
            List<SlaProcess> slaProcesses = new List<SlaProcess>();

            slaProcesses = [
                    SELECT id,Name
                    FROM SlaProcess
                    WHERE Name = 'service contract_process_v9'
            ];

            for (WorkOrder workOrderRecord : workOrders) {
                if (workOrderRecord.RecordTypeId == CTS_RECORDTYPE) {
                    if (!accountIdToWorkOrderMap.containsKey(workOrderRecord.AccountId))
                        accountIdToWorkOrderMap.put(workOrderRecord.AccountId, new List<WorkOrder>());
                    accountIdToWorkOrderMap.get(workOrderRecord.AccountId).add(workOrderRecord);
                }
            }

            if (!accountIdToWorkOrderMap.isEmpty()) {
                checkForEntitlements(accountIdToWorkOrderMap, workOrdersToUpdate);

                if (!accountIdToWorkOrderMap.isEmpty())
                    createEntitlements(accountIdToWorkOrderMap, workOrdersToUpdate, slaProcesses);
            }

            if (!workOrdersToUpdate.isEmpty())
                update workOrdersToUpdate;
        } catch (Exception er) {
            System.debug('Error in Creating/Updating Entitlements' + er.getMessage());
        }
    }

    /*
* @Name	: createEntitlements
* @Description: Creates the Entitlement Records on Work Orders
* @Date   	: 11-December-2020
* @Author 	: CloudShapers
*/
    private static void createEntitlements(Map<String, List<WorkOrder>> accountIdToWorkOrderMap,
            List<WorkOrder> workOrdersToUpdate, List<SlaProcess> slaProcesses) {
        Map<String, Entitlement> accountIdToEntitlementsMap = new Map<String, Entitlement>();

        for (String accountId : accountIdToWorkOrderMap.keySet()) {
            accountIdToEntitlementsMap.put(accountId, new Entitlement(Name = accountId,
                    AccountId = accountId,
                    SlaProcessId = !slaProcesses.isEmpty() ? slaProcesses[0].Id : null
            ));
        }

        insert accountIdToEntitlementsMap.values();

        for (String accountId : accountIdToEntitlementsMap.keySet()) {
            if (accountIdToEntitlementsMap.get(accountId).Id != null)
                for (WorkOrder workOrderRec : accountIdToWorkOrderMap.get(accountId)) {
                    workOrdersToUpdate.add(new WorkOrder(EntitlementId = accountIdToEntitlementsMap.get(accountId).Id,
                            Id = workOrderRec.Id));
                }
        }
    }

    /*
* @Name	: createEntitlements
* @Description: Updates the Entitlement Records on Work Orders
* @Date   	: 11-December-2020
* @Author 	: CloudShapers
*/
    private static void checkForEntitlements(Map<String, List<WorkOrder>> accountIdToWorkOrderMap,
            List<WorkOrder> workOrdersToUpdate) {
        List<Entitlement> entitlements;

        entitlements = [
                SELECT Id,AccountId
                FROM Entitlement
                WHERE accountId = :accountIdToWorkOrderMap.keySet()
        ];

        for (Entitlement entitlementRec : entitlements) {
            for (WorkOrder workOrderRec : accountIdToWorkOrderMap.get(entitlementRec.accountId)) {
                workOrdersToUpdate.add(new WorkOrder(EntitlementId = entitlementRec.id,
                        Id = workOrderRec.Id));
            }
            accountIdToWorkOrderMap.remove(entitlementRec.accountId);
        }
    }

}