@isTest
public with sharing class sObjectServiceTest {
    
   	@TestSetup
    public static void createTestData() {
    	
    	insert new Lead(FirstName = 'John', LastName = 'Test1', email='sObjectServiceTest@irco.com');
    	
        List<PavilionSettings__c> psettingList = new List<PavilionSettings__c>();
        psettingList = TestUtilityClass.createPavilionSettings();        
        insert psettingList;    	
    }
    
	@isTest 
	private static void testInsertRecordsAndLogErrors(){
        
		// Insert an invalid lead that will fail
		Lead testLead = new Lead(FirstName = 'Test', LastName = null);
		sObjectService.insertRecordsAndLogErrors(new Lead[]{testLead}, 'sObjectServiceTest', 'testInsertRecordsAndLogErrors');

		system.assertEquals(1, [Select Count() From Lead]); // Already 1 lead created in the system before the test
        system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c = 'sObjectServiceTest' and Method_Name__c = 'testInsertRecordsAndLogErrors']);
        
        // Insert a valid lead
        testLead.LastName = 'Test2';
        testLead.email = 'sObjectServiceTest2@irco.com';
        sObjectService.insertRecordsAndLogErrors(new Lead[]{testLead}, 'sObjectServiceTest', 'testInsertRecordsAndLogErrors');
        
		system.assertEquals(2, [Select Count() From Lead]);
        system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c = 'sObjectServiceTest' and Method_Name__c = 'testInsertRecordsAndLogErrors']);        
    }  
    
	@isTest 
	private static void testUpsertRecordsAndLogErrors(){
        
        Lead firstLead = [Select Id, LastName From Lead];
        firstLead.LastName = 'NewLastName';
        
		// Upsert a change to the first lease, a new invalid lead, and a valid lead
		Lead testLead1 = new Lead(FirstName = 'Test', LastName = null, email = 'sObjectServiceTest3@irco.com');
		Lead testLead2 = new Lead(FirstName = 'Test', LastName = 'Test2', email = 'sObjectServiceTest4@irco.com');
		
		sObjectService.upsertRecordsAndLogErrors(new Lead[]{firstLead, testLead1, testLead2}, 'sObjectServiceTest', 'testUpsertRecordsAndLogErrors');

        system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c = 'sObjectServiceTest' and Method_Name__c = 'testUpsertRecordsAndLogErrors']);
		system.assertEquals(2, [Select Count() From Lead]);
		system.assertEquals(1, [Select Count() From Lead Where LastName = 'NewLastName']);
    }      
    @isTest 
	private static void testDeleteRecordsAndLogErrors(){
        
        Lead firstLead = [Select Id, LastName From Lead];
        firstLead.LastName = 'NewLastName';
        
		Lead testLead2 = new Lead(FirstName = 'Test', LastName = 'Test2', email = 'sObjectServiceTest4@irco.com');
        upsert new Lead[]{firstLead, testLead2};
		
		sObjectService.deleteRecordsAndLogErrors(new Lead[]{firstLead, testLead2}, 'sObjectServiceTest', 'testUpsertRecordsAndLogErrors');

		system.assertEquals(0, [Select Count() From Lead]);
		system.assertEquals(0, [Select Count() From Lead Where LastName = 'NewLastName']);
    } 
    
    @isTest 
    private static void testUpdateRecordsAndLogErrors(){
        
        Lead l = [Select Id, LastName From Lead];
        
        system.assertEquals('Test1', l.LastName);
        
        l.LastName = 'Test2';
        
        sObjectService.updateRecordsAndLogErrors(new Lead[]{l}, 'TestClass', 'TestMethod');
        
		l = [Select Id, LastName From Lead];

		system.assertEquals('Test2', l.LastName);   
        system.assertEquals(0, [Select Count() From Apex_Log__c]);
        
        // Force it to fail
        l.LastName = null;
        
        sObjectService.updateRecordsAndLogErrors(new Lead[]{l}, 'TestClass', 'TestMethod');
        
		l = [Select Id, LastName From Lead];

		system.assertEquals('Test2', l.LastName);   
        
        Apex_Log__c[] errors = [Select Class_Name__c, Method_Name__c From Apex_Log__c];
        system.assertEquals(1, errors.size()); 
        system.assertEquals('TestClass', errors[0].Class_Name__c);
        system.assertEquals('TestMethod', errors[0].Method_Name__c);
    }
    
    @isTest
    private static void testProcessApprovalRequestsAndLogErrors(){
    
    	Account a = [Select Id From Account LIMIT 1];
    
    	Approval.ProcessSubmitRequest[] requests = new Approval.ProcessSubmitRequest[]{};
    
		Approval.ProcessSubmitRequest approvalReq = new Approval.ProcessSubmitRequest();
		approvalReq.setProcessDefinitionNameOrId('Bogus Approval Name');			
		approvalReq.setObjectId(a.Id);
		approvalReq.setSubmitterId(UserInfo.getUserId());		
						
		requests.add(approvalReq);
		
		sObjectService.processApprovalRequestsAndLogErrors(requests, 'TestClass', 'TestMethod');
		
		// Verify an error was logged
		system.assertEquals(1, [Select Count() From Apex_Log__c Where Class_Name__c = 'TestClass' and Method_Name__c = 'TestMethod']); 	
    }   
    
    @isTest
	private static void testGetRecordTypeId(){
		
		Id ccAccountId = sObjectService.getRecordTypeId(Account.getSObjectType(), 'IR Comp MEIA');
		system.assertEquals([Select Id From RecordType Where sObjectType = 'Account' and Name = 'IR Comp MEIA' LIMIT 1].Id, ccAccountId);		
	}    
    
    @isTest
    private static void testGetObjectFieldTokenMap(){
        Map<String, Schema.SObjectField> fldTokenMap = sObjectService.getObjectFieldTokenMap(Contact.sObjectType);
        system.assert(fldTokenMap.containsKey('AccountId'));
    }
    
    @isTest
    private static void testGetObjectFieldDescribeMap(){
        Map<String, DescribeFieldResult> fldDescMap = sObjectService.getObjectFieldDescribeMap(Contact.sObjectType);
        system.assert(fldDescMap.containsKey('AccountId'));
    }  
     @isTest
    private static void testgetAllCreateableFields(){
       List<String> fldDescMap = sObjectService.getAllCreateableFields(Contact.sObjectType);
       // system.assert(fldDescMap.containsKey('AccountId'));
    }  
}