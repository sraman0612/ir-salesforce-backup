@isTest
public with sharing class CTS_IOT_Site_Helix_EventsControllerTest {
    
    @TestSetup
    private static void createData(){

        Account acct1 = CTS_TestUtility.createAccount('Test Account1', false);
        Account acct2 = CTS_TestUtility.createAccount('Test Account2', false);
        insert new Account[]{acct1, acct2};

        Id assetRecTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByName().get('IR Comp Global Asset').getRecordTypeId();
        Asset asset1 = CTS_TestUtility.createAsset('Test Asset1', '12345', acct1.Id, assetRecTypeId, false);
        Asset asset2 = CTS_TestUtility.createAsset('Test Asset2', '67890', acct1.Id, assetRecTypeId, false);
        Asset asset3 = CTS_TestUtility.createAsset('Test Asset3', '55555', acct2.Id, assetRecTypeId, false);
        insert new Asset[]{asset1, asset2, asset3};

        CTS_RMS_Event__c rmsEvent1 = NotificationsTestDataService.createRmsEvent(false, asset1.Id, 'Warning1', 'This is a warning!');
        rmsEvent1.Event_Timestamp__c = DateTime.now();
        rmsEvent1.Event_Code__c = '1';

        CTS_RMS_Event__c rmsEvent2 = NotificationsTestDataService.createRmsEvent(false, asset1.Id, 'Warning2', 'This is a warning!');
        rmsEvent2.Event_Timestamp__c = DateTime.now().addHours(-1);
        rmsEvent2.Event_Code__c = '2';

        CTS_RMS_Event__c rmsEvent3 = NotificationsTestDataService.createRmsEvent(false, asset1.Id, 'Warning3', 'This is a warning!');
        rmsEvent3.Event_Timestamp__c = DateTime.now().addHours(-2);
        rmsEvent3.Event_Code__c = '3';

        CTS_RMS_Event__c rmsEvent4 = NotificationsTestDataService.createRmsEvent(false, asset2.Id, 'Warning4', 'This is a warning!');
        rmsEvent4.Event_Timestamp__c = DateTime.now().addHours(-3);
        rmsEvent4.Event_Code__c = '4';

        CTS_RMS_Event__c rmsEvent5 = NotificationsTestDataService.createRmsEvent(false, asset2.Id, 'Warning5', 'This is a warning!');
        rmsEvent5.Event_Timestamp__c = DateTime.now().addHours(-4);
        rmsEvent5.Event_Code__c = '5';

        CTS_RMS_Event__c rmsEvent6 = NotificationsTestDataService.createRmsEvent(false, asset2.Id, 'Warning6', 'This is a warning!');
        rmsEvent6.Event_Timestamp__c = DateTime.now().addHours(-5);
        rmsEvent6.Event_Code__c = '6'; 
        
        CTS_RMS_Event__c rmsEvent7 = NotificationsTestDataService.createRmsEvent(false, asset3.Id, 'Warning7', 'This is a warning!');
        rmsEvent6.Event_Timestamp__c = DateTime.now().addHours(-6);
        rmsEvent6.Event_Code__c = '7';          

        insert new CTS_RMS_Event__c[]{rmsEvent1, rmsEvent2, rmsEvent3, rmsEvent4, rmsEvent5, rmsEvent6, rmsEvent7};
    }

    @isTest
    private static void getEventsTestSuccess(){

        Account[] accts = [Select Id, Name From Account Order By Name ASC];

        Test.startTest();

        CTS_IOT_Site_Helix_EventsController.EventsResponse response = CTS_IOT_Site_Helix_EventsController.getEvents(accts[0].Id, null, 5, 0);
        system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
        system.assertEquals(5, response.events.size());
        system.assertEquals(6, response.totalRecordCount);

        CTS_RMS_Event__c[] rmsEvents = [
            Select Id,Asset__r.Asset_Name_Displayed__c,Name,Event_Timestamp__c,Event_Code__c,Event_Message__c 
            From CTS_RMS_Event__c
            Where Asset__r.AccountId = :accts[0].Id
            Order By Event_Timestamp__c DESC];

        system.assertEquals(6, rmsEvents.size());

        for (Integer i = 0; i < response.events.size(); i++){

            CTS_IOT_Site_Helix_EventsController.HelixEvent helixEvent = response.events[i];
            CTS_RMS_Event__c rmsEvent = rmsEvents[i];

            system.assertEquals('/asset/' + rmsEvent.Asset__c, helixEvent.assetRecordURL);
            system.assertEquals(rmsEvent.Asset__r.Asset_Name_Displayed__c, helixEvent.assetNickname);
            system.assertEquals('/detail/' + rmsEvent.Id, helixEvent.eventURL);
            system.assertEquals(rmsEvent.Name, helixEvent.eventName);
            system.assertEquals(rmsEvent.Event_Timestamp__c, helixEvent.eventTimestamp);
            system.assertEquals(rmsEvent.Event_Code__c, helixEvent.eventCode);
            system.assertEquals(rmsEvent.Event_Message__c, helixEvent.eventMessage);            
        }

        Test.stopTest();
    }

    @isTest
    private static void getEventsTestSuccess2(){

        Account[] accts = [Select Id, Name From Account Order By Name ASC];

        Test.startTest();

        CTS_IOT_Site_Helix_EventsController.EventsResponse response = CTS_IOT_Site_Helix_EventsController.getEvents(accts[0].Id, null, 5, 5);
        system.assertEquals(true, response.success);
        system.assertEquals('', response.errorMessage);
        system.assertEquals(1, response.events.size());
        system.assertEquals(6, response.totalRecordCount);

        CTS_RMS_Event__c[] rmsEvents = [
            Select Id,Asset__r.Asset_Name_Displayed__c,Name,Event_Timestamp__c,Event_Code__c,Event_Message__c 
            From CTS_RMS_Event__c
            Where Asset__r.AccountId = :accts[0].Id
            Order By Event_Timestamp__c DESC];

        system.assertEquals(6, rmsEvents.size());
        
        for (Integer i = 4; i < response.events.size(); i++){

            CTS_IOT_Site_Helix_EventsController.HelixEvent helixEvent = response.events[i];
            CTS_RMS_Event__c rmsEvent = rmsEvents[i];

            system.assertEquals('/asset/' + rmsEvent.Asset__c, helixEvent.assetRecordURL);
            system.assertEquals(rmsEvent.Asset__r.Asset_Name_Displayed__c, helixEvent.assetNickname);
            system.assertEquals('/detail/' + rmsEvent.Id, helixEvent.eventURL);
            system.assertEquals(rmsEvent.Name, helixEvent.eventName);
            system.assertEquals(rmsEvent.Event_Timestamp__c, helixEvent.eventTimestamp);
            system.assertEquals(rmsEvent.Event_Code__c, helixEvent.eventCode);
            system.assertEquals(rmsEvent.Event_Message__c, helixEvent.eventMessage);            
        }        

        Test.stopTest();
    }    
}