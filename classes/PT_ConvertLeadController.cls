/**
    @author Ben Lorenz
    @date 17OCT2018
    @description To convert PT leads. Controller class for PT_LeadConvert.page
*/

public class PT_ConvertLeadController {

    public class PT_ConvertLeadControllerException extends Exception {}
    
    public PT_Parent_Opportunity__c oppInput {get;set;}
public Boolean fireContactInsertEception = FALSE;
    public Boolean fireOppInputInsertEception = FALSE;
    public Boolean fireActivityInsertEception = FALSE;
    public Boolean fireConvertEception = FALSE;
    public String isForwardToDistributor;
    public String messageToDistributor;
    public String distributorContactId;
    
    String contactId;
    Lead l;
    ApexPages.StandardController stdCont;
    
    public PT_ConvertLeadController(ApexPages.StandardController cont) {
        
        isForwardToDistributor = apexpages.currentpage().getparameters().get('forwardToDistributor');
        messageToDistributor = apexpages.currentpage().getparameters().get('messageToDistributor');
        distributorContactId = apexpages.currentpage().getparameters().get('distributorContactId');
        System.debug('isForwardToDistributor:'+isForwardToDistributor);
        
        oppInput = new PT_Parent_Opportunity__c();
        
        if(!Test.isRunningTest()) {
            //Added Id field for Case # 01212031
           cont.addFields(new List<String> {'Email', 'Company', 'FirstName', 'LastName', 'Name', 'Country', 'LeadSource','PT_Lead_ID__c','PT_Campaign__c', 'Id'}); 
        }
        l = (Lead) cont.getRecord();

        String tempAccName = '%' + l.Country + '%';
        String ptRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();
        
        List<Contact> contactsList = [SELECT Id, Email, AccountId, Account.Name FROM Contact WHERE Email =: l.Email AND RecordType.DeveloperName='PT_powertools'];
        if(contactsList.size() > 0 && String.isNotBlank(l.Email)) {//if a contact exists by the lead email            
            for(Contact c: contactsList) {
                oppInput.Account__c = c.AccountId;
            }            
        }else { //no contact found by email, search for accounts
            if(String.isNotBlank(l.Company)){
                for(Account a: [SELECT Id, Name FROM Account WHERE Name =: l.Company AND BillingCountry =: l.Country AND RecordTypeId =: ptRT]) {                                
                    oppInput.Account__c = a.Id;
                }
            }
            /*
            18jun2019 forcing rep to select an account
            if(null==OppInput.Account__c){
              for(Account a: [SELECT Id, Name FROM Account WHERE Name LIKE 'New%' AND Name LIKE :tempAccName AND BillingCountry =: l.Country AND RecordTypeId =: ptRT] ) {                                
                oppInput.Account__c = a.Id;
              }
            }
            */
        }
        
        /*
        if(null==oppInput.Account__c) { //If accountsSelectList is empty until this point, show an error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No account found to convert against.'));
        }
        */
    }
    
    public PageReference convert() {
        //create a SavePoint to rollback to if any of the steps fail
        Savepoint sp = Database.setSavepoint();
        
        //An Account was found and selected based on Country
        if(null == oppInput.Account__c) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No account found to convert against.'));
          return null;
        }

        Contact c;
        if(String.isBlank(contactId)) { //A contact was not found matching the lead email. Create one.
            c = new Contact();
            c.FirstName = l.FirstName;
            c.LastName = l.LastName;
            c.Email = l.Email;
            c.PT_UID__c = l.Email;
            c.AccountId = oppInput.Account__c;
            c.Lead_Company__c = l.Company;     
            c.LeadSource = l.LeadSource;
        } else { //Contact was found, set the Lead Company field only from "Company" field.
            c = new Contact(Id = contactId, Lead_Company__c = l.Company,PT_UID__c=l.Email);
        }
        
        try {
            if(Test.isRunningTest() && fireContactInsertEception) {throw new DMLException('EXCEPTION');}
            if(String.isNotBlank(c.PT_UID__c)){
                Database.upsert(c,Contact.PT_UID__c);
            }else{
                upsert c;
            }
            contactId = c.Id; //set the contactId anyway
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug('e**'+e.getMessage());
            Database.rollback(sp);//rollback to starting point
            return null;
        }
        
        oppInput.Converted_from_PT_Lead__c = true; //marker that this Opp Inout was created from this process
        oppInput.Lead_SourcePL__c = l.LeadSource;
        oppInput.PT_Lead_Id__c = l.PT_Lead_ID__c;
        oppInput.PT_Campaign__c = l.PT_Campaign__c;
        //Added PT_Lead__c field for Case # 01212031
        oppInput.PT_Lead__c = l.Id;
        
        try { //Create Opportunity Input record
          if(Test.isRunningTest() && fireOppInputInsertEception) {throw new DMLException('EXCEPTION');}
            insert oppInput;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug('e**'+e.getMessage());
            Database.rollback(sp);//rollback to starting point
            return null;
        }
        
        List<Task> tasks = [SELECT Id, WhatId, WhoId FROM Task WHERE WhoId =: l.Id];
        List<Event> events = [SELECT Id, WhatId, WhoId FROM Event WHERE WhoId =: l.Id];
        
        try {//reassign activities to the Opp Input and Contact for conversion
          if(Test.isRunningTest() && fireActivityInsertEception) {throw new DMLException('EXCEPTION');}
            for(Task t: tasks) {
                t.WhoId = contactId;
                t.WhatId = oppInput.Id;    
            }
            update tasks;
            
            for(Event e: events) {
                e.WhoId = contactId;
                e.WhatId = oppInput.Id;    
            }
            update events;
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed to reassign the activities.'));
            System.debug('e**'+e.getMessage());
            Database.rollback(sp);//rollback to the starting point
            return null;
        }
        
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(l.Id);
        lc.setAccountId(oppInput.Account__c);
        lc.setContactId(contactId);
        lc.setDoNotCreateOpportunity(true);        
        lc.setConvertedStatus('4a. Create Opportunity');
        
        try {
        	if(Test.isRunningTest() && fireConvertEception) {throw new DMLException('EXCEPTION');}
            Database.LeadConvertResult lcr = Database.convertLead(lc);//convert the lead
            if(lcr.isSuccess()) { //show message and redirect on success
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM , 'Lead Converted Successfully.'));
                
                //2020-02-28 BenJ - Need to set the lead Status__c
                Lead leadToUpdate = new Lead();
                leadToUpdate.Id = l.Id;
                leadToUpdate.PT_Status__c = '4. Close';

        		if(isForwardToDistributor == 'true') 
                {
                    leadToUpdate.PT_Forward_to_distributor__c = true;
                    leadToUpdate.Status = '4b. Forward to Distributor';
                    leadToUpdate.PT_Message__c = messageToDistributor;
                    if(distributorContactId != 'null') leadToUpdate.Distribution_Account_s_Contact__c = distributorContactId;
                }

                update leadToUpdate;
                
                return (new ApexPages.StandardController(oppInput)).view(); //redirect to Opportunity Input record
            }            
        } catch(Exception e) { //show error message and rollback on error
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug('e**'+e.getMessage());
            Database.rollback(sp);//rollback to starting point
        }   
        
        return null;
    }

}