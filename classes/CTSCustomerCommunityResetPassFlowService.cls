public without sharing class CTSCustomerCommunityResetPassFlowService {

    @InvocableMethod
    public static void resetPassword(List<Id> userIds){

        Network community = [SELECT Id, ChangePasswordEmailTemplateId FROM Network Where Name = 'IR Comp Customer Portal'];

        String resetPasswordTemplate;

        if (community != null && community.ChangePasswordEmailTemplateId != null){
            resetPasswordTemplate = [Select Id, DeveloperName From EmailTemplate Where Id = :community.ChangePasswordEmailTemplateId].DeveloperName;
        }

        for (Id userId : userIds){

            if (String.isNotBlank(resetPasswordTemplate)){
                System.resetPasswordWithEmailTemplate(userId, true, resetPasswordTemplate);
            }
            else{
                System.resetPassword(userId, true);
            }
        }
    }
}