@IsTest
public class B2BRefundCreditCardInvocableTest {

    @IsTest
    static void testRefundCreditCardSuccess() {
        // Arrange
        List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument> testArguments = new List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument>();
        B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument arg = new B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument();
        arg.paymentId = 'VALID_PAYMENT_ID';
        arg.refundAmount = 100.00;
        arg.refundComments = 'VALID_COMMENTS';
        testArguments.add(arg);

        // Act
        Test.startTest();
        List<B2BRefundCreditCardInvocable.ReversalResponse> responses = B2BRefundCreditCardInvocable.refundCreditCard(testArguments);
        Test.stopTest();

        // Assert
        System.assertEquals(1, responses.size(), 'There should be one response');
        System.assert(responses[0].success, 'The response should indicate success');
        System.assert(responses[0].message == null, 'The message should be null for a successful operation');
    }

    @IsTest
    static void testRefundCreditCardFailure() {
        // Arrange
        List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument> testArguments = new List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument>();
        B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument arg = new B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument();
        arg.paymentId = 'INVALID_PAYMENT_ID';
        arg.refundAmount = 50.00;
        arg.refundComments = 'INVALID_COMMENTS';
        testArguments.add(arg);

        // Act
        Test.startTest();
        List<B2BRefundCreditCardInvocable.ReversalResponse> responses = B2BRefundCreditCardInvocable.refundCreditCard(testArguments);
        Test.stopTest();

        // Assert
        System.assertEquals(1, responses.size(), 'There should be one response');
        System.assert(!responses[0].success, 'The response should indicate failure');
    }

    @IsTest
    static void testRefundCreditCardMultipleInputs() {
        // Arrange
        List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument> testArguments = new List<B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument>();

        B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument arg1 = new B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument();
        arg1.paymentId = 'VALID_PAYMENT_ID';
        arg1.refundAmount = 100.00;
        arg1.refundComments = 'VALID_COMMENTS';

        B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument arg2 = new B2BRefundCreditCardInvocable.B2BRefundCreditCardArgument();
        arg2.paymentId = 'INVALID_PAYMENT_ID';
        arg2.refundAmount = 50.00;
        arg2.refundComments = 'INVALID_COMMENTS';

        testArguments.add(arg1);
        testArguments.add(arg2);

        // Act
        Test.startTest();
        List<B2BRefundCreditCardInvocable.ReversalResponse> responses = B2BRefundCreditCardInvocable.refundCreditCard(testArguments);
        Test.stopTest();

        // Assert
        System.assertEquals(2, responses.size(), 'There should be two responses');

        System.assert(responses[0].success, 'The first response should indicate success');
        System.assert(responses[0].message == null, 'The message for the first response should be null');

        System.assert(!responses[1].success, 'The second response should indicate failure');
    }
}