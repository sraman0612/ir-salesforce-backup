Global class CTS_OM_SurveyRandomizationBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id,ContactId,CTS_OM_Send_Email_to_Contact__c,OwnerId'+
            ' FROM Case'+ 
            ' WHERE '+
            'Status!=\'Resolved\' AND '+
            'isClosed = false AND '+
            'CreatedDate = LAST_N_DAYS:2 AND '+
            'CTS_OM_Send_Email_to_Contact__c = false AND '+
            '(RecordType.DeveloperName = \'CTS_OM_Americas\' OR RecordType.DeveloperName = \'CTS_OM_ZEKS\') ORDER by CreatedDate DESC';
            
            
        system.debug('query----->'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<Case>caseList){
        CTS_OM_SurveyRandomization.updateCase(CaseList);
    }
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }
}