public with sharing class PT_DSMP_TaskTriggerHandler {
/**************************************************************************************
-- - Author        : Spoon Consulting Ltd
-- - Description   : Task Trigger Handler
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks
-- -----------  ----  -------  -------------------------------------------------------
-- 03-DEC-2018  MGR    1.0     Initial version
-- 27-FEB-2019  Ben L  2.0     Refactored to get objective from custom lookup, whatid will be account in order to push task to account activity timeline
--------------------------------------------------------------------------------------
**************************************************************************************/ 

	//public static final String OBJACCOUNTOBJECTIVEPREFIX = PT_DSMP_Objectives__c.SObjectType.getDescribe().getKeyPrefix();

	public void handleAfterInsert(List<Task> lstNewTask){
		
		Set<String> setAccountObjectiveId = new Set<String>();
		
		for(Integer i=0;i<lstNewTask.size();i++){
		  if(lstNewTask[i].PT_DSMP_Account_Objectives__c != null){setAccountObjectiveId.add(lstNewTask[i].PT_DSMP_Account_Objectives__c);}
		}

		if(!setAccountObjectiveId.isEmpty()){PT_DSMP_AP01_CountTasksRecords.countTaskRecords(setAccountObjectiveId);}
	}
	
	public void handleAfterUpdate(List<Task> lstNewTask, List<Task> lstOldTask){
	  Set<String> setAccountObjectiveId = new Set<String>();
	  
	  for(Integer i=0;i<lstNewTask.size();i++){
	    if(lstNewTask[i].PT_DSMP_Account_Objectives__c != null &&
	       (
	         (lstNewTask[i].PT_DSMP_Account_Objectives__c != null && lstNewTask[i].PT_DSMP_Account_Objectives__c != lstOldTask[i].PT_DSMP_Account_Objectives__c) ||
	         (lstNewTask[i].Status != null && lstNewTask[i].Status != lstOldTask[i].Status) ||
	         (lstNewTask[i].ActivityDate != null && lstNewTask[i].ActivityDate != lstOldTask[i].ActivityDate)
	       )){
        setAccountObjectiveId.add(lstNewTask[i].PT_DSMP_Account_Objectives__c);
			}
		}

		if(!setAccountObjectiveId.isEmpty()){PT_DSMP_AP01_CountTasksRecords.countTaskRecords(setAccountObjectiveId);}
	}

  public void handleAfterDelete(List<Task> lstOldTask){
    
    Set<String> setAccountObjectiveId = new Set<String>();
    
    for(Integer i=0;i<lstOldTask.size();i++){
      if(lstOldTask[i].PT_DSMP_Account_Objectives__c != null){setAccountObjectiveId.add(lstOldTask[i].PT_DSMP_Account_Objectives__c);}
		}

		if(!setAccountObjectiveId.isEmpty()){PT_DSMP_AP01_CountTasksRecords.countTaskRecords(setAccountObjectiveId);}
	}
}