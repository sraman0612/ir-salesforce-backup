/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the controller class for SalesCallPlan_Page which has various methods for the required functionality.
------------------------------------------------------------*/
public with sharing class SalesCallPlan_Page_Extension
{
    public String salesCallPlanId;
    //commented by CG as part of descoped object
    //public Sales_Call_Plan__c  salesCallPlan{get;set;}
    public List<questionWrapper> questionList {get; set;}
    public string accountName{get;set;}
    public string optyName{get;set;}
    public string optyowner{get;set;}
    public string accountowner{get;set;}
    public boolean optyflag{get;set;}
    public boolean printflag{get;set;}

    public questionWrapper questionList1{get;set;}
    public questionWrapper questionList2{get;set;}
    public questionWrapper questionList3{get;set;}
    public questionWrapper questionList4{get;set;}
    public questionWrapper questionList5{get;set;}

    public list<QuestionAnswerMappingObject__c> allQuestionMapping;

    //Delaration of wrapper class for questions and answers in salescall plan
    public class questionWrapper{
        public string question {get;set;}
        public string answer {get;set;}
        public string questionId;
        public string catagory{get;set;}

        public questionWrapper(){

        }

        //Constructor for questionWrapper
        public questionWrapper(string q,string a,string qid,string c){

            question=q;
            answer=a;
            questionId=qid;
            catagory=c;
        }
    }
    
    //Constructor for SalesCallPlan_Page_Extension
    public SalesCallPlan_Page_Extension(ApexPages.StandardController controller)
    {
        optyflag=true;
        questionList= new List<questionWrapper>();
        questionWrapper qW;
        string sName;
        if(!Test.isrunningtest())
            controller.addFields(new List<String>{'Account_Salescallplan__c','Last_Modifed_Date_Time__c','Last_Modified_User__c','Opportunity_salescallplan__c','Attendees__c','Meeting_End__c','Meeting_Location__c','Date_of_Last_Meeting__c','Meeting_Objective__c','Meeting_Start__c','CountModified__c','Parent__c','Sales_Manager_Leader_Feedback__c','FeedBack_Date_Time__c'});
        //commented by CG as part of descoped object
        /*salesCallPlan=(Sales_Call_Plan__c) controller.getRecord();
        salesCallPlanId=salesCallPlan.id;
        if(salesCallPlan.id==null){
            printflag=false;
			//commented by CG as part of descoped object
            list<QuestionsList__c> allSalescall=new list<QuestionsList__c>([Select id,
                                            QuestionList_SalesCall__c,Category__c
                                            from QuestionsList__c
                                            where Template__c='Sales Call Plan'
                                            order by Name]);

            questionList1=new questionWrapper(allSalescall[0].QuestionList_SalesCall__c,'',allSalescall[0].id,allSalescall[0].Category__c);
            questionList2=new questionWrapper(allSalescall[1].QuestionList_SalesCall__c,'',allSalescall[1].id,allSalescall[1].Category__c);
            questionList3=new questionWrapper(allSalescall[2].QuestionList_SalesCall__c,'',allSalescall[2].id,allSalescall[2].Category__c);
            questionList4=new questionWrapper(allSalescall[3].QuestionList_SalesCall__c,'',allSalescall[3].id,allSalescall[3].Category__c);
            questionList5=new questionWrapper(allSalescall[4].QuestionList_SalesCall__c,'',allSalescall[4].id,allSalescall[4].Category__c);

            if(salesCallPlan.Account_Salescallplan__c!=null && salesCallPlan.Opportunity_salescallplan__c==null){
                salesCallPlan.Parent__c='Account';
                sName=[Select name from account where id=:salesCallPlan.Account_Salescallplan__c].name+'_'+datetime.now();
            }
            else if(salesCallPlan.Opportunity_salescallplan__c!=null && salesCallPlan.Account_Salescallplan__c==null){
                salesCallPlan.Parent__c='Opportinity';
                salesCallPlan.Account_Salescallplan__c=[Select id,
                                                               accountid
                                                               from opportunity
                                                               where id=:salesCallPlan.Opportunity_salescallplan__c].accountid;
                sName=[Select id,account.name from opportunity where id=:salesCallPlan.Opportunity_salescallplan__c].account.name+'_'+[Select name from opportunity where id=:salesCallPlan.Opportunity_salescallplan__c].name+'_'+datetime.now();                                               
               
            }
            if(sName.length()>80){
                sName=sName.substring(0,80);
            }
            salesCallPlan.name=sName;

        }
        else{
            printflag=true;
            //salesCallPlan = [Select id,Attendees__c,Last_Modified_User__c,Last_Modifed_Date_Time__c,Date_of_Last_Meeting__c,Meeting_End__c,Meeting_Location__c,Meeting_Objective__c,Meeting_Start__c,Account_Salescallplan__c,Opportunity_salescallplan__c,CountModified__c,Sales_Manager_Leader_Feedback__c,FeedBack_Date_Time__c from Sales_Call_Plan__c
                                                                                                                                                            //where id=:salesCallPlan.id];
                
                /*allQuestionMapping=new list<QuestionAnswerMappingObject__c>([Select id,
                                                           Questions__r.name,
                                                           Questions__r.QuestionList_SalesCall__c,
                                                           Answers__c,Questions__c,Questions__r.Category__c
                                                           from QuestionAnswerMappingObject__c
                                                           where SalesCallPlanId__c=:salesCallPlan.id
                                                           order by Questions__r.name]);

                questionList1=new questionWrapper(allQuestionMapping[0].Questions__r.QuestionList_SalesCall__c,allQuestionMapping[0].Answers__c,allQuestionMapping[0].Questions__c,allQuestionMapping[0].Questions__r.Category__c);
                questionList2=new questionWrapper(allQuestionMapping[1].Questions__r.QuestionList_SalesCall__c,allQuestionMapping[1].Answers__c,allQuestionMapping[1].Questions__c,allQuestionMapping[1].Questions__r.Category__c);
                questionList3=new questionWrapper(allQuestionMapping[2].Questions__r.QuestionList_SalesCall__c,allQuestionMapping[2].Answers__c,allQuestionMapping[2].Questions__c,allQuestionMapping[2].Questions__r.Category__c);
                questionList4=new questionWrapper(allQuestionMapping[3].Questions__r.QuestionList_SalesCall__c,allQuestionMapping[3].Answers__c,allQuestionMapping[3].Questions__c,allQuestionMapping[3].Questions__r.Category__c);
                questionList5=new questionWrapper(allQuestionMapping[4].Questions__r.QuestionList_SalesCall__c,allQuestionMapping[4].Answers__c,allQuestionMapping[4].Questions__c,allQuestionMapping[4].Questions__r.Category__c);
                

            }

        if(salesCallPlan.Account_Salescallplan__c!=null){
            Account act=[Select name,owner.name from account where id=:salesCallPlan.Account_Salescallplan__c];
            accountName=act.name;
            accountowner=act.owner.name;
            if(salesCallPlan.Opportunity_salescallplan__c==null)
                optyflag=false;
        }
        if(salesCallPlan.Opportunity_salescallplan__c!=null){
            Opportunity opty=[Select name,owner.name from opportunity where id=:salesCallPlan.Opportunity_Salescallplan__c];
            optyName=opty.name;
            optyowner=opty.owner.name;
        }
     }

    //This method is called when user clicks on Save button in salescall plan
     public PageReference SaveSalesCall()
     {
        /*try{
         if(salesCallPlan.id== null){
            salesCallPlan.CountModified__c=1;
         }
         else{
            salesCallPlan.CountModified__c++;
         }
         Datetime myDT = Datetime.now();
         String myDate = myDT.format();
         salesCallPlan.Last_Modifed_Date_Time__c=myDate;
         salesCallPlan.Last_Modified_User__c=Userinfo.getname();
         upsert salesCallPlan;

         if(salesCallPlanId == null)
         {
            List<QuestionAnswerMappingObject__c> toUpsertList=new List<QuestionAnswerMappingObject__c>();
            
            toUpsertList.add(new QuestionAnswerMappingObject__c(Answers__c=questionList1.answer,Questions__c=questionList1.questionId,SalesCallPlanId__c=salesCallPlan.id));
            toUpsertList.add(new QuestionAnswerMappingObject__c(Answers__c=questionList2.answer,Questions__c=questionList2.questionId,SalesCallPlanId__c=salesCallPlan.id));
            toUpsertList.add(new QuestionAnswerMappingObject__c(Answers__c=questionList3.answer,Questions__c=questionList3.questionId,SalesCallPlanId__c=salesCallPlan.id));
            toUpsertList.add(new QuestionAnswerMappingObject__c(Answers__c=questionList4.answer,Questions__c=questionList4.questionId,SalesCallPlanId__c=salesCallPlan.id));
            toUpsertList.add(new QuestionAnswerMappingObject__c(Answers__c=questionList5.answer,Questions__c=questionList5.questionId,SalesCallPlanId__c=salesCallPlan.id));
            insert toUpsertList; 
         }
         else{
            allQuestionMapping[0].Answers__c=questionList1.answer;
            allQuestionMapping[1].Answers__c=questionList2.answer;
            allQuestionMapping[2].Answers__c=questionList3.answer;
            allQuestionMapping[3].Answers__c=questionList4.answer;
            allQuestionMapping[4].Answers__c=questionList5.answer;
            update allQuestionMapping;
         }
        
        }catch(Exception Ex){
        }

         if(!string.isblank(salesCallPlan.Account_Salescallplan__c) && string.isblank(salesCallPlan.Opportunity_salescallplan__c)){
            PageReference p = new PageReference('/'+salesCallPlan.Account_Salescallplan__c);
            p.setRedirect(true);
            return p ;
         }
         else if(string.isblank(salesCallPlan.Account_Salescallplan__c) && !string.isblank(salesCallPlan.Opportunity_salescallplan__c))
         {
            PageReference p1 = new PageReference('/'+salesCallPlan.Opportunity_salescallplan__c);
            p1 .setRedirect(true);
            return p1 ;
         }
         else{
            if(salesCallPlan.Parent__c=='Account'){
                PageReference p = new PageReference('/'+salesCallPlan.Account_Salescallplan__c);
                p.setRedirect(true);
                return p ;
            }
            else{
                PageReference p1 = new PageReference('/'+salesCallPlan.Opportunity_salescallplan__c);
                p1 .setRedirect(true);
                return p1 ;
            }
         }
        }
    
    //This method is called when user clicks on Cancel button in salescall plan
    public PageReference CancelSalesCall()
    {
        if(salesCallPlan.Opportunity_salescallplan__c==null){
            PageReference p2 = new PageReference('/'+salesCallPlan.Account_Salescallplan__c);
            p2 .setRedirect(true);
            return p2 ;
         }
         else
         {
            PageReference p3 = new PageReference('/'+salesCallPlan.Opportunity_salescallplan__c);
            p3 .setRedirect(true);
            return p3 ;
         }
    }*/

}
}