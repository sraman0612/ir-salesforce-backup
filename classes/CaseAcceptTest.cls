@IsTest
private class CaseAcceptTest {
    @IsTest
    public static void unitTest1(){
        User u = CTS_TestUtility.createUser(true);
        User us = CTS_TestUtility.createUser(true);

        system.runAs(u){
            Account ac = CTS_TestUtility.createAccount('Test Account1', true);
            Contact ct = CTS_TestUtility.createContact('Henry', 'John','johnH@gmail.com', ac.Id, true);
            Case c = CTS_TestUtility.createCase('subject test', ct.AccountId , ct.Id, true);
            
            c.OwnerId = u.Id;
            c.Status = 'New';
            //c.Assigned_From_Address_Picklist__c = 'ir_compressors_export@irco.com';
            c.GDI_Department__c = u.Department__c;
            c.Brand__c = u.Brand__c;
            c.Product_Category__c = u.Default_Product_Category__c;
            c.Org_Wide_Email_Address_ID__c= '0D24Q00000003G9';
            c.Case_Accepted__c = false;
            c.SuppliedEmail = 'johnH@gmail.com';
            c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_Non_Warranty_Claims').getRecordTypeId();
            update c;
            
            test.startTest();
            system.debug('Test currCase1 ====> '+c);
            u.Assigned_From_Email_Address__c = 'cx_dublin_support@irco.com';
            update u;
            Case c1 = CaseAccept.getCase(c.Id);
            CaseAccept.updateCase(c);
            CaseAccept.updateCase(c1);
            
            //CaseAccept.processRecords(c.Id);
            test.stopTest();
        }
    }
    @IsTest
    public static void unitTest2(){
        User u = CTS_TestUtility.createUser(true);
        //User us = CTS_TestUtility.createUser(true);

        system.runAs(u){
            Account ac = CTS_TestUtility.createAccount('Test Account1', true);
            Contact ct = CTS_TestUtility.createContact('Henry', 'John','johnH@gmail.com', ac.Id, true);
            Case c = CTS_TestUtility.createCase('subject test', ct.AccountId , ct.Id, true);
            
            c.OwnerId = u.Id;
            c.Status = 'New';
            //c.Assigned_From_Address_Picklist__c = 'ir_compressors_export@irco.com';
            c.GDI_Department__c = u.Department__c;
            c.Brand__c = u.Brand__c;
            c.Product_Category__c = u.Default_Product_Category__c;
            c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
            c.Case_Accepted__c = false;
            c.SuppliedEmail = 'johnH@gmail.com';
            c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CTS_OM_Account_Management').getRecordTypeId();
            update c;
            
            test.startTest();
            system.debug('Test currCase1 ====> '+c);
            u.Assigned_From_Email_Address__c = 'ir_compressors_export@irco.com';
            update u;
            Case c1 = CaseAccept.getCase(c.Id);
            CaseAccept.updateCase(c);
            CaseAccept.updateCase(c1);
            
            //CaseAccept.processRecords(c.Id);
            test.stopTest();
        }
    }
    @IsTest
    public static void unitTest3(){
        User u = CTS_TestUtility.createUser(false);
        u.Business__c = 'Compressor';
        insert u;

        Account ac, ac1;

        system.runAs(u){
            //User us = CTS_TestUtility.createUser(true);
            ac1 = CTS_TestUtility.createAccount('Test Account1', true);
            ac = new Account(
                Name = 'Test Account1', 
                BillingStreet = '123 Sesame Street',
                BillingCity = 'Miami',
                BillingState = 'FL',
                BillingPostalCode = '47372',
                BillingCountry = 'USA',
                ShippingCountry  = 'USA',
                ShippingStreet = '123 Sesame Street',
                ShippingCity = 'Miami',
                ShippingState = 'FL',
                ShippingPostalCode = '47372',
                County__c = 'Orange',
                RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId(),
                Account_Region__c = 'Oracle 11i'
                //Account_Division__c = division.Id
            );

        
       // if(isInsert){
            insert ac;
       // }

            Business_Relationship__c br = new Business_Relationship__c();
            br.BR_Owner__c = UserInfo.getUserId();
            br.Account__c = ac.id;
            br.Business__c ='Compressor';
            br.Type__c = 'Prospect';
            insert br;
        }

        u.Business__c = 'Low Pressure';
        update u;

        system.runAs(u){

            Contact ct = CTS_TestUtility.createContact('Henry', 'John','johnH@gmail.com', ac.Id, true);
            Opportunity op3 = new Opportunity(Name='Test Opp 2');
            op3.CurrencyIsoCode = 'USD';
            op3.stagename = 'Stage 1. Qualify';
            op3.amount = 1000000;
            op3.closedate = system.today();
            op3.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
            op3.AccountId = ac.Id;
            op3.Business__c='Low Pressure';
            
            //try{
                insert op3;
        // }catch(exception ex){
            //  System.assertEquals(1, l3.getErrors().size());
            //  System.assertEquals('You can not create Leads for businesses other than yours.', l3.getErrors()[0].getMessage());
            //}
            Case c = new Case();// CTS_TestUtility.createCase('subject test', ct.AccountId , ct.Id, true);
            c.subject = 'subject test'; 
            c.accountId = ct.AccountId;
            c.contactId = ct.Id;
            c.OwnerId = u.Id;
            c.Status = 'New';
            //c.Assigned_From_Address_Picklist__c = 'ir_compressors_export@irco.com';
            c.GDI_Department__c = u.Department__c;
            c.Brand__c = u.Brand__c;
            c.Business__c='Vacuum';
            c.Related_Opportunity__c=op3.Id;
            c.Product_Category__c = u.Default_Product_Category__c;
            c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
            c.Case_Accepted__c = false;
            c.SuppliedEmail = 'johnH@gmail.com';
            c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
            insert c;
            
            test.startTest();
           
                system.debug('Test currCase1 ====> '+c);
                u.Assigned_From_Email_Address__c = 'ir_compressors_export@irco.com';
                update u;
                Case c1 = CaseAccept.getCase(c.Id);
                CaseAccept.updateCase(c);
                CaseAccept.updateCase(c1);
            
            
            //CaseAccept.processRecords(c.Id);
            test.stopTest();
        }
    }
}