/**
 * Created by wenboliu on 07/10/2024.
 */

global with sharing class B2BCartTaxCalculationsRob implements sfdc_checkout.CartTaxCalculations {

    global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
        try {
            Map<Id, CartItem> cartItemsMap = new Map<Id, CartItem>();

            // Get the delivery group to retrieve the DeliverTo country information
            Id cartDeliveryGroupId = [SELECT CartDeliveryGroupId FROM CartItem WHERE CartId = :cartId WITH SECURITY_ENFORCED][0].CartDeliveryGroupId;
            CartDeliveryGroup deliveryGroup = [SELECT DeliverToCountry FROM CartDeliveryGroup WHERE Id = :cartDeliveryGroupId WITH SECURITY_ENFORCED][0];

            // Fetch tax rate based on the country from custom metadata
            String deliverToCountry = deliveryGroup.DeliverToCountry;
            Decimal taxRate = getTaxRateByCountry(deliverToCountry)/100;
            if (taxRate == null) {
                String errorMessage = 'No tax rate defined for the country: ' + deliverToCountry;
                return integrationStatusFailedWithCartValidationOutputError(integStatus, errorMessage, jobInfo, cartId);
            }
            // Product Type can be added to filter to remove shipping cost
            for (CartItem cartItem : [SELECT Sku, TotalPrice, Type, DistributedAdjustmentAmount FROM CartItem WHERE CartId = :cartId WITH SECURITY_ENFORCED]) {
                cartItemsMap.put(cartItem.Id,cartItem);
            }
            deleteOldCartTax(cartItemsMap.keySet());
            CartTax[] cartTaxestoInsert = new CartTax[]{};
            for (CartItem cItem : cartItemsMap.values()) {
                CartTax tax = new CartTax(
                        Amount = (cItem.TotalPrice + cItem.DistributedAdjustmentAmount) * taxRate,
                        CartItemId = cItem.Id,
                        Name = 'SalesTax',
                        TaxCalculationDate = Date.today(),
                        TaxRate = taxRate,
                        TaxType = 'Estimated'
                );
                cartTaxestoInsert.add(tax);
            }
            insert(cartTaxestoInsert);
            integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;

        } catch(Exception e) {
            return integrationStatusFailedWithCartValidationOutputError(
                    integStatus,
                    'An exception of type ' + e.getTypeName() + ' has occurred: ' + e.getMessage(),
                    jobInfo,
                    cartId
            );
        }
        return integStatus;
    }

    // Fetch the tax rate based on the country from the custom metadata type
    private Decimal getTaxRateByCountry(String country) {
        List<B2B_Tax_Setting__mdt> taxRates = [
                SELECT Tax_Percentage__c
                FROM B2B_Tax_Setting__mdt
                WHERE Country_Code__c = :country
                LIMIT 1
        ];
        if (!taxRates.isEmpty()) {
            return taxRates[0].Tax_Percentage__c;
        }
        return null; // Return null if no tax rate is found for the country
    }

    private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
            sfdc_checkout.IntegrationStatus integrationStatus, String errorMessage, sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
        integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
        // For the error to be propagated to the user, we need to add a new CartValidationOutput record.
        // The following fields must be populated:
        // BackgroundOperationId: Foreign Key to the BackgroundOperation
        // CartId: Foreign key to the WebCart that this validation line is for
        // Level (required): One of the following - Info, Error, or Warning
        // Message (optional): Message displayed to the user (maximum 255 characters)
        // Name (required): The name of this CartValidationOutput record. For example CartId:BackgroundOperationId
        // RelatedEntityId (required): Foreign key to WebCart, CartItem, CartDeliveryGroup
        // Type (required): One of the following - SystemError, Inventory, Taxes, Pricing, Shipping, Entitlement, Other
        CartValidationOutput cartValidationError = new CartValidationOutput(
                BackgroundOperationId = jobInfo.jobId,
                CartId = cartId,
                Level = 'Error',
                Message = errorMessage?.left(255),
                Name = (String)cartId + ':' + jobInfo.jobId,
                RelatedEntityId = cartId,
                Type = 'Taxes'
        );
        insert(cartValidationError);
        return integrationStatus;
    }

    private static void deleteOldCartTax(Set<Id> cartItemIdSet) {
        List<CartTax> cartTaxList = [SELECT Id FROM CartTax WHERE CartItemId IN :cartItemIdSet WITH SECURITY_ENFORCED];
        if(!cartTaxList.isEmpty()) {
            delete cartTaxList;
        }
    }
}