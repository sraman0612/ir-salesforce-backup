/**
	@author Providity
	@date 15APR2019
	@description A helper class that let's you execute SOQLs directly from Lightning Components
*/

public with sharing class LightningApexHelperController {
    @AuraEnabled
    public static List<sObject> executeSoql(String soql) {
        List<sObject> result = Database.query(soql);
        return result;
    }
    
    @AuraEnabled
    public static String getFields(String objectName, String fsName) {        
        if(String.isBlank(objectName)) return null;
        
        try {
        	if(!String.isBlank(fsName)) {
                Schema.FieldSet fieldSetObj = Schema.getGlobalDescribe().get(objectName).getDescribe().fieldSets.getMap().get(fsName);                
                return JSON.serialize(fieldSetObj.getFields());
            } else {
                return JSON.serialize(Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().keySet());
            }   
        } catch(Exception e) {
            System.debug('error**' + e.getMessage());
            return null;
        }     
    }
}