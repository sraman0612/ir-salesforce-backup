public with sharing class CTS_IOT_Asset_ListController {

    private class CTS_IOT_Asset_ListControllerException extends Exception{}

    @TestVisible private static Boolean forceError = false;

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Data_Service.AssetDetail[] assets = new CTS_IOT_Data_Service.AssetDetail[]{};
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
        @AuraEnabled public CTS_IOT_Community_Administration__c userSettings = CTS_IOT_Community_Administration__c.getInstance(UserInfo.getUserId());
        @AuraEnabled public Integer totalRecordCount = 0;
        @AuraEnabled public PicklistEntry[] equipmentTypeOptions = new PicklistEntry[]{};
    }

    private class PicklistEntry{
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public PicklistEntry(String label, String value){
            this.label = label;
            this.value = value;
        }
    }

    @AuraEnabled
	public static InitResponse getInit(String searchTerm, String siteId, Integer pageSize, Integer offSet, Integer totalRecordCount, String equipmentTypeFilter, String helixStatusFilter){
		
		InitResponse response = new InitResponse();
		
		try{				
            
            if (Test.isRunningTest() && forceError){
                throw new CTS_IOT_Asset_ListControllerException('test error');
            }        
            
            for (Schema.PicklistEntry pe : Schema.SObjectType.CTS_IOT_Frame_Type__c.fields.CTS_IOT_Type__c.getPicklistValues()){
                response.equipmentTypeOptions.add(new PicklistEntry(pe.getLabel(), pe.getValue()));
            }

            if (pageSize == null){
                pageSize = response.userSettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.userSettings.User_Selected_List_Page_Size__c) : (response.communitySettings.User_Selected_List_Page_Size__c != null ? Integer.valueOf(response.communitySettings.User_Selected_List_Page_Size__c) : null);
            }

            String whereClauseFilters = '';

            if (String.isNotBlank(helixStatusFilter)){
                whereClauseFilters = 'IRIT_RMS_Flag__c = ' + Boolean.valueOf(helixStatusFilter);
            }

            if (String.isNotBlank(equipmentTypeFilter)){
                whereClauseFilters += (String.isNotBlank(whereClauseFilters) ? ' and ' : '') + 'Equipment_Type__c = \'' + equipmentTypeFilter + '\'';
            }            

            response.assets = CTS_IOT_Data_Service.getCommunityUserAssets(searchTerm, String.isNotBlank(siteId) ? siteId : null, null, pageSize, offSet, whereClauseFilters);
            response.totalRecordCount = CTS_IOT_Data_Service.getCommunityUserAssetsCount(String.isNotBlank(siteId) ? siteId : null, searchTerm, whereClauseFilters);

            if (pageSize != null && (response.userSettings.User_Selected_List_Page_Size__c == null || pageSize != response.userSettings.User_Selected_List_Page_Size__c)){
                response.userSettings.User_Selected_List_Page_Size__c = pageSize;
                upsert response.userSettings;
            }            
		}
		catch(Exception e){
			system.debug('exception: ' + e.getMessage());
			response.success = false;
			response.errorMessage = e.getMessage();
		}
		
		return response;
	}   
}