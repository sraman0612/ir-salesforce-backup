// Created on 07/04/2020
// Created by Harish
// Task_BillTo_ShipTOInactiveCheck
// This Class prevents creation of Tasks on Bill TO and Inactive Ship To Accounts

public class Task_BillTo_ShipTOInactiveCheck {
    
    public static void BillTo_ShipTOInactiveCheck(List<Task>TaskList)
    {
        system.debug('This is New Code');
        Set<Id> accoundIdSet = new Set<Id>();
        for(Task t : TaskList)
        {
            system.debug('Inside New code : for loop');
            if(t.WhatId <> null)
            {
                Schema.SObjectType entityType = t.WhatId.getSObjectType();
                system.debug('entity Type is :' + entityType);
                //System.assert(entityType == Account.sObjectType);
                if(entityType == Account.sObjectType)
                {
                    accoundIdSet.add(t.WhatId);
                    System.debug('AccountIdSet Contains' +accoundIdSet);
                }
            }
        }
        // Query Account and Store into a Map 
        Map<Id , Account> accountMap = new Map<Id , Account>([SELECT Type , Status__c , RecordTypeName__c FROM Account where id IN : accoundIdSet]);
        system.debug('Map is :' + accountMap);
        // Loop through New task and then Check for the Type and Status
        for(task t : Tasklist)
        {
            if(t.WhatId <> null)
            {
                if(accountMap.containsKey(t.WhatId))
                {
                    Account acc = accountMap.get(t.WhatId);
                    system.debug('acc is :' + acc);
                    if(acc.Type == 'Bill To' || ((acc.Type == 'Ship To' || acc.Type == 'Prospect') && acc.Status__c == 'Inactive' && (acc.RecordTypeName__c == 'Site_Account_NA_Air' ||acc.RecordTypeName__c == 'CTS_Inactive_Account' || acc.RecordTypeName__c == 'CTS_New_AIRD_Account' || acc.RecordTypeName__c == 'CTS_AIRD_Distributor_End_User')))
                    {
                        t.addError(system.Label.Task_Not_allowed_Under_Bill_To);
                    }  
                }
            }
        }
    }
    
    public static void BillTo_ShipTOInactiveCheckEvent(List<Event>Eventlist)
    {
        system.debug('This is New Code');
        Set<Id> accoundIdSet = new Set<Id>();
        for(Event e : Eventlist)
        {
            system.debug('Inside New code : for loop');
            if(e.WhatId <> null)
            {
                Schema.SObjectType entityType = e.WhatId.getSObjectType();
                system.debug('entity Type is :' + entityType);
                //System.assert(entityType == Account.sObjectType);
                if(entityType == Account.sObjectType)
                {
                    accoundIdSet.add(e.WhatId);
                    System.debug('AccountIdSet Contains' +accoundIdSet);
                }
            }
        }
        // Query Account and Store into a Map 
        Map<Id , Account> accountMap = new Map<Id , Account>([SELECT Type , Status__c , RecordTypeName__c FROM Account where id IN : accoundIdSet]);
        system.debug('Map is :' + accountMap);
        // Loop through New task and then Check for the Type and Status
        for(Event e : Eventlist)
        {
            if(e.WhatId <> null)
            {
                if(accountMap.containsKey(e.WhatId))
                {
                    Account acc = accountMap.get(e.WhatId);
                    system.debug('acc is :' + acc);
                    if(acc.Type == 'Bill To' || ((acc.Type == 'Ship To' || acc.Type == 'Prospect') && acc.Status__c == 'Inactive' && (acc.RecordTypeName__c == 'Site_Account_NA_Air' ||acc.RecordTypeName__c == 'CTS_Inactive_Account' || acc.RecordTypeName__c == 'CTS_New_AIRD_Account' || acc.RecordTypeName__c == 'CTS_AIRD_Distributor_End_User')))
                    {
                        e.addError(system.Label.Task_Not_allowed_Under_Bill_To);
                    }  
                }
            }
        }
    }
}