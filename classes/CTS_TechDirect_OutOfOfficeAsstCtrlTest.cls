/**
 * Test class of CTS_TechDirect_OutOfOfficeAsstCtrl
 */
@isTest
public class CTS_TechDirect_OutOfOfficeAsstCtrlTest {
    
    @isTest
    public static void testSave(){
        createTestData();
        String init = CTS_TechDirect_OutOfOfficeAsstCtrl.getInit();
        System.assert(init != null);
        
        String updateResult = CTS_TechDirect_OutOfOfficeAsstCtrl.save(init);
        
    }
    
    @isTest
    public static void testFailure(){
        String init = CTS_TechDirect_OutOfOfficeAsstCtrl.getInit();
        System.assert(init != null);
 		// passing NULL to cover catch block 
        String updateResult = CTS_TechDirect_OutOfOfficeAsstCtrl.save(null);
        
    }
    
	
   
    public static void createTestData(){
        Out_of_Office_Settings__c ooo = CTS_TestUtility.createOutOfOfficeSetting(true, UserInfo.getUserId(), true);
        
        Out_of_Office_Case_Routings__c caseRouting = CTS_TestUtility.createOutOfOfficeCaseRouting(UserInfo.getUserId(), UserInfo.getUserId(), true);
    }
}