@IsTest
public class C_AttachmentTest {

    @TestSetup
    public static void setup() {
        Case testCase = New Case();
        testCase.Subject = 'Test Coverage Case';
        testCase.Assigned_From_Address_Picklist__c = 'test@test.com';
        Case testParent = New Case();
        testParent.Subject = 'Test Coverage Parent Case';
        testParent.Assigned_From_Address_Picklist__c = 'test@test.com';
        Database.insert(testParent);
        testCase.ParentId = testParent.Id;
        Database.insert(testCase);
        
        /*EmailMessage testEmail = New EmailMessage();
        testEmail.ParentId = testCase.Id;
        testEmail.FromAddress = 'test@test.com';
        testEmail.ToAddress = 'test@test.com';
        Database.insert(testEmail);*/
        
        Attachment testAtt = New Attachment();
        testAtt.Name = 'Test Attachment';
        testAtt.Body = Blob.valueOf('Test Attachment Body');
        testAtt.ParentId = testCase.Id;
        Database.insert(testAtt);
    }
    
    @IsTest
    public static void test() {
        Case testCase = [SELECT Id, ParentId, Subject FROM Case WHERE Subject = 'Test Coverage Case' LIMIT 1];
       
        ApexPages.StandardController stdCtrl = New ApexPages.StandardController(testCase);
        C_Attachments_Extension attExt = New C_Attachments_Extension(stdCtrl);
        
        attExt.redo();
        attExt.cloneAttachments();
        attExt.deleteAttachments();
        attExt.getTopLevelCase(testCase.Id);
    }
    
}