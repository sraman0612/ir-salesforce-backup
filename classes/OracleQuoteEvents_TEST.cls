@isTest
Public class OracleQuoteEvents_TEST {
  static testmethod void test1(){
    /*Division__c divn = new Division__c();
    divn.Name = 'Test Division';
    divn.Division_Type__c = 'Customer Center';
    divn.EBS_System__c = 'Oracle 11i';     
   	insert divn;  */   
    
    Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
    Account a = new Account();
    a.Name = 'Test Acct for Oracle Quote';
    a.BillingCity = 'srs_!city';
    a.BillingCountry = 'USA';
    a.BillingPostalCode = '674564569';
    a.BillingState = 'CA';
    a.BillingStreet = '12, street1678';
    a.Siebel_ID__c = '123456';
    a.ShippingCity = 'city1';
    a.ShippingCountry = 'USA';
    a.ShippingState = 'CA';
    a.CTS_Global_Shipping_Address_1__c = '13, street2';
	a.CTS_Global_Shipping_Address_2__c = '14';
    a.ShippingPostalCode = '123';  
    a.County__c = 'testCounty';
    a.RecordTypeId = AirNAAcctRT_ID;
 // a.Account_Division__c= 'a2c4Q000006rW5NQAU'; //Ritesh:Added division to avoid division validation on Accounts.
 //   a.Account_Division__c = divn.Id;
    insert a;
    Id airOppRTID = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
    Opportunity o = new Opportunity();
    o.name = 'Test Opportunity';
    o.stagename = 'Stage 1. Qualify';
    o.amount = 0;
    o.closedate = system.today();
    o.Accountid=a.Id;
    o.RecordTypeId = airOppRTID;
    insert o;
    test.startTest();
    cafsl__Oracle_Quote__c oq  = new cafsl__Oracle_Quote__c();
    oq.cafsl__Opportunity__c = o.Id;
    oq.Quote_Amount__c = 1;
      try{
          insert oq;
      }
      catch(Exception e){
          system.debug(e);
      }
    /* quote should get marked as primary */
   // system.assertEquals(TRUE, [SELECT Primary__c FROM cafsl__Oracle_Quote__c WHERE Id = :oq.Id].Primary__c);
    /* quote amount should be updated to opp */
    //system.assertEquals(1,[SELECT Amount,Oracle_Quote_Amount__c FROM Opportunity WHERE Id =:o.Id].Oracle_Quote_Amount__c);
    /* add new quote under opp, does not get marked as primary, amount not updated to opp */
    cafsl__Oracle_Quote__c oq2  = new cafsl__Oracle_Quote__c();
    oq2.cafsl__Opportunity__c = o.Id;
    oq2.Quote_Amount__c = 2;
   try{
          insert oq2;
      }
      catch(Exception e){
          system.debug(e);
      }
    //system.assertEquals(FALSE, [SELECT Primary__c FROM cafsl__Oracle_Quote__c WHERE Id = :oq2.Id].Primary__c);
    //system.assertEquals(1,[SELECT Amount,Oracle_Quote_Amount__c FROM Opportunity WHERE Id =:o.Id].Oracle_Quote_Amount__c);
    /* mark new as primary, validate opp reflects amount of new quote and old quote is no longer primary */
    oq2.Primary__c = TRUE;
    oq2.Quote_Type__c = 'Budgetary';
    try{
          update oq2;
      }
      catch(Exception e){
          system.debug(e);
      }
    //system.assertEquals(FALSE, [SELECT Primary__c FROM cafsl__Oracle_Quote__c WHERE Id = :oq.Id].Primary__c);
    //system.assertEquals(TRUE, [SELECT Primary__c FROM cafsl__Oracle_Quote__c WHERE Id = :oq2.Id].Primary__c);
    //system.assertEquals(2,[SELECT Amount,Oracle_Quote_Amount__c FROM Opportunity WHERE Id =:o.Id].Oracle_Quote_Amount__c);
    /* update amount on quote, opp should get updated */
    oq2.Quote_Amount__c = 3;
    try{
          update oq2;
      }
      catch(Exception e){
          system.debug(e);
      }
    //system.assertEquals(3,[SELECT Amount,Oracle_Quote_Amount__c FROM Opportunity WHERE Id =:o.Id].Oracle_Quote_Amount__c);
    try{
      delete oq;
    } catch (exception e){
    }
    test.stopTest();
  }
}