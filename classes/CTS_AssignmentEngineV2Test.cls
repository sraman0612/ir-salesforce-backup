@isTest
private class CTS_AssignmentEngineV2Test {
    
    // Method to test lead trigger
    static testMethod void testLeadTrigger() {
        
        Group q = new Group(name = 'Test Queue', developername = 'Test_Queue', type = 'Queue');
        insert q;
        
        QueueSobject mappingObject = new QueueSobject(QueueId = q.Id, SobjectType = 'Lead');
        //insert mappingObject;
        System.runAs(new User(Id = UserInfo.getUserId()))
        {insert mappingObject;}
        Assignment_Group_Name__c agn1 = new Assignment_Group_Name__c(name = 'Test AGN 1');
        insert agn1;
        
        Assignment_Group_Queues__c agq1 = new Assignment_Group_Queues__c(Assignment_Group_Name__c = agn1.Id, name = 'Test Queue');
        insert agq1;
        
        Assignment_Groups__c ag1 = new Assignment_Groups__c();
        ag1.group_name__c = agn1.Id;
        ag1.state__c = 'New York';
        ag1.county__c = 'test county';
        ag1.active__c = 'True';
        ag1.Assignment_Key__c = 'New Yorktest county';
        ag1.Zone__c = 'B12';
        ag1.user__c = UserInfo.getUserId();
        insert ag1;
        Test.startTest();
        Lead l = new Lead();
        l.firstname = 'George';
        l.lastname = 'Acker';
        l.phone = '1234567890';
        l.City = 'city';
        l.Country = 'USA';
        l.PostalCode = '12345';
        l.State = 'AK';
        l.Street = '12, street1';
        l.company = 'ABC Corp';
        l.county__c = 'test county';
        l.ownerId = q.Id;
        l.Assignment_Key__c = 'New Yorktest county';
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
        
        insert l;
        List<Lead> updatedLeadList = [SELECT Zone__c FROM Lead WHERE id=:l.Id]; 
        //system.assertEquals(updatedLeadList[0].Zone__c, 'B12');
        Test.stopTest();
    }
    
}