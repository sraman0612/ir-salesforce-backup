@IsTest 
public class TestContactPrimacycheck 
{
    
    public static testMethod void multiplePrimaryCheck()
    {
        /*Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;*/
        
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
        a.BillingState = 'OH';
        a.BillingCountry='USA';
//		a.Account_Division__c = divn.Id;
        insert a;
        //return a;
        
        Contact c = new Contact(FirstName = 'Test1');
        c.Email = 'Test1@Test1.com';
        c.Title = 'Mr';
        c.lastname = 'test1';
        c.RecordTypeId  = '012j0000000m7kqAAA';
        c.Primary__c = true;
        c.Phone = '1212121212';
        c.accountid = a.Id;
        insert c;
        
        Contact c1 = new Contact(FirstName = 'Test12');
        c1.Email = 'Test2@Test1.com';
        c1.Title = 'Mr';
        c1.lastname = 'test2';
        c1.RecordTypeId  = '012j0000000m7kqAAA';
        c1.accountid = a.Id;
        c1.Phone = '1212121212';
        //c1.Primary__c = true;
        insert c1;
        
    }
    
    public static testMethod void multiplePrimaryCheckupdate()
    {
        /*Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;*/
               
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
        a.BillingState = 'OH';
        a.BillingCountry='USA';
//		a.Account_Division__c = divn.Id;   
        insert a;
        //return a;
        
        Contact c = new Contact(FirstName = 'Test1');
        c.Email = 'Test1@Test1.com';
        c.Title = 'Mr';
        c.lastname = 'test1';
        c.RecordTypeId  = '012j0000000m7kqAAA';
        c.Primary__c = true;
        c.Phone = '1212121212';
        c.accountid = a.Id;
        insert c;
        
        Contact c1 = new Contact(FirstName = 'Test2');
        c1.Email = 'Test2@Test1.com';
        c1.Title = 'Mr';
        c1.lastname = 'test2';
        c1.RecordTypeId  = '012j0000000m7kqAAA';
        c1.accountid = a.Id;
        c1.Phone = '1212121212';
        insert c1;
        //c1.Primary__c=true;
        update c1;
        //System.assertEquals(c1.Primary__c,true);
        
    }
        public static testMethod void makrFirstContactPrimary()
    {
        /*Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;*/
               
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
//        a.Account_Division__c = divn.Id;
        a.BillingState = 'OH';
        a.BillingCountry='USA';
        insert a;
        //return a;
        
        Contact c = new Contact(FirstName = 'Test1');
        c.Email = 'Test1@Test1.com';
        c.Title = 'Mr';
        c.lastname = 'test1';
        c.RecordTypeId  = '012j0000000m7kqAAA';
        c.Phone = '1212121212';
        c.accountid = a.Id;
        insert c;
    }
    
   /* public static testMethod void Taskcheck()
    {
        Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;
               
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
		a.Account_Division__c = divn.Id;
        insert a;
        //return a;  
        
        Task t = new task(Subject = 'TEST');
        t.OwnerId = '005j000000BwPoqAAF';
        t.Subject = 'Test';
        t.Status = 'Not Started';
        t.Priority = 'Normal';
        t.WhatId = a.Id;
        insert t;
    }  */
    
   /* public static testMethod void Eventcheck()
    {
        Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;
               
        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Site_Account_NA_Air').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
		a.Account_Division__c = divn.Id;
        insert a;
        //return a;  
        
        Event t = new Event(Subject = 'TEST');
        t.OwnerId = '005j000000BwPoqAAF';
        t.StartDateTime =  Date.today();
        t.EndDateTime =  Date.today();
        t.WhatId = a.Id;
        insert t;
    }*/

        public static testMethod void ImageTest() {
        /*Division__c divn = new Division__c();
    	divn.Name = 'Test Division';
    	divn.Division_Type__c = 'Customer Center';
    	divn.EBS_System__c = 'Oracle 11i';     
   		insert divn;*/

        Id AirNAAcctRT_ID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();    
        Account a = new Account(Name = 'TestCPQAccount');
        a.ShippingCountry = 'USA';
//        a.ShippingStreet = '123';
  		a.CTS_Global_Shipping_Address_1__c = '1 street 1';
    	a.CTS_Global_Shipping_Address_2__c = 'street 2';
    	a.RecordTypeId = AirNAAcctRT_ID;            
        a.ShippingPostalCode = '123';
        a.ShippingCity = 'test';
        a.ShippingState = 'CA';
        a.County__c = '123';
        a.WasConverted__c = true;
//        a.Account_Division__c = 'a2c4Q000006rW5n';
//        a.Account_Division__c = divn.Id;
        a.BillingState = 'OH';
        a.BillingCountry='USA';
        insert a;
            
            ContentVersion docVersion = new ContentVersion();
            docVersion.Title = '24032020_12356';
            docVersion.PathOnClient = docVersion.Title + '.png';
            docVersion.VersionData = Blob.valueOf('Test Content');
            docVersion.IsMajorVersion = true;
            docVersion.ContentLocation = 'S';
            //docVersion.Account_Initiative__c = a.Id ;
            //docVersion.
            insert docVersion;
ContentVersion docQuery = [SELECT ContentDocumentId FROM ContentVersion WHERE Title = '24032020_12356' LIMIT 1];
ContentDocumentLink docLinkQuery = [SELECT Id,Visibility FROM ContentDocumentLink WHERE ContentDocumentId =: docQuery.ContentDocumentId LIMIT 1];
            System.assertEquals('AllUsers', docLinkQuery.Visibility);
        }
}