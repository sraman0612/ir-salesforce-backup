//
// (c) 2015, Appirio Inc
//
// Utility to "soft disable" triggers via a custom setting
//
// May 8, 2015     George Acker     original
//
public class Utility_Trigger_SoftDisable
{
    private Trigger_Soft_Disable__c custSetting;
    
    public Utility_Trigger_SoftDisable(String table)
    {
        
        if(null!=Trigger_Soft_Disable__c.getInstance(table))
          custSetting = Trigger_Soft_Disable__c.getInstance(table);
    }
    
    public boolean insertDisabled()
    {
        return custSetting != null && custSetting.disable_insert__c;
    }
    
    public boolean updateDisabled()
    {
        return custSetting != null && custSetting.disable_update__c;
    }
    
    public boolean deleteDisabled()
    {
        return custSetting != null && custSetting.disable_delete__c;
    }
    
    public boolean undeleteDisabled()
    {
        return custSetting != null && custSetting.disable_undelete__c;
    }
}