@IsTest
private class B2BCartShippingCostCalculatorTest {
	@testSetup
	static void setup() {
		UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' limit 1];
		User usr = CTS_TestUtility.createUser(false);
		usr.UserRoleId = r1.Id;
		usr.LastName = 'test ship';
		insert usr;
		PermissionSet ps1 = [SELECT Id FROM PermissionSet WHERE Name = 'B2BAdmin'];
		PermissionSet ps2 = [SELECT Id FROM PermissionSet WHERE Name = 'B2B_Admin_Additional_Access'];
		PermissionSetAssignment psa1 = new PermissionSetAssignment
				(AssigneeId = usr.Id, PermissionSetId = ps1.Id);
		insert psa1;
		PermissionSetAssignment psa2 = new PermissionSetAssignment
				(AssigneeId = usr.Id, PermissionSetId = ps2.Id);
		insert psa2;
		System.runAs(usr) {
			Account testAccount = new Account(Name = 'TestAccount');
			insert testAccount;
			BuyerAccount testBuyerAccount = new BuyerAccount(BuyerId = testAccount.Id, Name = 'test Buyer Account', IsActive = true);
			insert testBuyerAccount;
			WebStore testWebStore = new WebStore(Name = 'TestWebStore');
			insert testWebStore;

			BuyerGroup testBuyerGroup = new BuyerGroup(Name = 'test Buyer Group', Shipping_Charge__c = 5);
			insert testBuyerGroup;

			BuyerGroupMember testBuyerGroupMember = new BuyerGroupMember(BuyerGroupId = testBuyerGroup.Id, BuyerId = testAccount.Id);
			insert testBuyerGroupMember;

			Account account = [SELECT Id FROM Account WHERE Name = 'TestAccount' LIMIT 1];
			WebStore webStore = [SELECT Id FROM WebStore WHERE Name = 'TestWebStore' LIMIT 1];
			WebCart cart = new WebCart(Name = 'Cart', WebStoreId = webStore.Id, AccountId = account.Id);
			insert cart;

			CartDeliveryGroup cartDeliveryGroup = new CartDeliveryGroup(CartId = cart.Id, Name = 'Robuschi Shipping');
			insert cartDeliveryGroup;

			CartItem cartItem = new CartItem(CartId = cart.Id, Type = 'Product', Name = 'TestProduct', CartDeliveryGroupId = cartDeliveryGroup.Id, Quantity = 4);
			insert cartItem;
		}
	}

	@IsTest
	static void calculateShippingCostSuccessTest() {
		UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
		User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id  AND LastName = 'test ship' LIMIT 1];
		System.runAs(user) {
			B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
			sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
			WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
			integInfo.jobId = null;

			Test.startTest();
			sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);
			//System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
			Test.stopTest();
		}
	}

	@IsTest
	static void calculateShippingCostSuccessTest2() {
		UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
		User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id  AND LastName = 'test ship' LIMIT 1];
		System.runAs(user) {
			B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
			Product2 shippingChargeProduct = new Product2(isActive = true, Name = cartShippingCostCalculator.SHIPPING_CHARGE_PRODUCT2_NAME);
			insert shippingChargeProduct;

			OrderDeliveryMethod defaultOrderDeliveryMethod = new OrderDeliveryMethod(
					Name = cartShippingCostCalculator.DEFAULT_DELIVERY_METHOD_NAME,
					Carrier = null,
					isActive = true,
					ProductId = shippingChargeProduct.Id,
					ClassOfService = null
			);
			insert defaultOrderDeliveryMethod;
			sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
			WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
			integInfo.jobId = null;

			Test.startTest();
			sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);
			//System.assertEquals(sfdc_checkout.IntegrationStatus.Status.SUCCESS, integrationResult.status);
			Test.stopTest();
		}
	}

	@isTest
	static void calculateShippingCostFailTest() {
		UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
		User user = [SELECT Id FROM User WHERE UserRoleId = :r1.Id  AND LastName = 'test ship' LIMIT 1];
		System.runAs(user) {
			B2BCartShippingCostCalculator cartShippingCostCalculator = new B2BCartShippingCostCalculator();
			sfdc_checkout.IntegrationInfo integInfo = new sfdc_checkout.IntegrationInfo();
			integInfo.jobId = null;
			WebCart webCart = [SELECT Id FROM WebCart WHERE Name = 'Cart' LIMIT 1];
			List<cartDeliveryGroup> cartDeliveryGroupList = [SELECT Id FROM cartDeliveryGroup];
			delete cartDeliveryGroupList;

			Test.startTest();
			sfdc_checkout.IntegrationStatus integrationResult = cartShippingCostCalculator.startCartProcessAsync(integInfo, webCart.Id);

			System.assertEquals(sfdc_checkout.IntegrationStatus.Status.FAILED, integrationResult.status);
			List<CartValidationOutput> cartValidationOutputs = [SELECT Id FROM CartValidationOutput WHERE Level = 'Error'];
			System.assertEquals(1, cartValidationOutputs.size());
			Test.stopTest();
		}
	}
}