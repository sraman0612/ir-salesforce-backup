public class C_MiddleMan_Controller {

    public PageReference redirect() {
        return new PageReference('/'+dest);
    }


    public String origin {get; private set;}
    public String dest {get; private set;}
    public String tabName {get; private set;}
    public String prefix {get; private set;}
    
    public C_MiddleMan_Controller(){
         origin = Apexpages.currentpage().getparameters().get('origin');
         dest = Apexpages.currentpage().getparameters().get('dest');
         tabName = Apexpages.currentpage().getparameters().get('name');
         prefix = Apexpages.currentpage().getparameters().get('prefix');
    }

}