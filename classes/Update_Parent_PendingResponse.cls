public class Update_Parent_PendingResponse {
    public static void updateParentCaseStatusMethod(List<Case> newCase){
        Map<Id, RecordType> recordTypesAI = new Map<Id,RecordType>([SELECT Id,Name FROM RecordType WHERE SobjectType='Case' and DeveloperName IN ('Action_Item', 'Internal_Case')]);
		for(Case c : newCase){
                  if(recordTypesAI.keySet().contains(c.RecordTypeId) && c.parentId != NULL){
              			C_Utils.updateParentStatus(c.parentId);       
                }
            }
    }
}