/**
 * Created by CloudShapers on 12/11/2020.
 */

@IsTest
private class CTS_WorkOrderTriggerHandlerTest {
    static final Id CTS_RECORDTYPE_WORKORDER = Schema.SObjectType.WorkOrder.getRecordTypeInfosByDeveloperName().get('Work_Order_CTS_Field_Service').getRecordTypeId();

    @isTest static void testCreateEntitlements() {
        List<Account> accounts = CTS_TestDataFactory.createLatamAccounts(1);
        Test.startTest();
        List<WorkOrder> workOrders = CTS_TestDataFactory.createTestWorkOrder(1, false, new Map<String, String>{
                'RecordTypeId' => CTS_RECORDTYPE_WORKORDER
        });
        workOrders[0].AccountId = accounts[0].id;
        insert workOrders;
        
        //Lets insert some line items here!
        
        WorkOrderLineItem woLineItem = new WorkOrderLineItem();
        woLineItem.WorkOrderId = workOrders[0].Id;
        
        Id ctsWoliRecordTypeId = Schema.SObjectType.WorkOrderLineItem.getRecordTypeInfosByDeveloperName().get('CTS_WOLI_Record_Type').getRecordTypeId();
        woLineItem.RecordTypeId=ctsWoliRecordTypeId;
        insert woLineItem;
        
        delete woLineItem;
        
        workOrders = [
                SELECT Id,EntitlementId
                FROM WorkOrder
                WHERE Id = :workOrders[0].Id
        ];
        
        
        
        System.assertEquals(workOrders[0].EntitlementId != null, true);
        delete workorders;
        Test.stopTest();
    }
}