public with sharing class PT_Sellings {
    @AuraEnabled
    public static PTSellingsWrapper getYears(Id accId) {
        String thisYear = String.valueOf(System.Today().year());
        List<String> allYears = new List<String>();
        List<String> allOpts = new List<String>();
        Schema.DescribeFieldResult fieldResult = PT_Selling__c.Year__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        // Add these values to the selectoption list.
        allOpts.add(thisYear);
        for (Schema.PicklistEntry a: values) {
            if(a.getValue() != thisYear){
            	allOpts.add(a.getValue());
            }
        }
        
        List<PT_Selling__c> ptSellinglst = getSellings(accId, thisYear);
        
        PTSellingsWrapper thisPtSellingWrap = 
            	new PTSellingsWrapper(allOpts, ptSellinglst);
        
        return thisPtSellingWrap;
    }
    
    @AuraEnabled
    public static List<PT_Selling__c> getSellings(Id accId, String year) {
        System.debug('Server');
        List<PT_Selling__c> ptSellinglst = [SELECT Id, Name, PT_Selling_Out__c FROM PT_Selling__c WHERE PT_Account__c =:accId AND Year__c =: year];
        if(ptSellinglst.isEmpty() && ptSellinglst.size() <=0){
            Schema.DescribeFieldResult fieldResult = PT_Selling__c.Month__c.getDescribe();
            List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
            List<PT_Selling__c> newPtSellinglst = new List<PT_Selling__c>();
            for (Schema.PicklistEntry a: values) {
                
                PT_Selling__c ptSelling = new PT_Selling__c();
                ptSelling.Year__c = Year;
                ptSelling.Month__c = a.getValue();
                ptSelling.Name = a.getValue() + ' - ' + Year;
                ptSelling.PT_Account__c = accId;
                newPtSellinglst.add(ptSelling);
            }
            insert newPtSellinglst;
            ptSellinglst.addAll(newPtSellinglst);
        }
        System.debug('Server2');
        return ptSellinglst;
    }
    
    @AuraEnabled
    public static void savePTSellings(Id accId, String year,
                                      List<PT_Selling__c> sellingLst) {
        
        update sellingLst;
    }
    
    
    
    public class PTSellingsWrapper {
        @AuraEnabled List<String> allYears{ get; set; }
        @AuraEnabled List<PT_Selling__c> ptSellinglst { get; set; }

        public PTSellingsWrapper(List<String> allYears,
                                 List<PT_Selling__c> ptSellinglst) {
             this.allYears = allYears;
             this.ptSellinglst = ptSellinglst;                                
        }
    }
}