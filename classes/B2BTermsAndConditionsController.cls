public with sharing class B2BTermsAndConditionsController {

    @AuraEnabled
    public static void updateCart(String recordId) {
        try {
            if (String.isNotEmpty(recordId)) {
                WebCart webCart = new WebCart();
                webCart.Id = recordId;
                webCart.Terms_and_conditions__c = true;
                update webCart;
            } else {
                throw new AuraHandledException('cart Id not provided');
            }
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}