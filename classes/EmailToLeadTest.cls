@isTest
private class EmailToLeadTest {

    @isTest static void testEmailService() {
        // Create a test email
        Messaging.InboundEmail testEmail = new Messaging.InboundEmail();
        testEmail.subject = 'Test Subject';
        testEmail.fromName = 'Test Sender';
        testEmail.fromAddress = 'test@example.com';
        testEmail.plainTextBody = 'This is a test email body.';
        
        List<Messaging.InboundEmail.BinaryAttachment> mockAttachments = new List<Messaging.InboundEmail.BinaryAttachment>();
        Messaging.InboundEmail.BinaryAttachment mockAttachment1 = new Messaging.InboundEmail.BinaryAttachment();
        mockAttachment1.fileName = 'TestAttachment1.txt';
        mockAttachment1.body = Blob.valueOf('Test attachment 1 content');
        mockAttachments.add(mockAttachment1);
        testEmail.binaryAttachments = mockAttachments;
        
        // Call the email service handler
        Messaging.InboundEmailResult result = new EmailToLead().handleInboundEmail(testEmail, null);

        // Check if the lead was created
        List<Lead> leads = [SELECT Id, FirstName, LastName, Company, Business__c, Brand__c, CTS_Product_Type__c, Description, recordtype.name FROM Lead WHERE FirstName = 'Test Sender'];
        System.assertEquals(1, leads.size());
        Lead newLead = leads[0];
        System.assertEquals('Test Sender', newLead.FirstName);
        System.assertEquals('Lead', newLead.LastName);
        System.assertEquals('test@example.com', newLead.Company);
        System.assertEquals('Standard Lead', newLead.recordtype.name);
        System.assertEquals('ADI', newLead.Business__c,'Lead Business should be ADI');
        System.assertEquals('Medical', newLead.CTS_Product_Type__c,'Lead Product Type should be Medical');
        System.assertEquals('ADI', newLead.Brand__c,'Lead Business should be ADI');
        System.assertEquals('This is a test email body.', newLead.Description);

        // Check if the email service result was successful
        System.assertEquals(true, result.success);
    }
}