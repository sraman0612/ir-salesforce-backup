@IsTest
global class B2BCustomSearchCalloutMock404 implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"statusCode": 404, "timestamp": "2024-12-16T15:17:56.562Z","path": "/ui/api/v1/model/asset-serial-number/RB24095860"}');
        res.setStatusCode(404);
        return res;
    }
}