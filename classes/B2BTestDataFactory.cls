@IsTest
public with sharing class B2BTestDataFactory {
    public static WebStore createWebStore(String name) {
        return new WebStore(
                Name = name
        );
    }

    public static ProductCatalog createCatalog(String name) {
        return new ProductCatalog(
                Name = name
        );
    }

    public static ProductCategory createProductCategory(String name, Id catalogId) {
        return new ProductCategory(
                Name = name,
                CatalogId = catalogId
        );
    }

    public static WebStoreCatalog createWebStoreCatalog(Id webstoreId, Id catalogId) {
        return new WebStoreCatalog(
                SalesStoreId = webstoreId,
                ProductCatalogId = catalogId
        );
    }

    public static Account createAccount(String name){
        return new Account(
                Name = name
        );
    }

    public static Contact createContact(Id accountId, String firstName, String lastName) {
        return new Contact(
                AccountId = accountId,
                FirstName = firstName,
                LastName = lastName
        );
    }

    public static Product2 createProduct(String name, String sku) {
        return new Product2(
                Name = name,
                StockKeepingUnit = sku,
                ProductCode = sku,
                IsActive = true
        );
    }

    public static Order createOrder(Id accountId, String status, String currencyIsoCode, Date effectiveDate){
        return new Order(
                AccountId = accountId,
                Status = status,
                CurrencyIsoCode = currencyIsoCode,
                EffectiveDate = effectiveDate
        );
    }

    public static OrderItem createOrderItem(Id orderId, Id productId, Decimal quantity, Decimal unitPrice, Decimal listPrice) {
        return new OrderItem(
                OrderId = orderId,
                Product2Id = productId,  // This field links to the Product object
                Quantity = quantity,
                UnitPrice = unitPrice,
                ListPrice = listPrice
        );
    }

    public static void setupStandardPricebook() {
        Id pricebookId = Test.getStandardPricebookId();
        Pricebook2 standardPricebook = new Pricebook2(
                Id = pricebookId,
                IsActive = true
        );
        update standardPricebook;
    }

    public static OrderDeliveryMethod createOrderDeliveryMethod(Id productId, String name){
        return new OrderDeliveryMethod(
                ProductId = productId,
                Name = name
        );
    }


    public static OrderDeliveryGroup createOrderDeliveryGroup(Id orderId, Id deliveryMethodId, String deliverToName, String deliverToStreet, String deliverToCity, String deliverToStateCode, String deliverToPostalCode, String deliverToCountryCode, Date desiredDeliveryDate){
        return new OrderDeliveryGroup(
                OrderId = orderId,
                OrderDeliveryMethodId = deliveryMethodId,
                DeliverToName = deliverToName,
                DeliverToStreet = deliverToStreet,
                DeliverToCity = deliverToCity,
                DeliverToPostalCode = deliverToPostalCode,
                DeliverToCountry = deliverToCountryCode,
                DesiredDeliveryDate = desiredDeliveryDate
        );
    }

    public static OrderPaymentSummary createOrderPaymentSummary (Id orderSummaryId, String type, String method, String currencyCode) {
        return new OrderPaymentSummary(
                OrderSummaryId = orderSummaryId,
                Type = type,
                Method = method,
                CurrencyIsoCode = currencyCode
        );
    }

    public static PaymentAuthorization createPaymentAuthorization (Id accountId, Id orderPaymentSummaryId, Id paymentMethodId, Decimal amount, String status, String processMode, String currencyCode) {
        return new PaymentAuthorization(
                AccountId = accountId,
                OrderPaymentSummaryId = orderPaymentSummaryId,
                PaymentMethodId = paymentMethodId,
                Amount = amount,
                Status = status,
                ProcessingMode = processMode,
                CurrencyIsoCode = currencyCode
        );
    }

    public static Payment createPayment(Id accountId, Id orderPaymentSummaryId, Decimal amount, String status, String type, String processMode, String currencyCode) {
        return new Payment(
                AccountId = accountId,
                OrderPaymentSummaryId = orderPaymentSummaryId,
                Amount = amount,
                Status = status,
                Type = type,
                ProcessingMode = processMode,
                CurrencyIsoCode = currencyCode
        );
    }

    public static CardPaymentMethod createCardPaymentMethod(Id accountId, String status, String processingMode, String cardCategory) {
        return new CardPaymentMethod(
                AccountId = accountId,
                Status = status,
                ProcessingMode = processingMode,
                CardCategory = cardCategory
        );
    }

    public static Refund createRefund (Id accountId, Id orderPaymentSummaryId, Id paymentId, Decimal amount, String status, String type, String processMode, String currencyCode) {
        return new Refund (
                AccountId = accountId,
                OrderPaymentSummaryId = orderPaymentSummaryId,
                PaymentId = paymentId,
                Amount = amount,
                Status = status,
                Type = type,
                ProcessingMode = processMode,
                CurrencyIsoCode = currencyCode
        );
    }
}