@isTest
public with sharing class CTSCustomerCommunityResetPassFlowSvcTest {

    @testSetup
    public static void createData(){
		UserRole userRole_1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'CTS_MEIA_East_Sales_Manager' LIMIT 1];
        User admin = [SELECT Id, Username, UserRoleId,isActive FROM User WHERE Profile.Name = 'System Administrator' And isActive = true LIMIT 1];
        admin.UserRoleId = userRole_1.id;
        update admin;
        System.runAs(admin) {
        Notification_Settings__c notificationSettings = NotificationsTestDataService.createNotificationSettings();
        CTS_IOT_Community_Administration__c settings = CTS_TestUtility.setDefaultSetting(true);

        Account shipTo1 = CTS_TestUtility.createAccount('Test Ship To1', true);

        Contact standCommContact = CTS_TestUtility.createContact('Contact123','Test', 'testcts1@gmail.com', shipTo1.Id, false);
        standCommContact.CTS_IOT_Community_Status__c = 'Approved';
        standCommContact.MobilePhone = '555-555-5555';
        insert standCommContact;

        User standCommUser = CTS_TestUtility.createUser(standCommContact.Id, CTS_IOT_Data_Service_WithoutSharing.defaultStandardUserCommunityProfile.Id, false);    
        standCommUser.LastName = 'Comm_Stand_User_Test1';
        insert standCommUser;    
    }    
}
    @isTest
    private static void resetPasswordTest(){

        User u = [Select Id From User Where LastName = 'Comm_Stand_User_Test1' LIMIT 1];

        Test.startTest();

        CTSCustomerCommunityResetPassFlowService.resetPassword(new List<Id>{u.Id});

        Test.stopTest();
    }
}