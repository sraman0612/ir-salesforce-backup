/*------------------------------------------------------------
Author:       Mahesh Karadkar(Cognizant)
Description:  This class sends the 'email verifaction' mail to user.
			  This class is being called from flow 'Force_email_verification_for_users'.
Case #03236104 
------------------------------------------------------------*/

public class SendVerificationEmailAction {
    public static Boolean isEmailSent = False;
    /*
    @Method Name    : sendEmail
    @author         : Mahesh Karadkar (AMS Team)
    @description    : This Method sends the 'email verifaction' mail to user.
						#03236104 - Dec24 Release
    @param          : newList : List of FlowInput
    @return         : void  
    */
     @InvocableMethod(label='Send Verification Email' category='Email' description='Send Emails to users to verify their email address')
    public static void sendEmail(List<FlowInput> lstInputs){
        
        if(lstInputs != null && !lstInputs.isEmpty()){
            isEmailSent = System.UserManagement.sendAsyncEmailConfirmation(
                lstInputs[0].userId, 
                lstInputs[0].emailTemplateId, 
                lstInputs[0].networkId, 
                lstInputs[0].landingPageUrl
            );
        }
    }
    
    public class FlowInput{
        @InvocableVariable(required=true) public Id userId;
        @InvocableVariable public Id emailTemplateId;
        @InvocableVariable public Id networkId;
        @InvocableVariable public String landingPageUrl;
    }   
    
}