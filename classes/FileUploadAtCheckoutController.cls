public with sharing class FileUploadAtCheckoutController {
    @AuraEnabled
    public static CartDetails getCartDetails(Id cartId) {
        Decimal amount = 0;
        String paymentOption = '';
        WebCart cart = [SELECT Id, GrandTotalAmount, AccountId FROM WebCart WHERE Id = :cartId LIMIT 1];
        if (cart != null) {
            amount = cart.GrandTotalAmount;
            if (cart.AccountId != null) {
                Account acc = [SELECT Robuschi_B2B_Store_Payment_Option__c FROM Account WHERE Id = :cart.AccountId LIMIT 1];
                paymentOption = acc.Robuschi_B2B_Store_Payment_Option__c;
            }
        }
        return new CartDetails(amount, paymentOption);
    }

    @AuraEnabled
    public static void uploadFile(String base64, String filename, String recordId) {
        ContentVersion cv = createContentVersion(base64, filename);
        updateCart(recordId, cv.Id);
    }

    @TestVisible
    private static void updateCart(String recordId, String contentVersionId) {
        if (String.isNotEmpty(recordId)) {
            WebCart webCart = new WebCart();
            webCart.Id = recordId;
            webCart.Content_Document_Id__c = contentVersionId;
            update webCart;
        } else {
            throw new AuraHandledException('cart Id not provided');
        }
    }

    @TestVisible
    private static ContentVersion createContentVersion(String base64, String filename) {
        try {
            ContentVersion cv = new ContentVersion();
            cv.VersionData = EncodingUtil.base64Decode(base64);
            cv.Title = filename;
            cv.PathOnClient = filename;
            insert cv;
            return cv;
        } catch (DMLException ex) {
            throw new AuraHandledException(ex.getMessage());
        } catch(AuraHandledException ex) {
            throw ex;
        } catch(Exception ex) {
            throw ex;
        }
    }

    @Future
    public static void createContentLink(String contentVersionId, String recordId) {
        if (contentVersionId != null && recordId != null) {
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.ContentDocumentId = [ SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionId].ContentDocumentId;
            cdl.LinkedEntityId = recordId;
            // ShareType is either 'V', 'C', or 'I'(V = Viewer, C = Collaborator, I = Inferred)
            cdl.ShareType = 'V';
            try {
                insert cdl;
            } catch (DMLException ex) {
                throw new AuraHandledException(ex.getMessage());
            } catch(AuraHandledException ex) {
                throw ex;
            } catch(Exception ex) {
                throw ex;
            }
        }
    }

    public class CartDetails {
        @AuraEnabled public Decimal totalAmount;
        @AuraEnabled public String paymentOption;

        public CartDetails(Decimal totalAmount, String paymentOption) {
            this.totalAmount = totalAmount;
            this.paymentOption = paymentOption;
        }
    }
}