public without sharing class NotificationService {

    //CUSTOMER_COMMUNITY & PARTNER_COMMUNITY value updated by yadav Vineet Capgemini as per new values
    private static Notification_Settings__c notificationSettings = Notification_Settings__c.getOrgDefaults(); 
    public static String orgCommunityDomain = 'https://' + [Select Domain From Domain Where HttpsOption = 'CommunityAlt'].Domain;
    public static Network CUSTOMER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Customer Portal'];
    public static Network PARTNER_COMMUNITY = [SELECT Id, UrlPathPrefix FROM Network Where Name = 'IR Comp Global Partner Community'];
    public static final String REAL_TIME = 'Real-Time';

    @TestVisible private static Map<Id, Notification_Subscription__c[]> notificationTypeSubscriptionsMap;
    private static Notification_Type__c[] allNotificationTypes;  

    public static Network getCommunity(User recipient){        
        return getIsPartner(recipient) ? PARTNER_COMMUNITY : CUSTOMER_COMMUNITY;
    }

    public static String getOrgUrlBase(User recipient){

        String domain = URL.getOrgDomainUrl().toExternalForm();

        if (!getIsInternalUser(recipient)){
            domain = orgCommunityDomain + '/' + getCommunity(recipient).UrlPathPrefix;
        }

        return domain;
    }    

    public static String getRecordLinkURL(User recipient, Id recordId){

        String recordLink = getOrgUrlBase(recipient);

        if (getIsCustomer(recipient) || getIsPartner(recipient)){
            recordLink += '/s/detail';
        }  

        recordLink += '/' + recordId;

        system.debug('record link for (' + recipient + '): ' + recordLink);

        return recordLink;
    }

    private static Set<String> customerPortalUserTypes = new Set<String>{'CsnOnly', 'CspLitePortal', 'PowerCustomerSuccess'};

    public static Boolean getIsCustomer(User user){
        return customerPortalUserTypes.contains(user.UserType);
    }

    private static Set<String> partnerPortalUserTypes = new Set<String>{'PowerPartner'};

    public static Boolean getIsPartner(User user){
        return partnerPortalUserTypes.contains(user.UserType);
    }

    public static Boolean getIsInternalUser(User user){
        return user.UserType == 'Standard';
    }

    public static Notification_Type__c[] getNotificationTypes(){

        if (allNotificationTypes == null){

            allNotificationTypes = 
               [Select Id, Name, Active__c, Mandatory__c, Notification_Triggering_Object_Relation__c, Triggering_Object__c, Triggering_Field__c, Triggering_Field_Value__c, Triggering_Object_Timestamp_Field__c, 
                    Triggering_Object_Parent_Type__c, Triggering_Object_Parent_Field__c, Parent_Object_Summary_Fieldset__c, Parent_Object_Grandparent_Type__c, Parent_Object_Grandparent_Field__c, 
                    Message_Template__c 
                From Notification_Type__c 
                Order By Sort_Order__c ASC];
        }

        return allNotificationTypes;
    }

    public static Notification_Subscription__c[] getCurrentUserNotificationSubscriptions(String recordId){
        return getCurrentUserNotificationSubscriptions(recordId, false);
    }

    public static Notification_Subscription__c[] getCurrentUserNotificationSubscriptions(String recordId, Boolean returnAll){

        Id currentUserId = UserInfo.getUserId();

        String queryStr = 
            'Select Id, Name, Delivery_Frequencies__c, Delivery_Methods__c, Scope__c, Record_ID__c, Opt_Out__c, ' +
                'Notification_Type__r.Name, Notification_Type__r.Triggering_Object_Timestamp_Field__c, Notification_Type__r.Triggering_Field_Value__c, Notification_Type__r.Parent_Object_Grandparent_Field__c' +
            ' From Notification_Subscription__c' +
            ' Where OwnerId = :currentUserId';

        if (!returnAll){

            if (String.isBlank(recordId)){
                queryStr += ' and Scope__c = \'All Sites\'';
            }
            else{

                String sObjectType = String.valueOf(Id.valueOf(recordId).getSObjectType());
                queryStr += ' and Record_ID__c = :recordId';        
            }
        }

        queryStr += ' Order By Notification_Type__r.Sort_Order__c ASC';

        system.debug('queryStr: ' + queryStr);

        return Database.query(queryStr);
    }

    public static Map<Id, Notification_Subscription__c[]> getSubscriptionsByNotificationTypes(Notification_Type__c[] notificationTypes){
        return getSubscriptionsByNotificationTypes(notificationTypes, null);
    }

    public static Map<Id, Notification_Subscription__c[]> getSubscriptionsByNotificationTypes(Notification_Type__c[] notificationTypes, Id ownerId){
        
        if (notificationTypeSubscriptionsMap == null){

            notificationTypeSubscriptionsMap = new Map<Id, Notification_Subscription__c[]>();

            if (notificationTypes != null){

                String query = 'Select Id, OwnerId, Scope__c, Record_ID__c, Delivery_Frequencies__c, Delivery_Methods__c, Opt_Out__c, ' +
                    'Notification_Type__c, Notification_Type__r.Triggering_Object_Timestamp_Field__c, Notification_Type__r.Triggering_Object_Parent_Type__c ' + 
                    'From Notification_Subscription__c ' +
                    'Where Notification_Type__c in :notificationTypes';

                if (ownerId != null){
                    query += ' and OwnerId = :ownerId';
                }

                for (Notification_Subscription__c subscription : Database.query(query)){
                
                    if (notificationTypeSubscriptionsMap.containsKey(subscription.Notification_Type__c)){
                        notificationTypeSubscriptionsMap.get(subscription.Notification_Type__c).add(subscription);
                    }
                    else{
                        notificationTypeSubscriptionsMap.put(subscription.Notification_Type__c, new Notification_Subscription__c[]{subscription});
                    }
                }
            }
        }

        return notificationTypeSubscriptionsMap;
    }    

    public static String convertHtmlToPlainText(String message){
        message = message.stripHtmlTags();
        return message;
    }    

    public static void unsubscribeUsersFromSMS(Set<Id> userIds){

        system.debug('unsubscribeUsersFromSMS: ' + userIds);

        Map<Id, User> userMap = new Map<Id, User>([Select Id, Name, UserType, Email From User Where Id in :userIds]);

        Notification_Subscription__c[] smsSubscriptionsToDelete = new Notification_Subscription__c[]{};
        Notification_Subscription__c[] smsSubscriptionsToUpdate = new Notification_Subscription__c[]{};

        for (Notification_Subscription__c subscription : [Select Id, Delivery_Methods__c From Notification_Subscription__c Where OwnerId in :userIds and Delivery_Methods__c Includes ('SMS')]){

            if (subscription.Delivery_Methods__c == 'SMS'){
                smsSubscriptionsToDelete.add(subscription);
            }
            else{

                List<String> deliveryMethodsToKeep = new List<String>();

                for (String deliveryMethod : subscription.Delivery_Methods__c.split(';')){
                    
                    if (deliveryMethod != 'SMS'){
                        deliveryMethodsToKeep.add(deliveryMethod);
                    }
                }

                subscription.Delivery_Methods__c = String.join(deliveryMethodsToKeep, ';');

                smsSubscriptionsToUpdate.add(subscription);
            }
        }

        system.debug('smsSubscriptionsToDelete: ' + smsSubscriptionsToDelete.size());

        if (smsSubscriptionsToDelete.size() > 0){
            sObjectService.deleteRecordsAndLogErrors(smsSubscriptionsToDelete, 'NotificationService', 'unsubscribeUsersFromSMS-smsSubscriptionsToDelete');
        }

        system.debug('smsSubscriptionsToUpdate: ' + smsSubscriptionsToUpdate.size());

        if (smsSubscriptionsToUpdate.size() > 0){
            sObjectService.updateRecordsAndLogErrors(smsSubscriptionsToUpdate, 'NotificationService', 'unsubscribeUsersFromSMS-smsSubscriptionsToUpdate');
        }        

        if (notificationSettings != null && String.isNotBlank(notificationSettings.Twilio_From_Phone_Number__c) && String.isNotBlank(notificationSettings.Org_Wide_Email_Address__c)){

            // Send an email notifying the user how they can re-subscribe to SMS notifications
            Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{};
            
            OrgWideEmailAddress[] oweaLst = [select Id from OrgWideEmailAddress where Address = :notificationSettings.Org_Wide_Email_Address__c];
            Id oweaId = oweaLst.size() > 0 ? oweaLst[0].Id : null;   
            
            String emailBody = 
                'We have received your request to no longer receive SMS notifications.  If you would like to receive these in the future you will need to log back into the community and update your subscriptions.  ' + 
                'In addition, you will need to reply back to the SMS sender phone number (' + notificationSettings.Twilio_From_Phone_Number__c + ') with the keyword of "START".<br><br>' + 
                'Community URL: ';

            for (Id userId : userIds){

                User recipient = userMap.get(userId);
                String communityURL = getOrgUrlBase(recipient) + '/s/'; 

                Messaging.SingleEmailMessage email = EmailService.buildSingleEmailMessage(
                    userId, 
                    null, 
                    null, 
                    null,
                    'Ingersoll Rand Notifications - SMS Unsubscribe',
                    null,
                    emailBody + communityURL, 
                    null,
                    oweaId
                );              

                email.setUseSignature(false);

                emails.add(email);
            }

            EmailService.sendSingleEmailMessages(emails);
        }
    }

    // Returns the following values: Record, Parent, Grandparent, Global
    public static String getSubscriptionLevel(Notification_Subscription__c subscription, Notification_Type__c notificationType){

        if (String.isBlank(subscription.Record_ID__c)){
            return 'Global';
        }
        else{

            String subscriptionRecordObjectName = String.valueOf(Id.valueOf(subscription.Record_ID__c).getSObjectType());
        
            system.debug('subscriptionRecordObjectName: ' + subscriptionRecordObjectName);

            String notificationTypeObjectTypeName = notificationType.Triggering_Object__c;
            String notificationTypeParentObjectTypeName = notificationType.Triggering_Object_Parent_Type__c;
            String notificationTypeGrandparentObjectTypeName = notificationType.Parent_Object_Grandparent_Type__c;

            system.debug('notificationTypeObjectTypeName: ' + notificationTypeObjectTypeName);
            system.debug('notificationTypeParentObjectTypeName: ' + notificationTypeParentObjectTypeName);
            system.debug('notificationTypeGrandparentObjectTypeName: ' + notificationTypeGrandparentObjectTypeName);

            if (String.isNotBlank(notificationTypeObjectTypeName) && subscriptionRecordObjectName == notificationTypeObjectTypeName){
                return 'Record';
            }
            else if (String.isNotBlank(notificationTypeParentObjectTypeName) && subscriptionRecordObjectName == notificationTypeParentObjectTypeName){
                return 'Parent';
            }
            else if (String.isNotBlank(notificationTypeGrandparentObjectTypeName) && subscriptionRecordObjectName == notificationTypeGrandparentObjectTypeName){
                return 'Grandparent';
            }        
        }

        return null;
    }
    
    public static void logError(Exception e, String className, String method){
        
        system.debug('exception during ' + method + ' method: ' + e.getMessage());
        
        insert new Apex_Log__c(
            Class_Name__c = className,
            Method_Name__c = method,
            Exception_Stack_Trace_String__c = e.getStackTraceString(),
            Message__c = e.getMessage()
        );
    }        
}