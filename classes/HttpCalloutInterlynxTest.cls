@isTest
public class HttpCalloutInterlynxTest {
	//HttpCalloutInterlynx.Payload payload = new HttpCalloutInterlynx.Payload();
    @testSetup
    public static void newTestleads(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(400));
        List<Lead> testLeads = new List<Lead>();        
        for(Integer i = 0; i<=5; i++){
            Lead testLead = new Lead();
            testLead.FirstName = 'test';
            testLead.LastName = 'lead'+i;
            if(i == 2){
                testLead.VPTECH_Brand__c = 'Tuthill Pumps';
                testLead.Brand__c = 'ALH High Pressure Hose Pump';
            }
            else if(i == 3){
                testLead.Email = 'test@gmail.com';
                testLead.VPTECH_Brand__c = 'LMI';
                testLead.Interlynx_Integration_Error__c = 'Test';
            }
            else
                testLead.VPTECH_Brand__c = 'Milton Roy';
            testLead.Assigner_Comments__c = 'Test Assigner Comments '+i;
            testLead.Lead_Source_1__c = 'Marketing '+i;
            testLead.Lead_Source_2__c = 'advertisement '+i;
            testLead.Lead_Source_3__c = 'Social media '+i;
            testLead.Lead_Source_4__c = 'Friend '+i;
            testLead.Title = 'Mr';
            testLead.Website = 'www.google.com';
            testLead.LeadSource = 'Identified by Sales Rep';
            testLead.Description = 'Description '+i;
            testLead.Phone = '989898';
            testLead.Ready_to_share_with_Interlynx__c = true;
            testLead.Country = 'US';
         	testLeads.add(testLead);
        }
        try{
            insert testLeads;
        }
        catch(Exception e){
            System.debug('Error Message by Interlynx Test '+e.getMessage());
        }
       //opps       
        Account acc = new Account();
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('JDE_Account').getRecordTypeId();
        acc.ShippingCity = 'Test City';
        acc.ShippingCountry = 'Test country';
        acc.ShippingPostalCode = '658776';
        acc.ShippingStreet = 'test street';
        acc.Name = 'Test Account';
        insert acc;
        List<Opportunity> testOpps = new List<Opportunity>();
        Map<integer, Id> testLeadIds = new Map<integer, Id>();
        Integer count = 0;
        for(Lead oLead : [Select id, firstname from lead]){
            testLeadIds.put(count, oLead.Id);
            count++;
        }
        
        for(Integer i = 0; i <= 5; i++){
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Milton_Roy_Opportunity').getRecordTypeId();
            opp.AccountId = acc.Id;
            opp.Lead_Id__c = testLeadIds.get(i);
            opp.Name = 'Test lead '+i;
            opp.CloseDate = Date.today();
            opp.Business__c = 'Milton Roy';
            opp.Confidence__c = '10'; 
            opp.StageName = 'Target';
            opp.Was_this_the_result_of_an_assessment__c = 'No';
            opp.Lead_Shared_To_Interlynx__c = true;
            testOpps.add(opp);
        }
        try{
            insert testOpps;
        }
        catch(Exception e){
            System.debug('Error Message by Interlynx Test '+e.getMessage());
        }
    }
    
    @isTest
    static void afterInsertTest(){
       /* Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(200));
        List<Lead> testLead = [SELECT Id, Ready_to_share_with_Interlynx__c FROM Lead];
        Test.startTest();
        HttpCalloutInterlynx.afterInsert(testLead);
        Test.stopTest();*/
        
    }
     @isTest
    static void afterUpdateTest(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(401));
        Lead newLead = new Lead(FirstName = 'test', LastName = 'lead', VPTECH_Brand__c = 'Milton Roy', Email = 'new@gmail.com');
        insert newLead;
        Lead updatedLead = new Lead(id = newLead.Id, Ready_to_share_with_Interlynx__c = true);
        update updatedLead;
        List<Lead> testLead = [SELECT  Id, Name,Lead_Shared_To_Interlynx__c, Ready_to_share_with_Interlynx__c FROM Lead];
        Map<Id, Lead> leadMap = new Map<Id, Lead>();
        for(Lead oLead : testLead){
             leadMap.put(oLead.Id, oLead);
        }
        Test.startTest();
        HttpCalloutInterlynx.afterUpdate(testLead, leadMap);
        Test.stopTest();
    }
    @isTest
    static void oppAfterInsertTest(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(405));
		List<Opportunity> testOpps = [Select Id, Lead_Id__c, Lead_Shared_To_Interlynx__c FROM Opportunity];
        Test.startTest();
        HttpCalloutInterlynx.oppAfterInsert(testOpps);
        Test.stopTest();
    }
    @isTest
    static void getCalloutResponseTest(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(500));
        List<HttpCalloutInterlynx.Payload> genPayload = new List<HttpCalloutInterlynx.Payload>();
        List<Lead> testLead = [SELECT LUID__c, RSM_ID__c, CreatedDate, LastModifiedDate, VPTECH_Brand__c,Title, FirstName, 
                            	LastName, Salutation, Email, Company, Street, City, State, PostalCode, 
                            	Country, Website, LeadSource, Lead_Source_1__c, Lead_Source_2__c, Lead_Source_3__c, 
                            	Lead_Source_4__c, Assigner_Comments__c, Description, Industry, First_Quote_Date__c, 
                            	Distributor_overall_response_time__c, Id, Phone, Product__c, Partner_Comments__c, 
                            	SIC_Description__c, NAICS_Description__c, Interlynx_Integration_Error__c, AIRD_Closed_Won_Total__c,
                            	Distributor_Sales_Person__c, Dist_ID__c, Lead_Shared_To_Interlynx__c FROM Lead];
        Set<Id> leadIds = new Set<Id>();
        for(Lead oLead: testLead){
            leadIds.add(oLead.Id);
        }
        Test.startTest();
        HttpCalloutInterlynx.getCalloutResponse(LeadIds);
        Test.stopTest();
    }	
    @isTest
    static void updateMethodTest(){
        List<Lead> testLead = [SELECT Id, Name FROM Lead];
        HttpCalloutInterlynx.updateMethod(testLead);
    }
    @isTest
    static void convertedLeadsTest(){
        Test.setMock(HttpCalloutMock.class, new InterlynxCalloutMockTest(200));
        List<Opportunity> newOpps = [Select Id, Lead_Id__c, Lead_Shared_To_Interlynx__c from Opportunity];
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : newOpps)
            oppIds.add(opp.Id);
        
        HttpCalloutInterlynx.convertedLeads(oppIds);
    }
}