/**
    @author Marc Powell (Providity)
    @date 09May-2019
    @description Test class for CTS_OM_CaseSatisfactionSurveyCtrl.
*/

@isTest
private class CTS_OM_CaseSatisfactionSurveyCtrl_Test {
    
    @testSetup
    static void setup() {
        Case newCTSCase = new Case();
        newCTSCase.Subject = 'Test';
        newCTSCase.RecordTypeId = [SELECT Id FROM RecordType WHERE sobjectType='Case' and DeveloperName='CTS_TechDirect_Ask_a_Question'].Id;
        newCTSCase.CTS_TechDirect_Category__c= 'Diagnostics';
        newCTSCase.CTS_TechDirect_Sub_Category__c= 'Vibration';
        newCTSCase.Description = 'Test'; 
        insert newCTSCase;
    }
    static testMethod void testCSat() {
        //Commented as part of EMEIA Cleanup
        Case c = [SELECT Id, CaseNumber, CTS_Case_Survey_Satisfaction_Level__c /*CC_Case_Survey_Comments__c*/ FROM Case];
        Test.setCurrentPage(new PageReference('/apex/CTS_OM_CaseSatSurveyVF?cId=' + c.Id + '&lvl=2'));
        CTS_OM_CaseSatisfactionSurveyCtrl cnt = new CTS_OM_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CTS_OM_CaseSatisfactionSurveyCtrl.submitFeedback(c.Id, '2', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CTS_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CTS_Case_Survey_Satisfaction_Level__c == '2');
    }
    
    static testMethod void testCSatNoId() {
        //Commented as part of EMEIA Cleanup
        Case c = [SELECT Id, CaseNumber, CTS_Case_Survey_Satisfaction_Level__c /*CC_Case_Survey_Comments__c*/ FROM Case];
        Test.setCurrentPage(new PageReference('/apex/CTS_OM_CaseSatSurveyVF?lvl=2'));
        CTS_OM_CaseSatisfactionSurveyCtrl cnt = new CTS_OM_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CTS_OM_CaseSatisfactionSurveyCtrl.submitFeedback('', '2', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CTS_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CTS_Case_Survey_Satisfaction_Level__c == null);
    }
    
    static testMethod void testCSatAlreadyDone() {
        //Commented as part of EMEIA Cleanup
        Case c = [SELECT Id, CaseNumber, CTS_Case_Survey_Satisfaction_Level__c /*CC_Case_Survey_Comments__c*/ FROM Case];
        c.CTS_Case_Survey_Satisfaction_Level__c = '3';
        update c;
        Test.setCurrentPage(new PageReference('/apex/CTS_OM_CaseSatSurveyVF?cId=' + c.Id + 'lvl=2'));
        CTS_OM_CaseSatisfactionSurveyCtrl cnt = new CTS_OM_CaseSatisfactionSurveyCtrl();
        Test.startTest();
        String res = CTS_OM_CaseSatisfactionSurveyCtrl.submitFeedback(c.Id, '2', 'Test');
        Test.stopTest();
        
        System.assert([SELECT CTS_Case_Survey_Satisfaction_Level__c FROM Case WHERE Id =: c.Id].CTS_Case_Survey_Satisfaction_Level__c == '3');
    }        
}