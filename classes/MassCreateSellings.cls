global class MassCreateSellings implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {        
    	PT_Record_types__c ptrt = PT_Record_types__c.getOrgDefaults();
        String query = 'select Id from account where recordtypeid=\''+ptrt.Account_RT_Id__c+'\'';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, list<account> scope) {
        List<String> months = new List<String>{'January','February','March','April','May','June','July','August','September','October','November','December'};
            List<Selling__c> sellings = new List<Selling__c>();    
        string nextYear = string.valueof((system.today().year())+1);
        
        for(account acc : scope){
            for(string mo : months){
                Selling__c s = new Selling__c(name=mo+' '+nextYear,PT_Account__c=acc.id,PT_Year__c=nextYear,currencyisocode='EUR');
                sellings.add(s);
            }
        }
        if(!sellings.isEmpty()){
            insert sellings;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('**** Finish');
    }
    
}