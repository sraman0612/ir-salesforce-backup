public class CostCenterTriggerHandler {
  // Map to store Existing Business Opportunities related to Cost Center
  public static Map<id, Opportunity> costCenterToOpportunityMap = new Map<id, Opportunity>();
  // List of Existing Opportunities to be inserted
  public static List<Opportunity> opportunitiesToInsert = new List<Opportunity>();
  // Map to Store Opportunity Revenue Schedules related to Cost Center
  public static Map<id, List<Opportunity_Revenue_Schedule__c>> costCenterToRevenuesMap = new Map<id, List<Opportunity_Revenue_Schedule__c>>();  
  // Set containing Non DSO Cost Center Default Branches whose Create_Existing_Business_Opportunity__c field is true
  public static Set<String> costCenterNumberSet = new Set<String>();
  // List of Existing Business Opportunities whose owner needs to be updated.
  public static List<Opportunity> opptiesToUpdate = new List<Opportunity>();
  // Map to store List of Accounts related to Cost Center
  public static Map<Id,List<Account>> costCenterToAccounts = new Map<Id, List<Account>>();
  // List of Accounts to be updated.
  public static List<Account> accountsToUpdate = new List<Account>();
  // Account types  
  public static String DSO = 'DSO';
  public static String IWD = 'IWD';
    
  // Method to update Create_Existing_Business_Opportunity__c of Cost Center as false
  public static void beforeInsertUpdate(List<Cost_Center__c> newCostCenterList){
    for(Cost_Center__c costCenter : newCostCenterList){
      costCenter.Create_Existing_Business_Opportunity__c= False;    
    } 
  }
  
  // Creates Existing Business Opportunities and Opportunity Revenue Schedules for Cost Center   
  public static void afterInsert(List<Cost_Center__c> newCostCenterList){
      // Retreiving Id of Existing Business Record Type
      Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;  
      opportunitiesToInsert = new List<Opportunity>();
        // Iterating through new cost centers list
        for(Cost_Center__c costCenter: newCostCenterList){
          // If costCenterNumberSet contains Default Branch 
          if(CostCenterTriggerHandler.costCenterNumberSet.contains(costCenter.Default_Branch__c)){
            Opportunity opp = new Opportunity();
            // Create Existing Business Opportunity for Cost Center
            opp = OpportunityUtils.createIwdExistingBusinessOpportunity(costCenter);
            opportunitiesToInsert.add(opp);
            // Create List of Opportunity Revenue Schedules for 12 months
            List<Opportunity_Revenue_Schedule__c> revenues = OpportunityUtils.createIwdRevenueSchedule();
            costCenterToRevenuesMap.put(costCenter.Id, revenues);  
          }
        }
        // If there are Opportunities to be inserted
        if(opportunitiesToInsert.size()>0){
          insert opportunitiesToInsert;
          List<Opportunity_Revenue_Schedule__c> revenuesToInsert = new List<Opportunity_Revenue_Schedule__c>();
          // Iterating through the Inserted Existing Business Opportunities 
          for (Opportunity opp : opportunitiesToInsert){
            // Iterating through Opportunity Revenue Schedules of Opp's Cost Center
            for (Opportunity_Revenue_Schedule__c ors : costCenterToRevenuesMap.get(opp.Cost_Center_Name__c)){
              // Assigning Opportunity to Opportunity Revenue Schedule
              ors.Opportunity__c = opp.Id;
              revenuesToInsert.add(ors);
            }
          } 
          // Inserting Opportunity Revenue Schedules
          insert revenuesToInsert; 
        } 
      }
      
      
      public static void afterUpdate(List<Cost_Center__c> newCostCenterList){
        opportunitiesToInsert = new List<Opportunity>();
        // Retreiving Id of Existing Business Record Type
        Id existingBusinessRecTypeId = OpportunityUtils.OPP_RECORD_TYPES.get(OpportunityUtils.RECORD_TYPE_EXISTING).Id;  
        // Retreiving Existing Business Opportunities of Cost Centers in Trigger.New
        for(Opportunity opp: [select id,Cost_Center_Name__c from Opportunity where Cost_Center_Name__c in :newCostCenterList  AND CloseDate = THIS_YEAR AND RecordTypeId = :existingBusinessRecTypeId ]){ 
          costCenterToOpportunityMap.put(opp.Cost_Center_Name__c, opp);
        }
        // Iterating through new Cost Center List
        for(Cost_Center__c costCenter: newCostCenterList){
          // If the Cost Center do not have Existing Business Opportunity and Create_Existing_Business_Opportunity__c is true.
          if(!costCenterToOpportunityMap.containsKey(costCenter.Id) && CostCenterTriggerHandler.costCenterNumberSet.contains(costCenter.Default_Branch__c)){
            Opportunity opp = new Opportunity();
            // Create Existing Business Opportunity
            opp = OpportunityUtils.createIwdExistingBusinessOpportunity(costCenter);
            opportunitiesToInsert.add(opp);
            // Create Opportunity Revenue Schedules
            List<Opportunity_Revenue_Schedule__c> revenues = OpportunityUtils.createIwdRevenueSchedule();
            costCenterToRevenuesMap.put(costCenter.Id, revenues);
          }
        }
        // Collecting Accounts related to Cost Center
        for(Account acc: [Select Id, OwnerId,Cost_Center_Name__c from Account where Cost_Center_Name__c In :newCostCenterList AND Cost_Center_Name__r.Channel__c != :DSO]){
          if (!costCenterToAccounts.containsKey(acc.Cost_Center_Name__c)){
            costCenterToAccounts.put(acc.Cost_Center_Name__c, new List<Account>());
          } 
          costCenterToAccounts.get(acc.Cost_Center_Name__c).add(acc);
        }
        // Iterating through Cost Centers to update Owners of Related Accounts and Related Existing Business Opportunities. 
        for(Cost_Center__c costCenter: newCostCenterList){
          Cost_Center__c oldCostCenter = (Cost_Center__c)Trigger.oldMap.get(costCenter.Id);
          // If Non DSO Cost Center's Owner is changed
          if(costCenter.Channel__c != DSO && costCenter.ownerId != Null && (oldCostCenter.ownerId == Null || costCenter.ownerId != oldCostCenter.ownerId)){
            // If Cost Center contains Existing Business Opportunity, change owner of opportunity to Cost Center's new Owner
            if(costCenterToOpportunityMap.containsKey(costCenter.Id)){
              Opportunity opp = new Opportunity();
              opp = costCenterToOpportunityMap.get(costCenter.Id);
              opp.OwnerId = costCenter.OwnerId;
              opptiesToUpdate.add(opp);
            } 
            // If Cost Center contains Accounts, update Account's owner to Cost Center's new Owner
            if(costCenterToAccounts.containsKey(costCenter.Id)){
              for(Account acc : costCenterToAccounts.get(costCenter.Id)){
                acc.OwnerId = costCenter.OwnerId;
                accountsToUpdate.add(acc);
              } 
            }
          } 
        } 
        // update Existing Business Opportunities
        if(!opptiesToUpdate.isEmpty()){
          update opptiesToUpdate;  
        }
        // Update Accounts of Cost Center
        if(!accountsToUpdate.isEmpty()){
          update accountsToUpdate; 
        }
        // If there are Opportunities to be inserted
        if(opportunitiesToInsert.size()>0){
            insert opportunitiesToInsert;
            List<Opportunity_Revenue_Schedule__c> revenuesToInsert = new List<Opportunity_Revenue_Schedule__c>();
            // Iterating through inserted Opportunities
            for (Opportunity opp : opportunitiesToInsert){
              // Iterating through Opportunity Revenue Schedules of Opp's Cost Center
              for (Opportunity_Revenue_Schedule__c ors : costCenterToRevenuesMap.get(opp.Cost_Center_Name__c)){
                // Assigning Opportunity to Opportunity Revenue Schedule
                ors.Opportunity__c = opp.Id;
                revenuesToInsert.add(ors);
              }
            }
            // Inserting Opportunity Revenue Schedules
            insert revenuesToInsert;
        }
    } 
   
}