@isTest
public class IRSPX_PercentCompleteTest {
    //Commented as part of EMEIA Cleanup
    // Account Record Types
    //public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
    public static final String AirNAAcct_RT_DEV_NAME = 'CTS_EU';

    // Opportunity Record Types
    //public static final String RS_New_Business_RT_DEV_NAVE = 'RS_Business';
    public static final String CTS_Global_New_RT_DEV_NAME = 'CTS_EU';
    
    // Account Plan Record Types
    public static final String CTS_Account_Plan = 'CTS_Account_Plan';
    
    // Opportunity Sales Call Plan Record Types
    public static final String CTS_Sales_Call_Plan = 'CTS_Sales_Call_Plan';
    
    // Opportunity Win Plan Record Types
    public static final String CTS_Opportunity_Win_Plan = 'CTS_Opportunity_Win_Plan';
    
    
    @testSetup static void setup(){
        AccountPlan__c accountPlan;
        List<AccountPlan__c> accountPlans = new List<AccountPlan__c>();
        Opportunity_Sales_Call_Plan__c salesCallPlan;
        List<Opportunity_Sales_Call_Plan__c> salesCallPlans = new List<Opportunity_Sales_Call_Plan__c>();
        OpportunityWinPlan__c oppyWinPlan;
        List<OpportunityWinPlan__c> oppyWinPlans = new List<OpportunityWinPlan__c>();
        String currentYear = Date.Today().Year().format().remove(',');
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        System.runAs(u){
            
            Account airdTestAccount =IRSPXTestDataFactory.createAccount('TestAcc1',AirNAAcct_RT_DEV_NAME);
            airdTestAccount.ownerid=u.id;
            insert airdTestAccount;
            
            for(RecordType rt: [Select Id, DeveloperName from RecordType where SobjectType='AccountPlan__c' and DeveloperName = :CTS_Account_Plan]){
                for (Integer i = 0; i < 10; i++){
                    accountPlan = IRSPXTestDataFactory.createAccountPlan(airdTestAccount.id, rt.DeveloperName, currentYear);
                    accountPlans.add(accountPlan);

                }
            }      
            
            Database.SaveResult[] srList = Database.insert(accountPlans,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Account plan fields that affected this error: ' + err.getFields());
                    }
                }
            }            
            
            for(RecordType rt: [Select Id, DeveloperName from RecordType where SobjectType='Opportunity_Sales_Call_Plan__c' and DeveloperName = :CTS_Sales_Call_Plan]){
                for (Integer i = 0; i < 10; i++){
                    salesCallPlan = IRSPXTestDataFactory.createSalesCallPlan(airdTestAccount.id, rt.DeveloperName);
                    salesCallPlans.add(salesCallPlan);
                }
            }  
            
            srList = Database.insert(salesCallPlans,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Sales call plan fields that affected this error: ' + err.getFields());
                    }
                }
            }
                
            Opportunity testOpp = IRSPXTestDataFactory.createOpportunity('TestOpty1',CTS_Global_New_RT_DEV_NAME,airdTestAccount.id,'Stage 1. Qualify',50000);
            testOpp.ownerid=u.id;
            insert testOpp;
                
            for(RecordType rt: [Select Id, DeveloperName from RecordType where SobjectType='OpportunityWinPlan__c' and DeveloperName = :CTS_Opportunity_Win_Plan]){
                for (Integer i = 0; i < 10; i++){
                    oppyWinPlan = IRSPXTestDataFactory.createOpportunityWinPlan(testOpp.id, rt.DeveloperName);
                    oppyWinPlans.add(oppyWinPlan);
                }
            }
            
            srList = Database.insert(oppyWinPlans,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Opportunity win plan fields that affected this error: ' + err.getFields());
                    }
                }
            }

        }
    }
    
    @isTest static void accountPlanPercentageCompleteTest(){
        List<AccountPlan__c> accountPlans = new List<AccountPlan__c>();
        List<Opportunity_Sales_Call_Plan__c> salesCallPlans = new List<Opportunity_Sales_Call_Plan__c>();
        List<OpportunityWinPlan__c> opportunityWinPlans = new List <OpportunityWinPlan__c>();
        Account testAccount;
        List<Account> accounts = new List<Account>();
        Object objectMessage;
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt.1'];
        
        System.runAs(u){
            
            accountPlans = [select id, name, AccountName__c, AccountPlanYear__c, Percentage_of_Fields_Populated__c, RecordTypeId from AccountPlan__c];
            
            for(AccountPlan__c ap: accountPlans){
                system.debug(ap);
                system.assert(ap.Percentage_of_Fields_Populated__c > 0,objectMessage);   
            }  
            
             salesCallPlans = [select id, name, Account__r.name, Percentage_of_Fields_Populated__c, RecordTypeId from Opportunity_Sales_Call_Plan__c];
            
            for(Opportunity_Sales_Call_Plan__c scp: salesCallPlans){
                system.debug(scp);
                system.assert(scp.Percentage_of_Fields_Populated__c > 0,objectMessage);   
            }  
            
             opportunityWinPlans = [select id, name,  Percentage_of_Fields_Populated__c, RecordTypeId from OpportunityWinPlan__c];
            
            for(OpportunityWinPlan__c owp: opportunityWinPlans){
                system.debug(owp);
                system.assert(owp.Percentage_of_Fields_Populated__c > 0,objectMessage);   
            }  
            //Commented as part of EMEIA Cleanup
           /* testAccount =IRSPXTestDataFactory.createAccount('TestAcc2',RS_HVAC_RT_DEV_NAME);
            testAccount.ownerid=u.id;
            insert testAccount;*/
            
             testAccount =IRSPXTestDataFactory.createAccount('TestAcc2','CTS_EU');
            testAccount.ownerid=u.id;
            insert testAccount;
            
        accounts.add(testAccount);
            
 Opportunity testOpp1 = IRSPXTestDataFactory.createOpportunity('TestOpty2',CTS_Global_New_RT_DEV_NAME,testAccount.id,'Stage 1. Qualify',50000);
            testOpp1.ownerid=u.id;
            insert testOpp1;
            OpportunityWinPlan__c owp = new OpportunityWinPlan__c(Name = 'Test', opportunity__c = testOpp1.id);
            insert owp;
            IRSPX_PercentComplete.updatePercentageCompleted(accounts);
        }

        
        Test.Stoptest();
    }
        
}