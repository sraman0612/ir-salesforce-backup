@IsTest
private class FileUploadInvocableTest {
    @IsTest
    static void attachFileToRecordTest() {
        Account account = new Account(Name = 'Test');
        insert account;
        ContentVersion contentVersion = FileUploadAtCheckoutController.createContentVersion('test script', 'Test');

        List<FileUploadInvocable.InputVariables> inputVariables = new List<FileUploadInvocable.InputVariables>();
        FileUploadInvocable.InputVariables inputVariable = new FileUploadInvocable.InputVariables();
        inputVariable.recordId = account.Id;
        inputVariable.contentVersionId = contentVersion.Id;
        inputVariables.add(inputVariable);

        Test.startTest();
        FileUploadInvocable.attachFileToRecord(inputVariables);
        Test.stopTest();

        ContentDocumentLink cdl = [SELECT Id, LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :account.Id];
        List<ContentVersion> contentVersions = [ SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion.Id];
        System.assertEquals(cdl.ContentDocumentId, contentVersions[0].ContentDocumentId);
    }
}