/**
    @author Providity
    @date 23FEB19
    @description Test class for CTS_CommunityCaseSearchController
*/
@isTest
private class CTS_CommunityCaseSearchController_Test {
	
    @testSetup
    static void setupData() {
        CTS_Community_Search_Settings__c setting = new CTS_Community_Search_Settings__c();
        setting.CTS_TD_Case_Record_Types__c = 'IR Comp TechDirect Ask a Question';
        setting.Limit_Records__c = 100;
        setting.Recent_Days_Count__c = 10;
        insert setting;
        
        
    }
    @isTest
    static void testQuery() {
        Test.startTest();
        List<Object> searchParams = CTS_CommunityCaseSearchController.getSearchParamsOnLoad();
        System.assert(searchParams.size() == 6);
        List<Object> picks = CTS_CommunityCaseSearchController.getPicklistOptions('Case', 'Status', null);
        System.assert(picks.size() > 0);
        List<Object> res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'search', 'filterBased', 'Test', null, 'mine', null, null);
        res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'open', 'filterBased', 'Test', null, 'mine', null, null);
        res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'closed', 'filterBased', 'Test', null, 'mine', null, null);
        res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'recent', 'filterBased', 'Test', null, 'mine', null, null);
        res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'collaborator', 'filterBased', 'Test', null, 'mine', null, null);
        res = CTS_CommunityCaseSearchController.getAllCases('Case', 'CTS_Case_Search_Fields', 'collaborator', 'allOnInit', 'Test', null, 'mine', null, null);
        System.assert(res.size() == 1);                
        Test.stopTest();
    }
}