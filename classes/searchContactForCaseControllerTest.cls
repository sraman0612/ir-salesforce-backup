/*------------------------------------------------------------

Test class to cover searchContactForCaseController.

History
<Date>       <Authors Name>     <Brief Description of Change>
2-Mar-2022  Capgemini			Initial Version (User Story - 1177) 

Revisions:
20-June-2024 : Update code against Case #03077072 - AMS Team

------------------------------------------------------------*/
@isTest
public class searchContactForCaseControllerTest {
    @isTest
    private static void unitTest1(){
        Id otherCaseRecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Case' and DeveloperName = 'Customer_Service' Order By Name ASC LIMIT 1].Id;
        Id otherContactRecordTypeId = [Select Id From RecordType Where isActive = True and sObjectType = 'Contact' and DeveloperName = 'CTS_EU_Contact' Order By Name ASC LIMIT 1].Id; 
        Contact ct2 = new Contact();
    	ct2.FirstName = 'John';
    	ct2.LastName = 'Test2';
    	ct2.Email = 'abc1@testing.com';
        ct2.Title = 'Test';
        ct2.Phone = '555-555-5555';        
    	ct2.RecordTypeId = otherContactRecordTypeId;
        insert ct2;
        Contact ct1 = new Contact();
    	ct1.FirstName = 'John1';
    	ct1.LastName = 'Test21';
    	ct1.Email = 'abc11@testing.com';
        ct1.Title = 'Test';
        ct1.Phone = '555-555-5555';        
    	ct1.RecordTypeId = otherContactRecordTypeId;
        insert ct1;
        
        //Updated code for Case-03077072 by AMS team (Pratik)
        //created new record for testing
        Contact ct3 = new Contact();
    	ct3.FirstName = 'John2';
    	ct3.LastName = 'Test22';
    	ct3.Email = 'abcd1234@testing.com';
        ct3.Title = 'Test';
        ct3.Phone = '555-555-5555';        
    	ct3.RecordTypeId = otherContactRecordTypeId;
        insert ct3;
        
        Case case1 = TestDataUtility.createCase(false, otherCaseRecordTypeId, null, null, 'Test 1', 'New');
		case1.ContactId =  ct1.Id;       
        insert case1;
        
        searchContactForCaseController.searchContactNameMethod('John1','abc11@testing.com');
        searchContactForCaseController.updateCurrentContact(case1.Id,ct2.Id);
        searchContactForCaseController.updateAllContact(case1.Id,ct3.Id);
        searchContactForCaseController.isMultipleContacts(case1.Id);
    }

}