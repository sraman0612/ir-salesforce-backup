/* Revisions:
13-Sept-2024 : Updated logic for Case #03210798 by AMS Team
*/

public class AssignmentEngineV2 {
    //
    //Removed the county dependency and added the assigment Key
    //
    
    public static void handleAssignment(sObject[] newList, Map<Id,sObject> oldMap)
    {
        Map<Integer,Id> queueIds = new Map<Integer,Id>();
        Set<String> assignmentKeysList = new Set<String>();
        
        Integer idx = 0;
        for (sObject cs : newList)
        {
            
            if(oldMap != null) {  
                if(cs.get('OwnerId') <> oldMap.get(cs.id).get('OwnerId')) {
                    queueIds.put(idx, (Id) cs.get('OwnerId'));
                    System.debug('>>>>>queueIds 1: '+queueIds);
                }
                if(cs.get('OwnerId') == oldMap.get(cs.id).get('OwnerId')&&((String)cs.get('OwnerId')).startsWith('00G')) {
                     queueIds.put(idx, (Id) cs.get('OwnerId'));
				}
            }else {
                queueIds.put(idx, (Id) cs.get('OwnerId'));
                System.debug('>>>>>queueIds 2: '+queueIds);
            }
            assignmentKeysList.add((String) cs.get('Assignment_Key__c'));
            idx++;
        }
        System.debug('>>>>>queueIds: '+queueIds);
        System.debug('>>>>>assignmentKeysList 1: '+assignmentKeysList);
        if (queueIds.isEmpty()) return;
        
        //
        //Find active Assignment Group for Queue
        //
        Map<Integer,Id> asgnGroupNameIds = new Map<Integer,Id>();
        Map<Id,Assignment_Group_Queues__c> asgnGroupQueues = new Map<Id,Assignment_Group_Queues__c>(); 
        
        for(Assignment_Group_Queues__c[] agq : [SELECT Assignment_Group_Name__c, QueueId__c
                                              FROM Assignment_Group_Queues__c 
                                              WHERE QueueId__c in :queueIds.values()
                                              AND Active__c = 'True'])
        {
            for (Integer i = 0; i < agq.size() ; i++)
            {
                asgnGroupQueues.put(agq[i].QueueId__c, agq[i]);
            }                                           
        }
        System.debug('>>>>>asgnGroupQueues: '+asgnGroupQueues); 
        if (asgnGroupQueues.isEmpty()) return;
    
        for (Integer i : queueIds.keySet()) {
            Assignment_Group_Queues__c agq = asgnGroupQueues.get(queueIds.get(i));
            
            if (agq <> null) {
                asgnGroupNameIds.put(i, agq.Assignment_Group_Name__c);
            }
            //else no active assignment group queue error
        }
         System.debug('*****asgnGroupNameIds: '+asgnGroupNameIds);
        System.debug('>>>>>asgnGroupNameIds: '+asgnGroupNameIds);
        System.debug('>>>>>assignmentKeyList: '+assignmentKeysList);
        if (asgnGroupNameIds.isEmpty()) return;
        
        Utility_MapChain groupChain = new Utility_MapChain();
        //Added for case no. 01095574
        Utility_MapChain groupChainDist = new Utility_MapChain();
        
        
        //Added Zone field for case no. 01095574
        for(Assignment_Groups__c ags : [SELECT Group_Name__c, User__c, Assignment_Key__c,Zone__c
                                   FROM Assignment_Groups__c 
                                   WHERE Group_Name__c IN :asgnGroupNameIds.values()
                                   AND Assignment_Key__c IN :assignmentKeysList])
        {
             String AGAssignKey = null != ags.Assignment_Key__c ? ags.Assignment_Key__c.toUpperCase() : '';  
             groupChain.putData(new object[] {ags.Group_Name__c, AGAssignKey },ags.user__c);
             //Added for case no. 01095574
             groupChainDist.putData(new object[] {ags.Group_Name__c, AGAssignKey },ags.Zone__c);
        }
        
        for (Integer i : queueIds.keySet())
        {
            String LeadAssignKey = null != newList[i].get('Assignment_Key__c') ? newList[i].get('Assignment_Key__c').toString().toUpperCase() : '';
            Id userToAssign = (Id) groupChain.getData(new object[]{asgnGroupNameIds.get(i), LeadAssignKey});
            String userDistrict =(String) groupChainDist.getData(new object[]{asgnGroupNameIds.get(i), LeadAssignKey});
            if (userToAssign != null)
                newList[i].put('ownerId',userToAssign);
                System.debug('<<<userToAssign>>>'+userToAssign);
                //Updating Lead Zone field for case no. 01095574
                newList[i].put('Zone__c',userDistrict);
        	}
    }
    
    /*
    *********************************************************
    @Method Name    : checkOutOfOffice
    @author         : Darshan Shah (AMS Team)
    @description    : This Method Checks if LeadOwner is out of office and assign it to delegated user specified on User record
						#03210798 - Sept24 Release
    @param          : newList : List of new sObject
    @param          : oldMap : Map of old sObject
    @return         : void
    ********************************************************
    */
    public static void checkOutOfOffice(sObject[] newList, Map<Id,sObject> oldMap){
        
        Map<sObject,Id> mapUserIdBySObject = new Map<sObject,Id>();
        for(sObject objSObject: newList){
            if(!((String)objSObject.get('OwnerId')).startsWith('00G') && objSObject.get('OwnerId') != oldMap.get(objSObject.id).get('OwnerId')) {
                mapUserIdBySObject.put(objSObject, (Id) objSObject.get('OwnerId'));
            }
        }
        if(!mapUserIdBySObject.isEmpty()){
            Map<Id,User> mapUserById = new Map<Id,User>([SELECT Id, Delegated_User_When_Out_Of_Office__c
                                                         FROM User
                                                         WHERE Id IN :mapUserIdBySObject.values()
                                                         AND Is_OOO__c = True AND isActive = True
                                                         AND Delegated_User_When_Out_Of_Office__c != null]);
            if(!mapUserById.isEmpty()){
                for(sObject objSObject : mapUserIdBySObject.keySet()){
                    objSObject.put('OwnerId', mapUserById.get((Id) objSObject.get('OwnerId')).Delegated_User_When_Out_Of_Office__c);
                }
            }
        }
    }
}