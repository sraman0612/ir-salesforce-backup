@isTest
private class ExtractFilesMultipleDownloadTest {
    @isTest
     static void getFilesTest(){
        Id MILTON_ROY_APPLICATION_RT = [SELECT Id,DeveloperName FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'Milton_Roy_Case_Management_Application' LIMIT 1].Id;
        Case c1 = new Case(Subject = 'Test Application Case 1',RecordTypeId = MILTON_ROY_APPLICATION_RT);

        

       
        ContentVersion contentVersion1 = new ContentVersion(

                    Title          = 'Test File 1',

                    PathOnClient   = 'Pic1.jpg',

                    VersionData    = Blob.valueOf('Test Content for Pic1'),

                    IsMajorVersion = true);

           
            
            //create contentDocumentLink to Case

           


        Test.StartTest();
        try{
            insert c1;
            insert contentVersion1;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            ContentDocumentLink cdl = new ContentDocumentLink();

            cdl.LinkedEntityId = c1.Id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.ShareType = 'V';
            cdl.Visibility = 'AllUsers';

            insert cdl;
            List<ContentDocument> docList = ExtractFilesMultipleDownload.getFiles(c1.Id);
            System.AssertEquals(1,docList.size());
        }
        catch(Exception e){
            System.debug(e.getMessage());
        }
        
        Test.StopTest();
        
    }

}