@isTest
public with sharing class IRSPXTestDataFactory {
	
	public static account createAccount(string accname,string recType){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Account; 
		Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByDeveloperName(); 
		return (new account(name=accname,recordtypeid=AccountRecordTypeInfo.get(recType).getRecordTypeId()));
	}
	
	public static opportunity createOpportunity(string oppName,string recType,id accid,string stage, decimal amount){
		Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.opportunity; 
		Map<String,Schema.RecordTypeInfo> opportunityRecordTypeInfo = cfrSchema.getRecordTypeInfosByDeveloperName();
		return (new opportunity(name=oppName,recordtypeid=opportunityRecordTypeInfo.get(recType).getRecordTypeId(),Accountid=accid,CloseDate=date.today(),StageName=stage,Amount=amount));
	}
	
    
    public static AccountPlan__c createAccountPlan(Id accountId, string developerName, string year){
    	Schema.DescribeSObjectResult accountPlanSchema = AccountPlan__c.sObjectType.getDescribe();
		Map<String, Schema.RecordTypeInfo> recordTypeInfo = accountPlanSchema.getRecordTypeInfosByDeveloperName();
        Id recordTypeId = recordTypeInfo.get(developerName).getRecordTypeId();
        
        AccountPlan__c ap = new AccountPlan__c(Name = developerName, RecordTypeId = recordTypeId, AccountName__c = accountId, AccountPlanYear__c = year);
        return ap;
	}
    
    public static Opportunity_Sales_Call_Plan__c createSalesCallPlan(Id accountId, string developerName){
    	Schema.DescribeSObjectResult opportunitySalesCallPlanSchema = Opportunity_Sales_Call_Plan__c.sObjectType.getDescribe();
		Map<String, Schema.RecordTypeInfo> recordTypeInfo = opportunitySalesCallPlanSchema.getRecordTypeInfosByDeveloperName();
        Id recordTypeId = recordTypeInfo.get(developerName).getRecordTypeId();
        
        Opportunity_Sales_Call_Plan__c scp = new Opportunity_Sales_Call_Plan__c(Name = developerName, RecordTypeId = recordTypeId, Account__c = accountId);
        return scp;
	}
    
    public static OpportunityWinPlan__c createOpportunityWinPlan(Id opportunityId, string developerName){
    	Schema.DescribeSObjectResult opportunityWinPlanSchema = OpportunityWinPlan__c.sObjectType.getDescribe();
		Map<String, Schema.RecordTypeInfo> recordTypeInfo = opportunityWinPlanSchema.getRecordTypeInfosByDeveloperName();
        Id recordTypeId = recordTypeInfo.get(developerName).getRecordTypeId();
        
        OpportunityWinPlan__c owp = new OpportunityWinPlan__c(Name = developerName, RecordTypeId = recordTypeId,  opportunity__c = opportunityId);
        return owp;
	}
}