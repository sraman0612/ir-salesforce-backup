/*

Purpose: 
Date: Mar 14, 2018
(c) CloudGofer.com

*/

@isTest
public with sharing class PT_MassUpdateTest {
            
    static testmethod void testUpdateOpps(){

        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
        Account a = new Account(
            RecordTypeId = ptAcctRTID,
            Name = 'TestAccoppusher',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );
        insert a;
        
        PT_CreateScheduleOpp.testMonths = 1;
                
        PT_Parent_Opportunity__c pOpp = TestUtilityClass.createParentOpp(a.Id);
        pOpp.Lead_SourcePL__c='Other';
        insert pOpp;
        
        // create an oppty
        Id PT_OpptyRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();  
        
        List<Opportunity> opps = new List<Opportunity>();

        for(Integer i=0;i<3;i++){
            Opportunity opp = new Opportunity();
            opp.PT_IR_Sales_Rep__c = 'Bruno Almeida';
            opp.Name = 'Test'+i;
            opp.StageName = '1.Target';
            opp.CloseDate = system.today();
            opp.PT_Invoice_Date__c = system.today();
            opp.PT_parent_Opportunity__c = pOpp.Id;
            opp.RecordTypeId = PT_OpptyRecordTypeId;
            opp.AccountId = a.Id;
            opps.add(opp);
        }
        insert opps;
  
        Test.startTest();

        // Update amount so that it trigger recalc on parent oppty and account
        List<Opportunity> opps2 = [select id, amount, stageName,PT_Invoice_Date__c from opportunity where PT_parent_opportunity__c=:pOpp.Id];
        for(integer i=0;i<opps2.size();i++){
            opps2[i].amount = 200;
            opps2[i].PT_Invoice_Date__c = system.today();
            
        }
        
        update opps2;              
        
        PT_MassUpdateOpp upOpp = new PT_MassUpdateOpp();
        Database.executeBatch(upOpp);
        
        Test.stopTest();
    }
    
    static testmethod void testUpdateParentOpps(){

        Id ptAcctRTID = [SELECT Id FROM RecordType WHERE sObjectType='Account' and DeveloperName='PT_Powertools'].Id;
        Account a = new Account(
            RecordTypeId = ptAcctRTID,
            Name = 'TestAccoppusher',
            Type = 'Customer',
            PT_Status__c = 'New',
            PT_IR_Territory__c = 'North',
            PT_IR_Region__c = 'EMEA'
        );
        insert a;

        PT_CreateScheduleOpp.testMonths = 1;
        
        List<PT_Parent_Opportunity__c> opps = new List<PT_Parent_Opportunity__c>();
        
        for(Integer i=0;i<3;i++){
            PT_Parent_Opportunity__c opp = new PT_Parent_Opportunity__c();
            opp.Account__c = a.id;
            opp.Sales_Rep__c = 'Bruno Almeida';
            opp.Name = 'Test'+i;
            opp.Lead_SourcePL__c='Other';
            opps.add(opp);
        }
        insert opps;

        Test.startTest();
        
        PT_MassUpdateParentOpp upOpp = new PT_MassUpdateParentOpp();
        Database.executeBatch(upOpp);

        Test.stopTest();
    }
}