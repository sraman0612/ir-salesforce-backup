public without sharing class IR_ServiceAgreementAssignment_Batch implements Database.Batchable<sObject> {

    private static final String F_QUERY = 'SELECT Id, Agreement_Start_Date__c, Agreement_End_Date__c FROM Service_Agreement__c WHERE Agreement_End_Date__c >= TODAY AND Assets_Associated__c = FALSE';
    
    public final string RUNNING_QUERY;

    public IR_ServiceAgreementAssignment_Batch(String iQuery){
        RUNNING_QUERY = String.isEmpty(iQuery) ? F_QUERY : iQuery;      
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(RUNNING_QUERY);
    }

    public void execute(Database.BatchableContext bc, List<Service_Agreement__c> scope){
        IR_ServiceAgreementAssignment_Helper.doWork(scope);
    }

    public void finish(Database.BatchableContext bc){
        System.debug('IR_ServiceAgreementAssignment_Batch END');
    }
}