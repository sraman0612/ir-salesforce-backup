/*------------------------------------------------------------
Author:       Mahesh Karadkar(Cognizant)
Description:  This is the test class for 'C_UpdateParent'.
Case #02900279 
------------------------------------------------------------*/
@isTest
public class C_UpdateParent_Test {
    @testSetup
    static void setupData(){
        
        User User1 = [select id from user where Profile.Name= 'Order Management' AND isActive = True limit 1];  
        Id accRTID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
        TestDataUtility objTestDataUtility = new TestDataUtility();
        
        Account objAccount = objTestDataUtility.createAccount('GD Parent');
        objAccount.ShippingCountry = 'USA';
        objAccount.ShippingCity = 'Augusta';
        objAccount.ShippingState = 'GA';
        objAccount.ShippingStreet = '2044 Forward Augusta Dr';
        objAccount.ShippingPostalCode = '566';
        insert objAccount;
        
        Contact objContact = new contact(FirstName = 'test', LastName = 'Test', Title ='Mr',email = 'test@email.ir.com', AccountId = objAccount.Id);
        objContact.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('End_Customer').getRecordTypeId();
        insert objContact;
        
        Id caseRTCustomerServiceId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Customer_Service').getRecordTypeId();
        Id caseRTInetrnalAIId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Internal_Case').getRecordTypeId();
        
        Case parentCase = new Case();
        parentCase.Account = objAccount;
        parentCase.Contact = objContact;
        parentCase.Origin = 'Email';
        parentCase.Type = 'Question';
        parentCase.Subject = 'Test Case';
        parentCase.recordTypeId = caseRTCustomerServiceId;
        insert parentCase;
        
        list<Case> InternalActionItemList = new list<Case>();
        Case internalActionItem = new Case();
        internalActionItem.Status ='Pending Approval' ;
        internalActionItem.ParentId = parentCase.Id;
        internalActionItem.AI_Email_Subject__c = 'Test AI 1';
        internalActionItem.RecordTypeId = caseRTInetrnalAIId;
        internalActionItem.IC_Acton_Item_Owner__c = UserInfo.getUserId();
        internalActionItem.OwnerId = User1.id;
        InternalActionItemList.add(internalActionItem);
        
        Case internalActionItem2 = new Case();
        internalActionItem2.Status ='More Info Required' ;
        //internalActionItem2.ParentId = parentCase.Id;
        internalActionItem2.AI_Email_Subject__c = 'Test AI 1';
        internalActionItem2.RecordTypeId = caseRTInetrnalAIId;
        internalActionItem2.IC_Acton_Item_Owner__c = UserInfo.getUserId();
        internalActionItem2.OwnerId = User1.id;
        InternalActionItemList.add(internalActionItem2);
        
        insert InternalActionItemList;
    }
    @isTest
    static void testBlankIcon(){
        Case internalActionItem = [select id from case where recordType.developerName = 'Internal_Case' And Status = 'Pending Approval'];
        internalActionItem.Status = 'Closed';
        update internalActionItem;
        system.assertEquals([select id,Unread_Action_Items__c from case where recordtype.developerName = 'Customer_Service'].Unread_Action_Items__c, False);
    }
    @isTest
    static void testBlueIcon(){
        list<Case> internalActionItemlist = [select id,status from case where recordType.developerName = 'Internal_Case' And parentId = null];
        if(!internalActionItemlist.isEmpty()){
            internalActionItemlist[0].parentId = [select id from case where recordtype.developerName = 'Customer_Service'].id;
            internalActionItemlist[0].status = 'Closed';
            update internalActionItemlist[0];
        }
        system.assertEquals([select id,Unread_Action_Items__c from case where recordtype.developerName = 'Customer_Service'].Unread_Action_Items__c, True);
    }
    @isTest
    static void testYellowIcon(){
        Case parentCase = [select id from case where recordtype.developerName = 'Customer_Service'];
        list<case> caseToUpadteList = new list<case>();
        for(case objCaseAi: [select id,status,parentId from case where recordtype.developerName = 'Internal_Case']){
            if(objCaseAi.ParentId == null ){
                objCaseAi.ParentId = parentCase.Id;
                caseToUpadteList.add(objCaseAi);
            }
            else{
                objCaseAi.Status = 'Closed';
                caseToUpadteList.add(objCaseAi);
            }
        }
        if(!caseToUpadteList.isEmpty()){
            update caseToUpadteList;
        }
        system.assertEquals([select id,Unread_Action_Items__c from case where recordtype.developerName = 'Customer_Service'].Unread_Action_Items__c, False);
    }
}