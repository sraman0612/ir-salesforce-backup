/*
Class: orderConfirmationEmailController
Purpose: Logic for the creating the data for the order summary confirmation email
Last Updated: 7/18/2023 E Hardin Providity
Update: Added the product name and the new authorization code fields
*/
public without sharing class orderConfirmationEmailController {
   public User user{get;set;}
    public Id recordId{get;set;}


    public OrderSummary getOrderSummary() {
        OrderSummary orderInfo = [
            SELECT Id, CreatedDate, OrderNumber, GrandTotalAmount, PoNumber, 
            Account.ShippingCity, Account.ShippingState,
	        Account.ShippingStreet, Account.ShippingPostalCode, TotalTaxAmount, TotalAdjustedDeliveryAmount, TotalAdjustedProductAmount,
            BillingPostalCode, BillingState, BillingStreet, BillingCountry, BillingCity, Card_Authorization__c
            FROM OrderSummary
            WHERE ID = :recordId LIMIT 1];
        system.debug('OrderSummary:' + orderInfo);
        return orderInfo;

    }

    public class OrderInfo{
        public List<OrderItemSummary> productInfo {get; set;}
        public OrderDeliveryGroupSummary orderDeliverygrpsummary {get;set;}
    }

    public OrderInfo getOrderDeliveryGrpSummary(){
        OrderInfo ordInfo = new OrderInfo();
        ordInfo.orderDeliverygrpsummary = [SELECT Id, DeliverToStreet, DeliverToCity, DeliverToCountry,
                                                OrderSummaryId, DeliverToState, DeliverToPostalCode, DeliveryInstructions
                                            FROM OrderDeliveryGroupSummary 
                                            WHERE OrderSummaryId =: recordId
                                            LIMIT 1];
        return ordInfo;
    }
    
    public OrderInfo getProductInfo(){
        OrderInfo ordInfo = new OrderInfo(); 
       	ordInfo.productInfo  =   new List<OrderItemSummary>();  
        

        List<OrderItemSummary> productsToAdd = [
            SELECT Id, Name, Product2.Name, Product2.Family, Product2.StockKeepingUnit, OrderSummaryId, Quantity, ListPrice, TotalLineAmount, UnitPrice, StockKeepingUnit
		    FROM OrderItemSummary
            WHERE OrderSummaryId =:recordId
            ORDER By StockKeepingUnit DESC NULLS LAST
        ];


            for(OrderItemSummary product: productsToAdd) {
                ordInfo.productInfo.add(product);
            }

        return ordInfo;
    }

    public User getUserInfo() {
        OrderSummary ordinfo = [SELECT Id, OwnerId FROM OrderSummary WHERE Id =: recordId LIMIT 1];
        User uInfo = [
            SELECT Id, Name, Email, Phone, FirstName, LastName, CompanyName
            FROM User
            WHERE ID = :ordinfo.OwnerId
        ];

        return uInfo;
    }

    public String getOrderDate() {
        OrderSummary currentOrder = [
            SELECT Id, CreatedDate
            FROM OrderSummary
            WHERE Id =:recordId LIMIT 1 
        ];

        String orderDate = currentOrder.CreatedDate.format('MMMMM dd, yyyy');

        return orderDate;

    }
}