/**
	@author Providity
	@date 10FEB2019
	@description Get all data categories associated with Knowledge
*/
public without sharing class CTS_DataCategoryGroups {
    /**
		Return All Data Categories associated with Knowledge
	*/
    public static List<Object> getDescribeDataCategoryGroupStructureResults() {
        List<DescribeDataCategoryGroupResult> describeCategoryResult;
        List<DescribeDataCategoryGroupStructureResult> describeCategoryStructureResult;
        
        try {
            List<String> objType = new List<String>();
            objType.add('KnowledgeArticleVersion');
            describeCategoryResult = Schema.describeDataCategoryGroups(objType);

            List<DataCategoryGroupSobjectTypePair> pairs = new List <DataCategoryGroupSobjectTypePair>();

            for(DescribeDataCategoryGroupResult singleResult: describeCategoryResult) {
                DataCategoryGroupSobjectTypePair p = new DataCategoryGroupSobjectTypePair();
                p.setSobject(singleResult.getSobject());
                p.setDataCategoryGroupName(singleResult.getName());
                pairs.add(p);
            }

            describeCategoryStructureResult = Schema.describeDataCategoryGroupStructures(pairs, false);

            for(DescribeDataCategoryGroupStructureResult singleResult: describeCategoryStructureResult) {
                                
                DataCategory[] toplevelCategories = singleResult.getTopCategories(); //Get the top level categories                
                List<DataCategory> allCategories = getAllCategories(toplevelCategories); //Recursively get all the categories
                for (DataCategory category: allCategories) {               
                    DataCategory[] childCategories = category.getChildCategories();
                }
            }
        } catch (Exception e) {
            System.debug('Something is broken!!!');
        }
        return new List<Object> {JSON.serialize(describeCategoryStructureResult)};
    }

    private static DataCategory[] getAllCategories(DataCategory[] categories) {
        if (categories.isEmpty()) {
            return new DataCategory[] {};
        } else {
            DataCategory[] categoriesClone = categories.clone();
            DataCategory category = categoriesClone[0];
            DataCategory[] allCategories = new DataCategory[] {category};
            categoriesClone.remove(0);
            categoriesClone.addAll(category.getChildCategories());
            allCategories.addAll(getAllCategories(categoriesClone));
            return allCategories;
        }
    }
}