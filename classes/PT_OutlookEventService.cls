/*
	Checkbox bug - Change from account that needs reparenting to an account that doesnt. Does the check box vanish?
	
	Poll for subject/date changes when its an outlook event with an id - Cannot do this as they may change and not save
		- Could visually display that the event should be saved
	
	Deletion of Outlook Events
	Event logging - Should these be kept
	
	- What if a user creates a new event, creates a PT_Outlook_Event__c and then changes the event dates - Need to poll for this
	- If it is already a salesforce event then it should update automatically
*/
public without sharing class PT_OutlookEventService
{
   public class OutlookEventPanelCtrlException extends Exception {}
       
    /*
		For logic see OutlookEventPanelHelper.js
    */
    @auraEnabled public static Map<String, Object> getContactStatuses(String[] emailAddresses, Id accountId)
    {
        Map<String,Object> result = new Map<String, Object>();
        result.put('isSuccess', true);
        result.put('method', 'getContactStatuses');
        result.put('emailAddresses', emailAddresses);
        try
        {
            if(String.isEmpty(accountId)) throw new OutlookEventPanelCtrlException('No account Id');
            Map<String,Contact> contactEmailToContact = new Map<String,Contact>();
            
            Id powerToolsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
            Contact[] contacts = [Select Id, Email, AccountId, Account.Name from Contact where RecordTypeId = :powerToolsContactRecordTypeId AND email In :emailAddresses];
            
            for(Contact thisContact: contacts)
            {
                contactEmailToContact.put(thisContact.Email, thisContact);
            }
            result.put('contactEmailToContact',contactEmailToContact);
            
            Map<String,Object> attendees = new Map<String,Object>();
            for(String emailAddress: emailAddresses)
            {
                emailAddress = emailAddress.toLowerCase();
                Map<String,String> attendee = new Map<String,String>();
                if(!contactEmailToContact.containsKey(emailAddress))
                {
                    attendee.put('statusCode', '003');
                    attendees.put(emailAddress, attendee); //Not found - needs creating
                }
                else
                {
                    Contact thisContact = contactEmailToContact.get(emailAddress);
                    attendee.put('accountName', thisContact.Account.Name);
                    if(thisContact.AccountId == accountId) 
                    {
                        attendee.put('statusCode', '005');
                        attendee.put('Id', thisContact.Id);
                        attendees.put(emailAddress, attendee); //Present and on correct account - no action needed
                    }
                    else
                    {
                        attendee.put('statusCode', '004');
                        attendee.put('Id', thisContact.Id);
                        attendees.put(emailAddress, attendee); //Exists but incorrect account - Reparent
                    }
                }
            }
            
            result.put('attendees',attendees);
            
        }
        catch(Exception e)
        {
            result.put('isSuccess', false);
            result.put('errorMessage','Exception:'+e.getMessage()+ ' - ' + e.getStackTraceString());
        }
        
        return result;
    }
    
    @auraEnabled
    public static Map<String,Object> getEvent(String eventContextJson)
    {
        
        Map<String,Object> result = new Map<String, Object>();
        result.put('method', 'getEvent');
        result.put('isSuccess', true);
        result.put('messages',new String[]{});
        result.put('UserInfo.getUserName()',UserInfo.getUserName());
        Integer timezoneOffset = UserInfo.getTimezone().getOffset(datetime.now()); 
        timezoneOffset = timezoneOffset/1000;	//Seconds
        timezoneOffset = timezoneOffset/60;		//Minutes
        timezoneOffset = timezoneOffset/60;		//Hours
        result.put('timezoneOffset',timezoneOffset);
        try
        {
            if(String.isEmpty(eventContextJson)) throw new OutlookEventPanelCtrlException('No event context');
            
            Map<String,Object> eventContext = (Map<String,Object>)JSON.deserializeUntyped(eventContextJson);
            result.put('eventContext', eventContext);
            
            String subject = (String)eventContext.get('subject');
            
            Map<String, DateTime> localDates = getLocalDateTimes(eventContext);
            DateTime localStartDateTime = localDates.get('localStartDateTime');   
            DateTime localEndDateTime = localDates.get('localEndDateTime'); 
            result.put('localStartDateTime', localStartDateTime);
            result.put('localEndDateTime', localEndDateTime);
            
            Id powerToolsEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
            result.put('query','Select Id from event where subject = '+subject+' AND RecordTypeId = '+powerToolsEventRecordTypeId+' AND startdatetime = '+localStartDateTime+' AND EndDateTime = '+localEndDateTime);
            sObject[] events = selectEvents(subject, localStartDateTime, localEndDateTime);
            if(!events.isEmpty()) 
            {
                result.put('objectType', 'Event');
             	Event thisEvent = (Event)events[0];
                if(!String.isEmpty(thisEvent.WhatId))
                {
                   Id whatId = thisEvent.WhatId;
                
                    String relatedToSObjectType = whatId.getSObjectType().getDescribe().getName();
                    if(relatedToSObjectType == 'Account')
                    {
                       	Account relatedAccount = [Select Id, Name from Account where Id = :whatId];
                        result.put('RelatedAccount',relatedAccount);
                    } 
                }
            }
            else 
            {
                events = selectOutlookEvents(subject, localStartDateTime, localEndDateTime);
                result.put('objectType', 'PT_Outlook_Event__c');
            }
            
            result.put('events', events);   
        }
        catch(Exception e)
        {
            result.put('isSuccess', false);
            result.put('errorMessage','Exception:'+e.getMessage()+ ' - ' + e.getStackTraceString());
        }
        
        
        return result;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<LookupSearchResult> search(String searchTerm, List<String> selectedIds, String anOptionalParam) {
        // We're not using anOptionalParam parameter
        // it's just here to demonstrate custom params
        
        // Prepare query paramters
        searchTerm += '*';
        
        // Execute search query
        List<List<SObject>> searchResults = [FIND :searchTerm IN ALL FIELDS RETURNING
                                             Account (Id, Name, BillingCity WHERE id NOT IN :selectedIds)
                                             LIMIT :10];
        
        // Prepare results
        List<LookupSearchResult> results = new List<LookupSearchResult>();
        
        // Extract Accounts & convert them into LookupSearchResult
        String accountIcon = 'standard:account';
        Account [] accounts = ((List<Account>) searchResults[0]);
        for (Account account : accounts) {
            results.add(new LookupSearchResult(account.Id, 'Account', accountIcon, account.Name, 'Account • '+ account.BillingCity));
        }
        
        return results;
    }
    
    /*
    OnLoad          OnSave                                                  Action
    None            None (A)                                                Insert Outlook          
                    Salesforce (B)                                          Update Salesforce       
    Outlook         Outlook (C)                                             Update Outlook          
                    Salesforce (Outlook will be linked) (D)                 Update Salesforce       
    Salesforce      Salesforce (E)                                          Update Salesforce       
    
    
    Test cases
    ============
    A - Create new, Click update button
            New outlook event inserted
    C - Reopen A, make a change. Click update button
            Existing outlook event updated
    D - Reopen A, Manually create SF event, check copies across, click update button
            The Salesforce event should be updated not the outlook event
    B - Create new outlook, In Salesforce manually create event, Click update button
            No outlook event in SF db. SF event updated
    E - Open an event with an SF event, Edit it, click update      
            SF event should update
	*/
    @auraEnabled public static Map<String,Object> updateEvent(String eventContextJson)
    {   
        System.debug('[PT_OutlookEventService.updateEvent]');
        Map<String,Object> result = new Map<String, Object>();
        result.put('method', 'updateEvent');
        try
        {
            if(String.isEmpty(eventContextJson)) throw new OutlookEventPanelCtrlException('No event context');            
            Map<String,Object> eventContext = (Map<String,Object>)JSON.deserializeUntyped(eventContextJson);
            
            Id eventId = (Id)eventContext.get('id');
            String subject = (String)eventContext.get('subject');
            String whom = (String)eventContext.get('whom');
            String accountId = (String)eventContext.get('accountId');
			String eventType = (String)eventContext.get('type');
            
            Map<String, DateTime> localDates = getLocalDateTimes(eventContext);
            DateTime localStartDateTime = localDates.get('localStartDateTime');   
            DateTime localEndDateTime = localDates.get('localEndDateTime');

            String eventSobjectType = 'None';
            if(!String.isEmpty(eventId))
            {
                eventSObjectType = eventId.getSObjectType().getDescribe().getName();
            }
            
            if(String.isEmpty(eventId) || eventSobjectType == 'PT_Outlook_Event__c')
            {
                Event[] refreshedEvents = selectEvents(subject, localStartDateTime, localEndDateTime);
                if(!refreshedEvents.isEmpty())
                {
                    eventId = refreshedEvents[0].Id;
                    eventSObjectType = eventId.getSObjectType().getDescribe().getName();
                }            	
            }
   
            SObject thisEvent;
            if(!String.isEmpty(eventId))
            {   
                if(eventSObjectType == 'PT_Outlook_Event__c')
                {
                    thisEvent = upsertOutlookEvent(eventId, subject, localStartDateTime, localEndDateTime, eventType, whom, accountId);
                }
                else
                {
                    thisEvent = updateSalesforceEvent(eventId,  eventType, whom, accountId);    
                }      
            }
            else
            { 
                thisEvent = upsertOutlookEvent(null, subject, localStartDateTime, localEndDateTime, eventType, whom, accountId);
                eventSObjectType = 'PT_Outlook_Event__c';
            }

            Object[] attendees = (Object[])eventContext.get('contacts');
            
            Map<String, Contact> upsertedContactsByEmail = upsertContacts(attendees, thisEvent, accountId);
                        
            createEventRelations(thisEvent, attendees, upsertedContactsByEmail);
            
            result.put('isSuccess', true);
            result.put('objectType',eventSobjectType);
            result.put('upsertedEvent',thisEvent);
            
            result.put('localStartDateTime', localStartDateTime);
            result.put('localEndDateTime', localEndDateTime);
            result.put('eventContext', eventContext);
            result.put('upsertedContacts', upsertedContactsByEmail.values());

        }
        catch(Exception e)
        {
            result.put('isSuccess', false);
            result.put('eventContextJson',eventContextJson);
            result.put('errorMessage','Exception:'+e.getMessage()+ ' - ' + e.getStackTraceString());
        }
        
        return result;
    }
    
    private static void createEventRelations(SObject thisEvent, Object[] attendees, Map<String, Contact> upsertedContactsByEmail)
    {
        System.debug('[PT_OutlookEventService.createEventRelations]');
        Id eventId = thisEvent.Id;
        String eventSObjectType = eventId.getSObjectType().getDescribe().getName();
        
        EventRelation[] eventRelations = new EventRelation[]{};

		//Map<eventId,Map<contactId,EventRelation>>            
        Map<Id,Map<Id,EventRelation>> eventRelationContactIds = PT_EventRelationService.getExistingEventRelationIds(new Set<Id>{eventId});
        Map<Id, EventRelation> existingEventRelationToContactId = eventRelationContactIds.get(eventId);
        
        Set<Id> attendeeContactIds = new Set<Id>();
        System.debug('[PT_OutlookEventService.createEventRelations] attendees:'+attendees);
        for(Object attendee: attendees)
        {
            Map<String,Object> thisAttendee = (Map<String,Object>)attendee;
            String statusCode = (String)thisAttendee.get('statusCode');
            String attendeeEmail = (String)thisAttendee.get('email');
            Boolean attendeeChecked = (Boolean)thisAttendee.get('checked');
            
            //003 = Create contact
            if(attendeeChecked == false && statusCode == '003') continue;
            
            String contactId = (String)thisAttendee.get('Id');

            if(attendeeChecked == true && statusCode == '003')
            {
                contactId = upsertedContactsByEmail.get(attendeeEmail).Id;
            }
            if(existingEventRelationToContactId!=null && existingEventRelationToContactId.containsKey(contactId)) continue;
			attendeeContactIds.add(contactId);
            
            EventRelation thisEmailRelation = new EventRelation();
            thisEmailRelation.EventId = thisEvent.Id; 
            thisEmailRelation.RelationId = contactId; 
            thisEmailRelation.IsParent = true;
            
            eventRelations.add(thisEmailRelation);
        }
        
        
        if(eventSobjectType == 'Event')
        {
            insert eventRelations;
            
            EventRelation[] eventRelationsToDelete = new EventRelation[]{};
            if(existingEventRelationToContactId != null)
            {
                for(Id contactId: existingEventRelationToContactId.KeySet())
                {
                    if(!attendeeContactIds.contains(contactId))
                    {
                        eventRelationsToDelete.add(existingEventRelationToContactId.get(contactId));
                    }
                }  
            }

            
            delete eventRelationsToDelete;
        }
        else
        {
            thisEvent.put('Event_Relations__c', JSON.serializePretty(eventRelations));
            update thisEvent;                
        }
    }
    
    private static Map<String, Contact> upsertContacts(Object[] attendees, SObject thisEvent, String accountId)
    {
        Id ptContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();
        String thisEventId = thisEvent.Id;
          
        Map<String,Contact> emailToContactForUpsert = new Map<String,Contact>();
        for(Object attendee: attendees)
        {
            Map<String,Object> thisAttendee = (Map<String,Object>)attendee;
            String statusCode = (String)thisAttendee.get('statusCode');
            String attendeeEmail = (String)thisAttendee.get('email');
            Boolean attendeeChecked = (Boolean)thisAttendee.get('checked');
            
            if(attendeeChecked == true && (statusCode == '003' || statusCode == '004'))
            {
                Contact thisContact = new Contact();
                thisContact.RecordTypeId = ptContactRecordTypeId;
                thisContact.AccountId = accountId;
                thisContact.Email = attendeeEmail;
                if(statusCode == '003')
                {
                    String names = (String)thisAttendee.get('name');
                    String firstName = '';
                    String lastName = '';
                    Integer spaceIndex = names.indexOf(' ');
                    if(spaceIndex == -1)
                    {
                        thisContact.LastName = names;
                    }
                    else
                    {
                        thisContact.FirstName = names.subString(0, spaceIndex);
                        thisContact.LastName = names.subString(spaceIndex, names.length());
                    }
                }
                else
                {
                    thisContact.Id = (String)thisAttendee.get('Id');
                }
                
                emailToContactForUpsert.put(attendeeEmail, thisContact);  
            }
        }
        
        upsert emailToContactForUpsert.values(); //Contacts
        return emailToContactForUpsert;
    }
    
    private static Map<String, DateTime> getLocalDateTimes(Map<String,Object> eventContext)
    {
        Map<String,Object> dates = (Map<String,Object>)eventContext.get('dates');
        Map<String, DateTime> result = new Map<String, DateTime>();

        if(dates == null) throw new OutlookEventPanelCtrlException('No dates found in event context');
        String startDateTimeString = (String)dates.get('start');
        startDateTimeString = startDateTimeString.replace('T',' ');
        startDateTimeString = startDateTimeString.left(19);
        
        DateTime localStartDateTime = DateTime.valueOf(startDateTimeString);   
        
        String endDateTimeString = (String)dates.get('end');
        endDateTimeString = endDateTimeString.replace('T',' ');
        endDateTimeString = endDateTimeString.left(19);
        
        DateTime localEndDateTime = DateTime.valueOf(endDateTimeString);
        
        Integer offset = UserInfo.getTimezone().getOffset(datetime.now()); 
        localStartDateTime = localStartDateTime.addSeconds(offset/1000);
        localEndDateTime = localEndDateTime.addSeconds(offset/1000);
        
        result.put('localStartDateTime',localStartDateTime);
        result.put('localEndDateTime',localEndDateTime);

        return result;
    }

    private static PT_Outlook_Event__c upsertOutlookEvent(Id eventId, string subject, Datetime localStartDateTime, Datetime localEndDateTime, String eventType, String whom, String accountId)
    {
        PT_Outlook_Event__c outlookEvent = new PT_Outlook_Event__c();
        
        outlookEvent.put('Start_Date_Time__c',localStartDateTime);
        outlookEvent.put('End_Date_Time__c',localEndDateTime);
        
        outlookEvent.put('Subject__c', subject);
        
        outlookEvent.put('Type__c', eventType);
        outlookEvent.put('Whom__c', whom);
        outlookEvent.put('Account__c',accountId);
        
        if(!String.isEmpty(eventId)) outlookEvent.Id = eventId;
                        
        upsert outlookEvent;
        return outlookEvent;
    }
    
    private static Event updateSalesforceEvent(String eventId, String eventType, String whom, String accountId)
    {
        System.debug('Update Event. AccountId:'+accountId);
        Event eventToUpdate = new Event();
        eventToUpdate.Id = eventId;
        eventToUpdate.put('Type',eventType);
        eventToUpdate.put('WhatId',accountId);
        eventToUpdate.put('PT_Whom__c', whom);
        
        update eventToUpdate;
        
        return eventToUpdate;
    }
    
    private static event[] selectEvents(String subject, DateTime localStartDateTime, DateTime localEndDateTime)
    {
        System.debug('PT_OutlookEventService.selectEvents');
        Id powerToolsEventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByDeveloperName().get('PT_powertools').getRecordTypeId();

		//For this load we can use exact start date time as we are using the sf event
        //https://developer.salesforce.com/docs/component-library/bundle/clients:hasItemContext/documentation
        
        //If not organiser
        EventRelation[] attendedEvents = [
			select 	id, RelationId, Event.Id, Event.Subject, Event.startdatetime, Event.EndDateTime, Event.Type, Event.WhatId, Event.PT_Whom__c
            from 	eventrelation 
            where 	RelationId = :UserInfo.getUserId() And 
            		Event.RecordTypeId = :powerToolsEventRecordTypeId AND
                    Event.subject = :subject AND
            		Event.startdatetime = :localStartDateTime AND
                    Event.EndDateTime = :localEndDateTime     		
        ];
        
        if(!attendedEvents.isEmpty())
        {
            EventRelation attendedEventRelation = attendedEvents[0];
            Event attendedEvent = new Event();
            attendedEvent.Id = attendedEventRelation.Event.Id;
            attendedEvent.Subject = attendedEventRelation.Event.Subject;
            attendedEvent.startdatetime = attendedEventRelation.Event.startdatetime;
            attendedEvent.EndDateTime = attendedEventRelation.Event.EndDateTime;
            attendedEvent.Type = attendedEventRelation.Event.Type;
            attendedEvent.WhatId = attendedEventRelation.Event.WhatId;
            attendedEvent.PT_Whom__c = attendedEventRelation.Event.PT_Whom__c;
            
            return new Event[]{attendedEvent};
            
        }
        else
        {    
            return [
                Select 	Id, startdatetime, EndDateTime, subject, Type, WhatId, PT_Whom__c
                from 	event 
                where 	OwnerId = :UserInfo.getUserId() And
                        RecordTypeId = :powerToolsEventRecordTypeId AND
                        Subject = :subject AND
                        startdatetime = :localStartDateTime AND
                        EndDateTime = :localEndDateTime
            ];
            
        }
        
        
        

        /*
        System.debug('Select Id from event where subject = '+subject+' AND RecordTypeId = '+powerToolsEventRecordTypeId+' AND startdatetime = '+localStartDateTime+' AND EndDateTime = '+localEndDateTime);
        return [
            Select 	Id, startdatetime, EndDateTime, subject, Type, WhatId, PT_Whom__c
           	from 	event 
            where 	subject = :subject AND
            		RecordTypeId = :powerToolsEventRecordTypeId AND
                    startdatetime = :localStartDateTime AND
                    EndDateTime = :localEndDateTime
    	];
		*/
		
    }
    
    private static PT_Outlook_Event__c[] selectOutlookEvents(String subject, DateTime localStartDateTime, DateTime localEndDateTime)
    {
        /*
         	Solutions
			=========
         	Can we put settings for this
				May need to include the linking select 
         
         	Do PT users often have more than 1 event at a time? Can check by looking at calendars. 
				If not then MAYBE using only start/end time is possible
			

			Potential query
        	Dates within a range and subject is the same
			Date is the same and subject starts or ends with the same x number of letters

			Current Solution
			================
			Get Oulook Events where start/end time is the same AND
			The outlook event is created by current user OR the subject is the same

			Journey
			========
        	Open new event
				- Save event (Before synced to outlook)
				- Change time or subject
				- Close event
				- Will not connect to SF event
				- Reopen event - Sync info lost
		
			Open existing event (Not synced yet)
				- Change subject and/or time
				- Close event
				- Will not connect to SF event as dates/subject will be wrong
				- Reopen event - Sync info lost


			What if based on this more than one event found?
        */
        
        return [
            Select 	Id, Subject__c, Type__c, Start_Date_Time__c, End_Date_Time__c, Account__c, Event_Id__c, Account__r.Name, Whom__c
            From	PT_Outlook_Event__c
            where 	(
                		CreatedById = :UserInfo.getUserId() OR
                		Subject__c = :subject 
                	) AND
            		Start_Date_Time__c = :localStartDateTime AND
            		End_Date_Time__c = :localEndDateTime
        ];
    }
}