/***********************************************************************
 Class          : serviceAgreementBatchTest 
 Created Date   : 9 Dec 2016
 Descritption   : Test Class for serviceAgreementBatchTest
 ************************************************************************/
@isTest
public class serviceAgreementBatchTest {
  static testmethod void test1(){
  String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' and Name = 'IR Comp MEIA'].Id;
        /*Division__c divObj = new Division__c();
        divObj.Name = 'Greensboro Customer Center';
        divObj.Oracle_Sales_Rep__c = '2282';
        divObj.Organization_Name__c = 'Customer Center';
        divObj.Pricelist__c = 'ITS_PRIMARY';
        divObj.Siebel_Id__c = '1-3DRAA';
        divObj.EBS_System__c = 'Oracle 11i';
        divObj.Division_Type__c = 'Customer Center';
        divObj.CurrencyIsoCode = 'USD';
        divObj.Area__c = 'Mid Atlantic';
        insert divObj;*/
        Account acc = new Account();
        //acc.Account_Division__c = divObj.Id;
        acc.Account_Region__c = 'Oracle 11i';
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'USA';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.CTS_Global_Shipping_Address_1__c = '13';
        acc.CTS_Global_Shipping_Address_2__c = 'street2';        
        acc.County__c = 'USA';
        acc.RecordTypeId = strRecordTypeId;
        insert acc;
      Service_Agreement__c sAgg = new Service_Agreement__c();
      sAgg.Account__c = acc.id;
      sAgg.Agreement_Number__c = 'SATEST123';
      sAgg.Agreement_Start_Date__c = System.TODAY();
      sAgg.Agreement_End_Date__c = System.TODAY() + 175;
      System.assertEquals(sAgg.Account__c, acc.id);
      insert sAgg;
      system.debug('Agreement is ' +sAgg);
      SchedulableContext sc;
      serviceAgreementBatch sAggScheduler=new serviceAgreementBatch();
      test.startTest();
      sAggScheduler.execute(sc);   
      //database.executeBatch(sAggScheduler);
      test.stopTest();
  
  }
}