@isTest
private class CTS_CentacPartTableControllerTest {     
    @testSetup static void setup() {
        List<Account> accList = CTS_TestDataFactory.createLatamAccounts(1);
        List<Opportunity> opptyList = CTS_TestDataFactory.createLatamOppoty(1, accList[0]);
        List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = CTS_TestDataFactory.createCentacPartQuote(1, opptyList[0]);
        List<Centac_RFQ_to_Fill__c>centacPartsList = new List<Centac_RFQ_to_Fill__c>();
        for(integer i=0;i<6;i++){
            Centac_RFQ_to_Fill__c centacPartObj = new Centac_RFQ_to_Fill__c();
            centacPartObj.Centac_Parts_Quote__c = centacPartQuoteList[0].Id;
            centacPartObj.Quantity__c = 1;
            centacPartObj.Description__c = 'Test';
            centacPartObj.CCN__c = '1223';
            centacPartObj.Location_in_Compressor__c = 'Test';
            centacPartObj.Drawing_or_PN__c = 'test12323';
            centacPartObj.Serial__c = 'test12323';
            centacPartObj.Notes__c = 'Test';
            centacPartsList.add(centacPartObj);
        }
        insert centacPartsList;
    }
    @isTest static void testPartInsert() {
        List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = [SELECT Id FROM CTS_Centac_Parts_Quote_ReRate__c];
        Test.startTest();
        CTS_CentacPartTableController.DataTableWrapper dataWrapperObj = CTS_CentacPartTableController.init(centacPartQuoteList[0].Id, false, true);
        List<Centac_RFQ_to_Fill__c>centacPartsList = dataWrapperObj.dataList;
        System.assertEquals(centacPartsList.size(), 6);
        Centac_RFQ_to_Fill__c centacPartObj = new Centac_RFQ_to_Fill__c();
        centacPartObj.Quantity__c = 1;
        centacPartObj.Centac_Parts_Quote__c = centacPartQuoteList[0].Id;
        centacPartObj.Description__c = 'Test';
        centacPartObj.CCN__c = '1223';
        centacPartObj.Location_in_Compressor__c = 'Test';
        centacPartObj.Drawing_or_PN__c = 'test12323';
        centacPartObj.Serial__c = 'test12323';
        centacPartObj.Notes__c = 'Test';
        centacPartsList.add(centacPartObj);
        String jsonString = JSON.serialize(centacPartsList);
        List<String> deleteRecordIds  = new List<String>();
        CTS_CentacPartTableController.updateRecords(jsonString, centacPartQuoteList[0].Id, deleteRecordIds,true ,false);
        List<Centac_RFQ_to_Fill__c > newCentacPartList = [SELECT Id FROM Centac_RFQ_to_Fill__c where Centac_Parts_Quote__c =: centacPartQuoteList[0].Id];
        system.assertEquals(newCentacPartList.size(),7 );
        Test.stopTest();
        
    }
    @isTest static void testPartUpdate() {
        List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = [SELECT Id FROM CTS_Centac_Parts_Quote_ReRate__c];
        
        Test.startTest();
        CTS_CentacPartTableController.DataTableWrapper dataWrapperObj = CTS_CentacPartTableController.init(centacPartQuoteList[0].Id, true, true);
        List<Centac_RFQ_to_Fill__c>centacPartList = dataWrapperObj.dataList;
        System.assertEquals(centacPartList.size(), 6);
        System.assertEquals(centacPartList[0].List_Price__c, null);
        centacPartList[0].List_Price__c = 12;
        String centacPartListJSON = JSON.serialize(centacPartList);
        List<String> deleteRecordIds  = new List<String>();
        CTS_CentacPartTableController.updateRecords(centacPartListJSON, centacPartQuoteList[0].Id, deleteRecordIds, false,true);
        System.assertEquals(centacPartList[0].List_Price__c, 12);
        Test.stopTest();
        
    }
     @isTest static void testPartDelete() {
        List<CTS_Centac_Parts_Quote_ReRate__c > centacPartQuoteList = [SELECT Id FROM CTS_Centac_Parts_Quote_ReRate__c];
        
        Test.startTest();
        CTS_CentacPartTableController.DataTableWrapper dataWrapperObj = CTS_CentacPartTableController.init(centacPartQuoteList[0].Id, true, true);
        List<Centac_RFQ_to_Fill__c>centacPartList = dataWrapperObj.dataList;
        system.assertEquals(centacPartList.size(),6 );
        String centacPartListJSON = JSON.serialize(centacPartList);
        List<String>deleteId = new List<String>();
        deleteId.add(centacPartList[0].Id);
        CTS_CentacPartTableController.updateRecords(centacPartListJSON, centacPartQuoteList[0].Id, deleteId,true, true);
        List<Centac_RFQ_to_Fill__c > newCentacPartList = [SELECT Id FROM Centac_RFQ_to_Fill__c where Centac_Parts_Quote__c =: centacPartQuoteList[0].Id];
        system.assertEquals(newCentacPartList.size(),5 );
        Test.stopTest();
        
    }
}