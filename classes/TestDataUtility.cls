@isTest
public class TestDataUtility {

    private static Boolean leaseNatureCreated = false;
    
    public user createPartnerUser(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);

        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(UserRoleId = portalRole.Id, ProfileId = profile1.Id,
                                           Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                                          Email='test@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                                          Firstname='test', Lastname='Test',
                                          LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                                          TimeZoneSidKey='America/New_York',country='USA');
           Database.insert(portalAccountOwner1);

        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        user user1;
        System.runAs ( portalAccountOwner1 ) {
            Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
           
            //creating state country custom setting data
            TestDataUtility.createStateCountries();
            //Commented as part of EMEIA Cleanup
            //creating county
           // CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA'); 
           // insert county;
            
             //creating sales rep here
              TestDataUtility testData = new TestDataUtility();
           //Commented as part of EMEIA Cleanup
              /* CC_Sales_Rep__c salesRepacc = testData.createSalesRep('1000');
           salesRepacc.CC_Sales_Rep__c = userinfo.getUserId(); 
           insert salesRepacc;*/
            //Create account
            Account portalAccount1 = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id, RecordTypeId=clubcarRecordTypeId,
            BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566'
);
Database.insert(portalAccount1);

            //Create contact
            Contact contact1 = new Contact(FirstName = 'Test',Lastname = 'McTesty',AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.ingersollrand.com');

Database.insert(contact1);

            //Create user
            Profile portalProfile = [SELECT Id FROM Profile where name ='Partner Community User' LIMIT 1];
            user1 = new User(Username = System.now().millisecond() + 'test12345@test.ingersollrand.com',
                                 ContactId = contact1.Id,ProfileId = portalProfile.Id,
                                 Alias = 'test123',Email = 'test12345@test.ingersollrand.com',
                                 EmailEncodingKey = 'UTF-8',LastName = 'McTesty',
                                 CommunityNickname = 'test12345',TimeZoneSidKey = 'America/New_York',country='USA',
                                 LocaleSidKey = 'en_US',LanguageLocaleKey = 'en_US');
            Database.insert(user1);
        }
       return user1; 
    }
    
    
    public Lead createLead(String RectypeName, String productAuthorization){
        
        Id rectypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get(RectypeName).getRecordTypeId();
         Lead l = new Lead();
        l.firstname = 'George';
        l.lastname = 'Acker';
        l.phone = '1234567890';
        l.City = 'city';
        l.Country = 'USA';
        l.PostalCode = '12345';
        l.State = 'AK';
        l.Street = '12, street1';
        l.company = 'ABC Corp';
        l.county__c = 'test county';
        l.RecordTypeId = rectypeId;
        //l.CC_Prod_Authorization__c = productAuthorization;
            
        //l.OwnerId    = ownertoSpecity;
       
        return l;
    }
    
    
    public Id createQueue(String queueName, string devName, String objName){
            Group g1 = new Group(Name= queueName, type='Queue', DeveloperName = devName);
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = objName);
           System.runAs(new User(Id = UserInfo.getUserId()))
            {
            insert q1;
            }
        return g1.id;
           
    }
    //Commented as part of EMEIA Cleaup
   /* public CC_Territory__c CreateTerritory(ID contrId){
        CC_Territory__c ter        = new CC_Territory__c();
        ter.CC_Agreement_Number__c = contrId;
        ter.CC_County__c             = 'test county';
        ter.CC_State__c              = 'AK';
        ter.CC_Country__c            = 'USA';
        ter.CC_Zipcode__c            = '12345';
        ter.CC_Territory_Type__c     = 'Channel';
        
        return ter;
            
        
    }
    
    public static CC_My_Team__c createMyTeam(Id accountId, Id userId, String teamRole){
        
        return new CC_My_Team__c(
            Account__c = accountId,
            User__c = userId,
            My_Team_Role__c = teamRole
        );
    }*/
    
    public static Opportunity createOpportunity(Id recordTypeId, Id accountId, String name, String stage, Date closeDate){
        
        Opportunity opp = new Opportunity(
            RecordTypeId = recordTypeId,
            AccountId = accountId,
            Name = name,
            StageName = stage,
            CloseDate = closeDate
        );
        
        return opp;
    }
    
    public static EmailTemplate createEmailTemplate(String name, String devName, Id folderId, String subject){
        
        EmailTemplate template = new EmailTemplate(
            Name = name,
            DeveloperName = devName,
            FolderId = folderId,
            IsActive = true,
            Subject = subject,
            TemplateType = 'Text',
            Body = 'Test message'
        );
        
        return template;
    }
    /*//Commented as part of EMEIA Cleaup
    public static Contract createCCServiceContract(Id accountId, String cType){
        
        return new Contract(
            RecordTypeId = sObjectService.getRecordTypeId(Contract.sObjectType, 'Club Car Service Contract'),
            AccountId = accountId,
            CC_Type__c = cType
        );
    }*/
    
  /*  public static echosign_dev1__Agreement_Template__c createAdobeAgreementTemplate(String name){
        
        echosign_dev1__Agreement_Template__c template = new echosign_dev1__Agreement_Template__c(
            Name = name
        );
        
        return template;
    }
    --commented SGOM-44*/
    /*//Commented as part of EMEIA Cleaup
    public contract createContract(Id accId, String subtype){
        contract contr = new contract();
        contr.AccountId = accId;
        contr.Status    = 'Draft';
        contr.CC_Sub_Type__c = subtype;
        return contr;
    }
    
    public CC_Sales_Rep__c createSalesRep(string salesPersonNumber){
        
        CC_Sales_Rep__c salesRep           = new CC_Sales_Rep__c();
        salesRep.CC_Sales_Person_Number__c = salesPersonNumber;
        salesRep.Name        = salesPersonNumber;
        salesRep.CC_Type__c  = 'Sales';
        salesRep.CC_Obsolete__c = false;
        return salesRep;
        
    }
    */
    public account createAccount(String RecordTypeNam){
          Id rectypeId           = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecordTypeNam).getRecordTypeId();
         //Commented as part of EMEIA Cleaup
          //creating county
           // CC_Counties__c county = new CC_Counties__c(Name='test',State__c='AA');
           // insert county;
           Account acc = new Account(Name = 'TestAccount', RecordTypeId=rectypeId,
           BillingCity ='TestCity', BillingCountry='USA', BillingStreet='TestStreet', BillingPostalCode='testzip',
          BillingState = 'TestState');
        return acc;
        
    }
    
    public account createAccountName(String Namegiven,String RecordTypeNam){
        Id rectypeId           = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecordTypeNam).getRecordTypeId();
          //CC_Counties__c county = new CC_Counties__c(Name=Namegiven,State__c='AA'); 
          //insert county;
       Account acc = new Account(Name = Namegiven, RecordTypeId=rectypeId,
                                 BillingCity ='Augusta', BillingCountry='USA', BillingStreet='2044 Forward Augusta Dr', BillingPostalCode= '566',BillingState = 'GA'
                               /* CC_Billing_County__c = county.id,,CC_Shipping_Billing_Address_Same__c=true*/);
       
    
      return acc;
      
  }
  
    /*Commented as part of EMEIA Cleaup
    
    public static Club_Car_Payment_Terms__c createCCPaymentTerms(String code, String termsMethod){
        
        Club_Car_Payment_Terms__c terms = new Club_Car_Payment_Terms__c(
            Name = code, 
            CC_Car_Terms_Method__c = termsMethod
        );
        
        return terms;
    }
    */
    public static Case createTransportationCase(String subReason, Id stlId, Double quotedPrice){
        
        Case c = createCase(sObjectService.getRecordTypeId(Case.sObjectType, 'Club Car - Transportation Request'));
        
        c.Reason = 'Transportation Request';
        c.Sub_Reason__c = subReason;
        //Commented as part of EMEIA Cleaup
        //c.CC_Short_Term_Lease__c = stlId;
       // c.CC_Quoted_Price__c = quotedPrice;
        
        return c;
    }
    
    public static Case createCase(Id recordTypeId){
        return new Case(
            RecordTypeId = recordTypeId);
    }    
    
    public static Case createCase(Boolean doInsert, Id recordTypeId, Id contactId, Id accountId, String subject, String status){
        Case newCase = new Case();
        newCase.RecordTypeId = recordTypeId;
        newCase.ContactId = contactId;
        newCase.AccountId = accountId;
        newCase.Subject = subject;
        newCase.Status = status;
        newCase.CTS_User_Department__c = 'CTS OM EU';
        newCase.Assigned_From_Address_Picklist__c = 'OM_General@irco.com';
        if (doInsert){
            insert newCase;
        }
        return newCase;
    }    
    public contact createContact(Id accId){
        contact con = new contact(FirstName = 'test', LastName = 'Test', Title ='Mr', AccountId = accId);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        return con;
    }
    
    public static user createpartnecommunityrUser(){
        Id p = [select id from profile where name='Partner Community User'].id;
         Id clubcarRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Club Car: New Customer').getRecordTypeId();
          Account ac = new Account(name ='Grazitti', RecordTypeId = clubcarRecordTypeId) ;
        insert ac; 
       
        Contact con = new Contact(FirstName = 'con', LastName ='testCon',AccountId = ac.Id);
        con.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        insert con;  
                  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
       
        insert user;
        return user;
    }
    public User createIntegrationUser(){
       Id p = [select id from profile where name='System Administrator'].id;
       User SampleUser = new User(alias = 'Sample', email='SampleUser@gmail.com',
                                   emailencodingkey='UTF-8',FirstName ='Clubcar', lastname='Integration', languagelocalekey='en_US',
                                   localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                                   timezonesidkey='America/Los_Angeles', username='SampleUser123213@gmail.com',CC_View_Parts_Bulletins__c=true,
                                   CC_Pavilion_Navigation_Profile__c='test pavilion navigation profile',PS_Business_Unit_Description__c = 'CLUB CAR',CC_Department__c='Marketing');
       return SampleUser;
    }      
    /*Commented as part of EMEIA Cleaup                          
    public void insertdealerCustomSetting(Id defaultAccID){
        List<CC_Dealer_Locator_Settings__c> dealerSettings = new List<CC_Dealer_Locator_Settings__c>();
        dealerSettings.add(new CC_Dealer_Locator_Settings__c(Name = 'R', value__c='Retail Dealer'));
        dealerSettings.add(new CC_Dealer_Locator_Settings__c(Name = 'Default', value__c=defaultAccID));
        insert dealerSettings;
    }
    
    
    public static CC_Asset__c createAsset(Id accId, Id prodId, Id asgId, Id ordId){
        CC_Asset__c ast      = new CC_Asset__c();
        ast.Account__c       = accId;
        ast.Product__c       = prodId;
        ast.Asset_Group__c   = asgId;
        ast.Order__c         = ordId;
        ast.Status__c        = 'Active';
        ast.Acquired_Date__c = date.today();
        return ast;
        
        
    }
    
    public static CC_Asset_Group__c createAssetGroup(Id accId, Id prodId){
        CC_Asset_Group__c assetGroup  = new CC_Asset_Group__c();
        assetGroup.Account__c         = accId;
        assetGroup.Product__c         = prodId;
        assetGroup.Fleet_Status__c    ='Lease-New';
        
        return assetGroup;
        
    }
    */
    public static Product2 createProduct(String name){
        
        Product2 product = new Product2();
        product.Name = name;
        product.RecordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Club Car').getRecordTypeId();
        product.IsActive = TRUE;
        
        return product;
    }
    /*Commented as part of EMEIA Cleaup
    public static CC_STL_Car_Location__c createCarLocation(){
        
        return new CC_STL_Car_Location__c(
            Name = 'TestLocation',
            Active__c = true,
            Warehouse__c = 'MI'
        );
    }
    
    public static CC_Short_Term_Lease__c createShortTermLease(){
        return createShortTermLease(null, null);
    }
    
    public static CC_Short_Term_Lease__c createShortTermLease(Id masterLeaseId, Id locationId){
        return createShortTermLeaseToSendToMAPICS(masterLeaseId, locationId, null);
    }
    
    public static CC_Short_Term_Lease__c createShortTermLeaseToSendToMAPICS(Id masterLeaseId, Id locationId, Id salesRepId){
        
        Case_Settings__c settings = Case_Settings__c.getOrgDefaults();
        
        if (!leaseNatureCreated){
            
            CC_Nature__c defaultNature = TestUtilityClass.createNature(true, 'Nature_Default', 'Description1');
            insert defaultNature;
    
            settings.Default_Lease_Nature_Product_ID__c = defaultNature.Id;
            upsert settings;
            
            leaseNatureCreated = true;
        }
        
        CC_Short_Term_Lease__c stl = new CC_Short_Term_Lease__c();
        stl.Master_Lease__c = masterLeaseId;
        stl.Nature_Lookup__c = settings.Default_Lease_Nature_Product_ID__c;
        stl.Beginning_Date__c = Date.today();
        stl.Ending_Date__c = Date.today().addYears(2);
        stl.Shipping_Address__c = '123 Main St';
        stl.Shipping_City__c = 'Michigan City';
        stl.Shipping_State__c = 'MI';
        stl.Shipping_Country__c = 'US';
        stl.Shipping_Zip__c = '55555';
        stl.Del_Sched_Location__c = locationId;
        stl.Location__c = locationId; 
        stl.Lease_Category__c = 'Non-Revenue';
        stl.Lease_Type__c = '4FUN';
        stl.Sales_Rep__c = salesRepId;  
        stl.Invoice_Terms__c = 'NET 30';
        stl.Invoice_Frequency__c = 'Monthly';
        stl.Num_of_Payments__c = 24;
        stl.Delivery_Quoted_Rate__c = 500;                 
        return stl;
    }
    
    public static CC_Master_Lease__c createMasterLease(){
        
        CC_Master_Lease__c m = new CC_Master_Lease__c();
        
        return m;
    }    
    
    public static CC_STL_Car_Info__c createSTLCarInfo(Id productId){
        
        CC_STL_Car_Info__c carInfo = new CC_STL_Car_Info__c();
        carInfo.Item__c = productId;
        carInfo.Per_Car_Cost__c = 25;
        
        return carInfo;
    }

    public static CC_STL_Car_Accessory__c createSTLCarAccessory(Id productId, String accessoryType, Double laborCost){
        
        CC_STL_Car_Accessory__c accessory = new CC_STL_Car_Accessory__c();
        accessory.Product__c = productId;
        accessory.Accessory_Type__c = accessoryType;
        accessory.Labor_Cost__c = laborCost;
        
        return accessory;
    }    
    
    public static CC_STL_Car_Accessory_Line_Item__c createSTLCarAccessoryLineItem(Id carId, Id accessoryId, Double unitLaborCost, Double quantity){
        
        CC_STL_Car_Accessory_Line_Item__c lineItem = new CC_STL_Car_Accessory_Line_Item__c();
        lineItem.STL_Car__c = carId;
        lineItem.STL_Car_Accessory__c = accessoryId;
        lineItem.Unit_Labor_Cost__c = unitLaborCost;
        lineItem.Quantity__c = quantity;
        
        return lineItem;
    }
    */
public static void createStateCountries(){
  StatesCountries__c[] sc = new List<StatesCountries__c>();
  if(null==StatesCountries__c.getValues('Georgia')){
    StatesCountries__c sc1 = new StatesCountries__c();
    sc1.name     = 'Georgia';
    sc1.type__c  = 'S';
    sc1.code__c  = 'GA';
    sc1.country__c = 'USA';
    sc.add(sc1);
  }
  if(null==StatesCountries__c.getValues('USA')){
    StatesCountries__c sc2 = new StatesCountries__c();
    sc2.name     = 'USA';
    sc2.type__c  = 'C';
    sc2.code__c  = 'US';
    sc.add(sc2);
  }
  if(null==StatesCountries__c.getValues('India')){
    StatesCountries__c sc3 = new StatesCountries__c();
    sc3.name     = 'India';
    sc3.type__c  = 'C';
    sc3.code__c  = 'IN';
    sc.add(sc3);
  }
  if(!sc.isEmpty()){insert sc;}
}
    
    /*Commented as part of EMEIA Cleaup
     public CC_Trade_In__c createTradeIn(Id asgId, Id oppId){
        CC_Trade_In__c tradein   = new CC_Trade_In__c();
        tradein.Asset__c         = asgId;
        tradein.Opportunity__c   = oppId;
        return tradein;
        
    }
    
    public static void createPurchaseTypeToProductMap(){
        List<CC_Purchase_Type_to_Product_Map__c> maplist = new List<CC_Purchase_Type_to_Product_Map__c>();
        maplist.add(new CC_Purchase_Type_to_Product_Map__c(Name = 'Ele', Product_ERP_Item_Number__c='Oppty PE'));
        maplist.add(new CC_Purchase_Type_to_Product_Map__c(Name = 'Gas' ,Product_ERP_Item_Number__c='Oppty PG')); 
        insert maplist;
        
    }
    
     public static List<Club_Car_Product_to_Asset_Map__c> createproductToassetMapSetting(){
        List<Club_Car_Product_to_Asset_Map__c> productToAssetMap = new List<Club_Car_Product_to_Asset_Map__c>();
        productToAssetMap.add(new Club_Car_Product_to_Asset_Map__c(Name='103774902'));
        productToAssetMap.add(new Club_Car_Product_to_Asset_Map__c(Name='AM1240701'));
        insert productToAssetMap;
        return productToAssetMap;
        
        
    }
    
    public static CC_Product_Line__c createProductLine(){
        CC_Product_Line__c productline = new CC_Product_Line__c();
        productline.Name               = 'Test ProdcutLine';
        productline.Product_Line_Key__c  = 'PL'+ TestUtilityClass.randomWithLimit(8);
        return productline;
    }
    
    
    
    public static CC_Plan__c createCCPlan(String rectypeName, Id SalesrepId, Id accId, Id productLineId, String repNumber, String prodLineName, String custNum){
        Id planRectypeId = Schema.SObjectType.CC_Plan__c.getRecordTypeInfosByName().get(rectypeName).getRecordTypeId();
        String planKeyStr='';
        CC_Plan__c plan = new CC_Plan__c();
        plan.Pipeline_Revenue__c  = 100;
        plan.Pipeline_Units__c    = 10;
        if(rectypeName=='Objective'){
          plan.Channel_Partner__c   = accId;
          planKeyStr = custNum + '_';
        }
        plan.Sales_Rep__c         = SalesrepId;
        plan.RecordTypeId         = planRectypeId;
        plan.Year__c        = string.valueOf(date.today().year());
        plan.Month__c        = string.valueOf(date.today().month());
        plan.Product_Line_Lookup__c = productLineId;
        planKeyStr += repNumber + '_' + plan.Year__c + '_' + plan.Month__c + '_' + prodLineName;
        plan.Plan_Key__c=planKeyStr;
        
        return plan;
        
        
        
        
    }
    */
    public static EmailMessage createEmailMessages(Boolean doInsert, Id parentId, String fromAddress, String toAddress, String subject, String body, Boolean inbound){
    
        EmailMessage msg = new EmailMessage();
        
        msg.ParentId = parentId;
        msg.FromAddress = fromAddress;
        msg.ToAddress = toAddress;
        msg.Subject = subject;
        msg.TextBody = body;
        msg.Incoming = inbound;
        
        if (doInsert){
            insert msg;
        }
        
        return msg;
    }
    
    public static Account createAccount(Boolean doInsert, String name)
    {
        id recordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_TechDirect_Account').getRecordTypeId();
        return createAccount(doInsert, name, recordTypeId);
    }
    
    public static Account createAccount(Boolean doInsert, String name, id recordTypeId){
        Account a = new Account();
        a.Name = name;
        a.CTS_Global_Shipping_Address_1__c = 'test address';
        a.CTS_Global_Shipping_City__c = 'test city';
        a.Country__c = 'Barbados';
        a.recordTypeId = recordTypeId;
       // a.RecordTypeId = recordTypeId;
        if (doInsert){
            insert a;
        }
        return a;
    }

}