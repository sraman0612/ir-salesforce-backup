/**
 * @author           Amit Datta
 * @description      Class to calculate the Shipping Cost
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  28/03/2024          Original Version
 **/

global without sharing class B2BCartShippingCostCalculator implements sfdc_checkout.CartShippingCharges {
	public final String SHIPPING_PROVIDER = 'Robuschi Fixed Shipping Cost';
	public final String SHIPPING_PROVIDER_NAME = 'Robuschi Shipping';
	public final String SHIPPING_CHARGE_PRODUCT2_NAME = 'Robuschi B2B Shipping Charge';
	public final String DEFAULT_DELIVERY_METHOD_NAME = 'Robuschi Order Delivery Method';

	global sfdc_checkout.IntegrationStatus startCartProcessAsync(sfdc_checkout.IntegrationInfo jobInfo, Id cartId) {
		sfdc_checkout.IntegrationStatus integStatus = new sfdc_checkout.IntegrationStatus();
		try {
			CartDeliveryGroup cartDeliveryGroup = [SELECT Id, DeliveryMethodId FROM CartDeliveryGroup WHERE CartId = :cartId WITH SECURITY_ENFORCED][0];

			List<cartItem> cartItems = [SELECT Id, Quantity, TotalAmount FROM cartItem WHERE CartId = :cartId AND Type = 'Product' WITH SECURITY_ENFORCED];
			// Get shipping cost
			Decimal shippingCost = getShippingCostPercentage(cartId);
			Decimal totalShippingCost = 0;
			for (CartItem cartItem : cartItems) {
				totalShippingCost = totalShippingCost + cartItem.TotalAmount * shippingCost;
			}
			totalShippingCost = totalShippingCost.round(System.RoundingMode.CEILING);
			//system.debug('totalShippingCost>>' + totalShippingCost);
			delete [SELECT Id FROM CartDeliveryGroupMethod WHERE CartDeliveryGroupId = :cartDeliveryGroup.Id WITH SECURITY_ENFORCED];

			List<Id> orderDeliveryMethodIds = getOrderDeliveryMethods();
			for (Id orderDeliveryMethodId : orderDeliveryMethodIds) {
				populateCartDeliveryGroupMethodWithShippingOptions(totalShippingCost, cartDeliveryGroup.Id, orderDeliveryMethodId, cartId);
			}

			integStatus.status = sfdc_checkout.IntegrationStatus.Status.SUCCESS;
		} catch (DmlException de) {
			// Catch any exceptions thrown when trying to insert the shipping charge to the CartItems
			Integer numErrors = de.getNumDml();
			String errorMessage = 'There were ' + numErrors + ' errors when trying to insert the charge in the CartItem: ';
			for (Integer errorIdx = 0; errorIdx < numErrors; errorIdx++) {
				errorMessage += 'Field Names = ' + de.getDmlFieldNames(errorIdx);
				errorMessage += 'Message = ' + de.getDmlMessage(errorIdx);
				errorMessage += ' , ';
			}

			return integrationStatusFailedWithCartValidationOutputError(integStatus, errorMessage, jobInfo, cartId);
		} catch (Exception e) {
			System.debug('exception:' + e.getStackTraceString() + e.getTypeName() + '>>>' + e.getMessage());
			return integrationStatusFailedWithCartValidationOutputError(
				integStatus,
				'An exception of type ' + e.getTypeName() + ' has occurred: ' + e.getMessage(),
				jobInfo,
				cartId
			);
		}
		return integStatus;
	}

	public static Decimal getShippingCostPercentage(Id cartId) {
		Id accountId = [SELECT AccountId FROM WebCart WHERE Id = :cartId LIMIT 1].AccountId;
		List<BuyerGroupMember> BuyerGroupDetails = [
			SELECT Id, BuyerGroupId, BuyerGroup.Shipping_Charge__c
			FROM BuyerGroupMember
			WHERE BuyerId = :accountId
			LIMIT 1
		];
		if (!BuyerGroupDetails.isEmpty() && BuyerGroupDetails[0]?.BuyerGroup?.Shipping_Charge__c != null) {
			return BuyerGroupDetails[0].BuyerGroup.Shipping_Charge__c / 100;
		} else {
			return 0;
		}
	}

	private List<Id> getOrderDeliveryMethods() {
		String defaultDeliveryMethodName = DEFAULT_DELIVERY_METHOD_NAME;
		Id product2IdForThisDeliveryMethod = getDefaultShippingChargeProduct2Id();

		// Check to see if a default OrderDeliveryMethod already exists.
		// If it doesn't exist, create one.
		List<Id> orderDeliveryMethodIds = new List<Id>();
		List<OrderDeliveryMethod> orderDeliveryMethods = new List<OrderDeliveryMethod>();
		//Integer i = 1;
		//String shippingOptionNumber = String.valueOf(i);
		//String name = defaultDeliveryMethodName;
		List<OrderDeliveryMethod> odms = [
			SELECT Id, ProductId, Carrier, ClassOfService
			FROM OrderDeliveryMethod
			WHERE Name = :defaultDeliveryMethodName
			WITH SECURITY_ENFORCED
		];
		// This is the case in which an Order Delivery method does not exist.
		if (odms.isEmpty()) {
			OrderDeliveryMethod defaultOrderDeliveryMethod = new OrderDeliveryMethod(
				Name = defaultDeliveryMethodName,
				Carrier = SHIPPING_PROVIDER,
				isActive = true,
				ProductId = product2IdForThisDeliveryMethod,
				ClassOfService = SHIPPING_PROVIDER
			);
			insert (defaultOrderDeliveryMethod);
			orderDeliveryMethodIds.add(defaultOrderDeliveryMethod.Id);
		} else {
			OrderDeliveryMethod existingodm = odms[0];
			if (existingodm.Carrier == null || existingodm.ClassOfService == null) {
				existingodm.ProductId = product2IdForThisDeliveryMethod;
				existingodm.Carrier = SHIPPING_PROVIDER;
				existingodm.ClassOfService = SHIPPING_PROVIDER;
				update (existingodm);
			}
			orderDeliveryMethodIds.add(existingodm.Id);
		}
		return orderDeliveryMethodIds;
	}

	// Create a CartDeliveryGroupMethod record for every shipping option returned from the external service
	private void populateCartDeliveryGroupMethodWithShippingOptions(Decimal cost, Id cartDeliveryGroupId, Id deliveryMethodId, Id webCartId) {
		CartDeliveryGroupMethod cartDeliveryGroupMethod = new CartDeliveryGroupMethod(
			CartDeliveryGroupId = cartDeliveryGroupId,
			DeliveryMethodId = deliveryMethodId,
			ExternalProvider = SHIPPING_PROVIDER,
			Name = SHIPPING_PROVIDER_NAME,
			ShippingFee = cost,
			CurrencyIsoCode = 'EUR',
			WebCartId = webCartId
		);
		insert (cartDeliveryGroupMethod);
	}

	private sfdc_checkout.IntegrationStatus integrationStatusFailedWithCartValidationOutputError(
		sfdc_checkout.IntegrationStatus integrationStatus,
		String errorMessage,
		sfdc_checkout.IntegrationInfo jobInfo,
		Id cartId
	) {
		integrationStatus.status = sfdc_checkout.IntegrationStatus.Status.FAILED;
		CartValidationOutput cartValidationError = new CartValidationOutput(
			BackgroundOperationId = jobInfo.jobId,
			CartId = cartId,
			Level = 'Error',
			Message = errorMessage.left(255),
			Name = (String) cartId + ':' + jobInfo.jobId,
			RelatedEntityId = cartId,
			Type = 'Shipping'
		);
		insert (cartValidationError);
		return integrationStatus;
	}

	private Id getDefaultShippingChargeProduct2Id() {
		String shippingChargeProduct2Name = SHIPPING_CHARGE_PRODUCT2_NAME;
		List<Product2> shippingChargeProducts = [SELECT Id FROM Product2 WHERE Name = :shippingChargeProduct2Name WITH SECURITY_ENFORCED];
		if (shippingChargeProducts.isEmpty()) {
			Product2 shippingChargeProduct = new Product2(isActive = true, Name = shippingChargeProduct2Name);
			insert (shippingChargeProduct);
			return shippingChargeProduct.Id;
		} else {
			return shippingChargeProducts[0].Id;
		}
	}
}