/*
* Revisions:
07-Jan-2025: #03378819 Code optimization for SOQL 101 error.
*/
public with sharing class Case_TriggerHandlerHelper {
    
    Final Static String STATUS_CLOSED = 'Closed';
    Final Static String INTERNAL_AI_RT = 'Internal_Case';
    Final Static String EXTERNAL_AI_RT = 'Action_Item';
    
    // Ensure that any contact assigned via E2C is of the applicable contact record types
    public static void assignContacts(Case[] cases, Set<Id> caseRecordTypeIds, Set<Id> contactRecordTypeIds, String contactMatchStatusField){
    
    	Set<Id> contactIdsToCheck = new Set<Id>();
    	Set<String> emailsToLookup = new Set<String>();
    	Case[] casesWithInitialContacts = new Case[]{};
    	Case[] casesWithNoContactFound = new Case[]{};
    	system.debug('*****contactRecordTypeIds****'+contactRecordTypeIds);
    	for (Case c : cases){
    	
    		if (caseRecordTypeIds.contains(c.RecordTypeId) && String.isNotBlank(c.SuppliedEmail)){
    			
    			if (c.ContactId != null){
                    
    				contactIdsToCheck.add(c.ContactId);
    				casesWithInitialContacts.add(c);
                   
    			}
    			else{
    				emailsToLookup.add(c.SuppliedEmail);
    				casesWithNoContactFound.add(c);
                   
    			}
    		}
    	}
    	
    	system.debug('contactIdsToCheck: ' + contactIdsToCheck);
    	system.debug('emailsToLookup: ' + emailsToLookup);
    	
    	if (!contactIdsToCheck.isEmpty() || !emailsToLookup.isEmpty()){
    	
    		if (!contactIdsToCheck.isEmpty()){
    		
    			// Verify the contacts assigned via E2C are of the correct contact record types
    			Map<Id, Contact> contactMap = new Map<Id, Contact>([Select Id, Name, RecordTypeId From Contact Where Id in :contactIdsToCheck and RecordTypeId in :contactRecordTypeIds]);
    			
    			for (Case c : casesWithInitialContacts){
    			
    				if (!contactMap.containsKey(c.ContactId)){
    					
    					system.debug('assigned contact is not of the correct record type, clearing the case contact');
    					c.ContactId = null; // Clear out the invalid contact
    					c.put(contactMatchStatusField, 'No Matches Found');
    				}
    				else{
    					c.put(contactMatchStatusField, 'Single Match Found');
    				}
    			}
    		}
    		
    		if (!emailsToLookup.isEmpty()){
    		
    			// Try to find contacts with matching emails with contact record types
    			Map<String, List<Id>> contactEmailMap = new Map<String, List<Id>>();	
    			
    			for (Contact c : [Select Id, Name, Email, RecordTypeId From Contact Where Email in :emailsToLookup and RecordTypeId in :contactRecordTypeIds Order By LastModifiedDate DESC, LastName ASC]){
    			
    				if (!contactEmailMap.containsKey(c.Email)){
    					contactEmailMap.put(c.Email, new List<Id>{c.Id});    					
    				}
    				else{
    					contactEmailMap.get(c.Email).add(c.Id);
    				}
    			}   		
    		    		
    			for (Case c : casesWithNoContactFound){
    			
    				List<Id> contactIds = contactEmailMap.get(c.SuppliedEmail);
    				
    				system.debug('contactIds matched: ' + contactIds);
    				
    				if (contactIds != null && contactIds.size() == 1){
    					c.ContactId = contactIds[0];
    					c.put(contactMatchStatusField, 'Single Match Found');
    				}
    				else if (contactIds != null && contactIds.size() > 1){
    					c.ContactId = contactIds[0];
    					c.put(contactMatchStatusField, 'Duplicate Matches Found');
    				}
    				else{
    					c.put(contactMatchStatusField, 'No Matches Found');
    				}
    			}
    		}
    	}
    } 
    /*-------------------------------------------------------------------------------------------------------------------------------------------------
      Author : Mansi Agnihotri--Cognizant AMS team
      Date : 05/12/2024
      Descriptionn : This method updates the action item rollup sumary field with correct value when the action item is deleted
	  Revisions: 07-Jan-2025: #03378819 Code optimization for SOQL 101 error.
     --------------------------------------------------------------------------------------------------------------------------------------------------*/    
    public static void updateActionItemRollupSummary(map<Id,Case>oldmap, map<Id,Case>newmap){
       	Set<Id> setCaseIds = new Set<Id>();
        Integer count = 0;
        for(case caseVar:newmap.values()){
            if(oldmap.get(caseVar.Id).status != newmap.get(caseVar.Id).status && newmap.get(caseVar.Id).status == STATUS_CLOSED){
                setCaseIds.add(caseVar.Id);
            }
        }
        
        if(!setCaseIds.isEmpty()){
            map<Id,List<case>> childcases = new map<Id,List<case>>();
            List<Case> casesList = [Select Id,Action_Item_Status_Rollup__c,
                                    (Select Id from cases where status != :STATUS_CLOSED AND parentId IN:setCaseIds AND (recordtype.developername=:INTERNAL_AI_RT OR recordtype.developername=:EXTERNAL_AI_RT))
                                    FROM Case where Id IN:setCaseIds];
            
            for(Case ca:casesList){
                childcases.put(ca.Id, ca.cases);
            }
            
            try{
                for(case caseVar: casesList){
                    newmap.get(caseVar.id).Action_Item_Status_Rollup__c = childcases.get(caseVar.Id).size();
                }
            }
            Catch(Exception ex){
                system.debug('Error occurred:'+ex.getMessage());
            }
        }
     }

}