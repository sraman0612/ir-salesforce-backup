public with sharing class PT_DSMP_Constant {
/**************************************************************************************
-- - Author        : Spoon Consulting
-- - Description   : Class to group common methods/constants
--
-- Maintenance History: 
--
-- Date         Name  Version  Remarks 
-- -----------  ----  -------  -------------------------------------------------------
-- 10-DEC-2018  MGR    1.0     Initial version
--------------------------------------------------------------------------------------
**************************************************************************************/  
	
 	/***********************************************************************
			* Profile constants : Section for Profile Name / ID
	************************************************************************/
    public static String getProfileIdAdmin(){
        return [Select Id From Profile 
	            Where name = 'Administrateur système' 
	                OR name = 'System Administrator'
	                OR name = 'Amministratore del sistema'
	                OR name = 'Systemadministrator'
	                OR name = 'Systemadministratör'
	                OR name = 'Administrador do sistema'
	                OR name = 'Systeembeheerder'
	                OR name = 'Systemadministrator'].Id;
    }



    public static map<Id, set<Id>> getParentChildRoles() {
        map<Id, set<Id>> parentAndChildren = new map<Id, set<Id>>();
        set<Id> children;
        
        for(UserRole ur : [select Id, ParentRoleId from UserRole]) {
            children = parentAndChildren.containsKey(ur.ParentRoleId) ? parentAndChildren.get(ur.ParentRoleId) : new set<Id>();
            children.add(ur.Id);
            parentAndChildren.put(ur.ParentRoleId, children);
        }

        return parentAndChildren;
    }


    public static set<Id> getSubordinateRoles(Id roleId) {
        map<Id, set<Id>> parentAndChildren = new map<Id, set<Id>>();
        parentAndChildren = getParentChildRoles();
        
        if(parentAndChildren.size() > 0)
            return getSubordinateRoles(roleId, parentAndChildren);
        else
            return new set<id>{};
    }
    
    public static set<Id> getSubordinateRoles(Id roleId, map<Id, set<Id>> parentAndChildren) {
        set<Id> subordinateRoles = new set<Id>();
        set<Id> remainingSubordinateRoles = new set<Id>();

        if(parentAndChildren.containsKey(roleId)) {
            subordinateRoles.addAll(parentAndChildren.get(roleId));

            for(Id subRoleId : subordinateRoles) {
                remainingSubordinateRoles.addAll(getSubordinateRoles(subRoleId, parentAndChildren));
            }
        }

        subordinateRoles.addAll(remainingSubordinateRoles);
        return subordinateRoles;
    }


    public static set<Id> getParentRoles(Id roleId) {
        map<Id, set<Id>> parentAndChildren = new map<Id, set<Id>>();
        parentAndChildren = getParentChildRoles();

        System.debug('mgr getParentRoles(roleId, parentAndChildren) ' + getParentRoles(roleId, parentAndChildren));

        return getParentRoles(roleId, parentAndChildren);
    }
    

    public static set<Id> getParentRoles(Id roleId, map<Id, set<Id>> parentAndChildren ) {
        set<Id> parentRoles = new set<Id>();
        set<Id> remainingParentRoles = new set<Id>();

        for(Id current : parentAndChildren.keyset()){    

            if(parentAndChildren.get(current).contains(roleId)) {
                parentRoles.add(current);

                for(Id subRoleId : parentRoles) {
                    remainingParentRoles.addAll(getParentRoles(subRoleId, parentAndChildren));
                }
            }

        }
        
        parentRoles.addAll(remainingParentRoles);
        
        return parentRoles;
    }

}