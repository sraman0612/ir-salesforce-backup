/**
 * @author           Amit Datta
 * @description      Test Class for RecommendedProducts.
 *
 * Modification Log
 * ------------------------------------------------------------------------------------------
 *         Developer                   Date                Description
 * ------------------------------------------------------------------------------------------
 *         Amit Datta                  14/01/2024          Original Version
 **/

@isTest
public with sharing class B2BRecommendedProductsTest {
	@testSetup
	static void setup() {
		UserRole r1 = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_Administrators' LIMIT 1];
		User usr = CTS_TestUtility.createUser(false);
		usr.UserRoleId = r1.Id;
		insert usr;
		System.runAs(usr) {
			Account account = CTS_TestUtility.createAccount('TestAccount', false);
			account.OwnerId = usr.Id;
			insert account;
			Contact con = CTS_TestUtility.createContact('lastnameContact', 'firstnameContact', 'test@test.com', account.Id, true);
			WebStore webStore = new WebStore(Name = 'TestWebStore');
			insert webStore;
			List<Product2> prdList = new List<Product2>();
			Product2 pr1 = new Product2(Name = 'prd1');
			prdList.add(pr1);
			Product2 pr2 = new Product2(Name = 'prd2');
			prdList.add(pr2);
			insert prdList;
			Cross_Sell_Recommendations__c csr = new Cross_Sell_Recommendations__c(
				Recommended_ProductId__c = prdList[0].Id,
				Recommended_Product_ParentId__c = prdList[1].Id
			);
			insert csr;
		}
	}
	@isTest
	static void getCSRecommendationProductsByProductIdTest() {
		String parentProductId = [SELECT Recommended_Product_ParentId__c FROM Cross_Sell_Recommendations__c LIMIT 1].Recommended_Product_ParentId__c;
		String contactId = [SELECT Id FROM Contact LIMIT 1].Id;
		String profileId = [SELECT Id FROM Profile WHERE name = 'B2B Robuschi Profile' LIMIT 1].Id;
		user usr = CTS_TestUtility.createUser(contactId, profileId, true);
		String communityId = [SELECT ID FROM Network WHERE Name = 'Robuschi B2B Store' LIMIT 1].Id;
		System.runAs(usr) {
			Test.startTest();
			List<B2BRecommendedProducts.CartItemWrapper> ciwList = B2BRecommendedProducts.getCSRecommendationProductsByProductId('', '');
			Assert.isNull(ciwList);
			ciwList = B2BRecommendedProducts.getCSRecommendationProductsByProductId(parentProductId, communityId);
			Assert.areEqual(1, ciwList.size());
			Test.stopTest();
		}

	}
}