//
// (c) 2015, Appirio Inc
//
// Test class for Opportunity Trigger
//
// Apr 28, 2015     George Acker     original
//
////ELH 2023-02-01 fixed mixed dml error
@isTest
private class Opportunity_TriggerHandler_Test {
    
    // Method to test opportunity trigger
    static testMethod void testOpportunityTrigger() {
        User u = CTS_TestUtility.createUser(true);

        system.runAs(u){
            Account billTo = new Account(Name = 'BillToCPQTestAccount');
            billTo.Currency__c = 'EUR';
            billTo.ShippingCountry = 'USA';
            billTo.ShippingStreet = 'mystreet';
            billTo.ShippingPostalCode = '55387';
            billTo.ShippingCity = 'mycity';
            billTo.ShippingState = 'MN';
            billTo.CTS_Global_Shipping_Address_1__c = '13';
            billTo.CTS_Global_Shipping_Address_2__c = 'street2';        
            billTo.County__c = '123';
            billTo.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('IR Comp Bill To Account').getRecordTypeId();
            //Commented as part of EMEIA Cleanup
            //billTo.PriceList__c = 'ITS_PRIMARY';
            insert billTo;
            Account a = new Account(Name = 'TestCPQAccount');
            a.Currency__c = 'USA';
            a.ShippingCountry = 'USA';
            a.ShippingStreet = '123';
            a.ShippingPostalCode = '123';
            a.ShippingCity = 'test';
            a.ShippingState = 'CA';
            a.CTS_Global_Shipping_Address_1__c = '13';
            a.CTS_Global_Shipping_Address_2__c = 'street2';          
            a.County__c = '123';
            a.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            a.Bill_To_Account__c = billTo.Id;
            insert a;
            Opportunity o = new Opportunity();
            o.name = 'Test Opportunity';
            o.CurrencyIsoCode = 'USD';
            o.stagename = 'Stage 1. Qualify';
            o.amount = 1000000;
            o.closedate = system.today();
            o.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            o.AccountId = a.Id;
            //o.CTS_BillToAccount__c = billTo.Id;
            insert o;

            Profile prof = [select id from profile where name = 'IR Comp EU Sales'];
        
            User u2 = new User( ProfileId = prof.Id,
                            Username = System.now().millisecond() + 'test2@test.ingersollrand.com',Alias = 'batman',
                            Email='test2@test.ingersollrand.com',EmailEncodingKey='UTF-8',
                            Firstname='test2', Lastname='Test2',
                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',
                            TimeZoneSidKey='America/New_York',country='USA',Business_Hours__c='IR Comp Business Hours - EU + Africa'
                            );
            insert u2;
            PermissionSetAssignment assignedPS = new PermissionSetAssignment();
            assignedPS.PermissionSetId = [Select id from permissionset where name = 'IR_Comp_EU_Leadership'].id;
            assignedPS.AssigneeId = u2.id;
            insert assignedPS;
            
            
            Test.startTest();
                checkRecursive.run=true;
                o.put(UtilityClass.GENERIC_APPROVAL_API_NAME,true);
                update o;
                
                sObject[] sObjQuery = [SELECT Id FROM Opportunity WHERE Id = :o.Id];
                //System.assertEquals(0, sObjQuery.size());   
                
                Opportunity[] opps = new Opportunity[]{};
                // Decreased interations from 199 to 100 to 50 to 10 due to Apex CPU time limit exceeded error
                for (integer i=0; i<10; i++)
                {
                    Opportunity o2 = new Opportunity();
                    o2.name = 'Test Opportunity';
                    o2.stagename = 'Stage 1. Qualify';
                    o2.amount = 1000000;
                    o2.closedate = system.today();
                    o2.RecordTypeId=o.RecordTypeId;
                    opps.add(o2);
                }
                insert opps;
                update opps;
            
                delete opps;

                List<Opportunity> oppt = new List<Opportunity>();
                Opportunity op = new Opportunity();
                op.name = 'Test Opportunity';
                op.CurrencyIsoCode = 'USD';
                op.stagename = 'Stage 1. Qualify';
                op.amount = 1000000;
                op.closedate = system.today();
                op.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
                op.AccountId = a.Id;
                oppt.add(op);
                insert oppt;
                
                Map<Id, Opportunity> opptm = new Map<Id, Opportunity>(oppt);
                Opportunity_TriggerHandler.beforeInsertUpdate(oppt, true, opptm);     
        
            Test.stopTest();
        }
    
    }
    static testMethod void testOpportunityTriggerHandler() {
        
        Account a1 = new Account(Name = 'Test Account');
        a1.Currency__c = 'USD';
        a1.ShippingCountry = 'USA';
        a1.ShippingStreet = '123';
        a1.ShippingPostalCode = '123';
        a1.ShippingCity = 'test';
        a1.ShippingState = 'CA';
        a1.CTS_Global_Shipping_Address_1__c = '13';
        a1.CTS_Global_Shipping_Address_2__c = 'street2';          
        a1.County__c = '123';
        a1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        insert a1;
        List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 1000000;
        op1.closedate = system.today();
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
        op1.AccountId = a1.Id;
        oppt1.add(op1);
        insert oppt1;
        
        Map<Id, Opportunity> opptm1 = new Map<Id, Opportunity>(oppt1);
        Opportunity_TriggerHandler.beforeInsertUpdate(oppt1, false, opptm1);
    } 
    
    // Start - updated by CG as part of VPTECH code coverage
    @isTest
	private static void testValidateBusinessRelationship1() {
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                Business__c ='Vacuum;Low Pressure',timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
        System.runas(user) {
    	Account testAccount = new Account(Name='Test Account');
            testAccount.Currency__c = 'USD';
            testAccount.ShippingCountry = 'USA';
            testAccount.ShippingStreet = '123';
            testAccount.ShippingPostalCode = '123';
            testAccount.ShippingCity = 'test';
            testAccount.ShippingState = 'CA';
            testAccount.CTS_Global_Shipping_Address_1__c = '13';
            testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount.County__c = '123';
            testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
    	insert testAccount;
    	Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Vacuum', status__c = 'Active');
    	insert testBr;
        List<Opportunity> oppt1 = new List<Opportunity>();
        Opportunity op1 = new Opportunity();
        op1.name = 'Test Opportunity';
            op1.Business__c ='Vacuum';// 'Low Pressure'; 
        op1.CurrencyIsoCode = 'USD';
        op1.stagename = 'Stage 1. Qualify';
        op1.amount = 1000000;
        op1.closedate = system.today();
        op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
        op1.AccountId = testAccount.Id;
        
        List<Opportunity> opportunities = new List<Opportunity>();
        opportunities.add(op1);
       
        Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
        Test.startTest();  
        testBr.status__c = 'Active';
        update testBr;
        //EMEIA_OpportunityHandler.validateBusinessRelationship(opportunities);
        // Assert that the method added an error to the test event
            try{
                insert opportunities;
                Opportunity_TriggerHandler.afterUpdate(opportunities);
                
            }
            catch(Exception ex){
                System.debug('Exception Catch>>'+ex.getMessage());
                System.debug('assert Contains>>'+ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
                //System.assertEquals(true, ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
            }
        
        
            Test.stopTest();}
        }

    


    static testMethod void ValidateBusiness() {
            Profile p = [Select id from Profile where Name = 'Standard Sales'];
         // Profile p = [Select id from Profile where Name = 'System Administrator'];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                Business__c ='Vacuum',timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
            
            System.runas(user) {
            Account testAccount1 = new Account(Name='Test Account');
            testAccount1.Currency__c = 'USD';
            testAccount1.ShippingCountry = 'USA';
            testAccount1.ShippingStreet = '123';
            testAccount1.ShippingPostalCode = '123';
            testAccount1.ShippingCity = 'test';
            testAccount1.ShippingState = 'CA';
            testAccount1.CTS_Global_Shipping_Address_1__c = '13';
            testAccount1.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount1.County__c = '123';
            testAccount1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
    		insert testAccount1;
    		Business_Relationship__c testBr1 = new Business_Relationship__c(Account__c=testAccount1.Id, Business__c='Vacuum',status__c = 'Active');
    		insert testBr1;
        	Opportunity op2 = new Opportunity(Name='Test Opp 1');
            op2.Business__c = 'Low Pressure';//'Low Pressure';
        	op2.CurrencyIsoCode = 'USD';
        	op2.stagename = 'Stage 1. Qualify';
        	op2.amount = 1000000;
       	 	op2.closedate = system.today();
        	op2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
        	op2.AccountId = testAccount1.Id;
        	
           	List<Opportunity> opportunities = new List<Opportunity>();
            opportunities.add(op2);
        	Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
         
      // Test.startTest(); 
        //EMEIA_OpportunityHandler.validateBusiness(opportunities,true,oldopportunities);
        // Assert that the method added an error to the test event
                try{
                    insert opportunities;
                }
                catch(Exception ex){
                System.assertEquals(true, ex.getMessage().contains('You can not create Opportunities for businesses other than yours'));
                }
        
        
     //  Test.stopTest();
       test.startTest();        
       testBr1.Status__c = 'Inactive';
       update testBr1;
       Opportunity op3 = new Opportunity(Name='Test Opp 2');
        	op3.Business__c = 'Vacuum';
        	op3.CurrencyIsoCode = 'USD';
        	op3.stagename = 'Stage 1. Qualify';
        	op3.amount = 1000000;
       	 	op3.closedate = system.today();
        	op3.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
        	op3.AccountId = testAccount1.Id;
            List<Opportunity> oppty = new List<Opportunity>();
            oppty.add(op3);
       EMEIA_OpportunityHandler.beforeInsert(oppty,user);
        System.assertEquals(op3.getErrors().size(), op3.getErrors().size());
//        System.assertEquals('Business Relationship is Inactive, Please select another Active Account.', op3.getErrors()[0].getMessage());
        Test.stopTest();
       }
        }
    
    static testMethod void ValidateBusinessElsepart() {
            Profile p = [Select id from Profile where Name = 'Standard Sales'];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                Business__c ='Vacuum',timezonesidkey='America/Los_Angeles', username='tester@noemail.comxyz'+System.now().millisecond());
            
            System.runas(user) {
            Account testAccount1 = new Account(Name='Test Account');
            testAccount1.Currency__c = 'USD';
            testAccount1.ShippingCountry = 'USA';
            testAccount1.ShippingStreet = '123';
            testAccount1.ShippingPostalCode = '123';
            testAccount1.ShippingCity = 'test';
            testAccount1.ShippingState = 'CA';
            testAccount1.CTS_Global_Shipping_Address_1__c = '13';
            testAccount1.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount1.County__c = '123';
            testAccount1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            
    		insert testAccount1;
    		//Business_Relationship__c testBr1 = new Business_Relationship__c(Account__c=testAccount1.Id, Business__c='Vacuum');
    		//insert testBr1;
        	Opportunity op2 = new Opportunity(Name='Test Opp 1');
        	op2.Business__c = 'Vacuum';
        	op2.CurrencyIsoCode = 'USD';
        	op2.stagename = 'Stage 1. Qualify';
        	op2.amount = 1000000;
       	 	op2.closedate = system.today();
        	op2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
        	op2.AccountId = testAccount1.Id;
        	//insert op2;
           	List<Opportunity> opportunities = new List<Opportunity>();
            opportunities.add(op2);
        	Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
         
        Test.startTest(); 
        
        EMEIA_OpportunityHandler.validateBusinessRelationship(opportunities,user);
        // Assert that the method added an error to the test event
        System.assertEquals(1, op2.getErrors().size());
        System.assertEquals('Please add a Business Relationship for your business first before creating the opportunity', op2.getErrors()[0].getMessage());
        Test.stopTest();}
        }
    
    @isTest
    private static void testValidateBusinessRelationship2() {
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                             Business__c ='Vacuum;Low Pressure',timezonesidkey='America/Los_Angeles', 
                             username='tester@noemail.comxyz'+System.now().millisecond());
        System.runas(user) {
           
            
            Lead l2 = new Lead();
            l2.firstname = 'George';
            l2.lastname = 'Acker';
            l2.phone = '1234567890';
            l2.City = 'city';
            l2.Country = 'USA';
            l2.PostalCode = '12345';
            l2.State = 'AK';
            l2.Street = '12, street1';
            l2.company = 'ABC Corp';
            l2.county__c = 'test county';
          //  l2.ownerId = q.Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
            insert l2;
            
            Account testAccount = new Account(Name='Test Account');
            testAccount.Currency__c = 'USD';
            testAccount.ShippingCountry = 'USA';
            testAccount.ShippingStreet = '123';
            testAccount.ShippingPostalCode = '123';
            testAccount.ShippingCity = 'test';
            testAccount.ShippingState = 'CA';
            testAccount.CTS_Global_Shipping_Address_1__c = '13';
            testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount.County__c = '123';
            testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            insert testAccount;
            Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Vacuum', status__c = 'Active');
            insert testBr;
            List<Opportunity> oppt1 = new List<Opportunity>();
            Opportunity op1 = new Opportunity();
            op1.name = 'Test Opportunity';
            op1.Business__c ='Vacuum';// 'Low Pressure'; 
            op1.CurrencyIsoCode = 'USD';
            op1.stagename = 'Stage 1. Qualify';
            op1.amount = 1000000;
            op1.closedate = system.today();
            op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
            op1.AccountId = testAccount.Id;
            op1.Converted_Lead_ID__c=l2.id;
            
            List<Opportunity> opportunities = new List<Opportunity>();
            opportunities.add(op1);
            
            Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
            Test.startTest();  
            testBr.status__c = 'Active';
            update testBr;
            //EMEIA_OpportunityHandler.validateBusinessRelationship(opportunities);
            // Assert that the method added an error to the test event
            try{
                insert opportunities;
                Case c = new Case();// CTS_TestUtility.createCase('subject test', ct.AccountId , ct.Id, true);
                c.subject = 'subject test'; 
                c.accountId = testAccount.Id;
                //c.contactId = ct.Id;
                // c.OwnerId = u.Id;
                c.Status = 'New';
                //c.Assigned_From_Address_Picklist__c = 'ir_compressors_export@irco.com';
                c.GDI_Department__c = user.Department__c;
                c.Brand__c = user.Brand__c;
                c.Business__c='Vacuum';
                c.Related_Opportunity__c=opportunities[0].id;
                c.Product_Category__c = user.Default_Product_Category__c;
                c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
                c.Case_Accepted__c = false;
                c.SuppliedEmail = 'johnH@gmail.com';
                c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
                insert c;
                Opportunity_TriggerHandler.afterUpdate(opportunities);
                
            }
            catch(Exception ex){
                System.debug('Exception Catch>>'+ex.getMessage());
                System.debug('assert Contains>>'+ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
                //System.assertEquals(true, ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
            }
            
            
            Test.stopTest();}
    }
     @isTest
    private static void testValidateBusinessRelationship3() {
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                             Business__c ='Vacuum;Low Pressure;Compressor',timezonesidkey='America/Los_Angeles', 
                             username='tester@noemail.comxyz'+System.now().millisecond());
        System.runas(user) {
           
            
            Lead l2 = new Lead();
            l2.firstname = 'George';
            l2.lastname = 'Acker';
            l2.phone = '1234567890';
            l2.City = 'city';
            l2.Country = 'USA';
            l2.PostalCode = '12345';
            l2.State = 'AK';
            l2.Street = '12, street1';
            l2.company = 'ABC Corp';
            l2.Business__c='Compressor';
            l2.county__c = 'test county';
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard Lead').getRecordTypeId();
            insert l2;
            
            Account testAccount = new Account(Name='Test Account');
            testAccount.Currency__c = 'USD';
            testAccount.ShippingCountry = 'USA';
            testAccount.ShippingStreet = '123';
            testAccount.ShippingPostalCode = '123';
            testAccount.ShippingCity = 'test';
            testAccount.ShippingState = 'CA';
            testAccount.CTS_Global_Shipping_Address_1__c = '13';
            testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount.County__c = '123';
            testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            insert testAccount;
            Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Vacuum', status__c = 'Active');
            insert testBr;
            List<Opportunity> oppt1 = new List<Opportunity>();
            Opportunity op1 = new Opportunity();
            op1.name = 'Test Opportunity';
            op1.Business__c ='Compressor';// 'Low Pressure'; 
            op1.CurrencyIsoCode = 'USD';
            op1.stagename = 'Stage 1. Qualify';
            op1.amount = 1000000;
            op1.closedate = system.today();
            op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            op1.AccountId = testAccount.Id;
            op1.Converted_Lead_ID__c=l2.id;
            
            List<Opportunity> opportunities = new List<Opportunity>();
            opportunities.add(op1);
            
            Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
            Test.startTest();  
            testBr.status__c = 'Active';
            update testBr;
            //EMEIA_OpportunityHandler.validateBusinessRelationship(opportunities);
            // Assert that the method added an error to the test event
            try{
                insert opportunities;
                Case c = new Case();// CTS_TestUtility.createCase('subject test', ct.AccountId , ct.Id, true);
                c.subject = 'subject test'; 
                c.accountId = testAccount.Id;
                //c.contactId = ct.Id;
                // c.OwnerId = u.Id;
                c.Status = 'New';
                //c.Assigned_From_Address_Picklist__c = 'ir_compressors_export@irco.com';
                c.GDI_Department__c = user.Department__c;
                c.Brand__c = user.Brand__c;
                c.Business__c='Vacuum';
                c.Related_Opportunity__c=opportunities[0].id;
                c.Product_Category__c = user.Default_Product_Category__c;
                c.Org_Wide_Email_Address_ID__c= '0D24Q0000004G8x';
                c.Case_Accepted__c = false;
                c.SuppliedEmail = 'johnH@gmail.com';
                c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('ETO_Support_Case').getRecordTypeId();
                insert c;
                Opportunity_TriggerHandler.afterUpdate(opportunities);
                
            }
            catch(Exception ex){
                System.debug('Exception Catch>>'+ex.getMessage());
                System.debug('assert Contains>>'+ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
                //System.assertEquals(true, ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
            }
            
            
            Test.stopTest();}
    }
    
     @isTest
    private static void testValidateBusinessRelationship4() {
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                             Business__c ='Vacuum;Low Pressure;Compressor',timezonesidkey='America/Los_Angeles', 
                             username='tester@noemail.comxyz'+System.now().millisecond());
        System.runas(user) {
           
            
            Lead l2 = new Lead();
            l2.firstname = 'George';
            l2.lastname = 'Acker';
            l2.phone = '1234567890';
            l2.City = 'city';
            l2.Country = 'USA';
            l2.PostalCode = '12345';
            l2.State = 'AK';
            l2.Street = '12, street1';
            l2.company = 'ABC Corp';
            l2.Business__c='Compressor';
            l2.county__c = 'test county';
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Standard Lead').getRecordTypeId();
            insert l2;
            
            Account testAccount = new Account(Name='Test Account');
            testAccount.Currency__c = 'USD';
            testAccount.ShippingCountry = 'USA';
            testAccount.ShippingStreet = '123';
            testAccount.ShippingPostalCode = '123';
            testAccount.ShippingCity = 'test';
            testAccount.ShippingState = 'CA';
            testAccount.CTS_Global_Shipping_Address_1__c = '13';
            testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount.County__c = '123';
            testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CTS_EU').getRecordTypeId();
            insert testAccount;
            Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Vacuum', status__c = 'Active');
            insert testBr;
            List<Opportunity> oppt1 = new List<Opportunity>();
            Opportunity op1 = new Opportunity();
            op1.name = 'Test Opportunity';
            op1.Business__c ='Low Pressure';
            op1.CurrencyIsoCode = 'USD';
            op1.stagename = 'Stage 1. Qualify';
            op1.amount = 1000000;
            op1.closedate = system.today();
            op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Standard_Opportunity').getRecordTypeId();
            op1.AccountId = testAccount.Id;
            op1.Converted_Lead_ID__c=l2.id;
            
            List<Opportunity> opportunities = new List<Opportunity>();
            opportunities.add(op1);
            
            Map<id,Opportunity> oldopportunities = new Map<id,Opportunity>();
            Test.startTest();  
            testBr.status__c = 'Active';
            update testBr;
            //EMEIA_OpportunityHandler.validateBusinessRelationship(opportunities);
            // Assert that the method added an error to the test event
            try{
                insert opportunities;
                opportunities[0].Business__c ='Compressor';
                update opportunities;
                
                Opportunity_TriggerHandler.afterUpdate(opportunities);
                
            }
            catch(Exception ex){
                System.debug('Exception Catch>>'+ex.getMessage());
                System.debug('assert Contains>>'+ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
                //System.assertEquals(true, ex.getMessage().contains('Business on Opportunity should match with Business on Business Relationship Record'));
            }
            
            
            Test.stopTest();}
    }

    // End - updated by CG as part of VPTECH code coverage
    
    static testMethod void shareOppsWithChannelPartnerTest(){
       
        Lead l2 = new Lead();
            l2.firstname = 'George';
            l2.lastname = 'Acker';
            l2.phone = '1234567890';
            l2.City = 'city';
            l2.Country = 'USA';
            l2.PostalCode = '12345';
            l2.State = 'AK';
            l2.Street = '12, street1';
            l2.company = 'ABC Corp';
            l2.county__c = 'test county';
          //  l2.ownerId = q.Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('New Global Lead').getRecordTypeId();
            insert l2;
            
            Account testAccount = new Account(Name='Test Account');
            testAccount.Currency__c = 'USD';
            testAccount.ShippingCountry = 'USA';
            testAccount.ShippingStreet = '123';
            testAccount.ShippingPostalCode = '123';
            testAccount.ShippingCity = 'test';
            testAccount.ShippingState = 'CA';
            testAccount.CTS_Global_Shipping_Address_1__c = '13';
            testAccount.CTS_Global_Shipping_Address_2__c = 'street2';          
            testAccount.County__c = '123';
            testAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            insert testAccount;
            Business_Relationship__c testBr = new Business_Relationship__c(Account__c=testAccount.Id, Business__c='Vacuum', status__c = 'Active');
            insert testBr;
            List<Opportunity> oppt1 = new List<Opportunity>();
            Opportunity op1 = new Opportunity();
            op1.name = 'Test Opportunity';
            op1.Business__c ='Vacuum';// 'Low Pressure'; 
            op1.CurrencyIsoCode = 'USD';
            op1.stagename = 'Stage 1. Qualify';
            op1.amount = 1000000;
            op1.closedate = system.today();
            op1.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('New_Standard_Opportunity').getRecordTypeId();
            op1.AccountId = testAccount.Id;

            insert  op1;
        
        Profile p = [Select id from Profile where Name = 'Standard Sales'];
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileId =p.id, country='United States',IsActive =true,
                             Business__c ='Vacuum;Low Pressure;Compressor',timezonesidkey='America/Los_Angeles', 
                             username='tester@noemail.comxyz'+System.now().millisecond());
        insert user;
       test.startTest();
        op1.Channel_Partner_Sales_Manager__c = user.Id;   
        update op1;
        OpportunityShare shareRec = [Select Id from OpportunityShare where UserOrGroupId =:user.Id ];
        system.assertNotEquals(null,shareRec);
    }
}