/***
    @author Providity
    @date 23FEB19
    @description Test class for CTS article search related classes
*/

@IsTest
private class CTS_ArticleSearchTest {
	    
    static User setupData() {
        CTS_Article_Search_Config__c config = CTS_Article_Search_Config__c.getOrgDefaults();
        if(config.SetupOwnerId != UserInfo.getOrganizationId() && String.isBlank(config.Available_Options_In_The_Page_Size__c)) {
        	config = new CTS_Article_Search_Config__c();
            config.Show_Search_Filter_Options__c = true;
            config.Record_Type__c = 'IR Comp TechDirect Knowledge Article';
            config.Default_Page_Size__c = 10;
            config.Available_Options_In_The_Page_Size__c = '5,10,20';
            config.Recent_Days_Count__c = 5;
            config.Query_Limit__c = 100;
            insert config;   
        }        
        
        Id profileId = [Select Id From Profile Where Name = 'System Administrator' LIMIT 1].Id;
        Id roleId = [Select Id From UserRole Where Name = 'CTS TechDirect BU Global' LIMIT 1].Id;
        User u = TestUtilityClass.createNonPortalUser(profileId);
        
        System.runAs(new User(Id = UserInfo.getUserId())) {            
            u.Username += '1';
            u.FederationIdentifier = '1234560';
            u.DB_Region__c = 'EMEIA';
            u.CTS_TechDirect_Service_Sub_Region__c = 'EMEIA';
            u.UserRoleId = roleId;
            u.UserPermissionsKnowledgeUser = true;
            insert u;
        }
        return u;
    }
    
    @isTest()
    static void testSearch() {
        
        User u = CTS_ArticleSearchTest.setupData();

        Test.startTest();

        Knowledge__kav kb = new Knowledge__kav(title = 'Test1', urlName = 'Test1' ,CTS_TechDirect_Article_Type__c = 'Test', language='en_US');
        kb.RecordTypeId = Schema.SObjectType.knowledge__kav.getRecordTypeInfosByName().get('IR Comp TechDirect Knowledge Article').getRecordTypeId();
        insert kb;

        Id kbId = [Select KnowledgeArticleId From Knowledge__kav Where Id = :kb.Id].KnowledgeArticleId;

        KbManagement.PublishingService.publishArticle(kbId, false);
                
        CTS_ArticleSearchController.SearchConfigWrapper wrap = CTS_ArticleSearchController.getSearchConfigDetails();
        List<Object> filters = CTS_ArticleSearchController.getFilters();
        
        List<CTS_ArticleSearchController.ObjectWrapper> oWrap1 = CTS_ArticleSearchController.getArticlesOnLoad('LastModifiedDate');
        List<CTS_ArticleSearchController.ObjectWrapper> oWrap2 = CTS_ArticleSearchController.getArticlesOnLoad('mostHelpfulArticles');
        List<CTS_ArticleSearchController.ObjectWrapper> oWrap3 = CTS_ArticleSearchController.getArticlesOnLoad('EntitySubscription');
        
        List<CTS_ArticleSearchController.ObjectWrapper> resWrap =  CTS_ArticleSearchController.search('Test', 'Centac', 'Airend', 'en_US', 'Tech Note');
        
        CTS_ArticleSearchController.getKBWProdDataCats(kb.Id);
        String KNOWLEDGE_OBJECT = CTS_ArticleSearchConstants.KNOWLEDGE_OBJECT;

        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    static void testFiles() {

        Test.startTest();

        Knowledge__kav kb = new Knowledge__kav(title = 'Test1', urlName = 'Test1' , CTS_TechDirect_Article_Type__c = 'Test', language='en_US');
        kb.RecordTypeId = Schema.SObjectType.knowledge__kav.getRecordTypeInfosByName().get('IR Comp OM Knowledge Article').getRecordTypeId();
        insert kb;
        
        ContentVersion contentVersion = new ContentVersion(
          Title = 'Penguins',
          PathOnClient = 'Penguins.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE LatestPublishedVersionId = :contentVersion.Id];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = kb.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
                
        CTS_FileCardsController.getData(kb.Id);

        Test.stopTest();
    }
    
    @isTest(SeeAllData=true)
    static void testFooter() {

        Test.startTest();

        Knowledge__kav kb = new Knowledge__kav(title = 'Test1', urlName = 'Test1' , CTS_TechDirect_Article_Type__c = 'Test', language='en_US');
        kb.RecordTypeId = Schema.SObjectType.knowledge__kav.getRecordTypeInfosByName().get('IR Comp OM Knowledge Article').getRecordTypeId();
        insert kb;

        CTS_FooterController.getInit(kb.Id);
        
        //CTS_FooterController.followUnfollow(kb.Id, 'Follow');
       // CTS_FooterController.followUnfollow(kb.Id, 'Unfollow');
        
        CTS_FooterController.vote(kb.Id, 'Up');
        
        CTS_FooterController.submitArticleFeedback('Test', kb.Id);
    }
    
    @isTest(SeeAllData=false)
    static void testSubmitCase() {
        Id artId = createKnowledge();
        CTS_FooterController.submitArticleFeedback('Test', artId);
    }
    
    @isTest(SeeAllData=false)
    static void testSaveSearchKeyboard() {
        CTS_ArticleSearchController.saveSearchKeyword('Test', 1, 'Product', 
                                           'Product', 'Product', 'en_US', 'Article');
    }
    
    @isTest(SeeAllData=false)
    static void testUpdateViewStat() {
        
        Id artId = createKnowledge();
        CTS_ArticleSearchController.updateViewStat(artId);
    }
     @isTest
    public static void testConstants(){
        CTS_ArticleSearchConstants cons = new CTS_ArticleSearchConstants();
    }
    private static Id createKnowledge() {
        User u = CTS_ArticleSearchTest.setupData();
        
        Knowledge__kav akv=new Knowledge__kav();
        List<Knowledge__kav> kvlist=new List<Knowledge__kav>();
        akv.UrlName='testurl';
        akv.Title='Test kav';
        akv.CTS_TechDirect_Author_Reviewer__c = u.Id;
        
        kvlist.add(akv);
        
        System.runAs(new User(Id = u.Id)) {
        	insert kvlist;
        }
        
		Knowledge__kav obj1 = [SELECT Id,Title,KnowledgeArticleId FROM Knowledge__kav WHERE id =: kvlist[0].id];
        return obj1.Id;
    }
    @isTest
		public static void objectWrapperTest(){
		CTS_ArticleSearchController.ObjectWrapper wrapper= new CTS_ArticleSearchController.ObjectWrapper();
			wrapper.searchType = 'Knowledge__kav';
            wrapper.iconName = 'standard:knowledge';
	}

}