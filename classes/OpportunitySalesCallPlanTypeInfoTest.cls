@isTest
public class OpportunitySalesCallPlanTypeInfoTest {
    //Commented as part of EMEIA Cleanup
    // Account Record Types
    //public static final String RS_HVAC_RT_DEV_NAME = 'RS_HVAC';
    public static final String AirNAAcct_RT_DEV_NAME = 'CTS_EU';
    
    //Opportunity Sales Call Plan Record Types
   // public static final String IRSPX_RS_Sales_Call_Plan_RT_DEV_NAME = 'IRSPX_RS_Sales_Call_Plan';
    public static final String CTS_Sales_Call_Plan_RT_DEV_NAME = 'CTS_Sales_Call_Plan';
    
    @testSetup static void setup(){
        Opportunity_Sales_Call_Plan__c salesCallPlan;
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt.1', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
        insert u;
        
        System.runAs(u){
            
            Account airdTestAccount = IRSPXTestDataFactory.createAccount('TestAcc1',AirNAAcct_RT_DEV_NAME);
            airdTestAccount.ownerid=u.id;
            insert airdTestAccount;
            
            salesCallPlan = IRSPXTestDataFactory.createSalesCallPlan(airdTestAccount.id, CTS_Sales_Call_Plan_RT_DEV_NAME);
            salesCallPlan.OwnerId = u.id;
            insert salesCallPlan;
			
            
           /* Account rhvacTestAccount =IRSPXTestDataFactory.createAccount('TestAcc2',RS_HVAC_RT_DEV_NAME);
            rhvacTestAccount.ownerid=u.id;
            insert rhvacTestAccount;*/
            
           /* salesCallPlan = IRSPXTestDataFactory.createSalesCallPlan(rhvacTestAccount.id, IRSPX_RS_Sales_Call_Plan_RT_DEV_NAME);
            salesCallPlan.OwnerId = u.id;
            insert salesCallPlan;
            */
        }
    }
    
    @isTest static void opportunitySalesCallPlanTypeTest(){
        OpportunitySalesCallPlanTypeInfo opportunitySalesCallPlanType;
        Map<Id,Schema.RecordTypeInfo> recordTypeInfo;
        String developerName;
        Opportunity_Sales_Call_Plan__c scp;
        
        Test.starttest();
        User u=[Select id from user where Alias = 'standt.1'];
        
        System.runAs(u){
            scp = [select Id, Name, RecordTypeId from Opportunity_Sales_Call_Plan__c where account__r.name = 'TestAcc1'];    
            opportunitySalesCallPlanType = OpportunitySalesCallPlanTypeInfo.getInstance();  
            recordTypeInfo = opportunitySalesCallPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)scp.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            system.assertEquals(CTS_Sales_Call_Plan_RT_DEV_NAME, developerName);
            
                  Account rhvacTestAccount = IRSPXTestDataFactory.createAccount('TestAcc2',AirNAAcct_RT_DEV_NAME);
            rhvacTestAccount.ownerid=u.id;
            insert rhvacTestAccount;
             try{
            scp = [select Id, Name, RecordTypeId from Opportunity_Sales_Call_Plan__c where account__r.name = 'TestAcc2'];
            opportunitySalesCallPlanType = OpportunitySalesCallPlanTypeInfo.getInstance();  
            recordTypeInfo = opportunitySalesCallPlanType.recordTypeInfoById;
            developerName = recordTypeInfo.get((Id)scp.RecordTypeId).getDeveloperName();
    
            system.debug('developerName: ' + developerName);
            //system.assertEquals(IRSPX_RS_Sales_Call_Plan_RT_DEV_NAME, developerName);
        }
                  catch(exception e){
                
            }
        }
        Test.Stoptest();
        
    }
}