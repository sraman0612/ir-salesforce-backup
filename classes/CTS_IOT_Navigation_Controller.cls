public with sharing class CTS_IOT_Navigation_Controller {

    public class InitResponse extends LightningResponseBase{
        @AuraEnabled public CTS_IOT_Community_Administration__c communitySettings = CTS_IOT_Community_Administration__c.getOrgDefaults();
    }

    @AuraEnabled
	public static InitResponse getInit(){
		return new InitResponse();
	}   
}