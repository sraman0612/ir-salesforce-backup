/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioStudio {
    global TwilioStudio() {

    }
    global static void send(TwilioSF.TwilioStudio.StudioDetail studioDetail) {

    }
    @InvocableMethod(label='Twilio Send to Studio' description='Initiates a Twilio Studio Flow')
    global static void send(List<TwilioSF.TwilioStudio.StudioDetail> studioDetails) {

    }
global class StudioDetail {
    @InvocableVariable(label='From Number' description='The Twilio From Number.' required=true)
    global String fromNumber;
    @InvocableVariable(label='Parameter 10 Key' required=false)
    global String param10Key;
    @InvocableVariable(label='Parameter 10 Value' required=false)
    global String param10Value;
    @InvocableVariable(label='Parameter 1 Key' required=false)
    global String param1key;
    @InvocableVariable(label='Parameter 1 Value' required=false)
    global String param1Value;
    @InvocableVariable(label='Parameter 2 Key' required=false)
    global String param2Key;
    @InvocableVariable(label='Parameter 2 Value' required=false)
    global String param2Value;
    @InvocableVariable(label='Parameter 3 Key' required=false)
    global String param3Key;
    @InvocableVariable(label='Parameter 3 Value' required=false)
    global String param3Value;
    @InvocableVariable(label='Parameter 4 Key' required=false)
    global String param4Key;
    @InvocableVariable(label='Parameter 4 Value' required=false)
    global String param4Value;
    @InvocableVariable(label='Parameter 5 Key' required=false)
    global String param5Key;
    @InvocableVariable(label='Parameter 5 Value' required=false)
    global String param5Value;
    @InvocableVariable(label='Parameter 6 Key' required=false)
    global String param6Key;
    @InvocableVariable(label='Parameter 6 Value' required=false)
    global String param6Value;
    @InvocableVariable(label='Parameter 7 Key' required=false)
    global String param7Key;
    @InvocableVariable(label='Parameter 7 Value' required=false)
    global String param7Value;
    @InvocableVariable(label='Parameter 8 Key' required=false)
    global String param8Key;
    @InvocableVariable(label='Parameter 8 Value' required=false)
    global String param8Value;
    @InvocableVariable(label='Parameter 9 Key' required=false)
    global String param9Key;
    @InvocableVariable(label='Parameter 9 Value' required=false)
    global String param9Value;
    @InvocableVariable(label='Studio Url' description='The Twilio Studio url.' required=true)
    global String studioUrl;
    @InvocableVariable(label='To Mobile Number' description='The Phone Number which will the twilio flow will take action on.' required=true)
    global String toNumber;
    global StudioDetail() {

    }
}
}
