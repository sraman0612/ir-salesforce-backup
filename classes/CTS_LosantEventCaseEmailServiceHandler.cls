/**
    @author Ben Lorenz
    @date 1AUG2019
    @description Email Service Handler Class for LosantEvent
*/

global class CTS_LosantEventCaseEmailServiceHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {        
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String bodyStr = email.plainTextBody;
        
        String subject = email.subject;
        String [] bodyParts;
        String [] idParts;
        String relatedIdStr;
                
        bodyParts = subject.split('\\[ ref:');
        idParts = bodyParts.size() == 2 ? bodyParts[1].split('\\:ref ]') : null;
        relatedIdStr = idParts != null && idParts.size() > 0 ? idParts[0].trim() : null;
        
        if(relatedIdStr == null) { //If thread Id not found in the email body
            bodyParts = bodyStr.split('\\[ ref:');
        	idParts = bodyParts.size() == 2 ? bodyParts[1].split('\\:ref ]') : null;
            relatedIdStr = idParts != null && idParts.size() > 0 ? idParts[0].trim() : null;
        }
        
        system.debug('### thread is is ' + relatedIdStr);
        if(relatedIdStr != null) relatedIdStr = Cases.getCaseIdFromEmailThreadId(relatedIdStr);
        
        if(relatedIdStr == null) { //If thread Id not found in the email body and the subject, log error and return
        system.debug('### msg body is ' + bodyStr);
            createLog('CTS_LosantEventCaseEmailServiceHandler', 'handleInboundEmail', 'Thread Id not found',bodyStr);
            return null;
        }
        
        system.debug('### the case id of the incoming email is ' + relatedIdStr);
        EmailMessage em = new EmailMessage();
        em.ToAddress =  email.toAddresses != null ? String.join(email.toAddresses, ',') : null;
        em.FromAddress = email.FromAddress;
        em.FromName = email.FromName;
        em.Subject = email.subject;
        em.status = '2';
        em.HtmlBody = email.htmlBody;
        em.Incoming= True;
        em.TextBody = bodyStr;
        em.RelatedToId = relatedIdStr;
        try {
            insert em;
            /* people who reply to the email may not be SF users so we will just set status and not re-assign the case */
            //Id newOwnerId = getUserIdByEmail(em.FromAddress);
            //Case cs = new Case(Id = relatedIdStr, Status = 'Assigned', CTS_RMS_Assigned_Thru_Email_Response__c = true);
            Case cs = new Case(Id = relatedIdStr, Status = 'Assigned', CTS_RMS_Assigned_Thru_Email_Response__c = true);
            //if(newOwnerId != null) {
                //cs.OwnerId = newOwnerId;               
            //}
            update cs;
        } catch(Exception e) {
            System.debug('e**' + e.getMessage());
            createLog('CTS_LosantEventCaseEmailServiceHandler', 'handleInboundEmail', JSON.serialize(e.getMessage()),JSON.serialize(e.getStackTraceString()));            
        }
        return result;
    }
    
    /**
		Find the user by Username first, then by email. 
		If none found, return null; if more than one found, can't determine so return null

    public static Id getUserIdByEmail(String email) {
      Boolean isSbox = [SELECT issandbox FROM Organization].isSandbox;
      String sboxName = UserInfo.getUserName().substringAfterLast('.');
      String uname  = isSbox ? email + '.' + sboxName:email;
        List<User> users = [SELECT Id FROM User WHERE Username =: uname];
        if(users.size() == 1) {
        	return users[0].Id;  
        } else if(users.size() > 1) {
            createLog('CTS_LosantEventCaseEmailServiceHandler', 'getUserIdByEmail', 'More than one users found for the username: ' + email);           
            return null;
        }
        users = [SELECT Id FROM User WHERE Email =: email];
        if(users.size() == 1) {
        	return users[0].Id;  
        } else if(users.size() > 1) {
            createLog('CTS_LosantEventCaseEmailServiceHandler', 'getUserIdByEmail', 'More than one users found for the Email: ' + email);            
            return null;
        } else {
            return null;
        }
        
    }
    **/
    
    private static void createLog(String cName, String mName, String msg, String stacktrace) {
        Apex_Log__c log = new Apex_Log__c();
        log.class_name__c = cName;
        log.method_name__c = mName;
        log.Message__c = msg;
        log.Exception_Stack_Trace_String__c = stacktrace;
        insert log;
    }
}