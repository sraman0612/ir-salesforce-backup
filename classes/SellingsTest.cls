@isTest
public class SellingsTest {
    
    static testMethod void testSellings(){
        string currentYear = string.valueof(system.today().year());
        string nextYear = string.valueof((system.today().year())+1);
        id accRt = Schema.SObjectType.Account.getRecordTypeInfosByName().get('PT_powertools').getRecordTypeId();
        insert new PT_Record_types__c(SetupOwnerId=UserInfo.getOrganizationId(), Account_RT_Id__c=accRt);
        PT_Record_types__c ptrt = PT_Record_types__c.getOrgDefaults();
        
        Profile p = [SELECT Id FROM Profile WHERE Name='PT Standard User']; 
        User u = new User(Alias = 'standt', Email='userTestIrco@ptTest.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='userTestIrco@ptTest.com');
        insert u;
        Account a = new Account(name='test',recordtypeid=ptrt.Account_RT_Id__c);
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * * ' + dt.month() + ' ? ' + dt.year();
        System.runAs(u) {
            test.startTest();
            insert a;
            Selling__c s = new Selling__c(name='January '+nextYear,PT_Account__c=a.id,PT_Year__c=nextYear);
            insert s;
            delete s;
            MassCreateSellingsExistingAccounts t = new MassCreateSellingsExistingAccounts();
            Database.executeBatch(t);
            string jobCreate=ScheduleCreateSellings.scheduleJob(CRON_EXP);
            String jobUpdate=ScheduleUpdateSellings.scheduleJob(CRON_EXP);
            test.stopTest(); 
        }
    }
    
}