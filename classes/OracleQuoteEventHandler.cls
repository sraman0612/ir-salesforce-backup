public class OracleQuoteEventHandler {
  
  public static void markAsPrimary(List<cafsl__Oracle_Quote__c> newLst, boolean beforeInsert){
    if(beforeInsert){ // mark the quote as primary if no other primaries for the related opp
      Set<Id> oppIds = new Set<Id>();
      for (cafsl__Oracle_Quote__c oq : newLst){
        if(null!=oq.cafsl__Opportunity__c)
          oppIds.add(oq.cafsl__Opportunity__c);
      }
      for(cafsl__Oracle_Quote__c oq : [SELECT Id,cafsl__Opportunity__c FROM cafsl__Oracle_Quote__c WHERE cafsl__Opportunity__c IN :oppIds and Primary__c = TRUE]){
        oppIds.remove(oq.cafsl__Opportunity__c);
      }
      for(cafsl__Oracle_Quote__c oq : newLst){
        if(null != oq.cafsl__Opportunity__c && oppIds.contains(oq.cafsl__Opportunity__c))
          oq.Primary__c = TRUE;
      }
    } /*else { //BEFORE UPDATE, set primary status when non primary being submittted
      for(cafsl__Oracle_Quote__c oq : newLst){
        if(oq.Order_Status__c == 'Submitted' && oq.Primary__c == FALSE 
          // && oldMap1.get(oq.Id).Order_Status__c != 'Submitted'
          //&& oldmap1.get(oq.Id).Primary__c!=FALSE
          ){
              oq.Primary__c = TRUE;
        }
      }
    }*/
  }
  
  public static void updateOppAndPriorPrimaryQuote(List<cafsl__Oracle_Quote__c> newLst, Map<Id,cafsl__Oracle_Quote__c> oldMap){
    /*
     if quote marked as primary copy amt to opp, uncheck any others marked as primary
    */
    Map<Id, Decimal> oppAmtMap = new Map<Id, Decimal>();
    Set<Id> currentQuotes = new Set<Id>();
    Set<Id> opps2ClosedWon = new Set<Id>();
    List<Opportunity> otupd = new List<Opportunity>();
    for(cafsl__Oracle_Quote__c oq : newLst){
        if( //quote being marked as primary or amount is changing on a primary quote
            null != oq.cafsl__Opportunity__c && 
            oq.Primary__c && 
            (null == oldMap || !(oldMap.get(oq.Id).Primary__c) || oq.Quote_Amount__c != oldMap.get(oq.id).Quote_Amount__c || oq.Has_Care__c!=oldMap.get(oq.id).Has_Care__c || oq.Has_Completes__c!=oldMap.get(oq.Id).Has_Completes__c || oq.Has_Services__c!= oldMap.get(oq.Id).Has_Services__c
            || oq.cafsl__Opportunity__c!=oldmap.get(oq.id).cafsl__Opportunity__c || oq.Quote_Type__c!=oldmap.get(oq.id).quote_type__c)
  	   	  ){
              oppAmtMap.put(oq.cafsl__Opportunity__c,oq.Quote_Amount__c);
              currentQuotes.add(oq.Id);
              Opportunity otu = new Opportunity();
              otu.Oracle_Quote_Amount__c = oq.Quote_Amount__c;
              otu.Has_Care__c = oq.Has_Care__c;
              otu.Has_Completes__c = oq.Has_Completes__c;
              otu.Has_Services__c = oq.Has_Services__c;
              otu.Id = oq.cafsl__Opportunity__c;
              otu.Type = oq.Quote_Type__c;
              otupd.add(otu);
        }
        if( //status change to submitted
            null != oq.cafsl__Opportunity__c && 
            null != oldMap &&
            oldMap.get(oq.Id).Order_Status__c != 'Submitted' && 
            oq.Order_Status__c == 'Submitted' && oq.Primary__c == FALSE
          ){
               opps2ClosedWon.add(oq.cafsl__Opportunity__c);
               currentquotes.add(oq.Id);
        }
    }
   
      //Added below code to set Primary Flag on Non-Primary Quote when Order status is updated to submitted.
     cafsl__Oracle_Quote__c [] quotePrimary = [SELECT Id, Primary__c 
                                                         FROM cafsl__Oracle_Quote__c 
                                                        WHERE Id IN :currentQuotes];
    if(!quotePrimary.isEmpty()){
      for(cafsl__Oracle_Quote__c oq : quotePrimary){
        oq.Primary__c=TRUE;
      }
      update quotePrimary;
    }
      
  /*  Opportunity [] opps2Update = [SELECT Id, Oracle_Quote_Amount__c,StageName 
                                    FROM Opportunity 
                                   WHERE Id IN :oppAmtMap.keyset() OR Id IN :opps2ClosedWon];
    if(!opps2Update.isEmpty()){
      for (Opportunity o : opps2Update){
        if(oppAmtMap.containsKey(o.Id)){
          system.debug('### updating app amt');
        //  o.Oracle_Quote_Amount__c = oppAmtMap.get(o.Id);
        }
        //if(opps2ClosedWon.contains(o.Id)){o.StageName = 'Closed Won';}
      }
      update opps2Update;
    }*/
    
      if(otupd.size()>0){
          update otupd;
      }
    
    cafsl__Oracle_Quote__c [] quotesToRemovePrimary = [SELECT Id, Primary__c 
                                                         FROM cafsl__Oracle_Quote__c 
                                                        WHERE Primary__c=TRUE AND 
                                                              (cafsl__Opportunity__c IN :oppAmtMap.keyset() OR cafsl__Opportunity__c IN :opps2ClosedWon) AND
                                                              (NOT Id IN :currentQuotes)];
    if(!quotesToRemovePrimary.isEmpty()){
      for(cafsl__Oracle_Quote__c oq : quotesToRemovePrimary){
        oq.Primary__c=FALSE;
      }
      update quotesToRemovePrimary;
    }
  }
  
  public static void preventDelete(List<cafsl__Oracle_Quote__c> oldLst){
    for(cafsl__Oracle_Quote__c  oq : oldLst){oq.addError('Oracle Quotes cannot be deleted from Salesforce.');}
  }
}