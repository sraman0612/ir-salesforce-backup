/*
    @author = George Acker
    This test method will tast the Map Chain Utility (Utility_MapChain).
    The test will pass in keys of various types as well as different types of values.
    Note that since the Map Chain data is of type object and the keys are a list of type object, it is flexible such that you could store anything.
    The tests serve as an example of how to leverage the Utility to store any type of object
*/
@isTest
private class Utility_MapChain_Test {

    static testMethod void myUnitTest() {
        Utility_MapChain chain = new Utility_MapChain();
        
        //put an integer in and make sure we get it back
        integer val = 10;
        chain.putData(new object[]{1,'test',3.0,false},val);
        object retrievedVal = chain.getData(new object[]{1,'test',3.0,false});
        system.assertEquals(val,retrievedVal);
        
        //check for a key that doesn't exist and make sure we get null
        object retrievedNull = chain.getData(new object[]{1,'test',3,false}); //note that 3.0 does not equal 3!
        system.assertEquals(null,retrievedNull);
        
        //put an sObject (user in this case) in and make sure we get it back
        User u = new User();
        chain.putData(new object[]{1,'test',3.0,true},u);
        object retrievedUser = chain.getData(new object[]{1,'test',3.0,true});
        system.assertEquals(u,retrievedUser);
        
        //put in a list of strings, over-writing the previous sObject we placed in, then retrieve them, add a value, retrieve them again and make sure everything checks out
        string[] strings = new string[]{'a','b','c'};
        chain.putData(new object[]{1,'test',3.0,true},strings);
        string[] retrievedStrings = (string[]) chain.getData(new object[]{1,'test',3.0,true});
        system.assertEquals(strings,retrievedStrings);
        retrievedStrings.add('d');
        string[] retrievedStrings2 = (string[]) chain.getData(new object[]{1,'test',3.0,true});
        system.assertEquals(retrievedStrings,retrievedStrings2);
        system.assertEquals(4,retrievedStrings2.size());
        system.assertEquals('d',retrievedStrings2[3]);
        
        //put user back in the chain to make 3 elements
        chain.putData(new object[]{1,'test2',3.0,true},u);
        
        //test the keyset method by making sure all of our keys came back correctly
        List<List<object>> keyset = chain.keyset();
        system.assertEquals(3,keyset.size());
        for(List<object> keys :keyset)
        {
            system.assertEquals(4,keys.size());
            //since we can't guaruntee order, make sure we get back one of the valid keys
            system.assert(keys == new object[]{1,'test2',3.0,true} || keys == new object[]{1,'test',3.0,true} || keys == new object[]{1,'test',3.0,false});
        }
        //none of our keys should be the same.  You should never have a duplicate key
        system.assert(keyset[0] != keyset[1] && keyset[0] != keyset[2] && keyset[1] != keyset[2]);
    }
}