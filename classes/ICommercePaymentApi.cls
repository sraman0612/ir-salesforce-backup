public interface ICommercePaymentApi {
    CommercePaymentApi.ResponseWrapper capture(CommercePaymentApi.RequestWrapper input, String paymentAuthorizationId);
    CommercePaymentApi.ResponseWrapper refundPayment(CommercePaymentApi.RequestWrapper input, String paymentId);
    CommercePaymentApi.ResponseWrapper reverseAuthorization(CommercePaymentApi.RequestWrapper input, String paymentAuthorizationId);

}