/*------------------------------------------------------------
Author:       Sambit Nayak(Cognizant)
Description:  This is the Test class for SalesCallPlan_Page_Extension.
------------------------------------------------------------*/
@istest(seeAllData=false)
public with sharing class SalesCallPlan_Page_Extension_Test {
    
    @testSetup
    static void dataSetup(){
            
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
            User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1@IR.com');
            insert u;
            
            System.runAs(u){
                //commented by CG as part of descoped object
                /*list<QuestionsList__c> questionList=new list<QuestionsList__c>();
            
                questionList.add(TestDataFactory.createQuestionList('Notes from Last Meeting','Sales Call Plan','Q1',u.id));
                questionList.add(TestDataFactory.createQuestionList('Pre-Meeting: Plan','Sales Call Plan','Q2',u.id));
                questionList.add(TestDataFactory.createQuestionList('Discover','Sales Call Plan','Q3',u.id));
                questionList.add(TestDataFactory.createQuestionList('Sell','Sales Call Plan','Q4',u.id));
                questionList.add(TestDataFactory.createQuestionList('Post-Meeting: Confirm','Sales Call Plan','Q5',u.id));
            
                insert questionList;*/
            
                account testAcc=TestDataFactory.createAccount('TestAcc1','IR Comp EU');
                testAcc.ownerid=u.id;
                insert testAcc;
                opportunity testOpp=TestDataFactory.createOpportunity('TestOpty1','IR Comp MEIA',testAcc.id,'No','Budgetary','Stage 1. Qualify','Pipeline');
                testOpp.ownerid=u.id;
                insert testOpp;
            }
    }
    
    public static testMethod void SalesCallPlan_Page_Extension_test(){
        
        User u=[Select id from user where Alias = 'standt'];
        System.runAs(u){
            account testAcc=[Select id from account limit 1];
            
            opportunity testOpp=[Select id from opportunity limit 1];
            //commented by CG as part of descoped object
            //Sales_Call_Plan__c Sc=new Sales_Call_Plan__c(Account_Salescallplan__c=testAcc.id);
            Test.starttest();
            /*ApexPages.StandardController Sales1=new ApexPages.StandardController(Sc);
            SalesCallPlan_Page_Extension Spe1=new SalesCallPlan_Page_Extension(Sales1);
            //Spe1.SaveSalesCall();
            //system.assertequals(Sc.CountModified__c,1);
            //Spe1.CancelSalesCall();
            //Sc.Opportunity_salescallplan__c=testOpp.id;
            //update Sc;
            //ApexPages.StandardController Sales2=new ApexPages.StandardController(Sc);
            //SalesCallPlan_Page_Extension Spe2=new SalesCallPlan_Page_Extension(Sales2);
            //Spe2.accountName='a';
            //Spe2.optyName='o';
            //Spe2.SaveSalesCall();
            //Spe2.CancelSalesCall();
            //Sales_Call_Plan__c Sc2=new Sales_Call_Plan__c(Opportunity_salescallplan__c=testOpp.id);
            //ApexPages.StandardController Sales3=new ApexPages.StandardController(Sc2);
            SalesCallPlan_Page_Extension Spe3=new SalesCallPlan_Page_Extension(Sales3);
            //Spe3.SaveSalesCall();*/
            ApexPages.StandardController Sales1=new ApexPages.StandardController(testAcc);
            SalesCallPlan_Page_Extension Spe2=new SalesCallPlan_Page_Extension(Sales1);
            Spe2.accountName='a';
            Spe2.optyName='o';
            Spe2.optyowner='TestOwner';
            Spe2.accountowner='TestAccountOwner';
            spe2.optyflag=false;
            Spe2.printflag=false;
            
            String qid=testAcc.Id;
            String q ='Test';
            String a ='TestAnswer';
            String c ='Test category';
            
            SalesCallPlan_Page_Extension.questionWrapper qw = new SalesCallPlan_Page_Extension.questionWrapper(q,a,qid,c);
            Test.StopTest();
        }
    }
}