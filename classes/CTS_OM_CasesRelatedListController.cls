/**
    @author Providity Consulting
    @date 15DEC19
    @description COntroller for Ltng Cmp CTS_OM_CasesRelatedList.cmp
*/ 
public with sharing class CTS_OM_CasesRelatedListController {
    
    @AuraEnabled
    public static Object getData(String recordId, String recordTypeId, String recordTypeName, String fields, String filterLookups,String sortedBy) {
        fields = fields.replace(' ', '');
        if(fields.contains('Name') || fields.contains('Id')) {
            Set<String> temp = new Set<String>(fields.split(','));
            temp.remove('Name');
            temp.remove('Id');
            fields = String.join(new List<String>(temp), ', ');
        }
        String objName = 'Case';
        System.debug('fields**' + fields);
        Sobject sobj;
         
        String primaryFilterField;
        String primaryObject;
        if(recordId != null && recordId.startsWith('500')){
            primaryObject = 'Case';
            primaryFilterField = 'ContactEmail';
        }else{
           primaryObject = 'Contact';
            primaryFilterField = 'Email'; 
        }
        try {
            String contactEmail;
            String primaryObjectQuery = 'select id,' +  primaryFilterField + ' FROM '+ PrimaryObject + ' WHERE id = : recordId ';
            for(Sobject s : Database.query(primaryObjectQuery)){
                sobj = s;
                contactEmail = (String)sobj.get(primaryFilterField); 
            }
            System.debug('::contactEmail'+contactEmail);
            
            if(contactEmail != null){
                String q = 'SELECT Id, Show_Record__c,  ' + (!fields.contains('CaseNumber') ? 'CaseNumber,' : '') +  fields + ' FROM ' + objName + ' WHERE ';
                q += ' ContactEmail =:contactEmail ';
                q += recordTypeId != null ? ' AND RecordTypeId = :recordTypeId' : (recordTypeName != null ? ' AND RecordType.Name = :recordTypeName' : '');
                q += ' order by '+ sortedBy + ' DESC';
                System.debug('q**'+q);
                List<Object> accs = Database.query(q);
                fields  = 'Show_Record__c,' + fields;
                Set<String> rFields = new Set<String>();
                rFields.addAll(fields.split(','));
                List<FieldDef> fieldDefs = new List<FieldDef>();
                FieldDef fdef;
                Map <String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
                for(String f : rFields) {
                    System.debug('::f'+f); 
                    if(f.contains('.Name')){
                        fdef = new FieldDef(f.trim(), f.replace('.Name','').trim(), 'STRING');
                        fieldDefs.add(fdef);
                    }else{
                        Schema.SObjectField sfield = fieldMap.get(f.trim());
                        Schema.DescribeFieldResult dfr = fieldMap.get(sfield.getDescribe().getName()).getDescribe();
                        fdef = new FieldDef(String.valueOf(dfr.getName()), String.valueOf(dfr.getLabel()), String.valueOf(dfr.getType()));
                        fieldDefs.add(fdef);
                    }
                }           
                List<Object> retList = new List<Object>{fieldDefs, accs};
                System.debug('retList**'+retList);
                return JSON.serialize(retList);
            }
          } catch(Exception e) {
            throw new AuraException(e);
        }
        return null;        
    }
    
    public class FieldDef {
        @AuraEnabled public String fieldName;
        @AuraEnabled public String label;
        @AuraEnabled public String type;
        @AuraEnabled public Boolean sortable;
        public FieldDef(String n, String l, String t) {
            this.fieldName = n; 
            this.label = l;
            if(t == 'DATE') this.type = 'date-local';
            else if(t == 'DATETIME') this.type = 'date';
            else this.type = t;
            //this.sortable = true; 
        }
    }
}