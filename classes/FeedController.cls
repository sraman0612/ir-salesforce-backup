public without sharing class FeedController {
    @AuraEnabled(cacheable=true)
    public static List<orgWideEmailAddress> orgWideFromAddress(){
        return [Select Id, Address, DisplayName FROM orgWideEmailAddress WHERE IsVerified = true AND IsAllowAllProfiles = true];
    }
    @AuraEnabled(cacheable=true)
    public static List<String> searchEmails(String searchTerm) {
        // Ensure the search term is at least 3 characters
        if (String.isBlank(searchTerm) || searchTerm.length() < 3) {
            return new List<String>();
        }
        List<String> emailResults = new List<String>();
        // Query User emails
        List<User> users = [SELECT Email FROM User WHERE Email LIKE :('%' + searchTerm + '%') LIMIT 50];
        for (User user : users) {
            if (user.Email != null) {
                emailResults.add(user.Email);
            }
        }
        // Query Contact emails
        List<Contact> contacts = [SELECT Email FROM Contact WHERE Email LIKE :('%' + searchTerm + '%') LIMIT 50];
        for (Contact contact : contacts) {
            if (contact.Email != null) {
                emailResults.add(contact.Email);
            }
        }
        return emailResults;
    }
    @AuraEnabled(cacheable=true)
    public static List<ChatterPostWrapper> getChatterPosts(Id caseId) {
        // Query FeedItem (posts) for the given case
        List<FeedItem> feedItems = [SELECT Id, CreatedDate,CreatedBy.Id, CreatedBy.Name, Body
        FROM FeedItem
        WHERE Type = 'TextPost'
        AND ParentId = :caseId
        ORDER BY CreatedDate DESC
        LIMIT 10];
        List<ChatterPostWrapper> postList = new List<ChatterPostWrapper>();
        // Iterate over FeedItems and get associated comments
        for (FeedItem feedItem : feedItems) {
            ChatterPostWrapper post = new ChatterPostWrapper();
            post.Id = feedItem.Id;
            post.Body = feedItem.Body;
            post.CreatedDate = feedItem.CreatedDate;
            post.CreatedByName = feedItem.CreatedBy.Name; // Add the creator's name
            post.CreatedById = feedItem.CreatedBy.Id;
            // Query FeedComments related to this FeedItem
            List<FeedComment> comments = [SELECT Id, CommentBody, CreatedDate, CreatedBy.Name
                                          FROM FeedComment
                                          WHERE FeedItemId = :feedItem.Id
                                          ORDER BY CreatedDate ASC];
            List<ChatterCommentWrapper> commentList = new List<ChatterCommentWrapper>();
            for (FeedComment comment : comments) {
                ChatterCommentWrapper chatterComment = new ChatterCommentWrapper();
                chatterComment.Id = comment.Id;
                chatterComment.Body = comment.CommentBody;
                chatterComment.CreatedDate = comment.CreatedDate;
                chatterComment.CreatedByName = comment.CreatedBy.Name; // Add the creator's name
                chatterComment.CreatedById = comment.CreatedBy.Id;
                commentList.add(chatterComment);
            }
            post.Comments = commentList;
            postList.add(post);
        }
        return postList;
    }
    @AuraEnabled(cacheable=true)
    public static List<EmailMessage> getCaseEmails (Id caseId){
        return [SELECT Id, Subject, Status, Incoming, FromName, CreatedDate, HtmlBody, TextBody, FromAddress, 
                ToAddress, CcAddress,BccAddress, CreatedById, CreatedBy.Name, hasAttachment
                FROM EmailMessage 
                WHERE ParentId = :caseId
                AND Status != '5'
                ORDER BY CreatedDate DESC
                LIMIT 10];
    }
    @AuraEnabled
    public static void postComment(String postId, String commentBody) {
        // Create a new FeedComment and insert it
        FeedComment newComment = new FeedComment();
        newComment.FeedItemId = postId;
        newComment.CommentBody = commentBody;
        insert newComment;
    }
    @AuraEnabled
    public static void sendEmail(Id emailId, List<String> attachmentIds, String fromAddress, List<String> toAddresses, List<String> ccAddresses, List<String> bccAddresses, String subject, String emailBody){
        EmailMessage originalEmail = [SELECT Id, hasAttachment, ThreadIdentifier, ParentId, ReplyToEmailMessageId
                                      FROM EmailMessage WHERE Id = :emailId LIMIT 1];
        orgWideEmailAddress fromAddressId = [Select Id, Address FROM orgWideEmailAddress WHERE Address = :fromAddress LIMIT 1];
        //String threadToken = EmailMessages.getFormattedThreadingToken(originalEmail.ParentId);
        Case threadToken = [Select Id, ThreadID__c FROm case where Id = :originalEmail.ParentId];
        String customSubject = threadToken != null ? subject + ' # ' + threadToken.ThreadID__c : subject;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
        email.setToAddresses(toAddresses);
        if (ccAddresses != null && ccAddresses.size() > 0) {
            email.setCCAddresses(ccAddresses);
        }
        if(bccAddresses != null && bccAddresses.size() > 0) {
            email.setBccAddresses(bccAddresses);
        }
        //check customSubject contains threadToken
        if(!subject.contains(threadToken.ThreadID__c)) {
            email.setSubject(customSubject);
        }
        else{
            email.setSubject(subject);
        }
        System.debug('received attachmentIds: ' + attachmentIds);
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        if(attachmentIds != null && attachmentIds.size() > 0) {
            contentVersionList = [
               SELECT VersionData, Title, FileExtension, ContentDocumentId
               FROM ContentVersion
               WHERE ContentDocumentId IN :attachmentIds
               ORDER BY VersionNumber DESC
           ];
            for(ContentVersion cv : contentVersionList){
                Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                System.debug('Content Version: ' + cv);
                System.debug('cv.Title: ' + cv.Title +'.'+cv.FileExtension);
                System.debug('cv.VersionData: ' + cv.VersionData);
                attachment.setFileName(cv.Title);
                attachment.setBody(cv.VersionData);
                emailAttachments.add(attachment);
            }
        }
        if(emailAttachments != null && emailAttachments.size() > 0) {
            email.setFileAttachments(emailAttachments);
        }
        email.setHtmlBody(emailBody);
        email.setOrgWideEmailAddressId(fromAddressId.Id);
        email.setWhatId(originalEmail.ParentId);
        email.setInReplyTo(originalEmail.ReplyToEmailMessageId);
        email.setReferences(originalEmail.ThreadIdentifier);
        System.debug('Email: ' + email);
        
        Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {email};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        if (results[0].success) {
            System.debug('The email was sent successfully.'+results[0]);
            System.debug('The email was sent successfully.');
        } else {
            System.debug('The email failed to send: '
                  + results[0].errors[0].message);
        }
        if(attachmentIds != null && attachmentIds.size() > 0) {
            EmailMessage newEmail = [Select Id FROM EmailMessage WHERE CreatedDate = today AND (subject = :customSubject OR subject = :subject) ORDER BY CreatedDate Desc LIMIT 1];        
            List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
            for(ContentVersion cv : contentVersionList){
                ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
                contentDocumentLink.ContentDocumentId = cv.ContentDocumentId;
                contentDocumentLink.LinkedEntityId = newEmail.Id;
                contentDocumentLink.ShareType = 'V';
                contentDocumentLink.Visibility = 'AllUsers';
                cdlList.add(contentDocumentLink);
            }
            insert cdlList;
        }
    }
    @AuraEnabled
    public static void markEmailAsRead(Id emailId){
        EmailMessage email = [SELECT Id, Status FROM EmailMessage WHERE Id = :emailId LIMIT 1];
        if(email != null && email.Status != '1'){
            email.Status = '1';
            update email;
            System.debug('Email status changed to read '+email.Id);
        }
    }
    @AuraEnabled(cacheable=true)
    public static List<ContentDocument> getEmailAttachments(Id emailMessageId) {
       List<ContentDocumentLink> linkList = [SELECT ContentDocumentId,LinkedEntityId 
                                             FROM ContentDocumentLink 
                                             WHERE LinkedEntityId = :emailMessageId];
        Set<Id> ContentDocumentIds  = new Set<Id>();
        for(ContentDocumentLink link : LinkList){
            ContentDocumentIds.add(link.ContentDocumentId);
        }
        return [SELECT Id, Title,CreatedDate, FileExtension,ContentSize, 
                LatestPublishedVersionId,LatestPublishedVersion.File_Size__c
                FROM ContentDocument WHERE Id IN :ContentDocumentIds AND FileExtension != 'snote'];                                   
    }
    @AuraEnabled
    public static ContentDocument uploadFiles(Id emailMessageId, String filename, String base64) {    
       ContentVersion cv = new ContentVersion();
       cv.Title = filename;
       cv.PathOnClient = '/' + filename;
       cv.VersionData = EncodingUtil.base64Decode(base64);
       insert cv;
       Id contentDocId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;
       ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
       contentDocumentLink.ContentDocumentId = contentDocId;
       contentDocumentLink.LinkedEntityId = emailMessageId;
       contentDocumentLink.ShareType = 'V';
       contentDocumentLink.Visibility = 'AllUsers';
       
       insert contentDocumentLink;
       System.debug('File uploaded successfully '+contentDocumentLink);
       return [SELECT Id, Title,CreatedDate, FileExtension, ContentSize, 
                LatestPublishedVersionId,LatestPublishedVersion.File_Size__c
                FROM ContentDocument WHERE Id = :contentDocId AND FileExtension != 'snote' LIMIT 1];        
   }

    public class ChatterPostWrapper {
        @AuraEnabled public String Id;
        @AuraEnabled public String Body;
        @AuraEnabled public Datetime CreatedDate;
        @AuraEnabled public String CreatedByName; // Creator's name
        @AuraEnabled public String CreatedById;
        @AuraEnabled public String instanceURL = System.URL.getOrgDomainUrl().toExternalForm();
        @AuraEnabled public List<ChatterCommentWrapper> Comments;
    }
    public class ChatterCommentWrapper {
        @AuraEnabled public String Id;
        @AuraEnabled public String Body;
        @AuraEnabled public Datetime CreatedDate;
        @AuraEnabled public String CreatedByName; // Creator's name
        @AuraEnabled public String CreatedById;
    }
 }