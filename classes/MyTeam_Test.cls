@istest//(seeAllData=false)
public class MyTeam_Test {
    @testSetup
    static void setupData(){
        User user = CTS_TestUtility.createUser(false);
        user.Email = 'VPTECH1@test.com';
        user.FirstName = 'CPI';
        user.LastName = 'Integration';
        user.Assigned_From_Email_Address__c =null;
        insert user;
        
        
        System.runAs(user){
            List<Account> accList = new List<Account>();
            List<Business_Relationship__c> brList = new List<Business_Relationship__c>();
            
            Account acct = new Account();
            acct.name = 'VPTECH Test Account';
            acct.Type = 'Ship To';
            acct.SAP_Account_External_ID__c = 'Parent';
            acct.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('GD_Parent').getRecordTypeId();
            insert acct;
            
            Business_Relationship__c br = new Business_Relationship__c();
            br.BR_Owner__c = UserInfo.getUserId();
            br.Account__c = acct.id;
            br.Type__c = 'Ship To';
            insert br;
            
            
        }
    }
    
    public static testmethod void checkOwnerTest(){
        
        User u = [Select id,Profile.name,FirstName,LastName from User Where EMail ='VPTECH1@test.com' LIMIT 1];
        System.debug('Profile : ' + u.Profile.name + ' , FirstName :  '+ u.FirstName + ' , LastName :  '+u.LastName);
        System.runAs(u){ 
           // test.startTest();
            Business_Relationship__c br = [Select id  from Business_Relationship__c Where Account__r.name ='VPTECH Test Account' LIMIT 1];
            
            MyTeam.getMyTeamAccount(10,1);
            //test.stopTest();
        }
    }
    
}