/**
 * @description       : 
 * @author            : RISHAB GOYAL
 * @group             : 
 * @last modified on  : 07-27-2023
 * @last modified by  : RISHAB GOYAL
**/
public class LeadToCaseConvertController {
    
    private static void setOwnerQueue(Lead myLead, Case MyCase) {

        if (myLead.VPTECH_Brand__c  != null) {

            String brand = myLead.VPTECH_Brand__c.toUpperCase();

            if (brand.contains('TRICONTINENT')){
                MyCase.OwnerId = MyCase.CTS_Region__c == 'EU' ? Label.TriContinent_EMEIA : Label.TriContinent;
            }
            else if (brand.contains('ILS') || brand.contains('ZINSSER ANALYTIC')){
                MyCase.OwnerId = Label.TriContinent;
            }
            else if (brand.contains('THOMAS') || brand.contains('OINA')){
                MyCase.OwnerId = Label.Thomas;
            }
            else if (brand.contains('WELCH')){
                MyCase.OwnerId = MyCase.CTS_Region__c == 'EU' ? Label.Welch_EMEIA : Label.Welch;
            }
        }
    }
    
    @AuraEnabled
    public static string caseMapping(string lead_id){
      
        String returnvalue;  
        Map<string, string> MapMappingTable = new map<string,string>();
        String qry;
        
        try{
            
            qry='';
            
            for (Lead_To_Case__c mappingTableRec : Lead_To_Case__c.getall().Values()){
                    
                if (mappingTableRec.Name != null && mappingTableRec.Case_Fields__c != Null){
                    
                    MapMappingTable.put(mappingTableRec.Name , mappingTableRec.Case_Fields__c);
                    qry += mappingTableRec.Name + ',';
                }
            }
            
            qry = 'select ' + qry + 'id FROM Lead where id =: lead_id';
            Lead MyLead = Database.query(qry);
            
            Case  ObjCase = new Case();
            objCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IR Comp OM Account Management').getRecordTypeId();
            for (String sMyLeadField: MapMappingTable.keySet()){
                String sLeadField = MapMappingTable.get(sMyLeadField);
                ObjCase.put(sLeadField, MyLead.get(sMyLeadField));
            }
        
            setOwnerQueue(MyLead, ObjCase);
            MyLead.Status= 'Rejected';
            MyLead.Disposition_Reason__c = 'Other';
            MyLead.Disposition_Reason_Other__c = 'Converted to Case';
            System.debug(ObjCase);
            update MyLead;
            System.debug('Starting Case Insert');
            insert ObjCase;

            returnvalue ='Success';
        }
        catch(Exception ex){
            system.debug('ex'+ex);
            returnvalue ='Case Not Created';   
        }
      
        return returnvalue;  
    }
}