/**
    @author Providity
    @date 5MAY2019
    @description Test class for LightningApexHelperController.cls
*/

@isTest
private class LightningApexHelperControllerTest {
    
    @testSetup
    static void setupData() {
        Id AirNAAcctRT_ID = [Select Id, DeveloperName from RecordType 
                                    where DeveloperName = 'CTS_MEIA' 
                                    and SobjectType='Account' limit 1].Id;
        Account acc = new Account();
        acc.Name = 'srs_1';
        acc.BillingCity = 'srs_!city';
        acc.BillingCountry = 'USA';
        acc.BillingPostalCode = '674564569';
        acc.BillingState = 'CA';
        acc.BillingStreet = '12, street1678';
        acc.Siebel_ID__c = '123456';
        acc.ShippingCity = 'city1';
        acc.ShippingCountry = 'United States';
        acc.ShippingState = 'CA';
        acc.ShippingStreet = '13, street2';
        acc.ShippingPostalCode = '123';  
        acc.County__c = 'testCounty';
        acc.RecordTypeId = AirNAAcctRT_ID;
        insert acc;
      
      /*
        Account acc = new Account(Name = 'Test Account', ShippingCountry  = 'United States');
        insert acc;
      */
    }
    static testMethod void testController() {        
        Test.startTest();        
        List<sObject> result = LightningApexHelperController.executeSoql('SELECT Id FROM Account LIMIT 1');
        //result should have one record
        System.assert(result.size() == 1);
        String fieldResult = LightningApexHelperController.getFields('Case', 'CTS_Case_Search_Fields');
        //field set result should not be null
        System.assert(String.isNotBlank(fieldResult));
        
        String fieldResult2 = LightningApexHelperController.getFields('Case', null);//without fielset name
        System.assert(String.isNotBlank(fieldResult2));//It would return all field names
    }
}