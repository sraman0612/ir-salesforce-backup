// 
// (c) 2017 IR Enterprise CRM
//
//Generate an Handler Class to Handle trigger on Lead
//
// 12-06-2017 Scott Urbanski

public class CHVAC_Lead_TriggerHandler {

    public static void beforeInsert(Lead[] newList)
    {
        AssignmentEngineV2.handleAssignment(newList, null);
    }
    
    public static void beforeUpdate(Lead[] newList, Map<Id,Lead> oldMap)
    {
        AssignmentEngineV2.handleAssignment(newList, oldMap);
    }

     // Method to delete the record once Approved by Approver
    public static void afterUpdate(List<Lead> newList, Map<Id,Lead> oldMap){
     //for CTS Delete Approval process, not being used - UtilityClass.deleteApprovedRecords(newList,'Lead',UtilityClass.GENERIC_APPROVAL_API_NAME);     //Record deleted
    }
      
}