@isTest
private class CTSCompTechEmailServiceTest3 {
    @isTest
    static void validateCTSCompTechEmailService() {

        // Email
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.fromAddress = 'mgcantoni.es@gmail.com';
        email.subject = 'Compressor 4 Pre-sale Missed Contact';
        String [] htmlText = new List<String>();
        htmlText.add('<P>You have a new voice message from a caller from 3143013675</P>');
        htmlText.add('');
        htmlText.add('<P>Contact ID: 167814331699</P');
        htmlText.add('<P>Date/Time: 10/19/2021 4:30:04 PM</P><P>Skill: 846941 - </P>');
        htmlText.add('<P>Number Dialed: 8338718075</P>');
        htmlText.add('<P>CallerID: 3143013675</P>');
        email.htmlBody = string.join(htmlText,'\n');

        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        String blobValue = 'This is a test of the Emergency Broadcast System';
        attachment.body = blob.valueOf(blobValue.repeat(60000));
        attachment.fileName = 'textfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };

        CTSCompTechEmailService testCTSCompTechEmailService = new CTSCompTechEmailService();
        testCTSCompTechEmailService.handleInboundEmail(email, env);

        List<Case> createdCases = [Select Id from Case];

       // System.assert(!createdCases.isEmpty(),'List should not be empty');

       //System.assert(createdCases.size() == 1,'1 case record should be returned');
	
    }
}